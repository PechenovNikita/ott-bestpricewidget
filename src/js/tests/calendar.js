'use strict';

import DateSelect from './../components/dateSelect';
import {generateMonthList} from './../mixins/Date';



let container = document.getElementById('test-calendar');

let selector = new DateSelect(container);

selector.__loading();
selector.initMonthList(generateMonthList());
setTimeout(function(){
  selector.__unloading();
},1000);