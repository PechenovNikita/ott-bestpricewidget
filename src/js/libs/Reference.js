'use strict';

import {byIATA as cityByIATA, byPart as cityByPart} from './reference/city';
import {byIATA as countryByIATA, byPart as countryByPart} from './reference/country';
import {byIATA as airportByIATA} from './reference/airline';

import sortPlaceData from './reference/mixins/sortPlaceData';

// /**
//  *
//  * @type {cityByIATA}
//  */
// export const getCity = cityByIATA;

// /**
//  *
//  * @type {cityByPart}
//  */
// export const getCityPart = cityByPart;

// /**
//  *
//  * @type {countryByIATA}
//  */
// export const getCountry = countryByIATA;

// /**
//  *
//  * @type {countryByPart}
//  */
// export const getCountryPart = countryByPart;

/**
 *
 * @param {string} part
 * @return {Array.<PlaceData>}
 */
export function getAllByPart(part) {
  let cities = cityByPart(part)
    , countries = countryByPart(part);

  return [].concat(cities, countries).sort(function (placeDataA, placeDataB) {
    return sortPlaceData(placeDataA, placeDataB, part);
  });
}
/**
 *
 * @param {sring} iata
 * @return {PlaceData|null}
 */
export function byIATA(iata) {
  return cityByIATA(iata) || airportByIATA(iata) || countryByIATA(iata) || null;
}
