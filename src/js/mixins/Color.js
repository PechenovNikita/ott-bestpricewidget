'use strict';

import {hexToRgb} from './color/mixins';
import Gradient from './color/Gradient';

/**
 *
 * @param {number} r
 * @param {number} g
 * @param {number} b
 * @return {string}
 */
export function rgbToHex(r, g, b) {
  return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

/**
 *
 * @param {number} r
 * @param {number} g
 * @param {number} b
 * @return {string}
 */
export function rgbToString(r, g, b) {
  return `rgb(${r}, ${b}, ${b})`;
}

/**
 *
 * @param {number} r
 * @param {number} g
 * @param {number} b
 * @param {number} alpha
 * @return {string}
 */
export function rbgToStringWithAlpha(r, g, b, alpha) {
  return `rgba(${r}, ${b}, ${b}, ${alpha})`;
}
/**
 *
 * @param color
 * @return {{r: Number, b: Number, g: Number}}
 */
function parseRGB(color) {
  let f = color.split(','), R = parseInt(f[0].slice(4)), G = parseInt(f[1]), B = parseInt(f[2]);
  return {
    r : R,
    b : B,
    g : G
  };
}

function shadeRGBString(color, percent) {
  let f = color.split(','), t = percent < 0 ? 0 : 255, p = percent < 0 ? percent * -1 : percent, R = parseInt(f[0].slice(4)), G = parseInt(f[1]), B = parseInt(f[2]);
  return 'rgb(' + (Math.round((t - R) * p) + R) + ',' + (Math.round((t - G) * p) + G) + ',' + (Math.round((t - B) * p) + B) + ')';
}

function shadeRGB(rgb, percent) {
  let t = percent < 0 ? 0 : 255, p = percent < 0 ? percent * -1 : percent;
  return 'rgb(' + (Math.round((t - rgb.r) * p) + rgb.r) + ',' + (Math.round((t - rgb.g) * p) + rgb.g) + ',' + (Math.round((t - rgb.b) * p) + rgb.b) + ')';

}
/**
 *
 * @param colorString
 * @return {?{r: Number, b: Number, g: Number}}
 */
function toRGB(colorString) {
  if (colorString.r && colorString.b && colorString.g)
    return colorString;
  if (typeof colorString === 'string') {
    if (colorString.indexOf('#') == 0 || colorString.length == 6)
      return hexToRgb(colorString);
    else if (colorString.indexOf('rgb') == 0)
      return parseRGB(colorString);
  }
  return null;
}

function setAlphaToRGB(rgb, alpha) {
  return `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${alpha})`;
}

export default {

  toRGB         : toRGB,
  setAlphaToRGB : setAlphaToRGB,

  shadeRGBString : shadeRGBString,
  shadeRGB       : shadeRGB,

  isLight    : function (colorString) {
    let rgb = toRGB(colorString);
    return (
      (0.213 * rgb.r +
      0.715 * rgb.g +
      0.072 * rgb.b) > 255 / 2
    );
  },
  /**
   *
   * @param gradientOptions
   * @return {Gradient}
   */
  gradient   : function (gradientOptions) {
    return new Gradient(gradientOptions);
  },
  inGradient : function (gradientOptions, delta) {
    let gr = new Gradient(gradientOptions);
    return gr.find(delta);
  }
};