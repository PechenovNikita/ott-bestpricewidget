const webpack = require('webpack')
  , autoprefixer = require('autoprefixer')
  , precss = require('precss');

module.exports = {

  entry  : {'app' : './src/js/widget.js'},
  target : 'web',
  output : {
    filename          : 'bestPrice-widget.js',
    library           : ['OTTBPWidget'],
    libraryTarget     : 'var',
    sourceMapFilename : 'bestPrice-widget.map'
  },

  module    : {
    preLoaders  : [
      {
        test    : /\.js$/,
        exclude : /node_modules/,
        loader  : "eslint-loader",
        options : {
          // eslint options (if necessary)
        }
      }
    ],
    loaders     : [
      {
        test    : /.js$/,
        loader  : 'babel-loader',
        exclude : /node_modules/,

        query : {
          compact : false,
          presets : ['es2015', /*'babili', */'stage-0'],
        }
      },
      {
        test   : /\.css$/,
        loader : "to-string-loader!css-loader!postcss-loader",
        // loader : "style-loader!css-loader!postcss-loader",
      }
    ],
    postLoaders : [
      {
        test   : /\.jsx?$/,
        loader : 'es3ify-loader'
      }
    ]
  },
  postcss   : function () {
    return [autoprefixer, precss];
  },
  devtool   : 'source-map',
  externals : {
    // 'react-dom' : 'ReactDOM',
    // 'react'     : 'React',
    // 'pikaday'   : 'Pikaday',
    'loader'      : 'Loader',
    'logger'      : 'Logger',
    'lib-Airline' : 'Airline',
    'lib-refData' : 'refData'
    // 'stations'  : 'Stations'
  },
  // plugins   : [
  //   new webpack.optimize.UglifyJsPlugin({
  //     compress : {
  //       warnings  : true
  //     }
  //   })
  // ],
  resolve   : {
    extensions : ['', '.js']
  }
};

