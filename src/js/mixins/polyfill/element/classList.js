'use strict';

// IE9 SUPPORT

import {trim} from './../String';
/**
 *
 * @type {Function}
 * @param {HTMLElement} el
 * @return {ClassList|{}|DOMTokenList}
 */
const classList = (function () {
  if (!('classList' in document.createElement('_')) || document.createElementNS && !('classList' in document.createElementNS('http://www.w3.org/2000/svg', 'g'))) {
    return (el) => new ClassList(el);
  } else {
    return (el) => {
      if (!el.classList)
        return new ClassList(null);
      return el.classList;
    };
  }
})();

export default classList;

class ClassList {
  constructor(element) {
    this._element = element;

    if (element && element.getAttribute) {

      let _className = trim(element.getAttribute('class') || '');

      /**
       * @type {Array.<string>}
       * @private
       */
      this._classes = _className ? _className.split(/\s+/) : [];
    } else {
      this._element = null;
      this._classes = [];
    }
    // @TODO Array[]
    // for (let i = 0; i < this._classes.length; i++) {
    //   this[i] = this._classes[i];
    // }
    //
    // this.length = this._classes.length;

  }

  __update() {
    if (this._element && this._element.setAttribute)
      this._element.setAttribute('class', this._classes.join(' '));
  }

  contains(str) {
    return this._classes.indexOf(str) > -1;
  }

  add(str) {
    if (!this.contains(str)) {
      this._classes.push(str);
      this.__update();
    }
  }

  remove(str) {
    if (this.contains(str)) {
      this._classes.splice(this._classes.indexOf(str), 1);
      this.__update();
    }
  }

  // @TODO doesnot use
  // toggle(str) {
  //   if (this.contains(str))
  //     this.remove(str);
  //   else
  //     this.add(str);
  // }

  // @TODO item()
}

