'use strict';
/* eslint no-empty: "off" */
/**
 *
 * @return {function(element:HTMLElement, selector:string):(HTMLElement|null)}
 * @private
 */
function _closest() {
  if (window.Element && Element.prototype.closest) {
    return (element, selector) => Element.prototype.closest.call(element, selector);
  }
  return (element, selector) => {
    let matches = (window.document || window.ownerDocument).querySelectorAll(selector), i, el = element;
    do {
      i = matches.length;
      while (--i >= 0 && matches.item(i) !== el) {}
    } while ((i < 0) && (el = el.parentElement));
    return el;
  };
}
/**
 *
 * @type {function(element:HTMLElement, selector:string):(HTMLElement|null)}
 */
export let closest = _closest();