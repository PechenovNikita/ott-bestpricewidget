'use strict';

import Query from './controller/Query';
import ControllerResults from './controller/Results';

import {defaultFormData} from './../config/_default';
import {deepExtend} from './../mixins/Deep';

export default class Controller {
  /**
   *
   * @param {EventStore} eventStore
   */
  constructor(eventStore) {
    /**
     *
     * @type {EventStore}
     * @private
     */
    this._eventStore = eventStore;

    /**
     * @type {FormatMainForm}
     * @private
     */
    this._formData = deepExtend(defaultFormData);

    this.__init().__initEvents();
  }

  /**
   *
   * @return {Controller}
   * @private
   */
  __init() {
    /**
     *
     * @type {ControllerResults}
     * @private
     */
    this._results = new ControllerResults(this._eventStore);
    /**
     *
     * @type {?Query}
     * @private
     */
    this._query = null;
    return this;
  }

  /**
   *
   * @private
   */
  __updateResults() {
    let query = new Query(this._formData);

    if ((!this._query || !query.isSameFull(this._query)) && query.$canBestDeals) {

      let loading = false;

      // Eve.dispatch('Controller::load:results');
      //Общий запрос без дат
      loading = (this._results.common(query) || loading);

      //Запрос как он есть
      loading = (this._results.bestDeals(query) || loading);

      //Запрос по месяцам
      loading = (this._results.monthly(query) || loading);

      // запрос по дате отправления
      loading = (this._results.fromDateToMonthly(query) || loading);

      // Запрос к api перелетов если полностью выбраны все данные
      loading = (this._results.fullSearch(query) || loading);

      if (loading)
        this._eventStore.dispatch('Controller::load:results', this._results.show(query));
      else
        this._eventStore.dispatch('Controller::show:results', this._results.show(query));
    }

    this._query = query;
  }

  /**
   *
   * @return {Controller}
   * @private
   */
  __initEvents() {

    this._eventStore.on(['Form::change'], function (event) {

      this._formData = event.detail;
      this.__updateResults();

    }.bind(this));

    this._eventStore.on(['Results::add:city'], function (event) {
      let iata = event.detail.iata;

      if (this._results.addCity(this._query, iata)) {
        this._eventStore.dispatch('Controller::load:results', this._results.show(this._query));
      } else {
        this._eventStore.dispatch('Controller::show:results', this._results.show(this._query));
      }

    }.bind(this));

    this._eventStore.on(['Results::done:request'], function (/*{detail:{query:Query,results:Results}}*/event) {

      if (this._query && this._query.isSame(event.detail.query)) {
        this._eventStore.dispatch('Controller::show:results', event.detail.results.show(this._query));
      }

    }.bind(this));

    return this;
  }
}