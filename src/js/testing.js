'use strict';

import Arr from './mixins/polyfill/Array';

import {isNative} from './mixins/Format';

describe('ARRAY', function () {

  it("перебор массива forEach", function () {
    assert.equal((() => {
      let temp = 0;
      Arr.forEach([0, 1, 2, 3, 4, 5], function (item) {
        temp += item;
      });
      return temp;
    })(), 15);
  });

  it("перебор массива map", function () {
    assert.deepEqual((() => {
      return Arr.map([0, 1, 2, 3, 4, 5], function (item) {
        return item + 1;
      });
    })(), [1, 2, 3, 4, 5, 6]);
  });

  it("перебор массива reduce", function () {
    assert.equal((() => {
      return Arr.reduce([0, 1, 2, 3, 4, 5], function (initValue, item) {
        return initValue + item;
      }, 1);
    })(), 16);
  });

  it("перебор массива filter", function () {
    assert.deepEqual((() => {
      return Arr.filter([0, 1, 2, 3, 4, 5, 6], function (item) {
        return item % 2 == 0;
      });
    })(), [0, 2, 4, 6]);
  });

});

describe("IS", function () {
  it("Нативно или нет", function () {
    assert.isTrue(isNative(Array.prototype.slice), 'Array.prototype.slice нативный');
    assert.isFalse(isNative(function () {}), 'function(){} не нативный');
  });
});