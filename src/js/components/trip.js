'use strict';

import Direction from './trip/direction';
import {docCreate} from './../mixins/Element';

import {price} from './../mixins/Format';
import {declOfNum} from './../mixins/Format';

import {link} from './../libs/OneTwoTrip';

import {byIATA as getCityByIata} from '../libs/reference/city';

import {countryResults, cityResults} from './trip/texts';

import ArrowSVG from '../mixins/svg/arrow';

import Loader from './loader';

export class TripLoader {
  /**
   *
   * @param {HTMLElement} container
   */
  constructor(container) {
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._container = container;
    this.__initHTML();
    this._container.appendChild(this._node);
  }

  __initHTML() {
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._node = docCreate('div', 'ottbp-trip-loader');
    this._loader = new Loader(this._node, {small : true});

    return this;
  }

  show() {
    this._loader.show();
  }

  hide() {
    this._loader.hide();
  }
}

export class TripText {
  /**
   *
   * @param container
   * @param {EventStore} eventStore
   */
  constructor(container, eventStore) {
    this._eventStore = eventStore;
    this._container = container;
    this.__initHTML().__initEvents();
    this._container.appendChild(this._node);
  }

  /**
   *
   * @return {TripText}
   * @private
   */
  __initHTML() {
    this._node = docCreate('div', 'ottbp-trip-text');
    return this;
  }

  /**
   *
   * @return {TripText}
   * @private
   */
  __initEvents() {
    this._node.addEventListener('click', (event) => {
      let target = event.target;
      while (target && target !== this._node) {
        if (target.hasAttribute && target.hasAttribute('data-iata')) {
          event.stopPropagation();
          event.preventDefault();
          // this._eventStore.dispatch('Results::add:city', {iata : target.getAttribute('data-iata')});
          this._eventStore.dispatch('Form::update:to:city', {value : target.getAttribute('data-iata')});
          return;
        }
        target = target.parentNode;
      }

    });

    return this;
  }

  clean() {
    return this.insert('');
  }

  /**
   *
   * @param {ResultsShow} options
   */
  test(options) {
    if (options.loading)
      this.clean();
    else {
      let insert = '';

      if (options.query.$isDestinationCountry) {
        // страна!
        insert = countryResults(options.store, options.query, options.results);

      } else {
        // город!
        insert = cityResults(options.query, options.results);
      }

      this.insert(insert);
    }
  }

  insert(text) {
    this._node.innerHTML = text;
    if (!text || text.length <= 0)
      this._node.setAttribute('style', 'display: none !important;');
    else
      this._node.style.display = '';
    return this;
  }
}

export default class Trip {
  /**
   *
   * @param {HTMLElement} container
   * @param {Response} options
   */
  constructor(container, options = {}) {
    this._container = container;
    /**
     *
     * @type {Response|{}}
     * @private
     */
    this._options = {};

    this.__parseOptions_v2(options).initHTML();

  }

  /**
   *
   * @param {Response} response
   * @return {Trip}
   * @private
   */
  __parseOptions_v2(response) {
    /**
     *
     * @type {Response}
     * @private
     */
    this._response = response;

    this._options.price = price(response.$price, response.$currency);
    let origin = getCityByIata(response.$from);
    this._options.origin = origin ? origin.city.ru : '';
    let destination = getCityByIata(response.$to);
    this._options.destinationCity = destination ? destination.city.ru : '';

    this._options.duration = response.$duration;
    this._options.durationString = declOfNum(this._options.duration, ['день', 'дня', 'дней']);

    let directions = response.$directions;

    if (directions[0])
      this._options.directionThere = directions[0];

    if (directions[1])
      this._options.directionBack = directions[1];

    return this;
  }

  /**
   *
   * @return {Trip}
   */
  initHTML() {

    this._node = docCreate('div', 'ottbp-trip');
    this.__initHeaderHTML().__initBodyHTML().__initFooterHTML();
    this._container.appendChild(this._node);
    return this;
  }

  /**
   *
   * @return {Trip}
   * @private
   */
  __initHeaderHTML() {
    let header = docCreate('div', 'ottbp-trip__header');

    let innerHTML = '<div class="ottbp-trip__title">';
    innerHTML += `<span class="ottbp-trip__title__inner ottbp-black">${this._options.destinationCity}</span>`;
    if (this._options.duration > 0) {
      innerHTML += `<span class="ottbp-trip__title__inner ottbp-gray">${`, на ${this._options.duration} ${this._options.durationString}`}</span>`;
    }
    innerHTML += '</div>';

    header.innerHTML = innerHTML;

    this._node.appendChild(header);
    return this;
  }

  /**
   *
   * @return {Trip}
   * @private
   */
  __initBodyHTML() {
    let body = docCreate('div', 'ottbp-trip__body')
      , list = docCreate('div', 'ottbp-trip__directions');

    let firstColumn = docCreate('div', 'ottbp-trip__directions__item');

    this._startDate = null;
    this._endDate = null;
    /**
     *
     * @type {Direction}
     */
    let tempDirection = new Direction(firstColumn, {
      directionName : 'Туда',
      type          : 'from'
    }, {direction : this._options.directionThere});

    list.appendChild(firstColumn);
    this._startDate = tempDirection.$startDate;

    this._from = tempDirection.$from;
    this._to = tempDirection.$to;

    if (this._options.directionBack) {
      let secondColumn = docCreate('div', 'ottbp-trip__directions__item');
      /**
       *
       * @type {Direction}
       */
      let tempDirection = new Direction(secondColumn, {
        directionName : 'Обратно',
        type          : 'to'
      }, {direction : this._options.directionBack});
      list.appendChild(secondColumn);
      this._endDate = tempDirection.$startDate;
    } else {
      list.className = 'ottbp-trip__directions ottbp-trip__directions--only_one';
    }

    body.appendChild(list);

    this._node.appendChild(body);

    return this;

  }

  /**
   *
   * @return {Trip}
   * @private
   */
  __initFooterHTML() {
    let url = link({}, this._response);
    // let url = generateLink(this._from, this._to, this._startDate, this._endDate, 'E'/*Query.options().selects.type*/);

    let arrow = (new ArrowSVG()).toString();

    let footer = docCreate('div', 'ottbp-trip__footer')
      ,
      price = `<div class="ottbp-trip__price"><span class="ottbp-trip__price__inner">${this._options.price}</span></div>`
      ,
      btn = `<div class="ottbp-trip__btn"><a href="${url}" type="button" target="_blank" class="ottbp-btn-link"><span class="ottbp-btn-link__inner">Выбрать</span>${arrow}</a></div>`;

    footer.innerHTML = price + btn;
    this._node.appendChild(footer);

    return this;
  }
}