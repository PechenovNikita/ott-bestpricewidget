'use strict';

// import  from '';

export const DEALS =
  [
    {
      'currency'    : 'RUB',
      'price'       : 18045,
      'dateCreated' : 1490218242200,
      'is2OW4RT'    : false,
      'fareType'    : 'direct',
      'departDate'  : '20171010',
      'from'        : 'MOW',
      'to'          : 'NYC',
      'to_country'  : 'US',
      'directions'  : [[{
        'from'          : 'SVO',
        'to'            : 'JFK',
        'continued'     : false,
        'startDate'     : '20171010',
        'startTerminal' : 'D',
        'startTime'     : '1425',
        'endDate'       : '',
        'endTerminal'   : '1',
        'endTime'       : '1720',
        'eTkAvail'      : true,
        'airCompany'    : 'SU',
        'flightNumber'  : '102',
        'fic'           : 'NVULA',
        'flightTime'    : '0955',
        'journeyTime'   : '0955',
        'fromGeo'       : {
          'lat'     : 55.7522,
          'lon'     : 37.6156,
          'country' : 'RU'
        },
        'toGeo'         : {
          'lat'     : 40.7143,
          'lon'     : -74.006,
          'country' : 'US'
        }
      }], [{
        'from'          : 'JFK',
        'to'            : 'SVO',
        'continued'     : false,
        'startDate'     : '20171016',
        'startTerminal' : '1',
        'startTime'     : '0100',
        'endDate'       : '',
        'endTerminal'   : 'D',
        'endTime'       : '1720',
        'eTkAvail'      : true,
        'airCompany'    : 'SU',
        'flightNumber'  : '123',
        'fic'           : 'NVULA',
        'flightTime'    : '0920',
        'journeyTime'   : '0920',
        'fromGeo'       : {
          'lat'     : 40.7143,
          'lon'     : -74.006,
          'country' : 'US'
        },
        'toGeo'         : {
          'lat'     : 55.7522,
          'lon'     : 37.6156,
          'country' : 'RU'
        }
      }]],
      'returnDate'  : '20171016'
    },
    {
      'currency'    : 'RUB',
      'price'       : 25459,
      'dateCreated' : 1490216301003,
      'is2OW4RT'    : false,
      'fareType'    : 'direct',
      'departDate'  : '20171010',
      'from'        : 'MOW',
      'to'          : 'LAX',
      'to_country'  : 'US',
      'directions'  : [[{
        'from'          : 'SVO',
        'to'            : 'LAX',
        'continued'     : false,
        'startDate'     : '20171010',
        'startTerminal' : 'D',
        'startTime'     : '1155',
        'endDate'       : '',
        'endTerminal'   : 'B',
        'endTime'       : '1405',
        'eTkAvail'      : true,
        'airCompany'    : 'SU',
        'flightNumber'  : '106',
        'fic'           : 'NVULA',
        'flightTime'    : '1210',
        'journeyTime'   : '1210',
        'fromGeo'       : {
          'lat'     : 55.7522,
          'lon'     : 37.6156,
          'country' : 'RU'
        },
        'toGeo'         : {
          'lat'     : 34.0522,
          'lon'     : -118.244,
          'country' : 'US'
        }
      }], [{
        'from'          : 'LAX',
        'to'            : 'SVO',
        'continued'     : false,
        'startDate'     : '20171020',
        'startTerminal' : 'B',
        'startTime'     : '1605',
        'endDate'       : '',
        'endTerminal'   : 'D',
        'endTime'       : '1345',
        'eTkAvail'      : true,
        'airCompany'    : 'SU',
        'flightNumber'  : '107',
        'fic'           : 'NVULA',
        'flightTime'    : '1140',
        'journeyTime'   : '1140',
        'dayChg'        : 1,
        'fromGeo'       : {
          'lat'     : 34.0522,
          'lon'     : -118.244,
          'country' : 'US'
        },
        'toGeo'         : {
          'lat'     : 55.7522,
          'lon'     : 37.6156,
          'country' : 'RU'
        }
      }]],
      'returnDate'  : '20171020'
    },
    {
      'currency'    : 'RUB',
      'price'       : 33399,
      'dateCreated' : 1490211833984,
      'is2OW4RT'    : false,
      'fareType'    : 'direct',
      'departDate'  : '20171002',
      'from'        : 'MOW',
      'to'          : 'WAS',
      'to_country'  : 'US',
      'directions'  : [[{
        'from'          : 'SVO',
        'to'            : 'IAD',
        'continued'     : false,
        'startDate'     : '20171002',
        'startTerminal' : 'D',
        'startTime'     : '0925',
        'endDate'       : '',
        'endTerminal'   : '',
        'endTime'       : '1250',
        'eTkAvail'      : true,
        'airCompany'    : 'SU',
        'flightNumber'  : '104',
        'fic'           : 'NVUA',
        'flightTime'    : '1025',
        'journeyTime'   : '1025',
        'fromGeo'       : {
          'lat'     : 55.7522,
          'lon'     : 37.6156,
          'country' : 'RU'
        },
        'toGeo'         : {
          'lat'     : 38.8929,
          'lon'     : -77.0057,
          'country' : 'US'
        }
      }], [{
        'from'          : 'IAD',
        'to'            : 'SVO',
        'continued'     : false,
        'startDate'     : '20171016',
        'startTerminal' : '',
        'startTime'     : '1445',
        'endDate'       : '',
        'endTerminal'   : 'D',
        'endTime'       : '0720',
        'eTkAvail'      : true,
        'airCompany'    : 'SU',
        'flightNumber'  : '105',
        'fic'           : 'NVUA',
        'flightTime'    : '0935',
        'journeyTime'   : '0935',
        'dayChg'        : 1,
        'fromGeo'       : {
          'lat'     : 38.8929,
          'lon'     : -77.0057,
          'country' : 'US'
        },
        'toGeo'         : {
          'lat'     : 55.7522,
          'lon'     : 37.6156,
          'country' : 'RU'
        }
      }]],
      'returnDate'  : '20171016'
    }
  ];

export const FLIGHTS = {
  'status' : 'success',
  'data'   : {}
};

//
// import Format, {zeroNumber} from '../mixins/Format';
// import AIRLINES from '../libs/ref/airlines';
// import CITIES from '../libs/ref/cities';
//
// let airline = [], cities = [];
//
// function generateAirlines() {
//   airline = Object.keys(AIRLINES);
// }
//
// function generateCities() {
//   cities = Object.keys(CITIES);
// }
//
// function initTemps() {
//   generateAirlines();
//   generateCities();
// }
// /**
//  *
//  * @param {Array.<string>} not
//  * @param {number} [index]
//  * @return {string}
//  */
// function getIATA(not = [], index = (Math.random() * cities.length) | 0) {
//   index = index >= cities.length ? 0 : index;
//   const city = cities[index];
//   if (not.indexOf(city) > -1)
//     return getIATA(not, index + 1);
//   return city;
// }
//
// function generateFare(from, to, startDate, endDate, continued) {
//   const time = (endDate.getTime() - startDate.getTime()) / 1000 / 60;
//
//   let minutes = time % 60
//     , hours = (time - minutes) / 60;
//   return {
//     'from'      : from,
//     'to'        : to,
//     'continued' : continued,
//
//     'startDate' : Format.query.bestDeals.dateToString(startDate).replace(/-/g, ''),
//     'startTime' : Format.query.bestDeals.dateToTimeString(startDate).replace(':', ''),
//     'endDate'   : Format.query.bestDeals.dateToString(endDate).replace(/-/g, ''),
//     'endTime'   : Format.query.bestDeals.dateToTimeString(endDate).replace(':', ''),
//
//     'airCompany' : airline[((Math.random() * airline.length) | 0)],
//     'flightTime' : `${zeroNumber(hours)}${zeroNumber(minutes)}`,
//   }
// }
// initTemps();
// console.log(generateFare('MOW', 'NYC', new Date(2017, 4, 10, 10, 0), new Date(2017, 4, 11, 7, 25), false));
//
// function generateDirection(origin, destination, date) {
//   const transfers = [0, 0, 1, 2][((Math.random() * 4) | 0)];
//   const duration = (60) + Math.random() * (22 * 60);
//
//
//
//   switch (transfers) {
//     case 0:
//
//       break;
//     case 1:
//       break;
//     case 2:
//       break;
//   }
//
//   return [];
// }
//
// console.log(generateDirection());
//
// function generateDeal() {
//   let deal = {
//     'currency'    : 'RUB',
//     'price'       : 1228,
//     'dateCreated' : 1490188502642,
//     'is2OW4RT'    : false,
//     'fareType'    : 'direct',
//     'departDate'  : '20170810',
//     'from'        : 'MOW',
//     'to'          : 'KVX',
//     'to_country'  : 'RU',
//     'directions'  : [
//       [
//         {
//           'from'          : 'DME',
//           'to'            : 'KVX',
//           'continued'     : false,
//           'startDate'     : '20170810',
//           'startDateTime' : '2017-08-10T05:35:00.000Z',
//           'startTime'     : '0835',
//           'endDate'       : '20170810',
//           'endDateTime'   : '2017-08-10T07:00:00.000Z',
//           'endTime'       : '1000',
//           'airCompany'    : '6W',
//           'flightTime'    : '0125',
//           'journeyTime'   : '0125'
//         }
//       ]
//     ],
//     'returnDate'  : '20170810'
//   };
// }
//
// export function generateDeals() {
//   const days = 365;
// }
//