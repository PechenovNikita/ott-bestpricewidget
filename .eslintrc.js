module.exports = {
  parser        : "babel-eslint",
  env           : {
    browser : true,
    es6     : true
  },
  "extends"     : "eslint:recommended",
  parserOptions : {
    sourceType   : "module",
    ecmaFeatures : {
      impliedStrict                : true,
      spread                       : true,
      experimentalObjectRestSpread : true
    }
  },
  rules         : {
    indent            : [
      "error",
      2, {
        "SwitchCase" : 1
      }
    ],
    "linebreak-style" : [
      "error",
      "unix"
    ],
    quotes            : [
      "error",
      "single"
    ],
    semi              : [
      "error",
      "always"
    ]
  }
};