var oneDay = 1000 * 60 * 60 * 24;
var viewLanguage = 'en-gb';
var color;
var query = {
  origin            : "MOW",
  roundtrip_flights : true,
  noPricing         : true,
  update            : true
};
var map;
var map_lat_shift = 2;
var map_lng_shift = -5;

// add new
var block = {
  width  : 142,
  height : 80
}

function updateTextValue(name, fn) {
  $('#' + name)[0].onchange = function () {
    if (!this.value) {
      delete query[name];
    } else {
      query[name] = this.value;
    }
    query.update = true;
    if (fn) {
      fn.apply(query);
    }
  }
};

function updateRangeValue(name, fn) {
  $('#' + name)[0].onchange = function () {
    if (!this.value) {
      delete query[name + '_from'];
      delete query[name + '_to'];
      query.update = true;
      return;
    }

    var parts = this.value.split('-');
    query[name + '_from'] = parts[0];
    query[name + '_to'] = parts[parts.length - 1];
    query.update = true;
  }
};

function updateCheckBoxValue(name, sendFalse) {
  $('#' + name)[0].onchange = function () {
    if (!this.checked) {
      if (sendFalse) {
        query[name] = false;
      } else {
        delete query[name];
      }
    } else {
      query[name] = true;
    }
    query.update = true;
  }
};

var updateInProgress;
function updateData() {
  if (!query.update) {
    return;
  }

  if (updateInProgress) {
    return;
  }
  updateInProgress = true;
  delete query.update;

  loadDealsData(query, function (err, data) {
    // console.log(data);
    rebuildDealsData(data);

    let height = data && Math.ceil(data.length / 7) * block.height;
    if (!height) {
      height = 0;
    }

    $("#deals svg").css('height', height + 'px');

    color = d3.scaleLinear()
      .domain([dealDataInfo.min, dealDataInfo.min + (dealDataInfo.max - dealDataInfo.min) / 2, dealDataInfo.max])
      .range(["green", "orange", "red"]);

    overlayDrawer();
    dealsDrawer();
    updateInProgress = false;
  });
}

window.onload = function () {
  $('.helpsign').on('mouseover', function () {
    $('.help').css("visibility", "visible");
  }).on('mouseout', function () {
    $('.help').css("visibility", "hidden");
  })

  $('#origin')[0].value = "MOW";
  updateTextValue('origin', function () {
    getCityLatLng(tw.refData[viewLanguage].Cities[query.origin.toUpperCase()], function (err, citypos) {
      console.log(citypos);
      map.set("center", new google.maps.LatLng(citypos.lat + map_lat_shift, citypos.lng + map_lng_shift));
    });
  });

  // empty by deafult
  $('#departure')[0].onclick = createDatePicker('departure');
  $('#return')[0].onclick = createDatePicker('return');

  updateTextValue('destinations');
  updateTextValue('destination_countries');
  updateTextValue('locale', function () {
    this.add_locale_top = this.locale ? true : undefined;
  });

  updateRangeValue('stay');

  updateTextValue('departure_days_of_week');
  updateTextValue('return_days_of_week');
  updateTextValue('source', function () {
    if (this.source) {
      if (this.source == '12trip') {
        delete this.source;
      }
      delete this.noPricing;
    } else {
      this.noPricing = true;
    }
  });

  updateCheckBoxValue('direct_flights');
  updateCheckBoxValue('roundtrip_flights', true);
  updateCheckBoxValue('group_by_date');
  updateCheckBoxValue('group_by_country');
  updateTextValue('timelimit', function () {
    if (isFinite(this.timelimit)) {
      this.timelimit = this.timelimit * 3600;
    }
  });
  updateTextValue('deals_limit');

  setInterval(updateData, 200);
  dealsDrawerInit();

  getCityLatLng(tw.refData[viewLanguage].Cities[query.origin.toUpperCase()], function (err, citypos) {
    initMap(citypos);
  });
};

function createDatePicker(name) {
  return function () {
    this.onclick = undefined;

    var departureFrom = new Date();
    departureFrom.setDate(departureFrom.getDate() + 13);
    var departureTo = new Date();
    departureTo.setDate(departureTo.getDate() + 29);

    $('#' + name).daterangepicker({
      startDate       : departureFrom,
      endDate         : departureTo,
      autoUpdateInput : true,
      locale          : {
        format      : 'DD/MM/YYYY',
        cancelLabel : 'Clear'
      }
    }, function (start, end, label) {
      query[name + '_date_from'] = start.format('YYYY-MM-DD');
      query[name + '_date_to'] = end.format('YYYY-MM-DD');
      query.update = true;
      console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });

    $('#' + name).on('cancel.daterangepicker', function (ev, picker) {
      $(this).val('');
      delete query[name + '_date_from'];
      delete query[name + '_date_to'];
      query.update = true;
    });

    this.click();
  }
}

function loadDealsData(options, callback) {
  console.log('loadDealsData', options);
  let script = document.createElement('script');

  let funcName = 'func_' + Date.now();

  window[funcName] = function (response) {
    console.log(response);
    callback(undefined, response);
  };
  // let xhttp = new XMLHttpRequest();
  //
  // xhttp.onreadystatechange = function() {
  // 	if (xhttp.readyState == 4 && xhttp.status == 200) {
  // 		callback(undefined, xhttp.response);
  // 	}
  // };

  // options.noPricing = true;

  let query = '?';
  for (let key in options) {
    query += ['&' + key, options[key]].join('=');
  }

  query += '&callback=' + funcName;

  script.src = "http://onetwotrip.com/_api/deals_v4/directApiTop" + query;

  document.body.appendChild(script);
  // xhttp.open("GET", "http://onetwotrip.com/_api/deals_v4/directApiTop" + query, true);
  // xhttp.responseType = "json";
  // xhttp.send();
}

function getDealFromGeo(directions) {
  return directions[0][0].fromGeo;
}

function getDealToGeo(directions) {
  var directionTrips = directions[0];
  return directionTrips[directionTrips.length - 1].toGeo;
}

var dealData = [];
var dealDataInfo = {
  min : +Infinity,
  max : -Infinity
};

function rebuildDealsData(data) {
  dealData = [];
  dealDataInfo.min = +Infinity;
  dealDataInfo.max = -Infinity;
  // console.log(data);
  for (var i in data) {
    var d = data[i];
    if (!d || !d.directions) {
      continue;
    }

    if (d.price < dealDataInfo.min) {
      dealDataInfo.min = d.price;
    }
    if (d.price > dealDataInfo.max) {
      dealDataInfo.max = d.price;
    }

    var geo = getDealToGeo(d.directions);

    if (!geo)
      return;

    var deal = {
      destination_lon : geo.lon,
      destination_lat : geo.lat,
      destination     : d.to,
      origin          : d.from,
      price           : Math.round(d.price),
      trips           : []
    };

    for (var ida in d.directions) {
      for (var idb in d.directions[ida]) {
        var trip = d.directions[ida][idb];
        deal.trips.push(trip);
      }
    }

    dealData.push(deal);
  }
}

var dealsPane;
function dealsDrawerInit() {
  dealsPane = d3.select('#deals')
    .append("svg")
}

function dealsDrawer() {
  if (!dealsPane) {
    return;
  }

  var points = dealsPane
    .selectAll("a")
    .data(dealData)

  points.exit().remove();

  // update existing pointss
  points.select(".db_cityname")
    .each(modifyDealsBlockCity)

  points.select(".db_price")
    .each(modifyDealsBlockPrice)

  points.select(".db_acsname")
    .each(modifyDealsBlockAcs)

  points.select(".db_dates")
    .each(modifyDealsBlockDates)

  points.select(".db_circle")
    .each(modifyDealsColor)

  points
    .each(modifyLinks)

  // add new
  var newdata = points.enter()
    .append('a')
    .attr('class', 'block')
    .attr('xlink:show', 'new')
    .each(modifyLinks)
    .append('g')
    .attr("transform", function (d, i) {
      var s = Math.floor(1000 / block.width);
      var y = Math.floor(i / s);
      var x = (i % s);
      return "translate(" + [(x * block.width), (y * block.height)].join() + ")"
    })

  newdata.append('rect')
    .attr("width", block.width)
    .attr("height", block.height)
    .attr("class", "deals")

  // Add a label.
  newdata.append("circle")
    .attr("cx", "127px")
    .attr("cy", "65px")
    .attr("r", 10)
    .attr("class", "db_circle")
    .each(modifyDealsColor)

  newdata.append("text")
    .attr("x", "2px")
    .attr("y", "15px")
    .attr("class", "dealblocktext db_cityname")
    .style("font-size", "18px")
    .each(modifyDealsBlockCity)

  newdata.append("text")
    .attr("x", "2px")
    .attr("y", "35px")
    .attr("class", "dealblocktext db_price")
    .each(modifyDealsBlockPrice)

  newdata.append("text")
    .attr("x", "2px")
    .attr("y", "55px")
    .attr("class", " dealblocktext db_acsname")
    .each(modifyDealsBlockAcs)

  newdata.append("text")
    .attr("x", "2px")
    .attr("y", "75px")
    .attr("class", " dealblocktext db_dates")
    .each(modifyDealsBlockDates)
}

function modifyLinks() {
  d3.select(this)
    .attr('href', function (d) {
      if (query.noPricing) {
        return;
      }
      // https://www.onetwotrip.com/ru/aviabilety/moscow-madrid_MOW-MAD/#1108MOWMAD
      var date = d.trips[0].startDate.slice(6, 8) + d.trips[0].startDate.slice(4, 6);
      var ret = '';
      if (query.roundtrip_flights) {
        ret = d.trips[d.trips.length - 1].startDate.slice(6, 8) + d.trips[d.trips.length - 1].startDate.slice(4, 6);
      }
      var source = query.source;
      if (!query.noPricing) {
        if (!source) { source = "_rm"; }
        source = '&scp=4,' + source + ',deals';
      }
      return 'https://www.onetwotrip.com/ru/aviabilety/' + d.origin + '-' + d.destination + '/?s' + source + '#' + date + d.origin + d.destination + ret;

    })
}
function modifyDealsBlockCity() {
  d3.select(this)
    .text(function (d) {
      return getCity(d.destination);
    });
}

function modifyDealsBlockPrice() {
  d3.select(this)
    .text(function (d) {
      return d.price + " RUB";
    });
}

function modifyDealsBlockAcs() {
  d3.select(this)
    .text(function (d) {
      var acs = d.trips.map(function (t, i) { return t.airCompany + (t.continued ? '-' : (i == d.trips.length - 1 ? '' : ':') ); });
      return acs.join('');
    })
}

function modifyDealsBlockDates() {
  d3.select(this)
    .text(function (d) {
      var dep = d.trips[0].startDate;
      dep = [dep.slice(0, 4), dep.slice(4, 6), dep.slice(6, 8)].join('-');
      dep = new Date(dep);
      dep = dep.toGMTString().split(' ').slice(1, 3).join(' ')
      var ret = '';
      if (query.roundtrip_flights) {
        ret = d.trips[d.trips.length - 1].startDate;
        ret = new Date([ret.slice(0, 4), ret.slice(4, 6), ret.slice(6, 8)].join('-'))
        ret = ret.toGMTString().split(' ').slice(1, 3).join(' ')
        return dep + '-' + ret;
      }
      return dep;
    })
}

function modifyDealsColor() {
  d3.select(this)
    .style("fill", function (d) {
      console.log(d.price, color(d.price));
      return color(d.price);
    })
}

function getCityLatLng(cityname, callback) {
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode({'address' : cityname}, function (results, status) {
    // console.log(cityname, results)
    if (status == google.maps.GeocoderStatus.OK) {
      // alert("location : " + results[0].geometry.location.lat() + " " +results[0].geometry.location.lng());
      callback(undefined, {
        lat : results[0].geometry.location.lat(),
        lng : results[0].geometry.location.lng()
      });
    } else {
      callback("Something got wrong " + status);
    }
  });
}

function initMap(center) {
  // Create the Google Map…
  map = new google.maps.Map(d3.select("#map").node(), {
    zoom      : 4,
    center    : new google.maps.LatLng(center.lat + map_lat_shift, center.lng + map_lng_shift),
    mapTypeId : google.maps.MapTypeId.ROADMAP
  });

  var overlay = new google.maps.OverlayView();

  // Add the container when the overlay is added to the map.
  overlay.onAdd = overlayInit;
  overlay.draw = overlayDrawer;

  // Bind our overlay to the map…
  overlay.setMap(map);
}

var layer;
function overlayInit() {
  layer = d3.select(getLastPane(this).overlayLayer)
    .append("div")
    .attr("class", "stations")
    .append("svg")
}

function getLastPane(context) {
  if (context.getPanes) {
    this.Pane = context.getPanes();
  }
  return this.Pane;
}
function getLastProjection(context) {
  if (context.getProjection) {
    this.Projection = context.getProjection();
  }
  return this.Projection;
}

function getCity(city) {
  return tw.refData[viewLanguage].Cities[city];
  // return city;
}

function overlayDrawer() {
  if (!layer) { return; }
  // console.log(this, 'dealData.length', dealData.length, dealDataInfo.min, dealDataInfo.max);
  if (this.getPanes) {
    var a = document.getElementsByClassName('stations');
    for (var k in a) {
      var b = a[k].parentNode;
      if (b) {
        b.removeChild(a[k]);
      }
    }

    layer = d3.select(this.getPanes().overlayLayer)
      .append("div")
      .attr("class", "stations")
      .append("svg")
  }

  var projection = getLastProjection(this),
    padding = 10;

  var points = layer
    .selectAll("g")
    .data(dealData)

  points.exit().remove();

  // update existing pointss
  points.each(transform)
  points.select("text")
    .text(function (d) {
      return getCity(d.destination);
    })
  points.select("circle")
    .style("fill", function (d) {
      return color(d.price);
    })

  var newdata = points.enter()
    .append('g')
    .each(transform)

  // Add a circle.
  newdata.append("circle")
    .attr("r", 9)
    .attr("class", "points")
    .style("fill", function (d) {
      return color(d.price);
    })

  // Add a label.
  newdata.append("text")
    .attr("x", 5)
    .attr("y", 0)
    .attr("class", "cityname")
    .text(function (d) {
      return getCity(d.destination);
    });

  function get_position(d) {
    d = new google.maps.LatLng(d.destination_lat, d.destination_lon);
    d = projection.fromLatLngToDivPixel(d);
    return d;
  }

  function transform(d) {
    d = get_position(d);

    return d3.select(this)
      .attr("transform", "translate(" + [(d.x), (d.y)].join() + ")")
  }
};

/*
 RAW INFLATING IMPLEMENTAITON
 function loadDealsData(callback){
 var xhttp = new XMLHttpRequest();

 xhttp.onreadystatechange = function() {
 if (xhttp.readyState == 4 && xhttp.status == 200) {
 var byteArray = new Uint8Array(xhttp.response);

 parseServerResponse(byteArray, function (err, data) {
 var results = [];
 for(var i in data){
 var text = BaToString(pako.inflate(data[i]));
 results.push(JSON.parse(text));
 }

 callback(undefined, results);
 });
 }
 };

 xhttp.open("GET", "//139.162.233.244:8090/deals/top?origin=MOW", true);
 xhttp.responseType = "arraybuffer";
 xhttp.send();
 }

 function parseServerResponse(response_body, callback){
 var pos = 0;
 var info_length;

 // response format
 // <-  size_info  -><-     data blocks       ->
 // ↓ size_info block length
 // 11;121;121;45;21;{....},{....},{....},{....}
 //    ↑   ↑   ↑  ↑  each data block length

 while(pos < 100){
 if(response_body[pos++] === 0x3b) {
 info_length = Number(BaToString(response_body.slice(0, pos - 1)));
 break;
 }
 }

 if(!isFinite(info_length)){
 callback('ERROR_DEALS_RESPONSE_FORMAT');
 return;
 }

 var sizes = BaToString(response_body.slice(0, info_length - 1)).split(';');
 var data = [];
 var pointer = Number(sizes[0]);

 if(!isFinite(pointer)){
 callback('ERROR_DEALS_RESPONSE_SIZE_FORMAT');
 return;
 }

 for(var i = 1; i < sizes.length; i++){
 var size = Number(sizes[i]);
 if(!isFinite(size)){
 callback('ERROR_DEALS_RESPONSE_SIZE_FORMAT2');
 return;
 }
 var textdata = response_body.slice(pointer, pointer + size);
 pointer += size;

 data.push(textdata);
 }

 callback(undefined, data);
 }

 function BaToString(a){
 return new TextDecoder().decode(a);
 }*/