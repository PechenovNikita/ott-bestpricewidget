'use strict';
import {linkCountry, linkCity} from './../../libs/OneTwoTrip';
import {byIATA as getCityByIATA} from '../../libs/reference/city';

/**
 *
 * @param {string} iata
 * @return {string}
 */
function nodeLinkCityByIata(iata) {
  let city = getCityByIATA(iata);
  if (city)
    return `<a href="javascript:void(0)" data-iata="${iata}">${city.city.ru}</a>`;
  return '';
}

/**
 *
 * @param {Store} store
 * @param {Query} query
 * @param {Array.<Response>} results
 * @return {string}
 */
export function countryResults(store, query, results) {
  let text = '';
  if (query.$isComplete) {
    let cities = store.getCities(query).splice(0, 3).map(nodeLinkCityByIata).join(', ');

    if (results.length == 0) {
      text = emptyResults();
      if (cities.length > 0) {
        text += `<div class="ottbp-info">Попробуйте уточнить город прибытия или добавить один из самых популярных: ${cities}.</div>`;
      } else {
        text += '<div class="ottbp-info">Уточните город прибытия или выберите другие даты путешествия.</div>';
      }
    } else {
      if (cities.length > 0) {
        text = `<div class="ottbp-info">Поиск по популярным городам: ${cities}.</div>`;
      }
    }
    text += `<div class="ottbp-more"><a href="${linkCountry(query)}" target="_blank">Искать подробнее</a></div>`;
  } else {
    if (results.length == 0) {
      text = emptyResults();
      text += needDates(query.$isRoundTrip, query.$departure);
    }
  }

  return text;
}

/**
 *
 * @param {Query} query
 * @param {Array.<Response>} results
 * @return {string}
 */
export function cityResults(query, results) {
  let text = '';
  if (query.$isComplete) {
    if (results.length == 0) {

      text = emptyResults();
      text += '<div class="ottbp-info">Выберите другие даты путешествия</div>';

    } else {

      text = `<div class="ottbp-more"><a href="${linkCity(query)}" target="_blank">Все перелёты</a></div>`;
    }
  } else {
    if (results.length == 0) {
      text = emptyResults();
      text += needDates(query.$isRoundTrip, query.$departure);
    }
  }

  return text;
}

/**
 *
 * @return {string}
 */
export function emptyResults() {
  return '<div class="ottbp-error">Быстро найти хороший вариант не удалось.</div>';
}

/**
 *
 * @param {boolean} isRoundTrip
 * @param {Date|null} [departureDate = null]
 * @return {string}
 */
export function needDates(isRoundTrip, departureDate = null) {
  if (isRoundTrip) {
    if (departureDate)
      return '<div class="ottbp-info">Выберите дату окончания путешествия.</div>';
    else
      return '<div class="ottbp-info">Выберите даты путешествия.</div>';
  }
  return '<div class="ottbp-info">Выберите дату отправления.</div>';
}