'use strict';

import Svg from '../Svg';
import './sprites';

export default class Airplane extends Svg {
  constructor() {
    super();

    this._attr = {
      width  : 14,
      height : 12
    };

    this._options = {
      fill : '#616161'
    };

    this._mirror = false;

    this.update();
  }

  /**
   *
   * @return {Airplane}
   */
  update() {
    this._inner = `<use xlink:href="#ottbp-s-air" fill="${this._options.fill}" transform="scale(${this._mirror ? '-1' : '1'},1) translate(${this._mirror ? -1 * this._attr.width : '0'},0)" class="ottbp-sprite-svg__air"></use>`;
    return this.render();
  }

  /**
   *
   * @param {boolean|null} [bool=false]
   * @return {Airplane}
   */
  mirror(bool = false) {
    this._mirror = !!bool;
    return this.update();
  }

  /**
   *
   * @param {number} [width]
   * @param {number} [height]
   * @return {Airplane}
   */
  resize(width = this._attr.width, height = this._attr.height) {
    return this.attr({
      width  : width,
      height : height
    });
  }

  /**
   *
   * @param fill
   * @return {Airplane}
   */
  fill(fill) {
    this._options.fill = fill;
    return this.update();
  }

}

