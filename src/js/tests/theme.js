'use strict';

import {deepExtend} from './../mixins/Deep';

import {bindReady} from './../mixins/Element';

function arrProps(props, value) {
  if (props.length > 0) {
    let temp = {};
    temp[props.shift()] = arrProps(props, value);
    return temp;
  } else {
    return value;
  }
}

bindReady(function () {
  let inputs = Array.prototype.slice.call(document.querySelectorAll('.test-theme-color'));

  const getData = function () {
    let data = inputs.reduce(function (obj, el) {
      if (el.name.indexOf('-') < 0)
        obj[el.name] = "#" + el.value;
      else {
        let name = el.name.split('-');
        obj = deepExtend({}, obj, arrProps(name, "#" + el.value));
      }
      return obj;
    }, {});
    return data;
  };

  const widthFixed = document.querySelector('.test-theme-fixed')
    , widthInput = document.querySelector('.test-theme-width');

  widthFixed.addEventListener('change', function () {
    if (this.checked) {
      widthInput.removeAttribute('disabled');
      window.NEWWidget.width(widthInput.value);
    } else {
      widthInput.setAttribute('disabled', 'disabled');
      window.NEWWidget.width(null);
    }
  });

  widthInput.addEventListener('input', function () {
    window.NEWWidget.width(widthInput.value);
  });

  inputs.forEach(function (el, index) {
    el.addEventListener('change', function (event) {
      window.NEWWidget.colors(getData());
    });
  });
});
