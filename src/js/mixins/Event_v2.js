'use strict';

import addEvent from './event/addEvent';
import delegate from './event/delegate';

/**
 *
 * @param {string} text
 * @param {Array.<string>} strings
 * @return {boolean}
 */
function hasStrings(text, strings) {
  return strings.some((str) => text.indexOf(str) > -1);
}

export default class EventStore {
  constructor() {
    /**
     *
     * @type {Object.<string, {runs: number, callbacks: Array<Function[5]>}>}
     * @private
     */
    this._store = {};
    this._DOMEvents = [];

    this.__initDOMEvents();
  }

  get $store() {
    return this._store;
  }

  /**
   *
   * @param {string|string[]} names
   * @param {function} callback
   * @param {number} [priority=0]
   * @param {boolean} [startIfRan=false]
   * @return {*}
   */
  on(names, callback, priority = 0, startIfRan = false) {
    if (Array.isArray(names))
      names.forEach(function (name) {
        this.on(name, callback, priority, startIfRan);
      }, this);
    else if (typeof names === 'string') {
      this.__on(names, callback, priority, startIfRan);
    }
  }

  /**
   *
   * @param {string} name
   * @param {function} callback
   * @param {number} priority
   * @param {boolean} [startIfRan]
   * @private
   */
  __on(name, callback, priority, startIfRan) {
    priority = (priority > 2 ? 2 : (priority < -2 ? -2 : priority));
    let index = 2 + priority;

    this.__createEventStore(name, false, hasStrings(name, ['Document', 'Window', 'Element']));

    this.$store[name].callbacks[index].push(callback);

    if (startIfRan && this.$store[name].run > 0) {
      callback({
        type   : name,
        detail : (this.$store[name].details[this.$store[name].details.length - 1])
      });
    }
  }

  /**
   *
   * @param {string|string[]} names
   * @param {*} [detail]
   */
  dispatch(names, detail) {
    // console.warn("START ", names);
    if (Array.isArray(names))
      names.forEach(function (name) {
        this.dispatch(name, detail);
      }, this);
    else if (typeof names === 'string') {
      this.__dispatch(names, detail);
    }
  }

  /**
   *
   * @param {string} name
   * @param {*} detail
   * @private
   */
  __dispatch(name, detail) {
    const currentStore = this.$store[name];
    if (currentStore) {
      currentStore.runs++;
      currentStore.details.push(detail);
      currentStore.callbacks.forEach(function (/*Function[]*/callbacks) {
        return callbacks.forEach(function (callback) {
          callback({
            type   : name,
            detail : detail
          });
        });
      });
    } else {
      this.__createEventStore(name, true, hasStrings(name, ['Document', 'Window', 'Element']));
    }
  }

  /**
   *
   * @param {string} name
   * @param {boolean} [started=false]
   * @param {boolean} [dom=false]
   * @private
   */
  __createEventStore(name, started = false, dom = false) {
    if (typeof this.$store[name] === 'undefined')
      this.$store[name] = {
        runs      : (started ? 1 : 0),
        dom       : dom,
        details   : [],
        callbacks : [[], [], [], [], []]
      };
    return this;
  }

  __addDomEvents(element, type, func) {
    this._DOMEvents.push({
      type     : type,
      element  : element,
      callback : func
    });

    addEvent(element, type, func);
  }

  __initDOMEvents() {
    const self = this;

    this.__addDomEvents(window, 'resize', function (event) {
      self.dispatch('Window::resize', {
        width    : window.innerWidth,
        original : event
      });
    });

    this.__addDomEvents(document, 'click', function (event) {
      self.dispatch('Document::click', {
        original : event
      });
    });

    this.__addDomEvents(document, 'mousemove', function (event) {
      self.dispatch('Document::mousemove', {
        original : event
      });
    });

    this.__addDomEvents(document, 'keydown', function (event) {
      self.dispatch('Document::keydown', {
        keyCode  : event.keyCode,
        original : event
      });
    });
  }

  /**
   *
   * @param {string} eventMethod
   * @param {string|HTMLElement} elementSelector
   * @param {function(event:{detail:{original:Event}})} callback
   * @param {HTMLElement} [parent=document]
   * @param {function(event:{detail:{original:Event}})} [notCallback]
   */
  delegateDocument(eventMethod, elementSelector, callback, notCallback, parent = document.body) {
    this.on(`Document::${eventMethod}`, (event) => {
      let originatEvent = event.detail.original;

      delegate(originatEvent, elementSelector, parent, (target) => {
        callback({
          type   : event.type,
          detail : {
            target   : target,
            original : originatEvent
          }
        });
      }, (event) => {
        if (notCallback)
          notCallback({
            type   : event.type,
            detail : {
              original : originatEvent
            }
          });
      });
    });
  }
}