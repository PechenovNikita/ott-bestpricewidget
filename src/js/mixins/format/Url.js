'use strict';
/**
 *
 * @param {string} countryEnName
 */
export function lowCountry(countryEnName) {
  let str = countryEnName.toLowerCase();
  str = str.replace(/[^\s0-9a-zA-Z-]+/g, '').replace(/[\s_-]+/g, '-');

  return str;
}

export default {
  lowCountry : lowCountry
};
