'use strict';

import airportsEN from '../ref/airports';
import airportsRU from '../refRu/airports';

import {byIATA as getCityByIATA} from './city';

/**
 *
 * @param {string} iata
 * @return {PlaceData|null}
 */
export function byIATA(iata) {
  return __getCity(iata);
}

/**
 *
 * @param {string} iata
 * @return {PlaceData|null}
 * @private
 */
function __getCity(iata) {
  if (airportsEN.hasOwnProperty(iata)) {

    let airportData = airportsEN[iata].split('|');
    let airportRuName = __getAirportRU(iata);

    if (!airportRuName)
      return null;
    /**
     * @type {PlaceData|null}
     */
    let city = (airportData[1] && airportData[1].length > 0 ? getCityByIATA(airportData[1]) : null);

    let data = {
      iata    : iata,
      type    : "airport",
      airport : {
        iata : iata,
        en   : airportData[0],
        ru   : airportRuName
      }
    };

    if (city) {
      data.city = city.city;
      data.country = city.country;
    }

    return data;
  }
  return null;
}
/**
 *
 * @param {string} iata
 * @return {null|string}
 * @private
 */
function __getAirportRU(iata) {
  if (airportsRU.hasOwnProperty(iata)) {
    return airportsRU[iata];
  }
  return null;
}
