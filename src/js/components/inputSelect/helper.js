'use strict';

import {docCreate} from './../../mixins/Element';
import classList from '../../mixins/polyfill/element/classList';

import EarthSVG from './../../mixins/svg/earth';
/**
 *
 * @type {Earth}
 */
const EARTH_ICON = (new EarthSVG()).resize(12, 12).fill('#9E9E9E');

let helpers = [];

document.addEventListener('mousemove', function (event) {
  let target = event.target;

  while (target && target != document.body) {
    if (classList(target).contains('ottbp-input-helper__list'))
      return true;
    target = target.parentNode;
  }

  onBlur();
});

function onBlur() {
  helpers.forEach(function (helper) {
    helper.onBlur();
  });
}

export default class InputHelper {
  /**
   *
   * @param {HTMLElement} container
   * @param {Object} [options = {}]
   * @param {PlaceData[]} [options.items]
   */
  constructor(container, options = {}) {
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._container = container;

    this._items = options.items || [];

    this._selected = false;

    helpers.push(this);

    this.initHTML();
  }

  initHTML() {

    this._wrapper = docCreate('div', ['ottbp-input-helper']);
    this._listNode = docCreate('ul', ['ottbp-input-helper__list']);

    this._wrapper.addEventListener('mousewheel', this.onScroll.bind(this));

    this.__initItemsHTML();

    this._wrapper.appendChild(this._listNode);

    this._shadow = docCreate('div', 'ottbp-control__helper__shadow');
    this._shadowId = Date.now();
    this._shadow.id = this._shadowId;

    this._container.appendChild(this._shadow);
    this._container.appendChild(this._wrapper);

    this._listNode.setAttribute('tabindex', '1');

    this._listNode.addEventListener('click', this.__onClick.bind(this));
    this._listNode.addEventListener('mousemove', this.__onHover.bind(this));

    return this;
  }

  __onClick(event) {
    let target = event.target;
    while (target && target != this._listNode) {
      if (classList(target).contains('ottbp-input-helper-link') && target.hasAttributes('data-index')) {

        event.stopPropagation();
        event.preventDefault();

        this.__preSelect(parseInt(target.getAttribute('data-index'), 10));
      }
      target = target.parentNode;
    }
  }

  __preSelect(index) {
    this._selected = true;
    this.onSelect(this._items[index]);
  }

  /**
   *
   * @callback onClickHelper
   * @param {PlaceData} item
   */

  /**
   *
   * @param {onClickHelper} callback
   * @return {InputHelper}
   */
  whenSelect(callback) {
    /**
     *
     * @type {onClickHelper}
     * @private
     */
    this._callbackSelect = callback;
    return this;
  }

  /**
   *
   * @param {PlaceData} item
   */
  onSelect(item) {
    if (this._callbackSelect)
      this._callbackSelect(item);
  }

  __onHover(event) {

    if (this._selected)
      return;

    this._hovered = true;
    let target = event.target;
    while (target != this._listNode) {
      if (classList(target).contains('ottbp-input-helper-link') && target.hasAttributes('data-index')) {
        this.__preHover(parseInt(target.getAttribute('data-index'), 10));
      }
      target = target.parentNode;
    }
  }

  /**
   *
   * @param {number} index
   * @return {InputHelper}
   * @private
   */
  __preHover(index) {
    if (this._lastHover && this._lastHover == index && !this._items[index])
      return this;
    this._lastHover = index;
    this.onHover(this._items[index]);
    this.__updateItemsStyles();
  }

  /**
   *
   * @callback onHoverHelper
   * @param {PlaceData} item
   */

  /**
   *
   * @param {onHoverHelper} callback
   */
  whenHover(callback) {
    /**
     *
     * @type {onHoverHelper}
     * @private
     */
    this._callbackHover = callback;
  }

  /**
   *
   * @param {PlaceData} item
   */
  onHover(item) {
    if (this._callbackHover)
      this._callbackHover(item);
  }

  onBlur() {
    if (!this._hovered)
      return;
    this._lastHover = -1;
    this._hovered = false;

    if (this._items && this._items.length > 0) {
      if (this._highlight < 0)
        this.onHover(this._items[0]);
      else
        this.onHover(this._items[this._highlight]);

      this.__updateItemsStyles();
    }
  }

  onScroll(event) {
    event.preventDefault();
    event.stopPropagation();

    let scrollTop = this._wrapper.scrollTop + event.deltaY;

    this._wrapper.scrollTop = scrollTop;

    this.__setScrollWrapper(scrollTop);

  }

  /**
   *
   * @param {number} scrollTop
   * @return {InputHelper}
   * @private
   */
  __setScrollWrapper(scrollTop) {
    this._wrapper.scrollTop = scrollTop;

    if (scrollTop > 0) {
      this.__setShadowStyle((scrollTop > 20) ? '1' : scrollTop / 20, (scrollTop > 20) ? '10px' : scrollTop / 20 * 10 + 'px');
    } else {
      this.__setShadowStyle(0, 0);
    }
    return this;
  }

  __setShadowStyle(opacity, height) {
    // listShadow(this._shadowId, opacity, height);
    // this._shadow.style.opacity = opacity;
    // this._shadow.style.height = height;

    this._shadow.setAttribute('style', `opacity:${opacity} !important; height:${height} !important`);
    return this;
  }

  __initItemsHTML() {
    this._itemsNode = [];
    this._fragment = document.createDocumentFragment();

    this._listNode.innerHTML = '';
    this._items.forEach(this.__createItemHTML, this);
    this._listNode.appendChild(this._fragment);

    return this;
  }

  /**
   *
   * @param {PlaceData} item
   * @param {number} index
   * @private
   */
  __createItemHTML(item, index) {
    let li = docCreate('li', 'ottbp-input-helper__list__item')
      , a = docCreate('a', 'ottbp-input-helper-link');

    if (index == this._highlight)
      classList(a).add('ottbp-highlight');

    a.tabIndex = '-1';
    a.setAttribute('data-index', index);

    let [icon, label, place] = ['', '', ''];

    switch (item.type) {
      case 'country':
        icon = `<span class="ottbp-helper-icon">${EARTH_ICON.toString()}</span>`;
        label = item.country.ru;
        break;
      case 'city':
        label = `${item.city.ru},`;
        place = item.country.ru;
        break;
    }

    a.innerHTML = `${icon}<span class="ottbp-helper-strong">${label}</span>&nbsp;<span class="ottbp-helper-gray">${place}</span><span class="ottbp-helper-iata">${item.iata}</span>`;

    li.appendChild(a);
    this._itemsNode.push(a);
    this._fragment.appendChild(li);
  }

  /**
   *
   * @return {InputHelper}
   * @private
   */
  __updateItemsStyles() {
    /**
     * @this {InputHelper}
     */
    this._itemsNode.forEach(function (item, index) {
      if (!this._hovered && index == this._highlight)
        classList(item).add('ottbp-highlight');
      else
        classList(item).remove('ottbp-highlight');
    }, this);

    return this;
  }

  highLight(delta) {
    let current = (this._highlight || this._highlight === 0 ? this._highlight : -1);
    // current = (current < 0) ? 0 : current;

    if (delta < 0) {
      this._highlight = (current <= 0) ? this._items.length - 1 : current - 1;
    } else if (delta > 0) {
      this._highlight = (current >= this._items.length - 1) ? 0 : current + 1;
    }

    if (this._highlight < 5)
      this.__setScrollWrapper(0);
    else
      this.__setScrollWrapper((this._highlight - 4) * 52);

    this.__updateItemsStyles().onHover(this._items[this._highlight]);
  }

  /**
   *
   * @param {Object} [options = {}]
   * @param {PlaceData[]} [options.items]
   */
  update(options = {}) {
    this._items = options.items || [];
    this._lastHover = -1;
    this._highlight = -1;

    this.__initItemsHTML().__setScrollWrapper(0);

    if (this._items && this._items.length > 0)
      this.onHover(this._items[0]);
  }

  get $currentItem() {
    let currentIndex = this._highlight;
    currentIndex = (currentIndex < 0) ? 0 : currentIndex;
    if (this._items && this._items.length > currentIndex) {
      return this._items[currentIndex];
    }
    return null;
  }
}