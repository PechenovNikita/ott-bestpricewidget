'use strict';
import {docCreate} from '../../mixins/Element';
import {declOfNum} from '../../mixins/Format';
import l8n from '../../store/l8n';

import {dateFromString, dateToShotString} from '../../mixins/Format';

import AirplaneSVG from '../../mixins/svg/airplane';

import {byIATA as getAirlineEN} from '../../libs/reference/airline';

import {
  split as timeSplit,
  toColon as timeToColon,
  toFormat as timeToFormat,
  toSeconds as timeToSeconds
} from '../../mixins/format/Time';

function duration(startDirection, endDirection) {
  let startDate = dateFromString((startDirection.endDate ? startDirection.endDate : endDirection.startDate))
    , startTime = timeSplit(startDirection.endTime);

  let endDate = dateFromString(endDirection.startDate)
    , endTime = timeSplit(endDirection.startTime);

  startDate.setHours(parseInt(startTime[0], 10), parseInt(startTime[1], 10));
  // startDate.setMinutes(parseInt(startTime[1], 10));

  endDate.setHours(parseInt(endTime[0], 10), parseInt(endTime[1], 10));
  // endDate.setMinutes(parseInt(endTime[1], 10));

  return (endDate.getTime() - startDate.getTime());
}
/**
 *
 * @param {Array} directions
 * @return {string}
 */
function parseDuration(directions) {

  if (directions.length == 1)
    return timeToFormat(directions[0].flightTime);

  let t = directions.reduce(function (time, direction, index) {
    if (index == 0) {
      return timeToSeconds(direction.flightTime);
    }

    return time
      + timeToSeconds(direction.flightTime)
      + duration(directions[index - 1], direction) / 1000;

  }, 0);

  let hour = Math.floor(t / 60 / 60)
    , min = Math.floor((t - (hour * 60 * 60)) / 60);

  if (hour)
    return `${hour} ${l8n.Direction.time.hour} ${min} ${l8n.Direction.time.min}`;
  return `${min} ${l8n.Direction.time.min}`;
}

export default class Direction {
  /**
   *
   * @return {Date}
   */
  get $startDate() {
    return this._options.startDate;
  }

  /**
   *
   * @return {string}
   */
  get $from() {
    return this._options.from;
  }

  /**
   *
   * @return {string}
   */
  get $to() {
    return this._options.to;
  }

  constructor(container, options = {}, data = {}) {
    this._container = container;
    this._options = options;

    this.__parseData(data).initHTML();
  }

  /**
   *
   * @param data
   * @return {Direction}
   * @private
   */
  __parseData(data) {

    this._options.accross = l8n.Direction.accross.none;
    if (data.direction.length > 1) {
      let num = data.direction.length - 1;
      this._options.accross = `${num}&nbsp;${declOfNum(num, l8n.Direction.accross.one_two_five)}`;
    }

    this._options.aircompany_id = data.direction[0].airCompany;
    this._options.aircompany_logo = `https://www.gstatic.com/flights/airline_logos/32px/${data.direction[0].airCompany}.png`;
    this._options.aircompany = getAirlineEN(data.direction[0].airCompany);
    this._options.startDate = dateFromString(data.direction[0].startDate);

    this._duration = parseDuration(data.direction);

    this._options.from = data.direction[0].from;
    this._options.to = data.direction[data.direction.length - 1].to;

    this._options.startTime = timeToColon(data.direction[0].startTime);
    this._options.endTime = timeToColon(data.direction[data.direction.length - 1].endTime);

    return this;
  }

  /**
   *
   * @return {Direction}
   */
  initHTML() {

    this._node = docCreate('div', 'ottbp-direction');

    let innerTitle = '<table class="ottbp-direction__title">';

    let air = new AirplaneSVG();

    air.resize(18, 16).fill('#616161').mirror(this._options.type == 'to');

    innerTitle += '<tr>';
    innerTitle += '<td>';
    innerTitle += air.toString();
    innerTitle += '</td>';

    // innerTitle += `<span class="ottbp-direction__title__icon ottbp-icon-fly ottbp-icon-fly--${this._options.type || "from"}"></span>`;
    innerTitle += '<td class="ottbp-direction__title__inner">';
    innerTitle += `<span class="ottbp-direction__title__inner">${this._options.directionName},&nbsp;</span>`;
    innerTitle += `<span class="ottbp-direction__title__inner">${dateToShotString(this._options.startDate, true)}</span>`;
    innerTitle += '</td>';
    innerTitle += '</tr>';
    innerTitle += '</table>';

    let innerTime = '<div class="ottbp-direction__time">';
    innerTime += `<span class="ottbp-direction__time__inner">${this._options.startTime} — ${ this._options.endTime}</span></div>`;

    let innerTimeSpent = '<div class="ottbp-direction__timespent">';
    innerTimeSpent += `<span class="ottbp-direction__timespent__inner">${this._duration}, ${this._options.accross}</span></div>`;

    let innerCompany = '<div class="ottbp-direction__company">';
    innerCompany += `<img class="ottbp-direction__company__logo" src="${this._options.aircompany_logo}" alt="${this._options.aircompany}">`;
    innerCompany += `<span class="ottbp-direction__company__inner">${this._options.aircompany}</span>`;
    innerCompany += '</div>';

    let innerHTML = `<div class="ottbp-direction__main">${innerTitle}${innerTime}</div>`;
    innerHTML += `<div class="ottbp-direction__add">${innerTimeSpent}${innerCompany}</div>`;

    this._node.innerHTML = innerHTML;
    this._container.appendChild(this._node);

    return this;
  }
}