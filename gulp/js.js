const gulp = require('gulp')
  , webpack = require('gulp-webpack')
  , livereload = require('gulp-livereload');

const config = require('./_config');

function biuld_index(isWatch) {
  let webpackConfig = require('./webpack');
  webpackConfig.watch = isWatch;
  webpackConfig.debug = isWatch;


  return gulp.src('./src/js/app.js')
    .pipe(webpack(webpackConfig))
    .pipe(gulp.dest('./dest/js'))
    .pipe(livereload());
}

function build_testing(isWatch){
  let testingConfig = require('./testing.conf');
  testingConfig.watch = isWatch;
  testingConfig.debug = isWatch;


  return gulp.src('./src/js/testing.js')
    .pipe(webpack(testingConfig))
    .pipe(gulp.dest('./dest/js'))
    .pipe(livereload());
}

function build_product(isWatch){
  let productConfig = require('./webpack.widget');
  productConfig.watch = isWatch;
  productConfig.debug = isWatch;


  return gulp.src('./src/js/widget.js')
    .pipe(webpack(productConfig))
    .pipe(gulp.dest('./dest/js'))
    .pipe(livereload());
}


gulp.task('js:build', biuld_index.bind(gulp, false));
gulp.task('js:build::product', build_product.bind(gulp, false));

gulp.task('js:watch', gulp.parallel(biuld_index.bind(gulp, true), build_product.bind(gulp, true), build_testing.bind(gulp, true)));

// gulp.task('js::libs:watch', biuld_libs.bind(gulp, true));

module.exports = {
  build : gulp.parallel('js:build'),
  watch : gulp.parallel('js:watch')
};