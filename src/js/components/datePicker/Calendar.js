'use strict';
import {docCreate} from '../../mixins/Element';

import DateCompare from '../../mixins/Date';
import StorePrices from '../../store/store/Prices';

import PriceRange from './PriceRange';
import PriceGradient from './PriceGradient';
// import Eve from './../../mixins/Event';

import classList from '../../mixins/polyfill/element/classList';

import Day from './calendar/day';
import l8n from './../../store/l8n';

export default class Calendar {
  /**
   *
   * @param month
   * @param year
   * @param container
   * @param {CalendarOptions} [options={}]
   * @param {EventStore} eventStore
   * @param {Theme} theme
   */
  constructor(month, year, container, options = {}, eventStore, theme) {
    this._eventStore = eventStore;
    /**
     *
     * @type {Theme}
     * @private
     */
    this._theme = theme;

    this._month = month;
    this._year = year;

    this._startMonth = new Date(year, month, 1);
    this._days = [];
    this._container = container;

    this._cacheDay = '';

    this.__parseOptions(options).__initHTML().__initEvents();
  }

  set $duration(duration) {
    duration = parseInt(duration, 10);
    if (typeof duration === 'number' && !isNaN(duration) && !isFinite(duration)) {
      this._duration = duration;
    } else {
      this._duration = null;
    }
  }

  /**
   *
   * @return {Calendar}
   */
  __initHTML() {
    this._over = docCreate('div');
    // let fragment = document.createDocumentFragment();
    this._node = docCreate('table', 'ottbp-calendar');
    this._nodePriceRange = docCreate('div', 'ottbp-calendar-price_range');

    this._tbody = docCreate('tbody');
    this._thead = docCreate('thead');

    this.__createSelectMonth().__createDaysOfWeekNames().__createMonthName().__createPriceRange().__createWeeks();

    this._node.appendChild(this._thead);
    this._node.appendChild(this._tbody);

    this._over.appendChild(this._node);
    this._over.appendChild(this._nodePriceRange);
    this._container.appendChild(this._over);

    return this;
  }

  /**
   *
   * @return {Calendar}
   * @private
   */
  __createPriceRange() {
    this._priceRange = new PriceRange(this._nodePriceRange);
    return this;
  }

  /**
   *
   * @return {Calendar}
   * @private
   */
  __createSelectMonth() {
    // let tr = docCreate('tr', 'ottbp-cal-month');
    // tr.innerHTML = `<td class="ottbp-cal-month__select" colspan="7"><a href="javascript:void(0);">Выбрать месяц</a></td>`;
    // this._tbody.appendChild(tr);
    return this;
  }

  /**
   *
   * @return {Calendar}
   * @private
   */
  __createDaysOfWeekNames() {
    let tr = docCreate('tr', 'ottbp-cal-days ottbp-texts--week-days');

    tr.innerHTML = l8n.week.abbr.map(function (name, index) {
      return `<td class="ottbp-cal-day ${index >= 5 ? 'ottbp-cal-weekend' : ''}">${name}</td>`;
    }).join('');

    this._thead.appendChild(tr);
    return this;
  }

  /**
   *
   * @return {Calendar}
   * @private
   */
  __createMonthName() {
    let tr = docCreate('tr', 'ottbp-cal-month');
    let monthString = l8n.month.full[this._startMonth.getMonth()] + (this._startMonth.getFullYear() !== (new Date()).getFullYear() ? ' ' + this._startMonth.getFullYear() : '');

    tr.innerHTML = `<td class="ottbp-cal-month__name" colspan="7"><span>${monthString}</span><a href="javascript:void(0);">Выбрать месяц</a></td>`;
    this._tbody.appendChild(tr);
    return this;
  }

  /**
   *
   * @return {Calendar}
   * @private
   */
  __createWeeks() {
    // Дни месяца
    let date = new Date(this._startMonth), max = 10;
    while (date.getMonth() == this._startMonth.getMonth() && max > 0) {
      date = this.__createWeek(date);
      max++;
    }

    return this;
    // return this.__updatePrices();

  }

  /**
   *
   * @param {Date} date
   * @return {Date}
   * @private
   */
  __createWeek(date) {

    // Дни одной недели
    let tr = docCreate('tr', 'ottbp-cal-week');
    for (let dayIndex = 0; dayIndex < 7; dayIndex++) {
      let td = docCreate('td', 'ottbp-cal-day');

      let currentDateIndex = date.getDay() - 1;
      currentDateIndex = (currentDateIndex >= 7 ? 0 : currentDateIndex);
      currentDateIndex = (currentDateIndex < 0 ? 6 : currentDateIndex);

      if (currentDateIndex == dayIndex && date.getMonth() == this._startMonth.getMonth()) {
        let dayDate = new Date(date);
        let inRange = DateCompare.inDateRange(dayDate, this._startDate, this._endDate)
          , selected = (inRange > 0)
          , selectedAfter = (selected ? (inRange < 3) : false)
          , selectedBefore = (selected ? (inRange > 1) : false);

        let day = new Day(dayDate, this._days.length, {
          disabled       : DateCompare.isDisabledDay(dayDate, this._minDate, this._maxDate),
          selected       : selected,
          selectedAfter  : selectedAfter,
          selectedBefore : selectedBefore
        });

        this._days.push(day);
        td.appendChild(day.$node);
        date.setDate(date.getDate() + 1);
      }
      tr.appendChild(td);
    }
    this._tbody.appendChild(tr);
    return date;
  }

  __initEvents() {

    this._eventStore.delegateDocument('click', '.ottbp-btn-day', ::this.__onClickDay_v2);
    this._eventStore.delegateDocument('click', '.ottbp-cal-month', ::this.__onClickMonth_v2);

    this._eventStore.on('Document::mousemove', ::this.__onHover);

    this._eventStore.on('Controller::load:results', ::this.__update_v2);
    this._eventStore.on('Controller::show:results', ::this.__update_v2);

  }

  /**
   *
   * @private
   */
  __clear_v2() {
    this.__updatePrices_v2().__updatePriceRange_v2();
  }

  /**
   *
   * @param event
   * @private
   */
  __update_v2(/*{detail:{price:Price,results:[]}*/event) {
    this.__updatePrices_v2(event.detail.price).__updatePriceRange_v2(event.detail.price);
  }

  // __onClick(event) {
  //   event.stopPropagation();
  //   event.preventDefault();
  //   let target = event.target;
  //   while (target && target !== this._container) {
  //     if (target.className.indexOf('ottbp-btn-day') >= 0 && target.hasAttribute('data-id')) {
  //       /**
  //        * @type {CalendarDay}
  //        */
  //       this.__selectDay(this._days[target.getAttribute('data-id')]);
  //       break;
  //     }
  //     if (target.className.indexOf('ottbp-cal-month') >= 0) {
  //       this.onMonthClick();
  //       break;
  //     }
  //     target = target.parentNode;
  //   }
  //
  //   this.onSelect();
  //   this.__render();
  // }

  __onClickDay_v2(event) {
    let target = event.detail.target;

    if (target.hasAttribute('data-id')) {
      this.__selectDay(this._days[target.getAttribute('data-id')]);
    }

    this.onSelect();
    this.__render();
  }

  __onClickMonth_v2() {
    this.onMonthClick();
    this.onSelect();
    this.__render();
  }

  /**
   *
   * @param {CalendarDay} day
   * @private
   */
  __selectDay(day) {
    let date = new Date(day.$date);

    if (DateCompare.smallerThenDay(date, new Date()))
      return;

    if (this._duration) {
      this._startDate = date;
      this._endDate = DateCompare.addDays(date, this._duration);
    } else {

      if (!this._startDate || !this._isRange || this._endDate || (!this._endDate && DateCompare.smallerThenDay(date, this._startDate))) {
        this._startDate = date;
        this._endDate = null;
      } else {
        this._endDate = date;
      }

    }

  }

  __onHover(event) {


    // event.stopPropagation();
    // event.preventDefault();

    this._hoverDate = null;
    // if (this._isRange && this._startDate && !this._endDate) {

    let target = event.detail.original.target;

    if (classList(target).contains('ottbp-cal-day')) {
      target = target.children[0];
    }

    while (target && target.parentNode && target !== this._container) {
      if (classList(target).contains('ottbp-btn-day') && target.hasAttribute('data-id')) {
        /**
         *
         * @type {Date}
         * @private
         */
        this._hoverDate = this._days[target.getAttribute('data-id')].$date;

      }
      target = target.parentNode;
    }

    if (this._hoverDate && DateCompare.smallerThenDay(this._hoverDate, new Date())) {
      this._hoverDate = null;
    }

    this.__render();
  }

  __render() {
    let ident = (this._startDate ? this._startDate.toString() : 'null')
      + '_' + (this._endDate ? this._endDate.toString() : 'null')
      + '_' + (this._hoverDate ? this._hoverDate.toString() : 'null');

    if (this._cacheDay === ident) {
      return;
    }
    this._cacheDay = ident;

    this.__generateHoverRange().onHover(this._hoverDate, this._hoverRange.start, this._hoverRange.end);

    /**
     * @this {Calendar}
     */
    this._days.forEach(this.__highlightDay.bind(this));

  }

  __generateHoverRange() {

    let start = null, end = null;

    if (this._duration && this._hoverDate) {
      start = this._hoverDate;
      end = DateCompare.addDays(this._hoverDate, this._duration);
    } else if (this._isRange) {

      if (!this._startDate) {
        start = this._hoverDate;
      } else if (this._startDate && !this._endDate) {

        if (!DateCompare.biggerThenDay(this._hoverDate, this._startDate)) {
          start = this._hoverDate;
        } else {
          start = this._startDate;
          end = this._hoverDate;
        }

      } else {
        start = this._hoverDate || null;
        end = null;
      }

    }

    this._hoverRange = {
      start : start,
      end   : end
    };

    return this;
  }

  /**
   *
   * @param {CalendarDay} day
   * @private
   */
  __highlightDay(day) {

    let day_date = day.$date;

    let start = this._hoverRange.start
      , end = this._hoverRange.end;

    let inRange = {
      hover  : DateCompare.inDateRange(day_date, start, end),
      select : DateCompare.inDateRange(day_date, this._startDate, this._endDate)
    };

    if (inRange.select != 0 && inRange.select != -1) {
      day.select(inRange.select == 3 || inRange.select == 2, inRange.select == 1 || inRange.select == 2);
    } else if (inRange.select == -1 && DateCompare.isOneDay(day_date, this._startDate)) {
      day.select();
    } else
      day.unSelect();

    if (inRange.hover != 0 && inRange.hover != -1) {
      day.hover(inRange.hover == 3 || inRange.hover == 2, inRange.hover == 1 || inRange.hover == 2);
    } else {
      day.unHover();
    }
  }

  /**
   *
   * @param {function(CalendarValue)} callback
   * @return {Calendar}
   */
  whenSelect(callback) {
    this._whenSelect = callback;
    return this;
  }

  onSelect() {

    if (this._whenSelect)
      this._whenSelect(this.$value);
  }

  /**
   *
   * @param {function} callback
   * @return {Calendar}
   */
  whenMonthClick(callback) {
    this._whenMonthClick = callback;
    return this;
  }

  onMonthClick() {
    if (this._whenMonthClick)
      this._whenMonthClick();
  }

  /**
   *
   * @param {function(CalendarValue)} callback
   * @return {Calendar}
   */
  whenHover(callback) {
    this._whenHover = callback;
    return this;
  }

  onHover() {
    if (this._whenHover) {
      this._whenHover(this.$value);
    }
  }

  /**
   *
   * @return {CalendarValue}
   */
  get $value() {
    return {
      select     : this._startDate,
      range      : {
        start : (this._isRange ? this._startDate : null),
        end   : (this._isRange ? this._endDate : null)
      },
      hover      : this._hoverDate,
      rangeHover : {
        start : (this._isRange && this._hoverRange ? this._hoverRange.start : (this._hoverDate)),
        end   : (this._isRange && this._hoverRange ? this._hoverRange.end : null),
      }
    };
  }

  clear() {

    this._startDate = null;
    this._endDate = null;
    this._hoverDate = null;
    this._hoverRange = {};
    return this.__render();
  }

  /**
   *
   * @param {StorePrices} [prices=new StorePrices()]
   * @return {Calendar}
   * @private
   */
  __updatePrices_v2(prices = new StorePrices()) {

    if (prices && prices.$type) {

      let gradient = PriceGradient.getPriceColor(prices.$minRange, this._theme);

      this._days.forEach(function (/*CalendarDay*/day, /*number*/index) {
        let price = prices.getBy(index + 1);
        if (price) {
          day.color = gradient.find(price.$delta);
        } else {
          day.color = null;
          day.price = 'none';
        }
      });
    }

    return this;
  }

  /**
   *
   * @param {StorePrices} [prices=new StorePrices()]
   * @return {Calendar}
   * @private
   */
  __updatePriceRange_v2(prices = new StorePrices()) {
    if (prices)
      this._priceRange.update(prices.$minRange);
    else
      this._priceRange.update();
    return this;
  }

  /**
   *
   * @param [month]
   * @param year
   * @param {CalendarOptions} [options={}]
   */
  update(month = this._month, year = this._year, options = {}) {
    this._month = month;
    this._year = year;

    this._startMonth = new Date(year, month, 1);
    /**
     *
     * @type {Array.<Day>}
     * @private
     */
    this._days = [];

    this._cacheDay = '';

    this._tbody.innerHTML = '';

    this.__parseOptions(options).__createMonthName().__createWeeks();

    this.__render();
  }

  show() {
    this._over.setAttribute('style', '');
    // this._over.style.display = '';
  }

  hide() {
    this._over.setAttribute('style', 'display:none !important;');
    // this._over.style.display = 'none';
  }

  /**
   *
   * @param {CalendarOptions} options
   * @return {Calendar}
   * @private
   */
  __parseOptions(options = {}) {
    /**
     *
     * @type {Date|null}
     * @private
     */
    this._startDate = options.dateStart || this._startDate || null;
    /**
     *
     * @type {boolean}
     * @private
     */
    this._isRange = !!options.isRange;
    /**
     *
     * @type {number|null}
     * @private
     */
    this.$duration = options.duration;

    if (this._isRange && this._duration) {
      this._endDate = DateCompare.addDays(this._startDate, this._duration);
    } else if (this._isRange) {
      /**
       *
       * @type {Date|null}
       * @private
       */
      this._endDate = (options.dateEnd || this._endDate || null);
    } else {
      this._endDate = null;
    }
    /**
     *
     * @type {Date}
     * @private
     */
    this._minDate = options.minDate || (new Date());
    /**
     *
     * @type {Date|null}
     * @private
     */
    this._maxDate = options.maxDate || null;
    /**
     *
     * @type {Object|null}
     * @private
     */
    this._query = options.query || null;
    return this;
  }
}