'use strict';

import Inputs from './inputs';

import Selects from './selects';

import {docCreate} from './../mixins/Element';
// import Eve from './../mixins/Event';

export default class Header {
  /**
   *
   * @param {HTMLElement} container
   * @param {EventStore} eventStore
   */
  constructor(container, eventStore) {
    this._eventStore = eventStore;
    this._container = container;
    this.__initHTML();
  }

  /**
   *
   * @return {Header}
   * @private
   */
  __initHTML() {

    this._node = docCreate('div', 'ottbp-form-header');
    this._containerInputs = docCreate('div', 'ottbp-form-header__inputs');
    this._containerSelects = docCreate('div', 'ottbp-form-header__selectors');

    this.__initItems().__initEvents();

    this._node.appendChild(this._containerInputs);
    this._node.appendChild(this._containerSelects);

    this._container.appendChild(this._node);

    return this;
  }

  /**
   *
   * @return {Header}
   * @private
   */
  __initItems() {
    this._inputs = new Inputs(this._containerInputs, this._eventStore);
    this._selectors = new Selects(this._containerSelects, this._eventStore);

    return this;
  }

  /**
   *
   * @return {Header}
   * @private
   */
  __initEvents() {
    //
    // this._eventStore.on('Inputs::change', ::this.__onFormChange);
    // this._eventStore.on('Selects::change', ::this.__onFormChange);

    return this;
  }

  __onFormChange() {
    // this._eventStore.dispatch('Form::main:update', this.value);
  }

  /**
   *
   * @param {number} containerWidth
   */
  resize(containerWidth){
    this._inputs.resize(containerWidth);
    this._selectors.resize(containerWidth);
  }

  /**
   *
   * @param {FormatMainForm} options
   */
  update(options){
    this._inputs.update(options.inputs);
    this._selectors.update(options.selectors);
  }

  /**
   *
   * @return {FormatMainForm}
   */
  get $value() {
    return {
      inputs    : this._inputs.$value,
      selectors : this._selectors.$value
    };
  }
}