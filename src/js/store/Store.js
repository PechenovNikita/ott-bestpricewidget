'use strict';

import StorePrices from './store/Prices';
import Request from './store/Request';
import Response from './store/Response';
import {monthList} from './../mixins/Date';

/**
 *
 * @typedef {Object}                    RequestOptions
 * @property {number}                     [count]
 * @property {boolean}                    [monthly]
 * @property {Object.<string, number>}    [months]
 * @property {string[]}                   [fromDates]
 * @property {string[]}                   [done]
 */
export class Store {
  constructor() {
    this._id = `store_${Date.now()}`;
    /**
     *
     * @type {{prices: (Object.<string,{request:Request,store:StorePrices}>), responses: (Object.<string,Response>)}}
     * @private
     */
    this._storage = {
      prices    : {},
      responses : {}
    };
  }

  /**
   *
   * @param {string} key
   * @return {{request: Request, store: StorePrices}}
   * @private
   */
  __getStore(key) {
    if (!this._storage.prices.hasOwnProperty(key))
      this._storage.prices[key] = {
        request : new Request(),
        store   : new StorePrices()
      };
    return this._storage.prices[key];
  }

  /**
   *
   * @param {Query} query
   * @return {Request}
   */
  getRequest(query) {
    let tempStore = this.__getStore(query.$key);
    return tempStore.request;
  }

  /**
   *
   * @param {Query} query
   * @param responseData
   * @return {*}
   */
  addData(query, responseData) {
    return this.addResponse(query, Response.create(responseData));
  }

  /**
   *
   * @param {Query} query
   * @param {Response} response
   */
  addResponse(query, response) {
    let keyQuery = query.$key, keyResponse = response.$key;

    let departureDate = response.$departure
      , departureYear = departureDate.getFullYear()
      , departureMonth = departureDate.getMonth();

    // проверяем находится ли даты отлета в промежутке "открытых месяцев"
    if (!monthList.some( (/* {year:number,month:number} */item) => item.year == departureYear && item.month == departureMonth)) {
      return;
    }

    let tempStore = this.__getStore(keyQuery);
    // проверяем на количество пересадок
    if (query.$countFlares < response.$countFlares) {
      // let newQuery = query.clone({transfer})

      return this;
    }

    this._storage.responses[keyResponse] = response;

    let args = [response.$key, response.$price];
    tempStore.store.addKey.apply(tempStore.store, args.concat(response.$indexes));

    return this;
  }

  /**
   *
   * @param {Query} query
   * @return {StorePrices}
   */
  getPrices(query) {
    let key = query.$key, prices;
    // console.log(key);
    let tempStore = this.__getStore(key);
    if (!query.$isComplete && query.$departure) {
      // если есть дата отправления но запрос не полный
      // то показываем прайсы с даты отправления
      if (query.$inMonth) {
        let month = query.$month;
        // с даты отаравления до текущего месяца
        prices = tempStore.store.getBy(`${query.$departureYear}_${query.$departureMonth}`, `${query.$departureDay}`, `${month.year}_${month.month}`);
      } else {
        // console.log('!query.complete && query.departure', 'else query.inMonth');
        prices = tempStore.store.getBy(`${query.$departureYear}_${query.$departureMonth}`, `${query.$departureDay}`);
      }

    } else {
      // console.log('else !query.complete && query.departure');
      // если запрос полный (выбрвно все что нужно) или наоборот без дат
      // возвращаем все прайсы или прайсы за месяц
      if (query.$inMonth) {
        let month = query.$month;
        prices = tempStore.store.getBy(`${month.year}_${month.month}`);
      } else {
        prices = tempStore.store;
      }
    }

    return prices || new StorePrices();
  }

  /**
   *
   * @param {Query} query
   * @return {StorePrices}
   */
  getClosestPrices(query) {
    let key = query.$key, prices;
    /**
     *
     * @type {{request: Request, store: StorePrices}}
     */
    let tempStore = this.__getStore(key);

    if (query.$departure) {
      if (query.$return) {
        prices = tempStore.store.getByOrThis(`${query.$departureYear}_${query.$departureMonth}`, `${query.$departureDay}`, `${query.$returnYear}_${query.$returnMonth}`, `${query.$returnDay}`);
      } else {
        prices = tempStore.store.getByOrThis(`${query.$departureYear}_${query.$departureMonth}`, `${query.$departureDay}`);
      }
    } else {
      prices = tempStore.store;
    }

    return prices || new StorePrices();
  }

  /**
   *
   * @param {Query} query
   * @return {StorePrices}
   */
  getRealPrices(query) {
    let key = query.$key, prices;
    /**
     *
     * @type {{request: Request, store: StorePrices}}
     */
    let tempStore = this.__getStore(key);

    if (query.$departure) {
      if (query.$return) {
        prices = tempStore.store.getBy(`${query.$departureYear}_${query.$departureMonth}`, `${query.$departureDay}`, `${query.$returnYear}_${query.$returnMonth}`, `${query.$returnDay}`);
      } else {
        prices = tempStore.store.getBy(`${query.$departureYear}_${query.$departureMonth}`, `${query.$departureDay}`);
      }
    } else {
      prices = tempStore.store;
    }

    return prices || new StorePrices();
  }

  /**
   *
   * @param {Query} query
   * @return {Array.<Response>}
   */
  getResponse(query) {
    let key = query.$key, indexes = [], globalIndexes = [];
    if (query.$departure) {
      indexes.push(`${query.$departureYear}_${query.$departureMonth}`, `${query.$departureDay}`);
      globalIndexes.push(`${query.$departureYear}_${query.$departureMonth}`, `${query.$departureDay}`);
      if (query.$return) {
        indexes.push(`${query.$returnYear}_${query.$returnMonth}`, `${query.$returnDay}`);
        globalIndexes.push(`${query.$returnYear}_${query.$returnMonth}`, `${query.$returnDay}`);
      } else {
        if (query.$isRoundTrip && query.$inMonth) {
          let month = query.$month;
          indexes.push(`${month.year}_${month.month}`);
        }
      }
    } else {
      if (query.$inMonth) {
        let month = query.$month;
        indexes.push(`${month.year}_${month.month}`);
      }
    }
    /**
     *
     * @type {StorePrices}
     */
    let mainStore = this.__getStore(key).store
      , priceStore = mainStore.getBy.apply(mainStore, indexes)
      , globalStore = mainStore.getBy.apply(mainStore, globalIndexes);

    if (query.$inMonth && !priceStore) {
      return [];
    }

    if (priceStore) {
      let keys = priceStore.$keys;
      if (!key || key.length == 0)
        keys = globalStore.$keys;

      return keys.map(function (responseKey) {
        return this._storage.responses[responseKey] || null;
      }, this).filter(function (response) {
        return !!response;
      });
    } else if (globalStore) {
      return globalStore.$keys.map(function (responseKey) {
        return this._storage.responses[responseKey] || null;
      }, this).filter(function (response) {
        return !!response;
      });
    }
    return [];
  }

  /**
   *
   * @param {Query} query
   */
  getCities(query) {
    /**
     *
     * @type {StorePrices}
     */
    let resultPrices = this.getRealPrices(query);
    let resultKeys = resultPrices.$keys;
    /**
     *
     * @type {StorePrices}
     */
    let closestPrice = this.getClosestPrices(query);
    /**
     *
     * @type {StorePrices}
     */
    let globalPrices = closestPrice.$global;
    /**
     *
     * @type {Array.<string>}
     */
    let globalKeys = globalPrices.$keys;

    let globalCities = {}, cities = [];

    resultKeys.forEach(function (responseKey) {
      if (this._storage.responses[responseKey]) {
        let city_iata = this._storage.responses[responseKey].$to;
        if (cities.indexOf(city_iata) < 0)
          cities.push(city_iata);
      }
    }.bind(this));

    if (cities.length > 2)
      return [];

    globalKeys.forEach(function (responseKey) {
      /**
       * @this {Store}
       */
      if (this._storage.responses[responseKey]) {
        let city_iata = this._storage.responses[responseKey].$to;
        if (cities.indexOf(city_iata) >= 0)
          return;

        if (!globalCities[city_iata])
          globalCities[city_iata] = 0;
        globalCities[city_iata]++;
      }
    }, this);

    // console.log(resultPrices, closestPrice, globalPrices, globalKeys, globalCities);

    let temp = {};
    for (let iata in globalCities) {
      if (!temp[globalCities[iata]]) {
        temp[globalCities[iata]] = [];
      }
      temp[globalCities[iata]].push(iata);
    }

    let t = [];
    for (let num in temp) {
      t.push(temp[num]);
    }

    t.reverse();

    return Array.prototype.concat.apply([], t);
  }
}

export default Store;