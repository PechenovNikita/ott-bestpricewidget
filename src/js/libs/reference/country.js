'use strict';

import countriesEN from '../ref/countries';
import countriesRU from '../refRu/countries';

import findKeys from './mixins/findKeys';
import sortPlaceData from './mixins/sortPlaceData';

/**
 *
 * @param {string} iata
 * @return {PlaceData}
 */
export function byIATA(iata) {
  return __getCountryEN(iata);
}

/**
 *
 * @param {string} iata
 * @return {PlaceData|null}
 * @private
 */
function __getCountryEN(iata) {
  if (countriesEN.hasOwnProperty(iata)) {
    //1|Czech Republic|49.82|15.47
    let countryData = countriesEN[iata].split('|');
    let countryRu = __getCountryRU(iata);

    if (!countryRu)
      return null;

    return {
      iata    : iata,
      type    : 'country',
      country : {
        iata : iata,
        en   : countryData[1],
        ru   : countryRu
      }
    };

  }

  return null;
}
/**
 *
 * @param {string} iata
 * @return {string|null}
 * @private
 */
function __getCountryRU(iata) {
  if (countriesRU.hasOwnProperty(iata))
    return countriesRU[iata];
  return null;
}

/**
 *
 * @param part
 * @return {Array.<*>}
 */
export function byPart(part) {
  return findKeys(part, countriesRU)
    .map(function (/*string*/iata) {
      return byIATA(iata);
    }).filter(function (/*(PlaceData|null)*/countryData) {
      return !!countryData;
    }).sort(function (/*PlaceData*/countryDataA, /*PlaceData*/countryDataB) {
      return sortPlaceData(countryDataA, countryDataB, part);
    });
}


