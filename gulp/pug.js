const gulp = require('gulp')
  , watch = require('gulp-watch')
  , pug = require('gulp-pug')
  , livereload = require('gulp-livereload');

const config = require('./_config');

function build_index() {
  return gulp.src('./src/pug/index.pug')
    .pipe(pug({
      pretty : true,
      locals : {config : config}
    }))
    .pipe(gulp.dest(config.routes.dest))
    .pipe(livereload());
}

gulp.task('pug:build', build_index);
gulp.task('pug:watch', function () {
  return watch('./src/pug/**/*.pug', {verbose : true}, build_index);
});

module.exports = {
  build : gulp.parallel('pug:build'),
  watch : gulp.parallel('pug:watch')
};