'use strict';

/**
 *
 * @type {FormatMainForm}
 */
export const defaultFormData = {
  inputs    : {
    from      : null,
    to        : null,
    toCountry : null
  },
  selectors : {
    duration : 'any',
    route    : true,
    transfer : 'any',
    type     : 'E'
  },
  date      : {
    start   : null,
    end     : null,
    inMonth : null
  }
};

/**
 *
 * @type {ThemeColor}
 */
export const defaultThemeColor = {
  WidgetBG : '#878D96',
  WidgetTX : '#FFFFFF',

  WidgetBtn   : '#FFD41E',
  WidgetBtnTX : '#616161',

  WidgetPriceGradient : {
    low    : '#a5e3af',
    middle : '#EFF9E8',
    high   : '#ffc2a3'
  }
};

/**
 *
 * @type {ThemeOptions}
 */
export const defaultTheme = {
  width : null,
  color : defaultThemeColor
};