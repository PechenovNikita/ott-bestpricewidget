'use strict';
/**
 *
 * @type {string[]}
 */
export const monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
/**
 *
 * @type {string[]}
 */
export const daysOfWeekShort = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПН', 'СБ', 'ВС'];

export const monthNameShort = ['янв', 'фев', 'март', 'апр', 'май', 'июнь', 'июль', 'авг', 'сен', 'окт', 'ноя', 'дек'];
export const monthName = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

export default {
  monthName       : monthName,
  monthNames      : monthNames,
  monthNameShort  : monthNameShort,
  daysOfWeekShort : daysOfWeekShort
};
