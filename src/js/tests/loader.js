'use strict';

import Loader from './../components/loader';

let btn = document.getElementById('loader-trigger')
  , loaderNode = document.getElementById('loader');

let loader = new Loader(loaderNode);
let trigger = false;

btn.addEventListener('click', function () {

  if (trigger)
    loader.hide();
  else
    loader.show();

  trigger = !trigger;
});
