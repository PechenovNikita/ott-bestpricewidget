'use strict';

import {dateFromString} from './Format';
/**
 *
 * @type {Date}
 */
export const currentDate = new Date();

/**
 *
 * @param {Date} date
 * @return {Date}
 */
export function normalizeDateToDay(date) {
  let d = new Date(date);
  d.setHours(0);
  d.setMinutes(0);
  d.setSeconds(0);
  d.setMilliseconds(0);
  return d;
}
/**
 *
 * @param {Date} currentDate
 * @param {Date} selectDate
 * @return {boolean}
 */
export function isOneDay(currentDate, selectDate) {
  if (!selectDate || !(selectDate instanceof Date) || !currentDate || !(currentDate instanceof Date)) {
    return false;
  }
  return selectDate.getMonth() == currentDate.getMonth()
    && selectDate.getFullYear() == currentDate.getFullYear()
    && selectDate.getDate() == currentDate.getDate();
}
/**
 *
 * @param {Date} currentDate
 * @param {Date} minDate
 * @return {boolean}
 */
export function smallerThenDay(currentDate, minDate) {

  if (!currentDate || !minDate || !(currentDate instanceof Date) || !(minDate instanceof Date))
    return false;

  return normalizeDateToDay(currentDate) < normalizeDateToDay(minDate);
}

export function biggerThenDay(currentDate, maxDate) {
  if (!currentDate || !maxDate || !(currentDate instanceof Date) || !(maxDate instanceof Date))
    return false;

  return normalizeDateToDay(currentDate) > normalizeDateToDay(maxDate);
}
/**
 *
 * @param {Date} date
 * @return {Date}
 */
export function nextDay(date) {
  if (!date || !(date instanceof Date))
    return date;
  return addDays(date, 1);
}
/**
 *
 * @param {Date} date
 * @param {number} days
 * @return {Date}
 */
export function addDays(date, days) {
  if (!date || !(date instanceof Date))
    return date;
  return new Date((new Date(date)).setDate(date.getDate() + days));
}

/**
 *
 * @param {Date} currentDate
 * @param {Date} minDate
 * @param {Date} maxDate
 * @return {boolean}
 */
export function isDisabledDay(currentDate, minDate, maxDate) {
  if (!currentDate || !(currentDate instanceof Date))
    return true;

  if (smallerThenDay(currentDate, minDate))
    return true;

  return biggerThenDay(currentDate, maxDate);
}
/**
 *
 * @param {Date} currentDate
 * @param {Date} minDate
 * @param {Date} maxDate
 * @return {Date}
 */

export function setDateInRange(currentDate, minDate, maxDate) {
  if (!currentDate || !(currentDate instanceof Date))
    return currentDate;

  if (smallerThenDay(currentDate, minDate))
    return new Date(minDate);

  if (biggerThenDay(currentDate, maxDate))
    return new Date(maxDate);

  return currentDate;

}
/**
 *
 * @param {Date} currentDate
 * @param {Date} minDate
 * @param {Date} maxDate
 * @return {number}
 */
export function inDateRange(currentDate, minDate, maxDate) {
  if (!minDate || !(minDate instanceof Date) || !maxDate || !(maxDate instanceof Date) || isOneDay(minDate, maxDate))
    return -1; // Range не задан
  if (isOneDay(currentDate, minDate))
    return 1; // старт
  if (isOneDay(currentDate, maxDate))
    return 3; // конец
  if (smallerThenDay(currentDate, maxDate) && biggerThenDay(currentDate, minDate))
    return 2; // посередине
  return 0;
}

/**
 *
 * @param {Date} currentDate
 * @param {Date} withDate
 * @return {boolean}
 */
export function isOneDayOrNotExist(currentDate, withDate) {
  if (!currentDate || !(currentDate instanceof Date))
    return false;
  if (!withDate || !(withDate instanceof Date))
    return true;
  return isOneDay(currentDate, withDate);
}
/**
 * @type {Array.<{year:number,month:number}>}
 */
export let monthList = (function () {
  let arrMonth = [{
    month : currentDate.getMonth(),
    year  : currentDate.getFullYear()
  }];

  let date = new Date(currentDate);

  for (let i = 0; i < 9; i++) {
    date.setMonth(date.getMonth() + 1);
    arrMonth.push({
      month : date.getMonth(),
      year  : date.getFullYear()
    });
  }

  return arrMonth;
})();

/**
 * @function
 * @param {Array} [results]
 * @return {{month: number, year: number, price: string, [prices]: number[]}[]}
 */
export let generateMonthList = (function () {

  function sa(numbers) {
    let len = numbers.length;
    if (len == 0)
      return 0;
    let sum = 0;
    for (let i = 0; i < len; i++) {
      sum += +numbers[i];
    }
    return Math.abs(sum / len);
  }

  /**
   *
   * @param {number} price
   * @param {number} low
   * @param {number} middle
   * @param {number} high
   * @param {Array<*>} [returns=[0, 1, 2]]
   * @return {*}
   */
  function priceIN(price, low, middle, high, returns = [0, 1, 2]) {
    let delLowMiddle = low + (middle - low) / 2
      , delMiddleHigh = middle + (high - middle) / 2;

    if (price < delLowMiddle)
      return returns[0];
    else if (price < delMiddleHigh)
      return returns[1];
    else
      return returns[2];
  }

  /**
   *
   * @param {Array} results
   * @param {{month: number, year: number, price: string, [prices]: number[]}[]} monthList
   * @return {*}
   */
  function arrayMonthFilter(results, monthList) {
    if (!results)
      return monthList;

    let temp = {};

    let prices = results.map(function (item) {
      let date = (dateFromString(item.departDate))
        , year = date.getFullYear()
        , month = date.getMonth();

      if (!temp[year])
        temp[year] = {};
      if (!temp[year][month])
        temp[year][month] = {prices : []};

      temp[year][month].prices.push(item.price);

      return item.price;
    });

    let low_price = Math.min.apply(Math, prices);
    let high_price = Math.max.apply(Math, prices);
    let middle_price = sa(prices);

    monthList = monthList.map(function (item) {
      if (temp[item.year] && temp[item.year][item.month]) {
        item.price = priceIN(sa(temp[item.year][item.month].prices), low_price, middle_price, high_price, ['low', 'middle', 'high']);
      }
      item.prices = [low_price, middle_price, high_price];
      return item;
    });

    return monthList;
  }

  return (results) => {

    let date = new Date();

    date.setDate(1);
    /**
     *
     * @type {{month:number, year:number, price:string, [prices]: number[]}[]}
     */
    let monthList = [{
      month : date.getMonth(),
      year  : date.getFullYear(),
      price : 'none'
    }];

    for (let i = 0; i < 9; i++) {
      date.setMonth(date.getMonth() + 1);
      monthList.push({
        month : date.getMonth(),
        year  : date.getFullYear(),
        price : 'none'
      });
    }

    monthList = arrayMonthFilter(results, monthList);

    return monthList;
  };
}());

export function days_between(date1, date2) {

  // The number of milliseconds in one day
  let ONE_DAY = 1000 * 60 * 60 * 24;

  // Convert both dates to milliseconds
  let date1_ms = date1.getTime();
  let date2_ms = date2.getTime();

  // Calculate the difference in milliseconds
  let difference_ms = Math.abs(date1_ms - date2_ms);

  // Convert back to days and return
  return Math.round(difference_ms / ONE_DAY);

}

// export const monthly = (function () {
//   let currentDate = new Date()
//     , date = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1)
//     , months = [];
//
//   for (let i = 0; i < 10; i++) {
//     months.push({
//       year  : date.getFullYear(),
//       month : date.getMonth()
//     });
//     date.setMonth(date.getMonth() + 1);
//   }
//
//   return months;
// }());

export default {
  normalizeDateToDay : normalizeDateToDay,
  isOneDay           : isOneDay,
  isOneDayOrNotExist : isOneDayOrNotExist,
  smallerThenDay     : smallerThenDay,
  biggerThenDay      : biggerThenDay,
  nextDay            : nextDay,
  addDays            : addDays,
  isDisabledDay      : isDisabledDay,
  setDateInRange     : setDateInRange,
  inDateRange        : inDateRange
};