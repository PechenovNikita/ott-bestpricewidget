/*eslint no-unused-vars: ["error", { "args": "none" }]*/
'use strict';

import Controller from '../Controller';
import Query from '../controller/Query';
import DemoControllerResults from './ControllerResults';

export default class DemoController extends Controller {
  /**
   *
   * @param {EventStore} eventStore
   */
  constructor(eventStore) {
    super(eventStore);
    /**
     *
     * @type {EventStore}
     * @private
     */
    this._eventStore = eventStore;
  }

  /**
   *
   * @return {DemoController}
   * @private
   */
  __init() {
    this._results = new DemoControllerResults(this._eventStore);
    return this;
  }

  /**
   *
   * @private
   */
  __updateResults() {
    let query = new Query(this._formData);

    this._eventStore.dispatch('Controller::show:results', this._results.show(query));
  }

  /**
   *
   * @return {Controller}
   * @private
   */
  __initEvents() {

    this._eventStore.on(['Form::change'], (event) => {
      this._formData = event.detail;
      this.__updateResults();
    });

    return this;
  }
}