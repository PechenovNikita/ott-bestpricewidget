'use strict';

import Request from './Request';

export default class Ajax extends Request {
  constructor(url, options) {
    super(url, options);

    this.options.method = (options.method && options.method.toLowerCase() == 'post' ? 'POST' : 'GET');
    this.options.timeout = (options.timeout || 60);
  }

  __initAdds() {
    this._adds = {};
    this._adds['_'] = this.$id;
  }

  __initSend() {

    const xmlhttp = new XMLHttpRequest();

    let self = this;

    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == XMLHttpRequest.DONE) {
        if (xmlhttp.status == 200) {
          self.__success(xmlhttp.responseText);
        }
        else if (xmlhttp.status == 400) {
          self.__fail();
        }
        else {
          self.__fail();
        }
      }
    };

    xmlhttp.open(this.options.method, this._src, true);
    xmlhttp.send();

  }

  __initTimeout(){
    return this;
  }

  __fail(){

  }
}
