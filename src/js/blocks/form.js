'use strict';

import {docCreate, classAddRemove} from './../mixins/Element';
import classList from '../mixins/polyfill/element/classList';

import {
  CONTAINER_SIZE_BIG,
  CONTAINER_SIZE_MEDIUM,
  CONTAINER_SIZE_SMALL,
  CONTAINER_SIZE_EXTRA_SMALL
} from '../config/_constant';

import Color from '../mixins/Color';

import Header from './header';
import Footer from './footer';

import {deepExtend} from './../mixins/Deep';

import {defaultFormData} from './../config/_default';

export default class Form {
  /**
   *
   * @param {HTMLElement} container
   * @param {EventStore} eventStore
   * @param {Theme} theme
   */
  constructor(container, eventStore, theme) {
    this._eventStore = eventStore;
    this._theme = theme;
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._container = container;

    this._query = {};
    this.initHTML().__initEvents();

    this._value = deepExtend(defaultFormData);

    this._eventStore.dispatch('Form::init', this.$value);

    this.resize();
  }

  /**
   *
   * @return {Form}
   */
  initHTML() {

    this._node = docCreate('div', ['ottbp-form']);

    this._containerHeader = docCreate('div', 'ottbp-form__header');

    this._containerFooter = docCreate('div', 'ottbp-form__footer');

    this._header = new Header(this._containerHeader, this._eventStore);
    this._footer = new Footer(this._containerFooter, this._eventStore, this._theme);

    this._node.appendChild(this._containerHeader);
    this._node.appendChild(this._containerFooter);

    this._container.appendChild(this._node);

    return this;
  }

  __initEvents() {

    let hovered = null;

    function hover(target) {
      if (hovered == target)
        return;
      if (hovered)
        unHover();

      hovered = target;
      let back = target.style.backgroundColor;
      target.setAttribute('data-back', back);
      target.setAttribute('style', `background-color: ${Color.shadeRGBString(back, -.10)} !important;`);
    }

    function unHover() {
      if (hovered) {
        hovered.setAttribute('style', `background-color: ${hovered.getAttribute('data-back')} !important;`);
      }
      hovered = null;
    }

    document.addEventListener('mouseover', (event) => {
      let target = event.target;

      while (target && target !== document.body && target !== this._node) {
        if (classList(target).contains('ottbp-js-hover')) {
          return hover(target);
        }
        target = target.parentNode;
      }
      unHover();

    });

    this._eventStore.on('Control::set', (event) => {
      let target = event.detail.target;

      let targets = Array.prototype.slice.call(this._node.querySelectorAll('input.ottbp-input__field, button.ottbp-select__btn'));

      targets.forEach(function (tr, index) {
        if (tr == target && targets[index + 1]) {
          targets[index + 1].focus();
        }
      });

    });

    let date = 0;

    this._eventStore.on(['Inputs::change', 'Selects::change', 'DatePicker::changed', 'DateSelect::view:months', 'DateSelect::view:calendar'], () => {
      // deepExtend(this._value, event.detail);
      let temp = Date.now();
      date = temp;
      setTimeout(() => {
        if (temp == date) {
          this._eventStore.dispatch('Form::change', this.$value);
        }
      }, 100);
    });

    // this._eventStore.on('Inputs::change', ::this.__whenInputsChange);
    // this._eventStore.on('Selects::change', ::this.__whenSelectorsChange);

    this._eventStore.on('Window::resize', ::this.resize);
    this._eventStore.on('Form::update:to:city', (event) => {
      this.update({inputs : {to : event.detail.value}});
    });

    return this;
  }

  resize() {
    let containerWidth = this._container.offsetWidth;

    let classes = ['ottbp-big', 'ottbp-more-big', 'ottbp-medium', 'ottbp-more-medium', 'ottbp-small', 'ottbp-more-small', 'ottbp-extrasmall', 'ottbp-more-extrasmall'];

    if (containerWidth >= CONTAINER_SIZE_BIG) {
      classAddRemove(this._node, ['ottbp-big', 'ottbp-more-extrasmall', 'ottbp-more-small', 'ottbp-more-medium', 'ottbp-more-big'], classes);
    } else if (containerWidth >= CONTAINER_SIZE_MEDIUM) {
      classAddRemove(this._node, ['ottbp-medium', 'ottbp-more-extrasmall', 'ottbp-more-small', 'ottbp-more-medium'], classes);
    } else if (containerWidth >= CONTAINER_SIZE_SMALL) {
      classAddRemove(this._node, ['ottbp-small', 'ottbp-more-extrasmall', 'ottbp-more-small'], classes);
    } else if (containerWidth >= CONTAINER_SIZE_EXTRA_SMALL) {
      classAddRemove(this._node, ['ottbp-extrasmall', 'ottbp-more-extrasmall'], classes);
    } else {
      classAddRemove(this._node, [], classes);
    }

    this._header.resize(containerWidth);
    this._footer.resize(containerWidth);
  }

  /**
   *
   * @param {FormatMainForm} options
   */
  update(options) {
    /**
     * @type {FormatMainForm}
     */
    let newData = deepExtend(this.$value, options);

    this._header.update(newData);
    this._footer.update(newData);
  }

  /**
   * @return {FormatMainForm}
   */
  get $value() {
    return deepExtend({}, this._header.$value, this._footer.$value);
  }
}