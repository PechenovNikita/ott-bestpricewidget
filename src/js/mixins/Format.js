'use strict';

import {monthNameShort, monthName} from './l8n';

import Request from './format/Request';
import Url from './format/Url';
const currentYear = (new Date()).getFullYear();
/**
 *
 * @param {number} num
 * @return {string}
 */
export function zeroNumber(num) {
  return `${num > 9 ? num : '0' + num}`;
}
/**
 *
 * @param moneyNumber
 * @param aftrerComma
 * @param comma
 * @param delimiter
 * @return {string}
 */
export function money(moneyNumber, aftrerComma, comma, delimiter) {
  aftrerComma = (isNaN(aftrerComma = Math.abs(aftrerComma)) ? 2 : aftrerComma);
  comma = comma == undefined ? '.' : comma;
  delimiter = delimiter == undefined ? ',' : delimiter;

  let s = moneyNumber < 0 ? '-' : '',
    i = String(parseInt(moneyNumber = Math.abs(Number(moneyNumber) || 0).toFixed(aftrerComma))),
    j = i.length;
  j = ((j = i.length) > 3 ? j % 3 : 0);

  return s + (j ? i.substr(0, j) + delimiter : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + delimiter) + (aftrerComma ? comma + Math.abs(moneyNumber - i).toFixed(aftrerComma).slice(2) : '');
}
/**
 *
 * @param price
 * @param currency
 * @return {string}
 */
export function price(price, currency) {
  let currencyString = 'USD';
  switch (currency) {
    case 'RUB':
      currencyString = '&#8381;';
      break;
  }

  return `${money(price, 0, '.', ' ')} ${currencyString}`;

}
/**
 *
 * @param dateString
 * @return {?Date}
 */
export function dateFromString(dateString) {
  if (!dateString || dateString.length != 8)
    return null;

  let year = parseInt(dateString.substr(0, 4), 10)
    , month = parseInt(dateString.substr(4, 2), 10) - 1
    , day = parseInt(dateString.substr(6, 2), 10);
  return new Date(year, month, day);
}

/**
 *
 * @param {string} dateString - 2017-01-17
 * @return {Date}
 */
export function dateFromQueryString(dateString) {
  let dates = dateString.split('-');
  return new Date(dates[0], parseInt(dates[1], 10) - 1, dates[2]);
}
/**
 *
 * @param {Date} date
 * @return {string} - YYYY-MM-DD
 */
export function stringFromDate(date) {
  let year = date.getFullYear()
    , month = date.getMonth() + 1
    , day = date.getDate();

  return `${year}-${zeroNumber(month)}-${zeroNumber(day)}`;
}
/**
 *
 * @param {Date} date
 * @return {string} : HH:MM
 */
export function timeFromDate(date) {
  return `${zeroNumber(date.getHours())}:${zeroNumber(date.getMinutes())}`;
}
/**
 *
 * @param {Date} date
 * @param {boolean} [hideYear]
 * @return {string}
 */
export function dateToShotString(/*Date*/date, hideYear) {
  let year = date.getFullYear()
    , month = date.getMonth()
    , day = date.getDate();

  return `${day} ${monthNameShort[month]}` + (year !== currentYear && !hideYear ? `, ${year}` : '');
}

export function dateDurations(startDate, endDate) {
  let timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
  return Math.ceil(timeDiff / (1000 * 3600 * 24));
}
/**
 *
 * @param date
 * @return {string}
 */
export function dateToFlightString(date) {
  let month = date.getMonth() + 1
    , day = date.getDate();

  return `${zeroNumber(day)}${zeroNumber(month)}`;
}

export function declOfNum(number, titles) {
  number = Math.abs(number);
  let cases = [2, 0, 1, 1, 1, 2];
  return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

/**
 *
 * @param {Date} date
 * @return {string}
 */
export function dateString(date) {
  let month = monthName[date.getMonth()]
    , day = date.getDate()
    , year = date.getFullYear();
  return `${day} ${month}${currentYear != year ? ' ' + year : ''}`;
}

export default{
  url     : {
    country : {
      low : Url.lowCountry
    }
  },
  query   : {
    bestDeals : {
      dateToString     : stringFromDate,
      dateToTimeString : timeFromDate,
      stringToDate     : dateFromString
    },
    searching : {
      dateToString : function (date) {
        if (date instanceof Date) {
          let month = date.getMonth() + 1
            , day = date.getDate();
          month = month > 9 ? month : `0${month}`;
          day = day > 9 ? day : `0${day}`;
          return `${day}${month}`;
        }
        return null;
      }
    }
  },
  request : Request
};

export function isNative(what) {
  return /\{\s+\[native code\]/.test(Function.prototype.toString.call(what));
}
