'use strict';

/**
 *
 * @param {string} hexString
 * @return {{r:number,g:number,b:number}}
 */
export function hexToRgb(hexString) {
  let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexString);
  return result ? {
    r : parseInt(result[1], 16),
    g : parseInt(result[2], 16),
    b : parseInt(result[3], 16)
  } : null;
}
/**
 *
 * @param {number} a
 * @param {number} b
 * @param {number} delta
 * @return {number}
 * @private
 */
function __colorNumberBetween(a, b, delta) {
  return a + (b - a) * delta;
}

export function betweenRGB(rgbA, rgbB, delta) {
  return {
    r : __colorNumberBetween(rgbA.r, rgbB.r, delta),
    g : __colorNumberBetween(rgbA.g, rgbB.g, delta),
    b : __colorNumberBetween(rgbA.b, rgbB.b, delta),
  };
}