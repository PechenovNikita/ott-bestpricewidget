'use strict';

import Svg from '../Svg';
import './sprites';

export default class Arrow extends Svg {
  constructor() {
    super();

    this._attr = {
      width  : 7,
      height : 10
    };

    this._options = {
      fill : '#FFFFFF'
    };

    this.update();
  }

  /**
   *
   * @return {Earth}
   */
  update() {
    this._inner = `<use xlink:href="#ottbp-s-arrow" fill="${this._options.fill}"  class="ottbp-sprite-svg__arrow"></use>`;
    return this.render();
  }

  /**
   *
   * @param {number} [width]
   * @param {number} [height]
   * @return {Earth}
   */
  resize(width = this._attr.width, height = this._attr.height) {
    return this.attr({
      width  : width,
      height : height
    });
  }

  /**
   *
   * @param fill
   * @return {Earth}
   */
  fill(fill) {
    this._options.fill = fill;
    return this.update();
  }

}

