'use strict';

import Color from './../mixins/Color';
import {deepExtend} from '../mixins/Deep';
import {defaultTheme} from '../config/_default';

import Params from '../config/_params';



export default class Theme {
  /**
   *
   * @param {ThemeOptions} theme
   */
  constructor(theme = {}) {
    /**
     * @type {ThemeOptions}
     */
    this._current = deepExtend({}, defaultTheme, theme);
    this._id = Params.get('id');
    this.__initHTML();
  }

  /**
   *
   * @return {Theme}
   * @private
   */
  __initHTML() {
    this._styleNode = document.createElement('style');
    this._styleNode.setAttribute('type', 'text/css');
    return this;
  }

  /**
   *
   * @return {string}
   */
  get $styles() {

    const darkenBtnColor = Color.shadeRGB(Color.toRGB(this._current.color.WidgetBtn), -.02)
      , lightenBtnColor = Color.shadeRGB(Color.toRGB(this._current.color.WidgetBtn), .15);

    const btnHover = (Color.isLight(this._current.color.WidgetBtn)
      ? Color.shadeRGB(Color.toRGB(this._current.color.WidgetBtn), -.15)
      : Color.shadeRGB(Color.toRGB(this._current.color.WidgetBtn), .15));

    const width = this._current.width
      ? `#${this._id} div.ottbp-widget {width: ${this._current.width}px !important; }`
      : '';

    return `#${this._id} div.ottbp-form-header {`
      + `background: ${this._current.color.WidgetBG} !important;` +
      '}' +

      `#${this._id} div.ottbp-trip__header {`
      + `background: ${this._current.color.WidgetBG} !important;` +
      '}' +

      `#${this._id} div.ottbp-container-picker{`
      + `background: ${this._current.color.WidgetBG} !important;` +
      '}' +

      `#${this._id} div.ottbp-trip__header span.ottbp-black{`
      + `color: ${this._current.color.WidgetTX} !important;` +
      '}' +

      `#${this._id} div.ottbp-trip__header span.ottbp-gray{`
      + `color: ${this._current.color.WidgetTX} !important;`
      + 'opacity: .5 !important;' +
      '}' +

      `#${this._id} .ottbp-select.ottbp-selected button.ottbp-select__btn, button.ottbp-search__btn {`
      + `background: ${this._current.color.WidgetBtn} !important;`
      + `color: ${this._current.color.WidgetBtnTX} !important;` +
      '}' +

      `#${this._id} button.ottbp-search__btn svg > use.ottbp-sprite-svg__search{`
      + `fill: ${this._current.color.WidgetBtnTX} !important;` +
      '}' +

      `#${this._id} .ottbp-select.ottbp-selected button.ottbp-select__btn:hover, button.ottbp-search__btn:hover {`
      + `background: ${btnHover} !important;` +
      '}' +

      `#${this._id} .ottbp-select.ottbp-disabled button.ottbp-select__btn,` +
      `#${this._id} .ottbp-select.ottbp-disabled button.ottbp-select__btn:hover {`
      + 'background: rgb(100, 100, 100) !important;'
      + 'opacity: .6 !important;'
      + 'color: rgba(50, 50, 50, 0.3) !important;' +
      '}' +

      `#${this._id} div.ottbp-loader__back__over {`
      + `background: ${this._current.color.WidgetBtn} !important;` +
      '}' +

      `#${this._id} div.ottbp-loader__front {`
      + `background: ${this._current.color.WidgetBtn} !important;`
      + `background: -moz-linear-gradient(45deg, ${darkenBtnColor} 0%, ${this._current.color.WidgetBtn} 50%, ${lightenBtnColor} 100%) !important;`
      + `background: -webkit-linear-gradient(45deg, ${darkenBtnColor} 0%, ${this._current.color.WidgetBtn} 50%, ${lightenBtnColor} 100%) !important;`
      + `background: linear-gradient(45deg, ${darkenBtnColor} 0%, ${this._current.color.WidgetBtn} 50%, ${lightenBtnColor} 100%) !important;` +
      '}' +

      `#${this._id} div.ottbp--low_price, button.ottbp--low_price {`
      + `background: ${this._current.color.WidgetPriceGradient.low} !important;` +
      '}' +

      `#${this._id} div.ottbp--middle_price, button.ottbp--middle_price {`
      + `background: ${this._current.color.WidgetPriceGradient.middle} !important;` +
      '}' +

      `#${this._id} div.ottbp--high_price, button.ottbp--high_price {`
      + `background: ${this._current.color.WidgetPriceGradient.high} !important;` +
      '}'

      + width;
  }

  /**
   *
   * @param {ThemeOptions} [styles]
   * @return {Theme}
   */
  __setStyles(styles = {}) {
    this._current = deepExtend({}, this._current, styles);
    this._styleNode.textContent = this.$styles;
    return this;
  }

  /**
   *
   * @param {ThemeOptions} [styles]
   * @return {Theme}
   */
  init(styles = {}) {
    this.__setStyles(styles);
    document.head.appendChild(this._styleNode);
    return this;
  }

  /**
   *
   * @param {ThemeOptions} styles
   * @return {Theme}
   */
  replace(styles = {}) {
    this.__setStyles(styles);
    return this;
  }

  /**
   *
   * @return {{low:string, middle:string, high:string}}
   */
  price() {
    return this._current.color.WidgetPriceGradient;
  }

}