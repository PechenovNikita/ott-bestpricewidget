const webpack = require('webpack')
  , autoprefixer = require('autoprefixer')
  , precss = require('precss');

module.exports = {

  entry     : {'libs' : './src/js/libs.js'},
  output    : {
    filename          : 'ott-libs.js',
    sourceMapFilename : 'ott-libs.map'
  },
  module    : {
    loaders : [
      {
        test    : /.jsx?$/,
        loader  : 'babel-loader',
        exclude : /node_modules/,

        query : {
          compact : false,
          presets : ['es2015', 'stage-0'],
          plugins : [
            // ["filter-imports", {
            //   "logger" : ["*", "default"],
            //   "Logger" : ["*", "default"]
            // }]
          ]
        }
      },
      {
        test   : /\.css$/,
        loader : "style-loader!css-loader!postcss-loader"
      }
    ]
  },
  postcss   : function () {
    return [autoprefixer, precss];
  },
  devtool   : 'source-map',
  externals : {
    // 'react-dom' : 'ReactDOM',
    // 'react'     : 'React',
    // 'pikaday'   : 'Pikaday',
    'loader' : 'Loader',
    'logger' : 'Logger'
    // 'stations'  : 'Stations'
  },
  plugins   : [
    new webpack.optimize.UglifyJsPlugin({
      compress : {
        warnings : true
      }
    })
  ],
  resolve   : {
    extensions : ['', '.js']
  }
};