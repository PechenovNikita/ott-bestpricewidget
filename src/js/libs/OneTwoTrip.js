'use strict';

import {jsonp, ajax} from './../mixins/Request';

import {deepExtend} from '../mixins/Deep';
import {byIATA as getByIATA} from '../libs/Reference';
import {byIATA as getCountryByIATA} from '../libs/reference/country';

import Format from '../mixins/Format';
import Params from '../config/_params';

import {DEALS as DEMO_DEALS, FLIGHTS as DEMO_FLIGHTS} from '../config/_demo';

/**
 * @typedef {Object}  OttFlightQuery
 * @property {string}   route - 1401MOWWAS
 * @property {string}   cs  - "E","B"
 */

/**
 *
 * @type {{string:string}}
 */
let ottSuggestPlacesCache = {};

/**
 * Поиск города по названию
 * @param findText
 * @param callback
 * @return {number|string}
 */
export function findPlaceAsync(findText, callback) {

  if (ottSuggestPlacesCache.hasOwnProperty(findText)) {
    return setTimeout(function () {
      callback(ottSuggestPlacesCache[findText], findText);
    }, 10);
  }

  return ajax('https://partner.onetwotrip.com/_api/avia/suggestPlaces', {
    data         : {
      query  : findText,
      marker : Params.get('marker')
    },
    responseType : 'json',
    success      : function (response) {
      ottSuggestPlacesCache[findText] = response;
      callback(response, findText);
    }
  });

  // return ajax(`https://partner.onetwotrip.com/_api/avia/suggestPlaces?query=${findText}`, function (response, id) {
  //   ottSuggestPlacesCache[findText] = response;
  //   callback(response, findText);
  // });
}

/**
 *
 * @param {OttDealsQuery} queryOptions
 * @param {RequestOptions} options
 * @return {number|string}
 */
export function bestDealsAsync(queryOptions, options) {
  options.data = deepExtend({}, queryOptions, {
    marker : Params.get('marker')
  });

  if (Params.get('demo')) {
    setTimeout(() => {
      options.success(DEMO_DEALS);
      options.always();
    }, 10);
    return 0;
  }

  return ajax('https://partner.onetwotrip.com/_api/deals_v4/directApiTop', options);
}
/**
 *
 * @param {OttFlightQuery} params
 * @param {RequestOptions} options
 * @return {number}
 */
export function searchingFlights(params, options,) {

  options.data = deepExtend({}, params, {
    ad        : 1,
    cn        : 0,
    'in'      : 0,
    marker    : Params.get('marker'),
    srcmarker : 'doinstant_ru'
  });

  if (Params.get('demo')) {
    setTimeout(() => {
      options.success(DEMO_FLIGHTS);
      options.always();
    }, 10);
    return 0;
  }

  return ajax('https://partner.onetwotrip.com/_api/searching/startSync', options);
}

/**
 *
 * @param {string} fromIata
 * @param {string} toIata
 * @param {Date} startDate
 * @param {Date} [endDate]
 * @param {string} [type=E]
 * @return {string}
 */
export function generateLink(fromIata, toIata, startDate, endDate = null, type = 'E') {

  let startDay = startDate.getDate()
    , startMonth = startDate.getMonth() + 1;

  startDay = startDay < 10 ? '0' + startDay : startDay;
  startMonth = startMonth < 10 ? '0' + startMonth : startMonth;

  let from = getByIATA(fromIata)
    , to = getByIATA(toIata);

  let fromName = from ? from.city.en : ''
    , toName = to ? to.city.en : '';

  let str = `${startDay}${startMonth}${fromIata}${toIata}`;

  if (endDate) {

    let endDay = endDate.getDate()
      , endMonth = endDate.getMonth() + 1;

    endDay = endDay < 10 ? '0' + endDay : endDay;
    endMonth = endMonth < 10 ? '0' + endMonth : endMonth;

    str += `${endDay}${endMonth}`;
  }

  if (type != 'E')
    str += `&${type}`;

  return `https://www.onetwotrip.com/ru/aviabilety/${fromName}-${toName}_${fromIata}-${toIata}/?s=true#${str}`;

}
/**
 *
 * @param {Query|{}} [query]
 * @param {Response|null} [response=null]
 * @return {string}
 */
export function link(query = {}, response = null) {
  if (query.$isDestinationCountry)
    return linkCountry(query);
  return linkCity(query, response);
}
/**
 *
 * @param {Query} query
 * @param {Response|null} response
 * @return {string}
 */
export function linkCity(query, response) {
  let params = query.$searchingQuery;

  let route, fromIata, toIata;

  if (params) {
    route = params.route;
    fromIata = query.$origin;
    toIata = query.$destinationCity;
  } else if (response) {
    route = response.$route;
    fromIata = response.$from;
    toIata = response.$to;
  } else {
    return 'javascript:void(0);';
  }

  let fromName = getByIATA(fromIata)
    , toName = getByIATA(toIata);

  return `https://www.onetwotrip.com/ru/aviabilety/${fromName}-${toName}_${fromIata}-${toIata}/?s=true#${route}`;
}
/**
 *
 * @param {Query} query
 * @return {string}
 */
export function linkCountry(query) {
  let country = getCountryByIATA(query.$destinationCountry);
  if (country) {
    return `https://www.onetwotrip.com/ru/aviabilety/to-${Format.url.country.low(country.country.en)}_${query.$destinationCountry}/`;
  }
  return 'javascript:void(0);';
}

/**
 * @typedef {Object}  DirectionOptions
 * @property {string}   airCompany    : "IK"
 * @property {boolean}  continued     : false
 * @property {string}   endDate       : "20161225"
 * @property {string}   endDateTime   : "2016-12-25T05:25:00.000Z"
 * @property {string}   endTerminal   : "1"
 * @property {string}   endTime       : "0825"
 * @property {string}   fic           : "OSALE"
 * @property {string}   flightNumber  : "8167"
 * @property {string}   flightTime    : "0120"
 * @property {string}   from          : "SVO"
 * @property {Object}   fromGeo       : Object
 * @property {string}   journeyTime   : "0120"
 * @property {string}   startDate     : "20161225"
 * @property {string}   startDateTime : "2016-12-25T04:05:00.000Z"
 * @property {string}   startTerminal : "D"
 * @property {string}   startTime     : "0705"
 * @property {string}   to            : "LED"
 * @property {Object}   toGeo         : Object
 */

/**
 * @typedef {Object}                    TripOptions
 * @property {string}                     currency    : "RUB"
 * @property {number}                     dateCreated : 1482178767364
 * @property {string}                     departDate  : "20161225"
 * @property {Array<DirectionOptions>[]}  directions
 * @property {string}                     fareType    : "direct" | "change"
 * @property {string}                     from        : "MOW"
 * @property {boolean}                    is2OW4RT    : false
 * @property {number}                     price       : 1470
 * @property {string}                     returnDate  : "20161225"
 * @property {string}                     to          : "LED"
 * @property {string}                     to_country  : "RU"
 */

/**
 * @typedef {Object}  OttSearchResponseTrip
 * @property {string}   airCmp -     "S7"
 * @property {string}   basis -    "WBSOW"
 * @property {string}   directionId -    "0"
 * @property {string}   endDate -    "2017-02-02T19:20:00"
 * @property {string}   fltNm -    "1027"
 * @property {string}   from -     "DME"
 * @property {string}   isDeparture -    "true"
 * @property {string}   jrnTm -    "0220"
 * @property {string}   key -    "170020170202DMEAER1027S7GH"
 * @property {string}   plane -    "738"
 * @property {string}   startDate -    "2017-02-02T17:00:00"
 * @property {string}   to -     "AER"
 */

/**
 * @typedef {Object}                        OttSearchResponseDirection
 * @property {Array<OttSearchResponseTrip>}   trips
 */
/**
 * @typedef {Object}                             SearchOptions
 * @property {string}                             airCmp  -  "S7"
 * @property {string}                             currency  -  "RUB"
 * @property {Array<OttSearchResponseDirection>}  dirs  -  Array[1]
 * @property {number}                             id  -  0
 * @property {string}                             key  - "170020170202DMEAER1027S7GH"
 * @property {string}                             price  - "1700"
 * @property {string}                             url  - "https://www.onetwotrip.com/ru/f/?env=7&meta_redirectid=1-1884-0-2#/book/42;-9q7r!;3fc29927-8c20-4977-b1ad-9741f78dd8b7;c165f39f-7cd6-42e2-9afc-ed5fca608c27;pGkB10wptEBDgoMZTJgvAg==;YcO5DcKvw5fDtcKTw51fwqXDqHwRJgPChDBXw5BswqJrPQh+GcOZPQXDmWXDui3ClcO+asOgw7zCusKGZybDiUMiB8KYA8KFw7PChcOfwqYrAw9WwqglaMKdBWHCi2jDkFbDhMKwPz%2FDuV1Hw6DCpSHDvMKLw5DCksO1MsKCCUjCusO7ZSXClTspw5F5wq4TQV15w5MHFhjCj8OWa8ORwq8VJcOWJmTDiifCrB8Rw64UKsOWRsORw6jDn3jCjsOqwolfW8OYw41yC8O+wqg9wprDpXbCocODcMOvw6gMF2rCtcOiFMO%2FRMK%2Fw5Z5w60=;;6e05+)D*u;0;RK;chhknkcvg;1,0,0;;tghgttcn;;;S;;m!,m!,1q,0,1q,1q,kk,m!,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1q,1q,RUB;h,RUB;;,,,;E;ryM6T5B:e5B*e1pbp%279$_s7_gh41027738_0PCn%2Fgt)8I*aI*6t)8t)4++++++++++++I(5I%27iI%27gI%2766_WE___WBSOW?meta=1"
 * @property {string}                             urlMob  -  "https://m.onetwotrip.com/ru/avia/b/?env=7&meta_redirectid=1-1884-0-2#/book/42;-9q7r!;3fc29927-8c20-4977-b1ad-9741f78dd8b7;c165f39f-7cd6-42e2-9afc-ed5fca608c27;pGkB10wptEBDgoMZTJgvAg==;YcO5DcKvw5fDtcKTw51fwqXDqHwRJgPChDBXw5BswqJrPQh+GcOZPQXDmWXDui3ClcO+asOgw7zCusKGZybDiUMiB8KYA8KFw7PChcOfwqYrAw9WwqglaMKdBWHCi2jDkFbDhMKwPz%2FDuV1Hw6DCpSHDvMKLw5DCksO1MsKCCUjCusO7ZSXClTspw5F5wq4TQV15w5MHFhjCj8OWa8ORwq8VJcOWJmTDiifCrB8Rw64UKsOWRsORw6jDn3jCjsOqwolfW8OYw41yC8O+wqg9wprDpXbCocODcMOvw6gMF2rCtcOiFMO%2FRMK%2Fw5Z5w60=;;6e05+)D*u;0;RK;chhknkcvg;1,0,0;;tghgttcn;;;S;;m!,m!,1q,0,1q,1q,kk,m!,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1q,1q,RUB;h,RUB;;,,,;E;ryM6T5B:e5B*e1pbp%279$_s7_gh41027738_0PCn%2Fgt)8I*aI*6t)8t)4++++++++++++I(5I%27iI%27gI%2766_WE___WBSOW?meta=1
 */
/**
 *
 * @param   {SearchOptions} fare
 * @return  {TripOptions}
 */

export function convertSearchFareToBestDeal(fare) {
  /**
   *
   * @type {TripOptions}
   */
  let ret = {
    price    : parseInt(fare.price, 10),
    currency : fare.currency
  };

  ret.directions = fare.dirs.map(function (/*OttSearchResponseDirection*/direction, x) {
    return direction.trips.map(function (/*OttSearchResponseTrip*/trip, y) {
      /**
       * @type {{year: number, month: number, day: number, hours: number, minutes: number, seconds: number}}
       */
      let parsedStartDate = parseDateString(trip.startDate)
        , parsedEndDate = parseDateString(trip.endDate);

      if (x == 0 && y == 0) {
        ret.departDate = `${parsedStartDate.year}${parsedStartDate.month}${parsedStartDate.day}`;
      }

      if (y == 0) {
        ret.returnDate = `${parsedStartDate.year}${parsedStartDate.month}${parsedStartDate.day}`;
      }

      // if (x == 1 && y == (trips.length - 1)) {
      //   ret.returnDate = `${parsedEndDate.year}${parsedEndDate.month}${parsedEndDate.day}`
      // }
      /**
       *
       * @type {DirectionOptions}
       */
      return {

        airCompany   : trip.airCmp,
        // continued     : false,
        endDate      : `${parsedEndDate.year}${parsedEndDate.month}${parsedEndDate.day}`,
        // endDateTime   : "2016-12-25T05:25:00.000Z",
        // endTerminal   : "1 ",
        endTime      : `${parsedEndDate.hours}${parsedEndDate.minutes}`,//"0825",
        // fic           : "OSALE",
        flightNumber : trip.fltNm,
        flightTime   : trip.jrnTm,//"0120"
        from         : trip.from,
        // fromGeo : Object,
        journeyTime  : trip.jrnTm,
        startDate    : `${parsedStartDate.year}${parsedStartDate.month}${parsedStartDate.day}`,
        // startDateTime : "2016-12-25T04:05:00.000Z",
        // startTerminal : "D",
        startTime    : `${parsedStartDate.hours}${parsedStartDate.minutes}`,//"0825",
        to           : trip.to,
        // toGeo : Object
      };
    });
  });

  return ret;
}
/**
 *
 * @param {string} key - 170020170202DMEAER1027S7GH = 17:00 2017-02-02 DME_AER 10:27 S7 GH
 * @param {string} dateString - 2017-02-02T17:00:00
 *
 * @return {{year: number, month: number, day: number, hours: number, minutes: number, seconds: number}}
 */
const parseDateString = (function () {
  // const regExp = /([\d]{2})([\d]{2})([\d]{4})([\d]{2})([\d]{2})[^\d]+([\d]{2})([\d]{2})/g;
  const regDateString = /([\d]{4})-([\d]{2})-([\d]{2})T([\d]{2}):([\d]{2}):([\d]{2})/;
  return function (dateString) {
    let match = regDateString.exec(dateString);

    return {
      year  : match[1],
      month : match[2],
      day   : match[3],

      hours   : match[4],
      minutes : match[5],
      seconds : match[6],
    };
  };
})();

export default {
  findPlaceAsync : findPlaceAsync,
  bestDealsAsync : bestDealsAsync
};