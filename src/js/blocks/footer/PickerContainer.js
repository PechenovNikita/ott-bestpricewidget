'use strict';

import DatePicker from './../../components/DatePicker';
import {CONTAINER_SIZE_MEDIUM, CONTAINER_SIZE_EXTRA_SMALL, CONTAINER_SIZE_SMALL} from '../../config/_constant';

import classList from '../../mixins/polyfill/element/classList';

import {docCreate} from './../../mixins/Element';

export default class PickerContainer {
  /**
   *
   * @param container
   * @param {EventStore} eventStore
   * @param {Theme} theme
   */
  constructor(container, eventStore, theme) {
    this._eventStore = eventStore;
    this._theme = theme;

    this._container = container;

    this._mini = false;
    this._open = false;

    this.__initHTML().__initEvents();

    this._container.appendChild(this._node);

  }

  /**
   *
   * @return {PickerContainer}
   * @private
   */
  __initHTML() {
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._node = docCreate('div', 'ottbp-container-picker');

    let _wrapper = docCreate('div', 'ottbp-container-picker__wrap');

    let _btn = docCreate('div', 'ottbp-container-picker__btn');
    this._containerPicker = docCreate('div', 'ottbp-container-picker__picker');

    this._btn = docCreate('button', 'ottbp-container-picker-btn');
    this._btnClean = docCreate('button', 'ottbp-container-picker-btn ottbp-container-picker-btn--clean');
    this._btnClean.innerHTML = 'сбросить';
    this._btn.innerText = 'Выбрать дату';

    this._btn.setAttribute('style', 'width:100% !important; border-radius: 2px !important;');
    this._btnClean.setAttribute('style', 'width:0 !important;');

    _btn.appendChild(this._btn);
    _btn.appendChild(this._btnClean);

    _wrapper.appendChild(_btn);
    _wrapper.appendChild(this._containerPicker);
    /**
     *
     * @type {DatePicker}
     * @private
     */
    this._picker = new DatePicker(this._containerPicker, this._eventStore, this._theme);

    this._node.appendChild(_wrapper);

    return this;
  }

  /**
   *
   * @return {PickerContainer}
   * @private
   */
  __initEvents() {

    // Eve.addEvent(this._btn, 'click', ::this.__onClickBtn_v2);
    // Eve.addEvent(this._btnClean, 'click', ::this.__onClean);

    this._eventStore.delegateDocument('click', this._node, (event) => {
      let target = event.detail.original.target;

      while (target != this._node) {
        if (target === this._btn)
          this.__onClickBtn_v2(event.detail.original);
        else if (target === this._btnClean)
          this.__onClean(event.detail.original);
        target = target.parentNode;
      }
    }, ::this.__onClickAnyWhere);

    this._eventStore.on('DatePicker::title:change', ::this.__onChangeText);
    return this;
  }

  __onChangeText(event) {
    let text = event.detail.text;
    this._canRemove = event.detail.canRemove;

    this._btn.innerHTML = text;

    this.render();
  }

  __onClickAnyWhere() {

    if (this._mini) {
      this._open = false;
      this.render();
    }

  }

  __onClickBtn_v2(event) {

    event.stopPropagation();
    event.preventDefault();

    if (this._mini) {
      this._open = !this._open;
    }
    this.render();

  }

  __onClean() {
    this._eventStore.dispatch('DatePicker::do:clean');
  }

  /**
   *
   * @return {boolean}
   */
  get $isLoading() {
    return this._picker.$isLoading;
  }

  /**
   *
   * @return {{start: ?Date, inMonth: ({year: number, month: number}|false), end: ?Date}}
   */
  get $value() {
    return this._picker.$value;
  }

  /**
   *
   * @return {number}
   */
  get $height() {
    if (this._mini)
      return this._node.offsetHeight;
    return this._picker.$height;
  }

  render() {
    if (this._mini) {
      if (this._open) {
        this._containerPicker.setAttribute('style', 'display: block !important;');
        classList(this._btn).add('ottbp-container-picker-btn--focus');
      } else {
        this._containerPicker.setAttribute('style', '');
        classList(this._btn).remove('ottbp-container-picker-btn--focus');
      }
    } else {
      classList(this._btn).remove('ottbp-container-picker-btn--focus');
      this._containerPicker.setAttribute('style', '');
    }

    const rad = '2px';

    if (this._canRemove) {
      let w1 = (this._extraSmall ? 70 : ( this._small ? 77 : 85 ))
        , w2 = 100 - w1;
      this._btn.setAttribute('style', `width:${w1}%!important; border-radius: ${this._open ? `${rad} 0 0 0` : `${rad} 0 0 ${rad}`}!important; ${this._open ? 'box-shadow: inset 1px 1px 0 0 #E0E0E0, inset 0 -1px 0 0 #E0E0E0!important;' : ''}`);
      this._btnClean.setAttribute('style', `width:${w2}%!important; border-radius: ${this._open ? `0 ${rad} 0 0` : `0 ${rad} ${rad} 0`}!important; ${this._open ? 'box-shadow: inset -1px 1px 0 0 #E0E0E0, inset 0 -1px 0 0 #E0E0E0!important;' : ''}`);
    } else {
      this._btn.setAttribute('style', `width:100%!important; border-radius: ${this._open ? `${rad} ${rad} 0 0` : `${rad}`}!important; ${this._open ? 'box-shadow: inset 0 0 0 1px #E0E0E0!important;' : ''}`);
      this._btnClean.setAttribute('style', 'width:0% !important;');
    }

    return this;
  }

  resize(containerWidth) {
    if (containerWidth >= CONTAINER_SIZE_MEDIUM) {
      this._mini = false;
      this._open = false;
    } else {

      this._extraSmall = containerWidth < CONTAINER_SIZE_EXTRA_SMALL;
      this._small = containerWidth < CONTAINER_SIZE_SMALL;

      this._mini = true;
    }

    this.render();
  }

  /**
   *
   * @param {FormatMainForm} options
   */
  update(options) {
    this._picker.updateValue(options);
  }
}
