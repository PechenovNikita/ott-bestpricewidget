'use strict';

import {deepExtend} from './Deep';

/**
 *
 * @param {string} innerSVG
 * @param {Object.<string,string>} [attrs = []]
 * @return {string}
 */
function createSVGString(innerSVG, attrs = {}) {

  let attr = Object.keys(attrs).reduce((last, key)=>{
    if (attrs[key])
      return last += `${key}="${attrs[key]}" `;
    return last;
  }, 'xmlns="http://www.w3.org/2000/svg"'+ ' ');

  // let attr = reduce(Object.keys(attrs), (last, key) => {
  //   if (attrs[key])
  //     return last += `${key}="${attrs[key]}" `;
  //   return last;
  // }, 'xmlns="http://www.w3.org/2000/svg" ');

  return `<svg ${attr}>${innerSVG}</svg>`;
}

// /**
//  *
//  * @param {string} innerSVG
//  * @param {Object.<string,string>} [attrs = {}]
//  * @return {Document}
//  */
// function createSVG(innerSVG, attrs = {}) {
//   let domParser = new DOMParser();
//   return domParser.parseFromString(createSVGString(innerSVG, attrs), 'application/xml');
// }
//
// /**
//  *
//  * @param {Document} svgDocument
//  * @param {HTMLElement} parentNode
//  */
// function appendSVG(svgDocument, parentNode) {
//   parentNode.appendChild(parentNode.ownerDocument.importNode(svgDocument.documentElement, true));
// }

export default class SVG {
  constructor() {
    this._inner = '';
    this._attr = {};
  }

  /**
   *
   * @param {Object} attrs
   * @return {SVG}
   */
  attr(attrs) {
    this._attr = deepExtend({}, this._attr, attrs);
    return this.update();
  }

  /**
   *
   * @return {SVG}
   */
  update() {
    this._inner = '';
    return this.render();
  }

  /**
   *
   * @return {SVG}
   */
  render() {

    // this._document = createSVG(this._inner, this._attr);

    // Object.keys(this._attr).forEach(function (key) {
    //   if (this._attr[key])
    //     this._node.setAttribute(key, this._attr[key]);
    //   else
    //     this._node.removeAttribute(key);
    // }, this);
    //
    // this._node.innerHTML = this.inner;
    return this;
  }

  /**
   *
   * @return {string}
   */
  toString() {
    return createSVGString(this._inner, this._attr);
  }

}

