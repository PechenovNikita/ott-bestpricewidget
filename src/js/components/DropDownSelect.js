'use strict';
import {docCreate, classAddRemove} from './../mixins/Element';
import DropDownHelper from './dropDownSelect/helper';
import classList from '../mixins/polyfill/element/classList';

export default class DropDownSelect {
  /**
   *
   * @param {DropDownSelectOptions[]} [items]
   * @param {number} [selected]
   * @param {HTMLElement} container
   * @param {Object} [options={}]
   * @param {string} [options.name]
   * @param {boolean|undefined} [options.disabled]
   * @param {EventStore} eventStore
   */
  constructor(items = [], selected = 0, container, options = {}, eventStore) {
    this._eventStore = eventStore;
    this._container = container;
    /**
     *
     * @type {DropDownSelectOptions[]}
     * @private
     */
    this._items = items || [];
    /**
     *
     * @type {number}
     * @private
     */
    this._selected = selected;
    /**
     *
     * @type {boolean}
     * @private
     */
    this._disabled = !!options.disabled;
    /**
     *
     * @type {?string}
     * @private
     */
    this._name = options.name || null;

    this.initHTML().initEvents();
  }

  initHTML() {
    // .ottbp-select(class=(selected ? 'selected' : ''))
    //   button.ottbp-select__btn.ottbp-texts--btn
    //    span.ottbp-select__btn__inner!= value
    //    .ottbp-select__helper

    let item = this._items[this._selected];
    this._node = docCreate('div', ['ottbp-select', (item.orange ? 'ottbp-selected' : '')]);

    this.__initButtonHTML().__initHelperHTML();

    this._container.appendChild(this._node);

    return this;
  }

  /**
   *
   * @return {DropDownSelect}
   * @private
   */
  __initClassHTML() {
    let item = this._items[this._selected];

    let add = [], remove = [];
    item.orange ? add.push('ottbp-selected') : remove.push('ottbp-selected');
    this._disabled ? add.push('ottbp-disabled') : remove.push('ottbp-disabled');
    classAddRemove(this._node, add, remove);

    return this;
  }

  /**
   *
   * @return {DropDownSelect}
   * @private
   */
  __initButtonHTML() {
    let btn = docCreate('button', 'ottbp-select__btn ottbp-texts--btn');
    this._btnInner = docCreate('span', 'ottbp-select__btn__inner');

    btn.appendChild(this._btnInner);

    this._node.appendChild(btn);
    return this.__initButtonInnerHTML();
  }

  /**
   *
   * @return {DropDownSelect}
   * @private
   */
  __initButtonInnerHTML() {
    let item = this._items[this._selected];
    this._btnInner.innerHTML = `${(item ? (item.title || '') : '')}`;
    return this;
  }

  __initHelperHTML() {
    this._helperNode = docCreate('div', 'ottbp-select__helper');
    this._node.appendChild(this._helperNode);
    return this.__initHelper();
  }

  __initHelper() {
    if (this._items.length > 2) {
      this._helper = new DropDownHelper(this._items, this._selected, this._helperNode, this._eventStore);
      this._helper.whenSelect(this.onSelectFromList.bind(this));
    }
    return this;
  }

  onSelectFromList(index) {

    this.hideHelper();
    if (this._selected == index)
      return this;
    this._selected = index;
    this.onSelect();
    this.__initButtonInnerHTML().__initClassHTML();
  }

  initEvents() {
    // Eve.addEvent(this._node, 'click', ::this.__onClick);
    this._eventStore.delegateDocument('click', this._node, ::this.__onClick, () => {
      this.hideHelper();
    });
  }

  __onClick() {
    // event.stopPropagation();
    // event.preventDefault();

    if (this._disabled)
      return;

    if (this._items.length <= 2)
      this.toggle();
    else {
      this.toggleHelper();
    }
  }

  toggle() {
    if (this._selected == 0)
      this._selected = 1;
    else
      this._selected = 0;

    this.onSelect();

    return this.__initButtonInnerHTML().__initClassHTML();
  }

  toggleHelper() {
    if (this._helperDisplay)
      this.hideHelper();
    else
      this.showHelper();
  }

  showHelper() {
    classList(this._node).add('ottbp--open');
    this._helperNode.setAttribute('style', '');
    this._helperDisplay = true;
    return this;
  }

  hideHelper() {
    this._helperNode.setAttribute('style', 'display: none !important;');
    this._helperDisplay = false;
    return this;
  }

  /**
   *
   * @param callback
   * @return {DropDownSelect}
   */
  whenSelect(callback) {
    this._whenSelect = callback;
    return this;
  }

  /**
   *
   */
  onSelect() {
    if (this._whenSelect)
      this._whenSelect(this.$value);
  }

  /**
   *
   * @return {DropDownSelect}
   */
  disable() {
    if (this._disabled)
      return this;
    this._disabled = true;
    return this.__initClassHTML();
  }

  /**
   *
   * @return {DropDownSelect}
   */
  enable() {
    if (!this._disabled)
      return this;
    this._disabled = false;
    return this.__initClassHTML();
  }

  // /**
  //  *
  //  * @return {?string}
  //  */
  // get $name() {
  //   return this._name;
  // }

  /**
   *
   * @return {DropDownSelectOptions}
   */
  get $value() {
    return this._items[this._selected];
  }

  /**
   *
   * @param newValue
   */
  update(newValue) {
    this._items.forEach((item, index) => {
      if (item.value === newValue) {
        this.onSelectFromList(index);
      }
    });
  }

}