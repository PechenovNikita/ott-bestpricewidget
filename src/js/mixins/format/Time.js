'use strict';

import l8n from '../../store/l8n';
/**
 *
 * @param {string} timeString = 1010
 * @return {string} = 10:10
 */
export function toColon(timeString) {
  let time = split(timeString);
  return `${time[0]}:${time[1]}`;
}

/**
 *
 * @param {string} timeString = 1010
 * @return {string} = {10 ч 10  мин}
 */
export function toFormat(timeString) {
  let time = split(timeString);
  let hour = 0, min = 0;
  if (time.length > 1) {
    hour = parseInt(time[0], 10);
    min = parseInt(time[1]);
  }

  if (hour)
    return `${hour} ${l8n.Direction.time.hour} ${min} ${l8n.Direction.time.min}`;
  return `${min} ${l8n.Direction.time.min}`;
}

/**
 *
 * @param {string} timeString = 1010
 * @return {Array.<number>|Boolean} = ['10', '10']
 */
export function split(timeString) {
  return timeString.match(/.{1,2}/g);
}

/**
 *
 * @param {string} timeString = 1010
 * @return {number} = 36600
 */
export function toSeconds(timeString) {
  let time = split(timeString);
  return parseInt(time[0], 10) * 60 * 60 + parseInt(time[1], 10) * 60;
}
