var OTTBPWidget =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/*eslint-env node*/
	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	__webpack_require__(1);
	
	__webpack_require__(5);
	
	var _Element = __webpack_require__(7);
	
	var _Deep = __webpack_require__(9);
	
	var _default = __webpack_require__(21);
	
	var _params = __webpack_require__(8);
	
	var _params2 = _interopRequireDefault(_params);
	
	var _Controller = __webpack_require__(15);
	
	var _Controller2 = _interopRequireDefault(_Controller);
	
	var _Controller3 = __webpack_require__(45);
	
	var _Controller4 = _interopRequireDefault(_Controller3);
	
	var _form = __webpack_require__(48);
	
	var _form2 = _interopRequireDefault(_form);
	
	var _Theme_v = __webpack_require__(87);
	
	var _Theme_v2 = _interopRequireDefault(_Theme_v);
	
	var _Event_v = __webpack_require__(88);
	
	var _Event_v2 = _interopRequireDefault(_Event_v);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Widget = function () {
	  /**
	   *
	   * @param {HTMLElement} contianer
	   * @param {{marker:string, type:string}} params
	   * @param {ThemeOptions} [theme]
	   * @param {FormatMainForm} [data]
	   */
	  function Widget(contianer, params) {
	    var theme = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	    var data = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
	
	    _classCallCheck(this, Widget);
	
	    this._container = contianer;
	
	    this._themeOptions = theme;
	    this._data = (0, _Deep.deepExtend)({}, _default.defaultFormData, data);
	
	    _params2["default"].init(params);
	
	    this.__init();
	  }
	
	  _createClass(Widget, [{
	    key: '__init',
	    value: function __init() {
	      this._node = (0, _Element.docCreate)('div', 'ottbp-widget');
	      this._container.appendChild(this._node);
	      /**
	       *
	       * @type {EventStore}
	       * @private
	       */
	      this._eventStore = new _Event_v2["default"]();
	
	      if (_params2["default"].get('demo')) {
	        /**
	         *
	         * @type {Controller}
	         * @private
	         */
	        this._store = new _Controller4["default"](this._eventStore);
	      } else {
	        this._store = new _Controller2["default"](this._eventStore);
	      }
	      this._theme = new _Theme_v2["default"]();
	      this._theme.init(this._themeOptions);
	      /**
	       *
	       * @type {Form}
	       * @private
	       */
	      this._form = new _form2["default"](this._node, this._eventStore, this._theme);
	
	      this._form.update(this._data);
	    }
	
	    /**
	     *
	     * @param {ThemeOptions} theme
	     * @return {Widget}
	     */
	
	  }, {
	    key: 'theme',
	    value: function theme(_theme) {
	      this._theme.replace(_theme);
	      this._form.resize();
	      return this;
	    }
	
	    /**
	     *
	     * @param {ThemeColor} colors
	     * @return {Widget}
	     */
	
	  }, {
	    key: 'colors',
	    value: function colors(_colors) {
	
	      this.theme({ color: _colors });
	      return this;
	    }
	
	    /**
	     *
	     * @param width
	     * @return {Widget}
	     */
	
	  }, {
	    key: 'width',
	    value: function width(_width) {
	      if (_width || _width === 0) {
	        _width = parseInt(_width, 10);
	        if (_width < 320) _width = 320;
	        this.theme({ width: _width });
	      } else {
	        this.theme({ width: null });
	      }
	      return this;
	    }
	
	    /**
	     *
	     * @param {FormatMainForm} data
	     * @return {Widget}
	     */
	
	  }, {
	    key: 'update',
	    value: function update(data) {
	      this._data = (0, _Deep.deepExtend)({}, this._data, data);
	      this._form.update(this._data);
	      return this;
	    }
	  }]);
	
	  return Widget;
	}();
	
	module.exports = Widget;

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(2);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(4)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/postcss-loader/index.js!./fonts.css", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/postcss-loader/index.js!./fonts.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(3)();
	// imports
	exports.push([module.id, "@import url(https://fonts.googleapis.com/css?family=Roboto:400,500&subset=latin,latin-ext,cyrillic-ext,cyrillic);", ""]);
	
	// module
	exports.push([module.id, "\n", ""]);
	
	// exports


/***/ },
/* 3 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(true) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(6);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(4)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/postcss-loader/index.js!./index.css", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/postcss-loader/index.js!./index.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(3)();
	// imports
	
	
	// module
	exports.push([module.id, ".ottbp-widget{font-size:medium!important;line-height:1!important;direction:ltr!important;text-align:left!important;text-align:start!important;font-family:Roboto,Roboto-Regular,sans-serif!important;color:#000!important;font-style:normal!important;font-weight:400!important;text-decoration:none!important;list-style-type:disc!important;background-attachment:scroll!important;background-color:transparent!important;background-image:none!important;background-position:0 0!important;background-repeat:repeat!important;border-style:none!important;border-width:medium!important;bottom:auto!important;clear:none!important;clip:auto!important;counter-increment:none!important;counter-reset:none!important;cursor:auto!important;display:inline!important;float:none!important;font-variant:normal!important;height:auto!important;left:auto!important;letter-spacing:normal!important;list-style-position:outside!important;list-style-image:none!important;margin:0!important;max-height:none!important;max-width:none!important;min-height:0!important;min-width:0!important;opacity:1;outline:invert none medium!important;overflow:visible!important;padding:0!important;position:static!important;quotes:\"\" \"\"!important;right:auto!important;table-layout:auto!important;text-align:inherit!important;text-decoration:inherit!important;text-indent:0!important;text-transform:none!important;top:auto!important;unicode-bidi:normal!important;vertical-align:baseline!important;visibility:inherit!important;white-space:normal!important;width:auto!important;word-spacing:normal!important;z-index:auto!important;background-origin:padding-box!important;background-clip:border-box!important;background-size:auto!important;-o-border-image:none!important;border-image:none!important;border-radius:0!important;box-shadow:none!important;box-sizing:content-box!important;-webkit-column-count:auto!important;-moz-column-count:auto!important;column-count:auto!important;-webkit-column-gap:normal!important;-moz-column-gap:normal!important;column-gap:normal!important;-webkit-column-rule:medium none #000!important;-moz-column-rule:medium none #000!important;column-rule:medium none #000!important;-webkit-column-span:1!important;-moz-column-span:1!important;column-span:1!important;-webkit-column-width:auto!important;-moz-column-width:auto!important;column-width:auto!important;-webkit-font-feature-settings:normal!important;font-feature-settings:normal!important;overflow-x:visible!important;overflow-y:visible!important;-webkit-hyphens:manual!important;-ms-hyphens:manual!important;hyphens:manual!important;-webkit-perspective:none!important;-ms-perspective:none!important;-o-perspective:none!important;perspective:none!important;-webkit-perspective-origin:50% 50%!important;-ms-perspective-origin:50% 50%!important;-o-perspective-origin:50% 50%!important;perspective-origin:50% 50%!important;-webkit-backface-visibility:visible!important;backface-visibility:visible!important;text-shadow:none!important;-webkit-transition:all 0s ease 0s!important;transition:all 0s ease 0s!important;-webkit-transform:none!important;transform:none!important;-webkit-transform-origin:50% 50%!important;transform-origin:50% 50%!important;-webkit-transform-style:flat!important;transform-style:flat!important;word-break:normal!important}.ottbp-widget a, .ottbp-widget abbr, .ottbp-widget acronym, .ottbp-widget address, .ottbp-widget applet, .ottbp-widget area, .ottbp-widget article, .ottbp-widget aside, .ottbp-widget audio, .ottbp-widget b, .ottbp-widget big, .ottbp-widget blockquote, .ottbp-widget button, .ottbp-widget canvas, .ottbp-widget caption, .ottbp-widget cite, .ottbp-widget code, .ottbp-widget col, .ottbp-widget colgroup, .ottbp-widget datalist, .ottbp-widget dd, .ottbp-widget del, .ottbp-widget dfn, .ottbp-widget div, .ottbp-widget dl, .ottbp-widget dt, .ottbp-widget em, .ottbp-widget fieldset, .ottbp-widget figcaption, .ottbp-widget figure, .ottbp-widget footer, .ottbp-widget form, .ottbp-widget h1, .ottbp-widget h2, .ottbp-widget h3, .ottbp-widget h4, .ottbp-widget h5, .ottbp-widget h6, .ottbp-widget header, .ottbp-widget hr, .ottbp-widget i, .ottbp-widget iframe, .ottbp-widget img, .ottbp-widget input, .ottbp-widget ins, .ottbp-widget kbd, .ottbp-widget label, .ottbp-widget legend, .ottbp-widget li, .ottbp-widget main, .ottbp-widget map, .ottbp-widget mark, .ottbp-widget menu, .ottbp-widget meta, .ottbp-widget nav, .ottbp-widget object, .ottbp-widget ol, .ottbp-widget optgroup, .ottbp-widget option, .ottbp-widget output, .ottbp-widget p, .ottbp-widget pre, .ottbp-widget progress, .ottbp-widget q, .ottbp-widget samp, .ottbp-widget section, .ottbp-widget select, .ottbp-widget small, .ottbp-widget span, .ottbp-widget strike, .ottbp-widget strong, .ottbp-widget sub, .ottbp-widget summary, .ottbp-widget sup, .ottbp-widget table, .ottbp-widget tbody, .ottbp-widget td, .ottbp-widget textarea, .ottbp-widget tfoot, .ottbp-widget th, .ottbp-widget thead, .ottbp-widget time, .ottbp-widget tr, .ottbp-widget tt, .ottbp-widget ul, .ottbp-widget var, .ottbp-widget video{background-attachment:scroll!important;background-color:transparent!important;background-image:none!important;background-position:0 0!important;background-repeat:repeat!important;border-color:#000!important;border-color:currentColor!important;border-style:none!important;border-width:medium!important;bottom:auto!important;clear:none!important;clip:auto!important;color:inherit!important;counter-increment:none!important;counter-reset:none!important;cursor:auto!important;direction:inherit!important;display:inline!important;float:none!important;font-family:inherit!important;font-size:inherit!important;font-style:inherit!important;font-variant:normal!important;font-weight:inherit!important;height:auto!important;left:auto!important;letter-spacing:normal!important;line-height:inherit!important;list-style-type:inherit!important;list-style-position:outside!important;list-style-image:none!important;margin:0!important;max-height:none!important;max-width:none!important;min-height:0!important;min-width:0!important;opacity:1;outline:invert none medium!important;overflow:visible!important;padding:0!important;position:static!important;quotes:\"\" \"\"!important;right:auto!important;table-layout:auto!important;text-align:inherit!important;text-decoration:inherit!important;text-indent:0!important;text-transform:none!important;top:auto!important;unicode-bidi:normal!important;vertical-align:baseline!important;visibility:inherit!important;white-space:normal!important;width:auto!important;word-spacing:normal!important;z-index:auto!important;background-origin:padding-box!important;background-clip:border-box!important;background-size:auto!important;-o-border-image:none!important;border-image:none!important;border-radius:0!important;box-shadow:none!important;box-sizing:content-box!important;-webkit-column-count:auto!important;-moz-column-count:auto!important;column-count:auto!important;-webkit-column-gap:normal!important;-moz-column-gap:normal!important;column-gap:normal!important;-webkit-column-rule:medium none #000!important;-moz-column-rule:medium none #000!important;column-rule:medium none #000!important;-webkit-column-span:1!important;-moz-column-span:1!important;column-span:1!important;-webkit-column-width:auto!important;-moz-column-width:auto!important;column-width:auto!important;-webkit-font-feature-settings:normal!important;font-feature-settings:normal!important;overflow-x:visible!important;overflow-y:visible!important;-webkit-hyphens:manual!important;-ms-hyphens:manual!important;hyphens:manual!important;-webkit-perspective:none!important;-ms-perspective:none!important;-o-perspective:none!important;perspective:none!important;-webkit-perspective-origin:50% 50%!important;-ms-perspective-origin:50% 50%!important;-o-perspective-origin:50% 50%!important;perspective-origin:50% 50%!important;-webkit-backface-visibility:visible!important;backface-visibility:visible!important;text-shadow:none!important;-webkit-transition:all 0s ease 0s!important;transition:all 0s ease 0s!important;-webkit-transform:none!important;transform:none!important;-webkit-transform-origin:50% 50%!important;transform-origin:50% 50%!important;-webkit-transform-style:flat!important;transform-style:flat!important;word-break:normal!important}.ottbp-widget, .ottbp-widget address, .ottbp-widget article, .ottbp-widget audio, .ottbp-widget blockquote, .ottbp-widget caption, .ottbp-widget colgroup, .ottbp-widget dd, .ottbp-widget dialog, .ottbp-widget div, .ottbp-widget dl, .ottbp-widget dt, .ottbp-widget fieldset, .ottbp-widget figure, .ottbp-widget footer, .ottbp-widget form, .ottbp-widget h1, .ottbp-widget h2, .ottbp-widget h3, .ottbp-widget h4, .ottbp-widget h5, .ottbp-widget h6, .ottbp-widget header, .ottbp-widget hgroup, .ottbp-widget hr, .ottbp-widget main, .ottbp-widget menu, .ottbp-widget nav, .ottbp-widget ol, .ottbp-widget option, .ottbp-widget p, .ottbp-widget pre, .ottbp-widget progress, .ottbp-widget section, .ottbp-widget summary, .ottbp-widget ul, .ottbp-widget video{display:block!important}.ottbp-widget h1, .ottbp-widget h2, .ottbp-widget h3, .ottbp-widget h4, .ottbp-widget h5, .ottbp-widget h6{font-weight:700!important}.ottbp-widget h1{font-size:2em!important;padding:.67em 0!important}.ottbp-widget h2{font-size:1.5em!important;padding:.83em 0!important}.ottbp-widget h3{font-size:1.17em!important;padding:.83em 0!important}.ottbp-widget h4{font-size:1em!important}.ottbp-widget h5{font-size:.83em!important}.ottbp-widget p{margin:1em 0!important}.ottbp-widget table{display:table!important}.ottbp-widget thead{display:table-header-group!important}.ottbp-widget tbody{display:table-row-group!important}.ottbp-widget tfoot{display:table-footer-group!important}.ottbp-widget tr{display:table-row!important}.ottbp-widget td, .ottbp-widget th{display:table-cell!important;padding:2px!important}.ottbp-widget ol, .ottbp-widget ul{margin:1em 0!important;-webkit-padding-start:0!important}.ottbp-widget ol li, .ottbp-widget ol ol li, .ottbp-widget ol ol ol li, .ottbp-widget ol ol ul li, .ottbp-widget ol ul ul li, .ottbp-widget ul li, .ottbp-widget ul ol ol li, .ottbp-widget ul ul li, .ottbp-widget ul ul ol li, .ottbp-widget ul ul ul li{list-style-position:inside!important;margin-top:.08em!important}.ottbp-widget ol ol, .ottbp-widget ol ol ol, .ottbp-widget ol ol ul, .ottbp-widget ol ul, .ottbp-widget ol ul ul, .ottbp-widget ul ol, .ottbp-widget ul ol ol, .ottbp-widget ul ul, .ottbp-widget ul ul ol, .ottbp-widget ul ul ul{padding-left:40px!important;margin:0!important}.ottbp-widget nav ol, .ottbp-widget nav ul{list-style-type:none!important}.ottbp-widget menu, .ottbp-widget ul{list-style-type:disc!important}.ottbp-widget ol{list-style-type:decimal!important}.ottbp-widget menu menu, .ottbp-widget menu ul, .ottbp-widget ol menu, .ottbp-widget ol ul, .ottbp-widget ul menu, .ottbp-widget ul ul{list-style-type:circle!important}.ottbp-widget menu menu menu, .ottbp-widget menu menu ul, .ottbp-widget menu ol menu, .ottbp-widget menu ol ul, .ottbp-widget menu ul menu, .ottbp-widget menu ul ul, .ottbp-widget ol menu menu, .ottbp-widget ol menu ul, .ottbp-widget ol ol menu, .ottbp-widget ol ol ul, .ottbp-widget ol ul menu, .ottbp-widget ol ul ul, .ottbp-widget ul menu menu, .ottbp-widget ul menu ul, .ottbp-widget ul ol menu, .ottbp-widget ul ol ul, .ottbp-widget ul ul menu, .ottbp-widget ul ul ul{list-style-type:square!important}.ottbp-widget li{display:list-item!important;min-height:auto!important;min-width:auto!important;padding-left:20px!important}.ottbp-widget strong{font-weight:700!important}.ottbp-widget em{font-style:italic!important}.ottbp-widget code, .ottbp-widget kbd, .ottbp-widget pre, .ottbp-widget samp{font-family:monospace!important}.ottbp-widget a{color:#00f!important;text-decoration:underline!important}.ottbp-widget a:visited{color:#529!important}.ottbp-widget a, .ottbp-widget a *, .ottbp-widget button, .ottbp-widget input[type=button], .ottbp-widget input[type=checkbox], .ottbp-widget input[type=radio], .ottbp-widget input[type=submit], .ottbp-widget select{cursor:pointer!important}.ottbp-widget button, .ottbp-widget input[type=submit]{text-align:center!important;padding:2px 6px 3px!important;border-radius:4px!important;text-decoration:none!important;font-family:arial,helvetica,sans-serif!important;font-size:small!important;background:#fff!important;-webkit-appearance:push-button!important;color:buttontext!important;border:1px #a6a6a6 solid!important;background:#d3d3d3!important;background:#fff;background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#fff),color-stop(100%,#ddd),color-stop(100%,#d1d1d1),color-stop(100%,#ddd))!important;background:-webkit-linear-gradient(top,#fff 0,#ddd 100%,#d1d1d1 100%,#ddd 100%)!important;background:linear-gradient(to bottom,#fff 0,#ddd 100%,#d1d1d1 100%,#ddd 100%)!important;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#dddddd', GradientType=0)!important;-o-box-shadow:1px 1px 0 #eee!important;box-shadow:1px 1px 0 #eee!important;outline:initial!important}.ottbp-widget button:active, .ottbp-widget input[type=button]:active, .ottbp-widget input[type=submit]:active{background:#3b679e!important;background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#3b679e),color-stop(50%,#2b88d9),color-stop(51%,#207cca),color-stop(100%,#7db9e8))!important;background:-webkit-linear-gradient(top,#3b679e 0,#2b88d9 50%,#207cca 51%,#7db9e8 100%)!important;background:linear-gradient(to bottom,#3b679e 0,#2b88d9 50%,#207cca 51%,#7db9e8 100%)!important;border-color:#5259b0!important}.ottbp-widget button{padding:1px 6px 2px 6px!important;margin-right:5px!important}.ottbp-widget input[type=hidden]{display:none!important}.ottbp-widget textarea{-webkit-appearance:textarea!important;background:#fff!important;padding:2px!important;margin-left:4px!important;word-wrap:break-word!important;white-space:pre-wrap!important;font-size:11px!important;font-family:arial,helvetica,sans-serif!important;line-height:13px!important;resize:both!important}.ottbp-widget input, .ottbp-widget select, .ottbp-widget textarea{border:1px solid #ccc!important}.ottbp-widget select{font-size:11px!important;font-family:helvetica,arial,sans-serif!important;display:inline-block}.ottbp-widget input:focus, .ottbp-widget textarea:focus{outline:auto 5px -webkit-focus-ring-color!important;outline:initial!important}.ottbp-widget input[type=text]{background:#fff!important;padding:1px!important;font-family:initial!important;font-size:small!important}.ottbp-widget input[type=checkbox], .ottbp-widget input[type=radio]{border:1px #2b2b2b solid!important;border-radius:4px!important}.ottbp-widget input[type=checkbox], .ottbp-widget input[type=radio]{outline:initial!important}.ottbp-widget input[type=radio]{margin:2px 2px 3px 2px!important}.ottbp-widget abbr[title], .ottbp-widget acronym[title], .ottbp-widget dfn[title]{cursor:help!important;border-bottom-width:1px!important;border-bottom-style:dotted!important}.ottbp-widget ins{background-color:#ff9!important;color:#000!important}.ottbp-widget del{text-decoration:line-through!important}.ottbp-widget blockquote, .ottbp-widget q{quotes:none!important}.ottbp-widget blockquote:after, .ottbp-widget blockquote:before, .ottbp-widget li:after, .ottbp-widget li:before, .ottbp-widget q:after, .ottbp-widget q:before{content:\"\"!important}.ottbp-widget input, .ottbp-widget select{vertical-align:middle!important}.ottbp-widget table{border-collapse:collapse!important;border-spacing:0!important}.ottbp-widget hr{display:block!important;height:1px!important;border:0!important;border-top:1px solid #ccc!important;margin:1em 0!important}.ottbp-widget [dir=rtl]{direction:rtl!important}.ottbp-widget mark{background-color:#ff9!important;color:#000!important;font-style:italic!important;font-weight:700!important}.ottbp-widget menu{padding-left:40px!important;padding-top:8px!important}.ottbp-widget [hidden], .ottbp-widget template{display:none!important}.ottbp-widget abbr[title]{border-bottom:1px dotted!important}.ottbp-widget sub, .ottbp-widget sup{font-size:75%!important;line-height:0!important;position:relative!important;vertical-align:baseline!important}.ottbp-widget sup{top:-.5em!important}.ottbp-widget sub{bottom:-.25em!important}.ottbp-widget img{border:0!important}.ottbp-widget figure{margin:0!important}.ottbp-widget textarea{overflow:auto!important;vertical-align:top!important}.ottbp-widget pre{white-space:pre!important}@-webkit-keyframes display_up{0%{opacity:0;-webkit-transform:translate(0,100px);transform:translate(0,100px)}100%{opacity:1;-webkit-transform:translate(0,0);transform:translate(0,0)}}@keyframes display_up{0%{opacity:0;-webkit-transform:translate(0,100px);transform:translate(0,100px)}100%{opacity:1;-webkit-transform:translate(0,0);transform:translate(0,0)}}@-webkit-keyframes hide_right{0%{opacity:1;-webkit-transform:translate(0,0);transform:translate(0,0)}100%{opacity:0;-webkit-transform:translate(100px,0);transform:translate(100px,0)}}@keyframes hide_right{0%{opacity:1;-webkit-transform:translate(0,0);transform:translate(0,0)}100%{opacity:0;-webkit-transform:translate(100px,0);transform:translate(100px,0)}}@-webkit-keyframes loader_spin{0%{-webkit-transform:rotateZ(0);transform:rotateZ(0)}100%{-webkit-transform:rotateZ(360deg);transform:rotateZ(360deg)}}@keyframes loader_spin{0%{-webkit-transform:rotateZ(0);transform:rotateZ(0)}100%{-webkit-transform:rotateZ(360deg);transform:rotateZ(360deg)}}@-webkit-keyframes loader_show{0%{opacity:0;-webkit-transform:scale(0);transform:scale(0)}100%{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}@keyframes loader_show{0%{opacity:0;-webkit-transform:scale(0);transform:scale(0)}100%{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}button{width:auto!important;overflow:visible!important}.ottbp-texts--day, .ottbp-texts--month, .ottbp-texts--textfield, .ottbp-texts--title, button.ottbp-texts--month, div.ottbp-texts--textfield, input[type=text].ottbp-texts--textfield{font-family:Roboto,Roboto-Regular,sans-serif!important;font-size:16px!important;letter-spacing:.2px!important;line-height:19px!important}.ottbp-texts--btn, a.ottbp-texts--btn, button.ottbp-texts--btn, div.ottbp-texts--price, tr.ottbp-texts--week-days{font-family:Roboto,Roboto-Regular,sans-serif!important;font-size:13px!important;letter-spacing:.2px!important;line-height:15px!important;text-decoration:none!important}button.ottbp--low_price, div.ottbp--low_price{background:#b8e9c0!important}button.ottbp--middle_price, div.ottbp--middle_price{background:#eff9e8!important}button.ottbp--high_price, div.ottbp--high_price{background:#ffd3bc!important}.ottbp-btn.ottbp--low_price:hover{background:#a5e3af!important}.ottbp-btn.ottbp--middle_price:hover{background:#e1f4d4!important}.ottbp-btn.ottbp--high_price:hover{background:#ffc2a3!important}div.ottbp-black, span.ottbp-black{color:#424242!important}div.ottbp-gray, span.ottbp-gray{color:#9e9e9e!important}.ottbp-inputs__maxi{position:relative!important}.ottbp-inputs__maxi:after, .ottbp-inputs__maxi:before{content:\" \"!important;display:table!important}.ottbp-inputs__maxi:after{clear:both!important}.ottbp-inputs__maxi div.ottbp-inputs--from{width:38.94230769%!important}.ottbp-inputs__maxi div.ottbp-inputs--to{width:49.03846154%!important}.ottbp-inputs__maxi div.ottbp-inputs--search{width:12.01923077%!important}.ottbp-inputs__maxi div.ottbp-inputs__field{box-sizing:border-box!important;float:left!important;border-right:1px solid #e0e0e0!important;border-bottom:0!important;border-top:0!important;border-left:0!important}.ottbp-inputs__maxi div.ottbp-inputs__field:first-child .ottbp-control{border-radius:2px 0 0 2px!important}.ottbp-inputs__maxi div.ottbp-inputs__field:last-child{border-right:0!important}.ottbp-inputs__maxi div.ottbp-inputs__field:last-child .ottbp-control{border-radius:0 2px 2px 0!important}.ottbp-inputs__maxi div.ottbp-inputs__field.ottbp-control--error{border-color:#ff5937!important}div.ottbp-inputs__field{box-sizing:border-box!important;border-bottom:1px solid #e0e0e0!important;border-top:0!important;border-left:0!important;border-right:0!important}div.ottbp-inputs__field:last-child{border-bottom:0!important}div.ottbp-inputs__field.ottbp-control--error{border-color:red!important}.ottbp-more-extrasmall .ottbp-inputs{position:relative!important}.ottbp-more-extrasmall .ottbp-inputs:after, .ottbp-more-extrasmall .ottbp-inputs:before{content:\" \"!important;display:table!important}.ottbp-more-extrasmall .ottbp-inputs:after{clear:both!important}.ottbp-more-extrasmall .ottbp-inputs div.ottbp-inputs--from{width:38.94230769%!important}.ottbp-more-extrasmall .ottbp-inputs div.ottbp-inputs--to{width:49.03846154%!important}.ottbp-more-extrasmall .ottbp-inputs div.ottbp-inputs--search{width:12.01923077%!important}.ottbp-more-extrasmall .ottbp-inputs div.ottbp-inputs__field{box-sizing:border-box!important;float:left!important;border-right:1px solid #e0e0e0!important;border-bottom:0!important;border-top:0!important;border-left:0!important}.ottbp-more-extrasmall .ottbp-inputs div.ottbp-inputs__field:first-child .ottbp-control{border-radius:2px 0 0 2px!important}.ottbp-more-extrasmall .ottbp-inputs div.ottbp-inputs__field:last-child{border-right:0!important}.ottbp-more-extrasmall .ottbp-inputs div.ottbp-inputs__field:last-child .ottbp-control{border-radius:0 2px 2px 0!important}.ottbp-more-extrasmall .ottbp-inputs div.ottbp-inputs__field.ottbp-control--error{border-color:#ff5937!important}.ottbp-more-big .ottbp-inputs div.ottbp-inputs--from{width:41.94711538%!important}.ottbp-more-big .ottbp-inputs div.ottbp-inputs--to{width:52.04326923%!important}.ottbp-more-big .ottbp-inputs div.ottbp-inputs--search{width:6.00961538%!important}div.ottbp-selects{margin:0 -4px!important;font-size:0!important;position:relative!important}div.ottbp-selects:after, div.ottbp-selects:before{content:\" \"!important;display:table!important}div.ottbp-selects:after{clear:both!important}div.ottbp-selects__cell{display:inline-block!important;box-sizing:border-box!important;width:50%!important;-webkit-transition:width .2s ease!important;transition:width .2s ease!important}div.ottbp-selects__cell__wrap{padding:4px!important}div.ottbp-results{width:100%!important;position:relative!important}ul.ottbp-results__list{list-style:none!important;margin:0!important;box-sizing:border-box!important;padding:0!important}li.ottbp-results__list__item{margin:0 0 12px!important;padding:0!important;-webkit-animation-name:display_up!important;animation-name:display_up!important;-webkit-animation-duration:.3s!important;animation-duration:.3s!important;-webkit-animation-fill-mode:both!important;animation-fill-mode:both!important;-webkit-animation-timing-function:cubic-bezier(.175,.885,.32,1)!important;-webkit-animation-timing-function:cubic-bezier(.175,.885,.32,1.275)!important;animation-timing-function:cubic-bezier(.175,.885,.32,1.275)!important}li.ottbp-results__list__item:nth-child(2){-webkit-animation-delay:.1s!important;animation-delay:.1s!important}li.ottbp-results__list__item:nth-child(3){-webkit-animation-delay:.2s!important;animation-delay:.2s!important}li.ottbp-results__list__item:nth-child(4){-webkit-animation-delay:.3s!important;animation-delay:.3s!important}li.ottbp-results__list__item div.ottbp-results__list__item--empty{color:#424242!important;text-align:center!important;display:none!important}.ottbp-results.ottbp-loading ul.ottbp-results__list{position:absolute!important;width:100%!important}.ottbp-results.ottbp-loading li.ottbp-results__list__item{-webkit-animation-name:hide_right!important;animation-name:hide_right!important}div.ottbp-results__loader{position:absolute!important;top:30px!important;left:0!important;right:0!important;margin:0 auto!important;text-align:center!important}div.ottbp-results__after-loader{text-align:center!important;padding-right:20px!important}div.ottbp-error, div.ottbp-info, div.ottbp-more{color:#424242!important;padding:10px 20px 10px!important;line-height:1.4!important}div.ottbp-info a{color:#01abfb!important;text-decoration:none!important}div.ottbp-info a:hover{color:#0188c8!important;text-decoration:underline!important}div.ottbp-more{text-align:right!important}div.ottbp-more a{display:inline-block!important;background:#01abfb!important;border-radius:2px!important;font-family:Roboto,Roboto-Medium,sans-serif!important;font-weight:500!important;font-size:14px!important;color:#fff!important;letter-spacing:.2px!important;padding:7px 11px!important;text-decoration:none!important;cursor:pointer!important}div.ottbp-more a:visited{color:#eee!important}div.ottbp-more a:hover{background:#0188c8!important;color:#fff!important}div.ottbp-form__header{position:relative!important;z-index:2!important}div.ottbp-form__footer{background:#fff!important;position:relative!important;z-index:1!important}div.ottbp-footer{position:relative!important;-webkit-transition:height .2s ease!important;transition:height .2s ease!important}.ottbp-more-medium div.ottbp-footer:after, .ottbp-more-medium div.ottbp-footer:before{content:\" \"!important;display:table!important}.ottbp-more-medium div.ottbp-footer:after{clear:both!important}div.ottbp-footer__loader{text-align:center!important;padding:30px 0!important;position:absolute!important;top:0!important;left:0!important;width:100%!important;z-index:5!important;-webkit-transform:translate3d(0,0,0)!important;transform:translate3d(0,0,0)!important}div.ottbp-footer__date{position:relative!important;z-index:10!important;-webkit-transform:translate3d(0,0,0)!important;transform:translate3d(0,0,0)!important}div.ottbp-footer__results{position:relative!important;z-index:2!important;-webkit-transform:translate3d(0,0,0)!important;transform:translate3d(0,0,0)!important}.ottbp-more-medium div.ottbp-footer__loader{z-index:10!important}.ottbp-more-medium div.ottbp-footer__date{min-height:1px!important;width:400px!important;float:left!important;z-index:5!important}.ottbp-more-medium div.ottbp-footer__results{min-height:1px!important;width:auto!important;float:left!important;z-index:5!important}div.ottbp-container-picker{background:#878d96!important;padding:0 20px 20px!important}.ottbp-more-medium div.ottbp-container-picker{background-color:transparent!important;padding:0!important}div.ottbp-container-picker__wrap{position:relative!important}.ottbp-more-medium div.ottbp-container-picker__btn{display:none!important}div.ottbp-container-picker__picker{top:32px!important;left:0!important;width:100%!important;z-index:2!important;position:absolute!important;display:none!important;padding:10px 5px!important;background-color:#fff!important;box-sizing:border-box!important;border:1px solid #e0e0e0!important;border-top:0!important}div.ottbp-container-picker__picker:after{content:''!important;position:absolute!important;z-index:-1!important;bottom:-3px!important;width:94%!important;left:50%!important;margin-left:-47%!important;height:30px!important;box-shadow:0 16px 30px -5px rgba(0,0,0,.2)!important;-webkit-transform:perspective(800px) rotateY(0) rotateX(45deg)!important;transform:perspective(800px) rotateY(0) rotateX(45deg)!important}.ottbp-more-extrasmall div.ottbp-container-picker__picker{padding:20px!important}.ottbp-more-medium div.ottbp-container-picker__picker{padding:0!important;background-color:transparent!important;top:0!important;position:relative!important;display:block!important;border:0!important}.ottbp-more-medium div.ottbp-container-picker__picker:after{display:none!important}button.ottbp-container-picker-btn{border-radius:0!important;background:#fff!important;border:0!important;outline:0!important;display:inline-block!important;width:100%!important;padding:9px 0 8px!important;margin:0!important;cursor:pointer!important;box-sizing:border-box!important;color:#616161!important;text-align:center!important;box-shadow:none!important;overflow:hidden!important;font-size:13px!important;line-height:15px!important;vertical-align:middle!important;-webkit-appearance:none!important;-moz-appearance:none!important;appearance:none!important;-webkit-transition:background .2s ease,color .2s ease,opacity .2s ease,width .2s ease,border-radius .1s ease!important;transition:background .2s ease,color .2s ease,opacity .2s ease,width .2s ease,border-radius .1s ease!important}button.ottbp-container-picker-btn:hover{background:#f5f5f5!important;box-shadow:inset 0 0 0 1px #e0e0e0!important}button.ottbp-container-picker-btn.ottbp-container-picker-btn--clean{background:#fff!important;color:rgba(255,166,96,.9)!important;font-size:12px!important}button.ottbp-container-picker-btn.ottbp-container-picker-btn--clean:hover{background:#fff!important;box-shadow:inset 0 0 0 1px rgba(255,166,96,.5)!important}button.ottbp-container-picker-btn:first-child{border-top-left-radius:2px!important;border-bottom-left-radius:2px!important;width:90%!important}button.ottbp-container-picker-btn:last-child{border-top-right-radius:2px!important;border-bottom-right-radius:2px!important;width:10%!important}button.ottbp-container-picker-btn:focus{box-shadow:0 0 5px #eee!important}div.ottbp-container-results{padding:10px!important}.ottbp-more-extrasmall div.ottbp-container-results{padding:20px!important}div.ottbp-form-header{background:rgba(62,71,87,.6)!important;border-radius:4px 4px 0 0!important;padding:20px 20px 4px!important}.ottbp-more-medium div.ottbp-form-header{padding:20px!important}div.ottbp-form-header__inputs{margin-bottom:12px!important}div.ottbp-form-header__selects{margin:-6px -6px!important}div.ottbp-control{position:relative!important;background:#fff!important;height:48px!important}div.ottbp-control.ottbp-control--error{box-shadow:inset 0 0 0 2px #ff5937!important}div.ottbp-control__placeholder{position:absolute!important;top:0!important;left:0!important;width:100%!important;height:100%!important;z-index:0!important}div.ottbp-control__input{position:relative!important;z-index:1!important}div.ottbp-control__helper{position:absolute!important;top:48px!important;z-index:100!important;width:100%!important;display:none!important}div.ottbp-control__helper:after{content:''!important;position:absolute!important;z-index:-1!important;bottom:-3px!important;width:94%!important;left:50%!important;margin-left:-47%!important;height:30px!important;box-shadow:0 16px 30px -5px rgba(0,0,0,.2)!important;-webkit-transform:perspective(800px) rotateY(0) rotateX(45deg)!important;transform:perspective(800px) rotateY(0) rotateX(45deg)!important}div.ottbp-control__helper__shadow{position:absolute!important;z-index:1!important;height:0!important;opacity:0!important;width:100%!important;top:0!important;left:0!important;background-image:-webkit-linear-gradient(bottom,rgba(255,255,255,0) 0,#9e9e9e 100%)!important;background-image:linear-gradient(0deg,rgba(255,255,255,0) 0,#9e9e9e 100%)!important}div.ottbp-control:hover{background:#eee!important;cursor:pointer!important}div.ottbp-control.ottbp-blur{background:#fff !important!important}.ottbp-control.ottbp-helper .ottbp-control__helper{display:block!important}div.ottbp-placeholder{margin:0!important;padding:15px 11px 14px!important}.ottbp-placeholder span{letter-spacing:inherit!important;font-size:inherit!important}span.ottbp-placeholder__hidden{color:transparent!important}span.ottbp-placeholder__visible{color:#9e9e9e!important;opacity:.7!important}.ottbp-control--error span.ottbp-placeholder__visible{color:#ff836a!important}input[type=text].ottbp-input__field{margin:0!important;padding:15px 11px 14px!important;background:0 0!important;border:0!important;outline:0!important;color:#424242!important;display:block!important;width:100%!important;box-sizing:border-box!important;box-shadow:none!important}input[type=text].ottbp-input__field:focus{box-shadow:inset 0 0 0 1px #d4d2d2!important}.ottbp-control--error input[type=text].ottbp-input__field{color:#9d1b00!important}.ottbp-control--error input[type=text].ottbp-input__field:focus{box-shadow:inset 0 0 0 1px #ff5937!important}div.ottbp-input-helper{max-height:303px!important;overflow-y:auto!important;position:relative!important}ul.ottbp-input-helper__list{background:#fff!important;list-style:none!important;margin:0!important;padding:0!important}ul.ottbp-input-helper__list li.ottbp-input-helper__list__item{display:block!important;border:1px solid #eee!important;border-top:0!important;margin-top:0!important;padding:0!important}a.ottbp-input-helper-link{display:block!important;position:relative!important;padding:17px 11px 16px!important;text-decoration:none!important}a.ottbp-input-helper-link span{font-family:Roboto,Roboto-Regular,sans-serif!important;font-weight:400!important;font-size:14px!important;letter-spacing:.17px!important;vertical-align:middle!important}a.ottbp-input-helper-link span svg{display:block!important}a.ottbp-input-helper-link span.ottbp-helper-icon{display:inline-block!important;height:12px!important;vertical-align:middle!important;margin-right:5px!important}a.ottbp-input-helper-link span.ottbp-helper-strong{color:#424242!important}a.ottbp-input-helper-link span.ottbp-helper-gray{color:#9e9e9e!important}a.ottbp-input-helper-link span.ottbp-helper-iata{display:block!important;position:absolute!important;color:#9e9e9e!important;right:0!important;top:0!important;padding:17px 11px 16px!important}a.ottbp-input-helper-link:hover{background:#e5e5e5!important}a.ottbp-input-helper-link.ottbp-highlight{background:#e5e5e5!important}button.ottbp-search__btn{height:40px!important;width:100%!important;border:none!important;box-sizing:border-box!important;-webkit-appearance:none!important;-moz-appearance:none!important;appearance:none!important;color:#616161!important;background:#ffd41e!important;box-shadow:none!important;outline:0!important;padding:0!important;text-align:center!important;border-radius:0!important;font-size:16px!important;font-weight:400!important}button.ottbp-search__btn span{display:inline-block!important;vertical-align:middle!important}button.ottbp-search__btn span+span{margin-left:10px!important}button.ottbp-search__btn:hover{box-shadow:none!important;background:#ffc901!important}button.ottbp-search__btn:focus{box-shadow:0 0 5px #eee!important}.ottbp-more-extrasmall button.ottbp-search__btn{height:48px!important;border-radius:0 3px 3px 0!important}.ottbp-more-extrasmall button.ottbp-search__btn>span.ottbp-search__btn__text{display:none!important}div.ottbp-select{position:relative!important}button.ottbp-select__btn{border-radius:2px!important;background:#fff!important;border:0!important;outline:0!important;display:block!important;width:100%!important;padding:9px 0 8px!important;cursor:pointer!important;box-sizing:border-box!important;color:#616161!important;text-align:center!important;box-shadow:none!important;overflow:hidden!important;-webkit-appearance:none!important;-moz-appearance:none!important;appearance:none!important;-webkit-transition:background .2s ease,color .2s ease,opacity .2s ease!important;transition:background .2s ease,color .2s ease,opacity .2s ease!important}button.ottbp-select__btn:hover{background:#f5f5f5!important;box-shadow:inset 0 0 0 1px #e0e0e0!important;border-radius:2px!important}button.ottbp-select__btn:focus{box-shadow:0 0 5px #eee!important}span.ottbp-select__btn__inner{color:inherit!important;font-family:inherit!important;font-size:inherit!important;white-space:nowrap!important}.ottbp-select.ottbp-selected button.ottbp-select__btn{background:#ffd41e!important}.ottbp-select.ottbp-selected button.ottbp-select__btn:hover{box-shadow:none!important;background:#ffc901!important}.ottbp-select.ottbp-disabled button.ottbp-select__btn{cursor:not-allowed!important;background:rgba(100,100,100,.6)!important;color:rgba(50,50,50,.3)!important;box-shadow:none!important;opacity:.3!important}div.ottbp-select__helper{position:absolute!important;top:0!important;left:0!important;width:100%!important;z-index:3!important}ul.ottbp-select-helper{list-style:none!important;margin:0!important;padding:0!important;border:1px solid #e0e0e0!important;background:#fff!important;border-radius:2px!important;display:none!important;position:relative!important}.ottbp--open ul.ottbp-select-helper{display:block!important}ul.ottbp-select-helper li.ottbp-select-helper__item{display:block!important;border-top:1px solid #e0e0e0!important;padding:0!important;margin-top:0!important;white-space:nowrap!important}ul.ottbp-select-helper li.ottbp-select-helper__item:after, ul.ottbp-select-helper li.ottbp-select-helper__item:before{display:none!important}ul.ottbp-select-helper li.ottbp-select-helper__item:first-child{border-top:0!important;margin-top:-1px!important}ul.ottbp-select-helper li.ottbp-select-helper__item button.ottbp-select__btn{border-radius:0!important}ul.ottbp-select-helper li.ottbp-select-helper__item button.ottbp-select__btn:hover{border:none!important;box-shadow:none!important}ul.ottbp-select-helper li.ottbp-select-helper__item.ottbp-selected{border-radius:2px 2px 0 0!important;margin-left:-1px!important;margin-right:-1px!important;background:#ffd41e!important}ul.ottbp-select-helper li.ottbp-select-helper__item.ottbp-selected a.ottbp-select-helper-item:hover{box-shadow:none!important;background:#ffc901!important}.ottbp-selected ul.ottbp-select-helper li.ottbp-select-helper__item:nth-child(2){border-top:0!important}a.ottbp-select-helper-item{overflow:hidden!important;border:0!important;outline:0!important;display:block!important;width:100%!important;padding:9px 0 8px!important;cursor:pointer!important;box-sizing:border-box!important;color:#616161!important;text-align:center!important}a.ottbp-select-helper-item span.ottbp-select-helper__inner{color:inherit!important;font-family:inherit!important;font-size:inherit!important;white-space:nowrap!important}a.ottbp-select-helper-item:hover{background:#e5e5e5!important;border-radius:2px!important}div.ottbp-date{background:#fff!important;position:relative!important}header.ottbp-date__header{display:none!important}.ottbp-more-medium header.ottbp-date__header{display:block!important}div.ottbp-date__title{color:#424242!important;padding:25px 20px 24px!important;text-align:center!important}span.ottbp-date__title__inner button, span.ottbp-date__title__inner span{vertical-align:middle!important}.ottbp-date__title .ottbp-btn.ottbp-btn-delete{font-family:Roboto,Roboto-Medium,sans-serif!important;border:0!important;background-color:transparent!important;background-image:none!important;box-shadow:none!important;outline:0!important;color:#ffa660!important;font-size:16px!important;padding:0!important;margin:0 10px!important;vertical-align:middle!important}div.ottbp-date__body{padding:0 20px!important}div.ottbp-date__body__inner{max-width:356px!important;margin:0 auto!important}div.ottbp-date__loader{position:absolute!important;top:30px!important;left:0!important;width:100%!important;text-align:center!important}div.ottbp-months{margin:0 -6px!important}div.ottbp-months:after, div.ottbp-months:before{content:\" \"!important;display:table!important}div.ottbp-months:after{clear:both!important}div.ottbp-months__item{float:left!important;width:50%!important;box-sizing:border-box!important;padding:6px!important}button.ottbp-month{border-radius:100px!important;border:0!important;margin:0!important;outline:0!important;color:#424242!important;padding:15px 0 14px!important;text-align:center!important;display:block!important;width:100%!important;cursor:pointer!important;background:rgba(255,255,255,.8)!important;box-shadow:0 0 0 1px rgba(224,224,224,.8)!important;-webkit-appearance:none!important;-moz-appearance:none!important;appearance:none!important}button.ottbp-month:hover{background:rgba(242,242,242,.8)!important}span.ottbp-month__inner{cursor:pointer!important;color:inherit!important}.ottbp-month.ottbp--custom_price{box-shadow:none!important}.ottbp-month--low, .ottbp-month.ottbp--low_price{background:#b8e9c0!important;box-shadow:none!important}.ottbp-month--low:hover, .ottbp-month.ottbp--low_price:hover{background:#a5e3af!important}.ottbp-month--middle, .ottbp-month.ottbp--middle_price{background:#eff9e8!important;box-shadow:none!important}.ottbp-month--middle:hover, .ottbp-month.ottbp--middle_price:hover{background:#e1f4d4!important}.ottbp-month--high, .ottbp-month.ottbp--high_price{background:#ffd3bc!important;box-shadow:none!important}.ottbp-month--high:hover, .ottbp-month.ottbp--high_price:hover{background:#ffc2a3!important}div.ottbp-months-price_range{padding:20px 0!important;text-align:center!important}.ottbp-more-extrasmall div.ottbp-months-price_range{text-align:left!important}div.ottbp-prices{font-size:0!important}div.ottbp-prices__item{display:inline-block!important;vertical-align:middle!important;margin-right:17px!important}div.ottbp-price{font-size:0!important}div.ottbp-price__color, div.ottbp-price__cost{vertical-align:middle!important;display:inline-block!important}div.ottbp-price__color{margin-right:7px!important}div.ottbp-price__color__circle{width:15px!important;height:15px!important;display:block!important;border-radius:50%!important}.ottbp-more-extrasmall div.ottbp-price__color__circle{width:20px!important;height:20px!important}div.ottbp-price__cost{color:#424242!important;font-size:10px!important;letter-spacing:.2px!important;line-height:13px!important;text-decoration:none!important}.ottbp-more-extrasmall div.ottbp-price__cost{font-size:13px!important;line-height:15px!important}.ottbp-calendar td{vertical-align:middle!important}table.ottbp-calendar{margin:auto!important;font-size:0!important;border-collapse:collapse!important}.ottbp-calendar tr.ottbp-cal-month{cursor:pointer!important;color:#9e9e9e!important}.ottbp-calendar td.ottbp-cal-month__name{color:inherit!important;padding:2px 0!important;font-family:Roboto,Roboto-Regular,sans-serif!important;font-size:14px!important;letter-spacing:.17px!important;cursor:pointer!important}.ottbp-more-small .ottbp-calendar td.ottbp-cal-month__name{padding:10px 8px!important}.ottbp-calendar td.ottbp-cal-month__name a{float:right!important;text-decoration:none!important;color:rgba(158,158,158,.5)!important}.ottbp-calendar td.ottbp-cal-month__name a:hover{color:#000!important}.ottbp-calendar td.ottbp-cal-month__select{text-align:center!important;color:inherit!important;font-family:Roboto,Roboto-Regular,sans-serif!important;font-size:14px!important;letter-spacing:.17px!important;cursor:pointer!important;padding:2px 0!important}.ottbp-more-small .ottbp-calendar td.ottbp-cal-month__select{padding:10px 8px!important}.ottbp-calendar td.ottbp-cal-month__select a{color:inherit!important}.ottbp-calendar td.ottbp-cal-month__select a:hover{color:#000!important}.ottbp-calendar tr.ottbp-cal-month:hover{cursor:pointer!important;text-decoration:underline!important}.ottbp-calendar tr.ottbp-cal-days{color:#9e9e9e!important}.ottbp-calendar tr.ottbp-cal-days td.ottbp-cal-weekend{color:#ff974d!important}.ottbp-calendar td.ottbp-cal-day{padding:0!important;margin:0!important;border:0!important;width:30px!important;height:30px!important;text-align:center!important}.ottbp-extrasmall .ottbp-calendar td.ottbp-cal-day{width:38px!important;height:38px!important}.ottbp-more-small .ottbp-calendar td.ottbp-cal-day{width:48px!important;height:48px!important}.ottbp-calendar thead td.ottbp-cal-day{height:40px!important}div.ottbp-calendar-price_range{padding:10px 5px!important;text-align:center!important}.ottbp-extrasmall div.ottbp-calendar-price_range{text-align:left!important;padding:15px 7px!important}.ottbp-more-small div.ottbp-calendar-price_range{padding:20px 10px!important}div.ottbp-btn-day{display:block!important;width:30px!important;height:30px!important;position:relative!important;text-align:center!important;border:0!important;background:0 0!important;outline:0!important;box-shadow:none!important;box-sizing:border-box!important;cursor:pointer!important;color:#000!important;text-decoration:none!important;z-index:100!important}div.ottbp-btn-day *{cursor:pointer!important}.ottbp-extrasmall div.ottbp-btn-day{width:38px!important;height:38px!important}.ottbp-more-small div.ottbp-btn-day{width:48px!important;height:48px!important}div.ottbp-btn-day__hover{position:absolute!important;z-index:0!important;top:1px!important;left:1px!important;background:0 0!important;width:28px!important;height:28px!important;border-radius:50%!important}.ottbp-extrasmall div.ottbp-btn-day__hover{width:36px!important;height:36px!important}.ottbp-more-small div.ottbp-btn-day__hover{width:44px!important;height:44px!important}div.ottbp-btn-day__back{position:absolute!important;top:1px!important;left:1px!important;width:28px!important;height:28px!important;border-radius:50%!important;z-index:1!important}.ottbp-extrasmall div.ottbp-btn-day__back{top:1px!important;left:1px!important;width:36px!important;height:36px!important}.ottbp-more-small div.ottbp-btn-day__back{top:2px!important;left:2px!important;width:44px!important;height:44px!important}div.ottbp-btn-day__price{width:24px!important;height:24px!important;border-radius:50%!important;z-index:2!important;vertical-align:middle!important;top:0!important;left:0!important;right:0!important;bottom:0!important;position:absolute!important;margin:auto!important}div.ottbp-btn-day__price.ottbp--none_price{background:rgba(255,255,255,.8)!important;box-shadow:0 0 0 1px rgba(224,224,224,.8)!important}.ottbp-extrasmall div.ottbp-btn-day__price{width:32px!important;height:32px!important}.ottbp-more-small div.ottbp-btn-day__price{width:36px!important;height:36px!important}div.ottbp-btn-day__inner{z-index:110!important;position:absolute!important;height:15px!important;width:20px!important;top:0!important;left:0!important;bottom:0!important;text-align:center!important;right:0!important;margin:auto!important;font-family:Roboto,Roboto-Regular,sans-serif!important;font-size:13px!important;letter-spacing:.2px!important;line-height:15px!important}.ottbp-extrasmall div.ottbp-btn-day__inner{font-size:14px!important;line-height:15px!important;height:15px!important;width:20px!important}.ottbp-more-small div.ottbp-btn-day__inner{font-size:16px!important;line-height:19px!important;height:19px!important;width:20px!important}.ottbp-btn-day.ottbp-btn-day--disabled div.ottbp-btn-day__inner{color:#d4d2d2!important}.ottbp-btn-day.ottbp-btn-day--disabled div.ottbp-btn-day__price{display:none!important}div.ottbp-btn-day:not(.ottbp-btn-day--disabled):hover div.ottbp-btn-day__price{width:28px!important;height:28px!important}.ottbp-extrasmall div.ottbp-btn-day:not(.ottbp-btn-day--disabled):hover div.ottbp-btn-day__price{width:36px!important;height:36px!important}.ottbp-more-small div.ottbp-btn-day:not(.ottbp-btn-day--disabled):hover div.ottbp-btn-day__price{width:44px!important;height:44px!important}.ottbp-btn-day.ottbp-btn-day--selected div.ottbp-btn-day__back{background:#ffe470!important}.ottbp-btn-day.ottbp-btn-day--selected div.ottbp-btn-day__price{width:0!important;height:0!important;opacity:0!important}.ottbp-btn-day.ottbp-btn-day--hover div.ottbp-btn-day__hover{background:rgba(128,128,128,.2)!important;border:1px solid rgba(255,238,163,.25)!important}.ottbp-btn-day.ottbp-btn-day--hover--after div.ottbp-btn-day__hover, .ottbp-btn-day.ottbp-btn-day--hover--before div.ottbp-btn-day__hover{width:29px!important}.ottbp-extrasmall .ottbp-btn-day.ottbp-btn-day--hover--after div.ottbp-btn-day__hover, .ottbp-extrasmall .ottbp-btn-day.ottbp-btn-day--hover--before div.ottbp-btn-day__hover{width:37px!important}.ottbp-more-small .ottbp-btn-day.ottbp-btn-day--hover--after div.ottbp-btn-day__hover, .ottbp-more-small .ottbp-btn-day.ottbp-btn-day--hover--before div.ottbp-btn-day__hover{width:46px!important}.ottbp-btn-day.ottbp-btn-day--hover--after.ottbp-btn-day--hover--before div.ottbp-btn-day__hover{width:30px!important}.ottbp-extrasmall .ottbp-btn-day.ottbp-btn-day--hover--after.ottbp-btn-day--hover--before div.ottbp-btn-day__hover{width:38px!important}.ottbp-more-small .ottbp-btn-day.ottbp-btn-day--hover--after.ottbp-btn-day--hover--before div.ottbp-btn-day__hover{width:48px!important}.ottbp-btn-day.ottbp-btn-day--hover--after div.ottbp-btn-day__hover{border-top-right-radius:0!important;border-bottom-right-radius:0!important;border-right:0!important}.ottbp-btn-day.ottbp-btn-day--hover--before div.ottbp-btn-day__hover{left:0!important;border-top-left-radius:0!important;border-bottom-left-radius:0!important;border-left:0!important}.ottbp-btn-day.ottbp-btn-day--selected--after div.ottbp-btn-day__back, .ottbp-btn-day.ottbp-btn-day--selected--before div.ottbp-btn-day__back{width:29px!important}.ottbp-extrasmall .ottbp-btn-day.ottbp-btn-day--selected--after div.ottbp-btn-day__back, .ottbp-extrasmall .ottbp-btn-day.ottbp-btn-day--selected--before div.ottbp-btn-day__back{width:37px!important}.ottbp-more-small .ottbp-btn-day.ottbp-btn-day--selected--after div.ottbp-btn-day__back, .ottbp-more-small .ottbp-btn-day.ottbp-btn-day--selected--before div.ottbp-btn-day__back{width:46px!important}.ottbp-btn-day.ottbp-btn-day--selected--after.ottbp-btn-day--selected--before div.ottbp-btn-day__back{width:30px!important;-webkit-transition:background .2s ease,border-radius .1s ease!important;transition:background .2s ease,border-radius .1s ease!important}.ottbp-extrasmall .ottbp-btn-day.ottbp-btn-day--selected--after.ottbp-btn-day--selected--before div.ottbp-btn-day__back{width:38px!important}.ottbp-more-small .ottbp-btn-day.ottbp-btn-day--selected--after.ottbp-btn-day--selected--before div.ottbp-btn-day__back{width:48px!important}.ottbp-btn-day.ottbp-btn-day--selected--after.ottbp-btn-day--selected--before div.ottbp-btn-day__price{width:0!important;height:0!important;opacity:0!important}.ottbp-btn-day.ottbp-btn-day--selected--after div.ottbp-btn-day__back{border-top-right-radius:0!important;border-bottom-right-radius:0!important}.ottbp-btn-day.ottbp-btn-day--selected--before div.ottbp-btn-day__back{left:0!important;border-top-left-radius:0!important;border-bottom-left-radius:0!important}div.ottbp-trip{background:#fff!important;box-shadow:0 1px 2px 0 rgba(0,0,0,.2),0 1px 3px 0 rgba(0,0,0,.1)!important}div.ottbp-trip__header{padding:9px 31px 8px!important;background:rgba(62,71,87,.6)!important}div.ottbp-trip__header span.ottbp-black{color:#fff!important}div.ottbp-trip__header span.ottbp-gray{color:#d4d2d2!important}.ottbp-trip__title{font-family:Roboto,Roboto-Medium,sans-serif!important;font-weight:500!important;font-size:16px!important;color:#424242!important;letter-spacing:.2px!important;line-height:19px!important}inner.ottbp-trip__title__inner{font-size:inherit!important;font-weight:inherit!important;font-family:inherit!important;line-height:inherit!important;letter-spacing:inherit!important}div.ottbp-trip__body{padding:12px 5px!important}div.ottbp-trip__directions{position:relative!important}div.ottbp-trip__directions:after, div.ottbp-trip__directions:before{content:\" \"!important;display:table!important}div.ottbp-trip__directions:after{clear:both!important}div.ottbp-trip__directions__item{float:left!important;width:50%!important;box-sizing:border-box!important}div.ottbp-trip__directions__item:nth-child(2){border-left:1px solid #eee!important}.ottbp-extrasmall div.ottbp-trip__directions__item{width:48%!important}.ottbp-more-small div.ottbp-trip__directions__item{width:45%!important}div.ottbp-trip__directions.ottbp-trip__directions--only_one{padding-top:15px!important;padding-bottom:10px!important}div.ottbp-trip__directions.ottbp-trip__directions--only_one div.ottbp-trip__directions__item{width:100%!important}div.ottbp-trip__directions.ottbp-trip__directions--only_one div.ottbp-direction{position:relative!important}div.ottbp-trip__directions.ottbp-trip__directions--only_one div.ottbp-direction:after, div.ottbp-trip__directions.ottbp-trip__directions--only_one div.ottbp-direction:before{content:\" \"!important;display:table!important}div.ottbp-trip__directions.ottbp-trip__directions--only_one div.ottbp-direction:after{clear:both!important}div.ottbp-trip__directions.ottbp-trip__directions--only_one div.ottbp-direction__add, div.ottbp-trip__directions.ottbp-trip__directions--only_one div.ottbp-direction__main{width:50%!important;float:left!important}div.ottbp-trip__footer{text-align:right!important;padding:0 24px 8px!important}div.ottbp-trip__btn, div.ottbp-trip__price{display:inline-block!important;vertical-align:middle!important}div.ottbp-trip__price{font-family:Roboto,Roboto-Medium,sans-serif!important;font-weight:500!important;font-size:21px!important;color:#4fbb04!important;letter-spacing:0!important}div.ottbp-trip__btn{margin-left:10px!important}a.ottbp-btn-link{display:block!important;background:#01abfb!important;border-radius:2px!important;font-family:Roboto,Roboto-Medium,sans-serif!important;font-weight:500!important;font-size:12px!important;color:#fff!important;letter-spacing:.2px!important;padding:7px 11px!important;text-decoration:none!important;cursor:pointer!important}.ottbp-btn-link svg, span.ottbp-btn-link__icon, span.ottbp-btn-link__inner{display:inline-block!important;vertical-align:middle!important}.ottbp-btn-link svg, span.ottbp-btn-link__icon{margin-left:10px!important}a.ottbp-btn-link:hover{background:#0188c8!important}div.ottbp-direction{padding:0 10px!important}.ottbp-extrasmall div.ottbp-direction{padding:0 10px!important}.ottbp-more-small div.ottbp-direction{padding:0 26px!important}table.ottbp-direction__title{font-family:Roboto,Roboto-Regular,sans-serif!important;font-size:13px!important;font-weight:400!important;color:#616161!important;letter-spacing:.18px!important;margin-bottom:7px!important;width:100%!important}.ottbp-direction__title span{display:inline-block!important;vertical-align:middle!important}.ottbp-direction__title td{vertical-align:middle!important}.ottbp-direction__title td:first-child{width:25px!important}span.ottbp-direction__title__icon{margin-right:10px!important}.ottbp-direction__title svg{display:inline-block!important;vertical-align:middle!important;margin-right:7px!important}span.ottbp-direction__title__inner{color:inherit!important}div.ottbp-direction__time{font-family:Roboto,Roboto-Medium,sans-serif!important;font-weight:500!important;font-size:18px!important;color:#212121!important;letter-spacing:0!important;margin-bottom:8px!important}div.ottbp-direction__timespent{font-family:Roboto,Roboto-Regular,sans-serif!important;font-weight:400!important;font-size:13px!important;color:#616161!important;letter-spacing:.18px!important;margin-bottom:9px!important}div.ottbp-direction__company{font-family:Roboto,Roboto-Regular,sans-serif!important;font-weight:400!important;font-size:13px!important;color:#9e9e9e!important;letter-spacing:.18px!important}img.ottbp-direction__company__logo{width:16px!important;height:16px!important;margin-right:10px!important;vertical-align:middle!important}span.ottbp-direction__company__inner{vertical-align:middle!important}div.ottbp-trip-text{padding:10px!important;-webkit-animation-name:display_up!important;animation-name:display_up!important;-webkit-animation-duration:.3s!important;animation-duration:.3s!important;-webkit-animation-fill-mode:both!important;animation-fill-mode:both!important;-webkit-animation-timing-function:cubic-bezier(.175,.885,.32,1)!important;-webkit-animation-timing-function:cubic-bezier(.175,.885,.32,1.275)!important;animation-timing-function:cubic-bezier(.175,.885,.32,1.275)!important}div.ottbp-loader{width:102.4px!important;height:102.4px!important;display:inline-block!important;position:relative!important;-webkit-transform:translate3d(0,0,0)!important;transform:translate3d(0,0,0)!important}.ottbp-loader div.ottbp-loader__back{width:100%!important;height:100%!important;position:absolute!important;border-radius:50%!important;overflow:hidden!important;background:rgba(101,101,172,.15)!important;z-index:1!important;-webkit-transition:opacity .3s ease 0s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) .1s!important;transition:opacity .3s ease 0s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) .1s,transform .3s cubic-bezier(.175,.885,.32,1.275) .1s!important;opacity:1!important;-webkit-transform:scale(1)!important;transform:scale(1)!important}.ottbp-loader.ottbp-loader--hide div.ottbp-loader__back{-webkit-transition:opacity .2s ease .3s,-webkit-transform 1s cubic-bezier(.175,.885,.32,1.275) 0s!important;transition:opacity .2s ease .3s,-webkit-transform 1s cubic-bezier(.175,.885,.32,1.275) 0s,transform 1s cubic-bezier(.175,.885,.32,1.275) 0s!important;opacity:0!important;-webkit-transform:scale(0)!important;transform:scale(0)!important}.ottbp-loader div.ottbp-loader__back__over{width:50%!important;height:50%!important;top:0!important;left:50%!important;background:#ffc901!important;position:absolute!important;-webkit-transform-origin:left bottom!important;transform-origin:left bottom!important;-webkit-animation-name:loader_spin!important;animation-name:loader_spin!important;-webkit-animation-duration:.9s!important;animation-duration:.9s!important;-webkit-animation-timing-function:linear!important;animation-timing-function:linear!important;-webkit-animation-fill-mode:both!important;animation-fill-mode:both!important;-webkit-animation-iteration-count:infinite!important;animation-iteration-count:infinite!important}.ottbp-loader .ottbp-loader--hide div.ottbp-loader__back__over{-webkit-animation-play-state:paused!important;animation-play-state:paused!important}.ottbp-loader div.ottbp-loader__front{-webkit-transition:opacity .1s ease 0s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) 0s!important;transition:opacity .1s ease 0s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) 0s,transform .3s cubic-bezier(.175,.885,.32,1.275) 0s!important}.ottbp-loader.ottbp-loader--hide div.ottbp-loader__front{-webkit-transition:opacity .3s ease .2s,-webkit-transform .5s cubic-bezier(.175,.885,.32,1.275) .2s!important;transition:opacity .3s ease .2s,-webkit-transform .5s cubic-bezier(.175,.885,.32,1.275) .2s,transform .5s cubic-bezier(.175,.885,.32,1.275) .2s!important;opacity:0!important;-webkit-transform:scale(0)!important;transform:scale(0)!important}.ottbp-loader div.ottbp-loader__front{position:absolute!important;z-index:1!important;width:76px!important;height:76px!important;border-radius:50%!important;margin:13.2px!important;background:#ffc901!important;background:-webkit-linear-gradient(45deg,#f6c200 0,#ffc901 50%,#ffd94e 100%)!important;background:linear-gradient(45deg,#f6c200 0,#ffc901 50%,#ffd94e 100%)!important;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f6c200', endColorstr='#ffd94e', GradientType=1)!important;box-shadow:0 1px 10px rgba(0,0,0,.15)!important}.ottbp-loader div.ottbp-loader__front__icon{width:31.2px!important;height:31.2px!important;display:block!important;position:absolute!important;margin:auto!important;left:0!important;right:0!important;bottom:0!important;top:0!important;opacity:1!important;-webkit-transition:opacity .1s ease .1s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) 0s!important;transition:opacity .1s ease .1s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) 0s,transform .3s cubic-bezier(.175,.885,.32,1.275) 0s!important}.ottbp-loader.ottbp-loader--hide div.ottbp-loader__front__icon{opacity:0!important;-webkit-transition:opacity .1s ease 0,-webkit-transform .5s cubic-bezier(.175,.885,.32,1.275) .2s!important;transition:opacity .1s ease 0,-webkit-transform .5s cubic-bezier(.175,.885,.32,1.275) .2s,transform .5s cubic-bezier(.175,.885,.32,1.275) .2s!important}div.ottbp-loader.ottbp-loader--small{width:51.2px!important;height:51.2px!important;display:inline-block!important;position:relative!important;-webkit-transform:translate3d(0,0,0)!important;transform:translate3d(0,0,0)!important}.ottbp-loader.ottbp-loader--small div.ottbp-loader__back{width:100%!important;height:100%!important;position:absolute!important;border-radius:50%!important;overflow:hidden!important;background:rgba(101,101,172,.15)!important;z-index:1!important;-webkit-transition:opacity .3s ease 0s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) .1s!important;transition:opacity .3s ease 0s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) .1s,transform .3s cubic-bezier(.175,.885,.32,1.275) .1s!important;opacity:1!important;-webkit-transform:scale(1)!important;transform:scale(1)!important}.ottbp-loader.ottbp-loader--small.ottbp-loader--hide div.ottbp-loader__back{-webkit-transition:opacity .2s ease .3s,-webkit-transform 1s cubic-bezier(.175,.885,.32,1.275) 0s!important;transition:opacity .2s ease .3s,-webkit-transform 1s cubic-bezier(.175,.885,.32,1.275) 0s,transform 1s cubic-bezier(.175,.885,.32,1.275) 0s!important;opacity:0!important;-webkit-transform:scale(0)!important;transform:scale(0)!important}.ottbp-loader.ottbp-loader--small div.ottbp-loader__back__over{width:50%!important;height:50%!important;top:0!important;left:50%!important;background:#ffc901!important;position:absolute!important;-webkit-transform-origin:left bottom!important;transform-origin:left bottom!important;-webkit-animation-name:loader_spin!important;animation-name:loader_spin!important;-webkit-animation-duration:.9s!important;animation-duration:.9s!important;-webkit-animation-timing-function:linear!important;animation-timing-function:linear!important;-webkit-animation-fill-mode:both!important;animation-fill-mode:both!important;-webkit-animation-iteration-count:infinite!important;animation-iteration-count:infinite!important}.ottbp-loader.ottbp-loader--small .ottbp-loader--hide div.ottbp-loader__back__over{-webkit-animation-play-state:paused!important;animation-play-state:paused!important}.ottbp-loader.ottbp-loader--small div.ottbp-loader__front{-webkit-transition:opacity .1s ease 0s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) 0s!important;transition:opacity .1s ease 0s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) 0s,transform .3s cubic-bezier(.175,.885,.32,1.275) 0s!important}.ottbp-loader.ottbp-loader--small.ottbp-loader--hide div.ottbp-loader__front{-webkit-transition:opacity .3s ease .2s,-webkit-transform .5s cubic-bezier(.175,.885,.32,1.275) .2s!important;transition:opacity .3s ease .2s,-webkit-transform .5s cubic-bezier(.175,.885,.32,1.275) .2s,transform .5s cubic-bezier(.175,.885,.32,1.275) .2s!important;opacity:0!important;-webkit-transform:scale(0)!important;transform:scale(0)!important}.ottbp-loader.ottbp-loader--small div.ottbp-loader__front{position:absolute!important;z-index:1!important;width:38px!important;height:38px!important;border-radius:50%!important;margin:6.6px!important;background:#ffc901!important;background:-webkit-linear-gradient(45deg,#f6c200 0,#ffc901 50%,#ffd94e 100%)!important;background:linear-gradient(45deg,#f6c200 0,#ffc901 50%,#ffd94e 100%)!important;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f6c200', endColorstr='#ffd94e', GradientType=1)!important;box-shadow:0 1px 10px rgba(0,0,0,.15)!important}.ottbp-loader.ottbp-loader--small div.ottbp-loader__front__icon{width:15.6px!important;height:15.6px!important;display:block!important;position:absolute!important;margin:auto!important;left:0!important;right:0!important;bottom:0!important;top:0!important;opacity:1!important;-webkit-transition:opacity .1s ease .1s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) 0s!important;transition:opacity .1s ease .1s,-webkit-transform .3s cubic-bezier(.175,.885,.32,1.275) 0s,transform .3s cubic-bezier(.175,.885,.32,1.275) 0s!important}.ottbp-loader.ottbp-loader--small.ottbp-loader--hide div.ottbp-loader__front__icon{opacity:0!important;-webkit-transition:opacity .1s ease 0,-webkit-transform .5s cubic-bezier(.175,.885,.32,1.275) .2s!important;transition:opacity .1s ease 0,-webkit-transform .5s cubic-bezier(.175,.885,.32,1.275) .2s,transform .5s cubic-bezier(.175,.885,.32,1.275) .2s!important}", ""]);
	
	// exports


/***/ },
/* 7 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	exports.bindReady = bindReady;
	exports.docCreate = docCreate;
	exports.setStyle = setStyle;
	exports.appendChildren = appendChildren;
	exports.classAddRemove = classAddRemove;
	exports.isNode = isNode;
	exports.isElement = isElement;
	function bindReady(callback) {
	  if (document.addEventListener) {
	    document.addEventListener('DOMContentLoaded', function FuncDOMContentLoaded() {
	      callback();
	      document.removeEventListener('DOMContentLoaded', FuncDOMContentLoaded, false);
	    }, false);
	  } else if (document.attachEvent) {
	    document.attachEvent('onreadystatechange', function Funconreadystatechange() {
	      if (document.readyState === 'complete') {
	        callback();
	        document.detachEvent('onreadystatechange', Funconreadystatechange);
	      }
	    });
	  }
	}
	
	/**
	 *
	 * @param {string} tagName
	 * @param {string[]|string} [classes]
	 * @return {HTMLElement}
	 */
	function docCreate(tagName, classes) {
	  /**
	   *
	   * @type {HTMLElement}
	   */
	  var tempNode = document.createElement(tagName);
	  if (Array.isArray(classes)) tempNode.className = classes.join(' ');else if (typeof classes == 'string') tempNode.className = classes;
	  return tempNode;
	}
	/**
	 *
	 * @param {HTMLElement} element
	 * @param {string|Object.<string,string>} styles
	 * @param {boolean} [important=true]
	 */
	function setStyle(element, styles) {
	  var important = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
	
	
	  if (!element) return;
	  if (typeof styles === 'string') {
	    element.setAttribute('style', styles + ' ' + (important ? '!important' : '') + ';');
	  } else if (styles) {
	    var style = '';
	    Object.keys(styles).forEach(function (styleName) {
	      style += styleName + ': ' + styles[styleName] + ' ' + (important ? '!important' : '') + ';';
	    });
	    element.setAttribute('style', style);
	  } else {
	    element.setAttribute('style', '');
	  }
	}
	/**
	 *
	 * @param {HTMLElement|DocumentFragment} container
	 * @param {HTMLElement[]} children
	 * @return {HTMLElement|DocumentFragment}
	 */
	function appendChildren(container, children) {
	  children.forEach(function (child) {
	    container.appendChild(child);
	  });
	  return container;
	}
	
	/**
	 *
	 * Class Add
	 *
	 * @param {HTMLElement} node
	 * @param {Array.<string>} addClasses
	 * @param {Array.<string>} [removeClasses=[]]
	 */
	function classAddRemove(node, addClasses) {
	  var removeClasses = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
	
	  var className = node.className,
	      oldClassName = className;
	
	  removeClasses.forEach(function (cl) {
	    var reg = new RegExp('' + cl, 'gm');
	    className = className.replace(reg, '');
	  });
	
	  addClasses.forEach(function (cl) {
	    if (className.indexOf(cl) < 0) className += ' ' + cl;
	  });
	
	  className = className.trim().replace(/[\s]{2,}/g, ' ');
	
	  if (className == oldClassName) return;
	
	  node.className = className.replace(/[\s]{2,}/g, ' ');
	}
	
	//Returns true if it is a DOM node
	function isNode(o) {
	  return (typeof Node === 'undefined' ? 'undefined' : _typeof(Node)) === 'object' ? o instanceof Node : o && (typeof o === 'undefined' ? 'undefined' : _typeof(o)) === 'object' && typeof o.nodeType === 'number' && typeof o.nodeName === 'string';
	}
	
	//Returns true if it is a DOM element
	function isElement(o) {
	  return (typeof HTMLElement === 'undefined' ? 'undefined' : _typeof(HTMLElement)) === 'object' ? o instanceof HTMLElement : //DOM2
	  o && (typeof o === 'undefined' ? 'undefined' : _typeof(o)) === 'object' && o !== null && o.nodeType === 1 && typeof o.nodeName === 'string';
	}

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _Deep = __webpack_require__(9);
	
	var params = {
	  marker: 'none',
	  demo: false
	};
	
	var _init = false;
	
	exports["default"] = {
	  get: function get(prop) {
	    return params[prop];
	  },
	  init: function init(data) {
	    if (_init) return;
	    _init = true;
	    (0, _Deep.deepExtend)(params, data);
	  }
	};

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/*eslint no-constant-condition: "off"*/
	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.deepExtend = exports.deepClone = undefined;
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _extend = __webpack_require__(10);
	
	var _extend2 = _interopRequireDefault(_extend);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	/**
	 *
	 * @param {T} item
	 * @return {T}
	 * @template T
	 */
	var deepClone = exports.deepClone = function deepClone(item) {
	  if (!item) {
	    return item;
	  } // null, undefined values check
	
	  var types = [Number, String, Boolean],
	      result = void 0;
	
	  // normalizing primitives if someone did new String('aaa'), or new Number('444');
	  types.forEach(function (type) {
	    if (item instanceof type) {
	      result = type(item);
	    }
	  });
	
	  if (typeof result == 'undefined') {
	    if (Object.prototype.toString.call(item) === '[object Array]') {
	      result = [];
	      item.forEach(function (child, index) {
	        result[index] = deepClone(child);
	      });
	    } else if ((typeof item === 'undefined' ? 'undefined' : _typeof(item)) == 'object') {
	      // testing that this is DOM
	      if (item.nodeType && typeof item.cloneNode == 'function') {
	        result = item.cloneNode(true);
	      } else if (!item.prototype) {
	        // check that this is a literal
	        if (item instanceof Date) {
	          result = new Date(item);
	        } else {
	          // it is an object literal
	          result = {};
	          for (var i in item) {
	            result[i] = deepClone(item[i]);
	          }
	        }
	      } else {
	        // depending what you would like here,
	        // just keep the reference, or create new object
	        if (false) {
	          // would not advice to do that, reason? Read below
	          result = new item.constructor();
	        } else {
	          result = item;
	        }
	      }
	    } else {
	      result = item;
	    }
	  }
	
	  return result;
	};
	
	var deepExtend = exports.deepExtend = _extend2["default"];
	
	exports["default"] = {
	  clone: deepClone,
	  extend: _extend2["default"]
	};

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(Buffer) {/*global Buffer:true*/
	
	/*!
	 * @description Recursive object extending
	 * @author Viacheslav Lotsmanov <lotsmanov89@gmail.com>
	 * @license MIT
	 *
	 * The MIT License (MIT)
	 *
	 * Copyright (c) 2013-2015 Viacheslav Lotsmanov
	 *
	 * Permission is hereby granted, free of charge, to any person obtaining a copy of
	 * this software and associated documentation files (the "Software"), to deal in
	 * the Software without restriction, including without limitation the rights to
	 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
	 * the Software, and to permit persons to whom the Software is furnished to do so,
	 * subject to the following conditions:
	 *
	 * The above copyright notice and this permission notice shall be included in all
	 * copies or substantial portions of the Software.
	 *
	 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
	 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
	 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
	 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
	 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	exports["default"] = deepExtend;
	function isSpecificValue(val) {
	  return val instanceof Buffer || val instanceof Date || val instanceof RegExp;
	}
	
	function cloneSpecificValue(val) {
	  if (val instanceof Buffer) {
	    var x = new Buffer(val.length);
	    val.copy(x);
	    return x;
	  } else if (val instanceof Date) {
	    return new Date(val.getTime());
	  } else if (val instanceof RegExp) {
	    return new RegExp(val);
	  } else {
	    throw new Error('Unexpected situation');
	  }
	}
	
	/**
	 * Recursive cloning array.
	 */
	function deepCloneArray(arr) {
	  var clone = [];
	  arr.forEach(function (item, index) {
	    if ((typeof item === 'undefined' ? 'undefined' : _typeof(item)) === 'object' && item !== null) {
	      if (Array.isArray(item)) {
	        clone[index] = deepCloneArray(item);
	      } else if (isSpecificValue(item)) {
	        clone[index] = cloneSpecificValue(item);
	      } else {
	        clone[index] = deepExtend({}, item);
	      }
	    } else {
	      clone[index] = item;
	    }
	  });
	  return clone;
	}
	
	/**
	 * Extening object that entered in first argument.
	 *
	 * Returns extended object or false if have no target object or incorrect type.
	 *
	 * If you wish to clone source object (without modify it), just use empty new
	 * object as first argument, like this:
	 *   deepExtend({}, yourObj_1, [yourObj_N]);
	 *
	 *   @type {function(...T):(T)}
	 *   @template T
	 */
	function deepExtend() /*obj_1, [obj_2], [obj_N]*/{
	  if (arguments.length < 1 || _typeof(arguments[0]) !== 'object') {
	    return false;
	  }
	
	  if (arguments.length < 2) {
	    return arguments[0];
	  }
	
	  var target = arguments[0];
	
	  // convert arguments to array and cut off target object
	  var args = Array.prototype.slice.call(arguments, 1);
	
	  var val = void 0,
	      src = void 0;
	
	  args.forEach(function (obj) {
	    // skip argument if it is array or isn't object
	    if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object' || Array.isArray(obj)) {
	      return;
	    }
	
	    Object.keys(obj).forEach(function (key) {
	      src = target[key]; // source value
	      val = obj[key]; // new value
	
	      // recursion prevention
	      if (val === target) {
	        return;
	
	        /**
	         * if new value isn't object then just overwrite by new value
	         * instead of extending.
	         */
	      } else if ((typeof val === 'undefined' ? 'undefined' : _typeof(val)) !== 'object' || val === null) {
	        target[key] = val;
	        return;
	
	        // just clone arrays (and recursive clone objects inside)
	      } else if (Array.isArray(val)) {
	        target[key] = deepCloneArray(val);
	        return;
	
	        // custom cloning and overwrite for specific objects
	      } else if (isSpecificValue(val)) {
	        target[key] = cloneSpecificValue(val);
	        return;
	
	        // overwrite by new value if source isn't object or array
	      } else if ((typeof src === 'undefined' ? 'undefined' : _typeof(src)) !== 'object' || src === null || Array.isArray(src)) {
	        target[key] = deepExtend({}, val);
	        return;
	
	        // source value and new value is objects both, extending...
	      } else {
	        target[key] = deepExtend(src, val);
	        return;
	      }
	    });
	  });
	
	  return target;
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(11).Buffer))

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {/*!
	 * The buffer module from node.js, for the browser.
	 *
	 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
	 * @license  MIT
	 */
	/* eslint-disable no-proto */
	
	'use strict'
	
	var base64 = __webpack_require__(12)
	var ieee754 = __webpack_require__(13)
	var isArray = __webpack_require__(14)
	
	exports.Buffer = Buffer
	exports.SlowBuffer = SlowBuffer
	exports.INSPECT_MAX_BYTES = 50
	
	/**
	 * If `Buffer.TYPED_ARRAY_SUPPORT`:
	 *   === true    Use Uint8Array implementation (fastest)
	 *   === false   Use Object implementation (most compatible, even IE6)
	 *
	 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
	 * Opera 11.6+, iOS 4.2+.
	 *
	 * Due to various browser bugs, sometimes the Object implementation will be used even
	 * when the browser supports typed arrays.
	 *
	 * Note:
	 *
	 *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
	 *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
	 *
	 *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
	 *
	 *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
	 *     incorrect length in some situations.
	
	 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
	 * get the Object implementation, which is slower but behaves correctly.
	 */
	Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined
	  ? global.TYPED_ARRAY_SUPPORT
	  : typedArraySupport()
	
	/*
	 * Export kMaxLength after typed array support is determined.
	 */
	exports.kMaxLength = kMaxLength()
	
	function typedArraySupport () {
	  try {
	    var arr = new Uint8Array(1)
	    arr.__proto__ = {__proto__: Uint8Array.prototype, foo: function () { return 42 }}
	    return arr.foo() === 42 && // typed array instances can be augmented
	        typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
	        arr.subarray(1, 1).byteLength === 0 // ie10 has broken `subarray`
	  } catch (e) {
	    return false
	  }
	}
	
	function kMaxLength () {
	  return Buffer.TYPED_ARRAY_SUPPORT
	    ? 0x7fffffff
	    : 0x3fffffff
	}
	
	function createBuffer (that, length) {
	  if (kMaxLength() < length) {
	    throw new RangeError('Invalid typed array length')
	  }
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    // Return an augmented `Uint8Array` instance, for best performance
	    that = new Uint8Array(length)
	    that.__proto__ = Buffer.prototype
	  } else {
	    // Fallback: Return an object instance of the Buffer class
	    if (that === null) {
	      that = new Buffer(length)
	    }
	    that.length = length
	  }
	
	  return that
	}
	
	/**
	 * The Buffer constructor returns instances of `Uint8Array` that have their
	 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
	 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
	 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
	 * returns a single octet.
	 *
	 * The `Uint8Array` prototype remains unmodified.
	 */
	
	function Buffer (arg, encodingOrOffset, length) {
	  if (!Buffer.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer)) {
	    return new Buffer(arg, encodingOrOffset, length)
	  }
	
	  // Common case.
	  if (typeof arg === 'number') {
	    if (typeof encodingOrOffset === 'string') {
	      throw new Error(
	        'If encoding is specified then the first argument must be a string'
	      )
	    }
	    return allocUnsafe(this, arg)
	  }
	  return from(this, arg, encodingOrOffset, length)
	}
	
	Buffer.poolSize = 8192 // not used by this implementation
	
	// TODO: Legacy, not needed anymore. Remove in next major version.
	Buffer._augment = function (arr) {
	  arr.__proto__ = Buffer.prototype
	  return arr
	}
	
	function from (that, value, encodingOrOffset, length) {
	  if (typeof value === 'number') {
	    throw new TypeError('"value" argument must not be a number')
	  }
	
	  if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
	    return fromArrayBuffer(that, value, encodingOrOffset, length)
	  }
	
	  if (typeof value === 'string') {
	    return fromString(that, value, encodingOrOffset)
	  }
	
	  return fromObject(that, value)
	}
	
	/**
	 * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
	 * if value is a number.
	 * Buffer.from(str[, encoding])
	 * Buffer.from(array)
	 * Buffer.from(buffer)
	 * Buffer.from(arrayBuffer[, byteOffset[, length]])
	 **/
	Buffer.from = function (value, encodingOrOffset, length) {
	  return from(null, value, encodingOrOffset, length)
	}
	
	if (Buffer.TYPED_ARRAY_SUPPORT) {
	  Buffer.prototype.__proto__ = Uint8Array.prototype
	  Buffer.__proto__ = Uint8Array
	  if (typeof Symbol !== 'undefined' && Symbol.species &&
	      Buffer[Symbol.species] === Buffer) {
	    // Fix subarray() in ES2016. See: https://github.com/feross/buffer/pull/97
	    Object.defineProperty(Buffer, Symbol.species, {
	      value: null,
	      configurable: true
	    })
	  }
	}
	
	function assertSize (size) {
	  if (typeof size !== 'number') {
	    throw new TypeError('"size" argument must be a number')
	  } else if (size < 0) {
	    throw new RangeError('"size" argument must not be negative')
	  }
	}
	
	function alloc (that, size, fill, encoding) {
	  assertSize(size)
	  if (size <= 0) {
	    return createBuffer(that, size)
	  }
	  if (fill !== undefined) {
	    // Only pay attention to encoding if it's a string. This
	    // prevents accidentally sending in a number that would
	    // be interpretted as a start offset.
	    return typeof encoding === 'string'
	      ? createBuffer(that, size).fill(fill, encoding)
	      : createBuffer(that, size).fill(fill)
	  }
	  return createBuffer(that, size)
	}
	
	/**
	 * Creates a new filled Buffer instance.
	 * alloc(size[, fill[, encoding]])
	 **/
	Buffer.alloc = function (size, fill, encoding) {
	  return alloc(null, size, fill, encoding)
	}
	
	function allocUnsafe (that, size) {
	  assertSize(size)
	  that = createBuffer(that, size < 0 ? 0 : checked(size) | 0)
	  if (!Buffer.TYPED_ARRAY_SUPPORT) {
	    for (var i = 0; i < size; ++i) {
	      that[i] = 0
	    }
	  }
	  return that
	}
	
	/**
	 * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
	 * */
	Buffer.allocUnsafe = function (size) {
	  return allocUnsafe(null, size)
	}
	/**
	 * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
	 */
	Buffer.allocUnsafeSlow = function (size) {
	  return allocUnsafe(null, size)
	}
	
	function fromString (that, string, encoding) {
	  if (typeof encoding !== 'string' || encoding === '') {
	    encoding = 'utf8'
	  }
	
	  if (!Buffer.isEncoding(encoding)) {
	    throw new TypeError('"encoding" must be a valid string encoding')
	  }
	
	  var length = byteLength(string, encoding) | 0
	  that = createBuffer(that, length)
	
	  var actual = that.write(string, encoding)
	
	  if (actual !== length) {
	    // Writing a hex string, for example, that contains invalid characters will
	    // cause everything after the first invalid character to be ignored. (e.g.
	    // 'abxxcd' will be treated as 'ab')
	    that = that.slice(0, actual)
	  }
	
	  return that
	}
	
	function fromArrayLike (that, array) {
	  var length = array.length < 0 ? 0 : checked(array.length) | 0
	  that = createBuffer(that, length)
	  for (var i = 0; i < length; i += 1) {
	    that[i] = array[i] & 255
	  }
	  return that
	}
	
	function fromArrayBuffer (that, array, byteOffset, length) {
	  array.byteLength // this throws if `array` is not a valid ArrayBuffer
	
	  if (byteOffset < 0 || array.byteLength < byteOffset) {
	    throw new RangeError('\'offset\' is out of bounds')
	  }
	
	  if (array.byteLength < byteOffset + (length || 0)) {
	    throw new RangeError('\'length\' is out of bounds')
	  }
	
	  if (byteOffset === undefined && length === undefined) {
	    array = new Uint8Array(array)
	  } else if (length === undefined) {
	    array = new Uint8Array(array, byteOffset)
	  } else {
	    array = new Uint8Array(array, byteOffset, length)
	  }
	
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    // Return an augmented `Uint8Array` instance, for best performance
	    that = array
	    that.__proto__ = Buffer.prototype
	  } else {
	    // Fallback: Return an object instance of the Buffer class
	    that = fromArrayLike(that, array)
	  }
	  return that
	}
	
	function fromObject (that, obj) {
	  if (Buffer.isBuffer(obj)) {
	    var len = checked(obj.length) | 0
	    that = createBuffer(that, len)
	
	    if (that.length === 0) {
	      return that
	    }
	
	    obj.copy(that, 0, 0, len)
	    return that
	  }
	
	  if (obj) {
	    if ((typeof ArrayBuffer !== 'undefined' &&
	        obj.buffer instanceof ArrayBuffer) || 'length' in obj) {
	      if (typeof obj.length !== 'number' || isnan(obj.length)) {
	        return createBuffer(that, 0)
	      }
	      return fromArrayLike(that, obj)
	    }
	
	    if (obj.type === 'Buffer' && isArray(obj.data)) {
	      return fromArrayLike(that, obj.data)
	    }
	  }
	
	  throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.')
	}
	
	function checked (length) {
	  // Note: cannot use `length < kMaxLength()` here because that fails when
	  // length is NaN (which is otherwise coerced to zero.)
	  if (length >= kMaxLength()) {
	    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
	                         'size: 0x' + kMaxLength().toString(16) + ' bytes')
	  }
	  return length | 0
	}
	
	function SlowBuffer (length) {
	  if (+length != length) { // eslint-disable-line eqeqeq
	    length = 0
	  }
	  return Buffer.alloc(+length)
	}
	
	Buffer.isBuffer = function isBuffer (b) {
	  return !!(b != null && b._isBuffer)
	}
	
	Buffer.compare = function compare (a, b) {
	  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
	    throw new TypeError('Arguments must be Buffers')
	  }
	
	  if (a === b) return 0
	
	  var x = a.length
	  var y = b.length
	
	  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
	    if (a[i] !== b[i]) {
	      x = a[i]
	      y = b[i]
	      break
	    }
	  }
	
	  if (x < y) return -1
	  if (y < x) return 1
	  return 0
	}
	
	Buffer.isEncoding = function isEncoding (encoding) {
	  switch (String(encoding).toLowerCase()) {
	    case 'hex':
	    case 'utf8':
	    case 'utf-8':
	    case 'ascii':
	    case 'latin1':
	    case 'binary':
	    case 'base64':
	    case 'ucs2':
	    case 'ucs-2':
	    case 'utf16le':
	    case 'utf-16le':
	      return true
	    default:
	      return false
	  }
	}
	
	Buffer.concat = function concat (list, length) {
	  if (!isArray(list)) {
	    throw new TypeError('"list" argument must be an Array of Buffers')
	  }
	
	  if (list.length === 0) {
	    return Buffer.alloc(0)
	  }
	
	  var i
	  if (length === undefined) {
	    length = 0
	    for (i = 0; i < list.length; ++i) {
	      length += list[i].length
	    }
	  }
	
	  var buffer = Buffer.allocUnsafe(length)
	  var pos = 0
	  for (i = 0; i < list.length; ++i) {
	    var buf = list[i]
	    if (!Buffer.isBuffer(buf)) {
	      throw new TypeError('"list" argument must be an Array of Buffers')
	    }
	    buf.copy(buffer, pos)
	    pos += buf.length
	  }
	  return buffer
	}
	
	function byteLength (string, encoding) {
	  if (Buffer.isBuffer(string)) {
	    return string.length
	  }
	  if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' &&
	      (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
	    return string.byteLength
	  }
	  if (typeof string !== 'string') {
	    string = '' + string
	  }
	
	  var len = string.length
	  if (len === 0) return 0
	
	  // Use a for loop to avoid recursion
	  var loweredCase = false
	  for (;;) {
	    switch (encoding) {
	      case 'ascii':
	      case 'latin1':
	      case 'binary':
	        return len
	      case 'utf8':
	      case 'utf-8':
	      case undefined:
	        return utf8ToBytes(string).length
	      case 'ucs2':
	      case 'ucs-2':
	      case 'utf16le':
	      case 'utf-16le':
	        return len * 2
	      case 'hex':
	        return len >>> 1
	      case 'base64':
	        return base64ToBytes(string).length
	      default:
	        if (loweredCase) return utf8ToBytes(string).length // assume utf8
	        encoding = ('' + encoding).toLowerCase()
	        loweredCase = true
	    }
	  }
	}
	Buffer.byteLength = byteLength
	
	function slowToString (encoding, start, end) {
	  var loweredCase = false
	
	  // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
	  // property of a typed array.
	
	  // This behaves neither like String nor Uint8Array in that we set start/end
	  // to their upper/lower bounds if the value passed is out of range.
	  // undefined is handled specially as per ECMA-262 6th Edition,
	  // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.
	  if (start === undefined || start < 0) {
	    start = 0
	  }
	  // Return early if start > this.length. Done here to prevent potential uint32
	  // coercion fail below.
	  if (start > this.length) {
	    return ''
	  }
	
	  if (end === undefined || end > this.length) {
	    end = this.length
	  }
	
	  if (end <= 0) {
	    return ''
	  }
	
	  // Force coersion to uint32. This will also coerce falsey/NaN values to 0.
	  end >>>= 0
	  start >>>= 0
	
	  if (end <= start) {
	    return ''
	  }
	
	  if (!encoding) encoding = 'utf8'
	
	  while (true) {
	    switch (encoding) {
	      case 'hex':
	        return hexSlice(this, start, end)
	
	      case 'utf8':
	      case 'utf-8':
	        return utf8Slice(this, start, end)
	
	      case 'ascii':
	        return asciiSlice(this, start, end)
	
	      case 'latin1':
	      case 'binary':
	        return latin1Slice(this, start, end)
	
	      case 'base64':
	        return base64Slice(this, start, end)
	
	      case 'ucs2':
	      case 'ucs-2':
	      case 'utf16le':
	      case 'utf-16le':
	        return utf16leSlice(this, start, end)
	
	      default:
	        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
	        encoding = (encoding + '').toLowerCase()
	        loweredCase = true
	    }
	  }
	}
	
	// The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
	// Buffer instances.
	Buffer.prototype._isBuffer = true
	
	function swap (b, n, m) {
	  var i = b[n]
	  b[n] = b[m]
	  b[m] = i
	}
	
	Buffer.prototype.swap16 = function swap16 () {
	  var len = this.length
	  if (len % 2 !== 0) {
	    throw new RangeError('Buffer size must be a multiple of 16-bits')
	  }
	  for (var i = 0; i < len; i += 2) {
	    swap(this, i, i + 1)
	  }
	  return this
	}
	
	Buffer.prototype.swap32 = function swap32 () {
	  var len = this.length
	  if (len % 4 !== 0) {
	    throw new RangeError('Buffer size must be a multiple of 32-bits')
	  }
	  for (var i = 0; i < len; i += 4) {
	    swap(this, i, i + 3)
	    swap(this, i + 1, i + 2)
	  }
	  return this
	}
	
	Buffer.prototype.swap64 = function swap64 () {
	  var len = this.length
	  if (len % 8 !== 0) {
	    throw new RangeError('Buffer size must be a multiple of 64-bits')
	  }
	  for (var i = 0; i < len; i += 8) {
	    swap(this, i, i + 7)
	    swap(this, i + 1, i + 6)
	    swap(this, i + 2, i + 5)
	    swap(this, i + 3, i + 4)
	  }
	  return this
	}
	
	Buffer.prototype.toString = function toString () {
	  var length = this.length | 0
	  if (length === 0) return ''
	  if (arguments.length === 0) return utf8Slice(this, 0, length)
	  return slowToString.apply(this, arguments)
	}
	
	Buffer.prototype.equals = function equals (b) {
	  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
	  if (this === b) return true
	  return Buffer.compare(this, b) === 0
	}
	
	Buffer.prototype.inspect = function inspect () {
	  var str = ''
	  var max = exports.INSPECT_MAX_BYTES
	  if (this.length > 0) {
	    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ')
	    if (this.length > max) str += ' ... '
	  }
	  return '<Buffer ' + str + '>'
	}
	
	Buffer.prototype.compare = function compare (target, start, end, thisStart, thisEnd) {
	  if (!Buffer.isBuffer(target)) {
	    throw new TypeError('Argument must be a Buffer')
	  }
	
	  if (start === undefined) {
	    start = 0
	  }
	  if (end === undefined) {
	    end = target ? target.length : 0
	  }
	  if (thisStart === undefined) {
	    thisStart = 0
	  }
	  if (thisEnd === undefined) {
	    thisEnd = this.length
	  }
	
	  if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
	    throw new RangeError('out of range index')
	  }
	
	  if (thisStart >= thisEnd && start >= end) {
	    return 0
	  }
	  if (thisStart >= thisEnd) {
	    return -1
	  }
	  if (start >= end) {
	    return 1
	  }
	
	  start >>>= 0
	  end >>>= 0
	  thisStart >>>= 0
	  thisEnd >>>= 0
	
	  if (this === target) return 0
	
	  var x = thisEnd - thisStart
	  var y = end - start
	  var len = Math.min(x, y)
	
	  var thisCopy = this.slice(thisStart, thisEnd)
	  var targetCopy = target.slice(start, end)
	
	  for (var i = 0; i < len; ++i) {
	    if (thisCopy[i] !== targetCopy[i]) {
	      x = thisCopy[i]
	      y = targetCopy[i]
	      break
	    }
	  }
	
	  if (x < y) return -1
	  if (y < x) return 1
	  return 0
	}
	
	// Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
	// OR the last index of `val` in `buffer` at offset <= `byteOffset`.
	//
	// Arguments:
	// - buffer - a Buffer to search
	// - val - a string, Buffer, or number
	// - byteOffset - an index into `buffer`; will be clamped to an int32
	// - encoding - an optional encoding, relevant is val is a string
	// - dir - true for indexOf, false for lastIndexOf
	function bidirectionalIndexOf (buffer, val, byteOffset, encoding, dir) {
	  // Empty buffer means no match
	  if (buffer.length === 0) return -1
	
	  // Normalize byteOffset
	  if (typeof byteOffset === 'string') {
	    encoding = byteOffset
	    byteOffset = 0
	  } else if (byteOffset > 0x7fffffff) {
	    byteOffset = 0x7fffffff
	  } else if (byteOffset < -0x80000000) {
	    byteOffset = -0x80000000
	  }
	  byteOffset = +byteOffset  // Coerce to Number.
	  if (isNaN(byteOffset)) {
	    // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
	    byteOffset = dir ? 0 : (buffer.length - 1)
	  }
	
	  // Normalize byteOffset: negative offsets start from the end of the buffer
	  if (byteOffset < 0) byteOffset = buffer.length + byteOffset
	  if (byteOffset >= buffer.length) {
	    if (dir) return -1
	    else byteOffset = buffer.length - 1
	  } else if (byteOffset < 0) {
	    if (dir) byteOffset = 0
	    else return -1
	  }
	
	  // Normalize val
	  if (typeof val === 'string') {
	    val = Buffer.from(val, encoding)
	  }
	
	  // Finally, search either indexOf (if dir is true) or lastIndexOf
	  if (Buffer.isBuffer(val)) {
	    // Special case: looking for empty string/buffer always fails
	    if (val.length === 0) {
	      return -1
	    }
	    return arrayIndexOf(buffer, val, byteOffset, encoding, dir)
	  } else if (typeof val === 'number') {
	    val = val & 0xFF // Search for a byte value [0-255]
	    if (Buffer.TYPED_ARRAY_SUPPORT &&
	        typeof Uint8Array.prototype.indexOf === 'function') {
	      if (dir) {
	        return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset)
	      } else {
	        return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset)
	      }
	    }
	    return arrayIndexOf(buffer, [ val ], byteOffset, encoding, dir)
	  }
	
	  throw new TypeError('val must be string, number or Buffer')
	}
	
	function arrayIndexOf (arr, val, byteOffset, encoding, dir) {
	  var indexSize = 1
	  var arrLength = arr.length
	  var valLength = val.length
	
	  if (encoding !== undefined) {
	    encoding = String(encoding).toLowerCase()
	    if (encoding === 'ucs2' || encoding === 'ucs-2' ||
	        encoding === 'utf16le' || encoding === 'utf-16le') {
	      if (arr.length < 2 || val.length < 2) {
	        return -1
	      }
	      indexSize = 2
	      arrLength /= 2
	      valLength /= 2
	      byteOffset /= 2
	    }
	  }
	
	  function read (buf, i) {
	    if (indexSize === 1) {
	      return buf[i]
	    } else {
	      return buf.readUInt16BE(i * indexSize)
	    }
	  }
	
	  var i
	  if (dir) {
	    var foundIndex = -1
	    for (i = byteOffset; i < arrLength; i++) {
	      if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
	        if (foundIndex === -1) foundIndex = i
	        if (i - foundIndex + 1 === valLength) return foundIndex * indexSize
	      } else {
	        if (foundIndex !== -1) i -= i - foundIndex
	        foundIndex = -1
	      }
	    }
	  } else {
	    if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength
	    for (i = byteOffset; i >= 0; i--) {
	      var found = true
	      for (var j = 0; j < valLength; j++) {
	        if (read(arr, i + j) !== read(val, j)) {
	          found = false
	          break
	        }
	      }
	      if (found) return i
	    }
	  }
	
	  return -1
	}
	
	Buffer.prototype.includes = function includes (val, byteOffset, encoding) {
	  return this.indexOf(val, byteOffset, encoding) !== -1
	}
	
	Buffer.prototype.indexOf = function indexOf (val, byteOffset, encoding) {
	  return bidirectionalIndexOf(this, val, byteOffset, encoding, true)
	}
	
	Buffer.prototype.lastIndexOf = function lastIndexOf (val, byteOffset, encoding) {
	  return bidirectionalIndexOf(this, val, byteOffset, encoding, false)
	}
	
	function hexWrite (buf, string, offset, length) {
	  offset = Number(offset) || 0
	  var remaining = buf.length - offset
	  if (!length) {
	    length = remaining
	  } else {
	    length = Number(length)
	    if (length > remaining) {
	      length = remaining
	    }
	  }
	
	  // must be an even number of digits
	  var strLen = string.length
	  if (strLen % 2 !== 0) throw new TypeError('Invalid hex string')
	
	  if (length > strLen / 2) {
	    length = strLen / 2
	  }
	  for (var i = 0; i < length; ++i) {
	    var parsed = parseInt(string.substr(i * 2, 2), 16)
	    if (isNaN(parsed)) return i
	    buf[offset + i] = parsed
	  }
	  return i
	}
	
	function utf8Write (buf, string, offset, length) {
	  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
	}
	
	function asciiWrite (buf, string, offset, length) {
	  return blitBuffer(asciiToBytes(string), buf, offset, length)
	}
	
	function latin1Write (buf, string, offset, length) {
	  return asciiWrite(buf, string, offset, length)
	}
	
	function base64Write (buf, string, offset, length) {
	  return blitBuffer(base64ToBytes(string), buf, offset, length)
	}
	
	function ucs2Write (buf, string, offset, length) {
	  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
	}
	
	Buffer.prototype.write = function write (string, offset, length, encoding) {
	  // Buffer#write(string)
	  if (offset === undefined) {
	    encoding = 'utf8'
	    length = this.length
	    offset = 0
	  // Buffer#write(string, encoding)
	  } else if (length === undefined && typeof offset === 'string') {
	    encoding = offset
	    length = this.length
	    offset = 0
	  // Buffer#write(string, offset[, length][, encoding])
	  } else if (isFinite(offset)) {
	    offset = offset | 0
	    if (isFinite(length)) {
	      length = length | 0
	      if (encoding === undefined) encoding = 'utf8'
	    } else {
	      encoding = length
	      length = undefined
	    }
	  // legacy write(string, encoding, offset, length) - remove in v0.13
	  } else {
	    throw new Error(
	      'Buffer.write(string, encoding, offset[, length]) is no longer supported'
	    )
	  }
	
	  var remaining = this.length - offset
	  if (length === undefined || length > remaining) length = remaining
	
	  if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
	    throw new RangeError('Attempt to write outside buffer bounds')
	  }
	
	  if (!encoding) encoding = 'utf8'
	
	  var loweredCase = false
	  for (;;) {
	    switch (encoding) {
	      case 'hex':
	        return hexWrite(this, string, offset, length)
	
	      case 'utf8':
	      case 'utf-8':
	        return utf8Write(this, string, offset, length)
	
	      case 'ascii':
	        return asciiWrite(this, string, offset, length)
	
	      case 'latin1':
	      case 'binary':
	        return latin1Write(this, string, offset, length)
	
	      case 'base64':
	        // Warning: maxLength not taken into account in base64Write
	        return base64Write(this, string, offset, length)
	
	      case 'ucs2':
	      case 'ucs-2':
	      case 'utf16le':
	      case 'utf-16le':
	        return ucs2Write(this, string, offset, length)
	
	      default:
	        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
	        encoding = ('' + encoding).toLowerCase()
	        loweredCase = true
	    }
	  }
	}
	
	Buffer.prototype.toJSON = function toJSON () {
	  return {
	    type: 'Buffer',
	    data: Array.prototype.slice.call(this._arr || this, 0)
	  }
	}
	
	function base64Slice (buf, start, end) {
	  if (start === 0 && end === buf.length) {
	    return base64.fromByteArray(buf)
	  } else {
	    return base64.fromByteArray(buf.slice(start, end))
	  }
	}
	
	function utf8Slice (buf, start, end) {
	  end = Math.min(buf.length, end)
	  var res = []
	
	  var i = start
	  while (i < end) {
	    var firstByte = buf[i]
	    var codePoint = null
	    var bytesPerSequence = (firstByte > 0xEF) ? 4
	      : (firstByte > 0xDF) ? 3
	      : (firstByte > 0xBF) ? 2
	      : 1
	
	    if (i + bytesPerSequence <= end) {
	      var secondByte, thirdByte, fourthByte, tempCodePoint
	
	      switch (bytesPerSequence) {
	        case 1:
	          if (firstByte < 0x80) {
	            codePoint = firstByte
	          }
	          break
	        case 2:
	          secondByte = buf[i + 1]
	          if ((secondByte & 0xC0) === 0x80) {
	            tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F)
	            if (tempCodePoint > 0x7F) {
	              codePoint = tempCodePoint
	            }
	          }
	          break
	        case 3:
	          secondByte = buf[i + 1]
	          thirdByte = buf[i + 2]
	          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
	            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F)
	            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
	              codePoint = tempCodePoint
	            }
	          }
	          break
	        case 4:
	          secondByte = buf[i + 1]
	          thirdByte = buf[i + 2]
	          fourthByte = buf[i + 3]
	          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
	            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F)
	            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
	              codePoint = tempCodePoint
	            }
	          }
	      }
	    }
	
	    if (codePoint === null) {
	      // we did not generate a valid codePoint so insert a
	      // replacement char (U+FFFD) and advance only 1 byte
	      codePoint = 0xFFFD
	      bytesPerSequence = 1
	    } else if (codePoint > 0xFFFF) {
	      // encode to utf16 (surrogate pair dance)
	      codePoint -= 0x10000
	      res.push(codePoint >>> 10 & 0x3FF | 0xD800)
	      codePoint = 0xDC00 | codePoint & 0x3FF
	    }
	
	    res.push(codePoint)
	    i += bytesPerSequence
	  }
	
	  return decodeCodePointsArray(res)
	}
	
	// Based on http://stackoverflow.com/a/22747272/680742, the browser with
	// the lowest limit is Chrome, with 0x10000 args.
	// We go 1 magnitude less, for safety
	var MAX_ARGUMENTS_LENGTH = 0x1000
	
	function decodeCodePointsArray (codePoints) {
	  var len = codePoints.length
	  if (len <= MAX_ARGUMENTS_LENGTH) {
	    return String.fromCharCode.apply(String, codePoints) // avoid extra slice()
	  }
	
	  // Decode in chunks to avoid "call stack size exceeded".
	  var res = ''
	  var i = 0
	  while (i < len) {
	    res += String.fromCharCode.apply(
	      String,
	      codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
	    )
	  }
	  return res
	}
	
	function asciiSlice (buf, start, end) {
	  var ret = ''
	  end = Math.min(buf.length, end)
	
	  for (var i = start; i < end; ++i) {
	    ret += String.fromCharCode(buf[i] & 0x7F)
	  }
	  return ret
	}
	
	function latin1Slice (buf, start, end) {
	  var ret = ''
	  end = Math.min(buf.length, end)
	
	  for (var i = start; i < end; ++i) {
	    ret += String.fromCharCode(buf[i])
	  }
	  return ret
	}
	
	function hexSlice (buf, start, end) {
	  var len = buf.length
	
	  if (!start || start < 0) start = 0
	  if (!end || end < 0 || end > len) end = len
	
	  var out = ''
	  for (var i = start; i < end; ++i) {
	    out += toHex(buf[i])
	  }
	  return out
	}
	
	function utf16leSlice (buf, start, end) {
	  var bytes = buf.slice(start, end)
	  var res = ''
	  for (var i = 0; i < bytes.length; i += 2) {
	    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256)
	  }
	  return res
	}
	
	Buffer.prototype.slice = function slice (start, end) {
	  var len = this.length
	  start = ~~start
	  end = end === undefined ? len : ~~end
	
	  if (start < 0) {
	    start += len
	    if (start < 0) start = 0
	  } else if (start > len) {
	    start = len
	  }
	
	  if (end < 0) {
	    end += len
	    if (end < 0) end = 0
	  } else if (end > len) {
	    end = len
	  }
	
	  if (end < start) end = start
	
	  var newBuf
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    newBuf = this.subarray(start, end)
	    newBuf.__proto__ = Buffer.prototype
	  } else {
	    var sliceLen = end - start
	    newBuf = new Buffer(sliceLen, undefined)
	    for (var i = 0; i < sliceLen; ++i) {
	      newBuf[i] = this[i + start]
	    }
	  }
	
	  return newBuf
	}
	
	/*
	 * Need to make sure that buffer isn't trying to write out of bounds.
	 */
	function checkOffset (offset, ext, length) {
	  if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
	  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
	}
	
	Buffer.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
	  offset = offset | 0
	  byteLength = byteLength | 0
	  if (!noAssert) checkOffset(offset, byteLength, this.length)
	
	  var val = this[offset]
	  var mul = 1
	  var i = 0
	  while (++i < byteLength && (mul *= 0x100)) {
	    val += this[offset + i] * mul
	  }
	
	  return val
	}
	
	Buffer.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
	  offset = offset | 0
	  byteLength = byteLength | 0
	  if (!noAssert) {
	    checkOffset(offset, byteLength, this.length)
	  }
	
	  var val = this[offset + --byteLength]
	  var mul = 1
	  while (byteLength > 0 && (mul *= 0x100)) {
	    val += this[offset + --byteLength] * mul
	  }
	
	  return val
	}
	
	Buffer.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 1, this.length)
	  return this[offset]
	}
	
	Buffer.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 2, this.length)
	  return this[offset] | (this[offset + 1] << 8)
	}
	
	Buffer.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 2, this.length)
	  return (this[offset] << 8) | this[offset + 1]
	}
	
	Buffer.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length)
	
	  return ((this[offset]) |
	      (this[offset + 1] << 8) |
	      (this[offset + 2] << 16)) +
	      (this[offset + 3] * 0x1000000)
	}
	
	Buffer.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length)
	
	  return (this[offset] * 0x1000000) +
	    ((this[offset + 1] << 16) |
	    (this[offset + 2] << 8) |
	    this[offset + 3])
	}
	
	Buffer.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
	  offset = offset | 0
	  byteLength = byteLength | 0
	  if (!noAssert) checkOffset(offset, byteLength, this.length)
	
	  var val = this[offset]
	  var mul = 1
	  var i = 0
	  while (++i < byteLength && (mul *= 0x100)) {
	    val += this[offset + i] * mul
	  }
	  mul *= 0x80
	
	  if (val >= mul) val -= Math.pow(2, 8 * byteLength)
	
	  return val
	}
	
	Buffer.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
	  offset = offset | 0
	  byteLength = byteLength | 0
	  if (!noAssert) checkOffset(offset, byteLength, this.length)
	
	  var i = byteLength
	  var mul = 1
	  var val = this[offset + --i]
	  while (i > 0 && (mul *= 0x100)) {
	    val += this[offset + --i] * mul
	  }
	  mul *= 0x80
	
	  if (val >= mul) val -= Math.pow(2, 8 * byteLength)
	
	  return val
	}
	
	Buffer.prototype.readInt8 = function readInt8 (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 1, this.length)
	  if (!(this[offset] & 0x80)) return (this[offset])
	  return ((0xff - this[offset] + 1) * -1)
	}
	
	Buffer.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 2, this.length)
	  var val = this[offset] | (this[offset + 1] << 8)
	  return (val & 0x8000) ? val | 0xFFFF0000 : val
	}
	
	Buffer.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 2, this.length)
	  var val = this[offset + 1] | (this[offset] << 8)
	  return (val & 0x8000) ? val | 0xFFFF0000 : val
	}
	
	Buffer.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length)
	
	  return (this[offset]) |
	    (this[offset + 1] << 8) |
	    (this[offset + 2] << 16) |
	    (this[offset + 3] << 24)
	}
	
	Buffer.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length)
	
	  return (this[offset] << 24) |
	    (this[offset + 1] << 16) |
	    (this[offset + 2] << 8) |
	    (this[offset + 3])
	}
	
	Buffer.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length)
	  return ieee754.read(this, offset, true, 23, 4)
	}
	
	Buffer.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 4, this.length)
	  return ieee754.read(this, offset, false, 23, 4)
	}
	
	Buffer.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 8, this.length)
	  return ieee754.read(this, offset, true, 52, 8)
	}
	
	Buffer.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
	  if (!noAssert) checkOffset(offset, 8, this.length)
	  return ieee754.read(this, offset, false, 52, 8)
	}
	
	function checkInt (buf, value, offset, ext, max, min) {
	  if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance')
	  if (value > max || value < min) throw new RangeError('"value" argument is out of bounds')
	  if (offset + ext > buf.length) throw new RangeError('Index out of range')
	}
	
	Buffer.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
	  value = +value
	  offset = offset | 0
	  byteLength = byteLength | 0
	  if (!noAssert) {
	    var maxBytes = Math.pow(2, 8 * byteLength) - 1
	    checkInt(this, value, offset, byteLength, maxBytes, 0)
	  }
	
	  var mul = 1
	  var i = 0
	  this[offset] = value & 0xFF
	  while (++i < byteLength && (mul *= 0x100)) {
	    this[offset + i] = (value / mul) & 0xFF
	  }
	
	  return offset + byteLength
	}
	
	Buffer.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
	  value = +value
	  offset = offset | 0
	  byteLength = byteLength | 0
	  if (!noAssert) {
	    var maxBytes = Math.pow(2, 8 * byteLength) - 1
	    checkInt(this, value, offset, byteLength, maxBytes, 0)
	  }
	
	  var i = byteLength - 1
	  var mul = 1
	  this[offset + i] = value & 0xFF
	  while (--i >= 0 && (mul *= 0x100)) {
	    this[offset + i] = (value / mul) & 0xFF
	  }
	
	  return offset + byteLength
	}
	
	Buffer.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0)
	  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
	  this[offset] = (value & 0xff)
	  return offset + 1
	}
	
	function objectWriteUInt16 (buf, value, offset, littleEndian) {
	  if (value < 0) value = 0xffff + value + 1
	  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) {
	    buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
	      (littleEndian ? i : 1 - i) * 8
	  }
	}
	
	Buffer.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = (value & 0xff)
	    this[offset + 1] = (value >>> 8)
	  } else {
	    objectWriteUInt16(this, value, offset, true)
	  }
	  return offset + 2
	}
	
	Buffer.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = (value >>> 8)
	    this[offset + 1] = (value & 0xff)
	  } else {
	    objectWriteUInt16(this, value, offset, false)
	  }
	  return offset + 2
	}
	
	function objectWriteUInt32 (buf, value, offset, littleEndian) {
	  if (value < 0) value = 0xffffffff + value + 1
	  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) {
	    buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff
	  }
	}
	
	Buffer.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset + 3] = (value >>> 24)
	    this[offset + 2] = (value >>> 16)
	    this[offset + 1] = (value >>> 8)
	    this[offset] = (value & 0xff)
	  } else {
	    objectWriteUInt32(this, value, offset, true)
	  }
	  return offset + 4
	}
	
	Buffer.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = (value >>> 24)
	    this[offset + 1] = (value >>> 16)
	    this[offset + 2] = (value >>> 8)
	    this[offset + 3] = (value & 0xff)
	  } else {
	    objectWriteUInt32(this, value, offset, false)
	  }
	  return offset + 4
	}
	
	Buffer.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) {
	    var limit = Math.pow(2, 8 * byteLength - 1)
	
	    checkInt(this, value, offset, byteLength, limit - 1, -limit)
	  }
	
	  var i = 0
	  var mul = 1
	  var sub = 0
	  this[offset] = value & 0xFF
	  while (++i < byteLength && (mul *= 0x100)) {
	    if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
	      sub = 1
	    }
	    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
	  }
	
	  return offset + byteLength
	}
	
	Buffer.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) {
	    var limit = Math.pow(2, 8 * byteLength - 1)
	
	    checkInt(this, value, offset, byteLength, limit - 1, -limit)
	  }
	
	  var i = byteLength - 1
	  var mul = 1
	  var sub = 0
	  this[offset + i] = value & 0xFF
	  while (--i >= 0 && (mul *= 0x100)) {
	    if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
	      sub = 1
	    }
	    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
	  }
	
	  return offset + byteLength
	}
	
	Buffer.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80)
	  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
	  if (value < 0) value = 0xff + value + 1
	  this[offset] = (value & 0xff)
	  return offset + 1
	}
	
	Buffer.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = (value & 0xff)
	    this[offset + 1] = (value >>> 8)
	  } else {
	    objectWriteUInt16(this, value, offset, true)
	  }
	  return offset + 2
	}
	
	Buffer.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = (value >>> 8)
	    this[offset + 1] = (value & 0xff)
	  } else {
	    objectWriteUInt16(this, value, offset, false)
	  }
	  return offset + 2
	}
	
	Buffer.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = (value & 0xff)
	    this[offset + 1] = (value >>> 8)
	    this[offset + 2] = (value >>> 16)
	    this[offset + 3] = (value >>> 24)
	  } else {
	    objectWriteUInt32(this, value, offset, true)
	  }
	  return offset + 4
	}
	
	Buffer.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
	  value = +value
	  offset = offset | 0
	  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
	  if (value < 0) value = 0xffffffff + value + 1
	  if (Buffer.TYPED_ARRAY_SUPPORT) {
	    this[offset] = (value >>> 24)
	    this[offset + 1] = (value >>> 16)
	    this[offset + 2] = (value >>> 8)
	    this[offset + 3] = (value & 0xff)
	  } else {
	    objectWriteUInt32(this, value, offset, false)
	  }
	  return offset + 4
	}
	
	function checkIEEE754 (buf, value, offset, ext, max, min) {
	  if (offset + ext > buf.length) throw new RangeError('Index out of range')
	  if (offset < 0) throw new RangeError('Index out of range')
	}
	
	function writeFloat (buf, value, offset, littleEndian, noAssert) {
	  if (!noAssert) {
	    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
	  }
	  ieee754.write(buf, value, offset, littleEndian, 23, 4)
	  return offset + 4
	}
	
	Buffer.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
	  return writeFloat(this, value, offset, true, noAssert)
	}
	
	Buffer.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
	  return writeFloat(this, value, offset, false, noAssert)
	}
	
	function writeDouble (buf, value, offset, littleEndian, noAssert) {
	  if (!noAssert) {
	    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
	  }
	  ieee754.write(buf, value, offset, littleEndian, 52, 8)
	  return offset + 8
	}
	
	Buffer.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
	  return writeDouble(this, value, offset, true, noAssert)
	}
	
	Buffer.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
	  return writeDouble(this, value, offset, false, noAssert)
	}
	
	// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
	Buffer.prototype.copy = function copy (target, targetStart, start, end) {
	  if (!start) start = 0
	  if (!end && end !== 0) end = this.length
	  if (targetStart >= target.length) targetStart = target.length
	  if (!targetStart) targetStart = 0
	  if (end > 0 && end < start) end = start
	
	  // Copy 0 bytes; we're done
	  if (end === start) return 0
	  if (target.length === 0 || this.length === 0) return 0
	
	  // Fatal error conditions
	  if (targetStart < 0) {
	    throw new RangeError('targetStart out of bounds')
	  }
	  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds')
	  if (end < 0) throw new RangeError('sourceEnd out of bounds')
	
	  // Are we oob?
	  if (end > this.length) end = this.length
	  if (target.length - targetStart < end - start) {
	    end = target.length - targetStart + start
	  }
	
	  var len = end - start
	  var i
	
	  if (this === target && start < targetStart && targetStart < end) {
	    // descending copy from end
	    for (i = len - 1; i >= 0; --i) {
	      target[i + targetStart] = this[i + start]
	    }
	  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
	    // ascending copy from start
	    for (i = 0; i < len; ++i) {
	      target[i + targetStart] = this[i + start]
	    }
	  } else {
	    Uint8Array.prototype.set.call(
	      target,
	      this.subarray(start, start + len),
	      targetStart
	    )
	  }
	
	  return len
	}
	
	// Usage:
	//    buffer.fill(number[, offset[, end]])
	//    buffer.fill(buffer[, offset[, end]])
	//    buffer.fill(string[, offset[, end]][, encoding])
	Buffer.prototype.fill = function fill (val, start, end, encoding) {
	  // Handle string cases:
	  if (typeof val === 'string') {
	    if (typeof start === 'string') {
	      encoding = start
	      start = 0
	      end = this.length
	    } else if (typeof end === 'string') {
	      encoding = end
	      end = this.length
	    }
	    if (val.length === 1) {
	      var code = val.charCodeAt(0)
	      if (code < 256) {
	        val = code
	      }
	    }
	    if (encoding !== undefined && typeof encoding !== 'string') {
	      throw new TypeError('encoding must be a string')
	    }
	    if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
	      throw new TypeError('Unknown encoding: ' + encoding)
	    }
	  } else if (typeof val === 'number') {
	    val = val & 255
	  }
	
	  // Invalid ranges are not set to a default, so can range check early.
	  if (start < 0 || this.length < start || this.length < end) {
	    throw new RangeError('Out of range index')
	  }
	
	  if (end <= start) {
	    return this
	  }
	
	  start = start >>> 0
	  end = end === undefined ? this.length : end >>> 0
	
	  if (!val) val = 0
	
	  var i
	  if (typeof val === 'number') {
	    for (i = start; i < end; ++i) {
	      this[i] = val
	    }
	  } else {
	    var bytes = Buffer.isBuffer(val)
	      ? val
	      : utf8ToBytes(new Buffer(val, encoding).toString())
	    var len = bytes.length
	    for (i = 0; i < end - start; ++i) {
	      this[i + start] = bytes[i % len]
	    }
	  }
	
	  return this
	}
	
	// HELPER FUNCTIONS
	// ================
	
	var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g
	
	function base64clean (str) {
	  // Node strips out invalid characters like \n and \t from the string, base64-js does not
	  str = stringtrim(str).replace(INVALID_BASE64_RE, '')
	  // Node converts strings with length < 2 to ''
	  if (str.length < 2) return ''
	  // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
	  while (str.length % 4 !== 0) {
	    str = str + '='
	  }
	  return str
	}
	
	function stringtrim (str) {
	  if (str.trim) return str.trim()
	  return str.replace(/^\s+|\s+$/g, '')
	}
	
	function toHex (n) {
	  if (n < 16) return '0' + n.toString(16)
	  return n.toString(16)
	}
	
	function utf8ToBytes (string, units) {
	  units = units || Infinity
	  var codePoint
	  var length = string.length
	  var leadSurrogate = null
	  var bytes = []
	
	  for (var i = 0; i < length; ++i) {
	    codePoint = string.charCodeAt(i)
	
	    // is surrogate component
	    if (codePoint > 0xD7FF && codePoint < 0xE000) {
	      // last char was a lead
	      if (!leadSurrogate) {
	        // no lead yet
	        if (codePoint > 0xDBFF) {
	          // unexpected trail
	          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
	          continue
	        } else if (i + 1 === length) {
	          // unpaired lead
	          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
	          continue
	        }
	
	        // valid lead
	        leadSurrogate = codePoint
	
	        continue
	      }
	
	      // 2 leads in a row
	      if (codePoint < 0xDC00) {
	        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
	        leadSurrogate = codePoint
	        continue
	      }
	
	      // valid surrogate pair
	      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000
	    } else if (leadSurrogate) {
	      // valid bmp char, but last char was a lead
	      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
	    }
	
	    leadSurrogate = null
	
	    // encode utf8
	    if (codePoint < 0x80) {
	      if ((units -= 1) < 0) break
	      bytes.push(codePoint)
	    } else if (codePoint < 0x800) {
	      if ((units -= 2) < 0) break
	      bytes.push(
	        codePoint >> 0x6 | 0xC0,
	        codePoint & 0x3F | 0x80
	      )
	    } else if (codePoint < 0x10000) {
	      if ((units -= 3) < 0) break
	      bytes.push(
	        codePoint >> 0xC | 0xE0,
	        codePoint >> 0x6 & 0x3F | 0x80,
	        codePoint & 0x3F | 0x80
	      )
	    } else if (codePoint < 0x110000) {
	      if ((units -= 4) < 0) break
	      bytes.push(
	        codePoint >> 0x12 | 0xF0,
	        codePoint >> 0xC & 0x3F | 0x80,
	        codePoint >> 0x6 & 0x3F | 0x80,
	        codePoint & 0x3F | 0x80
	      )
	    } else {
	      throw new Error('Invalid code point')
	    }
	  }
	
	  return bytes
	}
	
	function asciiToBytes (str) {
	  var byteArray = []
	  for (var i = 0; i < str.length; ++i) {
	    // Node's code seems to be doing this and not & 0x7F..
	    byteArray.push(str.charCodeAt(i) & 0xFF)
	  }
	  return byteArray
	}
	
	function utf16leToBytes (str, units) {
	  var c, hi, lo
	  var byteArray = []
	  for (var i = 0; i < str.length; ++i) {
	    if ((units -= 2) < 0) break
	
	    c = str.charCodeAt(i)
	    hi = c >> 8
	    lo = c % 256
	    byteArray.push(lo)
	    byteArray.push(hi)
	  }
	
	  return byteArray
	}
	
	function base64ToBytes (str) {
	  return base64.toByteArray(base64clean(str))
	}
	
	function blitBuffer (src, dst, offset, length) {
	  for (var i = 0; i < length; ++i) {
	    if ((i + offset >= dst.length) || (i >= src.length)) break
	    dst[i + offset] = src[i]
	  }
	  return i
	}
	
	function isnan (val) {
	  return val !== val // eslint-disable-line no-self-compare
	}
	
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 12 */
/***/ function(module, exports) {

	'use strict'
	
	exports.byteLength = byteLength
	exports.toByteArray = toByteArray
	exports.fromByteArray = fromByteArray
	
	var lookup = []
	var revLookup = []
	var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array
	
	var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
	for (var i = 0, len = code.length; i < len; ++i) {
	  lookup[i] = code[i]
	  revLookup[code.charCodeAt(i)] = i
	}
	
	revLookup['-'.charCodeAt(0)] = 62
	revLookup['_'.charCodeAt(0)] = 63
	
	function placeHoldersCount (b64) {
	  var len = b64.length
	  if (len % 4 > 0) {
	    throw new Error('Invalid string. Length must be a multiple of 4')
	  }
	
	  // the number of equal signs (place holders)
	  // if there are two placeholders, than the two characters before it
	  // represent one byte
	  // if there is only one, then the three characters before it represent 2 bytes
	  // this is just a cheap hack to not do indexOf twice
	  return b64[len - 2] === '=' ? 2 : b64[len - 1] === '=' ? 1 : 0
	}
	
	function byteLength (b64) {
	  // base64 is 4/3 + up to two characters of the original data
	  return b64.length * 3 / 4 - placeHoldersCount(b64)
	}
	
	function toByteArray (b64) {
	  var i, j, l, tmp, placeHolders, arr
	  var len = b64.length
	  placeHolders = placeHoldersCount(b64)
	
	  arr = new Arr(len * 3 / 4 - placeHolders)
	
	  // if there are placeholders, only get up to the last complete 4 chars
	  l = placeHolders > 0 ? len - 4 : len
	
	  var L = 0
	
	  for (i = 0, j = 0; i < l; i += 4, j += 3) {
	    tmp = (revLookup[b64.charCodeAt(i)] << 18) | (revLookup[b64.charCodeAt(i + 1)] << 12) | (revLookup[b64.charCodeAt(i + 2)] << 6) | revLookup[b64.charCodeAt(i + 3)]
	    arr[L++] = (tmp >> 16) & 0xFF
	    arr[L++] = (tmp >> 8) & 0xFF
	    arr[L++] = tmp & 0xFF
	  }
	
	  if (placeHolders === 2) {
	    tmp = (revLookup[b64.charCodeAt(i)] << 2) | (revLookup[b64.charCodeAt(i + 1)] >> 4)
	    arr[L++] = tmp & 0xFF
	  } else if (placeHolders === 1) {
	    tmp = (revLookup[b64.charCodeAt(i)] << 10) | (revLookup[b64.charCodeAt(i + 1)] << 4) | (revLookup[b64.charCodeAt(i + 2)] >> 2)
	    arr[L++] = (tmp >> 8) & 0xFF
	    arr[L++] = tmp & 0xFF
	  }
	
	  return arr
	}
	
	function tripletToBase64 (num) {
	  return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F]
	}
	
	function encodeChunk (uint8, start, end) {
	  var tmp
	  var output = []
	  for (var i = start; i < end; i += 3) {
	    tmp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2])
	    output.push(tripletToBase64(tmp))
	  }
	  return output.join('')
	}
	
	function fromByteArray (uint8) {
	  var tmp
	  var len = uint8.length
	  var extraBytes = len % 3 // if we have 1 byte left, pad 2 bytes
	  var output = ''
	  var parts = []
	  var maxChunkLength = 16383 // must be multiple of 3
	
	  // go through the array every three bytes, we'll deal with trailing stuff later
	  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
	    parts.push(encodeChunk(uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)))
	  }
	
	  // pad the end with zeros, but make sure to not forget the extra bytes
	  if (extraBytes === 1) {
	    tmp = uint8[len - 1]
	    output += lookup[tmp >> 2]
	    output += lookup[(tmp << 4) & 0x3F]
	    output += '=='
	  } else if (extraBytes === 2) {
	    tmp = (uint8[len - 2] << 8) + (uint8[len - 1])
	    output += lookup[tmp >> 10]
	    output += lookup[(tmp >> 4) & 0x3F]
	    output += lookup[(tmp << 2) & 0x3F]
	    output += '='
	  }
	
	  parts.push(output)
	
	  return parts.join('')
	}


/***/ },
/* 13 */
/***/ function(module, exports) {

	exports.read = function (buffer, offset, isLE, mLen, nBytes) {
	  var e, m
	  var eLen = nBytes * 8 - mLen - 1
	  var eMax = (1 << eLen) - 1
	  var eBias = eMax >> 1
	  var nBits = -7
	  var i = isLE ? (nBytes - 1) : 0
	  var d = isLE ? -1 : 1
	  var s = buffer[offset + i]
	
	  i += d
	
	  e = s & ((1 << (-nBits)) - 1)
	  s >>= (-nBits)
	  nBits += eLen
	  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}
	
	  m = e & ((1 << (-nBits)) - 1)
	  e >>= (-nBits)
	  nBits += mLen
	  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}
	
	  if (e === 0) {
	    e = 1 - eBias
	  } else if (e === eMax) {
	    return m ? NaN : ((s ? -1 : 1) * Infinity)
	  } else {
	    m = m + Math.pow(2, mLen)
	    e = e - eBias
	  }
	  return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
	}
	
	exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
	  var e, m, c
	  var eLen = nBytes * 8 - mLen - 1
	  var eMax = (1 << eLen) - 1
	  var eBias = eMax >> 1
	  var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0)
	  var i = isLE ? 0 : (nBytes - 1)
	  var d = isLE ? 1 : -1
	  var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0
	
	  value = Math.abs(value)
	
	  if (isNaN(value) || value === Infinity) {
	    m = isNaN(value) ? 1 : 0
	    e = eMax
	  } else {
	    e = Math.floor(Math.log(value) / Math.LN2)
	    if (value * (c = Math.pow(2, -e)) < 1) {
	      e--
	      c *= 2
	    }
	    if (e + eBias >= 1) {
	      value += rt / c
	    } else {
	      value += rt * Math.pow(2, 1 - eBias)
	    }
	    if (value * c >= 2) {
	      e++
	      c /= 2
	    }
	
	    if (e + eBias >= eMax) {
	      m = 0
	      e = eMax
	    } else if (e + eBias >= 1) {
	      m = (value * c - 1) * Math.pow(2, mLen)
	      e = e + eBias
	    } else {
	      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen)
	      e = 0
	    }
	  }
	
	  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}
	
	  e = (e << mLen) | m
	  eLen += mLen
	  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}
	
	  buffer[offset + i - d] |= s * 128
	}


/***/ },
/* 14 */
/***/ function(module, exports) {

	var toString = {}.toString;
	
	module.exports = Array.isArray || function (arr) {
	  return toString.call(arr) == '[object Array]';
	};


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Query = __webpack_require__(16);
	
	var _Query2 = _interopRequireDefault(_Query);
	
	var _Results = __webpack_require__(22);
	
	var _Results2 = _interopRequireDefault(_Results);
	
	var _default = __webpack_require__(21);
	
	var _Deep = __webpack_require__(9);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Controller = function () {
	  /**
	   *
	   * @param {EventStore} eventStore
	   */
	  function Controller(eventStore) {
	    _classCallCheck(this, Controller);
	
	    /**
	     *
	     * @type {EventStore}
	     * @private
	     */
	    this._eventStore = eventStore;
	
	    /**
	     * @type {FormatMainForm}
	     * @private
	     */
	    this._formData = (0, _Deep.deepExtend)(_default.defaultFormData);
	
	    this.__init().__initEvents();
	  }
	
	  /**
	   *
	   * @return {Controller}
	   * @private
	   */
	
	
	  _createClass(Controller, [{
	    key: '__init',
	    value: function __init() {
	      /**
	       *
	       * @type {ControllerResults}
	       * @private
	       */
	      this._results = new _Results2["default"](this._eventStore);
	      /**
	       *
	       * @type {?Query}
	       * @private
	       */
	      this._query = null;
	      return this;
	    }
	
	    /**
	     *
	     * @private
	     */
	
	  }, {
	    key: '__updateResults',
	    value: function __updateResults() {
	      var query = new _Query2["default"](this._formData);
	
	      if ((!this._query || !query.isSameFull(this._query)) && query.$canBestDeals) {
	
	        var loading = false;
	
	        // Eve.dispatch('Controller::load:results');
	        //Общий запрос без дат
	        loading = this._results.common(query) || loading;
	
	        //Запрос как он есть
	        loading = this._results.bestDeals(query) || loading;
	
	        //Запрос по месяцам
	        loading = this._results.monthly(query) || loading;
	
	        // запрос по дате отправления
	        loading = this._results.fromDateToMonthly(query) || loading;
	
	        // Запрос к api перелетов если полностью выбраны все данные
	        loading = this._results.fullSearch(query) || loading;
	
	        if (loading) this._eventStore.dispatch('Controller::load:results', this._results.show(query));else this._eventStore.dispatch('Controller::show:results', this._results.show(query));
	      }
	
	      this._query = query;
	    }
	
	    /**
	     *
	     * @return {Controller}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	
	      this._eventStore.on(['Form::change'], function (event) {
	
	        this._formData = event.detail;
	        this.__updateResults();
	      }.bind(this));
	
	      this._eventStore.on(['Results::add:city'], function (event) {
	        var iata = event.detail.iata;
	
	        if (this._results.addCity(this._query, iata)) {
	          this._eventStore.dispatch('Controller::load:results', this._results.show(this._query));
	        } else {
	          this._eventStore.dispatch('Controller::show:results', this._results.show(this._query));
	        }
	      }.bind(this));
	
	      this._eventStore.on(['Results::done:request'], function ( /*{detail:{query:Query,results:Results}}*/event) {
	
	        if (this._query && this._query.isSame(event.detail.query)) {
	          this._eventStore.dispatch('Controller::show:results', event.detail.results.show(this._query));
	        }
	      }.bind(this));
	
	      return this;
	    }
	  }]);
	
	  return Controller;
	}();
	
	exports["default"] = Controller;

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Format = __webpack_require__(17);
	
	var _Format2 = _interopRequireDefault(_Format);
	
	var _Deep = __webpack_require__(9);
	
	var _default = __webpack_require__(21);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Query = function () {
	  /**
	   *
	   * @param {FormatMainForm} [options]
	   */
	  function Query(options) {
	    _classCallCheck(this, Query);
	
	    /**
	     *
	     * @type {?string}
	     * @private
	     */
	    this._fromCity = null;
	    /**
	     *
	     * @type {?string}
	     * @private
	     */
	    this._toCity = null;
	    /**
	     *
	     * @type {?string}
	     * @private
	     */
	    this._toCountry = null;
	    /**
	     *
	     * @type {'E'|'B'}
	     * @private
	     */
	    this._type = 'E';
	
	    /**
	     *
	     * @type {boolean}
	     * @private
	     */
	    this._roundtrip = true;
	    /**
	     *
	     * @type {string|number}
	     * @private
	     */
	    this._transfer = 'any';
	    /**
	     *
	     * @type {string|number}
	     * @private
	     */
	    this._duration = 'any';
	    /**
	     *
	     * @type {?Date}
	     * @private
	     */
	    this._departure = null;
	    /**
	     *
	     * @type {?Date}
	     * @private
	     */
	    this._departureDate = null;
	    /**
	     *
	     * @type {?Date}
	     * @private
	     */
	    this._returnDate = null;
	
	    this.__parseOptions(options);
	  }
	
	  /**
	   *
	   * @return {string|null}
	   */
	
	
	  _createClass(Query, [{
	    key: '__name',
	
	
	    /**
	     *
	     * @param transfer
	     * @param query
	     * @return {string|null}
	     * @private
	     */
	    value: function __name(transfer) {
	      var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.$bestDealsQuery;
	
	      if (!query || !query.origin || !query.destination_countries && !query.destinations) return null;
	
	      var str = query.origin + '_' + query.destination_countries + '_' + query.destinations;
	      str += '_' + (query.direct_flights ? 'true' : 'false');
	      str += '_' + (query.roundtrip_flights ? 'true' : 'false');
	      str += '_' + transfer;
	
	      if (query.roundtrip_flights) str += '_' + query.stay_from + '_' + query.stay_to;
	
	      return str;
	    }
	
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: '__key',
	
	
	    // /**
	    //  *
	    //  * @return {string}
	    //  */
	    // get $key_withDeparture() {
	    //   if (!this._key_withDeparture) {
	    //     this._key_withDeparture = `${this.$key}_${this.departure ? this.departure.getTime() : 'null'}`;
	    //   }
	    //   return this._key_withDeparture;
	    // }
	
	    // /**
	    //  *
	    //  * @return {string}
	    //  */
	    // get $key_anyDuration() {
	    //   if (!this._key_anyDuration) {
	    //     this._key_anyDuration = this.__key(undefined, null);
	    //   }
	    //   return this._key_anyDuration;
	    // }
	
	    // /**
	    //  *
	    //  * @return {string}
	    //  */
	    // get $key_anyCountFlares() {
	    //   if (!this._key_anyCountFlares) {
	    //     this._key_anyCountFlares = this.__key(Infinity);
	    //   }
	    //   return this._key_anyCountFlares;
	    // }
	
	    // /**
	    //  *
	    //  * @return {string|*}
	    //  */
	    // get $key_anyCountFlaresAndDuration() {
	    //   if (!this._key_anyCountFlaresAndDuration) {
	    //     this._key_anyCountFlaresAndDuration = this.__key(Infinity, null);
	    //   }
	    //   return this._key_anyCountFlaresAndDuration;
	    // }
	
	    /**
	     *
	     * @param {number|Infinity} [countFlares]
	     * @param {number|string|null} [duration]
	     * @return {string}
	     * @private
	     */
	    value: function __key() {
	      var countFlares = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.$countFlares;
	      var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.$duration;
	
	      var str = this.$origin + '_' + this.$destinationCity + '_' + this.$destinationCountry;
	      str += '_' + this.$isRoundTrip;
	      str += '_' + countFlares;
	
	      if (this.$isRoundTrip) str += '_' + duration;
	
	      return str;
	    }
	
	    /**
	     *
	     * @param {FormatMainForm} [options]
	     * @private
	     */
	
	  }, {
	    key: '__parseOptions',
	    value: function __parseOptions(options) {
	      /**
	       * @type {FormatMainForm}
	       */
	      var tempOptions = (0, _Deep.deepExtend)({}, _default.defaultFormData, options);
	      /**
	       *
	       * @type {FormatMainForm}
	       * @private
	       */
	      this._options = tempOptions;
	
	      this.$origin = tempOptions.inputs.from;
	      this.$destinationCity = tempOptions.inputs.to;
	      this.$destinationCountry = tempOptions.inputs.toCountry;
	
	      this.$duration = tempOptions.selectors.duration;
	      this.$transfer = tempOptions.selectors.transfer;
	      this.$roundTrip = tempOptions.selectors.route;
	      this.$type = tempOptions.selectors.type;
	
	      this.$departure = tempOptions.date.start;
	      this.$return = tempOptions.date.end;
	    }
	
	    /**
	     *
	     * @return {?string}
	     */
	
	  }, {
	    key: 'queryDate',
	    value: function queryDate(name, query) {
	      if (this['_' + name + 'Date'] instanceof Date) {
	        var dateString = _Format2["default"].query.bestDeals.dateToString(this['_' + name + 'Date']);
	        query[name + '_date_from'] = dateString;
	        query[name + '_date_to'] = dateString;
	      }
	    }
	
	    /**
	     *
	     * @return {OttDealsQuery}
	     * @private
	     */
	
	  }, {
	    key: '__basicBestDealsQuery',
	    value: function __basicBestDealsQuery() {
	      if (this._queryBestDealsBasic) return (0, _Deep.deepClone)(this._queryBestDealsBasic);
	      /**
	       *
	       * @type {OttDealsQuery}
	       */
	      var query = {
	        origin: this.$origin,
	        deals_limit: 500
	      };
	
	      // console.log('__basicBestDealsQuery()');
	
	      if (this.$destinationCity) query.destinations = this.$destinationCity;else if (this.$destinationCountry) query.destination_countries = this.$destinationCountry;
	
	      if (this.$isRoundTrip) {
	        query.roundtrip_flights = true;
	      }
	
	      if (this.$isDirect) {
	        query.direct_flights = true;
	      }
	
	      var duration = this.$duration;
	      if (this.$isRoundTrip && duration) {
	        query.stay_from = duration;
	        query.stay_to = duration;
	      }
	      /**
	       *
	       * @type {OttDealsQuery}
	       * @private
	       */
	      this._queryBestDealsBasic = query;
	
	      return (0, _Deep.deepClone)(query);
	    }
	
	    /**
	     *
	     * @return {OttDealsQuery}
	     */
	
	  }, {
	    key: 'bestDealsQueryMonthly',
	
	
	    /**
	     *
	     * @param {number} year
	     * @param {number} month
	     * @return {OttDealsQuery}
	     */
	    value: function bestDealsQueryMonthly(year, month) {
	
	      var query = this.__basicBestDealsQuery();
	
	      query.departure_date_from = _Format2["default"].query.bestDeals.dateToString(new Date(year, month, 1));
	      query.departure_date_to = _Format2["default"].query.bestDeals.dateToString(new Date(year, month + 1, 0));
	      query.group_by_date = true;
	
	      return query;
	    }
	
	    /**
	     *
	     * @param {number} year
	     * @param {number} month
	     * @return {OttDealsQuery}
	     */
	
	  }, {
	    key: 'bestDealsQueryReturnMonthly',
	    value: function bestDealsQueryReturnMonthly(year, month) {
	      var query = this.$bestDealsQuery;
	
	      query.return_date_from = _Format2["default"].query.bestDeals.dateToString(new Date(year, month, 1));
	      query.return_date_to = _Format2["default"].query.bestDeals.dateToString(new Date(year, month + 1, 0));
	
	      return query;
	    }
	
	    // static dateToSearching
	
	    /**
	     *
	     * @return {?OttFlightQuery}
	     */
	
	  }, {
	    key: 'isSame',
	
	
	    // /**
	    //  *
	    //  * @return {boolean}
	    //  */
	    // get $canSearchFlight() {
	    //   if (this.origin && this.destinationCity) {
	    //     if (this.isRoundTrip) {
	    //       return !!(this.$departure && this.$return);
	    //     } else {
	    //       return !!(this.$departure);
	    //     }
	    //   }
	    //   return false;
	    // }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {boolean}
	     */
	    value: function isSame(query) {
	      return this.$key == query.$key;
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'isSameFull',
	    value: function isSameFull(query) {
	      return this.$nameFull == query.$nameFull;
	    }
	
	    // /**
	    //  *
	    //  * @param {Query} query
	    //  * @return {boolean}
	    //  */
	    // isSameWithDate(query) {
	    //   return this.nameWithDate == query.nameWithDate;
	    // }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'clone',
	
	
	    /**
	     *
	     * @param {FormatMainForm} options
	     * @return {Query}
	     */
	    value: function clone(options) {
	      var newOptions = (0, _Deep.deepExtend)({}, this._options, options);
	      return new Query(newOptions);
	    }
	  }, {
	    key: '$nameWithDate',
	    get: function get() {
	      /**
	       * @this {Query}
	       */
	      if (this._nameWithDate) return this._nameWithDate;
	      /**
	       *
	       * @type {string|null}
	       */
	      var name = this.$name;
	
	      name += '_' + (this.$departure ? this.$departure.getTime() : 'null');
	      name += '_' + (this.$return ? this.$return.getTime() : 'null');
	      /**
	       *
	       * @type {string|null}
	       * @private
	       */
	      this._nameWithDate = name;
	
	      return name;
	    }
	
	    /**
	     *
	     * @return {string|null}
	     */
	
	  }, {
	    key: '$nameFull',
	    get: function get() {
	      if (this._nameFull) return this._nameFull;
	      /**
	       *
	       * @type {string|null}
	       */
	      var name = this.$nameWithDate;
	
	      if (this.$inMonth) {
	        var month = this.$month;
	        name += '_' + month.year + '_' + month.month;
	      } else {
	        name += '_false';
	      }
	      /**
	       *
	       * @type {string|null}
	       * @private
	       */
	      this._nameFull = name;
	
	      return name;
	    }
	
	    /**
	     *
	     * @return {string|null}
	     */
	
	  }, {
	    key: '$nameWithAnyTransfer',
	    get: function get() {
	      if (this.$transfer == 'any') return this.$name;
	      return this.__name('any');
	    }
	
	    /**
	     *
	     * @return {string|null}
	     */
	
	  }, {
	    key: '$nameWithAnyDuration',
	    get: function get() {
	      if (this.$duration === null) return this.$name;
	      var query = this.$bestDealsQuery;
	      query.stay_from = undefined;
	      query.stay_to = undefined;
	      return this.__name(this.$transfer, query);
	    }
	
	    /**
	     *
	     * @return {string|null}
	     */
	
	  }, {
	    key: '$nameWithAnyDurationAndTransfer',
	    get: function get() {
	      if (this.$duration === null) return this.$name;
	      var query = this.$bestDealsQuery;
	      query.stay_from = undefined;
	      query.stay_to = undefined;
	      return this.__name('any', query);
	    }
	
	    /**
	     *
	     * @return {string|null}
	     */
	
	  }, {
	    key: '$name',
	    get: function get() {
	      if (this._name) {
	        return this._name;
	      }
	      /**
	       *
	       * @type {string|null}
	       * @private
	       */
	      this._name = this.__name(this.$transfer);
	      return this._name;
	    }
	  }, {
	    key: '$key',
	    get: function get() {
	      if (!this._key) {
	        this._key = this.__key();
	      }
	      return this._key;
	    }
	  }, {
	    key: '$origin',
	    get: function get() {
	      return typeof this._fromCity === 'string' && this._fromCity ? this._fromCity : null;
	    }
	
	    /**
	     *
	     * @param {?string} from
	     */
	    ,
	    set: function set(from) {
	      this._fromCity = from;
	    }
	
	    /**
	     *
	     * @return {?string}
	     */
	
	  }, {
	    key: '$destinationCity',
	    get: function get() {
	      return typeof this._toCity === 'string' && this._toCity ? this._toCity : null;
	    }
	
	    /**
	     *
	     * @return {?string}
	     */
	    ,
	
	
	    /**
	     *
	     * @param {?string} cityIata
	     */
	    set: function set(cityIata) {
	      if (cityIata) {
	        this._toCity = cityIata;
	        this._toCountry = null;
	      } else {
	        this._toCity = null;
	      }
	    }
	
	    /**
	     *
	     * @param {?string} countryIata
	     */
	
	  }, {
	    key: '$destinationCountry',
	    get: function get() {
	      return typeof this._toCountry === 'string' && this._toCountry ? this._toCountry : null;
	    },
	    set: function set(countryIata) {
	      if (countryIata) {
	        this._toCity = null;
	        this._toCountry = countryIata;
	      } else {
	        this._toCountry = null;
	      }
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: '$isDestinationCountry',
	    get: function get() {
	      return !!this.$destinationCountry;
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: '$isDirect',
	    get: function get() {
	      return this.$countFlares == 1;
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: '$isRoundTrip',
	    get: function get() {
	      return !!this._roundtrip;
	    }
	
	    /**
	     *
	     * @param {?boolean} roundTrip
	     */
	
	  }, {
	    key: '$roundTrip',
	    set: function set(roundTrip) {
	      this._roundtrip = !!roundTrip;
	    }
	
	    // /**
	    //  *
	    //  * @return {?boolean}
	    //  */
	    // get $roundTrip() {
	    //   return this._roundtrip;
	    // }
	
	    /**
	     *
	     * @return {number|string}
	     */
	
	  }, {
	    key: '$transfer',
	    get: function get() {
	      return this._transfer;
	    }
	
	    /**
	     *
	     * @param {number|string} transfer
	     */
	    ,
	    set: function set(transfer) {
	      switch (transfer) {
	        case 'none':
	        case 0:
	          this._transfer = 'none';
	          break;
	        case 1:
	          this._transfer = 1;
	          break;
	        default:
	          this._transfer = 'any';
	      }
	    }
	
	    /**
	     *
	     * @return {?string}
	     */
	
	  }, {
	    key: '$type',
	    get: function get() {
	      switch (this._type) {
	        case 'E':
	        case 'e':
	          return 'E';
	
	        case 'B':
	        case 'b':
	          return 'B';
	        default:
	          return 'E';
	      }
	    }
	
	    /**
	     *
	     * @param {?string} type
	     */
	    ,
	    set: function set(type) {
	      switch (type) {
	        case 'E':
	        case 'e':
	          this._type = 'E';
	          break;
	        case 'B':
	        case 'b':
	          this._type = 'B';
	          break;
	        default:
	          this._type = 'E';
	      }
	    }
	
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '$countFlares',
	    get: function get() {
	      // Без пересадок
	      if (this._transfer == 'none') return 1;
	      // Любые пересадки
	      else if (this._transfer == 'any') return Infinity;
	      // Не больше одной
	      return 2;
	    }
	
	    /**
	     *
	     * @return {null|number|string}
	     */
	
	  }, {
	    key: '$duration',
	    get: function get() {
	      var duration = parseInt(this._duration, 10);
	      return this._duration == 'any' || !duration ? null : duration;
	    }
	
	    /**
	     *
	     * @param {null|number|string} duration
	     */
	    ,
	    set: function set(duration) {
	      if (!duration) this._duration = 'any';else this._duration = duration;
	    }
	
	    /**
	     *
	     * @param {?Date} date
	     */
	
	  }, {
	    key: '$departure',
	    set: function set(date) {
	      this._departureDate = date instanceof Date ? date : null;
	    }
	
	    /**
	     *
	     * @return {?Date}
	     */
	    ,
	    get: function get() {
	      return this._departureDate instanceof Date ? this._departureDate : null;
	    }
	
	    // /**
	    //  *
	    //  * @return {string}
	    //  */
	    // get $departureBestDeal() {
	    //   let departure = this.departure;
	    //   return departure instanceof Date ? Format.query.bestDeals.dateToString(departure) : '';
	    // }
	
	    /**
	     *
	     * @return {?number}
	     */
	
	  }, {
	    key: '$departureDay',
	    get: function get() {
	      var date = this.$departure;
	      return date instanceof Date ? date.getDate() : null;
	    }
	
	    /**
	     *
	     * @return {?number}
	     */
	
	  }, {
	    key: '$departureMonth',
	    get: function get() {
	      var date = this.$departure;
	      return date instanceof Date ? date.getMonth() : null;
	    }
	
	    /**
	     *
	     * @return {?number}
	     */
	
	  }, {
	    key: '$departureYear',
	    get: function get() {
	      var date = this.$departure;
	      return date instanceof Date ? date.getFullYear() : null;
	    }
	
	    /**
	     *
	     * @param {?Date} date
	     */
	
	  }, {
	    key: '$return',
	    set: function set(date) {
	      this._returnDate = date instanceof Date ? date : null;
	    }
	
	    /**
	     *
	     * @return {?Date}
	     */
	    ,
	    get: function get() {
	      return this._returnDate instanceof Date ? this._returnDate : null;
	    }
	
	    /**
	     *
	     * @return {?number}
	     */
	
	  }, {
	    key: '$returnDay',
	    get: function get() {
	      var date = this.$return;
	      return date instanceof Date ? date.getDate() : null;
	    }
	
	    /**
	     *
	     * @return {?number}
	     */
	
	  }, {
	    key: '$returnMonth',
	    get: function get() {
	      var date = this.$return;
	      return date instanceof Date ? date.getMonth() : null;
	    }
	
	    /**
	     *
	     * @return {?number}
	     */
	
	  }, {
	    key: '$returnYear',
	    get: function get() {
	      var date = this.$return;
	      return date instanceof Date ? date.getFullYear() : null;
	    }
	  }, {
	    key: '$commonBestDealsQuery',
	    get: function get() {
	      return this.__basicBestDealsQuery();
	    }
	
	    /**
	     *
	     * @return {OttDealsQuery}
	     */
	
	  }, {
	    key: '$bestDealsQuery',
	    get: function get() {
	
	      if (this._queryBestDeals) return (0, _Deep.deepClone)(this._queryBestDeals);
	      /**
	       *
	       * @type {OttDealsQuery}
	       */
	      var query = this.__basicBestDealsQuery();
	      this.queryDate('return', query);
	      this.queryDate('departure', query);
	      /**
	       *
	       * @type {OttDealsQuery}
	       * @private
	       */
	      this._queryBestDeals = query;
	      return (0, _Deep.deepClone)(query);
	    }
	  }, {
	    key: '$searchingQuery',
	    get: function get() {
	      /**
	       * @this {Query}
	       */
	      /**
	       *
	       * @type {OttFlightQuery}
	       */
	      var query = {
	        cs: this.$type
	      };
	
	      if (this.$origin && this.$destinationCity) {
	        // console.log('(this.origin && this.destinationCity)');
	        if (this.$isRoundTrip) {
	          // console.log('(this.isRoundTrip)');
	          if (this.$departure && this.$return) {
	            // console.log('(this.departure && this.return)');
	            query.route = '' + _Format2["default"].query.searching.dateToString(this.$departure) + this.$origin + this.$destinationCity + _Format2["default"].query.searching.dateToString(this.$return);
	          } else {
	            // console.log('else (this.departure && this.return)');
	            return null;
	          }
	        } else {
	          // console.log('else (this.isRoundTrip)');
	          if (this.$departure) {
	            // console.log('(this.departure)');
	            query.route = '' + _Format2["default"].query.searching.dateToString(this.$departure) + this.$origin + this.$destinationCity;
	          } else {
	            // console.log('else (this.departure)');
	            return null;
	          }
	        }
	      } else {
	        // console.log('else (this.origin && this.destinationCity)');
	        return null;
	      }
	      return query;
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: '$canBestDeals',
	    get: function get() {
	      return !!(this.$origin && (this.$destinationCity || this.$destinationCountry));
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: '$needFromDayToMonthly',
	    get: function get() {
	      return !!(this.$canBestDeals && this.$isRoundTrip && this.$departure && !this.$return);
	    }
	  }, {
	    key: '$isComplete',
	    get: function get() {
	      /**
	       * @this {Query}
	       */
	      if (!this.$origin) return false;
	      if (!this.$destinationCity && !this.$destinationCountry) return false;
	
	      if (this.$isRoundTrip) return !!(this.$departure && this.$return);
	      return !!this.$departure;
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: '$inMonth',
	    get: function get() {
	      return !!this.$month;
	    }
	
	    /**
	     *
	     * @return {?{year:numbermonth:number}}
	     */
	
	  }, {
	    key: '$month',
	    get: function get() {
	      return this._options && this._options.date ? this._options.date.inMonth : null;
	    }
	  }]);
	
	  return Query;
	}();
	
	exports["default"] = Query;

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.zeroNumber = zeroNumber;
	exports.money = money;
	exports.price = price;
	exports.dateFromString = dateFromString;
	exports.dateFromQueryString = dateFromQueryString;
	exports.stringFromDate = stringFromDate;
	exports.timeFromDate = timeFromDate;
	exports.dateToShotString = dateToShotString;
	exports.dateDurations = dateDurations;
	exports.dateToFlightString = dateToFlightString;
	exports.declOfNum = declOfNum;
	exports.dateString = dateString;
	exports.isNative = isNative;
	
	var _l8n = __webpack_require__(18);
	
	var _Request = __webpack_require__(19);
	
	var _Request2 = _interopRequireDefault(_Request);
	
	var _Url = __webpack_require__(20);
	
	var _Url2 = _interopRequireDefault(_Url);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	var currentYear = new Date().getFullYear();
	/**
	 *
	 * @param {number} num
	 * @return {string}
	 */
	function zeroNumber(num) {
	  return '' + (num > 9 ? num : '0' + num);
	}
	/**
	 *
	 * @param moneyNumber
	 * @param aftrerComma
	 * @param comma
	 * @param delimiter
	 * @return {string}
	 */
	function money(moneyNumber, aftrerComma, comma, delimiter) {
	  aftrerComma = isNaN(aftrerComma = Math.abs(aftrerComma)) ? 2 : aftrerComma;
	  comma = comma == undefined ? '.' : comma;
	  delimiter = delimiter == undefined ? ',' : delimiter;
	
	  var s = moneyNumber < 0 ? '-' : '',
	      i = String(parseInt(moneyNumber = Math.abs(Number(moneyNumber) || 0).toFixed(aftrerComma))),
	      j = i.length;
	  j = (j = i.length) > 3 ? j % 3 : 0;
	
	  return s + (j ? i.substr(0, j) + delimiter : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + delimiter) + (aftrerComma ? comma + Math.abs(moneyNumber - i).toFixed(aftrerComma).slice(2) : '');
	}
	/**
	 *
	 * @param price
	 * @param currency
	 * @return {string}
	 */
	function price(price, currency) {
	  var currencyString = 'USD';
	  switch (currency) {
	    case 'RUB':
	      currencyString = '&#8381;';
	      break;
	  }
	
	  return money(price, 0, '.', ' ') + ' ' + currencyString;
	}
	/**
	 *
	 * @param dateString
	 * @return {?Date}
	 */
	function dateFromString(dateString) {
	  if (!dateString || dateString.length != 8) return null;
	
	  var year = parseInt(dateString.substr(0, 4), 10),
	      month = parseInt(dateString.substr(4, 2), 10) - 1,
	      day = parseInt(dateString.substr(6, 2), 10);
	  return new Date(year, month, day);
	}
	
	/**
	 *
	 * @param {string} dateString - 2017-01-17
	 * @return {Date}
	 */
	function dateFromQueryString(dateString) {
	  var dates = dateString.split('-');
	  return new Date(dates[0], parseInt(dates[1], 10) - 1, dates[2]);
	}
	/**
	 *
	 * @param {Date} date
	 * @return {string} - YYYY-MM-DD
	 */
	function stringFromDate(date) {
	  var year = date.getFullYear(),
	      month = date.getMonth() + 1,
	      day = date.getDate();
	
	  return year + '-' + zeroNumber(month) + '-' + zeroNumber(day);
	}
	/**
	 *
	 * @param {Date} date
	 * @return {string} : HH:MM
	 */
	function timeFromDate(date) {
	  return zeroNumber(date.getHours()) + ':' + zeroNumber(date.getMinutes());
	}
	/**
	 *
	 * @param {Date} date
	 * @param {boolean} [hideYear]
	 * @return {string}
	 */
	function dateToShotString( /*Date*/date, hideYear) {
	  var year = date.getFullYear(),
	      month = date.getMonth(),
	      day = date.getDate();
	
	  return day + ' ' + _l8n.monthNameShort[month] + (year !== currentYear && !hideYear ? ', ' + year : '');
	}
	
	function dateDurations(startDate, endDate) {
	  var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
	  return Math.ceil(timeDiff / (1000 * 3600 * 24));
	}
	/**
	 *
	 * @param date
	 * @return {string}
	 */
	function dateToFlightString(date) {
	  var month = date.getMonth() + 1,
	      day = date.getDate();
	
	  return '' + zeroNumber(day) + zeroNumber(month);
	}
	
	function declOfNum(number, titles) {
	  number = Math.abs(number);
	  var cases = [2, 0, 1, 1, 1, 2];
	  return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
	}
	
	/**
	 *
	 * @param {Date} date
	 * @return {string}
	 */
	function dateString(date) {
	  var month = _l8n.monthName[date.getMonth()],
	      day = date.getDate(),
	      year = date.getFullYear();
	  return day + ' ' + month + (currentYear != year ? ' ' + year : '');
	}
	
	exports["default"] = {
	  url: {
	    country: {
	      low: _Url2["default"].lowCountry
	    }
	  },
	  query: {
	    bestDeals: {
	      dateToString: stringFromDate,
	      dateToTimeString: timeFromDate,
	      stringToDate: dateFromString
	    },
	    searching: {
	      dateToString: function dateToString(date) {
	        if (date instanceof Date) {
	          var month = date.getMonth() + 1,
	              day = date.getDate();
	          month = month > 9 ? month : '0' + month;
	          day = day > 9 ? day : '0' + day;
	          return '' + day + month;
	        }
	        return null;
	      }
	    }
	  },
	  request: _Request2["default"]
	};
	function isNative(what) {
	  return (/\{\s+\[native code\]/.test(Function.prototype.toString.call(what))
	  );
	}

/***/ },
/* 18 */
/***/ function(module, exports) {

	'use strict';
	/**
	 *
	 * @type {string[]}
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var monthNames = exports.monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
	/**
	 *
	 * @type {string[]}
	 */
	var daysOfWeekShort = exports.daysOfWeekShort = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПН', 'СБ', 'ВС'];
	
	var monthNameShort = exports.monthNameShort = ['янв', 'фев', 'март', 'апр', 'май', 'июнь', 'июль', 'авг', 'сен', 'окт', 'ноя', 'дек'];
	var monthName = exports.monthName = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
	
	exports["default"] = {
	  monthName: monthName,
	  monthNames: monthNames,
	  monthNameShort: monthNameShort,
	  daysOfWeekShort: daysOfWeekShort
	};

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.queryString = queryString;
	exports.urlString = urlString;
	
	var _Deep = __webpack_require__(9);
	
	/**
	 *
	 * @param {Object} options
	 * @param {Object} [adds={}]
	 * @return {string}
	 */
	function queryString(options) {
	  var adds = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	  var query = '',
	      arr = [];
	  options = (0, _Deep.deepExtend)({}, adds, options);
	  for (var propName in options) {
	    if (options.hasOwnProperty(propName)) {
	      if (options[propName] !== null) {
	        arr.push(encodeURI(propName) + '=' + encodeURI(options[propName]));
	      }
	    }
	  }
	
	  query += arr.join('&');
	  return query;
	}
	/**
	 *
	 * @param {string} url
	 * @param {Object} options
	 * @param {Object} [adds={}]
	 * @return {string}
	 */
	function urlString(url, options) {
	  var adds = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	
	  var query = queryString(options, adds);
	  if (url.indexOf('?') > -1) return url + '&' + query;
	  return url + '?' + query;
	}
	
	exports["default"] = {
	  queryString: queryString,
	  urlString: urlString
	};

/***/ },
/* 20 */
/***/ function(module, exports) {

	'use strict';
	/**
	 *
	 * @param {string} countryEnName
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.lowCountry = lowCountry;
	function lowCountry(countryEnName) {
	  var str = countryEnName.toLowerCase();
	  str = str.replace(/[^\s0-9a-zA-Z-]+/g, '').replace(/[\s_-]+/g, '-');
	
	  return str;
	}
	
	exports["default"] = {
	  lowCountry: lowCountry
	};

/***/ },
/* 21 */
/***/ function(module, exports) {

	'use strict';
	
	/**
	 *
	 * @type {FormatMainForm}
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var defaultFormData = exports.defaultFormData = {
	  inputs: {
	    from: null,
	    to: null,
	    toCountry: null
	  },
	  selectors: {
	    duration: 'any',
	    route: true,
	    transfer: 'any',
	    type: 'E'
	  },
	  date: {
	    start: null,
	    end: null,
	    inMonth: null
	  }
	};
	
	/**
	 *
	 * @type {ThemeColor}
	 */
	var defaultThemeColor = exports.defaultThemeColor = {
	  WidgetBG: '#878D96',
	  WidgetTX: '#FFFFFF',
	
	  WidgetBtn: '#FFD41E',
	  WidgetBtnTX: '#616161',
	
	  WidgetPriceGradient: {
	    low: '#a5e3af',
	    middle: '#EFF9E8',
	    high: '#ffc2a3'
	  }
	};
	
	/**
	 *
	 * @type {ThemeOptions}
	 */
	var defaultTheme = exports.defaultTheme = {
	  width: null,
	  color: defaultThemeColor
	};

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	/*eslint no-unused-vars: ["error", { "args": "none" }]*/
	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _OneTwoTrip = __webpack_require__(23);
	
	var _Date = __webpack_require__(40);
	
	var _Store = __webpack_require__(41);
	
	var _Store2 = _interopRequireDefault(_Store);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 *
	 * @typedef {Object} ResultsOptions
	 * @property {number} [count]
	 * @property {boolean} [monthly]
	 * @property {Object.<string, number>} [months]
	 * @property {string[]} [fromDates]
	 * @property {string[]} [done]
	 */
	
	/**
	 *
	 * @param {Response} responseA
	 * @param {Response} responseB
	 * @return {number}
	 */
	function sortResponseByPrice( /*Response*/responseA, /*Response*/responseB) {
	  return responseA.$price - responseB.$price;
	}
	
	var ControllerResults = function () {
	  /**
	   *
	   * @param {EventStore} eventStore
	   */
	  function ControllerResults(eventStore) {
	    _classCallCheck(this, ControllerResults);
	
	    this._eventStore = eventStore;
	    /**
	     *
	     * @type {Object.<string,{count:number, monthly:boolean}>}
	     * @private
	     */
	    this._requests = {};
	    /**
	     *
	     * @type {Store}
	     * @private
	     */
	    this._store = new _Store2["default"]();
	  }
	
	  _createClass(ControllerResults, [{
	    key: '__complete',
	    value: function __complete(query) {
	      this._eventStore.dispatch('Results::done:request', {
	        results: this,
	        query: query
	      });
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'common',
	    value: function common(query) {
	      return this.__bestDeals(query, query.$commonBestDealsQuery);
	    }
	
	    /**
	     *
	     * @param {Query} query
	     */
	
	  }, {
	    key: 'bestDeals',
	    value: function bestDeals(query) {
	      return this.__bestDeals(query, query.$bestDealsQuery);
	    }
	
	    /**
	     *
	     * Возвращает boolean (true - будем грузить результаты false - результаты загружены)
	     * @param {Query} query
	     * @param {OttDealsQuery} params
	     * @param {function} [callback]
	     * @return {boolean}
	     * @private
	     */
	
	  }, {
	    key: '__bestDeals',
	    value: function __bestDeals(query, params, callback) {
	
	      this._query = query;
	
	      var request = this._store.getRequest(query);
	
	      if (request.isLoadRequest(params)) {
	        return true;
	      }
	
	      if (request.isDoneRequest(params)) {
	        return false;
	      }
	
	      request.add();
	
	      request.loadRequest(params);
	
	      /**
	       * @this {ControllerResults}
	       */
	      (0, _OneTwoTrip.bestDealsAsync)(params, {
	        success: function (response) {
	          this.__parseDealsResponse(response, query);
	          request.doneRequest(params);
	          if (callback) callback();
	        }.bind(this),
	        always: function () {
	          // this.__removeRequest(query);
	          request.done().isFull(this.__complete.bind(this, query));
	        }.bind(this)
	      });
	
	      return true;
	    }
	
	    /**
	     * Загружаем данные по месяцам и дням
	     * @param {Query} query
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'monthly',
	    value: function monthly(query) {
	      this._query = query;
	
	      var request = this._store.getRequest(query);
	      return _Date.monthList.reduce(function ( /*boolean*/loaded, /*({year:number,month:number})*/month) {
	        /**
	         * @this {ControllerResults}
	         */
	        if (request.isDoneMonth(month.year + '_' + month.month)) {
	          // если месяц уже загружен
	          return loaded || false;
	        } else {
	          return this.__bestDeals(query, query.bestDealsQueryMonthly(month.year, month.month)) || loaded;
	        }
	      }.bind(this), false);
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'fromDateToMonthly',
	    value: function fromDateToMonthly(query) {
	      this._query = query;
	
	      if (!query.$needFromDayToMonthly) return false;
	
	      return _Date.monthList.reduce(function ( /*boolean*/loaded, /*{year:number,month:number}*/month) {
	        /**
	         * @this {ControllerResults}
	         */
	        return this.__bestDeals(query, query.bestDealsQueryReturnMonthly(month.year, month.month)) || loaded;
	      }.bind(this), false);
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'fullSearch',
	    value: function fullSearch(query) {
	      this._query = query;
	
	      if (query.$searchingQuery) {
	
	        var request = this._store.getRequest(query),
	            params = query.$searchingQuery;
	
	        if (request.isDoneRequest(params)) return false;
	
	        request.add();
	        // this.__addRequest(query);
	
	        (0, _OneTwoTrip.searchingFlights)(params, {
	          success: function (response) {
	            /**
	             * @this {ControllerResults}
	             */
	            this.__parseSearchingString(response, query);
	            request.doneRequest(params);
	          }.bind(this),
	          always: function () {
	            /**
	             * @this {ControllerResults}
	             */
	            request.done().isFull(this.__complete.bind(this, query));
	          }.bind(this)
	        });
	
	        return true;
	      }
	      return false;
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @param {string} cityIata
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'addCity',
	    value: function addCity(query, cityIata) {
	
	      var cityQuery = query.clone({
	        inputs: {
	          toCountry: null,
	          to: cityIata
	        }
	      });
	
	      var cityParams = cityQuery.$searchingQuery;
	
	      if (cityParams) {
	        var request = this._store.getRequest(query);
	        if (request.isDoneRequest(cityParams)) return false;
	        request.add();
	
	        (0, _OneTwoTrip.searchingFlights)(cityParams, {
	          success: function (response) {
	            /**
	             * @this {ControllerResults}
	             */
	            this.__parseSearchingString(response, query, cityIata);
	            request.doneRequest(cityParams);
	          }.bind(this),
	          always: function () {
	            /**
	             * @this {ControllerResults}
	             */
	            request.done().isFull(this.__complete.bind(this, query));
	          }.bind(this)
	        });
	        return true;
	      }
	      return false;
	    }
	
	    /**
	     *
	     * @param {{[status]:string [data]:{[frs]:SearchOptions[]}}} response
	     * @param {Query} query
	     * @param {string} [cityIata]
	     * @private
	     */
	
	  }, {
	    key: '__parseSearchingString',
	    value: function __parseSearchingString(response, query) {
	      var cityIata = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : query.$destinationCity;
	
	      // let jsonResponse = JSON.parse(response);
	      if (!response || !response || !response.data || response.status !== 'success' || !response.data.frs) return;
	
	      response.data.frs.forEach(function ( /* SearchOptions */fare) {
	        /**
	         *
	         * @type {TripOptions}
	         */
	        var bestDealFare = (0, _OneTwoTrip.convertSearchFareToBestDeal)(fare);
	
	        bestDealFare.from = query.$origin;
	        bestDealFare.to = cityIata;
	
	        this._store.addData(query, bestDealFare);
	
	        // this._result.addBestDeals(query, bestDealFare);
	      }.bind(this));
	    }
	
	    /**
	     *
	     * @param response
	     * @param query
	     * @private
	     */
	
	  }, {
	    key: '__parseDealsResponse',
	    value: function __parseDealsResponse(response, query) {
	      if (!response || typeof response === 'string') return;
	
	      response.forEach(function (item) {
	        this._store.addData(query, item);
	        // this._result.addBestDeals(query, item);
	      }, this);
	    }
	
	    /**
	     *
	     * @typedef {Object}            ResultsShow
	     * @property {boolean}            loading
	     * @property {Store}              store
	     * @property {Query}              query
	     * @property {StorePrices}        price
	     * @property {Array.<Response>}   results
	     */
	
	    /**
	     *
	     * @param {Query} query
	     * @return {ResultsShow}
	     */
	
	  }, {
	    key: 'show',
	    value: function show(query) {
	      // return this._result.response(query);
	      /**
	       *
	       * @type {number}
	       */
	      var count = 0;
	      /**
	       *
	       * @type {boolean}
	       */
	      var isCountry = query.$isDestinationCountry;
	      /**
	       *
	       * @type {Array<string>}
	       */
	      var cities = [];
	      /**
	       *
	       * @type {Array<Response>}
	       */
	      var minCity = [];
	      /**
	       *
	       * @type {Array.<Response>}
	       */
	      var results = this._store.getResponse(query)
	      // Сортируем выдачу
	      .sort(sortResponseByPrice)
	      // Фильтруем выдачу
	      .filter(function ( /*Response*/response) {
	
	        if (count >= 3) return false;
	
	        if (!isCountry) {
	          count++;
	          return count < 4;
	        }
	
	        if (cities.indexOf(response.$to) < 0) {
	          count++;
	          cities.push(response.$to);
	          return count < 4;
	        }
	
	        minCity.push(response);
	
	        return false;
	      }, this);
	
	      if (minCity.length > 0) {
	        switch (results.length) {
	          case 0:
	            results = results.concat(minCity.splice(0, 3)).sort(sortResponseByPrice);
	            break;
	          case 1:
	            results = results.concat(minCity.splice(0, 2)).sort(sortResponseByPrice);
	            break;
	          case 2:
	            results = results.concat(minCity.splice(0, 1)).sort(sortResponseByPrice);
	            break;
	        }
	      }
	
	      return {
	        store: this._store,
	        loading: !this._store.getRequest(query).isFull(),
	        query: query,
	        price: this._store.getPrices(query),
	        results: results
	      };
	    }
	  }, {
	    key: 'price',
	    value: function price(query) {}
	  }]);
	
	  return ControllerResults;
	}();
	
	exports["default"] = ControllerResults;

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.findPlaceAsync = findPlaceAsync;
	exports.bestDealsAsync = bestDealsAsync;
	exports.searchingFlights = searchingFlights;
	exports.generateLink = generateLink;
	exports.link = link;
	exports.linkCity = linkCity;
	exports.linkCountry = linkCountry;
	exports.convertSearchFareToBestDeal = convertSearchFareToBestDeal;
	
	var _Request = __webpack_require__(24);
	
	var _Deep = __webpack_require__(9);
	
	var _Reference = __webpack_require__(28);
	
	var _country = __webpack_require__(34);
	
	var _Format = __webpack_require__(17);
	
	var _Format2 = _interopRequireDefault(_Format);
	
	var _params = __webpack_require__(8);
	
	var _params2 = _interopRequireDefault(_params);
	
	var _demo = __webpack_require__(39);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	/**
	 * @typedef {Object}  OttFlightQuery
	 * @property {string}   route - 1401MOWWAS
	 * @property {string}   cs  - "E","B"
	 */
	
	/**
	 *
	 * @type {{string:string}}
	 */
	var ottSuggestPlacesCache = {};
	
	/**
	 * Поиск города по названию
	 * @param findText
	 * @param callback
	 * @return {number|string}
	 */
	function findPlaceAsync(findText, callback) {
	
	  if (ottSuggestPlacesCache.hasOwnProperty(findText)) {
	    return setTimeout(function () {
	      callback(ottSuggestPlacesCache[findText], findText);
	    }, 10);
	  }
	
	  return (0, _Request.ajax)('https://partner.onetwotrip.com/_api/avia/suggestPlaces', {
	    data: { query: findText },
	    responseType: 'json',
	    success: function success(response) {
	      ottSuggestPlacesCache[findText] = response;
	      callback(response, findText);
	    }
	  });
	
	  // return ajax(`https://partner.onetwotrip.com/_api/avia/suggestPlaces?query=${findText}`, function (response, id) {
	  //   ottSuggestPlacesCache[findText] = response;
	  //   callback(response, findText);
	  // });
	}
	
	/**
	 *
	 * @param {OttDealsQuery} queryOptions
	 * @param {RequestOptions} options
	 * @return {number|string}
	 */
	function bestDealsAsync(queryOptions, options) {
	  options.data = queryOptions;
	
	  if (_params2["default"].get('demo')) {
	    setTimeout(function () {
	      options.success(_demo.DEALS);
	      options.always();
	    }, 10);
	    return 0;
	  }
	
	  return (0, _Request.jsonp)('https://www.onetwotrip.com/_api/deals_v4/directApiTop', options);
	}
	/**
	 *
	 * @param {OttFlightQuery} params
	 * @param {RequestOptions} options
	 * @return {number}
	 */
	function searchingFlights(params, options) {
	
	  options.data = (0, _Deep.deepExtend)({}, params, {
	    ad: 1,
	    cn: 0,
	    'in': 0,
	    marker: _params2["default"].get('marker'),
	    srcmarker: 'doinstant_ru'
	  });
	
	  if (_params2["default"].get('demo')) {
	    setTimeout(function () {
	      options.success(_demo.FLIGHTS);
	      options.always();
	    }, 10);
	    return 0;
	  }
	
	  return (0, _Request.ajax)('https://partner.onetwotrip.com/_api/searching/startSync', options);
	}
	
	/**
	 *
	 * @param {string} fromIata
	 * @param {string} toIata
	 * @param {Date} startDate
	 * @param {Date} [endDate]
	 * @param {string} [type=E]
	 * @return {string}
	 */
	function generateLink(fromIata, toIata, startDate) {
	  var endDate = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
	  var type = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'E';
	
	
	  var startDay = startDate.getDate(),
	      startMonth = startDate.getMonth() + 1;
	
	  startDay = startDay < 10 ? '0' + startDay : startDay;
	  startMonth = startMonth < 10 ? '0' + startMonth : startMonth;
	
	  var from = (0, _Reference.byIATA)(fromIata),
	      to = (0, _Reference.byIATA)(toIata);
	
	  var fromName = from ? from.city.en : '',
	      toName = to ? to.city.en : '';
	
	  var str = '' + startDay + startMonth + fromIata + toIata;
	
	  if (endDate) {
	
	    var endDay = endDate.getDate(),
	        endMonth = endDate.getMonth() + 1;
	
	    endDay = endDay < 10 ? '0' + endDay : endDay;
	    endMonth = endMonth < 10 ? '0' + endMonth : endMonth;
	
	    str += '' + endDay + endMonth;
	  }
	
	  if (type != 'E') str += '&' + type;
	
	  return 'https://www.onetwotrip.com/ru/aviabilety/' + fromName + '-' + toName + '_' + fromIata + '-' + toIata + '/?s=true#' + str;
	}
	/**
	 *
	 * @param {Query|{}} [query]
	 * @param {Response|null} [response=null]
	 * @return {string}
	 */
	function link() {
	  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	  var response = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	
	  if (query.$isDestinationCountry) return linkCountry(query);
	  return linkCity(query, response);
	}
	/**
	 *
	 * @param {Query} query
	 * @param {Response|null} response
	 * @return {string}
	 */
	function linkCity(query, response) {
	  var params = query.$searchingQuery;
	
	  var route = void 0,
	      fromIata = void 0,
	      toIata = void 0;
	
	  if (params) {
	    route = params.route;
	    fromIata = query.$origin;
	    toIata = query.$destinationCity;
	  } else if (response) {
	    route = response.$route;
	    fromIata = response.$from;
	    toIata = response.$to;
	  } else {
	    return 'javascript:void(0);';
	  }
	
	  var fromName = (0, _Reference.byIATA)(fromIata),
	      toName = (0, _Reference.byIATA)(toIata);
	
	  return 'https://www.onetwotrip.com/ru/aviabilety/' + fromName + '-' + toName + '_' + fromIata + '-' + toIata + '/?s=true#' + route;
	}
	/**
	 *
	 * @param {Query} query
	 * @return {string}
	 */
	function linkCountry(query) {
	  var country = (0, _country.byIATA)(query.$destinationCountry);
	  if (country) {
	    return 'https://www.onetwotrip.com/ru/aviabilety/to-' + _Format2["default"].url.country.low(country.country.en) + '_' + query.$destinationCountry + '/';
	  }
	  return 'javascript:void(0);';
	}
	
	/**
	 * @typedef {Object}  DirectionOptions
	 * @property {string}   airCompany    : "IK"
	 * @property {boolean}  continued     : false
	 * @property {string}   endDate       : "20161225"
	 * @property {string}   endDateTime   : "2016-12-25T05:25:00.000Z"
	 * @property {string}   endTerminal   : "1"
	 * @property {string}   endTime       : "0825"
	 * @property {string}   fic           : "OSALE"
	 * @property {string}   flightNumber  : "8167"
	 * @property {string}   flightTime    : "0120"
	 * @property {string}   from          : "SVO"
	 * @property {Object}   fromGeo       : Object
	 * @property {string}   journeyTime   : "0120"
	 * @property {string}   startDate     : "20161225"
	 * @property {string}   startDateTime : "2016-12-25T04:05:00.000Z"
	 * @property {string}   startTerminal : "D"
	 * @property {string}   startTime     : "0705"
	 * @property {string}   to            : "LED"
	 * @property {Object}   toGeo         : Object
	 */
	
	/**
	 * @typedef {Object}                    TripOptions
	 * @property {string}                     currency    : "RUB"
	 * @property {number}                     dateCreated : 1482178767364
	 * @property {string}                     departDate  : "20161225"
	 * @property {Array<DirectionOptions>[]}  directions
	 * @property {string}                     fareType    : "direct" | "change"
	 * @property {string}                     from        : "MOW"
	 * @property {boolean}                    is2OW4RT    : false
	 * @property {number}                     price       : 1470
	 * @property {string}                     returnDate  : "20161225"
	 * @property {string}                     to          : "LED"
	 * @property {string}                     to_country  : "RU"
	 */
	
	/**
	 * @typedef {Object}  OttSearchResponseTrip
	 * @property {string}   airCmp -     "S7"
	 * @property {string}   basis -    "WBSOW"
	 * @property {string}   directionId -    "0"
	 * @property {string}   endDate -    "2017-02-02T19:20:00"
	 * @property {string}   fltNm -    "1027"
	 * @property {string}   from -     "DME"
	 * @property {string}   isDeparture -    "true"
	 * @property {string}   jrnTm -    "0220"
	 * @property {string}   key -    "170020170202DMEAER1027S7GH"
	 * @property {string}   plane -    "738"
	 * @property {string}   startDate -    "2017-02-02T17:00:00"
	 * @property {string}   to -     "AER"
	 */
	
	/**
	 * @typedef {Object}                        OttSearchResponseDirection
	 * @property {Array<OttSearchResponseTrip>}   trips
	 */
	/**
	 * @typedef {Object}                             SearchOptions
	 * @property {string}                             airCmp  -  "S7"
	 * @property {string}                             currency  -  "RUB"
	 * @property {Array<OttSearchResponseDirection>}  dirs  -  Array[1]
	 * @property {number}                             id  -  0
	 * @property {string}                             key  - "170020170202DMEAER1027S7GH"
	 * @property {string}                             price  - "1700"
	 * @property {string}                             url  - "https://www.onetwotrip.com/ru/f/?env=7&meta_redirectid=1-1884-0-2#/book/42;-9q7r!;3fc29927-8c20-4977-b1ad-9741f78dd8b7;c165f39f-7cd6-42e2-9afc-ed5fca608c27;pGkB10wptEBDgoMZTJgvAg==;YcO5DcKvw5fDtcKTw51fwqXDqHwRJgPChDBXw5BswqJrPQh+GcOZPQXDmWXDui3ClcO+asOgw7zCusKGZybDiUMiB8KYA8KFw7PChcOfwqYrAw9WwqglaMKdBWHCi2jDkFbDhMKwPz%2FDuV1Hw6DCpSHDvMKLw5DCksO1MsKCCUjCusO7ZSXClTspw5F5wq4TQV15w5MHFhjCj8OWa8ORwq8VJcOWJmTDiifCrB8Rw64UKsOWRsORw6jDn3jCjsOqwolfW8OYw41yC8O+wqg9wprDpXbCocODcMOvw6gMF2rCtcOiFMO%2FRMK%2Fw5Z5w60=;;6e05+)D*u;0;RK;chhknkcvg;1,0,0;;tghgttcn;;;S;;m!,m!,1q,0,1q,1q,kk,m!,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1q,1q,RUB;h,RUB;;,,,;E;ryM6T5B:e5B*e1pbp%279$_s7_gh41027738_0PCn%2Fgt)8I*aI*6t)8t)4++++++++++++I(5I%27iI%27gI%2766_WE___WBSOW?meta=1"
	 * @property {string}                             urlMob  -  "https://m.onetwotrip.com/ru/avia/b/?env=7&meta_redirectid=1-1884-0-2#/book/42;-9q7r!;3fc29927-8c20-4977-b1ad-9741f78dd8b7;c165f39f-7cd6-42e2-9afc-ed5fca608c27;pGkB10wptEBDgoMZTJgvAg==;YcO5DcKvw5fDtcKTw51fwqXDqHwRJgPChDBXw5BswqJrPQh+GcOZPQXDmWXDui3ClcO+asOgw7zCusKGZybDiUMiB8KYA8KFw7PChcOfwqYrAw9WwqglaMKdBWHCi2jDkFbDhMKwPz%2FDuV1Hw6DCpSHDvMKLw5DCksO1MsKCCUjCusO7ZSXClTspw5F5wq4TQV15w5MHFhjCj8OWa8ORwq8VJcOWJmTDiifCrB8Rw64UKsOWRsORw6jDn3jCjsOqwolfW8OYw41yC8O+wqg9wprDpXbCocODcMOvw6gMF2rCtcOiFMO%2FRMK%2Fw5Z5w60=;;6e05+)D*u;0;RK;chhknkcvg;1,0,0;;tghgttcn;;;S;;m!,m!,1q,0,1q,1q,kk,m!,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1q,1q,RUB;h,RUB;;,,,;E;ryM6T5B:e5B*e1pbp%279$_s7_gh41027738_0PCn%2Fgt)8I*aI*6t)8t)4++++++++++++I(5I%27iI%27gI%2766_WE___WBSOW?meta=1
	 */
	/**
	 *
	 * @param   {SearchOptions} fare
	 * @return  {TripOptions}
	 */
	
	function convertSearchFareToBestDeal(fare) {
	  /**
	   *
	   * @type {TripOptions}
	   */
	  var ret = {
	    price: parseInt(fare.price, 10),
	    currency: fare.currency
	  };
	
	  ret.directions = fare.dirs.map(function ( /*OttSearchResponseDirection*/direction, x) {
	    return direction.trips.map(function ( /*OttSearchResponseTrip*/trip, y) {
	      /**
	       * @type {{year: number, month: number, day: number, hours: number, minutes: number, seconds: number}}
	       */
	      var parsedStartDate = parseDateString(trip.startDate),
	          parsedEndDate = parseDateString(trip.endDate);
	
	      if (x == 0 && y == 0) {
	        ret.departDate = '' + parsedStartDate.year + parsedStartDate.month + parsedStartDate.day;
	      }
	
	      if (y == 0) {
	        ret.returnDate = '' + parsedStartDate.year + parsedStartDate.month + parsedStartDate.day;
	      }
	
	      // if (x == 1 && y == (trips.length - 1)) {
	      //   ret.returnDate = `${parsedEndDate.year}${parsedEndDate.month}${parsedEndDate.day}`
	      // }
	      /**
	       *
	       * @type {DirectionOptions}
	       */
	      return {
	
	        airCompany: trip.airCmp,
	        // continued     : false,
	        endDate: '' + parsedEndDate.year + parsedEndDate.month + parsedEndDate.day,
	        // endDateTime   : "2016-12-25T05:25:00.000Z",
	        // endTerminal   : "1 ",
	        endTime: '' + parsedEndDate.hours + parsedEndDate.minutes, //"0825",
	        // fic           : "OSALE",
	        flightNumber: trip.fltNm,
	        flightTime: trip.jrnTm, //"0120"
	        from: trip.from,
	        // fromGeo : Object,
	        journeyTime: trip.jrnTm,
	        startDate: '' + parsedStartDate.year + parsedStartDate.month + parsedStartDate.day,
	        // startDateTime : "2016-12-25T04:05:00.000Z",
	        // startTerminal : "D",
	        startTime: '' + parsedStartDate.hours + parsedStartDate.minutes, //"0825",
	        to: trip.to
	      };
	    });
	  });
	
	  return ret;
	}
	/**
	 *
	 * @param {string} key - 170020170202DMEAER1027S7GH = 17:00 2017-02-02 DME_AER 10:27 S7 GH
	 * @param {string} dateString - 2017-02-02T17:00:00
	 *
	 * @return {{year: number, month: number, day: number, hours: number, minutes: number, seconds: number}}
	 */
	var parseDateString = function () {
	  // const regExp = /([\d]{2})([\d]{2})([\d]{4})([\d]{2})([\d]{2})[^\d]+([\d]{2})([\d]{2})/g;
	  var regDateString = /([\d]{4})-([\d]{2})-([\d]{2})T([\d]{2}):([\d]{2}):([\d]{2})/;
	  return function (dateString) {
	    var match = regDateString.exec(dateString);
	
	    return {
	      year: match[1],
	      month: match[2],
	      day: match[3],
	
	      hours: match[4],
	      minutes: match[5],
	      seconds: match[6]
	    };
	  };
	}();
	
	exports["default"] = {
	  findPlaceAsync: findPlaceAsync,
	  bestDealsAsync: bestDealsAsync
	};

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.jsonp = jsonp;
	exports.ajax = ajax;
	
	var _JSONP = __webpack_require__(25);
	
	var _JSONP2 = _interopRequireDefault(_JSONP);
	
	var _Ajax = __webpack_require__(27);
	
	var _Ajax2 = _interopRequireDefault(_Ajax);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	/**
	 *
	 * @param   {string}          url
	 * @param   {RequestOptions}  options
	 * @return  {number|string}
	 */
	function jsonp(url, options) {
	  var request = new _JSONP2["default"](url, options);
	  request.send();
	  return request.$id;
	}
	
	/**
	 *
	 * @param   {string}          url
	 * @param   {RequestOptions}  options
	 * @return  {number|string}
	 */
	function ajax(url, options) {
	  var request = new _Ajax2["default"](url, options);
	  request.send();
	  return request.$id;
	}

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Request2 = __webpack_require__(26);
	
	var _Request3 = _interopRequireDefault(_Request2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 *
	 * @type {number}
	 */
	
	var head = null;
	
	/**
	 * @class JSONP
	 * @extends Request
	 */
	
	var JSONP = function (_Request) {
	  _inherits(JSONP, _Request);
	
	  /**
	   *
	   * @param {string} url
	   * @param {RequestOptions} {options={}}
	   */
	  function JSONP(url) {
	    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	    _classCallCheck(this, JSONP);
	
	    return _possibleConstructorReturn(this, (JSONP.__proto__ || Object.getPrototypeOf(JSONP)).call(this, url, options));
	  }
	
	  /**
	   *
	   * @return {JSONP}
	   * @private
	   */
	
	
	  _createClass(JSONP, [{
	    key: '__initAdds',
	    value: function __initAdds() {
	      this._adds = {};
	
	      this._generatedFunction = 'jsonp_' + this.$id;
	      this._adds[this.options.method] = this._generatedFunction;
	
	      return this;
	    }
	
	    // __onResponse(response) {
	    //   if (this._done)
	    //     return;
	    //   this._done = true;
	    //
	    //   countRequest--;
	    //   this._options.success(response, this.id);
	    //   this.__deleteFunction().__deleteScript();
	    //   return this.__onAlways();
	    // }
	
	  }, {
	    key: '__beforeNewTry',
	    value: function __beforeNewTry() {
	      // this.__deleteFunction().__deleteScript();
	      this._generatedFunction = 'jsonp_' + this.$id + '_try' + this._tryes;
	    }
	  }, {
	    key: '__onSuccess',
	    value: function __onSuccess() {
	      // this.__deleteFunction().__deleteScript();
	    }
	  }, {
	    key: '__onAlways',
	    value: function __onAlways() {
	      // if (this._options.always)
	      //   this._options.always();
	      // playRequest();
	      // return this;
	      this.__deleteScript();
	    }
	
	    /**
	     *
	     * @return {JSONP}
	     * @private
	     */
	
	  }, {
	    key: '__initFunction',
	    value: function __initFunction() {
	      window[this._generatedFunction] = this.__success.bind(this);
	      return this;
	    }
	
	    /**
	     *
	     * @return {JSONP}
	     * @private
	     */
	
	  }, {
	    key: '__deleteFunction',
	    value: function __deleteFunction() {
	      // try {
	      //   delete window[this._generatedFunction];
	      // } catch (e) {
	      //   window[this._generatedFunction] = undefined;
	      // }
	      return this;
	    }
	
	    /**
	     *
	     * @return {JSONP}
	     * @private
	     */
	
	  }, {
	    key: '__initScript',
	    value: function __initScript() {
	      //noinspection JSValidateTypes
	      /**
	       *
	       * @type {HTMLScriptElement}
	       */
	      var script = document.createElement('script');
	
	      script.async = true;
	      script.setAttribute('data-id', this._generatedFunction);
	
	      script.src = this._src;
	
	      if (!head) head = document.getElementsByTagName('head')[0];
	
	      head.appendChild(script);
	
	      if (!this._scripts)
	        /**
	         *
	         * @type {Array<HTMLScriptElement>}
	         * @private
	         */
	        this._scripts = [];
	      this._scripts.push(script);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {JSONP}
	     * @private
	     */
	
	  }, {
	    key: '__deleteScript',
	    value: function __deleteScript() {
	      // console.log("__deleteScript", this.id, this._generatedFunction);
	      if (this._scripts) this._scripts.forEach(function ( /* HTMLScriptElement */script) {
	        if (script && script.parentNode) script.parentNode.removeChild(script);
	      });
	      return this;
	    }
	
	    // send() {
	    // if (countRequest >= MAX_REQUESTS) {
	    //   pausedRequest.push(this);
	    // } else {
	    //   countRequest++;
	    //   this._tryes++;
	    //   this.__initFunction().__initSrc().__initTimeout().__append();
	    // }
	    // }
	
	    /**
	     *
	     * @return {JSONP}
	     * @private
	     */
	
	  }, {
	    key: '__initSend',
	    value: function __initSend() {
	      this.__initFunction().__initScript();
	      return this;
	    }
	  }]);
	
	  return JSONP;
	}(_Request3["default"]);
	
	exports["default"] = JSONP;

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	/**
	 *
	 * @typedef {Object}    RequestOptions
	 *
	 * @property {function}   [success=function(response:any, id:string){}] - выполняется при ответе сервера
	 * @property {function}   [fail=function(id:string){}] - выполняется при ошибке и timeout
	 * @property {function}   [always=function(id:string){}] - выполняется всегда по завершению запроса
	 *
	 * @property {number}     [timeout=20] - время ожидания ответа (сек)
	 * @property {number}     [repeats=5] - количество повторов при timeout
	 * @property {string}     [responseType=json] - тип ответа
	 *
	 * @property {Object}     [data={}] - данные
	 *
	 * @property {string}     [method=callback] - название параметра функции jsonp
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Deep = __webpack_require__(9);
	
	var _Format = __webpack_require__(17);
	
	var _Format2 = _interopRequireDefault(_Format);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 *
	 * @type {Array<Request>}
	 */
	var pausedRequest = [];
	
	/**
	 *
	 * @type {number}
	 */
	var countRequest = 0;
	var MAX_REQUESTS = 12;
	
	function startPausedRequest() {
	  setTimeout(function () {
	    if (countRequest < MAX_REQUESTS && pausedRequest.length > 0) {
	      pausedRequest.pop().send();
	    }
	  }, 237);
	}
	
	/**
	 *
	 * @type {RequestOptions}
	 */
	var defaultRequestOptions = {
	  success: function success() {},
	  fail: function fail() {},
	  always: function always() {},
	
	  timeout: 20,
	  repeats: 3,
	
	  responseType: 'json',
	
	  data: {},
	
	  method: 'callback'
	};
	
	var Request = function () {
	  /**
	   *
	   * @param {string} url
	   * @param {RequestOptions} options
	   */
	  function Request(url, options) {
	    _classCallCheck(this, Request);
	
	    /**
	     *
	     * @type {string}
	     * @private
	     */
	    this._url = url;
	    /**
	     * @type {RequestOptions}
	     */
	    this.options = (0, _Deep.deepExtend)({}, defaultRequestOptions, options);
	    /**
	     *
	     * @type {Object}
	     * @private
	     */
	    this._data = this.options.data;
	    /**
	     *
	     * @type {number}
	     * @private
	     */
	    this._timestamp = Date.now();
	    /**
	     *
	     * @type {string}
	     * @private
	     */
	    this._id = this._timestamp + '_' + Math.round(Math.random() * 1000001);
	    /**
	     *
	     * @type {boolean}
	     * @private
	     */
	    this._done = false;
	    /**
	     *
	     * @type {Object}
	     * @private
	     */
	    this._adds = {};
	    /**
	     *
	     * @type {number}
	     * @private
	     */
	    this._tryes = 0;
	
	    this.__initAdds();
	  }
	
	  /**
	   *
	   * @return {Request}
	   * @private
	   */
	
	
	  _createClass(Request, [{
	    key: '__initAdds',
	    value: function __initAdds() {
	      /* Дополнительные данные {this._adds} для потомков */
	      return this;
	    }
	
	    // Events
	
	  }, {
	    key: 'success',
	    value: function success(callback) {
	      this.options.success = callback;
	      return this;
	    }
	  }, {
	    key: 'fail',
	    value: function fail(callback) {
	      this.options.fail = callback;
	      return this;
	    }
	  }, {
	    key: 'always',
	    value: function always(callback) {
	      this.options.always = callback;
	      return this;
	    }
	
	    // Properties
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: '__initSrc',
	
	
	    /**
	     *
	     * @return {Request}
	     * @private
	     */
	    value: function __initSrc() {
	      this._src = _Format2["default"].request.urlString(this._url, this._data, this._adds);
	      return this;
	    }
	
	    // Методы
	
	  }, {
	    key: 'send',
	    value: function send() {
	
	      if (countRequest >= MAX_REQUESTS) {
	        pausedRequest.push(this);
	      } else {
	        countRequest++;
	
	        this._tryes++;
	        this.__initSrc();
	        this.__initSend();
	        this.__initTimeout();
	        return this;
	      }
	    }
	
	    /**
	     *
	     * @return {Request}
	     * @private
	     */
	
	  }, {
	    key: '__initSend',
	    value: function __initSend() {
	      /* Отпарвляем запрос */
	      return this;
	    }
	
	    /**
	     *
	     * @return {Request}
	     * @private
	     */
	
	  }, {
	    key: '__initTimeout',
	    value: function __initTimeout() {
	      /* Инициализируем таймаут */
	      setTimeout(this.__timeout.bind(this), 1000 * this.options.timeout);
	      return this;
	    }
	
	    // Ответы
	
	  }, {
	    key: '__success',
	    value: function __success(response) {
	      if (this._done) return;
	      this._done = true;
	
	      var data = response;
	
	      if (this.options.responseType === 'json') {
	        if (response) {
	          if (typeof response === 'string') {
	            try {
	              data = JSON.parse(response);
	            } catch (e) {
	              // console.warn('Error parse json');
	            }
	          }
	        } else {
	          data = {};
	        }
	      }
	
	      if (this.options.success) this.options.success(data, this.$id);
	
	      this.__onSuccess(data);
	      this.__always();
	    }
	    /*eslint no-unused-vars: ["error", {"args": "none"}]*/
	
	  }, {
	    key: '__onSuccess',
	    value: function __onSuccess(response) {
	      /* Делает после удачного ответа */
	    }
	  }, {
	    key: '__timeout',
	    value: function __timeout() {
	      if (this._done) return;
	
	      this.__clean();
	
	      if (this._tryes < this.options.repeats) {
	        this.__beforeNewTry();
	        countRequest--;
	        setTimeout(this.send.bind(this), 300);
	      } else {
	        if (this.options.fail) this.options.fail();
	
	        this.__onTimeout();
	        this.__always();
	      }
	    }
	  }, {
	    key: '__onTimeout',
	    value: function __onTimeout() {
	      /* После всех попыток запроса */
	    }
	  }, {
	    key: '__fail',
	    value: function __fail() {
	      if (this.options.fail) this.options.fail();
	      this.__always();
	    }
	  }, {
	    key: '__always',
	    value: function __always() {
	      if (this.options.always) this.options.always();
	      this.__onAlways();
	
	      countRequest--;
	      startPausedRequest();
	    }
	  }, {
	    key: '__onAlways',
	    value: function __onAlways() {
	      /* После удачого или неудачного запроса */
	    }
	  }, {
	    key: '__beforeNewTry',
	    value: function __beforeNewTry() {
	      /*Запускается перед инициализацией новой попытки*/
	      return this;
	    }
	  }, {
	    key: 'abort',
	    value: function abort() {}
	  }, {
	    key: '__clean',
	    value: function __clean() {}
	  }, {
	    key: '$id',
	    get: function get() {
	      return this._id;
	    }
	  }]);
	
	  return Request;
	}();
	
	exports["default"] = Request;

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Request2 = __webpack_require__(26);
	
	var _Request3 = _interopRequireDefault(_Request2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Ajax = function (_Request) {
	  _inherits(Ajax, _Request);
	
	  function Ajax(url, options) {
	    _classCallCheck(this, Ajax);
	
	    var _this = _possibleConstructorReturn(this, (Ajax.__proto__ || Object.getPrototypeOf(Ajax)).call(this, url, options));
	
	    _this.options.method = options.method && options.method.toLowerCase() == 'post' ? 'POST' : 'GET';
	    _this.options.timeout = options.timeout || 60;
	    return _this;
	  }
	
	  _createClass(Ajax, [{
	    key: '__initAdds',
	    value: function __initAdds() {
	      this._adds = {};
	      this._adds['_'] = this.$id;
	    }
	  }, {
	    key: '__initSend',
	    value: function __initSend() {
	
	      var xmlhttp = new XMLHttpRequest();
	
	      var self = this;
	
	      xmlhttp.onreadystatechange = function () {
	        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
	          if (xmlhttp.status == 200) {
	            self.__success(xmlhttp.responseText);
	          } else if (xmlhttp.status == 400) {
	            self.__fail();
	          } else {
	            self.__fail();
	          }
	        }
	      };
	
	      xmlhttp.open(this.options.method, this._src, true);
	      xmlhttp.send();
	    }
	  }, {
	    key: '__initTimeout',
	    value: function __initTimeout() {
	      return this;
	    }
	  }, {
	    key: '__fail',
	    value: function __fail() {}
	  }]);
	
	  return Ajax;
	}(_Request3["default"]);
	
	exports["default"] = Ajax;

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.getAllByPart = getAllByPart;
	exports.byIATA = byIATA;
	
	var _city = __webpack_require__(29);
	
	var _country = __webpack_require__(34);
	
	var _airline = __webpack_require__(37);
	
	var _sortPlaceData = __webpack_require__(33);
	
	var _sortPlaceData2 = _interopRequireDefault(_sortPlaceData);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	// /**
	//  *
	//  * @type {cityByIATA}
	//  */
	// export const getCity = cityByIATA;
	
	// /**
	//  *
	//  * @type {cityByPart}
	//  */
	// export const getCityPart = cityByPart;
	
	// /**
	//  *
	//  * @type {countryByIATA}
	//  */
	// export const getCountry = countryByIATA;
	
	// /**
	//  *
	//  * @type {countryByPart}
	//  */
	// export const getCountryPart = countryByPart;
	
	/**
	 *
	 * @param {string} part
	 * @return {Array.<PlaceData>}
	 */
	function getAllByPart(part) {
	  var cities = (0, _city.byPart)(part),
	      countries = (0, _country.byPart)(part);
	
	  return [].concat(cities, countries).sort(function (placeDataA, placeDataB) {
	    return (0, _sortPlaceData2["default"])(placeDataA, placeDataB, part);
	  });
	}
	/**
	 *
	 * @param {sring} iata
	 * @return {PlaceData|null}
	 */
	function byIATA(iata) {
	  return (0, _city.byIATA)(iata) || (0, _airline.byIATA)(iata) || (0, _country.byIATA)(iata) || null;
	}

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.byIATA = byIATA;
	exports.byPart = byPart;
	
	var _cities = __webpack_require__(30);
	
	var _cities2 = _interopRequireDefault(_cities);
	
	var _cities3 = __webpack_require__(31);
	
	var _cities4 = _interopRequireDefault(_cities3);
	
	var _findKeys = __webpack_require__(32);
	
	var _findKeys2 = _interopRequireDefault(_findKeys);
	
	var _sortPlaceData = __webpack_require__(33);
	
	var _sortPlaceData2 = _interopRequireDefault(_sortPlaceData);
	
	var _country = __webpack_require__(34);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	/**
	 *
	 * @param {string} iata
	 * @return {PlaceData|null}
	 */
	function byIATA(iata) {
	  return __getCity(iata);
	}
	
	/**
	 *
	 * @param {string} iata
	 * @return {PlaceData|null}
	 * @private
	 */
	function __getCity(iata) {
	  if (_cities2["default"].hasOwnProperty(iata)) {
	
	    var cityData = _cities2["default"][iata].split('|');
	    var cityRuName = __getCityRU(iata);
	
	    if (!cityRuName) return null;
	
	    var country = (0, _country.byIATA)(cityData[1]);
	
	    if (!country) return null;
	
	    return {
	      iata: iata,
	      type: 'city',
	      city: {
	        iata: iata,
	        en: cityData[0],
	        ru: cityRuName
	      },
	      country: {
	        iata: country.iata,
	        ru: country.country.ru,
	        en: country.country.en
	      }
	    };
	  }
	  return null;
	}
	/**
	 *
	 * @param {string} iata
	 * @return {null|string}
	 * @private
	 */
	function __getCityRU(iata) {
	  if (_cities4["default"].hasOwnProperty(iata)) {
	    return _cities4["default"][iata];
	  }
	  return null;
	}
	/**
	 *
	 * @param {string} part
	 * @return {Array.<PlaceData>}
	 */
	function byPart(part) {
	  return (0, _findKeys2["default"])(part, _cities4["default"]).map(function ( /*string*/iata) {
	    return byIATA(iata);
	  }).filter(function ( /*(PlaceData|null)*/cityData) {
	    return !!cityData;
	  }).sort(function ( /*PlaceData*/cityDataA, /*PlaceData*/cityDataB) {
	    return (0, _sortPlaceData2["default"])(cityDataA, cityDataB, part);
	  });
	}

/***/ },
/* 30 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports["default"] = {
	  LOP: 'Praya|ID|-8.75|116.27|128',
	  GBB: 'Gabala|AZ|40.98|47.85|127',
	  MOW: 'Moscow|RU|55.75|37.62|126',
	  GZP: 'Alanya|TR|36.30|32.30|125',
	  LON: 'London|GB|51.51|-0.13|124',
	  PAR: 'Paris|FR|48.85|2.35|123',
	  MAD: 'Madrid|ES|40.42|-3.70|122',
	  IEV: 'Kiev|UA|50.43|30.52|121',
	  NYC: 'New York|US|40.71|-74.01|120',
	  AMS: 'Amsterdam|NL|52.37|4.90|119',
	  LED: 'St Petersburg|RU|59.89|30.26|118',
	  NCE: 'Nice|FR|43.70|7.25|117',
	  GVA: 'Geneva|CH|46.20|6.15|116',
	  PRG: 'Prague|CZ|50.09|14.42|115',
	  ACE: 'Lanzarote|ES|29.00|-13.67|114',
	  MUC: 'Munich|DE|48.35|11.78|113',
	  BER: 'Berlin|DE|52.52|13.40|112',
	  IST: 'Istanbul|TR|41.01|28.95|111',
	  ROV: 'Rostov-on-Don|RU|47.23|39.72|110',
	  BCN: 'Barcelona|ES|41.39|2.16|109',
	  PKC: 'Petropavlovsk Kamchatskiy|RU|53.02|158.65|108',
	  CLJ: 'Cluj|RO|46.77|23.60|107',
	  TLV: 'Tel Aviv|IL|32.07|34.76|106',
	  KIV: 'Chisinau|MD|47.01|28.86|105',
	  VIE: 'Vienna|AT|48.11|16.56|104',
	  BEG: 'Belgrade|RS|44.82|20.47|103',
	  ZRH: 'Zurich|CH|47.37|8.55|102',
	  LIS: 'Lisbon|PT|38.72|-9.13|101',
	  FRA: 'Frankfurt|DE|50.12|8.68|100',
	  HEL: 'Helsinki|FI|60.18|24.93|99',
	  DUS: 'Dusseldorf|DE|51.22|6.78|98',
	  OSL: 'Oslo|NO|59.91|10.74|97',
	  LCA: 'Larnaca|CY|34.92|33.63|96',
	  PMI: 'Palma Mallorca|ES|39.55|2.73|96',
	  BRU: 'Brussels|BE|50.85|4.35|95',
	  LAX: 'Los Angeles|US|34.05|-118.24|95',
	  BUD: 'Budapest|HU|47.50|19.08|94',
	  RIX: 'Riga|LV|56.95|24.10|93',
	  ZAG: 'Zagreb|HR|45.80|16.00|92',
	  WAW: 'Warsaw|PL|52.25|21.00|91',
	  ODS: 'Odessa|UA|46.48|30.73|90',
	  NUE: 'Nuremberg|DE|49.45|11.07|89',
	  LJU: 'Ljubljana|SI|46.06|14.51|88',
	  CPH: 'Copenhagen|DK|55.68|12.57|88',
	  BKK: 'Bangkok|TH|13.75|100.52|87',
	  HAV: 'Havana|CU|23.13|-82.36|86',
	  LUX: 'Luxembourg|LU|49.61|6.13|85',
	  SFO: 'San Francisco|US|37.77|-122.42|84',
	  DUB: 'Dublin|IE|53.33|-6.25|84',
	  AER: 'Sochi|RU|43.60|39.73|83',
	  LDY: 'Londonderry|GB|55.04|-7.15|82',
	  ROM: 'Rome|IT|41.90|12.48|81',
	  BUH: 'Bucharest|RO|44.43|26.10|80',
	  CBR: 'Canberra|AU|-35.28|149.13|79',
	  CAI: 'Cairo|EG|30.05|31.25|78',
	  MAN: 'Manchester|GB|53.48|-2.24|77',
	  KUF: 'Samara|RU|53.20|50.15|76',
	  AYT: 'Antalya|TR|36.91|30.69|75',
	  ATH: 'Athens|GR|37.98|23.73|74',
	  FAO: 'Faro|PT|37.02|-7.93|73',
	  SIP: 'Simferopol|AK|44.95|34.10|72',
	  SVX: 'Ekaterinburg|RU|56.86|60.61|71',
	  SCL: 'Santiago|CL|-33.45|-70.67|70',
	  STO: 'Stockholm|SE|59.33|18.06|70',
	  PIZ: 'Point Lay|US|69.76|-163.05|70',
	  SYD: 'Sydney|AU|-33.87|151.21|69',
	  HOU: 'Houston|US|29.76|-95.36|68',
	  DEL: 'Delhi|IN|28.67|77.22|67',
	  DXB: 'Dubai|AE|25.25|55.28|66',
	  DPS: 'Denpasar Bali|ID|-8.75|115.17|66',
	  ALG: 'Algiers|DZ|36.76|3.05|66',
	  BPN: 'Balikpapan|ID|-1.27|116.83|65',
	  BOM: 'Mumbai|IN|19.01|72.85|64',
	  UTP: 'Pattaya|TH|12.68|101.02|63',
	  HKG: 'Hong Kong|HK|22.32|113.92|62',
	  ALA: 'Almaty|KZ|43.25|76.95|61',
	  MRS: 'Marseille|FR|43.30|5.40|61',
	  KGD: 'Kaliningrad|RU|54.71|20.50|60',
	  VCE: 'Venice|IT|45.44|12.33|60',
	  VLC: 'Valencia|ES|39.47|-0.38|60',
	  SSH: 'Sharm El Sheik|EG|27.86|34.29|59',
	  OPO: 'Porto|PT|41.15|-8.62|58',
	  ORN: 'Oran|DZ|35.69|-0.64|58',
	  TIP: 'Tripoli|LY|32.89|13.18|57',
	  OVB: 'Novosibirsk|RU|55.04|82.93|56',
	  CHI: 'Chicago|US|41.85|-87.65|55',
	  GOI: 'Goa|IN|15.38|73.83|54',
	  MJV: 'Murcia|ES|37.98|-1.12|54',
	  TIV: 'Tivat|ME|42.44|18.70|53',
	  OMS: 'Omsk|RU|55.00|73.40|53',
	  SIN: 'Singapore|SG|1.29|103.86|53',
	  VVO: 'Vladivostok|RU|43.11|131.87|52',
	  TGD: 'Podgorica|ME|42.44|19.26|52',
	  CUN: 'Cancun|MX|21.17|-86.85|52',
	  VNO: 'Vilnius|LT|54.68|25.32|52',
	  MIA: 'Miami|US|25.77|-80.19|52',
	  EDI: 'Edinburgh|GB|55.95|-3.20|51',
	  TSE: 'Astana|KZ|51.18|71.43|50',
	  MLA: 'Malta|MT|35.90|14.52|49',
	  TYO: 'Tokyo|JP|35.69|139.69|48',
	  DOK: 'Donetsk|UA|48.00|37.80|47',
	  MIL: 'Milan|IT|45.46|9.19|46',
	  MEX: 'Mexico City|MX|19.43|-99.14|45',
	  HAM: 'Hamburg|DE|53.55|10.00|45',
	  KLV: 'Karlovy Vary|CZ|50.23|12.87|45',
	  CEK: 'Chelyabinsk|RU|55.15|61.43|44',
	  KRR: 'Krasnodar|RU|45.03|38.98|44',
	  HRG: 'Hurghada|EG|27.26|33.81|44',
	  LIM: 'Lima|PE|-12.05|-77.05|43',
	  UFA: 'Ufa|RU|54.73|55.93|43',
	  BOS: 'Boston|US|42.36|-71.06|42',
	  UIO: 'Quito|EC|-0.22|-78.50|42',
	  SKG: 'Thessaloniki|GR|40.64|22.94|42',
	  MSQ: 'Minsk|BY|53.90|27.57|41',
	  JKT: 'Jakarta|ID|-6.17|106.83|40',
	  ATL: 'Atlanta|US|33.75|-84.39|39',
	  KZN: 'Kazan|RU|55.75|49.13|39',
	  CAS: 'Casablanca|MA|33.59|-7.61|39',
	  GLA: 'Glasgow|GB|55.83|-4.25|39',
	  LAS: 'Las Vegas|US|35.97|-115.12|39',
	  CGN: 'Cologne|DE|50.93|6.95|38',
	  PEE: 'Perm|RU|58.00|56.25|38',
	  HNL: 'Honolulu|US|21.31|-157.86|38',
	  GOJ: 'Nizhniy Novgorod|RU|56.33|44.00|38',
	  PHL: 'Philadelphia|US|39.95|-75.16|37',
	  BEY: 'Beirut|LB|33.87|35.51|37',
	  BLQ: 'Bologna|IT|44.49|11.34|37',
	  VOG: 'Volgograd|RU|48.80|44.59|36',
	  LYS: 'Lyon|FR|45.75|4.85|36',
	  ETH: 'Elat|IL|29.56|34.95|35',
	  NAS: 'Nassau|BS|25.08|-77.35|34',
	  BJS: 'Beijing|CN|39.91|116.40|33',
	  MLE: 'Male|MV|4.18|73.51|33',
	  MNL: 'Manila|PH|14.60|120.98|33',
	  CMB: 'Colombo|LK|6.93|79.85|33',
	  KUL: 'Kuala Lumpur|MY|3.17|101.70|33',
	  YTO: 'Toronto|CA|43.70|-79.42|32',
	  STL: 'St Louis|US|38.63|-90.20|31',
	  ORK: 'Cork|IE|51.90|-8.50|31',
	  CVG: 'Cincinnati|US|39.16|-84.46|31',
	  BFS: 'Belfast|GB|54.58|-5.93|31',
	  MKE: 'Milwaukee|US|43.04|-87.91|31',
	  BWI: 'Baltimore|US|39.29|-76.61|31',
	  MBJ: 'Montego Bay|JM|18.47|-77.92|31',
	  MEM: 'Memphis|US|35.15|-90.05|31',
	  CLT: 'Charlotte|US|35.23|-80.84|31',
	  RDU: 'Raleigh|US|35.77|-78.64|31',
	  ABZ: 'Aberdeen|GB|57.13|-2.10|31',
	  CLE: 'Cleveland|US|41.50|-81.70|31',
	  WAS: 'Washington|US|38.89|-77.01|30',
	  NCL: 'Newcastle|GB|54.97|-1.61|29',
	  PFO: 'Paphos|CY|34.77|32.42|29',
	  TAS: 'Tashkent|UZ|41.32|69.25|29',
	  REK: 'Reykjavik|IS|64.15|-21.95|28',
	  ARH: 'Arkhangelsk|RU|64.54|40.54|27',
	  LWO: 'Lvov|UA|49.84|24.02|26',
	  ADL: 'Adelaide|AU|-34.93|138.60|25',
	  MSY: 'New Orleans|US|29.95|-90.08|25',
	  AUH: 'Abu Dhabi|AE|24.47|54.37|25',
	  SOF: 'Sofia|BG|42.70|23.32|24',
	  BTS: 'Bratislava|SK|48.15|17.12|24',
	  VRN: 'Verona|IT|45.43|11.00|24',
	  HAN: 'Hanoi|VN|21.03|105.85|24',
	  CAG: 'Cagliari|IT|39.21|9.13|23',
	  TJM: 'Tyumen|RU|57.15|65.53|23',
	  FRU: 'Bishkek|KG|42.87|74.60|23',
	  MEL: 'Melbourne|AU|-37.81|144.96|23',
	  PWQ: 'Pavlodar|KZ|52.30|76.95|22',
	  WLG: 'Wellington|NZ|-41.28|174.78|21',
	  RIO: 'Rio De Janeiro|BR|-22.90|-43.21|21',
	  ANK: 'Ankara|TR|39.93|32.86|21',
	  IKT: 'Irkutsk|RU|52.30|104.30|21',
	  HKT: 'Phuket|TH|7.88|98.40|21',
	  KEJ: 'Kemerovo|RU|55.33|86.08|21',
	  MTY: 'Monterrey|MX|25.67|-100.32|21',
	  KIN: 'Kingston|JM|18.00|-76.79|21',
	  ALC: 'Alicante|ES|38.35|-0.48|20',
	  SEZ: 'Mahe Island|SC|-4.67|55.52|20',
	  DTT: 'Detroit|US|42.33|-83.05|20',
	  ANC: 'Anchorage|US|61.22|-149.90|20',
	  CYS: 'Cheyenne|US|41.14|-104.82|20',
	  SDQ: 'Santo Domingo|DO|18.47|-69.90|20',
	  YMQ: 'Montreal|CA|45.52|-73.65|20',
	  TRN: 'Turin|IT|45.08|7.68|20',
	  BRI: 'Bari|IT|41.12|16.85|20',
	  MRV: 'Mineralnye Vody|RU|44.21|43.14|20',
	  MRU: 'Mauritius|MU|-20.30|57.58|20',
	  IBZ: 'Ibiza|ES|38.91|1.43|20',
	  SEL: 'Seoul|KR|37.57|127.00|19',
	  ULN: 'Ulaanbaatar|MN|47.92|106.92|18',
	  RTW: 'Saratov|RU|51.57|46.03|18',
	  MJF: 'Mosjoen|NO|65.83|13.20|18',
	  TBS: 'Tbilisi|GE|41.73|44.79|18',
	  NBO: 'Nairobi|KE|-1.28|36.82|18',
	  HER: 'Heraklion|GR|35.33|25.13|17',
	  OMO: 'Mostar|BA|43.34|17.81|17',
	  BHX: 'Birmingham|GB|52.47|-1.92|18',
	  DEN: 'Denver|US|39.74|-104.98|18',
	  SKP: 'Skopje|MK|42.00|21.43|18',
	  RMF: 'Marsa Alam|EG|25.63|34.57|18',
	  YVR: 'Vancouver|CA|49.25|-123.12|16',
	  IPC: 'Easter Island|CL|-27.12|-109.37|16',
	  PSG: 'Petersburg|US|56.80|-132.94|16',
	  HFA: 'Haifa|IL|32.82|34.99|15',
	  TLN: 'Toulon|FR|43.12|5.93|16',
	  KTT: 'Kittila|FI|67.67|24.90|16',
	  TCI: 'Tenerife|ES|28.47|-16.25|16',
	  SCU: 'Santiago|CU|19.97|-75.84|16',
	  BNE: 'Brisbane|AU|-27.47|153.03|16',
	  OLB: 'Olbia|IT|40.92|9.49|16',
	  IXL: 'Leh IN|IN|34.17|77.58|16',
	  FNC: 'Funchal|PT|32.63|-16.90|16',
	  SHA: 'Shanghai|CN|31.22|121.46|13',
	  ASF: 'Astrakhan|RU|46.34|48.04|13',
	  KXK: 'Komsomolsk Na Amure|RU|50.55|137.01|13',
	  TOF: 'Tomsk|RU|56.50|84.97|13',
	  RTM: 'Rotterdam|NL|51.92|4.48|13',
	  BGI: 'Bridgetown|BB|13.10|-59.62|13',
	  'VAR': 'Varna|BG|43.22|27.92|13',
	  CCS: 'Caracas|VE|10.50|-66.92|13',
	  HRK: 'Kharkov|UA|50.00|36.25|13',
	  PES: 'Petrozavodsk|RU|61.78|34.35|13',
	  BOD: 'Bordeaux|FR|44.83|-0.57|13',
	  TLL: 'Tallinn|EE|59.43|24.73|14',
	  MIR: 'Monastir|TN|35.78|10.83|13',
	  KJA: 'Krasnoyarsk|RU|56.01|92.79|13',
	  ADK: 'Adak Island|US|51.78|-176.64|13',
	  VRA: 'Varadero|CU|23.15|-81.25|13',
	  DLM: 'Dalaman|TR|36.77|28.80|13',
	  CIX: 'Chiclayo|PE|-6.77|-79.84|13',
	  INN: 'Innsbruck|AT|47.27|11.40|14',
	  DFW: 'Dallas|US|32.78|-97.30|13',
	  YXU: 'London|CA|42.98|-81.23|13',
	  LXA: 'Lhasa|CN|29.65|91.10|12',
	  AAR: 'Aarhus|DK|56.16|10.21|12',
	  USM: 'Koh Samui|TH|9.50|100.00|12',
	  SVQ: 'Sevilla|ES|37.38|-5.99|12',
	  TIA: 'Tirana|AL|41.33|19.82|12',
	  HAJ: 'Hanover|DE|52.37|9.73|12',
	  LTT: 'St Tropez|FR|43.28|6.63|11',
	  BXN: 'Bodrum|TR|37.04|27.43|12',
	  NJC: 'Nizhnevartovsk|RU|60.93|76.57|12',
	  SZG: 'Salzburg|AT|47.80|13.03|12',
	  BTK: 'Bratsk|RU|56.13|101.61|12',
	  NTE: 'Nantes|FR|47.22|-1.55|12',
	  KGF: 'Karaganda|KZ|49.80|73.10|12',
	  RNS: 'Rennes|FR|48.08|-1.68|12',
	  SEA: 'Seattle|US|47.61|-122.33|12',
	  PRN: 'Pristina|RS|42.67|21.17|12',
	  SPU: 'Split|HR|43.51|16.44|12',
	  MCM: 'Monte Carlo|MC|43.73|7.42|11',
	  BOJ: 'Bourgas|BG|42.50|27.47|12',
	  SID: 'Sal Island|CV|16.75|-22.95|12',
	  VSG: 'Lugansk|UA|48.57|39.33|12',
	  AKL: 'Auckland|NZ|-36.87|174.77|12',
	  EBB: 'Entebbe|UG|0.06|32.45|12',
	  KTM: 'Kathmandu|NP|27.72|85.32|12',
	  PUJ: 'Punta Cana|DO|18.80|-71.57|12',
	  DBV: 'Dubrovnik|HR|42.65|18.09|12',
	  UUD: 'Ulan Ude|RU|51.83|107.62|12',
	  BRN: 'Berne|CH|46.95|7.45|10',
	  ASU: 'Asuncion|PY|-25.27|-57.67|10',
	  CPO: 'Copiapo|CL|-27.37|-70.33|10',
	  MSN: 'Madison|US|43.08|-89.37|10',
	  ABJ: 'Abidjan|CI|5.34|-4.03|10',
	  PUY: 'Pula|HR|44.87|13.85|10',
	  EVN: 'Yerevan|AM|40.18|44.51|10',
	  DAC: 'Dhaka|BD|23.72|90.41|10',
	  HMO: 'Hermosillo|MX|29.07|-110.97|10',
	  KHV: 'Khabarovsk|RU|48.48|135.09|10',
	  ASE: 'Aspen|US|39.19|-106.82|10',
	  HTA: 'Chita|RU|52.03|113.55|10',
	  STR: 'Stuttgart|DE|48.77|9.18|10',
	  MSP: 'Minneapolis|US|44.98|-93.26|10',
	  OHD: 'Ohrid|MK|41.12|20.80|10',
	  MKC: 'Kansas City|US|39.29|-94.73|10',
	  RAK: 'Marrakech|MA|31.63|-8.00|10',
	  RMI: 'Rimini|IT|44.06|12.58|10',
	  JKH: 'Chios|GR|38.41|26.02|10',
	  ASP: 'Alice Springs|AU|-23.70|133.88|10',
	  DOM: 'Dominica|DM|15.54|-61.31|9',
	  MBA: 'Mombasa|KE|-4.05|39.67|7',
	  DNK: 'Dnepropetrovsk|UA|48.45|34.98|7',
	  LXR: 'Luxor|EG|25.70|32.64|7',
	  LHE: 'Lahore|PK|31.55|74.34|7',
	  TOB: 'Tobruk|LY|32.08|23.98|7',
	  LEI: 'Almeria|ES|36.83|-2.45|7',
	  PTG: 'Pietersburg|ZA|-23.90|29.45|7',
	  ASB: 'Ashgabat|TM|37.95|58.38|7',
	  BAH: 'Muharraq|BH|26.27|50.64|7',
	  AMM: 'Amman|JO|31.95|35.93|7',
	  KWI: 'Kuwait|KW|29.37|47.98|7',
	  MXS: 'Maota|WS|-13.72|-172.23|7',
	  LUG: 'Lugano|CH|46.01|8.96|7',
	  IZM: 'Izmir|TR|38.41|27.15|7',
	  BIO: 'Bilbao|ES|43.26|-2.93|7',
	  BOG: 'Bogota|CO|4.60|-74.08|7',
	  HOG: 'Holguin|CU|20.89|-76.26|8',
	  APW: 'Apia|WS|-13.83|-171.73|7',
	  IPH: 'Ipoh|MY|4.58|101.08|7',
	  SAO: 'Sao Paulo|BR|-23.55|-46.64|7',
	  TRV: 'Thiruvananthapuram|IN|8.48|76.92|7',
	  JED: 'Jeddah|SA|21.52|39.22|7',
	  ZTH: 'Zakinthos|GR|37.79|20.90|7',
	  MCT: 'Muscat|OM|23.61|58.59|7',
	  JMK: 'Mykonos|GR|37.45|25.38|7',
	  PSA: 'Pisa|IT|43.72|10.40|7',
	  ORL: 'Orlando|US|28.54|-81.38|7',
	  SBH: 'St Barthelemy|BL|17.90|-62.85|7',
	  ISB: 'Islamabad|PK|33.61|73.10|7',
	  CHQ: 'Chania|GR|35.51|24.02|7',
	  MGA: 'Managua|NI|12.15|-86.27|7',
	  RUH: 'Riyadh|SA|24.64|46.77|7',
	  BOB: 'Bora Bora|PF|-16.50|-151.75|7',
	  KBR: 'Kota Bharu|MY|6.13|102.25|7',
	  MQF: 'Magnitogorsk|RU|53.42|59.05|7',
	  DOH: 'Doha|QA|25.29|51.53|7',
	  LPA: 'Las Palmas|ES|27.93|-15.38|7',
	  KRK: 'Krakow|PL|50.08|19.92|7',
	  CTA: 'Catania|IT|37.50|15.09|7',
	  FLN: 'Florianopolis|BR|-27.60|-48.55|7',
	  ECN: 'Ercan|CY|35.16|33.49|7',
	  BLR: 'Bangalore|IN|12.98|77.60|7',
	  PRI: 'Praslin Island|SC|-4.32|55.73|7',
	  STI: 'Santiago|DO|19.47|-70.70|7',
	  SFT: 'Skelleftea|SE|64.77|20.95|7',
	  CFG: 'Cienfuegos|CU|22.15|-80.44|7',
	  ARM: 'Armidale|AU|-30.52|151.65|7',
	  HUY: 'Humberside|GB|53.58|-0.35|7',
	  AUA: 'Aruba|AW|12.50|-69.97|7',
	  CUE: 'Cuenca|EC|-2.88|-78.98|7',
	  AAQ: 'Anapa|RU|44.89|37.32|7',
	  DHN: 'Dothan|US|31.22|-85.39|7',
	  ITO: 'Hilo|US|19.73|-155.09|7',
	  DAM: 'Damascus|SY|33.50|36.30|7',
	  LEH: 'Le Havre|FR|49.49|0.11|7',
	  DMM: 'Dammam|SA|26.43|50.11|7',
	  PIT: 'Pittsburgh|US|40.44|-80.00|7',
	  NAP: 'Naples|IT|40.83|14.25|7',
	  ZQN: 'Queenstown|NZ|-45.02|168.74|7',
	  DTM: 'Dortmund|DE|51.52|7.45|7',
	  KHI: 'Karachi|PK|24.87|67.05|7',
	  CPT: 'Cape Town|ZA|-33.92|18.42|7',
	  KUN: 'Kaunas|LT|54.90|23.90|7',
	  BUS: 'Batumi|GE|41.64|41.63|7',
	  MOV: 'Moranbah|AU|-22.00|148.05|7',
	  CFU: 'Kerkyra|GR|39.62|19.92|7',
	  OSA: 'Osaka|JP|34.69|135.50|6',
	  DAV: 'David|PA|8.43|-82.43|5',
	  BUE: 'Buenos Aires|AR|-34.58|-58.41|5',
	  MMK: 'Murmansk|RU|68.97|33.08|5',
	  IXB: 'Bagdogra|IN|26.70|88.32|5',
	  PNH: 'Phnom Penh|KH|11.55|104.92|5',
	  JAI: 'Jaipur|IN|26.92|75.82|5',
	  NGO: 'Nagoya|JP|35.18|136.91|5',
	  MSU: 'Maseru|LS|-29.32|27.48|5',
	  SGC: 'Surgut|RU|61.25|73.42|5',
	  PZB: 'Pietermaritzburg|ZA|-29.62|30.38|5',
	  IXM: 'Madurai|IN|9.93|78.12|5',
	  UUS: 'Yuzhno Sakhalinsk|RU|46.95|142.74|4',
	  CNM: 'Carlsbad|US|32.34|-104.26|5',
	  PKV: 'Pskov|RU|57.83|28.33|4',
	  SJO: 'San Jose|CR|9.93|-84.08|5',
	  USH: 'Ushuaia|AR|-54.80|-68.30|5',
	  CGP: 'Chittagong|BD|22.33|91.84|5',
	  SUV: 'Suva|FJ|-18.13|178.42|5',
	  TPE: 'Taipei|TW|25.04|121.53|5',
	  AUX: 'Araguaina|BR|-7.19|-48.21|5',
	  YEK: 'Arviat|CA|61.11|-94.06|5',
	  SDY: 'Sidney|US|47.71|-104.19|5',
	  DNZ: 'Denizli|TR|37.77|29.09|5',
	  SQO: 'Storuman|SE|65.10|17.10|5',
	  ASR: 'Kayseri|TR|38.73|35.49|5',
	  MMA: 'Malmo|SE|55.60|13.00|4',
	  PDX: 'Portland|US|45.59|-122.59|5',
	  FKL: 'Franklin|US|35.93|-86.87|5',
	  SGN: 'Ho Chi Minh City|VN|10.75|106.67|5',
	  MSS: 'Massena|US|44.93|-74.89|5',
	  FKB: 'Karlsruhe Baden Baden|DE|48.78|8.08|5',
	  JTY: 'Astypalaia Island|GR|36.57|26.37|5',
	  PZE: 'Penzance|GB|50.12|-5.54|5',
	  ZAD: 'Zadar|HR|44.12|15.24|5',
	  YQB: 'Quebec|CA|46.81|-71.21|5',
	  MAG: 'Madang|PG|-5.22|145.80|5',
	  BSB: 'Brasilia|BR|-15.78|-47.93|4',
	  TAL: 'Tanana|US|65.17|-152.08|5',
	  PUW: 'Pullman|US|46.73|-117.18|5',
	  KLO: 'Kalibo|PH|6.35|124.82|5',
	  OSR: 'Ostrava|CZ|49.83|18.28|5',
	  PEZ: 'Penza|RU|53.20|45.00|5',
	  PTY: 'Panama City|PA|8.97|-79.53|5',
	  BAK: 'Baku|AZ|40.40|49.88|5',
	  SDF: 'Louisville|US|38.27|-85.75|5',
	  MFM: 'Macau|MO|22.20|113.55|5',
	  MPW: 'Mariupol|UA|47.07|37.50|4',
	  PWM: 'Portland|US|43.64|-70.30|5',
	  REP: 'Siem Reap|KH|13.37|103.85|5',
	  PPL: 'Phaplu|NP|27.52|86.58|5',
	  YOW: 'Ottawa|CA|45.42|-75.69|5',
	  IND: 'Indianapolis|US|39.77|-86.16|5',
	  AQJ: 'Aqaba|JO|29.53|35.01|5',
	  ZNE: 'Newman|AU|-23.37|119.73|5',
	  FMN: 'Farmington|US|36.73|-108.22|5',
	  MOD: 'Modesto|US|37.64|-121.00|5',
	  DAD: 'Da Nang|VN|16.07|108.22|5',
	  DYR: 'Anadyr|RU|64.75|177.48|5',
	  HUN: 'Hualien|TW|23.98|121.60|5',
	  FLW: 'Santa Cruz Flores|PT|39.45|-31.12|5',
	  PNQ: 'Pune|IN|18.31|73.55|5',
	  PPG: 'Pago Pago|AS|-14.28|-170.70|5',
	  ETZ: 'Metz Nancy|FR|48.98|6.25|5',
	  VXE: 'Sao Vicente|CV|16.85|-24.97|5',
	  SMI: 'Samos|GR|37.75|26.80|5',
	  CHC: 'Christchurch|NZ|-43.53|172.63|5',
	  AEY: 'Akureyri|IS|65.67|-18.10|5',
	  CMI: 'Champaign|US|40.12|-88.24|5',
	  BAX: 'Barnaul|RU|53.36|83.76|3',
	  NQT: 'Nottingham Uk|GB|52.97|-1.17|3',
	  MCP: 'Macapa|BR|0.04|-51.07|2',
	  LSE: 'La Crosse|US|43.80|-91.24|2',
	  MAS: 'Manus Island|PG|-5.56|154.62|2',
	  BKO: 'Bamako|ML|12.65|-8.00|2',
	  VDS: 'Vadso|NO|70.07|29.77|2',
	  AGA: 'Agadir|MA|30.40|-9.60|2',
	  CYF: 'Chefornak|US|60.16|-164.27|2',
	  DIL: 'Dili|TL|-8.56|125.57|2',
	  MFU: 'Mfuwe|ZM|-12.89|31.92|2',
	  ASO: 'Asosa|ET|10.07|34.53|2',
	  NEF: 'Neftekamsk|RU|56.10|54.35|1',
	  ZTA: 'Tureira|PF|-20.82|-138.50|',
	  ZIH: 'Zihuatanejo|MX|17.63|-101.55|2',
	  PND: 'Punta Gorda|BZ|16.10|-88.80|2',
	  BMW: 'Bordj Badji Mokhtar|DZ|21.32|1.03|2',
	  CFR: 'Caen|FR|49.18|-0.37|2',
	  XBE: 'Bearskin Lake|CA|53.95|-90.98|',
	  BLK: 'Blackpool|GB|53.82|-3.05|2',
	  YRC: '|CA|',
	  OLC: 'Sao Paulo De Olivenca|BR|-3.47|-68.96|2',
	  BEN: 'Benghazi|LY|32.12|20.07|2',
	  EBD: 'El Obeid|SD|13.18|30.22|2',
	  YVM: 'Qikiqtarjuaq|CA|67.58|-63.83|',
	  KNK: 'Kakhonak|US|59.43|-154.85|',
	  JAV: 'Ilulissat|GL|69.22|-51.10|2',
	  ACU: 'Achutupo|PA|9.58|-79.72|',
	  ADQ: 'Kodiak|US|57.79|-152.41|2',
	  SEB: 'Sebha|LY|27.02|14.46|',
	  TLA: 'Teller|US|65.26|-166.36|2',
	  LEJ: 'Leipzig|DE|51.34|12.37|2',
	  UMY: 'Sumy|UA|50.92|34.78|1',
	  BSZ: 'Bartletts|US|58.23|-157.35|2',
	  WST: 'Westerly|US|41.38|-71.83|2',
	  MJN: 'Majunga|MG|-15.72|46.32|2',
	  ODO: 'Odessa|US|31.92|-102.39|1',
	  LDB: 'Londrina|BR|-23.31|-51.16|2',
	  KTA: 'Karratha|AU|-20.73|116.86|2',
	  NNM: 'Naryan-Mar|RU|67.62|53.15|2',
	  AXR: 'Arutua|PF|-15.25|-146.75|2',
	  SHC: 'Shire Indaselassie|ET|12.98|39.13|2',
	  MED: 'Madinah|SA|24.47|39.61|2',
	  RZR: 'Ramsar|IR|36.90|50.68|',
	  LWB: 'Lewisburg|US|35.45|-86.79|2',
	  MAA: 'Chennai|IN|13.09|80.28|2',
	  DUD: 'Dunedin|NZ|-45.87|170.50|2',
	  DOG: 'Dongola|SD|19.18|30.45|2',
	  KCT: 'Koggala|LK|5.98|80.33|2',
	  MWF: 'Maewo|VU|-15.17|168.17|2',
	  EAM: 'Nejran|SA|17.61|44.43|2',
	  YRG: 'Rigolet|CA|54.33|-58.42|',
	  NYA: 'Nyagan|RU|',
	  MII: 'Marilia|BR|-22.21|-49.95|2',
	  ELH: 'North Eleuthera|BS|25.47|-76.69|2',
	  JKG: 'Jonkoping|SE|57.78|14.18|2',
	  GES: 'General Santos|PH|6.11|125.17|2',
	  ZAL: 'Valdivia|CL|-39.81|-73.25|2',
	  PBO: 'Paraburdoo|AU|-23.17|117.75|2',
	  YAT: 'Attawapiskat|CA|52.94|-82.40|2',
	  AGM: 'Tasiilaq|GL|65.60|-37.68|',
	  CJA: 'Cajamarca|PE|-7.16|-78.50|2',
	  ZNZ: 'Zanzibar|TZ|-6.16|39.20|2',
	  SON: 'Espiritu Santo|VU|-15.52|167.22|2',
	  PEG: 'Perugia|IT|43.10|12.38|2',
	  VTB: 'Vitebsk|BY|55.19|30.19|1',
	  BUQ: 'Bulawayo|ZW|-20.15|28.58|2',
	  ACC: 'Accra|GH|5.55|-0.22|2',
	  YCU: 'Yun Cheng|CN|35.02|110.99|',
	  PPV: 'Port Protection|US|56.33|-133.67|',
	  BHZ: 'Belo Horizonte|BR|-19.92|-43.94|2',
	  AZO: 'Kalamazoo|US|42.29|-85.59|2',
	  NOI: 'Novorossiysk|RU|44.72|37.77|1',
	  JAN: 'Jackson|US|32.30|-90.20|2',
	  PKZ: 'Pakse|LA|15.13|105.78|2',
	  PYC: 'Playon Chico|PA|9.57|-79.61|',
	  ESC: 'Escanaba|US|45.75|-87.06|2',
	  KSC: 'Kosice|SK|48.72|21.25|2',
	  TMP: 'Tampere|FI|61.50|23.79|2',
	  NDB: 'Nouadhibou|MR|20.90|-17.04|2',
	  ABC: 'Albacete|ES|38.98|-1.85|2',
	  BFE: 'Bielefeld|DE|52.03|8.53|1',
	  CMF: 'Chambery|FR|45.57|5.93|2',
	  ROS: 'Rosario|AR|-32.95|-60.67|2',
	  SLZ: 'Sao Luiz|BR|-2.53|-44.30|2',
	  JBR: 'Jonesboro|US|35.84|-90.70|2',
	  LNY: 'Lanai|US|20.78|-156.95|2',
	  FAH: 'Farah|AF|32.37|62.12|',
	  BVE: 'Brive La Gaill|FR|45.17|1.53|2',
	  CCJ: 'Kozhikode|IN|11.25|75.77|2',
	  NUP: 'Nunapitchuk|US|60.90|-162.46|2',
	  HGD: 'Hughenden|AU|-20.85|144.20|2',
	  NGS: 'Nagasaki|JP|32.76|129.87|2',
	  MWQ: 'Magwe|MM|20.15|94.92|2',
	  RDM: 'Redmond|US|47.67|-122.12|2',
	  YGW: 'Poste De La Baleine|CA|55.28|-77.77|2',
	  CFC: 'Cacador|BR|-26.78|-51.02|2',
	  BAJ: 'Bali|PG|-4.83|149.13|1',
	  AJI: 'Agri|TR|39.65|43.02|2',
	  KWJ: 'Kwangju|KR|35.15|126.92|2',
	  UIB: 'Quibdo|CO|5.69|-76.64|',
	  OKJ: 'Okayama|JP|34.65|133.92|2',
	  IUE: 'Niue|NU|-19.03|-169.87|2',
	  PBC: 'Puebla|MX|19.05|-98.20|2',
	  BKS: 'Bengkulu|ID|-3.80|102.27|2',
	  LAU: 'Lamu|KE|-2.28|40.90|2',
	  SLC: 'Salt Lake City|US|40.76|-111.89|2',
	  HUS: 'Hughes|US|34.95|-90.47|2',
	  FUG: 'Fuyang|CN|32.90|115.82|2',
	  SGK: 'Sangapi|PG|-5.08|144.77|',
	  XIL: 'Xilinhot|CN|43.97|116.07|2',
	  AOS: 'Amook Bay|US|57.45|-153.82|2',
	  YGX: 'Gillam|CA|56.46|-94.21|2',
	  BMO: 'Bhamo|MM|24.27|97.23|2',
	  UEO: 'Kume Jima|JP|26.37|126.72|2',
	  SVA: 'Savoonga|US|63.69|-170.48|2',
	  SLI: 'Solwezi|ZM|-12.17|26.37|',
	  RFP: 'Raiatea|PF|-16.83|-151.42|2',
	  RZS: 'Sawan|PK|26.97|68.87|',
	  CGY: 'Cagayan|PH|6.98|118.51|2',
	  IFO: 'Ivano Frankovsk|UA|48.92|24.71|2',
	  HEH: 'Heho|MM|20.72|96.82|2',
	  PUG: 'Port Augusta|AU|-32.50|137.77|2',
	  UMD: 'Uummannaq|GL|70.67|-52.12|',
	  KYU: 'Koyukuk|US|64.88|-157.70|2',
	  RIG: 'Rio Grande|BR|-32.03|-52.10|2',
	  LAK: 'Aklavik|CA|68.22|-134.99|',
	  NKI: 'Naukiti|US|55.37|-133.22|',
	  ARK: 'Arusha|TZ|-3.37|36.68|2',
	  SLM: 'Salamanca|ES|40.97|-5.65|2',
	  JFR: 'Paamiut|GL|62.00|-49.75|2',
	  GJT: 'Grand Junction|US|39.06|-108.55|2',
	  YIF: 'Pakuashipi|CA|51.22|-58.67|2',
	  HYA: 'Hyannis|US|41.65|-70.28|2',
	  KSJ: 'Kasos Island|GR|35.42|26.92|2',
	  ACA: 'Acapulco|MX|16.86|-99.89|2',
	  ZVK: 'Savannakhet|LA|104.76|16.56|',
	  ASV: 'Amboseli|KE|-2.63|37.25|',
	  OGG: 'Maui|US|20.90|-156.43|2',
	  SMS: 'Saint Marie|MG|-17.08|49.82|2',
	  TDX: 'Trat|TH|12.24|102.52|2',
	  ERH: 'Errachidia|MA|31.95|-4.40|',
	  ACR: 'Araracuara|CO|-0.38|-72.30|',
	  YLE: 'Wha Ti/Lac La Martre|CA|63.15|-117.27|',
	  YGT: 'Igloolik|CA|69.38|-81.80|2',
	  RIW: 'Riverton|US|40.52|-111.94|2',
	  EWB: 'New Bedford|US|41.64|-70.93|2',
	  IXU: 'Aurangabad|IN|19.88|75.33|2',
	  YKQ: 'Waskaganish|CA|51.20|-78.77|2',
	  BJA: 'Bejaia|DZ|36.75|5.08|2',
	  EKT: 'Eskilstuna|SE|59.37|16.50|1',
	  LNZ: 'Linz|AT|48.31|14.29|2',
	  YXC: 'Cranbrook|CA|49.50|-115.77|2',
	  BDT: 'Bado Lite|CD|4.08|22.45|2',
	  NUI: 'Nuiqsut|US|70.22|-150.98|2',
	  THN: 'Trollhattan|SE|58.27|12.30|2',
	  EAA: 'Eagle|US|64.78|-141.15|2',
	  RXS: 'Roxas City|PH|11.59|122.75|2',
	  OZG: 'Zagora|MA|30.27|-5.85|2',
	  TMJ: 'Termez|UZ|37.27|67.29|2',
	  RAR: 'Rarotonga|CK|-21.23|-159.78|2',
	  ELG: 'El Golea|DZ|30.57|2.86|',
	  REC: 'Recife|BR|-8.05|-34.88|2',
	  KHS: 'Khasab|OM|26.19|56.24|2',
	  OSD: 'Ostersund|SE|63.18|14.65|2',
	  TRS: 'Trieste|IT|45.65|13.78|2',
	  ABQ: 'Albuquerque|US|35.08|-106.65|2',
	  MLY: 'Manley Hot Springs|US|65.00|-150.63|2',
	  SAV: 'Savannah|US|32.08|-81.10|2',
	  GYM: 'Guaymas|MX|27.93|-110.90|2',
	  AWD: 'Aniwa|VU|-19.25|169.50|2',
	  HNH: 'Hoonah|US|58.11|-135.44|2',
	  FMO: 'Muenster|DE|51.96|7.63|2',
	  FMY: 'Fort Myers|US|26.62|-81.84|2',
	  BSX: 'Bassein|MM|16.80|94.78|',
	  ZUH: 'Zhuhai|CN|22.28|113.57|2',
	  BIM: 'Bimini|BS|25.73|-79.25|2',
	  IPA: 'Ipota|VU|-18.75|169.18|2',
	  RGA: 'Rio Grande|AR|-53.78|-67.70|2',
	  VTE: 'Vientiane|LA|17.97|102.60|2',
	  KAN: 'Kano|NG|12.00|8.52|2',
	  XGR: 'Kangiqsualujjuaq|CA|58.69|-65.95|2',
	  CLL: 'College Station|US|30.63|-96.33|2',
	  BHQ: 'Broken Hill|AU|-31.95|141.43|2',
	  KLW: 'Klawock|US|55.55|-133.10|2',
	  YQJ: '|CA|',
	  NGQ: '|CN|',
	  JTR: 'Thira Island|GR|36.40|25.48|2',
	  SIA: 'Xi An|CN|34.26|108.93|2',
	  MVY: 'Martha S Vineyard|US|41.39|-70.62|2',
	  KBU: 'Kotabaru|ID|-3.33|116.33|',
	  HOR: 'Horta|PT|38.53|-28.63|2',
	  NDJ: 'N Djamena|TD|12.11|15.05|2',
	  YUD: 'Umiujaq|CA|56.56|-76.55|2',
	  HUQ: 'Houn|LY|29.50|15.50|',
	  POG: 'Port Gentil|GA|-0.72|8.78|2',
	  AKF: 'Kufrah|LY|24.20|23.33|2',
	  USU: 'Busuanga|PH|12.10|120.17|',
	  PCE: 'Painter Creek|US|57.16|-157.43|',
	  YAA: 'Anahim Lake|CA|52.45|-125.30|',
	  FBD: 'Faizabad|AF|',
	  LPK: 'Lipetsk|RU|52.62|39.60|2',
	  ANA: 'Anaheim|US|33.84|-117.92|1',
	  RCM: 'Richmond|AU|-42.74|147.44|2',
	  MRA: 'Misurata|LY|32.38|15.09|2',
	  KGS: 'Kos Gr|GR|36.79|27.10|2',
	  SPC: 'Santa Cruz|ES|28.62|-17.75|2',
	  NOZ: 'Novokuznetsk|RU|53.75|87.17|2',
	  TIR: 'Tirupati|IN|13.65|79.42|2',
	  YXT: 'Terrace|CA|54.52|-128.60|2',
	  WWT: 'Newtok|US|60.94|-164.63|2',
	  CXR: 'Cam Ranh|VN|11.92|109.16|1',
	  ONP: 'Newport|US|44.63|-124.05|2',
	  SHE: 'Shenyang|CN|41.79|123.43|2',
	  LEB: 'Lebanon|US|40.34|-76.41|2',
	  CSL: 'San Luis Obispo|US|35.28|-120.66|2',
	  VRC: 'Virac|PH|13.58|124.20|',
	  ILD: 'Alguaire|ES|41.73|0.54|',
	  QSR: 'Salerno|IT|40.66|14.80|2',
	  MJD: 'Mohenjodaro|PK|27.34|68.14|2',
	  SVI: 'San Vincent|CO|2.15|-74.77|',
	  SJT: 'San Angelo|US|31.46|-100.44|2',
	  HGA: 'Hargeisa|SO|9.56|44.06|2',
	  ANV: 'Anvik|US|62.66|-160.21|2',
	  HOD: 'Hodeidah|YE|14.75|42.98|2',
	  OAJ: 'Jacksonville|US|34.83|-77.61|2',
	  OHO: 'okhotsk|Russia|0',
	  QSF: 'Setif|DZ|36.18|5.33|',
	  LZO: 'Luzhou|CN|31.28|118.13|2',
	  HAQ: 'Hanimaadhoo|MV|6.75|73.17|',
	  WWK: 'Wewak|PG|-3.55|143.63|2',
	  YCK: 'Colville|CA|67.17|-126.00|2',
	  HJR: 'Khajuraho|IN|24.85|79.93|2',
	  RBA: 'Rabat|MA|34.02|-6.83|2',
	  LXG: 'Luang Namtha|LA|21.05|101.47|',
	  IGM: 'Kingman|US|35.19|-114.05|2',
	  VPS: 'Valparaiso|US|30.49|-86.53|2',
	  MOU: 'Mountain Village|US|37.93|-107.86|2',
	  TOU: 'Touho|NC|-20.80|165.25|',
	  BKZ: 'Bukoba|TZ|-1.33|31.81|2',
	  BOO: 'Bodo|NO|67.28|14.38|2',
	  KPR: 'Port Williams|US|39.46|-95.03|2',
	  INH: 'Inhambane|MZ|-23.87|35.41|',
	  CSG: 'Columbus|US|32.51|-84.94|2',
	  ABL: 'Ambler|US|40.15|-75.22|2',
	  CGI: 'Cape Girardeau|US|37.31|-89.52|2',
	  NTL: 'Newcastle|AU|-32.93|151.78|2',
	  ZAT: 'Zhaotong|CN|27.33|103.76|',
	  FUK: 'Fukuoka|JP|33.58|130.40|2',
	  MPM: 'Maputo|MZ|-25.97|32.59|2',
	  LNJ: 'Lincang|CN|23.74|100.03|',
	  CUL: 'Culiacan|MX|24.80|-107.39|2',
	  ILY: 'Islay|GB|55.80|-6.20|2',
	  KSQ: 'Karshi|UZ|38.80|65.77|2',
	  ACH: 'Altenrhein|CH|47.48|9.55|2',
	  MCX: 'Makhachkala|RU|43.00|47.50|2',
	  CBO: 'Cotabato|PH|7.22|124.25|2',
	  OUZ: 'Zouerate|MR|22.69|-12.46|2',
	  URS: 'Kursk|RU|51.73|36.19|1',
	  SEN: 'Southend|GB|51.54|0.71|2',
	  GUM: 'Guam|GU|13.44|144.74|2',
	  BCM: 'Bacau|RO|46.57|26.90|2',
	  BHS: 'Bathurst|AU|-33.41|149.65|2',
	  CEN: 'Ciudad Obregon|MX|27.48|-109.93|2',
	  MDC: 'Manado|ID|1.50|124.84|2',
	  NKG: 'Nanjing|CN|32.06|118.78|2',
	  DOU: 'Dourados|BR|-22.22|-54.81|2',
	  UNA: 'Una BR|BR|-15.35|-39.00|2',
	  TOY: 'Toyama|JP|36.68|137.22|2',
	  AYQ: 'Ayers Rock|AU|-25.38|131.08|2',
	  LQN: 'Qala Nau|AF|34.95|63.67|',
	  DIG: 'Diqing|CN|26.13|109.31|2',
	  GCI: 'Guernsey|GB|49.44|-2.60|2',
	  YMN: 'Makkovik|CA|55.17|-59.17|2',
	  HGL: 'Helgoland|DE|54.18|7.89|2',
	  STW: 'Stavropol|RU|45.04|41.97|2',
	  GUA: 'Guatemala City|GT|14.62|-90.53|2',
	  IGT: 'Nazran|RU|0',
	  TJN: 'Takume|PF|-15.85|-142.27|',
	  KAA: 'Kazama|ZM|-10.22|31.13|2',
	  CCN: 'Chakcharan|AF|34.53|65.27|',
	  TGG: 'Kuala Terengganu|MY|5.33|103.13|2',
	  LOH: 'Loja|EC|-3.99|-79.20|2',
	  TYN: 'Taiyuan|CN|37.87|112.56|2',
	  BNK: 'Ballina|AU|-28.87|153.57|2',
	  JHS: 'Sisimiut|GL|66.93|-53.68|2',
	  DDC: 'Dodge City|US|37.75|-100.02|2',
	  TPI: 'Tapini|PG|-8.37|146.98|2',
	  BVI: 'Birdsville|AU|-25.90|139.37|2',
	  HVG: 'Honningsvag|NO|70.98|25.98|2',
	  AZS: 'Samana|DO|19.27|-69.74|',
	  MZR: 'Mazar I Sharif|AF|36.71|67.21|',
	  OSW: 'Orsk|RU|51.20|58.57|2',
	  YGQ: 'Geraldton|CA|49.73|-86.95|2',
	  SDP: 'Sand Point|US|55.34|-160.50|2',
	  KHC: 'Kerch|AK|45.37|36.45|1',
	  KOT: 'Kotlik|US|63.03|-163.55|2',
	  LYA: 'Luoyang|CN|34.68|112.45|2',
	  BXU: 'Butuan|PH|8.95|125.54|2',
	  HYD: 'Hyderabad|IN|17.38|78.47|2',
	  TQR: 'San Domino Island|IT|42.12|15.48|2',
	  CMG: 'Corumba|BR|-19.01|-57.65|2',
	  CVU: 'Corvo Island|PT|39.70|-31.10|2',
	  CMW: 'Camaguey|CU|21.38|-77.92|2',
	  YSG: 'Lutselke/Snowdrift|CA|62.42|-110.08|',
	  BIR: 'Biratnagar|NP|26.48|87.28|2',
	  ROT: 'Rotorua|NZ|-38.14|176.25|2',
	  AHO: 'Alghero|IT|40.56|8.32|2',
	  NCP: 'Luzon Island|PH|16.00|121.00|',
	  KCM: 'Kahramanmaras|TR|37.53|36.95|',
	  RVV: 'Rairua|PF|-25.88|-147.65|',
	  JDH: 'Jodhpur|IN|26.29|73.03|2',
	  TVC: 'Traverse City|US|44.76|-85.62|2',
	  SLP: 'San Luis Potosi|MX|22.15|-100.98|2',
	  BST: 'Bost|AF|31.55|64.37|',
	  KHG: 'Kashi|CN|39.45|75.98|2',
	  MQN: 'Mo I Rana|NO|66.32|14.17|2',
	  MDU: 'Mendi|PG|-6.15|143.65|2',
	  BHK: 'Bukhara|UZ|39.77|64.43|2',
	  PMQ: 'Perito Moreno|AR|-41.12|-71.42|2',
	  DED: 'Dehra Dun|IN|30.32|78.03|2',
	  BWN: 'Bandar Seri Bagawan|BN|4.88|114.93|2',
	  MHK: 'Manhattan|US|39.18|-96.57|2',
	  ILZ: 'Zilina|SK|49.22|18.74|2',
	  FAT: 'Fresno|US|36.75|-119.77|2',
	  HGU: 'Mt Hagen|PG|-5.83|144.29|2',
	  YGN: 'Greenway Sound|CA|50.85|-126.85|',
	  YNJ: 'Yanji|CN|42.91|129.51|2',
	  ANU: 'Antigua|AG|17.05|-61.80|2',
	  OOM: 'Cooma|AU|-36.23|149.13|2',
	  PRC: 'Prescott|US|34.65|-112.42|2',
	  LRH: 'La Rochelle|FR|46.17|-1.15|2',
	  GGG: 'Longview|US|32.39|-94.72|2',
	  YBE: 'Uranium City|CA|59.57|-108.62|2',
	  SVC: 'Silver City|US|32.77|-108.28|2',
	  TMU: 'Tambor|CR|9.73|-85.02|2',
	  BJM: 'Bujumbura|BI|-3.38|29.36|2',
	  POM: 'Port Moresby|PG|-9.46|147.19|2',
	  BJF: 'Batsfjord|NO|70.63|29.70|2',
	  BFN: 'Bloemfontein|ZA|-29.13|26.20|2',
	  SLA: 'Salta|AR|-24.78|-65.42|2',
	  DAU: 'Daru|PG|-9.09|143.19|2',
	  KGP: 'Kogalym|RU|62.20|74.53|2',
	  PQS: 'Pilot Station|US|61.94|-162.88|2',
	  LBF: 'North Platte|US|41.12|-100.77|2',
	  MHT: 'Manchester|US|42.94|-71.44|2',
	  RAH: 'Rafha|SA|29.64|43.50|2',
	  YEV: 'Inuvik|CA|68.35|-133.72|2',
	  RRG: 'Rodrigues Island|MU|-19.75|63.35|2',
	  KKE: 'Kerikeri|NZ|-35.22|173.97|2',
	  GEO: 'Georgetown|GY|6.80|-58.17|2',
	  SJD: 'San Jose Cabo|MX|23.17|-109.70|2',
	  MAJ: 'Majuro|MH|7.10|171.38|2',
	  DUT: 'Dutch Harbor|US|48.01|-120.25|2',
	  TBZ: 'Tabriz|IR|38.08|46.29|2',
	  CKH: 'Chokurdah|RU|70.63|147.88|2',
	  FYV: 'Fayetteville|US|36.00|-94.17|2',
	  KPO: 'Pohang|KR|35.99|129.42|2',
	  BFD: 'Bradford|US|41.80|-78.64|2',
	  GET: 'Geraldton|AU|-28.77|114.60|2',
	  HDF: 'Heringsdorf|DE|53.97|14.17|2',
	  OBU: 'Kobuk|US|66.91|-156.88|2',
	  CAK: 'Akron|US|41.08|-81.52|2',
	  YLW: 'Kelowna|CA|49.88|-119.49|2',
	  LPX: 'Liepaja|LV|56.52|21.02|1',
	  VAO: 'Suavanao|SB|-7.57|158.67|',
	  PBH: 'Paro|BT|27.43|89.42|2',
	  LYR: 'Longyearbyen|NO|78.21|15.80|2',
	  AGN: 'Angoon|US|57.50|-134.58|2',
	  ABX: 'Albury|AU|-36.08|146.92|2',
	  KRC: 'Kerinci|ID|-1.72|101.25|',
	  KKA: 'Koyuk|US|64.93|-161.16|2',
	  NUA: 'Nuwara Eliya|LK|6.97|80.77|2',
	  CIJ: 'Cobija|BO|-11.03|-68.73|2',
	  CDC: 'Cedar City|US|37.68|-113.06|2',
	  SYJ: 'Sirjan|IR|29.55|55.66|',
	  NAL: 'Nalchik|RU|43.50|43.61|2',
	  BUN: 'Buenaventura|CO|3.83|-77.00|',
	  AOK: 'Karpathos|GR|35.50|27.23|2',
	  AKY: 'Akyab|MM|20.15|92.90|2',
	  SYR: 'Syracuse|US|43.05|-76.15|2',
	  LRE: 'Longreach|AU|-23.45|144.25|2',
	  LLF: 'Ling Ling|CN|26.70|111.96|2',
	  CRW: 'Charleston|US|38.37|-81.59|2',
	  STV: 'Surat|IN|21.17|72.83|2',
	  DVL: 'Devils Lake|US|48.11|-98.87|2',
	  HRO: 'Harrison|US|40.97|-73.71|2',
	  GNY: 'Sanliurfa|TR|37.15|38.79|2',
	  LIR: 'Liberia|CR|10.63|-85.43|2',
	  SJU: 'San Juan|PR|18.44|-66.00|2',
	  VVC: 'Villavicencio|CO|4.08|-73.57|',
	  CDV: 'Cordova|US|60.49|-145.47|2',
	  SDE: 'Santiago Del Estero|AR|-27.78|-64.27|2',
	  PMC: 'Puerto Montt|CL|-41.47|-72.94|2',
	  TDK: 'Taldy Kurgan|KZ|45.15|78.43|1',
	  TTT: 'Taitung|TW|22.75|121.10|2',
	  CHA: 'Chattanooga|US|35.05|-85.31|2',
	  CLY: 'Calvi|FR|42.57|8.75|2',
	  MAB: 'Maraba|BR|-5.37|-49.12|2',
	  FEG: 'Fergana|UZ|40.39|71.78|2',
	  MGS: 'Mangaia Island|CK|-21.93|-157.93|2',
	  CME: 'Ciudad Del Carmen|MX|18.63|-91.83|2',
	  NOJ: 'Nojabrxsk|RU|63.17|75.62|2',
	  WVB: 'Walvis Bay|NA|-22.96|14.51|2',
	  BID: 'Block Island|US|41.17|-71.58|2',
	  YYL: 'Lynn Lake|CA|56.83|-101.08|2',
	  SVS: 'Stevens Village|US|66.01|-149.09|2',
	  KIF: 'Kingfisher Lake|CA|53.01|-89.85|',
	  EYK: 'Beloyarsky|RU|63.70|66.70|0',
	  LDN: 'Lamidanda|NP|27.25|86.72|',
	  UND: 'Kunduz|AF|36.66|68.91|',
	  GAJ: 'Yamagata|JP|38.25|140.34|2',
	  CSY: 'Cheboksary|RU|56.13|47.27|2',
	  FMA: 'Formosa|AR|-26.18|-58.18|2',
	  ONT: 'Ontario|US|34.06|-117.61|2',
	  UAK: 'Narsarsuaq|GL|61.17|-45.42|2',
	  ACV: 'Eureka|US|40.80|-124.16|2',
	  IDA: 'Idaho Falls|US|43.47|-112.03|2',
	  PVH: 'Porto Velho|BR|-8.76|-63.90|2',
	  ALS: 'Alamosa|US|37.47|-105.87|2',
	  ODE: 'Odense|DK|55.40|10.38|1',
	  YBP: 'Yibin|CN|28.77|104.57|2',
	  BGW: 'Baghdad|IQ|33.34|44.39|2',
	  PVD: 'Providence|US|41.82|-71.41|2',
	  CHU: 'Chuathbaluk|US|61.58|-159.24|',
	  UIK: 'Ust-Ilimsk|RU|58.13|102.55|1',
	  MSJ: 'Misawa|JP|40.68|141.36|2',
	  INC: 'Yinchuan|CN|38.47|106.27|2',
	  NCY: 'Annecy|FR|45.90|6.12|2',
	  GRQ: 'Groningen|NL|53.22|6.57|2',
	  LSY: 'Lismore|AU|-28.81|153.28|2',
	  KWL: 'Guilin|CN|25.28|110.29|2',
	  SSJ: 'Sandnessjoen|NO|66.02|12.63|2',
	  YAK: 'Yakutat|US|59.55|-139.73|2',
	  CHO: 'Charlottesville|US|38.03|-78.48|2',
	  MLM: 'Morelia|MX|19.70|-101.18|2',
	  SFE: 'San Fernando|PH|15.03|120.69|2',
	  JST: 'Johnstown|US|40.33|-78.92|2',
	  LGG: 'Liege|BE|50.64|5.57|2',
	  ERI: 'Erie|US|42.13|-80.09|2',
	  ZMT: 'Masset|CA|54.02|-132.10|2',
	  CPV: 'Campina Grande|BR|-7.23|-35.88|2',
	  SCX: 'Salina Cruz|MX|16.17|-95.20|2',
	  VIX: 'Vitoria|BR|-20.32|-40.34|2',
	  IXC: 'Chandigarh|IN|30.74|76.79|2',
	  PAT: 'Patna|IN|25.60|85.12|2',
	  MES: 'Medan|ID|3.58|98.67|2',
	  MWX: 'Gwangju|KR|34.99|126.38|',
	  GZT: 'Gaziantep|TR|37.06|37.38|2',
	  LOK: 'Lodwar|KE|3.15|35.60|',
	  KHN: 'Nanchang|CN|28.68|115.88|2',
	  CFS: 'Coffs Harbour|AU|-30.30|153.13|2',
	  DIB: 'Dibrugarh|IN|27.48|94.90|2',
	  ABI: 'Abilene|US|32.45|-99.73|2',
	  MYT: 'Myitkyina|MM|25.38|97.40|2',
	  GOA: 'Genoa|IT|44.41|8.93|2',
	  TAC: 'Tacloban|PH|9.22|123.55|2',
	  KHZ: 'Kauehi|PF|-15.77|-145.12|2',
	  OGZ: 'Vladikavkaz|RU|43.20|44.60|2',
	  XQP: 'Quepos|CR|9.45|-84.15|2',
	  SHB: 'Nakashibetsu|JP|43.57|144.97|2',
	  NRV: 'Guam|GU|13.46|144.76|1',
	  AIU: 'Atiu Island|CK|-19.99|-158.12|2',
	  YHZ: 'Halifax|CA|44.65|-63.60|2',
	  GHA: 'Ghardaia|DZ|32.49|3.67|2',
	  PKA: 'Napaskiak|US|60.71|-161.77|2',
	  BDA: 'Bermuda|BM|32.30|-64.75|2',
	  HET: 'Hohhot|CN|40.81|111.65|2',
	  JSH: 'Sitia|GR|35.22|26.12|2',
	  WRO: 'Wroclaw|PL|51.10|17.03|2',
	  YYR: 'Goose Bay|CA|53.40|-60.16|2',
	  HRL: 'Harlingen|US|26.19|-97.70|2',
	  KNS: 'King Island|AU|-39.88|143.88|2',
	  KIS: 'Kisumu|KE|-0.10|34.75|2',
	  AUK: 'Alakanuk|US|62.69|-164.62|2',
	  MNU: 'Moulmein|MM|16.49|97.63|2',
	  BAV: 'Baotou|CN|40.65|109.82|2',
	  MOC: 'Montes Claros|BR|-16.73|-43.86|2',
	  RBX: '|SD|',
	  CNY: 'Moab|US|38.57|-109.55|2',
	  LBL: 'Liberal|US|37.04|-100.92|2',
	  BZE: 'Belize City|BZ|17.50|-88.20|2',
	  HAD: 'Halmstad|SE|56.67|12.86|2',
	  TUU: 'Tabuk|SA|28.38|36.58|2',
	  HME: 'Hassi Messaoud|DZ|31.70|6.05|2',
	  BZK: 'Briansk|RU|53.27|34.33|1',
	  DIE: 'Diegosuarez|MG|-12.35|49.29|2',
	  KHY: 'Khoy|IR|38.55|44.95|2',
	  DOY: 'Dongying|CN|37.52|118.79|',
	  JLR: 'Jabalpur|IN|23.17|79.95|2',
	  YQK: 'Kenora|CA|49.82|-94.43|2',
	  KMW: 'Kostroma|RU|57.77|40.93|1',
	  XBJ: 'Birjand|IR|32.90|59.25|',
	  DGE: 'Mudgee|AU|-32.43|149.58|2',
	  SAC: 'Sacramento|US|38.58|-121.49|2',
	  FDH: 'Friedrichshafen|DE|47.65|9.48|2',
	  TGH: 'Tongoa|VU|-16.90|168.55|',
	  RBV: 'Ramata|SB|-8.17|157.64|',
	  HAK: 'Haikou|CN|20.05|110.34|2',
	  LUH: 'Ludhiana|IN|30.90|75.85|2',
	  PPK: 'Petropavlovsk|KZ|54.88|69.16|2',
	  ISG: 'Ishigaki|JP|24.33|124.15|2',
	  CPE: 'Campeche|MX|19.85|-90.53|2',
	  YPL: 'Pickle Lake|CA|51.21|-90.21|2',
	  LSA: 'Losuia|PG|-8.53|151.07|2',
	  TQL: '|RU|',
	  GIS: 'Gisborne|NZ|-38.65|178.00|2',
	  YRR: 'Stuart Island|CA|50.42|-125.17|',
	  AAX: 'Araxa|BR|-19.59|-46.94|2',
	  YGR: 'Iles De Madeleine|CA|47.37|-61.90|2',
	  LUO: 'Luena|AO|-11.78|19.92|2',
	  PGM: 'Port Graham|US|59.35|-151.83|',
	  IRJ: 'La Rioja|AR|-29.43|-66.85|2',
	  AOJ: 'Aomori|JP|40.82|140.75|2',
	  AKK: 'Akhiok|US|56.95|-154.17|2',
	  OSI: 'Osijek|HR|45.55|18.69|2',
	  WTL: 'Tuntatuliak|US|60.35|-162.63|2',
	  NBC: 'Naberevnye Chelny|RU|55.76|52.43|2',
	  YHP: 'Poplar Hill|CA|52.08|-94.30|',
	  PKR: 'Pokhara|NP|28.23|83.98|2',
	  LMT: 'Klamath Falls|US|42.22|-121.78|2',
	  BNY: 'Bellona|SB|-11.30|159.82|2',
	  HJJ: 'Zhi Jiang|CN|30.42|111.75|',
	  EJA: 'Barrancabermeja|CO|7.07|-73.85|2',
	  VDZ: 'Valdez|US|61.13|-146.24|2',
	  ENY: 'Yanan|CN|22.20|113.09|2',
	  KLN: 'Larsen Bay|US|57.54|-153.98|2',
	  CAL: 'Campbelltown|GB|55.44|-5.69|2',
	  KKH: 'Kongiganak|US|59.97|-162.75|2',
	  ACT: 'Waco|US|31.55|-97.15|2',
	  RUE: '|CD|',
	  PNR: 'Pointe Noire|CG|1.56|15.78|2',
	  EYP: 'El Yopal|CO|5.32|-72.39|',
	  YGZ: 'Grise Fiord|CA|76.42|-82.96|',
	  FBA: 'Fonte Boa|BR|-2.53|-66.08|2',
	  TEZ: 'Tezpur|IN|26.71|92.80|',
	  TCT: 'Takotna|US|62.99|-156.06|2',
	  BWO: 'Balakovo|RU|51.87|47.75|1',
	  GAM: 'Gambell|US|63.78|-171.74|2',
	  KHM: 'Khamtis|MM|25.99|95.68|',
	  DIR: 'Dire Dawa|ET|9.59|41.87|2',
	  SOG: 'Sogndal|NO|58.33|6.30|2',
	  AGP: 'Malaga|ES|36.72|-4.42|2',
	  MEA: 'Macae|BR|-22.37|-41.79|2',
	  ULP: 'Quilpie|AU|-26.62|144.25|2',
	  BQB: 'Busselton|AU|-33.65|115.33|2',
	  KZO: 'Kzyl Orda|KZ|44.83|65.83|2',
	  SLQ: 'Sleetmute|US|61.70|-157.17|2',
	  MLS: 'Miles City|US|46.41|-105.84|2',
	  CIK: 'Chalkyitsik|US|66.65|-143.72|2',
	  YGG: 'Ganges Harbor|CA|48.85|-123.50|',
	  TKQ: 'Kigoma|TZ|-4.88|29.67|',
	  KWG: 'Krivoy Rog|UA|48.05|33.22|2',
	  KQT: 'Qurghonteppa|TJ|37.87|68.86|',
	  NME: 'Nightmute|US|60.48|-164.72|2',
	  LSW: 'Lhoksumawe|ID|5.17|97.17|',
	  MWZ: 'Mwanza|TZ|-2.52|32.90|2',
	  TAB: 'Tobago|TT|11.25|-60.67|2',
	  MUH: 'Mersa Matruh|EG|31.35|27.25|',
	  NOB: 'Nosara Beach|CR|9.80|-85.53|',
	  YPJ: 'Aupaluk|CA|59.30|-69.60|2',
	  BNN: 'Bronnoysund|NO|65.47|12.22|2',
	  ALH: 'Albany|AU|-35.02|117.89|2',
	  KOK: 'Kokkola|FI|63.83|23.12|2',
	  OAI: '|AF|',
	  USN: 'Ulsan|KR|35.54|129.32|2',
	  SYY: 'Stornoway|GB|58.22|-6.37|2',
	  OPS: 'Sinop|BR|-11.86|-55.50|2',
	  NAW: 'Narathiwat|TH|6.42|101.82|2',
	  DKR: 'Dakar|SN|14.70|-17.44|2',
	  YEA: 'Edmonton|CA|53.55|-113.47|2',
	  CUR: 'Curacao|CW|12.17|-69.00|2',
	  YWB: 'Kangiqsujuaq|CA|61.60|-71.96|2',
	  PHX: 'Phoenix|US|33.45|-112.07|2',
	  AJL: 'Aizawl|IN|23.73|92.72|2',
	  LBU: 'Labuan|MY|5.28|115.24|2',
	  SAN: 'San Diego|US|32.72|-117.16|2',
	  TEB: 'Teterboro|US|40.86|-74.06|2',
	  YAC: 'Cat Lake|CA|51.72|-91.82|',
	  GOT: 'Gothenburg|SE|57.71|11.97|2',
	  YCS: 'Chesterfield Inlet|CA|63.34|-90.70|2',
	  WNZ: 'Wenzhou|CN|28.02|120.65|2',
	  PEX: '|RU|65.12|57.13|1',
	  AFT: 'Afutara Aerodrome|SB|-9.20|160.85|',
	  OZZ: 'Ourzazate|MA|30.92|-6.92|2',
	  NRD: 'Norderney|DE|53.70|7.15|2',
	  GUC: 'Gunnison|US|38.55|-106.92|2',
	  KOS: 'SIHANOUKVILLE|CAMBODIA|0',
	  GAO: 'Guantanamo|CU|20.14|-75.21|2',
	  KMI: 'Miyazaki|JP|31.90|131.43|2',
	  PED: 'Pardubice|CZ|50.04|15.78|2',
	  PFN: 'Panama City|US|30.21|-85.68|2',
	  NUX: 'Novy Urengoy|RU|66.08|76.52|2',
	  GDL: 'Guadalajara|MX|20.67|-103.33|2',
	  XSC: 'South Caicos|TC|21.52|-71.50|2',
	  KLD: 'Tver|RU|56.86|35.89|1',
	  BLJ: 'Batna|DZ|35.57|6.18|',
	  BUC: 'Burketown|AU|-17.72|139.57|2',
	  KSR: 'Sandy River|US|56.23|-160.23|2',
	  TLS: 'Toulouse|FR|43.60|1.44|2',
	  PLN: 'Pellston|US|45.55|-84.78|2',
	  YIN: 'Yining|CN|29.03|114.57|2',
	  OHH: '|RU|',
	  EDO: 'Edremit|TR|39.58|27.03|',
	  QOW: '|NG|',
	  YPT: 'Pender Harbor|CA|49.49|-123.98|',
	  PGF: 'Perpignan|FR|42.70|2.90|2',
	  ISN: 'Williston|US|48.15|-103.62|2',
	  PBU: 'Putao|MM|27.33|97.42|',
	  EQS: 'Esquel|AR|-42.90|-71.32|2',
	  NLA: 'N Dola|ZM|-12.97|28.63|2',
	  THS: 'Sukhothai|TH|17.01|99.79|2',
	  KTG: 'Ketapang|ID|-6.12|106.95|2',
	  VHM: 'Vilhelmina|SE|64.62|16.65|2',
	  SFJ: 'Kangerlussuaq|GL|72.63|-55.63|2',
	  KPY: 'Port Bailey|US|57.93|-153.03|2',
	  CVM: 'Ciudad Victoria|MX|23.73|-99.13|2',
	  UIN: 'Quincy|US|42.25|-71.00|2',
	  CKC: 'Cherkassy|UA|49.42|32.00|1',
	  PUF: 'Pau Fr|FR|43.38|-0.42|2',
	  TMT: 'Trombetas|BR|-7.40|-72.42|2',
	  YXL: 'Sioux Lookout|CA|50.07|-91.98|2',
	  MCN: 'Macon|US|32.84|-83.63|2',
	  ULB: 'Ulei|VU|-16.42|168.33|',
	  ZBF: 'Bathurst|CA|47.63|-65.74|2',
	  SZV: 'Suzhou|CN|31.31|120.62|1',
	  KGK: 'Koliganek|US|59.73|-157.28|2',
	  BJU: 'Bajura|NP|29.37|81.33|',
	  PIP: 'Pilot Point|US|33.40|-96.96|2',
	  YYE: 'Fort Nelson|CA|58.81|-122.70|2',
	  RJK: 'Rijeka|HR|45.34|14.41|2',
	  CIC: 'Chico|US|39.73|-121.84|2',
	  SKE: 'Skien|NO|59.20|9.60|2',
	  HYN: 'Huangyan|CN|28.65|121.26|2',
	  BAS: 'Balalae|SB|-6.98|155.88|2',
	  GAN: 'Gan Island|MV|-0.69|73.16|',
	  GMR: 'Gambier Island|PF|-23.17|-135.00|2',
	  OER: 'Ornskoldsvik|SE|63.30|18.72|2',
	  WSX: 'Westsound|US|48.67|-122.88|',
	  SIQ: 'Singkep|ID|-0.50|104.42|',
	  ADH: 'Aldan|RU|58.60|125.40|1',
	  ILO: 'Iloilo|PH|10.70|122.56|2',
	  BUW: 'Baubau|ID|-4.38|123.05|2',
	  KVK: 'Kirovsk|RU|67.58|33.58|',
	  PNP: 'Popondetta|PG|-8.77|148.23|2',
	  YKU: 'Chisasibi|CA|53.67|-78.33|2',
	  SVD: 'St Vincent|VC|13.25|-61.20|2',
	  INU: 'Nauru Island|NR|-0.55|166.92|2',
	  HOV: 'Orsta Volda|NO|62.18|6.15|2',
	  YGK: 'Kingston|CA|44.23|-76.48|2',
	  SSR: 'Sara|VU|-15.50|168.30|',
	  STX: 'St Croix|VI|17.70|-64.80|2',
	  BRE: 'Bremen|DE|53.08|8.81|2',
	  GKA: 'Goroka|PG|-6.08|145.38|2',
	  KSL: 'Kassala|SD|15.46|36.40|2',
	  NLD: 'Nuevo Laredo|MX|27.50|-99.52|2',
	  KRG: 'Karasabai|GY|4.02|-59.52|2',
	  FRL: 'Forli|IT|44.22|12.05|2',
	  YRB: 'Resolute|CA|74.72|-94.97|',
	  YVA: 'Moroni|KM|-11.70|43.24|2',
	  ZCO: 'Temuco|CL|-38.73|-72.60|2',
	  YUY: 'Rouyn Noranda|CA|48.21|-78.83|2',
	  MGQ: 'Mogadishu|SO|2.07|45.37|2',
	  GMZ: 'San Sebastian De La Gomera|ES|28.10|-17.10|2',
	  NGB: 'Ningbo|CN|29.88|121.54|2',
	  RUN: 'Reunion Island|RE|-20.88|55.52|2',
	  DAB: 'Daytona Beach|US|29.21|-81.02|2',
	  MCW: 'Mason City|US|43.15|-93.20|2',
	  SGF: 'Springfield|US|37.24|-93.39|2',
	  PVE: 'El Porvenir|PA|9.00|-81.00|',
	  YES: 'Yasouj|IR|30.70|51.55|',
	  JCU: 'Ceuta|ES|35.89|-5.32|2',
	  GNU: 'Goodnews Bay|US|59.12|-161.59|2',
	  FIH: 'Kinshasa|CD|-4.33|15.31|2',
	  SGY: 'Skagway|US|59.46|-135.31|2',
	  WAE: 'Wadi Ad Dawasir|SA|20.50|45.20|',
	  MYW: 'Mtwara|TZ|-10.27|40.18|2',
	  SHR: 'Sheridan|US|44.80|-106.96|2',
	  KLR: 'Kalmar|SE|56.67|16.37|2',
	  ELV: 'Elfin Cove|US|58.19|-136.34|2',
	  MCY: 'Maroochydore|AU|-26.65|153.10|2',
	  SDN: 'Sandane|NO|61.77|6.22|2',
	  YUX: 'Hall Beach|CA|68.79|-81.24|2',
	  SLL: 'Salalah|OM|17.02|54.08|2',
	  VIS: 'Visalia|US|36.33|-119.29|2',
	  YYB: 'North Bay|CA|46.32|-79.47|2',
	  ESR: 'El Salvador|CL|-26.25|-69.62|2',
	  SOB: 'Saarmelleek|HU|46.71|17.24|1',
	  VHZ: 'Vahitahi|PF|-18.58|-138.83|',
	  ALF: 'Alta|NO|69.97|23.24|2',
	  CTC: 'Catamarca|AR|-28.47|-65.78|2',
	  LDG: 'Leshukonskoye|RU|64.90|45.75|',
	  AMV: 'Amderma|RU|69.77|61.55|2',
	  PUQ: 'Punta Arenas|CL|-53.15|-70.92|2',
	  GCW: '|US|',
	  GPS: 'Galapagos Is|EC|-0.43|-90.28|2',
	  POJ: 'Patos De Minas|BR|-18.58|-46.52|2',
	  FSC: 'Figari|FR|41.49|9.13|2',
	  DNR: 'Dinard|FR|48.63|-2.07|2',
	  MDK: 'Mbandaka|CD|0.07|18.27|2',
	  RKZ: 'Rikaze|CN|2',
	  WYA: 'Whyalla|AU|-33.03|137.58|2',
	  GJA: 'Guanaja|HN|16.40|-85.90|2',
	  TOM: 'Tombouctou|ML|16.77|-3.01|2',
	  EGX: 'Egegik|US|58.22|-157.38|2',
	  CTU: 'Chengdu|CN|30.67|104.07|1',
	  YVQ: 'Norman Wells|CA|65.28|-126.83|2',
	  VEE: 'Venetie|US|67.01|-146.42|2',
	  YXY: 'Whitehorse|CA|60.72|-135.05|2',
	  SAF: 'Santa Fe|US|35.62|-106.09|2',
	  FDE: 'Forde|NO|61.45|5.84|2',
	  HUX: 'Santa Cruz Huatulco|MX|15.75|-96.13|2',
	  PVR: 'Puerto Vallarta|MX|20.62|-105.23|2',
	  GAE: 'Gabes|TN|33.88|10.12|2',
	  TOX: 'Tobolsk|RU|58.20|68.27|1',
	  PUU: 'Puerto Asis|CO|0.52|-76.50|2',
	  PAH: 'Paducah|US|37.08|-88.60|2',
	  SAT: 'San Antonio|US|29.42|-98.49|2',
	  KET: 'Kengtung|MM|21.28|99.60|2',
	  FBM: 'Lubumbashi|CD|-11.67|27.47|2',
	  VAN: 'Van TR|TR|38.46|43.33|2',
	  ZTB: 'Tete A La Baleine|CA|50.72|-59.33|2',
	  EDR: 'Edward River|AU|-14.90|141.62|2',
	  PWE: 'Pevek|RU|69.78|170.60|2',
	  EAT: 'Wenatchee|US|47.42|-120.31|2',
	  IAR: 'Yaroslavl|RU|57.63|39.87|1',
	  BOH: 'Bournemouth|GB|50.72|-1.88|2',
	  BWW: '|CU|',
	  PTJ: 'Portland|AU|-38.33|141.60|2',
	  TSN: 'Tianjin|CN|39.14|117.18|2',
	  RCL: 'Redcliffe|VU|-27.21|153.07|',
	  TLM: 'Tlemcen|DZ|35.02|-1.45|',
	  YBW: 'Bedwell Harbor|CA|48.75|-123.23|2',
	  SSG: 'Malabo|GQ|3.75|8.78|2',
	  IXI: 'Lilabari|IN|27.29|94.09|2',
	  IKO: 'Nikolski|US|52.94|-168.85|',
	  EBJ: 'Esbjerg|DK|55.47|8.45|2',
	  FAC: 'Faaite|PF|-16.68|-145.33|',
	  LNV: 'Lihir Island|PG|-3.08|152.58|2',
	  YRJ: 'Roberval|CA|48.52|-72.23|2',
	  ZGU: 'Gaua|VU|-14.22|167.59|',
	  NRL: 'North Ronaldsay|GB|59.37|-2.43|2',
	  PAG: 'Pagadian|PH|7.83|123.46|',
	  JIJ: 'Jijiga|ET|9.37|42.77|',
	  CBB: 'Cochabamba|BO|-17.38|-66.15|2',
	  LAN: 'Lansing|US|42.73|-84.56|2',
	  RRR: '|PF|',
	  DRK: '|CR|',
	  NJF: 'Al Najaf|IQ|',
	  MPL: 'Montpellier|FR|43.60|3.88|2',
	  KCH: 'Kuching|MY|1.55|110.33|2',
	  LPI: 'Linkoping|SE|58.42|15.62|2',
	  HAS: 'Hail|SA|27.55|41.38|2',
	  ERZ: 'Erzurum|TR|39.91|41.28|2',
	  REN: 'Orenburg|RU|51.78|55.12|2',
	  SDD: 'Lubango|AO|-14.92|13.50|2',
	  JSU: 'Maniitsoq|GL|65.42|-52.90|2',
	  KKJ: 'Kita Kyushu|JP|33.83|130.95|2',
	  BGM: 'Binghamton|US|42.10|-75.92|2',
	  KMO: 'Manokotak|US|58.98|-159.06|2',
	  TCX: 'Tabas|IR|33.68|56.90|',
	  FYU: 'Ft Yukon|US|66.58|-145.21|2',
	  MLL: 'Marshall|US|32.54|-94.37|2',
	  LKA: 'Larantuka|ID|-8.35|122.98|2',
	  RNL: 'Rennell|SB|-11.67|160.30|2',
	  ETR: 'Etrepagny|EC|49.36|1.68|',
	  LDH: 'Lord Howe Island|AU|-31.55|159.08|2',
	  JUJ: 'Jujuy|AR|-24.18|-65.30|2',
	  GEV: 'Gallivare|SE|67.13|20.70|2',
	  TST: 'Trang|TH|7.56|99.61|2',
	  BES: 'Brest|FR|48.45|-4.42|2',
	  YLL: 'Lloydminster|CA|53.28|-110.00|2',
	  MUE: 'Kamuela|US|20.01|-155.67|2',
	  KRT: 'Khartoum|SD|15.59|32.53|2',
	  BKI: 'Kota Kinabalu|MY|5.98|116.07|2',
	  UNG: 'Kiunga|PG|-6.12|141.30|2',
	  SJW: 'Shijiazhuang|CN|38.04|114.48|2',
	  TNO: 'Tamarindo|CR|10.92|-85.45|2',
	  DIN: 'Dien Bien Phu|VN|21.38|103.02|2',
	  PSP: 'Palm Springs|US|33.83|-116.55|2',
	  MKP: 'Makemo|PF|-16.45|-143.67|2',
	  RVK: 'Roervik|NO|64.88|11.23|2',
	  IMF: 'Imphal|IN|24.82|93.95|2',
	  RUA: 'Arua|UG|3.02|30.93|2',
	  CGA: 'Craig|US|55.48|-133.15|2',
	  WKA: 'Wanaka|NZ|-44.70|169.15|2',
	  BTU: 'Bintulu|MY|3.17|113.03|2',
	  CLM: 'Port Angeles|US|48.12|-123.43|2',
	  BTZ: 'Bursa|TR|40.19|29.06|1',
	  AAN: 'Al Ain|AE|24.19|55.76|2',
	  TIM: 'Timika|ID|-4.78|136.53|2',
	  DEF: 'Dezful|IR|32.44|48.38|',
	  GFF: 'Griffith|AU|-34.28|146.03|2',
	  SRX: 'Sert|LY|31.06|16.61|',
	  NAV: 'Nevsehir|TR|38.63|34.71|2',
	  EUG: 'Eugene|US|44.05|-123.09|2',
	  MXM: 'Morombe|MG|-21.73|43.35|2',
	  NDU: 'Runda|NA|-17.95|19.72|2',
	  SAH: 'Sanaa|YE|15.35|44.21|2',
	  GGW: 'Glasgow|US|48.21|-106.62|2',
	  LOS: 'Lagos|NG|6.45|3.40|2',
	  IMT: 'Iron Mountain|US|45.82|-88.07|2',
	  DGO: 'Durango|MX|24.03|-104.67|2',
	  BHO: 'Bhopal|IN|23.27|77.40|2',
	  YAP: 'Yap FM|FM|9.48|138.08|2',
	  TGK: 'Taganrog|RU|47.23|38.88|2',
	  ISC: 'Scilly, Isles Of|GB|49.90|-6.40|2',
	  YKL: 'Schefferville|CA|54.80|-66.83|2',
	  LKB: 'Lakeba|FJ|-18.22|-178.78|2',
	  YWL: 'Williams Lake|CA|52.14|-122.14|2',
	  CTD: 'Chitre|PA|7.97|-80.43|2',
	  OVS: 'SOVETSKY|RU|43.26|-90.74|0',
	  TXK: 'Texarkana|US|33.43|-94.05|2',
	  NVI: 'Navoi|UZ|40.08|65.38|2',
	  LYG: 'Lianyungang|CN|34.60|119.16|2',
	  BKC: 'Buckland|US|42.59|-72.79|2',
	  YKT: 'Klemtu|CA|52.60|-128.52|',
	  TKJ: 'Tok Ak|US|63.30|-143.00|2',
	  SIT: 'Sitka|US|57.05|-135.33|2',
	  CRI: 'Crooked Island|BS|22.75|-74.22|2',
	  VLL: 'Valladolid|ES|41.65|-4.72|2',
	  AXD: 'Alexandroupolis|GR|40.85|25.87|2',
	  RAS: 'Rasht|IR|37.28|49.59|2',
	  DLG: 'Dillingham|US|59.04|-158.46|2',
	  CNX: 'Chiang Mai|TH|18.79|98.98|2',
	  HUI: 'Hu PG|VN|16.40|107.70|2',
	  YYT: 'St Johns|CA|47.62|-52.75|2',
	  RTG: 'Ruteng|ID|-8.60|120.45|2',
	  BNC: 'Beni|CD|0.57|29.47|',
	  SDR: 'Santander|ES|43.46|-3.80|2',
	  JSY: 'Syros Island|GR|37.42|24.95|2',
	  SZX: 'Shenzhen|CN|22.55|114.07|2',
	  YXP: 'Pangnirtung|CA|66.15|-65.71|2',
	  DGP: 'Daugavpils|LV|55.88|26.53|1',
	  UBA: 'Uberaba|BR|-19.75|-47.93|2',
	  TNG: 'Tangier|MA|35.78|-5.81|2',
	  SUL: 'Sui PK|PK|28.65|69.17|2',
	  EPR: 'Esperance|AU|-33.87|121.90|2',
	  RIC: 'Richmond|US|37.51|-77.32|2',
	  GNV: 'Gainesville|US|29.69|-82.28|2',
	  HKD: 'Hakodate|JP|41.78|140.74|2',
	  LFW: 'Lome|TG|6.13|1.22|2',
	  POZ: 'Poznan|PL|52.42|16.97|2',
	  VVZ: 'Illizi|DZ|26.53|8.55|',
	  IMP: 'Imperatriz|BR|-5.53|-47.49|2',
	  KDO: 'Kadhdhoo|MV|1.88|73.52|',
	  MOT: 'Minot|US|48.23|-101.30|2',
	  DJE: 'Djerba|TN|33.87|10.86|2',
	  SDK: 'Sandakan|MY|5.83|118.12|2',
	  BBN: 'Bario|MY|3.75|115.45|2',
	  PGK: 'Pangkalpinang|ID|-2.13|106.13|2',
	  CAZ: 'Cobar|AU|-31.50|145.82|2',
	  KFP: 'False Pass|US|54.85|-163.41|2',
	  SJC: 'San Jose|US|37.34|-121.89|2',
	  YXS: 'Prince George|CA|53.92|-122.75|2',
	  TMM: 'Tamatave|MG|-18.17|49.38|2',
	  KTB: 'Thorne Bay|US|55.69|-132.52|2',
	  OVD: 'Asturias|ES|43.57|-6.03|2',
	  SLY: 'Salehard|RU|66.58|66.60|2',
	  LCJ: 'Lodz|PL|51.75|19.47|1',
	  TIH: 'Tikehau|PF|-15.00|-147.75|2',
	  RDD: 'Redding|US|40.59|-122.39|2',
	  RGI: 'Rangiroa Island|PF|-14.97|-147.67|2',
	  BNX: 'Banja Luka|BA|44.78|17.19|2',
	  RDV: 'Red Devil|US|61.76|-157.31|2',
	  YJT: 'Stephenville|CA|48.55|-58.58|2',
	  SCT: 'Socotra|YE|12.50|54.00|2',
	  JOS: 'Jos NG|NG|9.87|8.89|2',
	  OLH: 'Old Harbor|US|57.20|-153.30|2',
	  RJA: 'Rajahmundry|IN|16.98|81.78|2',
	  NQU: 'Nuqui|CO|5.70|-77.28|2',
	  HIB: 'Hibbing|US|47.43|-92.94|2',
	  FLO: 'Florence|US|34.19|-79.72|2',
	  TJQ: 'Tanjung Pandan|ID|-2.75|107.65|2',
	  GDX: 'Magadan|RU|59.57|150.80|2',
	  BDJ: 'Banjarmasin|ID|-3.33|114.58|2',
	  BFL: 'Bakersfield|US|35.37|-119.02|2',
	  YDF: 'Deer Lake|CA|49.22|-57.40|2',
	  SVB: 'Sambava|MG|-14.27|50.17|2',
	  NDY: 'Sanday|GB|59.25|-2.58|2',
	  KVR: 'KAVALEROVO |RU|27.65|-80.41|0',
	  JYV: 'Jyvaskyla|FI|62.23|25.73|2',
	  ABE: 'Allentown|US|40.61|-75.49|2',
	  ZHY: '|CN|',
	  TCD: 'Tarapaca|CO|-2.93|-69.77|',
	  BBK: 'Kasane|BW|-17.82|25.15|2',
	  PHS: 'Phitsanulok|TH|16.83|100.25|2',
	  GUR: 'Alotau|PG|-10.30|150.42|2',
	  CCC: 'Cayo Coco|CU|23.04|-80.32|2',
	  PBP: 'Punta Islita|CR|10.42|-85.17|',
	  KER: 'Kerman|IR|30.29|57.08|2',
	  MRQ: 'Marinduque|PH|13.36|121.82|',
	  KHE: 'Kherson|UA|46.68|32.63|1',
	  LGK: 'Langkawi|MY|6.32|99.85|2',
	  EGO: 'Belgorod|RU|50.61|36.58|2',
	  HFD: 'Hartford|US|41.76|-72.69|2',
	  MPN: 'Mount Pleasant|FK|-51.82|-58.45|2',
	  SBN: 'South Bend|US|41.68|-86.25|2',
	  CYI: 'Chiayi|TW|23.47|120.38|2',
	  CCU: 'Kolkata|IN|22.57|88.37|2',
	  LIL: 'Lille|FR|50.63|3.07|2',
	  MOL: 'Molde|NO|62.73|7.18|2',
	  SHW: 'Sharurah|SA|17.42|47.10|2',
	  TGM: 'Tirgu Mures|RO|46.53|24.53|2',
	  EMD: 'Emerald|AU|-23.53|148.17|2',
	  VLD: 'Valdosta|US|30.83|-83.28|2',
	  AUQ: 'Atuona|PF|-9.80|-139.03|2',
	  ZUM: 'Churchhill Falls|CA|53.56|-64.11|2',
	  KYA: 'Konya|TR|37.87|32.48|2',
	  IGR: 'Iguazu|AR|-25.74|-54.47|2',
	  EEK: 'Eek|US|60.22|-162.02|',
	  LCG: 'La Coruna|ES|43.37|-8.40|2',
	  SWF: 'Newburgh|US|41.50|-74.01|2',
	  CAJ: 'Canaima|VE|6.25|-62.83|2',
	  WRE: 'Whangarei|NZ|-35.73|174.32|2',
	  AET: 'Allakaket|US|66.57|-152.65|2',
	  GYN: 'Goiania|BR|-16.68|-49.25|2',
	  TTA: 'Tan Tan|MA|28.45|-11.08|2',
	  FWA: 'Ft Wayne|US|40.98|-85.19|2',
	  CFB: 'Cabo Frio|BR|-22.88|-42.02|2',
	  NSN: 'Nelson|NZ|-41.30|173.22|2',
	  LBR: 'Labrea|BR|-7.25|-64.85|',
	  LUV: 'Langgur|ID|-5.67|132.72|2',
	  KXF: 'Koro|FJ|-17.33|179.83|2',
	  AMI: 'Mataram|ID|-8.58|116.12|2',
	  AFA: 'San Rafael|AR|-34.60|-68.33|2',
	  EGN: 'Geneina|SD|13.45|22.45|2',
	  VKG: 'Rach Gia|VN|10.02|105.08|2',
	  PPP: 'Proserpine|AU|-20.42|148.58|2',
	  EGE: 'Eagle|US|39.65|-106.91|2',
	  ZLO: 'Manzanillo|MX|19.11|-104.35|2',
	  GWT: 'Westerland|DE|54.91|8.30|2',
	  HHQ: 'Hua Hin|TH|12.57|99.96|1',
	  WIC: 'Wick|GB|58.43|-3.08|2',
	  TIN: 'Tindouf|DZ|27.65|-8.21|2',
	  YZG: 'Salluit|CA|62.20|-75.63|2',
	  PYJ: 'Polyarnyj|RU|66.42|112.05|1',
	  KIR: 'Kerry County|IE|52.05|-9.50|2',
	  BHR: 'Bharatpur|NP|27.68|84.43|',
	  PIH: 'Pocatello|US|42.87|-112.45|2',
	  PQI: 'Presque Isle|US|46.68|-68.02|2',
	  TRG: 'Tauranga|NZ|-37.69|176.17|2',
	  AKN: 'King Salmon|US|58.69|-156.66|2',
	  OWB: 'Owensboro|US|37.77|-87.11|2',
	  SOQ: 'Sorong|ID|-0.88|131.25|2',
	  GRZ: 'Graz|AT|47.07|15.45|2',
	  YQL: 'Lethbridge|CA|49.70|-112.82|2',
	  YRL: 'Red Lake|CA|51.05|-93.82|2',
	  YAY: 'St Anthony|CA|51.37|-55.58|2',
	  YQM: 'Moncton|CA|46.12|-64.80|2',
	  BJW: 'Bajawa|ID|-8.78|120.98|2',
	  CJJ: 'Cheongju|KR|36.64|127.49|2',
	  BET: 'Bethel|US|41.37|-73.41|2',
	  THQ: '|CN|',
	  TCG: 'Tacheng|CN|46.67|83.33|',
	  SSM: 'Sault Ste Marie|US|46.50|-84.35|2',
	  TAH: 'Tanna|VU|-19.33|169.25|2',
	  XMS: 'Macas|EC|-2.32|-78.13|2',
	  LTK: 'Latakia|SY|35.52|35.78|2',
	  KOA: 'Kona|US|19.73|-156.05|2',
	  YKA: 'Kamloops|CA|50.67|-120.32|2',
	  KCA: 'Kuqa|CN|41.73|82.94|2',
	  DRT: 'Del Rio|US|29.36|-100.90|2',
	  NZH: 'Manzhouli|CN|49.57|117.33|',
	  PXU: 'Pleiku|VN|13.98|108.00|2',
	  MKY: 'Mackay|AU|-21.15|149.20|2',
	  EFL: 'Kefalonia|GR|38.12|20.51|2',
	  BMI: 'Bloomington|US|40.48|-88.93|2',
	  MVR: 'Maroua|CM|10.60|14.32|2',
	  KOJ: 'Kagoshima|JP|31.60|130.55|2',
	  DMU: 'Dimapur|IN|25.90|93.73|2',
	  YEI: 'Bursa|TR|40.23|29.55|',
	  URG: 'Uruguaina|BR|-5.81|-37.44|2',
	  KUS: 'Kulusuk|GL|65.57|-37.18|2',
	  CWB: 'Curitiba|BR|-25.43|-49.27|2',
	  ODN: 'Long Seridan|MY|3.98|115.07|2',
	  YHK: 'Gjoa Haven|CA|68.63|-95.88|2',
	  DPL: 'Dipolog|PH|8.59|123.34|2',
	  SNN: 'Shannon|IE|52.70|-8.86|2',
	  WEF: 'Weifang|CN|36.71|119.10|2',
	  EJH: 'Wedjh|SA|26.20|36.48|2',
	  SXP: 'Sheldon Point|US|62.53|-164.83|2',
	  CPD: 'Coober Pedy|AU|-29.02|134.72|2',
	  TUB: 'Tubuai|PF|-23.35|-149.47|',
	  RNA: 'Arona|SB|-9.86|161.98|',
	  IZO: 'Izumo|JP|35.37|132.77|2',
	  VEL: 'Vernal|US|40.46|-109.53|2',
	  OTD: 'Contadora|PA|8.67|-79.57|2',
	  YER: 'Fort Severn|CA|56.03|-87.83|',
	  OSM: 'Mosul|IQ|36.34|43.12|2',
	  ANR: 'Antwerp|BE|51.19|4.45|',
	  NHA: 'Nha Trang|VN|12.25|109.18|2',
	  CBI: 'Cape Barren Island TAS|AU|-40.33|148.17|2',
	  SHY: 'Shinyanga|TZ|-3.67|33.43|2',
	  KCZ: 'Kochi|JP|33.54|133.68|2',
	  ABR: 'Aberdeen|US|45.45|-98.43|2',
	  MUX: 'Multan|PK|30.20|71.48|2',
	  WNA: 'Napakiak|US|60.70|-161.95|2',
	  SKC: 'Suki|PG|-8.08|141.80|2',
	  JOL: 'Jolo|PH|6.06|121.01|2',
	  SPP: 'Menongue|AO|-14.64|17.73|',
	  IPT: 'Williamsport|US|41.24|-77.00|2',
	  KKQ: '|RU|',
	  WTP: 'Woitape|PG|-8.55|147.25|2',
	  BJX: 'Leon|MX|21.12|-101.67|2',
	  PFQ: 'Parsabad|IR|39.61|47.88|',
	  DAT: 'Datong|CN|40.09|113.29|2',
	  HCR: 'Holy Cross|US|42.60|-91.22|2',
	  MTS: 'Manzini|SZ|-26.48|31.37|2',
	  MWK: 'Matak|ID|3.30|106.27|',
	  SCO: 'Aktau|KZ|48.03|72.83|2',
	  HOF: 'Hofuf|SA|25.50|49.83|2',
	  KRO: 'Kurgan|RU|55.43|65.30|2',
	  YQZ: 'Quesnel|CA|53.00|-122.50|2',
	  RJL: 'Logrono|ES|42.47|-2.45|2',
	  YUT: '|CA|',
	  XAP: 'Chapeco|BR|-27.10|-52.62|2',
	  KLF: 'Kaluga|RU|54.50|36.27|1',
	  SJL: 'Sao Gabriel|BR|-30.33|-54.33|',
	  BSG: 'Bata|GQ|1.91|9.80|',
	  LOD: 'Longana|VU|-15.32|167.97|2',
	  VIJ: 'Virgin Gorda|VG|18.50|-64.40|2',
	  SXL: 'Sligo|IE|54.27|-8.48|2',
	  FUT: 'Futuna|WF|-14.25|-178.15|2',
	  IWA: 'Ivanovo|RU|56.99|40.99|1',
	  QRW: 'Warri|NG|5.60|5.82|',
	  HSL: 'Huslia|US|65.70|-156.40|2',
	  WJR: 'Wajir|KE|1.77|40.60|',
	  BME: 'Broome|AU|-17.97|122.23|2',
	  REG: 'Reggio Calabria|IT|38.11|15.66|2',
	  NUK: 'Nukutavake|PF|-19.28|-138.83|2',
	  TAE: 'Daegu|KR|35.87|128.59|2',
	  FJR: 'Al Fujairah|AE|25.11|56.33|2',
	  ROK: 'Rockhampton|AU|-23.38|150.50|2',
	  YLH: 'Lansdowne|CA|60.26|-134.77|2',
	  DMD: 'Doomadgee|AU|-18.00|138.83|2',
	  JNS: 'Narsaq|GL|77.30|-68.84|2',
	  AGF: 'Agen|FR|44.20|0.63|2',
	  KBL: 'Kabul|AF|34.52|69.18|2',
	  LUP: 'Kalaupapa|US|21.19|-156.99|2',
	  BWA: 'Bhairawa|NP|27.50|83.42|',
	  PJM: 'Puerto Jimenez|CR|8.58|-83.33|',
	  AES: 'Aalesund|NO|62.47|6.15|2',
	  AXT: 'Akita|JP|39.72|140.12|2',
	  HKB: 'Healy Lake|US|63.87|-148.97|2',
	  ROR: 'Koror|PW|7.34|134.48|2',
	  DTI: 'Diamantina|BR|-18.23|-43.65|2',
	  YHI: 'Holman Island|CA|70.65|-117.73|2',
	  TNC: 'Tin City|US|35.56|-121.08|2',
	  AKX: 'Aqtobe|KZ|50.28|57.21|2',
	  NUS: 'Norsup|VU|-16.07|167.38|2',
	  COK: 'Kochi|IN|9.97|76.23|2',
	  ISU: 'Sulaimaniyah|IQ|35.56|45.31|',
	  GTA: 'Gatokae|SB|-8.77|158.18|',
	  RUI: 'Ruidoso|US|33.33|-105.67|1',
	  PVU: 'Provo|US|40.23|-111.66|2',
	  STS: 'Santa Rosa|US|38.51|-122.81|2',
	  PSC: 'Pasco|US|46.24|-119.10|2',
	  AAL: 'Aalborg|DK|57.05|9.93|2',
	  RNO: 'Reno|US|39.53|-119.81|2',
	  MXZ: 'Meixian|CN|24.30|116.12|2',
	  DGT: 'Dumaguete|PH|9.31|123.31|2',
	  EVG: 'Sveg|SE|62.03|14.35|2',
	  UET: 'Quetta|PK|30.19|67.01|2',
	  CKG: 'Chongqing|CN|29.56|106.55|2',
	  ANE: 'Angers|FR|47.47|-0.55|2',
	  JBQ: 'Higuero|DO|',
	  KYP: 'Kyaukpyu|MM|22.87|96.43|2',
	  KXU: 'Katiu|PF|-16.34|-144.40|',
	  GCK: 'Garden City|US|37.93|-100.73|2',
	  FTU: 'Fort Dauphin|MG|-25.03|47.00|2',
	  QUO: 'Uyo|NG|4.88|8.09|2',
	  GNA: 'Grodna|BY|53.68|23.81|2',
	  KVL: 'Kivalina|US|67.73|-164.53|2',
	  VAW: 'Vardoe|NO|70.35|31.05|2',
	  DJJ: 'Jayapura|ID|-2.53|140.70|2',
	  SKU: 'Skiros|GR|38.88|24.53|2',
	  EIS: 'Beef Island|VG|18.44|-64.54|2',
	  KKC: 'Khon Kaen|TH|16.43|102.83|2',
	  TDD: 'Trinidad|BO|-14.83|-64.90|2',
	  AQP: 'Arequipa|PE|-16.40|-71.53|2',
	  PET: 'Pelotas|BR|-31.77|-52.34|2',
	  TBB: 'Tuy Hoa|VN|13.08|109.30|2',
	  GDE: 'Gode|ET|5.90|43.63|2',
	  MNT: 'Minto|US|48.29|-97.37|2',
	  SNA: 'Santa Ana|US|33.75|-117.87|2',
	  BGR: 'Bangor|US|44.81|-68.83|2',
	  XIC: 'Xichang|CN|27.98|102.19|',
	  RVN: 'Rovaniemi|FI|66.50|25.72|2',
	  JIM: 'Jimma|ET|7.67|36.83|2',
	  IYK: 'Inyokern|US|35.65|-117.81|2',
	  KSA: 'Kosrae|FM|5.35|162.96|',
	  LGL: 'Long Lellang|MY|3.42|115.13|2',
	  LKO: 'Lucknow|IN|26.85|80.92|2',
	  LBV: 'Libreville|GA|0.38|9.45|2',
	  MID: 'Merida|MX|20.97|-89.62|2',
	  YFX: 'Fox Harbour|CA|52.37|-55.68|',
	  SUF: 'Lamezia Terme|IT|38.97|16.31|2',
	  UAQ: 'San Juan|AR|-31.54|-68.54|2',
	  IPI: 'Ipiales|CO|0.83|-77.64|2',
	  SYX: 'Sanya|CN|18.24|109.50|2',
	  MXX: 'Mora|SE|61.00|14.55|2',
	  KLX: 'Kalamata|GR|37.04|22.11|2',
	  ENU: 'Enugu|NG|6.43|7.48|2',
	  SBY: 'Salisbury Ocean City|US|38.34|-75.51|2',
	  YTM: 'Mont Tremblant|CA|46.25|-74.58|2',
	  OAG: 'Orange|AU|-33.38|149.12|2',
	  JQE: 'Jaque|PA|7.50|-78.17|',
	  AZN: 'Andizhan|UZ|40.73|72.30|2',
	  RKD: 'Rockland|US|42.13|-70.92|2',
	  SWJ: 'South West Bay|VU|-16.25|167.17|',
	  DBQ: 'Dubuque|US|42.50|-90.66|2',
	  ATQ: 'Amritsar|IN|31.63|74.87|2',
	  BGA: 'Bucaramanga|CO|7.13|-73.13|2',
	  MNG: 'Maningrida|AU|-12.05|134.23|',
	  VAP: 'Valparaiso|CL|-33.05|-71.60|1',
	  HOQ: 'Hof De|DE|50.29|11.86|2',
	  BBI: 'Bhubaneswar|IN|20.23|85.83|2',
	  BRL: 'Burlington|US|40.79|-91.12|2',
	  IVL: 'Ivalo|FI|68.65|27.60|2',
	  CHT: 'Chatham Island|NZ|-43.80|-176.35|2',
	  DEZ: 'Deirezzor|SY|35.33|40.15|2',
	  LNB: 'Lamen Bay|VU|-16.58|168.18|2',
	  NNT: 'Nan Th|TH|18.80|100.78|2',
	  JDO: 'Juazeiro Do Norte|BR|-7.21|-39.32|2',
	  ESD: 'East Sound|US|48.60|-122.84|2',
	  MCV: 'McArthur River|AU|-16.47|136.09|',
	  UIP: 'Quimper|FR|48.00|-4.10|2',
	  PGU: '|IR|',
	  LNX: 'Smolensk|RU|54.78|32.04|1',
	  VOZ: 'Voronezh|RU|51.67|39.17|2',
	  VOL: 'Volos|GR|39.37|22.95|2',
	  MBE: 'Monbetsu|JP|44.35|143.35|2',
	  ATY: 'Watertown|US|44.91|-97.15|2',
	  MMJ: 'Matsumoto|JP|36.23|137.97|2',
	  YNB: 'Yanbo|SA|24.12|38.07|2',
	  PLP: 'La Palma|PA|8.33|-78.13|',
	  SYZ: 'Shiraz|IR|29.61|52.54|2',
	  NST: 'Nakhon Si Tham|TH|8.47|99.95|2',
	  SFL: 'Sao Filipe|CV|14.90|-24.52|2',
	  IMK: 'Simikot|NP|29.97|81.82|',
	  IOM: 'Isle Of Man|GB|54.08|-4.63|2',
	  JUM: 'Jumla|NP|29.27|82.19|',
	  GYS: 'Guang Yuan|CN|32.40|105.70|2',
	  TUG: 'Tuguegarao|PH|17.64|121.73|',
	  UTS: '|RU|',
	  TEN: 'Tongren|CN|27.72|109.19|2',
	  WWP: 'Whale Pass|US|56.67|-133.83|',
	  KIH: 'Kish Island|IR|26.54|53.98|2',
	  ERF: 'Erfurt|DE|50.98|11.03|2',
	  MSZ: 'Namibe|AO|-15.20|12.15|2',
	  OSS: 'Osh|KG|40.62|72.82|2',
	  LMY: 'Lake Murray|PG|-6.80|141.43|2',
	  ZCL: 'Zacatecas|MX|22.78|-102.57|2',
	  ABK: 'Kabri Dar|ET|6.73|44.27|',
	  LLB: '|CN|',
	  ACS: 'Achinsk|RU|56.27|90.57|1',
	  YXH: 'Medicine Hat|CA|50.05|-110.67|2',
	  GSP: 'Greenville|US|34.85|-82.38|2',
	  PNV: 'Panevezys|LT|55.73|24.38|1',
	  SAL: 'San Salvador|SV|13.71|-89.20|2',
	  LPY: 'Le Puy|FR|45.03|3.88|2',
	  TKD: 'Takoradi|GH|4.89|-1.77|',
	  TMS: 'Sao Tome|ST|0.38|6.72|2',
	  LQM: 'Puerto Leguizamo|CO|-0.30|-74.58|',
	  BQN: 'Aguadilla|PR|18.50|-67.13|2',
	  PTU: 'Platinum|US|59.01|-161.82|2',
	  SCM: 'Scammon Bay|US|61.84|-165.58|2',
	  JDF: 'Juiz De Fora|BR|-21.76|-43.35|2',
	  AJA: 'Ajaccio|FR|41.92|8.73|2',
	  APO: 'Apartado|CO|7.89|-76.63|2',
	  CAY: 'Cayenne|GF|4.93|-52.33|2',
	  RIS: 'Rishiri|JP|45.18|141.25|2',
	  EDA: 'Edna Bay|US|55.95|-133.67|',
	  TAT: 'Poprad Tatry|SK|49.07|20.24|2',
	  KNG: 'Kaimana|ID|-3.65|133.75|2',
	  BRT: 'Bathurst Isl|AU|-11.77|130.63|2',
	  FCA: 'Kalispell|US|48.20|-114.31|2',
	  NDE: 'Mandera|KE|3.93|41.85|2',
	  AMA: 'Amarillo|US|35.22|-101.83|2',
	  CYB: 'Cayman Brac|KY|19.72|-79.82|2',
	  SRG: 'Semarang|ID|-6.97|110.42|2',
	  UTN: 'Upington|ZA|-28.45|21.25|2',
	  ATK: 'Atqasuk|US|70.47|-157.40|2',
	  EZS: 'Elazig|TR|38.68|39.22|2',
	  BBA: 'Balmaceda|CL|-45.92|-71.68|2',
	  DMB: 'Taraz|KZ|42.90|71.37|1',
	  CUQ: 'Coen|AU|-13.93|143.20|2',
	  NOC: 'Knock|IE|53.92|-8.82|2',
	  VAS: 'Sivas|TR|39.75|37.02|2',
	  RVT: 'Ravensthorpe|AU|-33.58|120.03|2',
	  WLS: 'Wallis Island|WF|-13.23|-176.17|2',
	  YIH: 'Yichang|CN|30.71|111.28|2',
	  GCM: 'Grand Cayman Island|KY|19.29|-81.36|1',
	  TBT: 'Tabatinga|BR|-4.25|-69.94|2',
	  YBR: 'Brandon|CA|49.90|-99.95|1',
	  EGC: 'Bergerac|FR|44.85|0.48|2',
	  UNK: 'Unalakleet|US|63.87|-160.79|2',
	  DJB: 'Jambi|ID|-1.60|103.62|2',
	  ZOS: 'Osorno|CL|-40.57|-73.15|2',
	  LWT: 'Lewistown|US|40.60|-77.57|2',
	  AEI: 'Algeciras|ES|36.13|-5.45|2',
	  BQG: 'Bogorodskoe|RU|2',
	  SRA: 'Santa Rosa|BR|-27.87|-54.48|',
	  RVD: 'Rio Verde|BR|-5.43|-40.43|2',
	  BSR: 'Basra|IQ|30.49|47.82|2',
	  AOI: 'Ancona|IT|43.60|13.51|2',
	  TBH: 'Tablas|PH|12.32|122.08|',
	  JQA: 'Qaarsut|GL|70.73|-52.65|2',
	  TIU: 'Timaru|NZ|-44.40|171.25|2',
	  EOI: 'Eday|GB|59.18|-2.78|2',
	  SNP: 'St Paul Island|US|57.15|-170.22|2',
	  SCW: 'Syktyvkar|RU|61.67|50.81|2',
	  JOI: 'Joinville|BR|-26.30|-48.85|2',
	  CAB: 'Cabinda|AO|-5.55|12.20|2',
	  TWA: 'Twin Hills|US|59.08|-160.28|2',
	  YZS: 'Coral Harbour|CA|64.13|-83.17|2',
	  HBA: 'Hobart|AU|-42.92|147.33|2',
	  OKI: 'Oki Island|JP|36.17|133.32|2',
	  MLN: 'Melilla|ES|35.29|-2.94|2',
	  NIB: 'Nikolai|US|63.01|-154.38|2',
	  YZP: 'Sandspit|CA|53.25|-131.81|2',
	  PMR: 'Palmerston|NZ|-40.32|175.62|2',
	  HYS: 'Hays|US|38.88|-99.33|2',
	  SCE: 'State College|US|40.79|-77.86|2',
	  SUX: 'Sioux City|US|42.50|-96.40|2',
	  OGS: 'Ogdensburg|US|44.69|-75.49|2',
	  MMO: 'Maio|CV|15.13|-23.22|2',
	  MZI: 'Mopti|ML|14.49|-4.19|2',
	  BSD: 'Baoshan|CN|48.10|123.95|2',
	  BLE: 'Borlange|SE|60.48|15.42|2',
	  EGM: 'Sege|SB|-8.55|157.85|2',
	  LVI: 'Livingstone|ZM|-17.85|25.87|2',
	  UBS: 'Columbus|US|33.47|-88.38|',
	  LKN: 'Leknes|NO|61.47|5.42|2',
	  LWS: 'Lewiston|US|46.37|-117.01|2',
	  LSC: 'La Serena|CL|-29.91|-71.25|2',
	  TER: 'Terceira|PT|38.72|-27.22|2',
	  MBS: 'Saginaw|US|43.42|-83.95|2',
	  SLH: 'Sola|VU|-13.88|167.55|',
	  LRT: 'Lorient|FR|47.75|-3.37|2',
	  IKS: 'Tiksi|RU|71.70|128.90|2',
	  RMT: '|PF|',
	  KEW: 'Keewaywin|CA|52.99|-92.84|',
	  GPT: 'Gulfport|US|30.37|-89.09|2',
	  LMC: 'Lamacarena|CO|3.32|-73.90|',
	  YSY: 'Sachs Harbour|CA|71.99|-125.24|',
	  AFL: 'Alta Floresta|BR|-9.90|-55.90|2',
	  URE: 'Kuressaare|EE|58.25|22.50|2',
	  GIL: 'Gilgit|PK|35.92|74.30|2',
	  MAM: 'Matamoros|MX|25.88|-97.50|2',
	  YHO: 'Hopedale|CA|55.47|-60.20|2',
	  PBM: 'Paramaribo|SR|5.83|-55.17|2',
	  MLX: 'Malatya|TR|38.35|38.31|2',
	  NNY: 'Nanyang|CN|32.99|112.53|2',
	  PFB: 'Passo Fundo|BR|-28.26|-52.41|2',
	  TLH: 'Tallahassee|US|30.44|-84.28|2',
	  YCD: 'Nanaimo|CA|49.17|-123.94|2',
	  FLR: 'Florence|IT|43.77|11.25|2',
	  LLI: 'Lalibela|ET|12.02|39.07|',
	  GEG: 'Spokane|US|47.66|-117.43|2',
	  LTI: 'Altai|MN|46.65|96.42|',
	  THL: 'Tachilek|MM|20.45|99.95|',
	  IJK: 'Izhevsk|RU|56.85|53.23|2',
	  YVZ: 'Deer Lake|CA|52.67|-94.50|2',
	  HRI: 'Hambantota|LK|6.28|81.12|0',
	  PLV: 'Poltava|UA|49.58|34.57|1',
	  HMV: 'Hemavan|SE|65.82|15.09|2',
	  FAR: 'Fargo|US|46.88|-96.79|2',
	  UTH: 'Udon Thani|TH|17.41|102.79|2',
	  KAT: 'Kaitaia|NZ|-35.10|173.24|2',
	  BFJ: 'Ba|FJ|-17.55|177.68|1',
	  KSZ: 'Kotlas|RU|61.23|46.70|1',
	  LAQ: 'Beida|LY|32.82|21.75|2',
	  NTN: 'Normanton|AU|-17.67|141.08|2',
	  NCA: 'North Caicos|TC|21.93|-71.98|2',
	  MTR: 'Monteria|CO|8.76|-75.89|2',
	  QBC: 'Bella Coola|CA|52.33|-126.67|2',
	  ARI: 'Arica|CL|-18.48|-70.33|2',
	  PHE: 'Port Hedland|AU|-20.32|118.57|2',
	  PLQ: 'Palanga|LT|55.92|21.07|2',
	  CCM: 'Criciuma|BR|-28.68|-49.37|2',
	  AKV: 'Akulivik|CA|60.80|-78.20|2',
	  GGM: '|KE|',
	  BTV: 'Burlington|US|44.47|-73.15|2',
	  YKM: 'Yakima|US|46.60|-120.51|2',
	  DKA: '|NG|',
	  ODY: 'Oudomxay|LA|20.58|104.17|',
	  SOU: 'Southampton|GB|50.90|-1.40|2',
	  BEL: 'Belem|BR|-1.46|-48.50|2',
	  PKY: 'Palangkaraya|ID|-2.20|113.83|2',
	  AUC: 'Arauca|CO|7.09|-70.76|2',
	  ACZ: 'Zabol|IR|31.09|61.54|',
	  MLW: 'Monrovia|LR|6.31|-10.80|2',
	  PEM: 'Puerto Maldonado|PE|-12.60|-69.18|2',
	  ILR: 'Ilorin|NG|8.50|4.55|2',
	  PHF: 'Newport News|US|36.98|-76.43|2',
	  KDI: 'Kendari|ID|-3.94|122.50|2',
	  SPN: 'Saipan|MP|15.19|145.75|2',
	  GYE: 'Guayaquil|EC|-2.17|-79.90|2',
	  NUL: 'Nulato|US|64.72|-158.10|2',
	  APN: 'Alpena|US|45.06|-83.43|2',
	  BIK: 'Biak|ID|-0.93|122.88|2',
	  ELQ: 'Gassim|SA|26.30|43.77|2',
	  ESL: 'Elista|RU|46.31|44.26|2',
	  CIP: 'Chipata|ZM|-13.63|32.65|2',
	  MAO: 'Manaus|BR|-3.10|-60.02|2',
	  MRX: 'Bandar Mahshahr|IR|30.54|49.17|',
	  SDJ: 'Sendai|JP|38.25|140.88|2',
	  NZE: 'Nzerekore|GN|7.81|-8.70|',
	  PKB: 'Parkersburg|US|39.27|-81.56|2',
	  FIE: 'Fair Isle|GB|59.53|-1.65|2',
	  TYL: 'Talara|PE|-4.58|-81.27|2',
	  MBI: 'Mbeya|TZ|-8.90|33.45|2',
	  BHV: 'Bahawalpur|PK|29.40|71.68|2',
	  YIW: 'Yiwu|CN|29.31|120.07|2',
	  BCI: 'Barcaldine|AU|-23.57|145.28|2',
	  CZS: 'Cruzeiro Do Sul|BR|-7.58|-72.78|2',
	  YPA: 'Prince Albert|CA|53.20|-105.77|2',
	  PGD: 'Punta Gorda|US|26.93|-82.05|2',
	  KDU: 'Skardu|PK|35.30|75.62|2',
	  KOE: 'Kupang|ID|-10.17|123.58|2',
	  EXI: 'Excursion Inlet|US|58.42|-135.43|2',
	  IKI: 'Iki Jp|JP|33.75|129.79|2',
	  TUS: 'Tucson|US|32.22|-110.93|2',
	  VER: 'Veracruz|MX|19.20|-96.13|2',
	  PSO: 'Pasto|CO|1.21|-77.28|2',
	  HOT: 'Hot Springs|US|34.48|-93.10|2',
	  AAA: 'Anaa|PF|-17.05|-145.42|2',
	  CFE: 'Clermont  Ferrand|FR|45.78|3.08|2',
	  GME: 'Gomel|BY|52.44|30.98|2',
	  BSA: 'Bossaso|SO|11.28|49.15|',
	  OLA: 'Orland|NO|63.70|9.62|2',
	  HVB: 'Hervey Bay|AU|-25.28|152.83|2',
	  YCY: 'Clyde River|CA|70.47|-68.59|2',
	  SEU: 'Seronera|TZ|-2.38|34.82|',
	  JPR: 'Ji Parana|BR|-10.89|-61.95|2',
	  LPS: 'Lopez Island|US|48.48|-122.89|2',
	  SHH: 'Shismaref|US|66.26|-166.06|2',
	  KEH: 'Kenmore|US|47.76|-122.26|',
	  EAR: 'Kearney|US|40.70|-99.08|2',
	  LHA: 'Lahr|DE|48.35|7.87|1',
	  WSN: 'South Naknek|US|58.72|-157.00|2',
	  JIK: 'Ikaria Island|GR|37.67|26.33|2',
	  KAO: 'Kuusamo|FI|65.97|29.18|2',
	  SHX: 'Shageluk|US|62.68|-159.56|2',
	  ICT: 'Wichita|US|37.69|-97.34|2',
	  RGS: 'Burgos|ES|',
	  FNA: 'Freetown|SL|8.49|-13.23|2',
	  MFK: 'Matsu|TW|26.17|119.92|2',
	  ERM: 'Erechim|BR|-27.64|-52.28|',
	  MDQ: 'Mar Del Plata|AR|-38.00|-57.55|2',
	  TNN: 'Tainan|TW|22.95|120.20|2',
	  ROP: 'Rota|MP|14.17|145.25|2',
	  MEG: 'Malange|AO|-9.54|16.35|2',
	  BHU: 'Bhavnagar|IN|21.77|72.15|2',
	  YCB: 'Cambridge Bay|CA|69.05|-105.12|2',
	  TWT: 'Tawitawi|PH|5.04|119.74|2',
	  BJR: 'Bahar Dar|ET|11.60|37.38|2',
	  KGE: 'Kagau|SB|-8.16|157.59|',
	  IOA: 'Ioannina|GR|39.67|20.85|2',
	  KAE: 'Kake|US|56.98|-133.95|2',
	  RCH: 'Riohacha|CO|11.54|-72.91|2',
	  GJL: 'Jijel|DZ|36.82|5.77|2',
	  SXB: 'Strasbourg|FR|48.58|7.75|2',
	  ERD: 'Berdyansk|UA|46.73|36.78|1',
	  WUS: 'Wuyishan|CN|27.76|118.04|2',
	  GIB: 'Gibraltar|GI|36.13|-5.35|2',
	  GZO: 'Gizo|SB|-8.12|156.83|2',
	  TKK: 'Truk|FM|7.42|151.78|2',
	  MYE: 'Miyakejima|JP|34.07|139.56|2',
	  TGO: 'Tongliao|CN|43.56|122.20|',
	  YMM: 'Ft McMurray|CA|56.68|-111.27|2',
	  JMO: 'Jomsom|NP|28.78|83.73|',
	  EWD: 'Wildman Lake|US|56.46|-159.76|',
	  CTG: 'Cartagena|CO|10.40|-75.51|2',
	  ABT: 'Al Baha|SA|20.29|41.63|2',
	  BYN: 'Bayankhongor|MN|46.10|100.68|',
	  CAE: 'Columbia|US|33.94|-81.12|2',
	  JRO: 'Kilimanjaro|TZ|-3.07|37.37|2',
	  BTI: 'Barter Island|US|70.12|-143.67|2',
	  KWM: 'Kowanyama|AU|-14.92|141.60|2',
	  ANF: 'Antofagasta|CL|-23.65|-70.40|2',
	  HEA: 'Herat|AF|34.21|62.23|',
	  CZM: 'Cozumel|MX|20.51|-86.95|2',
	  CJC: 'Calama|CL|-22.47|-68.93|2',
	  SIF: 'Simara|NP|27.16|84.98|',
	  GOU: 'Garoua|CM|9.30|13.40|2',
	  PAD: 'Paderborn|DE|51.72|8.75|2',
	  VBY: 'Visby|SE|57.63|18.30|2',
	  BJZ: 'Badajoz|ES|38.88|-6.97|2',
	  SGD: 'Sonderborg|DK|54.92|9.78|2',
	  TRI: 'Bristol|US|36.55|-82.47|2',
	  ATA: 'Anta|PE|-9.35|-77.60|',
	  SRY: 'Sary|IR|36.64|53.20|',
	  GHB: 'Governor S Harbour|BS|25.28|-76.33|2',
	  KPB: 'Point Baker|US|56.33|-133.58|',
	  CWL: 'Cardiff|GB|51.50|-3.20|2',
	  JAL: 'Jalapa|MX|19.53|-96.92|2',
	  UAS: 'Samburu|KE|-3.77|39.28|2',
	  BFQ: 'Bahia Pinas|PA|7.58|-78.17|',
	  CCZ: 'Chub Cay|BS|25.42|-77.88|',
	  CEJ: 'Chernigov|UA|51.50|31.30|1',
	  BMY: 'Belep Island|NC|-19.75|163.67|2',
	  LST: 'Launceston|AU|-41.45|147.17|2',
	  BUX: 'Bunia|CD|1.57|30.25|2',
	  KRN: 'Kiruna|SE|67.85|20.22|2',
	  RIY: 'Riyan|YE|14.65|49.33|2',
	  TAM: 'Tampico|MX|22.22|-97.85|2',
	  SLW: 'Saltillo|MX|25.42|-101.00|2',
	  'ВЛМ': 'Vladimir|Russia|0',
	  DLZ: 'Dalanzadgad|MN|43.97|104.68|',
	  YSO: 'Postville|CA|54.92|-59.97|2',
	  OGX: 'Ouargla|DZ|31.95|5.33|2',
	  ADZ: 'San Andres|CO|12.58|-81.70|2',
	  AUY: 'Aneityum|VU|-20.33|169.67|',
	  PGX: 'Perigueux|FR|45.18|0.72|2',
	  MQL: 'Mildura|AU|-34.20|142.15|2',
	  PNL: 'Pantelleria|IT|36.83|11.95|2',
	  HIJ: 'Hiroshima|JP|34.40|132.45|2',
	  GOV: 'Gove|AU|-12.23|136.77|2',
	  SBA: 'Santa Barbara|US|34.42|-119.70|2',
	  ART: 'Watertown|US|43.99|-76.02|2',
	  DOD: 'Dodoma|TZ|-6.18|35.75|2',
	  KRF: 'Kramfors|SE|62.93|17.79|2',
	  KRY: 'Karamay|CN|45.58|84.89|2',
	  OBX: 'Obo|PG|-7.58|141.32|',
	  PMV: 'Porlamar|VE|10.95|-63.85|2',
	  PLZ: 'Port Elizabeth|ZA|-33.97|25.58|2',
	  YNO: 'North Spirit Lake, ON|CA|52.50|-92.42|',
	  LET: 'Leticia|CO|-4.22|-69.94|2',
	  MLB: 'Melbourne|US|28.10|-80.63|2',
	  AMQ: 'Ambon|ID|-3.72|128.20|2',
	  RST: 'Rochester|US|43.91|-92.50|2',
	  AXK: 'Ataq|YE|13.87|46.30|2',
	  MLO: 'Milos|GR|36.75|24.43|2',
	  DEC: 'Decatur|US|39.83|-88.87|2',
	  IGU: 'Iguassu Falls|BR|-25.60|-54.48|2',
	  YBX: 'Blanc Sablon|CA|51.43|-57.22|2',
	  AGS: 'Augusta|US|33.37|-81.97|2',
	  KNH: 'Kinmen|TW|24.44|118.32|2',
	  SFA: 'Sfax|TN|34.74|10.76|2',
	  BJT: 'Bentota River|LK|6.48|79.98|2',
	  CZU: 'Corozal|CO|9.34|-75.28|',
	  ODB: 'Cordoba|ES|37.88|-4.77|1',
	  TAP: 'Tapachula|MX|14.90|-92.28|2',
	  IXZ: 'Port Blair|IN|11.67|92.75|2',
	  TOH: 'Torres|VU|-13.17|166.75|',
	  BNI: 'Benin City|NG|6.33|5.63|2',
	  ELP: 'El Paso|US|31.76|-106.49|2',
	  CEE: 'Cherepovets|RU|59.28|38.07|2',
	  LDS: '|CN|',
	  WAT: 'Waterford|IE|52.20|-7.09|2',
	  TGZ: 'Tuxtla Gutierrez|MX|16.75|-93.12|2',
	  BGC: 'Braganca|PT|41.82|-6.75|2',
	  SZA: 'Soyo|AO|-6.03|12.42|2',
	  MOF: 'Maumere|ID|-8.62|122.23|2',
	  BEB: 'Benbecula|GB|57.43|-7.35|2',
	  NYR: 'Nyurba|RU|63.28|118.35|0',
	  CPC: 'Chapelco|AR|-40.12|-71.22|2',
	  GTF: 'Great Falls|US|47.50|-111.30|2',
	  MTV: 'Mota Lava|VU|-13.67|167.67|2',
	  EVV: 'Evansville|US|37.97|-87.56|2',
	  GST: 'Glacier Bay|US|58.43|-135.71|2',
	  MKZ: 'Malacca|MY|2.20|102.25|2',
	  HIR: 'Honiara|SB|-9.43|159.95|2',
	  KNX: 'Kununurra|AU|-15.77|128.73|2',
	  TAO: 'Qingdao|CN|36.10|120.37|2',
	  BKM: 'Bakalalan|MY|3.97|115.62|2',
	  HDN: 'Hayden|US|47.77|-116.79|2',
	  MTJ: 'Montrose|US|38.48|-107.88|2',
	  BPX: 'Bangda|CN|30.56|97.11|',
	  SPS: 'Wichita Falls|US|33.91|-98.49|2',
	  CQM: 'Ciudad Real|ES|38.98|-3.92|',
	  ULO: 'Ulaangom|MN|49.98|92.07|',
	  ORI: 'Port Lions|US|57.87|-152.88|2',
	  KHD: 'Khorramabad|IR|33.49|48.36|2',
	  NRA: 'Narrandera|AU|-34.75|146.55|2',
	  PNI: 'Pohnpei|FM|6.91|158.23|2',
	  TSJ: 'Tsushima|JP|35.17|136.72|2',
	  UTT: 'Umtata|ZA|-31.58|28.78|2',
	  CID: 'Cedar Rapids|US|42.01|-91.64|2',
	  PEN: 'Penang|MY|5.42|100.33|2',
	  LLA: 'Lulea|SE|65.58|22.15|2',
	  VDH: 'Dong Hoi|VN|17.52|106.59|',
	  KSD: 'Karlstad|SE|59.38|13.50|2',
	  WGA: 'Wagga Wagga|AU|-35.12|147.37|2',
	  EAU: 'Eau Claire|US|44.81|-91.50|2',
	  ATW: 'Appleton|US|44.26|-88.42|2',
	  SOM: 'San Tome|VE|8.95|-64.15|2',
	  ISE: 'Isparta|TR|37.76|30.55|2',
	  YTL: 'Big Trout|CA|53.82|-89.89|',
	  SYM: 'Simao|CN|22.83|101.00|2',
	  UAP: 'Ua Pou|PF|-9.34|-140.08|',
	  BYM: 'Bayamo|CU|20.38|-76.64|2',
	  YRA: 'Rae Lakes|CA|64.12|-117.35|',
	  NNB: 'Santa Ana|SB|-10.83|162.50|2',
	  NTQ: 'Wajima|JP|37.29|136.96|',
	  RMA: 'Roma|AU|-26.58|148.78|2',
	  DAR: 'Dar Es Salaam|TZ|-6.80|39.28|2',
	  HLT: 'Hamilton|AU|-37.75|142.03|2',
	  PSV: 'Papa Stour|GB|60.32|-1.70|',
	  SEY: 'Selibaby|MR|15.18|-12.21|2',
	  SAB: 'Saba Island|BQ|17.65|-63.22|2',
	  WEI: 'Weipa|AU|-12.67|141.87|2',
	  BUG: 'Benguela|AO|-12.58|13.41|2',
	  IAO: 'Del Carmin|PH|9.86|126.01|',
	  TNJ: 'Tanjung Pinang|ID|0.92|104.54|2',
	  AQI: 'Qaisumah|SA|28.31|46.13|2',
	  TVU: 'Taveuni|FJ|-16.69|179.87|2',
	  TBI: 'The Bight|BS|24.32|-75.45|',
	  PVK: 'Preveza|GR|38.95|20.75|2',
	  RHP: 'Ramechhap|NP|27.33|86.08|',
	  LFT: 'Lafayette|US|30.20|-91.99|2',
	  PHO: 'Point Hope|US|68.35|-166.81|2',
	  LLW: 'Lilongwe|MW|-13.98|33.78|2',
	  YXX: 'Abbotsford|CA|49.06|-122.25|2',
	  MEQ: 'Meulaboh|ID|4.15|96.12|',
	  MBL: 'Manistee|US|44.24|-86.32|2',
	  KEB: 'Nanwalek|US|59.35|-151.92|',
	  OKA: 'Okinawa|JP|26.34|127.80|2',
	  ZAM: 'Zamboanga|PH|6.91|122.07|2',
	  SXR: 'Srinagar|IN|34.08|74.82|2',
	  HIA: 'Thomasville|CN|35.95|-80.18|',
	  KID: 'Kristianstad|SE|56.03|14.13|2',
	  MPI: 'Mamitupo|PA|9.58|-79.84|',
	  MUB: 'Maun|BW|-19.98|23.42|2',
	  SCK: 'Stockton|US|37.96|-121.29|1',
	  HGN: 'Mae Hongson|TH|19.30|97.98|2',
	  HGR: 'Hagerstown|US|39.64|-77.72|2',
	  YDP: 'Nain|CA|56.53|-61.67|2',
	  PUM: 'Pomala|ID|-4.18|121.62|2',
	  STT: 'St Thomas|VI|18.34|-64.97|2',
	  UDR: 'Udaipur|IN|24.58|73.68|2',
	  EMK: 'Emmonak|US|62.78|-164.52|2',
	  SRP: 'Stord|NO|59.88|5.42|2',
	  TNF: 'Toussus Le Noble|FR|48.75|2.11|1',
	  APF: 'Naples|US|26.15|-81.78|2',
	  ACK: 'Nantucket|US|41.28|-70.10|2',
	  GLK: 'Galcaio|SO|6.77|47.43|',
	  TPP: 'Tarapoto|PE|-2.18|-74.10|2',
	  MUZ: 'Musoma|TZ|-1.50|33.80|2',
	  NRK: 'Norrkoping|SE|58.60|16.18|2',
	  YWG: 'Winnipeg|CA|49.88|-97.15|2',
	  MAR: 'Maracaibo|VE|10.63|-71.64|2',
	  TFI: 'Tufi|PG|-9.08|149.32|2',
	  ADD: 'Addis Ababa|ET|9.03|38.70|2',
	  KAD: 'Kaduna|NG|10.52|7.44|2',
	  HLN: 'Helena|US|46.59|-112.04|2',
	  HPN: 'White Plains|US|41.03|-73.76|2',
	  TUE: 'Tupile|PA|9.57|-79.49|',
	  PIU: 'Piura|PE|-5.20|-80.63|2',
	  KIT: 'Kithira|GR|36.25|23.02|2',
	  PMA: 'Pemba|TZ|-5.24|39.80|2',
	  RBH: 'Brooks Lodge|US|58.55|-155.80|',
	  BON: 'Bonaire|BQ|12.20|-68.25|2',
	  KGI: 'Kalgoorlie|AU|-30.75|121.47|2',
	  HYG: 'Hydaburg|US|55.21|-132.83|2',
	  KWN: 'Quinhagak|US|59.75|-161.92|2',
	  GAF: 'Gafsa|TN|34.42|8.82|',
	  IQT: 'Iquitos|PE|-3.75|-73.25|2',
	  FKI: 'Kisangani|CD|0.52|25.20|2',
	  SJZ: 'Sao Jorge Island|PT|38.65|-28.23|2',
	  EAP: 'Mulhouse Basel|FR|2',
	  YMP: 'McNeil|CA|50.61|-127.10|',
	  YBK: 'Baker Lake|CA|64.32|-96.02|2',
	  UYL: 'Nyala|SD|12.07|24.89|',
	  OLP: 'Olympic Dam|AU|-31.00|136.00|2',
	  UDI: 'Uberlandia|BR|-18.92|-48.28|2',
	  KYK: 'Karluk|US|57.57|-154.46|2',
	  AHE: 'Ahe|PF|-14.43|-146.26|',
	  KDM: 'Kaadedhdhoo|MV|0.49|73.00|',
	  YHG: 'Charlottetown|CA|52.77|-56.10|2',
	  LZN: 'Nangan|TW|26.16|119.96|',
	  XNN: 'Xining|CN|36.62|101.77|2',
	  YYF: 'Penticton|CA|49.50|-119.59|2',
	  TKX: 'Takaroa|PF|-14.46|-145.03|',
	  GND: 'Grenada|GD|12.15|-61.62|2',
	  WLH: 'Walaha|VU|-15.42|167.70|',
	  YDS: 'Desolation Sound|CA|49.97|-124.73|',
	  BCA: 'Baracoa|CU|20.35|-74.50|2',
	  IXA: 'Agartala|IN|23.84|91.28|2',
	  GOM: 'Goma|CD|-1.68|29.22|2',
	  SZF: 'Samsun|TR|41.26|36.56|2',
	  ZYL: 'Sylhet|BD|24.90|91.87|2',
	  ABA: 'Abakan|RU|53.71|91.42|2',
	  MCZ: 'Maceio|BR|-9.67|-35.74|2',
	  LAW: 'Lawton|US|34.61|-98.39|2',
	  YQX: 'Gander|CA|48.97|-54.59|2',
	  WTK: 'Noatak|US|67.57|-162.97|2',
	  OTZ: 'Kotzebue|US|66.90|-162.60|2',
	  MZL: 'Manizales|CO|5.07|-75.52|2',
	  PQQ: 'Pt Macquarie|AU|-31.43|152.87|2',
	  VIG: 'El Vigia|VE|8.62|-71.65|2',
	  COL: 'Coll Island|GB|56.62|-6.62|',
	  HPH: 'Haiphong|VN|20.86|106.68|2',
	  OZH: 'Zaporozhe|UA|47.82|35.18|2',
	  BDS: 'Brindisi|IT|40.63|17.94|2',
	  POA: 'Porto Alegre|BR|-30.03|-51.23|2',
	  POP: 'Puerto Plata|DO|19.80|-70.68|2',
	  TNL: 'Ternopol|UA|49.55|25.58|1',
	  DIK: 'Dickinson|US|29.46|-95.05|2',
	  BEJ: 'Berau|ID|2.17|117.70|2',
	  AZR: 'Adrar|DZ|36.82|4.30|2',
	  CYZ: 'Cauayan|PH|16.93|121.76|',
	  WBQ: 'Beaver|US|40.70|-80.30|2',
	  SLE: 'Salem|US|44.94|-123.03|2',
	  TTQ: 'Tortuquero|CR|10.57|-83.52|2',
	  HRE: 'Harare|ZW|-17.86|31.03|2',
	  YBG: 'Bagotville|CA|48.32|-70.99|2',
	  HDS: 'Hoedspruit|ZA|-24.35|30.97|2',
	  PKE: 'Parkes|AU|-33.13|148.18|2',
	  ZGS: 'Gethsemanie|CA|50.33|-60.67|2',
	  LGI: 'Deadmans Cay|BS|23.23|-75.23|2',
	  AKR: 'Akure|NG|7.25|5.20|2',
	  AJU: 'Aracaju|BR|-10.91|-37.07|2',
	  SVP: 'Kuito|AO|-12.40|16.96|',
	  JER: 'Jersey|GB|49.21|-2.22|2',
	  IIL: 'Ilaam|IR|33.59|46.40|',
	  MAZ: 'Mayaguez|PR|18.26|-67.15|2',
	  VDE: 'Valverde|ES|27.81|-17.89|2',
	  HUW: 'Kirmington|BR|53.58|-0.35|2',
	  DVO: 'Davao|PH|7.07|125.61|2',
	  TRO: 'Taree|AU|-31.90|152.47|2',
	  UVE: 'Ouvea|NC|-20.64|166.57|',
	  YFH: 'Fort Hope|CA|66.53|-86.70|2',
	  KNW: 'New Stuyahok|US|59.45|-157.31|2',
	  AAY: 'Al Ghaydah|YE|16.22|52.18|2',
	  HON: 'Huron|US|44.36|-98.21|2',
	  GVR: 'Governador Valadares|BR|-18.85|-41.95|2',
	  BPS: 'Porto Seguro|BR|-16.45|-39.06|2',
	  YGP: 'Gaspe|CA|48.83|-64.48|2',
	  AYP: 'Ayacucho|PE|-13.16|-74.22|2',
	  JCB: 'Joacaba|BR|-27.17|-51.52|2',
	  GIZ: 'Gizan|SA|16.87|42.58|2',
	  CUU: 'Chihuahua|MX|28.63|-106.08|2',
	  AKU: 'Aksu|CN|41.12|80.26|2',
	  UPG: 'Ujung Pandang|ID|-5.07|119.55|2',
	  IXE: 'Mangalore|IN|12.87|74.88|2',
	  YQI: 'Yarmouth|CA|43.83|-66.12|2',
	  ZTU: 'Zaqatala|AZ|41.56|46.66|2',
	  AIA: 'Alliance|US|40.92|-81.11|2',
	  IGA: 'Inagua|BS|21.08|-73.30|2',
	  YYU: 'Kapuskasing|CA|49.42|-82.43|2',
	  MHG: 'Mannheim Germany|DE|49.50|8.47|2',
	  VCT: 'Victoria|US|28.85|-96.91|2',
	  GDN: 'Gdansk|PL|54.35|18.67|2',
	  BDP: 'Bhadrapur|NP|26.53|88.08|',
	  HSG: 'Saga|JP|33.25|130.30|2',
	  FNT: 'Flint|US|43.01|-83.69|2',
	  KOZ: 'Ouzinkie|US|57.92|-152.50|2',
	  COO: 'Cotonou|BJ|6.35|2.43|2',
	  CIT: 'Shimkent|KZ|42.30|69.60|1',
	  DIJ: 'Dijon|FR|47.32|5.02|2',
	  OIM: 'Oshima|JP|37.12|138.50|2',
	  YYG: 'Charlottetown|CA|46.29|-63.13|2',
	  WMK: 'Meyers Chuck|US|55.73|-132.18|',
	  PHW: 'Phalaborwa|ZA|-23.95|31.12|2',
	  UEL: 'Quelimane|MZ|-17.88|36.89|2',
	  SHL: 'Shillong|IN|25.57|91.88|2',
	  SML: 'Stella Maris|BS|23.60|-75.30|2',
	  RKT: 'Ras Al Khaimah|AE|25.79|55.94|2',
	  MPH: 'Caticlan|PH|11.93|121.96|2',
	  VRK: 'Varkaus|FI|62.32|27.92|1',
	  KKB: 'Kitoi Bay|US|58.19|-152.35|2',
	  CRD: 'Comodoro Rivadavia|AR|-45.87|-67.50|2',
	  BRR: 'Barra|GB|57.02|-7.44|2',
	  SOY: 'Stronsay|GB|59.13|-2.63|2',
	  CYO: 'Cayo Largo Del Sur|CU|21.62|-81.52|2',
	  WXN: 'Wanxian|CN|30.80|108.39|2',
	  YMT: 'Chibougamau|CA|49.92|-74.37|2',
	  RSU: 'Yeosu|KR|37.02|126.96|2',
	  BRD: 'Brainerd|US|46.36|-94.20|2',
	  APL: 'Nampula|MZ|-15.12|39.26|2',
	  DNH: 'Dunhuang|CN|40.17|94.68|2',
	  HAA: 'Hasvik|NO|70.48|22.15|2',
	  TNK: 'Tununak|US|60.59|-165.26|2',
	  DBO: 'Dubbo|AU|-32.25|148.62|2',
	  MFR: 'Medford|US|42.37|-122.87|2',
	  PKN: 'Pangkalanbun|ID|-2.68|111.62|2',
	  TVY: 'Tavoy|MM|14.08|98.20|2',
	  PMY: 'Puerto Madryn|AR|-42.77|-65.05|2',
	  YQQ: 'Comox|CA|49.68|-124.94|2',
	  KCK: 'Kansas City|US|39.16|-94.65|1',
	  AUU: 'Aurukun Mission|AU|-13.17|142.25|2',
	  JKR: 'Janakpur|NP|26.71|85.92|',
	  GTE: 'Grte Eylandt|AU|-13.97|136.45|2',
	  SVJ: 'Svolvaer|NO|68.23|14.57|2',
	  AOE: 'Anadolu University|TR|39.81|30.52|',
	  HVD: 'Khovd|MN|48.02|91.65|',
	  UYN: 'Yulin|CN|38.27|109.73|',
	  GGT: 'George Town|BS|23.47|-75.78|2',
	  URC: 'Urumqi|CN|43.80|87.58|1',
	  ULG: 'Ulgit|MN|48.97|89.97|2',
	  CEC: 'Crescent City|US|41.76|-124.20|2',
	  OBN: 'Oban|GB|56.42|-5.47|2',
	  FAI: 'Fairbanks|US|64.84|-147.72|2',
	  DUJ: 'Dubois|US|43.53|-109.63|2',
	  KUM: 'Yakushima|JP|30.33|130.50|1',
	  COD: 'Cody|US|44.53|-109.06|2',
	  YIK: 'Ivujivik|CA|62.42|-77.92|2',
	  VAK: 'Chevak|US|61.53|-165.59|2',
	  PAP: 'Port Au Prince|HT|18.54|-72.33|2',
	  JOG: 'Yogjakarta|ID|-7.79|110.43|2',
	  KPV: 'Perryville|US|37.72|-89.86|2',
	  KSU: 'Kristiansund|NO|63.11|7.84|2',
	  JCK: 'Julia Creek|AU|-20.65|141.73|2',
	  LIG: 'Limoges|FR|45.85|1.25|2',
	  RAJ: 'Rajkot|IN|22.30|70.78|2',
	  PHC: 'Port Harcourt|NG|4.79|7.00|2',
	  HTY: '|TR|',
	  LIF: 'Lifou|NC|-20.88|167.22|2',
	  RUD: '|IR|',
	  RMP: 'Rampart|US|65.50|-150.17|2',
	  HVR: 'Havre|US|48.54|-109.76|2',
	  AVP: 'Scranton|US|41.41|-75.66|2',
	  UKX: 'Ust-Kut|RU|56.85|105.73|2',
	  SBZ: 'Sibiu|RO|45.80|24.15|2',
	  KKU: 'Ekuk|US|58.81|-158.56|2',
	  XUZ: 'Xuzhou|CN|34.27|117.19|2',
	  KMJ: 'Kumamoto|JP|32.80|130.72|2',
	  YMH: 'Marys Harbour|CA|52.30|-55.83|',
	  TUL: 'Tulsa|US|36.15|-95.99|2',
	  KZD: 'Krakor|KH|12.53|104.20|',
	  BRS: 'Bristol|GB|51.45|-2.58|2',
	  KMA: 'Kerema|PG|-7.97|145.77|2',
	  TBU: 'Nuku Alofa|TO|-21.13|-175.20|2',
	  INV: 'Inverness|GB|57.47|-4.23|2',
	  WHK: 'Whakatane|NZ|-37.98|177.00|2',
	  OLF: 'Wolf Point|US|48.09|-105.64|2',
	  YGH: 'Fort Good Hope|CA|66.27|-128.65|',
	  UAH: 'Ua Huka|PF|-8.93|-139.55|',
	  YNC: 'Wemindji|CA|53.00|-78.82|',
	  GXF: 'Seiyun|YE|15.96|48.79|',
	  DHB: '|US|',
	  LTD: 'Ghadames|LY|30.13|9.50|2',
	  IRK: 'Kirksville|US|40.19|-92.58|2',
	  WSZ: 'Westport|NZ|-41.74|171.58|2',
	  SNW: 'Sandoway|MM|18.47|94.37|2',
	  GOQ: 'Golmud|CN|36.40|94.79|',
	  TWU: 'Tawau|MY|4.25|117.90|2',
	  ZWL: 'Wollaston Lake|CA|58.10|-103.18|2',
	  EBA: 'Elba Island|IT|42.77|10.28|2',
	  TKE: 'Tenakee|US|57.78|-135.22|2',
	  TTB: 'Tortoli|IT|39.93|9.66|1',
	  FOG: 'Foggia|IT|41.46|15.55|2',
	  YUS: '|CN|',
	  EZV: '|RU|',
	  WNR: 'Windorah|AU|-32.37|148.93|2',
	  NIC: 'Nicosia|CY|35.17|33.37|1',
	  OUK: 'Outer Skerries|GB|60.42|-0.75|',
	  YSK: 'Sanikiluaq|CA|56.53|-79.23|2',
	  SKD: 'Samarkand|UZ|39.65|66.96|2',
	  TBG: 'Tububil|PG|-5.38|141.32|2',
	  YHA: 'Port Hope Simpson|CA|52.55|-56.30|2',
	  XTG: 'Thargomindah|AU|-28.00|143.82|2',
	  BXR: 'Bam|IR|29.08|58.45|',
	  MJU: 'Mamuju|ID|-2.68|118.89|2',
	  MKW: 'Manokwari|ID|-0.87|134.08|2',
	  CDR: 'Chadron|US|42.83|-103.00|2',
	  MGT: 'Milingimbi|AU|-12.09|134.89|',
	  BVC: 'Boa Vista|CV|16.08|-22.83|2',
	  SGS: 'Sanga Sanga|PH|5.23|121.15|2',
	  CIF: 'Chifeng|CN|42.27|118.96|2',
	  YQD: 'The Pas|CA|53.83|-101.25|2',
	  MVP: 'Mitu|CO|1.13|-70.05|',
	  BPT: 'Beaumont|US|30.09|-94.10|2',
	  KMV: 'Kalemyo|MM|23.18|94.07|2',
	  PSS: 'Posadas|AR|-27.38|-55.88|2',
	  MOI: 'Mitiaro Island|CK|-19.50|-158.00|2',
	  RYK: 'Rahim Yar Khan|PK|28.39|70.29|',
	  SVU: 'Savusavu|FJ|-16.78|179.33|2',
	  HMJ: 'Khmelnitskiy|UA|49.42|27.00|1',
	  CAH: 'Ca Mau|VN|9.18|105.18|',
	  LBJ: 'Labuan Bajo|ID|-8.48|119.87|2',
	  SJJ: 'Sarajevo|BA|43.85|18.38|2',
	  PLX: 'Semipalatinsk|KZ|50.33|80.25|2',
	  DLY: 'Dillons Bay|VU|-18.70|169.15|2',
	  MDG: 'Mudanjiang|CN|44.58|129.60|2',
	  PLJ: 'Placencia|BZ|16.52|-88.37|2',
	  SCQ: 'Santiago De Compostela|ES|42.88|-8.55|2',
	  GDT: 'Grand Turk|TC|21.47|-71.13|2',
	  GRV: 'Groznyj|RU|43.33|45.75|2',
	  DGA: 'Dangriga|BZ|16.97|-88.22|2',
	  GRJ: 'George|ZA|-33.97|22.45|2',
	  EWN: 'New Bern|US|35.11|-77.04|2',
	  TOL: 'Toledo|US|41.66|-83.56|2',
	  GAU: 'Guwahati|IN|26.19|91.75|2',
	  TTE: 'Ternate|ID|0.80|127.40|2',
	  GCC: 'Gillette|US|44.29|-105.50|2',
	  NIN: 'Ninilchik|US|60.07|-151.73|2',
	  LZC: 'Lazaro Cardenas Michoacan|MX|18.00|-102.22|2',
	  FDF: 'Ft De France|MQ|14.59|-61.01|2',
	  BQL: 'Boulia|AU|-22.90|139.90|2',
	  AXP: 'Spring Point|BS|22.45|-74.00|2',
	  AMH: 'Arba Mintch|ET|6.03|37.55|2',
	  SAX: 'Sambu|PA|8.03|-78.08|',
	  DOL: 'Deauville|FR|49.36|0.07|2',
	  MSL: 'Muscle Shoals|US|34.74|-87.67|2',
	  IWJ: 'Iwami|JP|34.78|134.53|2',
	  NAN: 'Nadi|FJ|-17.80|177.42|2',
	  YTG: 'Sullivan Bay|CA|50.88|-126.80|',
	  CEI: 'Chiang Rai|TH|19.90|99.83|2',
	  DLH: 'Duluth|US|46.78|-92.11|2',
	  KRP: 'Karup|DK|56.31|9.17|2',
	  KDV: 'Kandavu|FJ|-19.03|178.38|2',
	  YPO: 'Peawanuck|CA|54.98|-85.43|',
	  BHH: 'Bisha|SA|20.00|42.60|2',
	  SRQ: 'Sarasota|US|27.34|-82.53|2',
	  PLH: 'Plymouth|GB|50.37|-4.14|2',
	  YGJ: 'Yonago|JP|35.43|133.33|2',
	  YWP: 'Webequie|CA|52.96|-87.37|',
	  LCH: 'Lake Charles|US|30.21|-93.20|2',
	  TYD: 'Tynda|RU|55.28|124.73|1',
	  BKG: 'Branson|US|36.53|-93.20|',
	  TLY: '|RU|',
	  TVS: '|CN|',
	  ILI: 'Iliamna|US|59.75|-154.91|2',
	  PKU: 'Pekanbaru|ID|0.53|101.45|2',
	  SCC: 'Prudhoe Bay Deadhorse|US|70.20|-148.47|2',
	  TYF: 'Torsby|SE|60.15|13.00|',
	  DZA: 'Dzaoudzi|YT|-12.78|45.25|2',
	  NIU: '|PF|',
	  DLA: 'Douala|CM|4.05|9.70|2',
	  ZRJ: 'Round Lake|CA|52.95|-91.32|',
	  FAY: 'Fayetteville|US|34.99|-78.88|2',
	  DZN: 'Zhezkazgan|KZ|47.70|67.73|2',
	  MOG: 'Mong Hsat|MM|20.53|99.27|',
	  ENI: 'El Nido|PH|11.21|119.41|',
	  EUN: 'El Aaiun|MA|27.13|-13.22|2',
	  BTT: 'Bettles|US|66.92|-151.52|2',
	  VTZ: 'Vishakhapatanam|IN|17.72|83.22|2',
	  JAC: 'Jackson|US|43.61|-110.74|2',
	  MSW: 'Massawa|ER|15.61|39.45|2',
	  SHM: 'Nanki Shirahama|JP|33.66|135.36|2',
	  NYU: 'Nyaung|MM|21.65|94.17|2',
	  YOG: 'Ogoki|CA|51.67|51.67|',
	  YPC: 'Paulatuk|CA|69.36|-124.07|',
	  MYJ: 'Matsuyama|JP|35.33|139.85|2',
	  CND: 'Constanta|RO|44.18|28.65|2',
	  ROO: 'Rondonopolis|BR|-16.47|-54.64|2',
	  CZH: 'Corozal|BZ|18.38|-88.38|2',
	  AIN: 'Wainwright|US|70.63|-160.03|2',
	  HLD: 'Hailar|CN|49.20|119.70|2',
	  GWY: 'Galway|IE|53.27|-9.05|2',
	  YBL: 'Campbell River|CA|50.02|-125.25|2',
	  SWA: 'Shantou|CN|23.36|116.68|2',
	  YVO: 'Val D Or|CA|48.05|-77.78|2',
	  ZFD: 'Fond Du Lac|CA|59.32|-107.17|2',
	  KWA: 'Kwajalein|MH|8.72|167.73|2',
	  NTG: 'Nantong|CN|32.03|120.88|2',
	  BHJ: 'Bhuj|IN|23.27|69.67|2',
	  SYO: 'Shonai|JP|38.81|139.79|2',
	  FUO: 'Fuoshan|CN|23.13|113.28|',
	  LIT: 'Little Rock|US|34.75|-92.29|2',
	  COQ: 'Choibalsan|MN|48.13|114.90|',
	  OBO: 'Obihiro|JP|42.92|143.20|2',
	  INI: 'Nis RS|RS|43.33|21.85|2',
	  UTM: 'Tunica|US|34.67|-90.37|',
	  CKY: 'Conakry|GN|9.51|-13.71|2',
	  ABM: 'Bamaga|AU|-10.89|142.39|2',
	  ELM: 'Elmira|US|42.09|-76.81|2',
	  ANS: 'Andahuaylas|PE|-13.66|-73.39|2',
	  PEC: 'Pelican|US|57.96|-136.23|2',
	  JCH: 'Qasigiannguit|GL|68.82|-51.18|2',
	  EDL: 'Eldoret|KE|0.52|35.28|2',
	  SBW: 'Sibu|MY|2.30|111.82|2',
	  HNS: 'Haines|US|59.24|-135.44|2',
	  KBV: 'Krabi|TH|8.07|98.92|2',
	  YXE: 'Saskatoon|CA|52.12|-106.64|2',
	  ARD: 'Alor Island|ID|-8.68|124.53|2',
	  CKB: 'Clarksburg|US|39.28|-80.34|2',
	  BSK: 'Biskra|DZ|34.85|5.73|2',
	  BEV: 'Beersheba|IL|31.23|34.78|1',
	  MFE: 'McAllen|US|26.20|-98.23|2',
	  UBP: 'Ubon Ratchath|TH|15.25|104.87|2',
	  GNS: 'Gunungsitoli|ID|1.28|97.62|2',
	  NWI: 'Norwich|GB|52.63|1.30|2',
	  GTS: 'Granites|AU|-26.97|133.62|',
	  SSA: 'Salvador|BR|-12.97|-38.51|2',
	  BAQ: 'Barranquilla|CO|10.96|-74.80|2',
	  LRS: 'Leros|GR|37.52|26.78|2',
	  TJA: 'Tarija|BO|-21.54|-64.73|2',
	  GLH: 'Greenville|US|33.48|-90.98|2',
	  SOC: 'Solo|ID|-7.56|110.83|2',
	  LOO: 'Laghouat|DZ|33.83|2.98|',
	  SKZ: 'Sukkur|PK|27.70|68.87|2',
	  RPR: 'Raipur|IN|21.23|81.63|2',
	  VLI: 'Port Vila|VU|-17.73|168.32|2',
	  MLG: 'Malang|ID|-7.98|112.63|2',
	  MMH: 'Mammoth Lakes|US|37.65|-118.97|2',
	  IXJ: 'Jammu|IN|32.73|74.87|2',
	  SNU: 'Santa Clara|CU|22.40|-79.97|2',
	  LCX: 'Longyan|CN|25.68|117.03|',
	  TUN: 'Tunis|TN|36.80|10.18|2',
	  YYQ: 'Churchill|CA|58.77|-94.17|2',
	  REL: 'Trelew|AR|-43.25|-65.31|2',
	  CTL: 'Charleville|AU|-26.40|146.25|2',
	  BYF: 'Albert|FR|2',
	  SKH: 'Surkhet|NP|28.33|82.51|',
	  LBA: 'Leeds|GB|53.80|-1.55|2',
	  BZY: 'Beltsy|RU|47.75|27.93|1',
	  FTE: 'El Calafate|AR|-50.28|-72.05|2',
	  LMP: 'Lampedusa|IT|35.50|12.60|2',
	  PPT: 'Papeete|PF|-17.53|-149.57|2',
	  MCK: 'McCook|US|40.20|-100.63|2',
	  CJB: 'Coimbatore|IN|11.00|76.97|2',
	  VSA: 'Villahermosa|MX|17.98|-92.92|2',
	  CPQ: 'Campinas|BR|-22.91|-47.06|2',
	  RSH: 'Russian Mission|US|61.78|-161.32|2',
	  KYS: 'Kayes|ML|14.43|-11.44|',
	  DRO: 'Durango|US|37.15|-107.75|2',
	  DPO: 'Devonport|AU|-41.17|146.35|2',
	  SRJ: 'San Borja|BO|-14.82|-66.83|',
	  KZB: 'Zachar Bay|US|57.56|-153.77|2',
	  VAG: 'Varginha|BR|-21.55|-45.43|2',
	  BUA: 'Buka Island|PG|-5.25|154.63|2',
	  PJA: 'Pajala|SE|67.20|23.37|2',
	  SDL: 'Sundsvall|SE|62.38|17.30|2',
	  TGU: 'Tegucigalpa|HN|14.10|-87.22|2',
	  FKS: 'Fukushima|JP|37.75|140.47|2',
	  PAZ: 'Poza Rica|MX|28.78|-111.60|2',
	  ELY: 'Ely NV|US|39.30|-114.84|2',
	  TUW: 'Tubala|PA|8.83|-77.67|',
	  YLC: 'Kimmirut Lake Harbour|CA|62.85|-69.88|2',
	  VDM: 'Viedma|AR|-40.80|-63.00|2',
	  KAW: 'Kawthaung|MM|10.05|98.52|',
	  AAE: 'Annaba|DZ|36.90|7.77|2',
	  FRE: 'Fera Island|SB|-8.10|159.58|2',
	  CAN: 'Guangzhou|CN|23.12|113.25|2',
	  DIW: 'Dikwella|LK|5.99|80.73|2',
	  TII: 'Tirinkot|AF|32.87|65.63|',
	  KHU: 'Kremenchug|UA|49.10|33.45|1',
	  JGN: 'Jiayuguan|CN|39.82|98.30|2',
	  YMU: '|CA|',
	  RZE: 'Rzeszow|PL|50.05|22.00|2',
	  GSM: 'Gheshm|IR|26.76|55.89|',
	  HOI: 'Hao Island|PF|-18.15|-140.95|2',
	  ZEM: 'East Main|CA|52.25|-78.52|',
	  SFQ: 'Sanli Urfa|TR|37.09|38.85|1',
	  KCC: 'Coffman Cove|US|56.01|-132.83|2',
	  UII: 'Utila|HN|16.09|-86.89|',
	  STM: 'Santarem|BR|-2.44|-54.71|2',
	  PLO: 'Port Lincoln|AU|-34.73|135.87|2',
	  ZSE: 'St Pierre Dela Reunion|RE|-21.33|55.48|2',
	  OAZ: 'Zaranj|AF|31.10|61.98|',
	  KMS: 'Kumasi|GH|6.68|-1.62|2',
	  ISP: 'Islip|US|40.73|-73.21|2',
	  ZFN: 'Tulita/Fort Norman|CA|64.92|-125.57|',
	  GLF: 'Golfito|CR|8.65|-83.15|2',
	  YNT: 'Yantai|CN|37.53|121.40|2',
	  PGV: 'Greenville|US|35.63|-77.39|2',
	  OMR: 'Oradea|RO|47.07|21.93|2',
	  PUS: 'Busan|KR|35.10|129.04|2',
	  KZI: 'Kozani|GR|40.30|21.79|1',
	  LMA: 'Lake Minchumina|US|63.88|-152.31|2',
	  KUK: 'Kasigluk|US|60.88|-162.52|',
	  GPI: 'Guapi|CO|2.56|-77.86|2',
	  NLK: 'Norfolk Island|NF|-29.04|167.94|2',
	  DEE: '|RU|',
	  DLE: 'Dole|FR|47.10|5.50|2',
	  DEA: 'Dera Ghazi Khan|PK|29.96|70.49|',
	  JGS: 'Ji an|CN|26.90|114.74|',
	  YBD: 'New Westminster|CA|49.21|-122.91|1',
	  BWE: 'Braunschweig|DE|52.27|10.53|2',
	  YYJ: 'Victoria|CA|48.43|-123.37|2',
	  TKV: 'Tatakoto|PF|-17.28|-138.33|',
	  IPL: 'El Centro|US|32.79|-115.56|2',
	  SDG: 'Sanandaj|IR|35.25|47.01|',
	  WRL: 'Worland|US|44.02|-107.95|2',
	  VAA: 'Vaasa|FI|63.10|21.62|2',
	  MSO: 'Missoula|US|46.87|-113.99|2',
	  MQP: 'Nelspruit|ZA|-25.38|31.10|',
	  TBN: 'Ft Leonard Wood|US|37.74|-92.14|2',
	  RBR: 'Rio Branco|BR|-9.97|-67.81|2',
	  BSO: 'Basco|PH|20.45|121.98|',
	  ERC: 'Erzincan|TR|39.75|39.49|2',
	  LAD: 'Luanda|AO|-8.84|13.23|2',
	  TOG: 'Togiak|US|59.06|-160.38|2',
	  USK: 'Usinsk|RU|65.99|57.53|2',
	  LBD: 'Khudzhand|TJ|40.22|69.70|2',
	  INL: 'International Falls|US|48.60|-93.41|2',
	  TBP: 'Tumbes|PE|-3.57|-80.44|2',
	  DOP: 'Dolpa|NP|29.00|82.82|',
	  NDR: 'Nador|MA|35.17|-2.93|2',
	  CEZ: 'Cortez|US|37.35|-108.59|2',
	  ZSJ: 'Sandy Lake|CA|56.98|-107.28|2',
	  VPY: 'Chimoio|MZ|-19.13|33.48|',
	  KUD: 'Kudat|MY|6.88|116.83|2',
	  HKN: 'Hoskins|PG|-5.45|150.40|2',
	  ELD: 'El Dorado|US|33.21|-92.67|2',
	  RRS: 'Roros|NO|62.58|11.40|2',
	  GOH: 'Nuuk|GL|64.18|-51.75|2',
	  ELF: 'El Fasher|SD|13.62|25.32|2',
	  LZY: '|CN|',
	  DDG: 'Dandong|CN|40.13|124.39|2',
	  LPB: 'La Paz|BO|-16.50|-68.15|2',
	  JAX: 'Jacksonville|US|30.50|-81.67|2',
	  BRQ: 'Brno|CZ|49.20|16.61|2',
	  VLU: 'Velikiye Luki|RU|56.38|30.62|1',
	  MVD: 'Montevideo|UY|-34.86|-56.17|2',
	  BXH: 'Balhash|KZ|46.88|75.02|1',
	  GTO: 'Gorontalo|ID|0.53|123.06|2',
	  UNN: 'Ranong|TH|9.97|98.63|2',
	  ARU: 'Aracatuba|BR|-21.21|-50.43|2',
	  YWJ: 'Deline|CA|65.17|123.50|',
	  HFS: 'Hagfors|SE|60.02|13.57|',
	  TKS: 'Tokushima|JP|34.07|134.57|2',
	  CSX: 'Changsha|CN|28.20|112.97|2',
	  ALZ: 'Alitak|US|56.90|-154.25|2',
	  JJU: 'Qaqortoq|GL|60.72|-46.03|2',
	  IBR: 'Omitami Iba|JP|36.18|140.41|',
	  LKY: 'Lake Manyara|TZ|-3.50|36.42|',
	  NIM: 'Niamey|NE|13.52|2.12|2',
	  BWK: 'Bol|HR|43.28|16.68|',
	  KAJ: 'Kajaani|FI|64.23|27.73|2',
	  MZG: 'Makung|TW|23.57|119.62|2',
	  PLS: 'Providenciales|TC|21.77|-72.27|2',
	  IFN: 'Isfahan|IR|32.66|51.67|2',
	  KLL: 'Levelock|US|59.11|-156.86|2',
	  RUS: 'Marau Island|SB|-10.53|161.48|2',
	  YPH: 'Inukjuak|CA|58.45|-78.10|2',
	  ADF: 'Adiyaman|TR|37.75|38.27|',
	  BRW: 'Barrow|US|71.29|-156.79|2',
	  IXD: 'Allahabad|IN|25.45|81.85|2',
	  TPA: 'Tampa|US|27.95|-82.46|2',
	  MPA: 'Mpacha|NA|-17.50|24.27|2',
	  HDG: '|CN|',
	  ULK: 'Lensk|RU|60.72|114.83|1',
	  CIH: 'Changzhi|CN|35.21|111.74|2',
	  YZF: 'Yellowknife|CA|62.46|-114.35|2',
	  LHW: 'Lanzhou|CN|36.06|103.79|2',
	  CBG: 'Cambridge|GB|52.20|0.12|1',
	  RAO: 'Ribeirao Preto|BR|-21.18|-47.81|2',
	  KKR: 'Kaukura|PF|-15.78|-146.67|2',
	  LEX: 'Lexington|US|38.04|-84.60|2',
	  YXJ: 'Ft St John|CA|56.23|-120.73|2',
	  MBT: 'Masbate|PH|12.37|123.07|',
	  GRR: 'Grand Rapids|US|42.88|-85.52|2',
	  KSN: 'Kostanay|KZ|53.17|63.58|2',
	  RNB: 'Ronneby|SE|56.20|15.30|2',
	  MVB: 'Franceville Mvengue|GA|-1.65|13.43|2',
	  KCF: 'Kadanwari|PK|27.20|69.15|',
	  BRA: 'Barreiras|BR|-12.15|-44.99|2',
	  TGJ: 'Tiga|NC|-21.10|167.80|',
	  TAI: 'Taiz|YE|13.57|44.03|2',
	  YPY: 'Fort Chipewyan|CA|58.76|-111.12|2',
	  WMO: 'White Mountain|US|64.68|-163.41|2',
	  BHI: 'Bahia Blanca|AR|-38.72|-62.28|2',
	  MFF: 'Moanda|GA|-1.57|13.20|2',
	  PHM: 'Boeblingen|DE|48.68|9.02|1',
	  MYD: 'Malindi|KE|-3.22|40.12|2',
	  RET: 'Rost|NO|67.48|12.08|2',
	  YBC: 'Baie Comeau|CA|46.30|-73.38|2',
	  VGZ: 'Villagarzon|CO|1.07|-76.72|',
	  HUE: 'Humera|ET|14.25|36.58|2',
	  TRW: 'Tarawa|KI|-0.87|169.54|2',
	  LGB: 'Long Beach|US|33.77|-118.19|2',
	  BPF: 'Batuna|SB|-8.55|158.12|',
	  SSI: 'St Simons Is|US|31.17|-81.46|2',
	  PCR: 'Puerto Carreno|CO|6.18|-67.63|',
	  GBE: 'Gaborone|BW|-24.65|25.91|2',
	  YYD: 'Smithers|CA|54.78|-127.17|2',
	  HNA: 'Morioka|JP|39.70|141.15|2',
	  LZH: 'Liuzhou|CN|29.85|118.18|2',
	  URT: 'Surat Thani|TH|9.13|99.32|2',
	  LDU: 'Lahad Datu|MY|5.03|118.32|2',
	  CZX: 'Changzhou|CN|31.78|119.97|2',
	  TBW: 'Tambov|RU|52.72|41.43|2',
	  FSP: 'St Pierre|PM|46.92|-56.17|2',
	  HKK: 'Hokitika|NZ|-42.72|170.97|2',
	  CRA: 'Craiova|RO|44.32|23.80|2',
	  KGT: '|CN|',
	  MIG: 'Mian Yang|CN|31.43|104.74|2',
	  MJM: 'Mbuji Mayi|CD|-6.15|23.63|2',
	  BCD: 'Bacolod|PH|8.12|123.92|2',
	  ONG: 'Mornington|AU|-38.22|145.03|2',
	  LNE: 'Lonorore|VU|-15.86|168.17|',
	  HNM: 'Hana|US|20.76|-155.99|2',
	  AZD: 'Yazd|IR|31.90|54.37|2',
	  KKN: 'Kirkenes|NO|69.72|30.05|2',
	  LEQ: 'Lands End|GB|50.05|-5.73|2',
	  MIS: 'Misima|PG|-10.67|152.75|2',
	  CXJ: 'Caxias Do Sul|BR|-29.17|-51.18|2',
	  AUR: 'Aurillac|FR|44.92|2.45|2',
	  RDZ: 'Rodez|FR|44.35|2.58|2',
	  LCR: 'La Chorrera|CO|-0.73|-73.02|',
	  TML: 'Tamale|GH|9.40|-0.83|2',
	  CSA: 'Colonsay Island|GB|56.08|-6.20|2',
	  SWT: 'Strzhewoi|RU|60.50|77.00|1',
	  JOK: 'Joshkar-Ola|RU|56.72|47.90|2',
	  PZO: 'Puerto Ordaz|VE|8.30|-62.72|2',
	  OSY: 'Namsos|NO|64.48|11.50|2',
	  KLU: 'Klagenfurt|AT|46.62|14.31|2',
	  TOD: 'Tioman|MY|2.75|104.17|2',
	  JEG: 'Aasiaat|GL|68.72|-52.87|2',
	  VTG: 'Vung Tau|VN|10.35|107.07|1',
	  OAX: 'Oaxaca|MX|17.05|-96.72|2',
	  YPR: 'Prince Rupert|CA|54.32|-130.32|2',
	  YNL: 'Points North Landing|CA|58.27|-104.08|2',
	  CKX: 'Chicken|US|64.07|-141.94|2',
	  YSM: 'Ft Smith|CA|60.02|-111.97|2',
	  JGO: 'Qeqertarsuaq|GL|62.12|-49.78|2',
	  UIH: 'Qui Nhon|VN|13.77|109.23|2',
	  YDA: 'Dawson City|CA|64.04|-139.12|2',
	  RVE: 'Saravena|CO|6.92|-71.90|',
	  CED: 'Ceduna|AU|-32.12|133.67|2',
	  HIN: 'Chinju|KR|35.19|128.09|2',
	  KND: 'Kindu|CD|-2.95|25.95|2',
	  BRM: 'Barquisimeto|VE|10.07|-69.32|2',
	  KCL: 'Chignik Lagoon|US|56.32|-158.55|2',
	  OOL: 'Gold Coast|AU|-28.00|153.43|2',
	  BAY: 'Baia Mare|RO|47.65|23.58|2',
	  CIZ: 'Coari|BR|-4.08|-63.13|',
	  TRD: 'Trondheim|NO|63.42|10.42|2',
	  ITH: 'Ithaca|US|42.44|-76.50|2',
	  TUR: 'Tucurui|BR|-3.77|-49.67|2',
	  XRY: 'Jerez De La Frontera|ES|36.68|-6.13|2',
	  CNS: 'Cairns|AU|-16.92|145.77|2',
	  LPM: 'Lamap|VU|-16.43|167.80|2',
	  MKQ: 'Merauke|ID|-8.47|140.33|2',
	  VDA: 'Ovda|IL|29.93|34.95|2',
	  IRZ: 'Santa Isabel Rio Negro|BR|-0.38|-65.00|2',
	  MMB: 'Memambetsu|JP|43.91|144.17|2',
	  YCO: 'Kugluktuk Coppermine|CA|67.83|-115.08|2',
	  SMR: 'Santa Marta|CO|11.25|-74.20|2',
	  PIX: 'Pico Island|PT|38.47|-28.33|2',
	  GDQ: 'Gondar|ET|12.60|37.47|2',
	  BLZ: 'Blantyre|MW|-15.78|35.00|2',
	  RUT: 'Rutland|US|43.61|-72.97|2',
	  VRL: 'Vila Real|PT|41.30|-7.75|2',
	  GBD: 'Great Bend|US|38.36|-98.76|2',
	  KIM: 'Kimberley|ZA|-28.73|24.77|2',
	  NAA: 'Narrabri|AU|-30.32|149.78|2',
	  KEK: 'Ekwok|US|59.35|-157.47|2',
	  ZAZ: 'Zaragoza|ES|41.66|-0.88|2',
	  JZH: 'Songpan|CN|32.60|103.60|2',
	  FOD: 'Fort Dodge|US|42.50|-94.17|2',
	  HSV: 'Huntsville|US|34.64|-86.78|2',
	  YUM: 'Yuma|US|32.73|-114.62|2',
	  NQN: 'Neuquen|AR|-38.95|-68.07|2',
	  HSN: 'Zhoushan|CN|30.02|122.10|2',
	  FRD: 'Friday Harbor|US|48.53|-123.02|2',
	  RSA: 'Santa Rosa|AR|-36.57|-64.27|2',
	  CYX: 'Cherskiy|RU|68.75|161.35|2',
	  KSM: 'St Marys|US|62.06|-163.30|2',
	  AHB: 'Abha|SA|18.22|42.51|2',
	  DAX: 'Daxian|CN|31.14|107.43|',
	  MUA: 'Munda|SB|-8.33|157.27|2',
	  CZL: 'Constantine|DZ|36.37|6.61|2',
	  YGV: 'Havre St Pierre|CA|50.25|-63.58|2',
	  CBT: 'Catumbela|AO|-12.48|13.48|',
	  BMD: 'Belo|MG|-19.70|44.55|2',
	  BUZ: 'Bushehr|IR|28.97|50.84|2',
	  UBJ: 'Ube Jp|JP|33.93|131.27|2',
	  SPI: 'Springfield|US|39.84|-89.68|2',
	  CGD: 'Changde|CN|29.03|111.68|2',
	  FON: 'Fortuna|CR|10.40|84.48|',
	  KGC: 'Kingscote|AU|-35.67|137.63|2',
	  LUN: 'Lusaka|ZM|-15.42|28.28|2',
	  ZFM: 'Fort Mcpherson|CA|67.48|-134.95|',
	  PPQ: 'Paraparaumu|NZ|-40.92|175.02|2',
	  ROW: 'Roswell|US|34.02|-84.36|2',
	  MEY: 'Meghauli|NP|27.58|84.23|2',
	  RSD: 'Rock Sound|BS|24.90|-76.20|2',
	  LYB: 'Little Cayman|KY|19.68|-80.05|2',
	  TMR: 'Tamenghest|DZ|22.81|5.46|',
	  FNJ: 'Pyongyang|KP|39.02|125.75|2',
	  AKI: 'Akiak|US|60.91|-161.21|2',
	  DCG: '|AE|2',
	  TRC: 'Torreon|MX|25.55|-103.43|2',
	  GDV: 'Glendive|US|47.11|-104.71|2',
	  FMM: 'Memmingen|DE|47.99|10.24|',
	  RAE: 'Arar|SA|30.90|41.14|',
	  CJU: 'Jeju City|KR|33.51|126.52|2',
	  CHX: 'Changuinola|PA|9.43|-82.52|2',
	  MGZ: 'Myeik|MM|12.45|98.62|',
	  ZHA: 'Zhangjiang|CN|28.90|111.48|2',
	  YYH: 'Taloyoak|CA|69.54|-93.52|2',
	  'FOR': 'Fortaleza|BR|-3.72|-38.54|2',
	  AXM: 'Armenia|CO|4.53|-75.68|2',
	  CSH: 'Solovetsky|RU|65.03|35.73|',
	  MST: 'Maastricht|NL|50.85|5.69|2',
	  TGR: 'Touggourt|DZ|33.10|6.07|2',
	  KUO: 'Kuopio|FI|62.90|27.68|2',
	  YHR: 'Chevery|CA|50.50|-59.50|2',
	  NHV: 'Nuku Hiva|PF|-8.90|-140.10|2',
	  WGP: 'Waingapu|ID|-9.65|120.27|2',
	  MGW: 'Morgantown|US|39.63|-79.96|2',
	  AJF: 'Al Jouf|SA|29.98|40.20|2',
	  CVN: 'Clovis|US|36.83|-119.70|2',
	  TMC: 'Tambolaka|ID|-9.34|119.18|',
	  HTS: 'Huntington|US|38.42|-82.45|2',
	  YFJ: 'Snare Lake|CA|64.19|-114.08|',
	  VBV: 'Vanuabalavu|FJ|-17.23|-178.95|2',
	  INZ: 'In Salah|DZ|27.25|2.52|2',
	  PNK: 'Pontianak|ID|-0.03|109.33|2',
	  MAQ: 'Mae Sot|TH|16.72|98.57|2',
	  SKO: 'Sokoto|NG|13.05|5.23|2',
	  YTE: 'Cape Dorset|CA|64.23|-76.54|2',
	  SRE: 'Sucre|BO|-19.04|-65.26|2',
	  LRM: 'La Romana|DO|18.42|-68.97|2',
	  DIU: 'Diu In|IN|20.72|70.92|2',
	  YVB: 'Bonaventure|CA|48.05|-65.48|2',
	  LIX: 'Likoma Island|MW|-12.08|34.73|2',
	  CKS: 'Carajas|BR|-2.95|-51.87|2',
	  BZV: 'Brazzaville|CG|-4.26|15.28|2',
	  SLK: 'Saranac Lake|US|44.33|-74.13|2',
	  KOC: 'Koumac|NC|-20.57|164.28|2',
	  MZH: 'Merzifon|TR|40.88|35.53|',
	  NPE: 'Napier Hastings|NZ|-39.47|176.87|2',
	  SUB: 'Surabaya|ID|-7.25|112.75|2',
	  CNJ: 'Cloncurry|AU|-20.70|140.50|2',
	  JIB: 'Djibouti|DJ|11.60|43.15|2',
	  GNM: 'Guanambi|BR|-14.30|-42.78|2',
	  VAI: 'Vanimo|PG|-2.68|141.30|2',
	  ZSA: 'San Salvador|BS|24.03|-74.47|2',
	  CNQ: 'Corrientes|AR|-27.47|-58.83|2',
	  TRK: 'Tarakan|ID|3.30|117.63|2',
	  ZPB: 'Sachigo Lake|CA|53.89|-92.20|',
	  MGM: 'Montgomery|US|32.30|-86.39|2',
	  NAU: 'Napuka Island|PF|-14.20|-141.25|2',
	  GPA: 'Patras|GR|38.24|21.73|2',
	  LBS: 'Labasa|FJ|-16.42|179.38|2',
	  YCF: 'Cortes Bay|CA|50.07|-124.93|',
	  NYT: 'Nay Pye Taw|MM|2',
	  HOB: 'Hobbs|US|32.70|-103.14|2',
	  SXM: 'St Maarten|SX|18.04|-63.11|2',
	  VHC: 'Saurimo|AO|-9.75|20.55|',
	  IBE: 'Ibague|CO|4.44|-75.23|2',
	  CPR: 'Casper|US|42.87|-106.31|2',
	  LKL: 'Lakselv|NO|70.05|24.93|2',
	  SVG: 'Stavanger|NO|58.97|5.75|2',
	  CNN: 'Chulman|RU|56.90|124.88|1',
	  MHH: 'Marsh Harbour|BS|26.55|-77.05|2',
	  HZH: 'Liping City|CN|26.34|109.15|',
	  ASJ: 'Amami O Shima|JP|28.42|129.70|2',
	  JNB: 'Johannesburg|ZA|-26.20|28.08|2',
	  PZU: 'Port Sudan|SD|19.62|37.22|2',
	  BVV: '|RU|',
	  WAG: 'Wanganui|NZ|-39.93|175.05|2',
	  FRS: 'Flores|GT|16.92|-89.88|2',
	  LDE: 'Lourdes|FR|43.10|-0.05|2',
	  LPQ: 'Louangphrabang|LA|19.89|102.14|2',
	  VLS: 'Valesdir|VU|-16.80|168.20|',
	  PXO: 'Porto Santo|PT|33.05|-16.33|2',
	  SLU: 'St Lucia|LC|13.92|-60.98|2',
	  VGD: 'Vologda|RU|59.22|39.90|1',
	  KDZ: 'Katugastota|LK|2',
	  EBU: 'St Etienne|FR|45.43|4.40|2',
	  IPN: 'Ipatinga|BR|-19.47|-42.54|2',
	  ATT: 'Atmautluak|US|60.87|-162.27|',
	  LOE: 'Loei|TH|17.48|101.72|2',
	  BHM: 'Birmingham|US|33.56|-86.76|2',
	  BDO: 'Bandung|ID|-6.91|107.62|2',
	  CRV: 'Crotone|IT|39.09|17.12|2',
	  LEN: 'Leon|ES|42.60|-5.57|2',
	  HTG: 'Hatanga|RU|71.97|102.50|1',
	  EXT: 'Exeter|GB|50.72|-3.53|2',
	  BDH: 'Bandar Lengeh|IR|26.55|54.88|2',
	  HYL: 'Hollis|US|55.48|-132.65|',
	  JXA: '|CN|',
	  TCZ: '|CN|',
	  PDT: 'Pendleton|US|45.67|-118.79|2',
	  SJE: 'San Jose Guaviare|CO|2.58|-72.67|2',
	  YSF: 'Stony Rapids|CA|59.25|-105.83|2',
	  HTN: 'Hotan|CN|37.10|79.93|2',
	  TTJ: 'Tottori|JP|35.50|134.23|2',
	  NBE: 'Enfidha|TN|36.08|10.44|',
	  KXA: 'Kasaan|US|55.54|-132.40|2',
	  MZT: 'Mazatlan|MX|23.22|-106.42|2',
	  WPM: 'Wipim|PG|-8.78|142.87|2',
	  ABS: 'Abu Simbel|EG|22.37|31.63|2',
	  AOO: 'Altoona|US|40.52|-78.39|2',
	  PSZ: 'Puerto Suarez|BO|-18.95|-57.80|2',
	  SLN: 'Salina|US|38.79|-97.65|2',
	  MOZ: 'Moorea|PF|-17.53|-149.83|2',
	  RGN: 'Yangon|MM|16.81|96.16|2',
	  NER: 'Neryungri|RU|56.65|124.60|2',
	  SPK: 'Sapporo|JP|43.05|141.35|2',
	  HOE: 'Houeisay|LA|20.50|103.75|',
	  IQQ: 'Iquique|CL|-20.22|-70.17|2',
	  TME: 'Tame|CO|6.50|-71.77|',
	  CXB: 'Coxs Bazaar|BD|21.45|91.97|2',
	  TCB: 'Treasure Cay|BS|26.73|-77.37|2',
	  MRY: 'Monterey|US|36.60|-121.89|2',
	  YWK: 'Wabush|CA|52.90|-66.87|2',
	  KMC: 'King Khalid Military City|SA|27.90|45.52|2',
	  CMX: 'Houghton|US|47.12|-88.57|2',
	  TUI: 'Turaif|SA|31.68|38.65|2',
	  CMU: 'Kundiawa|PG|-6.02|144.97|2',
	  ROA: 'Roanoke|US|37.27|-79.94|2',
	  AFZ: 'Sabzevar|IR|36.17|57.60|',
	  TCQ: 'Tacna|PE|-18.01|-70.25|2',
	  ALW: 'Walla Walla|US|46.06|-118.34|2',
	  WUX: 'Wuxi|CN|31.58|120.29|2',
	  KDH: 'Kandahar|AF|31.51|65.85|2',
	  ATD: 'Atoifi|SB|-8.87|161.03|',
	  GFN: 'Grafton|AU|-29.68|152.93|2',
	  SCY: 'San Cristobal|EC|-0.83|-89.43|2',
	  MUK: 'Mauke Island|CK|-20.16|-157.34|2',
	  SKK: 'Shaktoolik|US|64.32|-161.14|',
	  EBL: 'Erbil|IQ|36.19|44.01|2',
	  BJB: 'Bojnord|IR|37.49|57.31|',
	  SHV: 'Shreveport|US|32.53|-93.75|2',
	  CHG: 'Chaoyang|CN|41.55|120.44|',
	  MSA: 'Muskrat Dam|CA|53.82|-91.98|',
	  PIA: 'Peoria|US|40.69|-89.59|2',
	  AEH: 'Abecher|TD|13.85|20.85|2',
	  LNS: 'Lancaster|US|40.12|-76.30|2',
	  ANM: 'Antalaha|MG|-14.88|50.28|2',
	  URA: 'Uralsk|KZ|51.23|51.37|2',
	  NCU: 'Nukus|UZ|42.48|59.63|2',
	  ALP: 'Aleppo|SY|36.20|37.16|2',
	  NFG: 'Nefteyugansk|RU|61.08|72.70|1',
	  VXO: 'Vaxjo|SE|56.88|14.82|2',
	  SHG: 'Shungnak|US|66.89|-157.14|2',
	  EAS: 'San Sebastian|ES|43.31|-1.97|2',
	  MXL: 'Mexicali|MX|32.65|-115.47|2',
	  SRZ: 'Santa Cruz|BO|-17.80|-63.17|2',
	  KWT: 'Kwethluk|US|60.81|-161.44|2',
	  SUN: 'Sun Valley|US|39.60|-119.78|2',
	  CPA: 'Cape Palmas|LR|4.38|-7.70|',
	  BUR: 'Burbank|US|34.18|-118.31|2',
	  PMO: 'Palermo|IT|38.12|13.37|2',
	  PUK: 'Pukarua|PF|-18.27|-137.00|2',
	  LPT: 'Lampang|TH|18.30|99.51|2',
	  YKG: 'Kangirsuk|CA|60.02|-70.03|2',
	  OPU: 'Balimo|PG|-8.05|142.95|2',
	  LCE: 'La Ceiba|HN|15.78|-86.80|2',
	  FEZ: 'Fez Ma|MA|34.05|-4.98|2',
	  ILM: 'Wilmington|US|34.27|-77.90|2',
	  YNA: 'Natashquan|CA|50.17|-61.75|2',
	  BQT: 'Brest|BY|52.10|23.70|2',
	  LME: 'Le Mans|FR|48.00|0.20|1',
	  MMY: 'Miyako Jima|JP|24.78|125.30|2',
	  KAC: 'Kameshli|SY|37.03|41.20|2',
	  EKS: '|RU|',
	  SOJ: 'Sorkjosen|NO|69.80|20.93|2',
	  MQM: 'Mardin|TR|37.31|40.74|2',
	  OMH: 'Urmieh|IR|37.62|45.08|2',
	  UKS: 'Sevastopol|AK|44.69|33.58|1',
	  BGF: 'Bangui|CF|4.37|18.58|2',
	  ZKG: 'Kegaska|CA|50.19|-61.29|2',
	  PPB: 'Presidente Prudente|BR|-22.13|-51.39|2',
	  SJI: 'San Jose|PH|12.45|121.15|2',
	  HHH: 'Hilton Head|US|32.22|-80.68|2',
	  LMN: 'Limbang|MY|4.75|115.00|2',
	  VDC: 'Vitoria Da Conquista|BR|-14.87|-40.84|2',
	  GBT: 'Gorgon|IR|36.91|54.41|',
	  BOI: 'Boise|US|43.61|-116.20|2',
	  SUG: 'Surigao|PH|9.78|125.49|2',
	  WNN: 'Wunnummin Lake|CA|52.92|-89.17|',
	  BUF: 'Buffalo|US|42.89|-78.88|2',
	  BZR: 'Beziers|FR|43.35|3.25|2',
	  MEE: 'Mare|NC|-21.57|168.00|2',
	  FAE: 'Faeroe Islands|FO|62.07|-7.27|2',
	  RIB: 'Riberalta|BO|-10.98|-66.10|2',
	  NBX: 'Nabire|ID|-3.37|135.48|2',
	  ICI: 'Cicia|FJ|-17.75|-179.30|2',
	  NBS: '|CN|',
	  YMF: 'Montagne Harbor|CA|48.82|-123.20|',
	  OST: 'Ostende|BE|51.22|2.92|1',
	  QUB: 'Ubari|LY|26.60|12.77|',
	  NDG: 'Qiqihar|CN|47.34|123.97|2',
	  BPL: '|CN|',
	  YAM: 'Sault Ste Marie|CA|46.48|-84.51|2',
	  AKP: 'Anaktuvuk Pass|US|68.14|-151.74|2',
	  HWN: 'Hwange|ZW|-18.37|26.48|1',
	  JTC: '|BR|',
	  ALM: 'Alamogordo|US|32.90|-105.96|2',
	  KRQ: 'Kramatorsk|UA|48.72|37.62|1',
	  FGU: 'Fangatau|PF|-16.05|-141.83|2',
	  BJI: 'Bemidji|US|47.47|-94.88|2',
	  CLP: 'Clarks Point|US|58.84|-158.55|2',
	  YQG: 'Windsor|CA|42.30|-83.02|2',
	  YUB: 'Tuktoyaktuk|CA|69.44|-133.03|',
	  AJR: 'Arvidsjaur|SE|65.58|19.17|2',
	  AUG: 'Augusta|US|44.32|-69.80|2',
	  ANI: 'Aniak|US|61.58|-159.52|2',
	  HFT: 'Hammerfest|NO|70.66|23.69|2',
	  LTO: 'Loreto|MX|22.27|-101.97|2',
	  LAO: 'Laoag|PH|18.20|120.59|2',
	  LAR: 'Laramie|US|41.31|-105.59|2',
	  AQG: 'Anqing|CN|30.51|117.05|2',
	  MXH: 'Moro|PG|-6.35|143.25|',
	  LJG: 'Lijiang City|CN|26.87|100.22|2',
	  BHE: 'Blenheim|NZ|-41.52|173.95|2',
	  NPL: 'New Plymouth|NZ|-39.07|174.08|2',
	  BNA: 'Nashville|US|36.17|-86.78|2',
	  PHG: 'Port Harcourt|NG|5.03|6.83|',
	  OKC: 'Oklahoma City|US|35.47|-97.52|2',
	  TVF: 'Thief River Falls|US|48.12|-96.18|2',
	  POS: 'Port Of Spain|TT|10.65|-61.52|2',
	  MYA: 'Moruya|AU|-35.92|150.08|2',
	  TUO: 'Taupo|NZ|-38.68|176.08|2',
	  JJN: 'Jinjiang|CN|24.88|118.60|2',
	  MEC: 'Manta|EC|-0.95|-80.73|2',
	  YYC: 'Calgary|CA|51.05|-114.08|2',
	  GWD: 'Gwadar|PK|25.12|62.33|2',
	  MDE: 'Medellin|CO|6.29|-75.54|2',
	  LYC: 'Lycksele|SE|64.60|18.67|2',
	  IWD: 'Ironwood|US|46.45|-90.17|2',
	  BZO: 'Bolzano|IT|46.49|11.33|2',
	  FNI: 'Nimes|FR|43.83|4.35|2',
	  BIL: 'Billings|US|45.78|-108.50|2',
	  FAV: 'Fakarava|PF|-16.32|-145.62|2',
	  AEB: 'Baise|CN|23.72|106.97|2',
	  MZV: 'Mulu|MY|4.03|114.80|2',
	  EIN: 'Eindhoven|NL|51.44|5.48|2',
	  PBI: 'West Palm Beach|US|26.72|-80.05|2',
	  DND: 'Dundee|GB|56.50|-2.97|2',
	  FKQ: 'Fak Fak|ID|-2.93|132.30|2',
	  PLM: 'Palembang|ID|-2.92|104.75|2',
	  SUJ: 'Satu Mare|RO|47.80|22.88|2',
	  TLE: 'Tulear|MG|-23.35|43.67|2',
	  XKS: 'Kasabonika|CA|53.52|-88.65|',
	  SKT: 'Sialkot|PK|32.50|74.52|2',
	  PDP: 'Punta Del Este|UY|-34.97|-54.95|2',
	  RMQ: 'Shalu|TW|24.25|120.60|',
	  PBD: 'Porbandar|IN|21.64|69.61|2',
	  BWT: 'Burnie|AU|-41.07|145.92|2',
	  CJS: 'Ciudad Juarez|MX|31.73|-106.48|2',
	  NNG: 'Nanning|CN|22.82|108.32|2',
	  SMX: 'Santa Maria|US|34.91|-120.46|2',
	  CLO: 'Cali|CO|3.44|-76.52|2',
	  COU: 'Columbia|US|38.81|-92.22|2',
	  WRG: 'Wrangell|US|56.47|-132.38|2',
	  SMQ: 'Sampit|ID|-3.08|113.05|2',
	  YNZ: 'Yancheng|CN|33.43|120.20|',
	  GUW: 'Atyrau|KZ|47.12|51.88|2',
	  CHS: 'Charleston|US|32.90|-80.04|2',
	  UGC: 'Urgench|UZ|41.55|60.63|2',
	  YVP: 'Kuujjuaq|CA|58.10|-68.40|2',
	  AEG: 'Aek Godang|ID|1.40|99.42|',
	  SIC: 'Sinop|TR|42.02|35.07|',
	  LYI: 'Linyi|CN|35.06|118.34|2',
	  KSH: 'Kermanshah|IR|34.31|47.06|2',
	  VGA: 'Vijayawada|IN|16.53|80.80|',
	  TIJ: 'Tijuana|MX|32.53|-117.02|2',
	  ONJ: 'Odate Noshiro|JP|40.19|140.38|2',
	  BDQ: 'Vadodara|IN|22.30|73.20|2',
	  AOR: 'Alor Setar|MY|6.12|100.37|2',
	  SRV: 'Stony River|US|61.78|-156.59|2',
	  SKB: 'St Kitts|KN|17.31|-62.72|2',
	  JNN: 'Nanortalik|GL|60.12|-45.22|2',
	  BKW: 'Beckley|US|37.78|-81.19|2',
	  BYC: 'Yacuiba|BO|-22.03|-63.68|2',
	  OZC: 'Ozamis City|PH|8.18|123.85|',
	  YKS: 'Yakutsk|RU|62.03|129.73|2',
	  FBS: 'Friday Harbor|US|48.54|-123.01|',
	  YZV: 'Sept Iles|CA|50.20|-66.38|2',
	  KTS: 'Brevig Mission|US|65.33|-166.49|2',
	  SDZ: 'Shetland Islands Area|GB|60.13|-1.13|2',
	  JUZ: 'Juzhou|CN|28.97|118.83|',
	  HOM: 'Homer|US|59.64|-151.55|2',
	  EKO: 'Elko|US|40.83|-115.76|2',
	  VNS: 'Varanasi|IN|25.33|83.00|2',
	  MGF: 'Maringa|BR|-23.43|-51.94|2',
	  RAI: 'Praia|CV|14.92|-23.52|2',
	  GHT: 'Ghat|LY|24.96|10.17|2',
	  KRL: 'Korla|CN|41.76|86.15|2',
	  VNX: 'Vilanculos|MZ|-22.60|35.11|2',
	  YMO: 'Moosonee|CA|51.32|-80.72|2',
	  IRC: 'Circle|US|65.83|-144.06|',
	  KOY: 'Olga Bay|US|57.08|-154.42|2',
	  FSZ: 'Shizuoka|JP|34.80|138.19|',
	  MYY: 'Miri|MY|4.38|113.98|2',
	  ELI: 'Elim|US|40.30|-78.94|2',
	  CRM: 'Catarman|PH|12.48|124.58|',
	  VBM: '|US|',
	  DHI: 'Dhangarhi|NP|28.68|80.63|',
	  UGT: 'Umnugobitour|MN|43.75|104.13|2',
	  HFE: 'Hefei|CN|31.86|117.28|2',
	  LAP: 'La Paz|MX|24.13|-110.30|2',
	  CEM: 'Central|US|65.57|-144.78|2',
	  EVE: 'Harstad Narvik|NO|68.50|16.68|2',
	  VFA: 'Victoria Falls|ZW|-17.93|25.83|2',
	  UTU: 'Ustupo|PA|9.67|-78.83|',
	  KGO: 'Kirovograd|UA|48.55|32.30|1',
	  RCP: 'Cinder Park|US|57.08|-157.81|',
	  ZAH: 'Zahedan|IR|29.48|60.90|',
	  GRI: 'Grand Island|US|40.92|-98.34|2',
	  TET: 'Tete|MZ|-16.16|33.59|2',
	  AVL: 'Asheville|US|35.60|-82.55|2',
	  TYS: 'Knoxville|US|35.96|-83.92|2',
	  YAO: 'Yaounde|CM|3.87|11.52|2',
	  GAY: 'Gaya|IN|24.78|85.00|2',
	  MHU: 'Mount Hotham|AU|-36.97|147.12|2',
	  VCA: 'Can Tho|VN|10.03|105.78|2',
	  TAY: 'Tartu|EE|58.37|26.74|2',
	  MKG: 'Muskegon|US|43.23|-86.25|2',
	  KIJ: 'Niigata|JP|37.92|139.05|2',
	  IHR: 'Iran Shahr|IR|27.23|60.72|',
	  ADU: 'Ardabil|IR|38.25|48.30|2',
	  OCC: 'Coca|EC|-0.47|-76.93|2',
	  GRB: 'Green Bay|US|44.52|-88.02|2',
	  RLG: 'Rostock Laage|DE|53.92|12.27|2',
	  BIQ: 'Biarritz|FR|43.48|-1.57|2',
	  PBJ: 'Paama|VU|-16.43|168.22|',
	  SKX: 'Saransk|RU|54.17|45.17|1',
	  LYH: 'Lynchburg|US|37.41|-79.14|2',
	  MJT: 'Mytilene|GR|39.11|26.55|2',
	  JMU: 'Jiamusi|CN|46.83|130.35|2',
	  IGG: 'Igiugig|US|59.33|-155.90|2',
	  PTA: 'Port Alsworth|US|60.20|-145.33|',
	  AMD: 'Ahmedabad|IN|23.03|72.62|2',
	  NKC: 'Nouakchott|MR|18.12|-16.04|2',
	  WEH: 'Weihai|CN|37.50|122.11|2',
	  PKP: 'Pukapuka|PF|-14.75|-138.97|2',
	  PPS: 'Puerto Princesa|PH|9.74|118.73|2',
	  ZBR: 'Chah-Bahar|IR|25.44|60.38|',
	  UME: 'Umea|SE|63.83|20.25|2',
	  ORB: 'Orebro Bofors|SE|59.28|15.22|2',
	  ATM: 'Altamira|BR|-15.45|-47.61|2',
	  WDH: 'Windhoek|NA|-22.57|17.08|2',
	  YSJ: 'St John|CA|45.32|-65.88|2',
	  LGQ: 'Lago Agrio|EC|0.08|-76.88|2',
	  ESM: 'Esmeraldas|EC|0.97|-79.63|2',
	  YIO: 'Pond Inlet|CA|72.68|-78.00|',
	  OXB: 'Bissau|GW|11.85|-15.58|2',
	  LBP: 'Long Banga|MY|3.20|115.38|2',
	  TPS: 'Trapani|IT|38.02|12.51|2',
	  MUR: 'Marudi|MY|4.18|114.32|2',
	  YOL: 'Yola|NG|9.20|12.48|2',
	  CBH: 'Bechar|DZ|31.62|-2.22|2',
	  HPB: 'Hooper Bay|US|61.53|-166.10|2',
	  BSC: 'Bahia Solano|CO|6.33|-77.43|2',
	  MQX: 'Makale|ET|13.50|39.48|2',
	  YPW: 'Powell River|CA|49.85|-124.54|2',
	  COR: 'Cordoba|AR|-31.40|-64.18|2',
	  TYR: 'Tyler|US|32.35|-95.30|2',
	  JRS: 'Jerusalem|IL|31.77|35.23|1',
	  STG: 'St George Island|US|56.60|-169.55|2',
	  SHP: 'Qinhuangdao|CN|39.93|119.59|2',
	  CAC: 'Cascavel|BR|-24.96|-53.46|2',
	  BHY: 'Beihai|CN|21.48|109.10|2',
	  WJU: 'WonJu|KR|37.43|37.43|',
	  HCN: 'Hengchun|TW|22.00|120.74|2',
	  IAS: 'Iasi|RO|47.17|27.60|2',
	  TRE: 'Tiree|GB|56.50|-6.92|2',
	  SNR: 'St Nazaire|FR|47.29|-2.18|2',
	  GDZ: 'Gelendzik|RU|44.57|38.02|2',
	  PMW: 'Palmas|BR|-10.24|-48.35|2',
	  OUA: 'Ouagadougou|BF|12.37|-1.52|2',
	  JUV: 'Upernavik|GL|72.78|-56.17|2',
	  CFA: 'Coffee Point|US|58.22|-157.50|2',
	  KVX: 'Kirov|RU|58.50|49.35|',
	  GLV: 'Golovin|US|64.54|-163.04|2',
	  ALY: 'Alexandria|EG|31.20|29.92|2',
	  VLK: 'Volgodonsk|RU|47.68|42.08|1',
	  PLW: 'Palu|ID|-0.90|119.86|2',
	  YQU: 'Grande Prairie|CA|55.17|-118.80|2',
	  TUP: 'Tupelo|US|34.26|-88.70|2',
	  UGI: 'Uganik|US|57.88|-153.50|2',
	  RHI: 'Rhinelander|US|45.64|-89.41|2',
	  MYU: 'Mekoryuk|US|60.39|-166.19|2',
	  OUL: 'Oulu|FI|65.02|25.47|2',
	  LGP: 'Legaspi|PH|13.15|123.76|2',
	  TMX: 'Timimoun|DZ|29.23|0.28|',
	  HAU: 'Haugesund|NO|59.41|5.28|2',
	  GCN: 'Grand Canyon|US|36.05|-112.14|2',
	  MKK: 'Hoolehua|US|21.15|-157.10|2',
	  PDS: 'Piedras Negras|MX|28.70|-100.52|2',
	  YWM: 'Williams Harbour|CA|52.34|-56.01|',
	  WNH: '|CN|',
	  LWY: 'Lawas|MY|4.85|115.40|2',
	  MKM: 'Mukah|MY|2.90|112.10|2',
	  BTJ: 'Banda Aceh|ID|5.56|95.33|2',
	  WYS: 'West Yellowstone|US|44.66|-111.10|2',
	  KMY: 'Moser Bay|US|56.99|-154.15|2',
	  VXC: 'Lichinga|MZ|-13.28|35.25|',
	  DSK: 'Dera Ismail Khan|PK|31.83|70.90|2',
	  PDV: 'Plovdiv|BG|42.15|24.75|2',
	  SQG: 'Sintang|ID|0.08|111.58|2',
	  JHM: 'Kapalua|US|21.00|-156.66|2',
	  YOP: 'Rainbow Lake|CA|58.50|-119.39|2',
	  PSR: 'Pescara|IT|42.46|14.21|2',
	  NQY: 'Newquay|GB|50.41|-5.08|2',
	  YZT: 'Port Hardy|CA|50.70|-127.42|2',
	  IAA: 'Igarka|RU|67.43|86.63|2',
	  KNQ: 'Kone|NC|-20.52|164.82|',
	  KWF: 'Waterfall|US|55.30|-133.24|',
	  TSV: 'Townsville|AU|-19.25|146.80|2',
	  TIQ: 'Tinian|MP|15.00|145.63|2',
	  KZS: 'Kastelorizo|GR|36.15|29.59|2',
	  OSK: 'Oskarshamn|SE|57.27|16.43|2',
	  DQA: 'Daqing|CN|',
	  YAG: 'Fort Frances|CA|48.60|-93.40|2',
	  TLJ: 'Tatalina|US|62.89|-155.97|2',
	  KGX: 'Grayling|US|44.66|-84.71|2',
	  BIC: 'Big Creek|US|58.30|-157.52|2',
	  ASA: 'Assab|ER|13.01|42.74|2',
	  VPG: 'Vipingo|KE|2',
	  SHJ: 'Sharjah|AE|25.36|55.39|2',
	  HDY: 'Hat Yai|TH|7.02|100.47|2',
	  TIZ: 'Tari|PG|-5.86|142.94|2',
	  AKJ: 'Asahikawa|JP|43.77|142.37|2',
	  SHD: 'Staunton|US|38.15|-79.07|2',
	  HAR: 'Harrisburg|US|40.27|-76.88|2',
	  NVK: 'Narvik|NO|68.44|17.44|2',
	  ILP: 'Ile Des Pins|NC|-22.62|167.50|2',
	  WUH: 'Wuhan|CN|30.58|114.27|2',
	  NLG: 'Nelson Lagoon|US|56.00|161.15|',
	  KTL: 'Kitale|KE|0.97|34.96|',
	  AGT: 'Ciudad Del Este|PY|-25.52|-54.62|2',
	  AHU: 'Al Hoceima|MA|35.24|-3.93|2',
	  LRR: 'Lar|IR|27.67|54.38|',
	  XFN: 'Xiangfan|CN|32.04|112.14|2',
	  GFK: 'Grand Forks|US|47.93|-97.03|2',
	  NLT: '|CN|',
	  VIL: 'Dakhla|MA|23.71|-15.94|2',
	  PYY: 'Pai City|TH|',
	  DLI: 'Dalat|VN|11.75|108.38|2',
	  LAE: 'Lae Pg|PG|-6.74|147.00|2',
	  SPR: 'San Pedro|BZ|17.91|-87.97|2',
	  HID: 'Horn Island|AU|-10.62|142.28|2',
	  UUA: 'Bugulma|RU|54.62|52.82|2',
	  ILE: 'Killeen|US|31.12|-97.73|2',
	  RGK: 'Gorno-Altaisk|RU|51.95|85.95|2',
	  ATZ: 'Assiut|EG|27.03|31.00|',
	  AGR: 'Agra|IN|27.18|78.02|1',
	  MAH: 'Menorca|ES|40.00|4.00|2',
	  KVD: 'GANJA|AZ|40.74|46.32|1',
	  PZI: 'Panzhihua|CN|26.55|101.73|2',
	  GMB: 'Gambela|ET|8.25|34.58|2',
	  PVS: 'Provideniya|RU|64.38|173.30|1',
	  PGA: 'Page|US|36.91|-111.46|2',
	  MTT: 'Minatitlan|MX|17.98|-94.52|2',
	  MJZ: 'Mirnyj|RU|62.53|114.03|2',
	  CHY: 'Choiseul Bay|SB|-6.70|156.43|2',
	  SCV: 'Suceava|RO|47.63|26.25|2',
	  BHB: 'Bar Harbor|US|44.39|-68.20|2',
	  RDN: 'Redang|MY|5.77|103.01|2',
	  DLU: 'Dali City|CN|25.67|100.32|2',
	  CAP: 'Cap Haitien|HT|19.76|-72.20|2',
	  CCV: 'Craig Cove|VU|-16.20|167.50|',
	  WNS: 'Nawabshah|PK|26.25|68.42|2',
	  GYI: 'Gisenyi|RW|-1.70|29.26|2',
	  SOV: 'Seldovia|US|59.44|-151.71|2',
	  TGC: 'Trenton|MY|36.07|-88.99|',
	  KDL: 'Kardla|EE|58.98|22.80|',
	  BTR: 'Baton Rouge|US|30.45|-91.15|2',
	  FUE: 'Puerto Del Rosario|ES|28.50|-13.87|2',
	  MNS: 'Mansa|ZM|-11.13|28.87|2',
	  KTW: 'Katowice|PL|50.27|19.02|2',
	  IQM: 'Qiemo|CN|38.13|85.53|2',
	  PXM: 'Puerto Escondido|MX|15.85|-97.07|2',
	  IRI: 'Iringa|TZ|-7.77|35.70|2',
	  KBC: 'Birch Creek|US|45.20|-87.61|2',
	  DLC: 'Dalian|CN|38.91|121.60|1',
	  BVH: 'Vilhena|BR|-12.74|-60.15|2',
	  GMA: 'Gemena|CD|3.25|19.77|2',
	  MYR: 'Myrtle Beach|US|33.69|-78.89|2',
	  URY: 'Gurayat|SA|31.33|37.33|2',
	  SNE: 'Sao Nicolau|CV|16.59|-24.29|2',
	  TPQ: 'Tepic|MX|21.50|-104.90|2',
	  BND: 'Bandar Abbas|IR|27.19|56.28|2',
	  SRI: 'Samarinda|ID|-0.50|117.15|2',
	  ORV: 'Noorvik|US|66.84|-161.03|2',
	  YTH: 'Thompson|CA|55.74|-97.86|2',
	  JJM: 'Meru-Kinna|KE|2',
	  NAK: 'Nakhon Ratchasima|TH|14.97|102.10|2',
	  BVB: 'Boa Vista|BR|2.82|-60.67|2',
	  NAT: 'Natal|BR|-5.79|-35.21|2',
	  OME: 'Nome|US|64.50|-165.41|2',
	  LSP: 'Las Piedras|VE|11.72|-70.19|2',
	  XKH: 'Xieng Khouang|LA|19.20|102.72|2',
	  RAB: 'Rabaul|PG|-4.20|152.18|2',
	  KUT: 'Kutaisi|GE|42.25|42.70|',
	  PJG: 'Panjgur|PK|26.97|64.10|2',
	  BRC: 'San Carlos Bariloche|AR|-41.15|-71.16|2',
	  TLT: 'Tuluksak|US|61.10|-160.96|2',
	  OHE: '|CN|',
	  NTX: 'Natuna Ranai|ID|3.95|108.38|',
	  TWF: 'Twin Falls|US|42.56|-114.46|2',
	  PIE: 'St Petersburg|US|27.92|-82.68|1',
	  JKL: 'Kalymnos Island|GR|37.69|-83.32|2',
	  IDR: 'Indore|IN|22.72|75.83|2',
	  KME: 'Kamembe|RW|-2.47|28.92|',
	  SYB: 'Seal Bay|US|58.17|-152.50|2',
	  NLV: 'Nikolaev|UA|47.05|31.92|2',
	  BEU: 'Bedourie|AU|-24.35|139.47|2',
	  MOA: 'Moa CU|CU|20.50|-74.88|2',
	  GLT: 'Gladstone|AU|-23.85|151.25|2',
	  BMV: 'Banmethuot|VN|12.67|108.05|2',
	  BKQ: 'Blackall|AU|-24.42|145.47|2',
	  WMN: 'Maroantsetra|MG|-15.43|49.73|2',
	  BIA: 'Bastia|FR|42.70|9.45|2',
	  REA: 'Reao|PF|-18.50|-136.40|2',
	  KYZ: 'Kyzyl|RU|51.70|94.47|1',
	  CXI: 'Christmas Island|KI|1.97|-157.45|2',
	  POL: 'Pemba|MZ|-12.96|40.51|2',
	  BAL: 'Batman|TR|37.92|41.08|',
	  RTB: 'Roatan|HN|16.30|-86.55|2',
	  BJL: 'Banjul|GM|13.46|-16.58|2',
	  UKA: 'Ukunda|KE|-4.30|39.57|',
	  NYM: 'Nadym|RU|65.48|72.72|2',
	  NNL: 'Nondalton|US|59.98|-154.84|',
	  CLQ: 'Colima|MX|19.23|-103.72|2',
	  BYO: 'Bonito|BR|-21.23|-56.46|',
	  HDD: 'Hyderabad|PK|25.32|68.36|2',
	  MNA: 'Melangguane|ID|4.05|126.70|2',
	  HLZ: 'Hamilton|NZ|-37.78|175.28|2',
	  LKG: 'Lokichoggio|KE|4.20|34.35|2',
	  YXN: 'Whale Cove|CA|62.17|-92.58|2',
	  JLN: 'Joplin|US|37.08|-94.51|2',
	  GWL: 'Gwalior|IN|26.22|78.18|2',
	  KAZ: 'Au ID|ID|1.17|127.87|2',
	  MAK: 'Malakal|SD|9.56|31.64|',
	  OIT: 'Oita|JP|33.24|131.60|2',
	  YZR: 'Sarnia|CA|42.97|-82.38|2',
	  YQC: 'Quaqtaq|CA|61.03|-69.61|2',
	  YHY: 'Hay River|CA|60.82|-115.80|2',
	  ZAJ: 'Zaranj|AF|31.10|61.98|',
	  MUN: 'Maturin|VE|9.75|-63.18|2',
	  DUR: 'Durban|ZA|-29.85|31.02|2',
	  JUI: 'Juist|DE|53.68|7.00|2',
	  RNN: 'Bornholm|DK|55.13|14.91|2',
	  AJN: 'Anjouan|KM|-12.25|44.42|2',
	  NMA: 'Namangan|UZ|40.98|71.56|2',
	  BDB: 'Bundaberg|AU|-24.85|152.35|2',
	  EUX: 'St Eustatius|BQ|17.49|-62.98|2',
	  NOS: 'Nossi Be|MG|-13.31|48.31|2',
	  GAL: 'Galena|US|42.42|-90.43|2',
	  AXU: 'Axum|ET|14.13|38.72|2',
	  SZZ: 'Szczecin|PL|53.43|14.55|2',
	  WKJ: 'Wakkanai|JP|45.41|141.67|2',
	  LPD: 'La Pedrera|CO|-1.30|-69.72|',
	  JHB: 'Johor Bahru|MY|1.47|103.75|2',
	  MZO: 'Manzanillo|CU|20.34|-77.12|2',
	  LAZ: 'Bom Jesus Da Lapa|BR|-13.26|-43.41|2',
	  DSM: 'Des Moines|US|41.60|-93.61|2',
	  WUA: 'Wuhai|CN|39.66|106.81|2',
	  CUC: 'Cucuta|CO|7.88|-72.51|2',
	  TAK: 'Takamatsu|JP|34.33|134.05|2',
	  IRA: 'Kirakira|SB|-10.45|161.92|2',
	  TUC: 'Tucuman|AR|-26.82|-65.22|2',
	  SJK: 'Sao Jose Dos Campos|BR|-23.18|-45.89|2',
	  TMI: 'Tumling Tar|NP|27.30|87.20|',
	  EYW: 'Key West|US|24.56|-81.78|2',
	  HMI: 'Hami|CN|42.92|93.42|',
	  ZBL: 'Biloela|AU|-24.42|150.50|2',
	  VIT: 'Vitoria|ES|42.83|-2.50|1',
	  IXS: 'Silchar|IN|24.82|92.80|2',
	  ESU: 'Essaouira|MA|31.51|-9.76|2',
	  YQY: 'Sydney|CA|46.17|-60.05|2',
	  FOA: 'Foula|GB|60.17|-2.08|',
	  MIU: 'Maiduguri|NG|11.85|13.16|2',
	  YMK: '|RU|',
	  OND: 'Ondangwa|NA|-17.92|15.95|2',
	  BZG: 'Bydgoszcz|PL|53.15|18.00|2',
	  KOV: 'Kokshetau|KZ|53.32|69.40|2',
	  OAK: 'Oakland|US|37.80|-122.27|2',
	  WAA: 'Wales|US|65.62|-168.09|2',
	  TRU: 'Trujillo|PE|-8.12|-79.03|2',
	  LUR: 'Cape Lisburne|US|68.88|-166.11|2',
	  MDL: 'Mandalay|MM|22.00|96.08|2',
	  UDJ: 'Uzhgorod|UA|48.62|22.28|1',
	  HTI: 'Hamilton Island|AU|-20.37|148.95|2',
	  KTN: 'Ketchikan|US|55.34|-131.65|2',
	  BVG: 'Berlevag|NO|70.85|29.10|2',
	  SUR: 'Summer Beaver|CA|52.72|-88.55|2',
	  ACX: 'Xingyi|CN|25.05|104.98|2',
	  TNR: 'Antananarivo|MG|-18.92|47.52|2',
	  YDQ: 'Dawson Creek|CA|55.77|-120.24|2',
	  PVO: 'Portoviejo|EC|-1.05|-80.45|2',
	  TEE: 'Tbessa|DZ|35.47|8.15|2',
	  GMO: '|NG|',
	  VPE: 'Ongiva|AO|-17.05|15.07|',
	  LUL: 'Laurel|US|39.10|-76.85|2',
	  JUB: 'Juba|SD|4.87|31.60|',
	  YOC: 'Old Crow|CA|67.58|-139.83|2',
	  PLD: 'Playa Samara|CR|10.25|-85.42|2',
	  CYP: 'Calbayog|PH|12.08|124.54|',
	  XMH: 'Manihi|PF|-14.44|-146.07|',
	  RKA: '|PF|',
	  USQ: 'Usak|TR|38.68|29.41|2',
	  CWC: 'Chernovtsy|UA|48.27|25.97|2',
	  SCZ: 'Santa Cruz Island|SB|-11.62|166.85|2',
	  DJG: 'Djanet|DZ|24.55|9.48|2',
	  SLX: 'Salt Cay|TC|21.33|-71.20|2',
	  JNX: 'Naxos|GR|37.05|25.48|2',
	  JRH: 'Jorhat|IN|26.75|94.22|2',
	  RUR: 'Rurutu|PF|-22.43|-151.33|2',
	  TRZ: 'Tiruchirappali|IN|10.77|78.71|2',
	  OKT: 'Oktiabrskij|RU|54.43|53.38|1',
	  AAT: 'Altay|CN|47.87|88.12|2',
	  WRY: 'Westray|GB|59.30|-3.00|2',
	  NVT: 'Navegantes|BR|-26.90|-48.65|2',
	  LXS: 'Limnos|GR|39.92|25.25|2',
	  TJU: 'Kulob|TJ|0.00|0.00|0',
	  ACP: 'Sahand|IR|37.35|46.15|',
	  FHZ: 'Fakahina|PF|-15.99|-140.16|',
	  PDG: 'Padang|ID|-0.95|100.35|2',
	  KOW: 'Ganzhou|CN|38.93|100.45|2',
	  SCN: 'Saarbruecken|DE|49.23|7.00|2',
	  KKI: 'Akiachak|US|60.91|-161.43|2',
	  SJP: 'Sao Jose Do Rio Preto|BR|-20.82|-49.38|2',
	  ADE: 'Aden|YE|12.78|45.04|2',
	  LYP: 'Faisalabad|PK|31.42|73.08|2',
	  WKK: 'Aleknagik|US|59.27|-158.62|2',
	  MAU: 'Maupiti|PF|-16.45|-152.25|2',
	  NVA: 'Neiva|CO|2.93|-75.33|2',
	  AEX: 'Alexandria|US|31.33|-92.55|2',
	  THE: 'Teresina|BR|-5.09|-42.80|2',
	  MTM: 'Metlakatla|US|55.13|-131.57|2',
	  MVT: 'Mataiva|PF|-17.75|-149.18|2',
	  YGL: 'La Grande|CA|53.63|-77.71|2',
	  DSA: 'Doncaster|GB|53.53|-1.12|2',
	  BFF: 'Scottsbluff|US|41.87|-103.67|2',
	  CMA: 'Cunnamulla|AU|-28.07|145.68|2',
	  CGQ: 'Changchun|CN|43.88|125.32|2',
	  FTA: 'Futuna Island|VU|-19.42|170.25|',
	  JNU: 'Juneau|US|58.30|-134.42|2',
	  TKU: 'Turku|FI|60.45|22.28|2',
	  BDU: 'Bardufoss|NO|69.04|18.59|2',
	  LIH: 'Lihue|US|21.98|-159.37|2',
	  IRG: 'Lockhart|AU|-35.23|146.72|2',
	  MCE: 'Merced|US|37.30|-120.48|2',
	  CKD: 'Crooked Creek|US|61.87|-158.10|',
	  ANX: 'Andenes|NO|69.32|16.13|2',
	  PTH: 'Port Heiden|US|56.95|-158.63|2',
	  YNS: 'Nemiscau|CA|49.74|49.74|',
	  MNI: 'Montserrat|MS|16.75|-62.23|2',
	  YPX: 'Puvirnituq|CA|60.03|-77.28|2',
	  MME: 'Teesside|GB|54.52|-1.42|2',
	  JSR: 'Jessore|BD|23.17|89.22|2',
	  PIN: 'Parintins|BR|-2.63|-56.74|2',
	  REU: 'Reus|ES|41.16|1.11|2',
	  JMS: 'Jamestown|US|46.93|-98.68|2',
	  INA: 'Inta|RU|66.07|60.10|1',
	  SMK: 'St Michael|US|63.48|-162.03|2',
	  JSI: 'Skiathos|GR|39.17|23.48|2',
	  LIW: 'Loikaw|MM|19.69|97.22|',
	  WIN: 'Winton|AU|-36.52|146.08|2',
	  SGU: 'St George|US|37.09|-113.59|2',
	  ULY: 'Ulyanovsk|RU|54.33|48.40|2',
	  KPN: 'Kipnuk|US|59.94|-164.04|2',
	  CCP: 'Concepcion|CL|-36.83|-73.05|2',
	  KWK: 'Kwigillingok|US|59.86|-163.13|2',
	  MRO: 'Masterton|NZ|-40.96|175.66|2',
	  ISA: 'Mount Isa|AU|-20.73|139.50|2',
	  LWN: 'Leninakan|AM|40.72|43.83|2',
	  JNZ: 'Jinzhou|CN|41.11|121.14|2',
	  SVL: 'Savonlinna|FI|61.87|28.88|1',
	  MIM: 'Merimbula|AU|-36.90|149.90|2',
	  ELS: 'East London|ZA|-33.02|27.91|2',
	  SOW: 'Show Low|US|34.25|-110.03|2',
	  BRO: 'Brownsville|US|25.90|-97.50|2',
	  GTZ: 'Grumeti|TZ|2',
	  RAP: 'Rapid City|US|44.08|-103.23|2',
	  HEK: 'Heihe|CN|50.22|127.43|',
	  MKL: 'Jackson|US|35.60|-88.92|2',
	  LEV: 'Levuka|FJ|-17.65|177.27|2',
	  ULZ: 'Uliastai|MN|47.75|96.85|',
	  OGN: 'Yonaguni|JP|24.45|123.00|2',
	  YFB: 'Iqaluit|CA|63.75|-68.51|2',
	  NAJ: 'Nakhichevan|AZ|39.19|45.46|2',
	  WNP: 'Naga|PH|13.59|123.27|',
	  YPM: 'St Pierre|CA|51.82|-94.00|2',
	  ORF: 'Norfolk|US|36.90|-76.20|2',
	  CGB: 'Cuiaba|BR|-15.60|-56.10|2',
	  BLA: 'Barcelona|VE|10.11|-64.69|2',
	  NEG: 'Negril|JM|18.27|-78.35|2',
	  AOC: 'Altenburg|DE|50.98|12.45|1',
	  PCL: 'Pucallpa|PE|-8.38|-74.55|2',
	  STD: 'Santo Domingo|VE|7.58|-72.07|2',
	  DUM: 'Dumai|ID|1.58|101.47|',
	  BTH: 'Batam|ID|1.08|104.05|2',
	  CRP: 'Corpus Christi|US|27.80|-97.40|2',
	  PTP: 'Pointe A Pitre|GP|16.27|-61.53|2',
	  RSJ: 'Rosario|US|48.63|-122.85|',
	  LRD: 'Laredo|US|27.51|-99.51|2',
	  DRG: 'Deering|US|43.07|-71.84|2',
	  ASM: 'Asmara|ER|15.33|38.93|2',
	  TAG: 'Tagbilaran|PH|9.65|123.86|2',
	  PBG: 'Plattsburgh|US|44.70|-73.48|',
	  YZZ: 'Trail|CA|49.10|-117.70|2',
	  NYI: 'Sunyani|GH|7.33|-2.33|2',
	  JGA: 'Jamnagar|IN|22.47|70.07|2',
	  CGO: 'Zhengzhou|CN|34.76|113.65|2',
	  KQA: 'Akutan|US|54.14|-165.77|2',
	  KVC: 'King Cove|US|55.06|-162.31|2',
	  PVA: 'Providencia|CO|13.35|-81.35|',
	  NYK: 'Nanyuki|KE|0.02|37.07|2',
	  RBY: 'Ruby|US|34.75|-80.18|2',
	  PZH: 'Zhob|PK|31.34|69.45|2',
	  CUF: 'Cuneo|IT|44.40|7.54|2',
	  ZEL: 'Bella Bella|CA|52.17|-128.10|',
	  XMN: 'Xiamen|CN|24.47|118.09|2',
	  NAH: 'Naha|ID|3.72|125.37|2',
	  WBM: 'Wapenamanda|PG|-5.63|143.92|2',
	  PER: 'Perth|AU|-31.93|115.83|2',
	  HHZ: 'Hikueru|PF|-17.53|-142.53|',
	  PEW: 'Peshawar|PK|34.01|71.57|2',
	  ENE: 'Ende|ID|-8.83|121.65|2',
	  VUS: 'Velikij Ustyug|RU|60.77|46.32|1',
	  KGA: 'Kananga|CD|-5.90|22.42|2',
	  PPW: 'Papa Westray|GB|59.35|-2.90|2',
	  THR: 'Teheran|IR|35.67|51.42|2',
	  BAZ: 'Barbelos|BR|-0.97|-62.93|',
	  IAN: 'Kiana|US|66.97|-160.42|2',
	  RGL: 'Rio Gallegos|AR|-51.63|-69.22|2',
	  CTM: 'Chetumal|MX|18.50|-88.30|2',
	  'INT': 'Winston-Salem|US|36.10|-80.24|1',
	  RTA: 'Rotuma Island|FJ|-12.50|177.08|2',
	  HLH: 'Ulanhot|CN|46.19|122.00|',
	  GSO: 'Greensboro|US|36.07|-79.79|2',
	  EAE: 'Emae|VU|-17.17|168.42|',
	  TCO: 'Tumaco|CO|1.82|-78.77|',
	  VUP: 'Valleduper|CO|10.44|-73.25|',
	  ARC: 'Arctic Village|US|68.14|-145.52|',
	  AXA: 'Anguilla|AI|18.22|-63.05|2',
	  KJI: '|CN|',
	  HUU: 'Huanuco|PE|-9.92|-76.23|2',
	  FPO: 'Freeport|BS|26.53|-78.70|2',
	  KEP: 'Nepalganj|NP|28.13|81.58|',
	  TZX: 'Trabzon|TR|41.01|39.73|2',
	  ELU: 'El Oued|DZ|33.33|6.88|2',
	  QPA: 'Padova|IT|45.42|11.88|1',
	  BLI: 'Bellingham|US|48.76|-122.49|2',
	  PIS: 'Poitiers|FR|46.58|0.33|2',
	  BQS: 'Blagoveshchensk|RU|50.27|127.53|2',
	  GLN: 'Goulimime|MA|29.02|-10.07|',
	  SFN: 'Santa Fe|AR|-31.63|-60.70|2',
	  LNK: 'Lincoln|US|40.80|-96.67|2',
	  BIS: 'Bismarck|US|46.81|-100.78|2',
	  KOI: 'Kirkwall|GB|58.97|-2.95|2',
	  ATC: 'Arthurs Town|BS|24.63|-75.67|2',
	  CUZ: 'Cuzco|PE|-13.52|-71.98|2',
	  JIQ: 'Zhoubai Town|CN|29.52|29.52|2',
	  FRW: 'Francistown|BW|-21.17|27.52|2',
	  PNS: 'Pensacola|US|30.42|-87.22|2',
	  NNX: 'Nunukan|ID|4.15|117.65|2',
	  AUS: 'Austin|US|30.27|-97.74|2',
	  KUU: 'Kulu|IN|31.98|77.10|2',
	  IRM: '|RU|',
	  IVC: 'Invercargill|NZ|-46.40|168.35|2',
	  SFG: 'St Martin|MF|18.05|-63.12|2',
	  DIY: 'Diyarbai|TR|37.92|40.21|2',
	  JDR: '|BR|',
	  FSD: 'Sioux Falls|US|43.55|-96.70|2',
	  PDL: 'Ponta Delgada|PT|37.73|-25.67|2',
	  LPL: 'Liverpool|GB|53.42|-3.00|2',
	  TOE: 'Tozeur|TN|33.92|8.13|2',
	  TEX: 'Telluride|US|37.94|-107.81|2',
	  KUH: 'Kushiro|JP|42.98|144.38|2',
	  KVG: 'Kavieng|PG|-2.57|150.80|2',
	  DRW: 'Darwin|AU|-12.46|130.84|2',
	  AGH: 'Angelholm|SE|56.25|12.87|2',
	  LBB: 'Lubbock|US|33.58|-101.86|2',
	  RZP: 'Taytay Sandoval|PH|11.05|121.94|2',
	  AGU: 'Aguascalientes|MX|21.88|-102.30|2',
	  DYG: 'Dayong|CN|29.13|110.48|2',
	  TWB: 'Toowoomba|AU|-27.55|151.97|2',
	  GER: 'Nueva Gerona|CU|21.88|-82.80|2',
	  CQD: 'Shahre Kord|IR|32.30|50.84|2',
	  LSH: 'Lashio|MM|22.93|97.75|2',
	  MLI: 'Moline|US|41.51|-90.52|2',
	  YRT: 'Rankin Inlet|CA|62.82|-92.08|2',
	  LMM: 'Los Mochis|MX|25.77|-108.97|2',
	  SKN: 'Stokmarknes|NO|68.56|14.91|2',
	  BEW: 'Beira|MZ|-19.84|34.84|2',
	  SMA: 'Santa Maria|PT|36.97|-25.10|2',
	  MSE: 'Manston|GB|50.95|-2.27|2',
	  ABD: 'Abadan|IR|30.35|48.30|2',
	  TOS: 'Tromso|NO|69.67|18.97|2',
	  TKP: 'Takapoto|PF|-15.45|-145.14|',
	  HMB: '|EG|',
	  FLS: 'Flinder Island|AU|-40.09|148.00|2',
	  TFF: 'Tefe|BR|-3.35|-64.71|2',
	  REX: 'Reynosa|MX|26.08|-98.28|2',
	  JIU: 'Jiujiang|CN|29.73|115.98|2',
	  JUL: 'Juliaca|PE|-15.50|-70.13|2',
	  PDA: 'Puerto Inirida|CO|3.88|-67.88|',
	  RKS: 'Rock Springs|US|41.59|-109.20|2',
	  OMA: 'Omaha|US|41.26|-95.94|2',
	  ABV: 'Abuja|NG|9.08|7.53|2',
	  OOK: 'Toksook Bay|US|60.53|-165.10|2',
	  GEL: 'Santo Angelo|BR|-28.30|-54.26|2',
	  BCX: 'Beloreck|RU|53.93|58.33|1',
	  LWE: 'Lewoleba|ID|-8.50|123.37|',
	  AKS: 'Akui|SB|-8.70|160.68|2',
	  QRO: 'Queretaro|MX|20.60|-100.38|2',
	  URJ: 'Uraj|RU|60.12|64.83|2',
	  BUL: 'Bulolo|PG|-7.20|146.65|2',
	  LLK: 'Lankaran|AZ|38.76|48.84|',
	  LUA: 'Lukla|NP|27.70|86.72|2',
	  CEB: 'Cebu|PH|10.31|123.89|2',
	  DRS: 'Dresden|DE|51.05|13.74|2',
	  PIF: 'Pingtung|TW|22.71|120.49|',
	  QYI: 'Hilversum|NL|52.22|5.18|1',
	  ZLT: 'La Tabatiere|CA|51.00|-59.00|2',
	  KWE: 'Guiyang|CN|26.58|106.72|2',
	  UKK: 'Ust Kamenogorsk|KZ|49.98|82.61|2',
	  LUQ: 'San Luis|AR|-33.30|-66.35|2',
	  HAC: 'Hachijo Jima|JP|33.11|139.79|2',
	  DBA: 'Dalbandin|PK|28.88|64.42|',
	  LEC: 'Lencois|BR|-13.82|-41.72|2',
	  PUB: 'Pueblo|US|38.25|-104.61|2',
	  YTF: 'Alma|CA|48.53|-71.67|2',
	  TKG: 'Bandar Lampung|ID|-5.45|105.27|2',
	  YBB: 'Pelly Bay|CA|68.88|-90.08|2',
	  AGX: 'Agatti Island|IN|10.83|72.18|',
	  BGO: 'Bergen|NO|60.39|5.32|2',
	  VIN: 'Vinnitsa|UA|49.23|28.48|1',
	  TBO: 'Tabora|TZ|-5.07|32.84|',
	  MRE: 'Mara Lodges|KE|-1.18|35.08|2',
	  RBQ: 'Rurrenabaque|BO|-14.44|-67.53|2',
	  PEI: 'Pereira|CO|4.81|-75.70|2',
	  ENA: 'Kenai|US|60.55|-151.26|2',
	  IAM: 'In Amenas|DZ|28.05|9.64|2',
	  BLL: 'Billund|DK|55.73|9.12|2',
	  YYY: 'Mont Joli|CA|50.08|-61.68|2',
	  YFS: 'Fort Simpson|CA|61.86|-121.35|2',
	  UCT: 'Ukhta|RU|63.57|53.80|2',
	  AHN: 'Athens|US|33.95|-83.33|2',
	  MQT: 'Marquette|US|46.54|-87.40|2',
	  JOE: 'Joensuu|FI|62.60|29.77|2',
	  GYA: 'Guayaramerin|BO|-10.80|-65.38|2',
	  DAY: 'Dayton|US|39.76|-84.19|2',
	  LAI: 'Lannion|FR|48.73|-3.47|2',
	  CFK: 'Chlef|DZ|36.21|1.33|',
	  AIT: 'Aitutaki|CK|-18.86|-159.79|2',
	  MYG: 'Mayaguana|BS|22.38|-72.95|2',
	  MEH: 'Mehamn|NO|71.03|27.85|2',
	  AST: 'Astoria|US|46.19|-123.83|2',
	  LPP: 'Lappeenranta|FI|61.07|28.18|2',
	  KMQ: 'Komatsu|JP|36.40|136.45|2',
	  TNA: 'Jinan|CN|36.67|117.00|2',
	  HVN: 'New Haven|US|41.31|-72.93|2',
	  YTS: 'Timmins|CA|48.47|-81.33|2',
	  DYU: 'Dushanbe|TJ|38.56|68.77|2',
	  JAU: 'Jauja|PE|-11.75|-75.25|',
	  GRW: 'Graciosa Island|PT|39.05|-28.05|2',
	  AKB: 'Atka|US|52.22|-174.20|',
	  WLK: 'Selawik|US|66.60|-160.01|2',
	  AWZ: 'Ahwaz|IR|31.33|48.69|2',
	  PPN: 'Popayan|CO|8.67|-76.21|2',
	  ASW: 'Aswan|EG|24.09|32.90|2',
	  OUD: 'Oujda|MA|34.68|-1.91|2',
	  ADA: 'Adana|TR|37.00|35.33|2',
	  TIF: 'Taif|SA|21.27|40.42|2',
	  MSR: 'Mus Tr|TR|38.73|41.63|2',
	  BTM: 'Butte|US|46.00|-112.53|2',
	  ZKE: 'Kaschechewan|CA|52.33|-81.33|',
	  LFM: 'Lamerd|IR|27.37|53.19|',
	  YHD: 'Dryden|CA|49.78|-92.75|2',
	  ERL: 'Erldunda|CN|-25.25|133.20|',
	  ALB: 'Albany|US|42.75|-73.81|2',
	  YKF: 'Kitchener|CA|43.45|-80.48|2',
	  BZN: 'Bozeman|US|45.68|-111.04|2',
	  NVR: 'Novgorod|RU|58.52|31.28|1',
	  YTQ: 'Tasiujaq|CA|58.70|-69.93|2',
	  YAX: 'Angling Lake|CA|53.25|-89.56|',
	  VKT: '|RU|67.48|63.98|1',
	  MAF: 'Midland|US|32.00|-102.08|2',
	  ROC: 'Rochester|US|43.12|-77.67|2',
	  TMW: 'Tamworth|AU|-31.10|150.93|2',
	  LUM: 'Luxi|CN|24.41|98.53|',
	  VCS: 'Con Dao|VN|10.37|106.63|',
	  LII: 'Mulia|ID|-2.33|106.27|1',
	  BOY: 'Bobo Dioulasso|BF|11.20|-4.30|2',
	  CDB: 'Cold Bay|US|55.19|-162.72|2',
	  FSM: 'Ft Smith|US|35.34|-94.37|2',
	  YFC: 'Fredericton|CA|45.95|-66.67|2',
	  VLN: 'Valencia|VE|10.18|-68.00|2',
	  KUV: 'Kunsan|KR|35.98|126.71|2',
	  PIR: 'Pierre|US|44.37|-100.35|2',
	  BBO: 'Berbera|SO|10.44|45.02|2',
	  ERN: 'Eirunepe|BR|-6.58|-69.88|',
	  JDZ: 'Jingdezhen|CN|29.29|117.20|2',
	  NSH: 'Now Shahr|IR|36.67|51.50|',
	  CMH: 'Columbus|US|39.99|-82.88|2',
	  APK: 'Apataki|PF|-15.42|-146.08|2',
	  MEI: 'Meridian|US|43.61|-116.39|2',
	  TXN: 'Tunxi|CN|29.71|118.31|2',
	  NAO: 'Nanchong|CN|30.80|106.07|',
	  HMA: 'Khanty Mansiysk|RU|61.00|69.00|2',
	  PAS: 'Paros|GR|37.08|25.15|2',
	  BMU: 'Bima|ID|-8.47|118.72|2',
	  RHO: 'Rhodes|GR|36.44|28.22|2',
	  VCL: 'Tamky|VN|15.42|108.70|2',
	  IOS: 'Ilheus|BR|-14.79|-39.05|2',
	  VDB: 'Fagernes|NO|60.98|9.25|2',
	  AVN: 'Avignon|FR|43.95|4.82|2',
	  NOU: 'Noumea|NC|-22.27|166.45|2',
	  YQT: 'Thunder Bay|CA|48.40|-89.32|2',
	  ACI: 'Alderney|GB|49.73|-2.25|2',
	  NAG: 'Nagpur|IN|21.15|79.10|2',
	  PNZ: 'Petrolina|BR|-9.40|-40.50|2',
	  ENH: 'Enshi|CN|30.32|109.48|',
	  MDZ: 'Mendoza|AR|-32.88|-68.82|2',
	  FUJ: 'Fukue|JP|32.69|128.84|2',
	  CGR: 'Campogrande|BR|-20.46|-54.67|2',
	  MWA: 'Marion|US|37.75|-89.02|2',
	  ALO: 'Waterloo|US|42.56|-92.40|2',
	  KRS: 'Kristiansand|NO|58.20|8.08|2',
	  PMZ: 'Palmar|CR|8.95|-83.47|',
	  PVC: 'Provincetown|US|42.06|-70.18|2',
	  JHW: 'Jamestown|US|42.15|-79.26|2',
	  NGK: '|RU|0.00|0.00|0',
	  GOP: 'Gorakhpur|IN|29.45|75.68|2',
	  FRO: 'Floro|NO|61.60|5.00|2',
	  AUW: 'Wausau|US|44.96|-89.63|2',
	  JNG: 'Jining|CN|35.42|116.53|',
	  YNP: 'Natuashish|CA|55.91|-61.18|2',
	  COS: 'Colorado Springs|US|38.83|-104.82|2',
	  FEN: 'Fernando De Noronha|BR|-3.85|-32.42|2',
	  IXR: 'Ranchi|IN|23.35|85.33|2',
	  JWN: 'Zanjan|IR|36.67|48.48|2',
	  EIE: 'Eniseysk|RU|58.47|92.12|1',
	  YFA: 'Fort Albany|CA|52.24|-81.59|',
	  LEA: 'Learmonth|AU|-37.42|143.73|2',
	  VQS: 'Vieques|PR|18.11|-65.42|2',
	  MXV: 'Moron|MN|49.63|100.17|',
	  KSY: 'Kars|TR|40.61|43.10|2',
	  HGH: 'Hangzhou|CN|30.26|120.17|2',
	  WBB: 'Stebbins|US|63.52|-162.29|2',
	  RCE: 'Roche Harbor|US|48.61|-123.17|2',
	  KAL: 'Kaltag|US|64.33|-158.72|2',
	  DCN: '|AU|',
	  JPA: 'Joao Pessoa|BR|-7.12|-34.86|2',
	  JHG: 'Jinghong|CN|21.98|100.82|2',
	  KLG: 'Kalskag|US|61.54|-160.31|2',
	  MCG: 'Mcgrath|US|62.96|-155.60|2',
	  KGL: 'Kigali|RW|-1.95|30.06|2',
	  VII: 'Vinh City|VN|18.70|105.63|2',
	  GRX: 'Granada|ES|37.19|-3.61|2',
	  MGB: 'Mt Gambier|AU|-37.75|140.78|2',
	  ELC: 'Elcho|AU|-12.02|135.57|2',
	  RES: 'Resistencia|AR|-27.45|-58.98|2',
	  PNA: 'Pamplona|ES|42.82|-1.64|2',
	  RUM: 'Rumjatar|NP|27.32|86.53|',
	  RIA: 'Santa Maria|BR|-29.71|-53.69|2',
	  PDB: 'Pedro Bay|US|59.78|-154.12|',
	  OTH: 'North Bend|US|43.41|-124.22|2',
	  PQC: 'Phu Quoc|VN|10.22|103.97|2',
	  YAB: 'Arctic Bay|CA|73.03|-85.18|2',
	  VGO: 'Vigo|ES|42.23|-8.72|2',
	  DWD: '|SA|',
	  MHD: 'Mashad|IR|36.30|59.61|2',
	  HUH: 'Huahine|PF|-16.75|-151.00|2',
	  NSK: '|RU|69.33|88.22|2',
	  MMZ: 'Maimana|AF|35.92|64.73|',
	  PYX: 'Pattaya|TH|12.68|101.02|1',
	  PML: 'Port Moller|US|55.99|-160.58|2',
	  GRO: 'Gerona|ES|41.98|2.82|2',
	  DSN: 'Dongsheng|CN|39.85|110.03|',
	  ABY: 'Albany|US|31.53|-84.20|2',
	  FLL: 'Ft Lauderdale|US|26.07|-80.15|2',
	  WUZ: 'Wuzhou|CN|23.46|111.25|',
	  KVA: 'Kavalla|GR|40.97|24.34|2',
	  PEU: 'Puerto Lempira|HN|15.22|-83.77|',
	  CJL: 'Chitral|PK|34.21|71.83|2',
	  FLA: 'Florencia|CO|1.59|-75.56|2',
	  KTE: 'Kerteh|MY|4.52|103.45|2',
	  YQR: 'Regina|CA|50.45|-104.62|2',
	  FUN: 'Funafuti|TV|-8.52|179.22|2',
	  MLU: 'Monroe|US|32.51|-92.12|2',
	  RCB: 'Richards Bay|ZA|-28.80|32.10|2',
	  HRB: 'Harbin|CN|45.75|126.65|2',
	  YSB: 'Sudbury|CA|46.49|-80.99|2',
	  NEV: 'Nevis|KN|17.15|-62.58|2',
	  SAP: 'San Pedro Sula|HN|15.50|-88.03|2',
	  KMG: 'Kunming|CN|25.04|102.72|1',
	  MOQ: 'Morondava|MG|-20.28|44.28|2',
	  KUA: 'Kuantan|MY|3.80|103.33|2',
	  YFO: 'Flin Flon|CA|54.77|-101.86|2',
	  PKK: 'Pakokku|MM|21.34|95.11|',
	  LKH: 'Long Akah|MY|3.32|114.78|2',
	  YOJ: 'High Level|CA|58.52|-117.14|2',
	  GYL: 'Argyle|AU|-16.33|128.67|',
	  BNS: 'Barinas|VE|8.62|-70.22|2',
	  ASD: 'Andros Town|BS|24.70|-77.77|2',
	  CBQ: 'Calabar|NG|4.95|8.32|2',
	  YCG: 'Castlegar|CA|49.30|-117.67|2',
	  TSR: 'Timisoara|RO|45.75|21.23|2',
	  OGL: 'Ogle|GY|6.80|-58.11|2',
	  CFN: 'Donegal|IE|54.65|-8.12|2',
	  TUK: 'Turbat|PK|26.00|63.05|2',
	  SGO: 'St George|AU|-28.05|148.59|2',
	  BOC: 'Bocas Del Toro|PA|9.33|-82.25|2',
	  KNU: 'Kanpur|IN|26.47|80.35|2',
	  MOB: 'Mobile|US|30.69|-88.04|2',
	  FOC: 'Fuzhou|CN|26.06|119.31|2',
	  KHH: 'Kaohsiung|TW|22.63|120.32|2',
	  DCM: 'Castres|FR|43.60|2.25|2',
	  MRZ: 'Moree|AU|-29.47|149.85|2'
	};

/***/ },
/* 31 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports["default"] = {
	  LOP: 'Прая',
	  GBB: 'Габала',
	  MOW: 'Москва',
	  GZP: 'Алания',
	  LON: 'Лондон',
	  PAR: 'Париж',
	  MAD: 'Мадрид',
	  IEV: 'Киев',
	  NYC: 'Нью-Йорк',
	  AMS: 'Амстердам',
	  LED: 'Санкт-Петербург',
	  NCE: 'Ницца',
	  GVA: 'Женева',
	  PRG: 'Прага',
	  ACE: 'Ланцарот',
	  MUC: 'Мюнхен',
	  BER: 'Берлин',
	  IST: 'Стамбул',
	  ROV: 'Ростов-на-Дону',
	  BCN: 'Барселона',
	  PKC: 'Петропавловск-Камчатский',
	  CLJ: 'Клуж',
	  TLV: 'Тель-Авив',
	  KIV: 'Кишинев',
	  VIE: 'Вена',
	  BEG: 'Белград',
	  ZRH: 'Цюрих',
	  LIS: 'Лиссабон',
	  FRA: 'Франкфурт',
	  HEL: 'Хельсинки',
	  DUS: 'Дюссельдорф',
	  OSL: 'Осло',
	  LCA: 'Ларнака',
	  PMI: 'Пальма Майорка',
	  BRU: 'Брюссель',
	  LAX: 'Лос-Анджелес',
	  BUD: 'Будапешт',
	  RIX: 'Рига',
	  ZAG: 'Загреб',
	  WAW: 'Варшава',
	  ODS: 'Одесса',
	  NUE: 'Нюрнберг',
	  LJU: 'Любляна',
	  CPH: 'Копенгаген',
	  BKK: 'Бангкок',
	  HAV: 'Гавана',
	  LUX: 'Люксембург',
	  SFO: 'Сан-Франциско',
	  DUB: 'Дублин',
	  AER: 'Сочи',
	  LDY: 'Лондондерри',
	  ROM: 'Рим',
	  BUH: 'Бухарест',
	  CBR: 'Канберра',
	  CAI: 'Каир',
	  MAN: 'Манчестер',
	  KUF: 'Самара',
	  AYT: 'Анталия',
	  ATH: 'Афины',
	  FAO: 'Фаро',
	  SIP: 'Симферополь',
	  SVX: 'Екатеринбург',
	  SCL: 'Сантьяго',
	  STO: 'Стокгольм',
	  PIZ: 'Поинт Лей',
	  SYD: 'Сидней',
	  HOU: 'Хьюстон',
	  DEL: 'Дели',
	  DXB: 'Дубаи',
	  DPS: 'Денпасар Бали',
	  ALG: 'Алжир',
	  BPN: 'Баликпапан',
	  BOM: 'Мумбаи',
	  UTP: 'Паттайа',
	  HKG: 'Гонконг',
	  ALA: 'Алматы',
	  MRS: 'Марсель',
	  KGD: 'Калининград',
	  VCE: 'Венеция',
	  VLC: 'Валенсия',
	  SSH: 'Шарм-эль-Шейх',
	  OPO: 'Порто',
	  ORN: 'Оран',
	  TIP: 'Триполи',
	  OVB: 'Новосибирск',
	  CHI: 'Чикаго',
	  GOI: 'Гоа',
	  MJV: 'Мурсия',
	  TIV: 'Тиват',
	  OMS: 'Омск',
	  SIN: 'Сингапур',
	  VVO: 'Владивосток',
	  TGD: 'Подгорица',
	  CUN: 'Канкун',
	  VNO: 'Вильнюс',
	  MIA: 'Майами',
	  EDI: 'Эдинбург',
	  TSE: 'Астана',
	  MLA: 'Мальта',
	  TYO: 'Токио',
	  DOK: 'Донецк',
	  MIL: 'Милан',
	  MEX: 'Мехико',
	  HAM: 'Гамбург',
	  KLV: 'Карловы-Вары',
	  CEK: 'Челябинск',
	  KRR: 'Краснодар',
	  HRG: 'Хургада',
	  LIM: 'Лима',
	  UFA: 'Уфа',
	  BOS: 'Бостон',
	  UIO: 'Кито',
	  SKG: 'Салоники',
	  MSQ: 'Минск',
	  JKT: 'Джакарта',
	  ATL: 'Атланта',
	  KZN: 'Казань',
	  CAS: 'Касабланка',
	  GLA: 'Глазго',
	  LAS: 'Лас-Вегас',
	  CGN: 'Кельн',
	  PEE: 'Пермь',
	  HNL: 'Гонолулу',
	  GOJ: 'Нижний Новгород',
	  PHL: 'Филадельфия',
	  BEY: 'Бейрут',
	  BLQ: 'Болонья',
	  VOG: 'Волгоград',
	  LYS: 'Лион',
	  ETH: 'Эйлат',
	  NAS: 'Нассау',
	  EWR: 'Ньюарк',
	  BJS: 'Пекин',
	  MLE: 'Мале',
	  MNL: 'Манила',
	  CMB: 'Коломбо',
	  KUL: 'Куала-Лумпур',
	  YTO: 'Торонто',
	  STL: 'Сент-Луис',
	  ORK: 'Корк',
	  CVG: 'Цинциннати',
	  BFS: 'Белфаст',
	  MKE: 'Милуоки',
	  BWI: 'Балтимор',
	  MBJ: 'Монтего Бэй',
	  MEM: 'Мемфис',
	  CLT: 'Шарлотт',
	  RDU: 'Рэли',
	  ABZ: 'Абердин',
	  CLE: 'Кливленд',
	  WAS: 'Вашингтон',
	  NCL: 'Ньюкасл',
	  PFO: 'Пафос',
	  TAS: 'Ташкент',
	  REK: 'Рейкьявик',
	  ARH: 'Архангельск',
	  LWO: 'Львов',
	  ADL: 'Аделаида',
	  MSY: 'Новый Орлеан',
	  AUH: 'Абу-Даби',
	  SOF: 'София',
	  BTS: 'Братислава',
	  VRN: 'Верона',
	  HAN: 'Ханой',
	  CAG: 'Кальяри',
	  TJM: 'Тюмень',
	  FRU: 'Бишкек',
	  MEL: 'Мельбурн',
	  PWQ: 'Павлодар',
	  WLG: 'Веллингтон',
	  RIO: 'Рио-де-Жанейро',
	  ANK: 'Анкара',
	  IKT: 'Иркутск',
	  HKT: 'Пхукет',
	  KEJ: 'Кемерово',
	  MTY: 'Монтеррей',
	  KIN: 'Кингстон',
	  ALC: 'Аликанте',
	  SEZ: 'Махе-Айленд',
	  DTT: 'Детройт',
	  ANC: 'Анкоридж',
	  CYS: 'Чейнни',
	  SDQ: 'Санто-Доминго',
	  GNB: 'Гренобль',
	  YMQ: 'Монреаль',
	  TRN: 'Турин',
	  BRI: 'Бари',
	  MRV: 'Минеральные Воды',
	  MRU: 'Маврикий',
	  IBZ: 'Ибица',
	  SEL: 'Сеул',
	  ULN: 'Улан-Батор',
	  RTW: 'Саратов',
	  MJF: 'Мосьоэн',
	  TBS: 'Тбилиси',
	  NBO: 'Найроби',
	  HER: 'Ираклион',
	  OMO: 'Мостар',
	  BHX: 'Бирмингем',
	  DEN: 'Денвер',
	  SKP: 'Скопье',
	  RMF: 'Марса-Алам',
	  YVR: 'Ванкувер',
	  IPC: 'Истер-Айленд',
	  PSG: 'Петерсбург',
	  HFA: 'Хайфа',
	  TLN: 'Тулон',
	  KTT: 'Киттила',
	  TCI: 'Тенерифе',
	  SCU: 'Сантьяго',
	  BNE: 'Брисбен',
	  OLB: 'Олбия',
	  IXL: 'Лех Индия',
	  FNC: 'Фунчал',
	  SHA: 'Шанхай',
	  ASF: 'Астрахань',
	  KXK: 'Комсомольск-на-Амуре',
	  TOF: 'Томск',
	  RTM: 'Роттердам',
	  BGI: 'Бриджтаун',
	  VAR: 'Варна',
	  CCS: 'Каракас',
	  HRK: 'Харьков',
	  PES: 'Петрозаводск',
	  BOD: 'Бордо',
	  TLL: 'Таллин',
	  MIR: 'Монастир',
	  KJA: 'Красноярск',
	  ADK: 'Адак-Айленд',
	  VRA: 'Варадеро',
	  DLM: 'Даламан',
	  CIX: 'Чиклайо',
	  INN: 'Инсбрук',
	  DFW: 'Даллас',
	  YXU: 'Лондон',
	  LXA: 'Лхаса',
	  AAR: 'Орхус',
	  USM: 'Кох-Самуи',
	  SVQ: 'Севилья',
	  TIA: 'Тирана',
	  PHT: 'Париж',
	  HAJ: 'Ганновер',
	  LTT: 'Сан-Тропез',
	  BXN: 'Бодрум',
	  NJC: 'Нижневартовск',
	  SZG: 'Зальцбург',
	  BTK: 'Братск',
	  NTE: 'Нант',
	  KGF: 'Караганда',
	  RNS: 'Ренн',
	  SEA: 'Сиэтл',
	  PRN: 'Приштина',
	  SPU: 'Сплит',
	  MCM: 'Монте-Карло',
	  BOJ: 'Бургас',
	  SID: 'Сал о-в',
	  VSG: 'Луганск',
	  AKL: 'Окленд',
	  EBB: 'Энтеббе',
	  KTM: 'Катманду',
	  PUJ: 'Пунта-Кана',
	  DBV: 'Дубровник',
	  UUD: 'Улан-Удэ',
	  BRN: 'Берн',
	  ASU: 'Асунсьон',
	  CPO: 'Копьяпо',
	  MSN: 'Мадисон',
	  ABJ: 'Абиджан',
	  PUY: 'Пула',
	  EVN: 'Ереван',
	  DAC: 'Дакка',
	  HMO: 'Хермосилльо',
	  EGS: 'Эгилсстадир',
	  KHV: 'Хабаровск',
	  ASE: 'Аспен',
	  HTA: 'Чита',
	  KAB: 'Кариба Дам',
	  STR: 'Штутгарт',
	  MSP: 'Миннеаполис',
	  OHD: 'Охрид',
	  MKC: 'Канзас-Сити',
	  RAK: 'Марракеш',
	  RMI: 'Римини',
	  JKH: 'Хиос',
	  RPN: 'Рош Пина',
	  ASP: 'Элис Спрингз',
	  DOM: 'Доминика',
	  MBA: 'Момбаса',
	  DNK: 'Днепропетровск',
	  LXR: 'Луксор',
	  LHE: 'Лахор',
	  TOB: 'Тобрук',
	  LEI: 'Альмерия',
	  PTG: 'Питерсбург',
	  ASB: 'Ашхабад',
	  BAH: 'Мухаррак',
	  AMM: 'Амман',
	  KWI: 'Кувейт',
	  MXS: 'Маота',
	  LUG: 'Лугано',
	  IZM: 'Измир',
	  BIO: 'Бильбао',
	  BOG: 'Богота',
	  HOG: 'Ольгин',
	  APW: 'Апиа',
	  IPH: 'Ипох',
	  SAO: 'Сан-Паоло',
	  TRV: 'Тируванантапурам',
	  JED: 'Джедда',
	  ZTH: 'Закинтос',
	  MCT: 'Мускат',
	  JMK: 'Миконос',
	  PSA: 'Пиза',
	  ORL: 'Орландо',
	  SBH: 'Сен-Бартельми',
	  ISB: 'Исламабад',
	  CHQ: 'Ханья',
	  MGA: 'Манагуа',
	  RUH: 'Рияд',
	  BOB: 'Бора-Бора',
	  KBR: 'Кота-Бару',
	  PRX: 'Париж',
	  MQF: 'Магнитогорск',
	  DOH: 'Доха',
	  LPA: 'Лас Пальмас',
	  KRK: 'Краков',
	  GON: 'Нью Лондон',
	  CTA: 'Катания',
	  FLN: 'Флорианополис',
	  AAH: 'Ахен',
	  ECN: 'Эркан',
	  BLR: 'Бангалор',
	  PRI: 'Праслин-Айленд',
	  STI: 'Сантьяго',
	  LTQ: 'Ле Туке',
	  SFT: 'Скеллефти',
	  CFG: 'Сиенфуэгос',
	  ARM: 'Армидейл',
	  HUY: 'Хамберсайд',
	  AUA: 'Аруба',
	  CUE: 'Куенка',
	  AAQ: 'Анапа',
	  DHN: 'Дотан',
	  ITO: 'Хило',
	  DAM: 'Дамаск',
	  LEH: 'Гавр',
	  DMM: 'Даммам',
	  PIT: 'Питсбург',
	  NAP: 'Неаполь',
	  TVL: 'Лейк-Тахо',
	  ZQN: 'Куинстаун',
	  DTM: 'Дортмунд',
	  KHI: 'Карачи',
	  CPT: 'Кейптаун',
	  KUN: 'Каунас',
	  BUS: 'Батуми',
	  MOV: 'Моранба',
	  CFU: 'Керкира',
	  OSA: 'Осака',
	  DAV: 'Давид',
	  BUE: 'Буэнос-Айрес',
	  MMK: 'Мурманск',
	  IXB: 'Багдогра',
	  PNH: 'Пномпень',
	  JAI: 'Джайпур',
	  NGO: 'Нагоя',
	  MSU: 'Масеру',
	  SGC: 'Сургут',
	  PZB: 'Питермарицбург',
	  IXM: 'Мадураи',
	  MVF: 'Моссоро',
	  UUS: 'Южно-Сахалинск',
	  CNM: 'Карлсбад',
	  KSW: 'Кирьят-Шмона',
	  PKV: 'Псков',
	  NGE: 'Нгаундере',
	  SJO: 'Сан-Хосе',
	  USH: 'Ушуайа',
	  CGP: 'Читтагонг',
	  ESS: 'Эссен',
	  SUV: 'Сува',
	  TPE: 'Тайбэй',
	  AUX: 'Арагуэйна',
	  YEK: 'Ариват',
	  SDY: 'Сидней',
	  DNZ: 'Денизли',
	  SQO: 'Сторуман',
	  ASR: 'Кайсери',
	  MMA: 'Мальме',
	  PDX: 'Портленд',
	  FKL: 'Франклин',
	  SGN: 'Хошимин',
	  MSS: 'Массена',
	  FKB: 'Карлсруэ Баден-Баден',
	  JTY: 'Астипалайя о-в',
	  PZE: 'Пензанс',
	  ZAD: 'Задар',
	  YQB: 'Квебек',
	  MAG: 'Маданг',
	  BSB: 'Бразилиа',
	  TAL: 'Танана',
	  PUW: 'Пуллман',
	  KLO: 'Калибо',
	  OCJ: 'Очо Риос',
	  OSR: 'Острава',
	  BSL: 'Базель',
	  PEZ: 'Пенза',
	  PTY: 'Панама-Сити',
	  AOT: 'Аоста',
	  BAK: 'Баку',
	  LOZ: 'Лондон',
	  SDF: 'Луисвилль',
	  XCR: 'Шалон-сюр-Марн',
	  MFM: 'Макао',
	  MPW: 'Мариуполь',
	  PWM: 'Портленд',
	  REP: 'Сием Рип',
	  PPL: 'Пхаплу',
	  MNZ: 'Манассас',
	  YOW: 'Оттава',
	  IND: 'Индианаполис',
	  AQJ: 'Акаба',
	  ZNE: 'Ньюмен',
	  SQH: 'Шонла',
	  FMN: 'Фармингтон',
	  MOD: 'Модесто',
	  DAD: 'Да-Нанг',
	  DYR: 'Анадырь',
	  HUN: 'Хуалянь',
	  QLS: 'Лозанна',
	  GZM: 'Гозо',
	  FLW: 'Санта-Круз Флорес',
	  PNQ: 'Пуна',
	  PPG: 'Паго-Паго',
	  ETZ: 'Метц Нанси',
	  VXE: 'Сан Висенте',
	  SMI: 'Самос',
	  CHC: 'Кристчерч',
	  AEY: 'Акурейри',
	  VNC: 'Венеция',
	  CMI: 'Шампань',
	  BAX: 'Барнаул',
	  SSX: 'Самсун',
	  DCF: 'Розо',
	  BQJ: 'Батагай',
	  EMA: 'East Midlands',
	  NQT: 'Ноттингем Бр',
	  CDI: 'Кашоейру-де-Итапемирин',
	  MCP: 'Макапа',
	  LSE: 'Ла Кросс',
	  MAS: 'Манус-Айленд',
	  BKO: 'Бамако',
	  VDS: 'Вадсё',
	  AGA: 'Агадир',
	  UWE: 'Висбаден',
	  MFA: 'Mafia',
	  GEW: 'Gewoia',
	  CYF: 'Чефорнак',
	  DIL: 'Дили',
	  MFU: 'Мфуве',
	  ASO: 'Асоса',
	  ENN: 'Nenana',
	  CLW: 'Clearwater',
	  MAT: 'Матади',
	  AGJ: 'Агуни',
	  NEF: 'Нефтекамск',
	  DBM: 'Debremarcos',
	  QIN: 'Мерсин',
	  ZTA: 'Tureira',
	  QAU: 'Бебедуро',
	  ZIH: 'Сихуатанех',
	  ORQ: 'Норуок',
	  JXN: 'Джексон',
	  NSM: 'Норсмен',
	  TLC: 'Толука',
	  PND: 'Пунта-Горда',
	  BMW: 'Bordj Баджи Мохтар',
	  CFR: 'Кан',
	  XBE: 'Bearskin Lake',
	  CKM: 'Кларксдейл',
	  BLK: 'Блэкпул',
	  YRC: '',
	  QEX: 'Emmerich',
	  OLC: '',
	  QLB: 'Lajeado',
	  KSF: 'Кассел',
	  IKP: 'Инкерман',
	  BEN: 'Бенгази',
	  WEX: 'Вексфорд',
	  EBD: 'Эль-Обейд',
	  YVM: 'Qikiqtarjuaq',
	  GLL: 'Гол',
	  TUF: 'Тур',
	  COE: 'Куэ д Ален',
	  VDP: 'Valle De La Pascua',
	  SFC: 'St Francois',
	  KNK: 'Какхонак',
	  MGR: 'Моултри',
	  MTO: 'Маттун',
	  JAV: 'Илуилиссат',
	  PMK: 'Палм Айленд',
	  ACU: 'Ахатупо',
	  DLP: 'Диснейленд Париж',
	  ADQ: 'Кодьяк',
	  SEB: 'Себа',
	  TLA: 'Teller',
	  LEJ: 'Лейпциг',
	  UMY: 'Сумы',
	  BSZ: '',
	  WST: 'Вестерли',
	  MJN: 'Мадзунга',
	  YDL: 'Дис Лэйк',
	  HLF: 'Хултсфред',
	  ODO: '',
	  GSA: 'Сабах',
	  LDB: 'Лондрина',
	  IHO: 'Ihosy',
	  KTA: 'Каррата',
	  NNM: 'Нарьян-Мар',
	  AXR: 'Арутуа',
	  SHC: 'Shire Indaselassie',
	  MED: 'Медина',
	  RZR: 'Рамсар',
	  LWB: 'Льюисбург',
	  GFY: 'Grootfontein',
	  GRY: 'Grimsey',
	  MAA: 'Ченнай',
	  XQB: 'Basingstoke',
	  DUD: 'Дунедин',
	  KMX: 'Хамис-Мушаит',
	  NOM: 'Номад-Ривер',
	  ZES: 'Goeppingen',
	  TMZ: 'Темс',
	  MEP: 'Мерсинг',
	  CBF: 'Каунсил Блаффс',
	  TGT: 'Танга',
	  AXN: 'Александрия',
	  ZWA: 'Andapa',
	  YHB: 'Хадсон-Бей',
	  JBK: 'Беркли',
	  DOG: 'Dongola',
	  MQS: 'Мюстик',
	  ATB: 'Атбара',
	  KCT: '',
	  MWF: 'Маэво',
	  BOE: 'Boundji',
	  KUC: 'Куриа',
	  EAM: 'Неджран',
	  XIO: 'Санкт-Мэрис',
	  YRG: 'Риголет',
	  TLR: 'Туларе',
	  NYA: 'Нягань',
	  TEK: 'Татитлек',
	  SPJ: 'Спарта',
	  ZTD: 'Скенектади',
	  HUT: 'Хатчинсон',
	  PLK: 'Бренсон Поинт Лукаут',
	  FIN: 'Finschhafen',
	  MII: 'Марилиа',
	  ELH: 'Норт Элутера',
	  PCA: 'Portage Creek',
	  DBN: 'Дублин',
	  JKG: 'Йончепинг',
	  GES: 'Женерал Сантос',
	  ZAL: 'Вальдивия',
	  RNG: 'Rangely',
	  STJ: 'Сент Джозеф',
	  PBO: 'Парабурду',
	  YAT: 'Аттавапискат',
	  AGM: 'Tasiilaq',
	  ASI: 'Джоржтаун',
	  CJA: 'Кахамарка',
	  ROI: 'Рой Ет',
	  RME: 'Рим',
	  ZNZ: 'Занзибар',
	  SON: 'Эспириту-Санту',
	  POT: 'Порт-Антонио',
	  MGX: 'Маоби',
	  PEG: 'Перуджа',
	  VTB: 'Витебск',
	  YOT: 'Йотвата',
	  KSK: 'Карлскога',
	  BUQ: 'Булауайо',
	  OWA: 'Оватонна',
	  RFN: 'Raufarhofn',
	  MIK: 'Миккели',
	  ACC: 'Аккра',
	  YCU: 'Yun Cheng',
	  USS: 'Санкти-Спиритус',
	  WAZ: 'Уорвик',
	  ASL: 'Маршалл',
	  LKP: 'Лейк-Пласид',
	  PPV: 'Port Protection',
	  TFC: 'Таормина',
	  RNE: 'Руан',
	  POD: 'Подор',
	  BHZ: 'Белу-Оризонти',
	  HBX: 'Хубли',
	  AZO: 'Каламазу',
	  QCM: 'Комо',
	  ZQQ: 'Wetzlar',
	  QFR: 'Frosinone',
	  NOI: 'Новороссийск',
	  MWE: 'Merowe',
	  JAN: 'Джексон',
	  PKZ: 'Паксе',
	  LAY: 'Ледисмит',
	  DSI: 'Дестин',
	  OUE: 'Уессо',
	  QSC: 'Сан-Карлос',
	  PYC: 'Плейкон-Чико',
	  ESC: 'Эсканаба',
	  HIL: 'Шилаво',
	  MQC: '',
	  KSC: 'Косице',
	  ZEZ: 'Hameln',
	  SYP: 'Сантьяго',
	  QVD: 'Сало',
	  XCH: 'о. Рождества',
	  TMP: 'Тампере',
	  NDB: 'Нуадхибу',
	  ABC: 'Альбасете',
	  RUY: 'Копан',
	  BFE: 'Билефельд',
	  CMF: 'Шамбери',
	  ROS: 'Росарио',
	  BFT: 'Бофорт',
	  SLZ: 'Сан-Луис',
	  JBR: 'Джонесборо',
	  LNY: 'Ланаи',
	  GIC: 'Бойгу-Айленд',
	  FAH: 'Фарах',
	  BVE: 'Брив ла Гайард',
	  CCJ: 'Кожикоде',
	  NUP: 'Нунапитчук',
	  HGD: 'Хьюэнден',
	  CBU: 'Cottbus',
	  NGS: 'Нагасаки',
	  MQK: 'Сан-Матиас',
	  ATO: 'Атенс',
	  ZCT: 'Delmenhorst',
	  MWQ: 'Магве',
	  RDM: 'Редмонд',
	  VSE: 'Визеу',
	  WPA: 'Пуэрто-Айсен',
	  YGW: 'Пост-де-ла-Бален',
	  ZON: 'Moers',
	  ZRX: 'Riesa',
	  CFC: 'Cacador',
	  BAJ: 'Бали',
	  FGR: 'Фуэнгирола',
	  BVR: 'Брава',
	  AJI: '',
	  ZZU: 'Мзузу',
	  KWJ: 'Квангджу',
	  EDG: 'Edgewood',
	  UIB: 'Кибдо',
	  OKJ: 'Окаяма',
	  IUE: 'Ниуэ',
	  PBC: 'Пуэбла',
	  JPD: 'Пасадена',
	  BKS: 'Бенгкулу',
	  LAU: 'Ламу',
	  SLC: 'Солт-Лейк-Сити',
	  HUS: 'Хьюз',
	  SMF: 'Сакраменто Калифорния',
	  FUG: 'Фуян',
	  SGK: 'Сангапи',
	  LGU: 'Логан',
	  XIL: 'Xilinhot',
	  QCI: 'Плайя-де-лос-Кристианос',
	  GXY: 'Greeley',
	  RTI: 'Роти',
	  QYH: 'Хенгело',
	  YSH: 'Смит-Фолз',
	  BOR: 'Белфорт',
	  AOS: 'Amook Bay',
	  UBB: 'Мабуиаг-Айленд',
	  ADG: 'Адриан',
	  HII: 'Лейк Хавасу Сити',
	  QDI: 'Dornbirn',
	  YGX: 'Гиллам',
	  ZTR: '',
	  UDE: 'Уден',
	  BMO: 'Бамо',
	  UEO: 'Кумеджима',
	  ION: 'Impfondo',
	  SVA: 'Савунга',
	  SLI: 'Солвези',
	  MOE: 'Момейк',
	  HRS: 'Харрисмит',
	  RFP: 'Раиатеа',
	  YDO: 'Dolbeau',
	  AXE: 'Xanxere',
	  JMC: 'Саусалито',
	  MKJ: 'Makoua',
	  RZS: 'Саван',
	  CGY: 'Кагаян де Оро',
	  IFO: 'Ивано-Франковск',
	  ZNR: 'Iserlohn',
	  HEH: 'Хехо',
	  PUG: 'Порт-Аугуста',
	  VMI: 'Валлеми',
	  EEN: 'Кини',
	  UMD: 'Уумманнак',
	  AIZ: 'Lake Of The Ozarks',
	  KYU: 'Коюкек',
	  RIG: 'Рио Гранде',
	  LAK: 'Aklavik',
	  ZCR: 'Дахау',
	  NKI: 'Naukiti',
	  BVX: 'Бейтсвилль',
	  ARK: 'Аруша',
	  GZA: 'Газа',
	  SLM: 'Саламанка',
	  DFI: 'Дефайенс',
	  JFR: 'Paamiut',
	  GJT: 'Гранд Джанкшн',
	  YIF: 'Пакуашипи',
	  HYA: 'Хианнис',
	  KSJ: 'Kasos Island',
	  ACA: 'Акапулько',
	  ZVK: 'Сваннакхэт',
	  ASV: 'Амбосели',
	  FMH: 'Фолмаут',
	  OGG: 'Мауи',
	  SMS: 'Сент-Мари',
	  QXB: 'Экс-ан-Прованс',
	  XEK: 'Мелвилль',
	  MNF: 'Мана Айленд',
	  TDX: 'Трат',
	  RWN: '',
	  ERH: 'Эррачидиа',
	  ACR: 'Араракуара',
	  YLE: 'Wha Ti/Lac La Martre',
	  ZNQ: 'Ингольштадт',
	  YGT: 'Иглулик',
	  YVG: 'Вермийон',
	  RIW: 'Ривертон',
	  ZPK: 'Ravensburg',
	  VNR: 'Венрук',
	  EWB: 'Нью Бедфорд',
	  IXU: 'Аурангабад',
	  YKQ: 'Васкаганиш',
	  TCN: 'Теуакан',
	  JAH: 'Обань',
	  BJA: 'Беджайя',
	  EKT: 'Эскилстуна',
	  OKR: 'Йорк-Айленд',
	  AGZ: 'Аггенейс',
	  QDN: 'Эден',
	  LNZ: 'Линц',
	  XBC: 'Бриансон',
	  YXC: 'Кранбрук',
	  SZK: 'Скукуза',
	  FEL: 'Furstenfeldbruck',
	  BDT: 'Bado Lite',
	  ZGE: 'Goerlitz',
	  NUI: 'Нуиксут',
	  THN: 'Тролльхеттан',
	  EAA: 'Игл',
	  RXS: 'Роксас сити',
	  OKY: 'Оаки',
	  OZG: '',
	  TMJ: 'Термез',
	  RAR: 'Раротонга',
	  ELG: 'Эль-Голеа',
	  REC: 'Ресифи',
	  KHS: 'Хасаб',
	  UST: 'Сент Огустин',
	  CZN: 'Chisana',
	  OSD: 'Остерзунд',
	  TRS: 'Триест',
	  LEU: 'Сео-де-Урхель',
	  WAI: 'Antsohihy',
	  ABQ: 'Альбукерке',
	  HTO: 'Ист Гемптон',
	  MLY: 'Мэнли-Хот-Спрингс',
	  SAV: 'Саванна',
	  GYM: 'Гваймас',
	  AWD: 'Анива',
	  HNH: 'Хуна',
	  FMO: 'Мюнстер',
	  FMY: 'Форт-Майерс',
	  TAD: 'Тринидад',
	  ZWM: 'Висмар',
	  PIO: 'Писко',
	  NUR: 'Нулларбор',
	  BSX: 'Бассейн',
	  CEH: 'Челинда',
	  ZUH: 'Чжухай',
	  AHT: 'Амчитка',
	  LSN: 'Лос-Банос',
	  BIM: 'Бимини',
	  IPA: 'Ипота',
	  RGA: 'Рио-Гранде',
	  DHD: 'Дерхем-Даунс',
	  DNN: 'Dalton',
	  VTE: 'Виентиане',
	  XMR: 'Марманде',
	  KAN: 'Кано',
	  DLL: 'Диллон',
	  XGR: 'Kangiqsualujjuaq',
	  RNP: 'Ронгелап',
	  CLL: 'Колледж Стейшн',
	  NIX: 'Ниоро',
	  BHQ: 'Брокен-Хилл',
	  RBP: 'Рабараба',
	  MFT: 'Мачу-Пиччу',
	  KLW: 'Клавок',
	  PEA: 'Пеннешо',
	  YQJ: '',
	  NGQ: '',
	  YCW: 'Чилливак',
	  TDB: 'Tetebedi',
	  KIW: 'Китве',
	  JTR: 'Тира о-в',
	  SIA: 'Сиань',
	  MVY: 'Мартас Вайн-ярд',
	  BYB: 'Dibaa',
	  KBU: 'Котабару',
	  PGH: 'Пантнагар',
	  HOR: 'Хорта',
	  NDJ: 'Нджамена',
	  YUD: 'Умиуяк',
	  HUQ: 'Хоун',
	  POU: 'Паукипси',
	  POG: 'Порт-Гентил',
	  SAD: 'Саффорд',
	  NIG: 'Никунау',
	  AKF: 'Kufrah',
	  ZLS: '',
	  HVE: 'Хенксвилль',
	  PKH: 'Портоэли',
	  USU: 'Busuanga',
	  PCE: 'Painter Creek',
	  TPR: 'Том-Прайс',
	  FAJ: 'Фажардо',
	  LOG: 'Longview',
	  YAA: 'Anahim Lake',
	  GMI: 'Гасмата',
	  FBD: '',
	  LPK: 'Липецк',
	  LVO: 'Лавертон',
	  GKL: 'Грейт-Кеппел-Айленд',
	  PCJ: 'Пуэрто ла Виктория',
	  ANA: 'Анахайм',
	  RCM: 'Ричмонд',
	  TEC: 'Телемако-Борба',
	  MRA: 'Мисурата',
	  KGS: 'Кос Греция',
	  SPC: 'Санта-Крус',
	  GLG: 'Гленгил',
	  CLD: 'Карлсбад',
	  ERV: 'Kerrville',
	  XVP: 'Виллепинте',
	  MUT: 'Muscatine',
	  NOZ: 'Новокузнецк',
	  TIR: 'Тирупати',
	  YXT: 'Террас',
	  CSB: 'Caransebes',
	  WWT: 'Ньюток',
	  CXR: 'Камрань',
	  CJI: 'Сиудад-Реал',
	  ONP: 'Ньюпорт',
	  SHE: 'Шеньян',
	  SCH: 'Шенектеди',
	  LEB: 'Ливан',
	  HWI: 'Hawk Inlet',
	  CSL: 'Сан Луис Обиспо',
	  VRC: 'Вирак',
	  ILD: 'Alguaire',
	  SGT: 'Штутгарт',
	  UNI: 'Юнион-Айленд',
	  QSR: 'Салерно',
	  MJD: 'Мохенджодаро',
	  SVI: 'Сан-Висенте',
	  QBQ: 'Безансон',
	  XNT: 'Синтай',
	  XSL: 'Сарлат',
	  HAE: 'Хавасупай',
	  ALX: 'Александер-сити',
	  DUG: 'Дуглас',
	  CYR: 'Колония',
	  QME: 'Мессина',
	  SJT: 'Сан Анджело',
	  UAC: 'Сан-Луис-Рио-Колорадо',
	  HGA: 'Харгейсе',
	  KNO: 'Knokke Het Zoute',
	  TNU: 'Ньютон',
	  YGB: 'Гиллис Бэй',
	  ANV: 'Анвик',
	  ZCU: 'Detmold',
	  AUI: 'Aua Island',
	  AAG: 'Арапоти',
	  HOD: 'Ходейда',
	  HNC: 'Hatteras',
	  QJM: 'Brusque',
	  QMU: 'Мутьи',
	  SGE: 'Зиген',
	  OAJ: 'Джексонвилл',
	  DDP: 'Dorado',
	  XFX: 'Фуа',
	  BDG: 'Бландинг',
	  'Охотск': 'охотск',
	  QSF: 'Сетиф',
	  LZO: 'Лучжоу',
	  HAQ: 'Hanimaadhoo',
	  EXM: 'Эксмаус-Галф',
	  WWK: 'Веварк',
	  HLW: 'Хлухлуве',
	  BMG: 'Блумингтон',
	  YCK: 'Колвилл',
	  HJR: 'Кхаджурахо',
	  RBA: 'Рабат',
	  MOO: 'Мумба',
	  LXG: 'Луанг-Намтха',
	  LBE: 'Латроуб',
	  IGM: 'Кингман',
	  VPS: 'Вальпараисо',
	  MOU: 'Маунтин Вилладж',
	  TOU: 'Туо',
	  BKZ: 'Букоба',
	  MHE: 'Митчелл',
	  BOO: 'Бодо',
	  KPR: 'Порт-Вильямс',
	  INH: 'Иньямбане',
	  CXT: 'Чартерс-Тауэрс',
	  QCA: 'Мекка',
	  ZPW: 'Швайнфурт',
	  CSG: 'Колумбус',
	  QNL: 'Ньюли Сюр Сейн',
	  ABL: 'Амбер',
	  HRZ: 'Хоризонтина',
	  CGI: 'Кейп-Жирардо',
	  GMN: 'Греймаут',
	  NTL: 'Ньюкасл',
	  XWF: 'Фалун',
	  ZAT: 'Жаньтон',
	  BUY: 'Банбери',
	  TXG: 'Тайчунг',
	  FUK: 'Фукуока',
	  MPM: 'Мапуто',
	  LNJ: 'Lincang',
	  ZCI: 'Bocholt',
	  CUL: 'Кулиакан',
	  UGN: 'Ваукеган',
	  ILY: 'Айли',
	  KSQ: 'Карши',
	  ACH: 'Альтенрейн',
	  XFM: 'Шониган',
	  BJC: 'Брумфилд',
	  MCX: 'Махачкала',
	  CBO: 'Котабато',
	  OUZ: 'Зуерате',
	  URS: 'Курск',
	  YCA: 'Куртенэ',
	  QMR: 'Марсала',
	  SEN: 'Саутенд',
	  GUM: 'Гуам',
	  BCM: 'Бакэу',
	  XOG: 'Оранж',
	  BHS: 'Батерст',
	  FFM: 'Фергюс Фоллз',
	  NSA: 'Нуса',
	  SHS: 'Шаши',
	  CEN: 'Сиудад-Обрегон',
	  MDC: 'Манадо',
	  NKG: 'Нанцзин',
	  DOU: 'Дорадус',
	  UNA: 'Уна',
	  TOY: 'Тояма',
	  AYQ: 'Айерс-Рок',
	  LQN: 'Кала Нау',
	  DIG: 'Дикинь',
	  GCI: 'Гернси',
	  YMN: 'Макковик',
	  GCV: 'Граватаи',
	  TAZ: 'Дашогуз',
	  YOO: 'Ошава',
	  HGL: 'Хелголанд',
	  SBT: 'Сан-Бернардино',
	  GWS: 'Гленвуд-Спрингс',
	  STW: 'Ставрополь',
	  LDK: 'Лидчепинг',
	  NDK: 'Намдрик',
	  GUA: 'Гватемала-Сити',
	  IGT: 'Назрань',
	  MVX: 'Минвул',
	  FRZ: 'Фрицлар',
	  MIE: 'Манси',
	  TJN: 'Takume',
	  COC: 'Конкордия',
	  QRZ: 'Resende',
	  GLD: 'Гудленд',
	  KAA: 'Kazama',
	  YWA: 'Петавава',
	  ZGO: 'Gotha',
	  CCN: 'Чагчаран',
	  QLD: 'Блида',
	  PEJ: 'Пескичи',
	  TGG: 'Куала-Теренгану',
	  LOH: 'Лоха',
	  EWI: 'Энаротали',
	  TYN: 'Тайюань',
	  BNK: 'Баллина',
	  QYO: 'Ольштын',
	  JHS: 'Сисимиут',
	  DDC: 'Додж Сити',
	  YLJ: 'Медоу-Лейк',
	  TPI: 'Тапини',
	  QVZ: 'Хамина',
	  IXW: 'Джамшедпур',
	  BVI: 'Бирдсвилл',
	  ULA: 'San Julian',
	  PMD: 'Палмдейл',
	  HVG: 'Хоннигсвог',
	  AVI: 'Сиего де Авила',
	  WSG: 'Вашингтон',
	  OLK: 'Фуэрте-Олимпо',
	  AZS: 'Самана',
	  OOT: 'Онотоа',
	  VOT: 'Вотупоранга',
	  MZR: 'Mazar I Sharif',
	  THU: 'Pituffik',
	  OSW: 'Орск',
	  LFO: 'Келафо',
	  YGQ: 'Джералдтон',
	  NYN: 'Нинган',
	  SDP: 'Сенд Поинт',
	  KHC: 'Керчь',
	  KOT: 'Котлик',
	  LYA: 'Люоянг',
	  BXU: 'Бутуан',
	  ECH: 'Эчука',
	  HYD: 'Хайдерабад',
	  TQR: 'Сан Домино Айленд',
	  YZE: 'Gore Bay',
	  OBE: 'Окичоби',
	  CMG: 'Корумба',
	  QCH: 'Colatina',
	  CVU: 'Корво-Айленд',
	  CMW: 'Камагуэй',
	  WSU: 'Wasu',
	  YSG: 'Lutselke/Snowdrift',
	  BIR: 'Биратнагар',
	  DNV: 'Дэнвилл',
	  CAQ: 'Кавказ',
	  ENO: 'Encarnacion',
	  ROT: 'Роторуа',
	  AHO: 'Альгеро',
	  NCP: 'Luzon Island',
	  KCM: 'Кахраманмарас',
	  RVV: 'Rairua',
	  JDH: 'Джодхпур',
	  TVC: 'Траверс-Сити',
	  SLP: 'Сан-Луис-Потоси',
	  DPE: 'Дьеп',
	  BST: 'Бост',
	  NWA: 'Moheli',
	  VLY: 'Энглси',
	  KHG: 'Каши',
	  MQN: 'Мо-И-Рана',
	  MDU: 'Менди',
	  BHK: 'Бухара',
	  PMQ: 'Перито-Морено',
	  DED: 'Дера Дун',
	  BWN: 'Бандар-Сери-Багаван',
	  SNF: 'Сан-Фелипе',
	  MHK: 'Манхэттен',
	  ILZ: 'Жилина',
	  TKN: 'Токаношима',
	  CSK: 'Cap Skirring',
	  FAT: 'Фресно',
	  MDY: 'Мидвэй-Айленд',
	  BAU: 'Бауру',
	  HGU: 'Маунт-Хаген',
	  BIU: 'Билдудалур',
	  ZQP: 'Wesel',
	  YGN: 'Greenway Sound',
	  YNJ: 'Яньцзи',
	  SMO: 'Санта-Моника',
	  ANU: 'Антигуа',
	  OOM: 'Коома',
	  TNV: 'Табуаэран',
	  PRC: 'Прескотт',
	  QTS: '',
	  LRH: 'Ла-Рошель',
	  BSQ: 'Бисби',
	  GGG: 'Лонгвью',
	  FLG: 'Флагстафф',
	  YBE: 'Ураниум-Сити',
	  SVC: 'Силвер Сити',
	  TMU: 'Тамбор',
	  HTR: 'Hateruma',
	  BJM: 'Буджумбура',
	  FIZ: 'Фицрой-Кроссинг',
	  POM: 'Порт-Морсби',
	  RER: 'Реталуелу',
	  BJF: 'Батсфьорд',
	  KVB: 'Сковде',
	  BFN: 'Блумфонтейн',
	  SLA: 'Сальта',
	  DAU: 'Дарю',
	  KGP: 'Когалым',
	  PQS: 'Pilot Station',
	  LBF: 'Норт Платт',
	  MHT: 'Манчестер',
	  YTR: 'Трентон',
	  RAH: 'Рафха',
	  YEV: 'Инувик',
	  RRG: 'Родригес-Айленд',
	  QBS: 'Брешиа',
	  KKE: 'Керикери',
	  GEO: 'Джорджтаун',
	  MFN: 'Милфорд Саунд',
	  SJD: 'Сан-Хосе Кабо',
	  MAJ: 'Маджеро',
	  DUT: 'Датч-Харбор',
	  BCB: 'Блэксбург',
	  CCL: 'Чинчилла',
	  TBZ: 'Тебриз',
	  CKH: 'Чокурдах',
	  ALL: 'Альбенга',
	  FYV: 'Файетвилль',
	  SKL: 'Айл-оф-Ска Гербидские о-ва',
	  KPO: 'Поханг',
	  BFD: 'Брадфорт',
	  GET: 'Джералдтон',
	  HDF: 'Херингсдорф',
	  OBU: 'Кобук',
	  CAK: 'Акрон',
	  YLW: 'Келовна',
	  OIR: 'Окусири',
	  LPX: 'Лиепая',
	  VAO: 'Суаванао',
	  CQF: 'Кале',
	  PBH: 'Паро',
	  XTY: 'Стратрой',
	  LYR: 'Лонгербьен',
	  QMZ: 'Майнц',
	  BBW: 'Брокен-Боу',
	  AGN: 'Ангун',
	  ABX: 'Олбэри',
	  HZL: 'Hazelton',
	  KDR: 'Kandrian',
	  KRC: 'Керинчи',
	  MVK: 'Малка',
	  KKA: 'Койек',
	  TUA: 'Тулкан',
	  MKH: 'Mokhotlong',
	  NUA: '',
	  CIJ: 'Кобиха',
	  CDC: 'Седар-Сити',
	  XVN: 'Верден',
	  SYJ: 'Сирджан',
	  NAL: 'Нальчик',
	  BUN: 'Буэнавентура',
	  AOK: 'Карпатос',
	  AKY: 'Акьяб',
	  SFM: 'Санфорд',
	  SYR: 'Сиракузы',
	  LRE: 'Лонгрич',
	  PNX: 'Шерман',
	  LLF: 'Лин Лин',
	  YST: 'Сент Тереса Поинт',
	  ZHX: 'Tubarao',
	  XEB: 'Эвиан ле Бэн',
	  MHZ: 'Милденхолл',
	  CUT: 'Cutral Co',
	  HYR: 'Хейвард',
	  CRW: 'Чарльзтон',
	  LFR: 'Ла Фриа',
	  STV: 'Сурат',
	  DVL: 'Девилз Лейк',
	  KHR: 'Хархорин',
	  SNI: 'Синоу',
	  EMX: 'El Maiten',
	  ZGW: 'Greifswald',
	  HRO: 'Харрисон',
	  GNY: 'Санлиурфа',
	  LIR: 'Либерия',
	  LEF: 'Lebakeng',
	  YEH: 'Асалойе',
	  LID: 'Лейден',
	  SJU: 'Сан-Хуан',
	  CXY: 'Cat Cay',
	  VVC: 'Вильявисенсио',
	  YEC: 'Ечон',
	  CDV: 'Кордова',
	  SDE: 'Сантьяго-дель-Эстеро',
	  MON: 'Маунт-Кук',
	  GYY: 'Гери',
	  ZET: 'Goslar',
	  PMC: 'Пуэрто Монт',
	  MLJ: 'Milledgeville',
	  TDK: 'Талды-Курган',
	  TTT: 'Тайтунг',
	  ELK: 'Элк-Сити',
	  CHA: 'Чаттануга',
	  CLY: 'Калви',
	  QOC: 'Osasco',
	  MAB: 'Мараба',
	  GAS: 'Гарисса',
	  FEG: 'Фергана',
	  YAL: 'Алерт-Бей',
	  MGS: 'о.Мангаиа',
	  CME: 'Сиудад-дель-Кармен',
	  OLO: 'Olomouc',
	  OUH: 'Оудсхорн',
	  VHN: 'Ван Хорн',
	  NOJ: 'Ноябрьск',
	  WVB: 'Валвис-Бей',
	  IVR: 'Инверелл',
	  BID: 'Блок-Айленд',
	  MJI: 'Маджи',
	  YYL: 'Линн-Лейк',
	  ZIT: 'Zittau',
	  XGY: 'Гримсби',
	  SUI: 'Сухуми',
	  BDK: 'Бондуку',
	  SVS: 'Стивенс Виллидж',
	  YLD: 'Шапло',
	  HTH: 'Хауторн',
	  SQI: 'Стерлинг',
	  KIF: 'Kingfisher Lake',
	  EYK: 'Белоярский',
	  CVQ: 'Карнарвон',
	  LDN: 'Ламиданда',
	  ASH: 'Нашуа',
	  YZU: 'Whitecourt',
	  BFR: 'Бедфорт',
	  UND: 'Кундуз',
	  GAJ: 'Ямагата',
	  WAO: 'Вабо',
	  KOH: 'Кулата',
	  OYO: 'Трес-Арройос',
	  HUC: 'Хумакао',
	  CUS: 'Колумбус',
	  CSY: 'Чебоксары',
	  FMA: 'Формоса',
	  ONT: 'Онтарио',
	  JEF: 'Джефферсон Сити',
	  UAK: 'Нарсарсуак',
	  XCI: 'Шамбор',
	  ACV: 'Юрека',
	  IDA: 'Айдахо Фоллз',
	  PVH: 'Порту Велью',
	  OCA: 'Оушен Риф',
	  ALS: 'Аламоса',
	  ESP: 'Ист Штраудсбург',
	  ODE: 'Оденсе',
	  YBP: 'Ибинь',
	  BGW: 'Багдад',
	  PVD: 'Провиденс',
	  NDA: 'Банданаира',
	  CHU: 'Chuathbaluk',
	  UIK: 'Усть-Илимск',
	  MSJ: 'Мисава',
	  ARV: 'Миноква',
	  INC: 'Фучуан',
	  NCY: 'Аннеси',
	  GRQ: 'Гронинген',
	  AMP: 'Ампанихи',
	  TPU: 'Тикапур',
	  LSY: 'Лисмор',
	  KWL: 'Гуйлинь',
	  SSJ: 'Санднессьоен',
	  YAK: 'Якутат',
	  TIG: 'Тингвон',
	  CHO: 'Шарлоттсвилль',
	  CGC: 'Cape Gloucester',
	  MLM: 'Морелия',
	  SFE: 'Сан-Фернандо',
	  AWK: 'Wake Island',
	  JST: 'Джонстаун',
	  LGG: 'Льеж',
	  ERI: 'Эри',
	  ZMT: 'Массе',
	  CVO: 'Корваллиз',
	  KPA: 'Kopiago',
	  CPV: 'Кампина Гранде',
	  CGU: 'Ciudad Guayana',
	  ZHQ: 'Halberstadt',
	  PHH: 'Пхантхиет',
	  SCX: 'Салина-Крус',
	  CMD: 'Кутамундра',
	  VIX: 'Витория',
	  IXC: 'Чандигарх',
	  PAT: 'Патна',
	  XZB: 'Касселман',
	  MES: 'Медан',
	  YBJ: 'Baie Johan Beetz',
	  MWX: 'Кванджу',
	  AZB: 'Амазон-Бей',
	  EBO: 'Эбон',
	  XVW: 'Вулвергемптон',
	  GZT: 'Газиантеп',
	  LOK: 'Лодвар',
	  KHN: 'Нанчанг',
	  CFS: 'Коффс Харбор',
	  PTE: 'Порт Стефенс',
	  DIB: 'Дибругарх',
	  ABI: 'Эбилин',
	  MYT: 'Мьиткьина',
	  LSI: 'Лервик',
	  GOA: 'Генуя',
	  XBQ: 'Блуа',
	  TAC: 'Таклобан',
	  KHZ: 'Кауэхи',
	  OGZ: 'Владикавказ',
	  XQP: 'Куэпос',
	  SHB: 'Накасибецу',
	  XED: '',
	  XVS: 'Валансьен',
	  NRV: 'Гуам',
	  EUF: 'Эуфола',
	  AIU: 'о. Атиу',
	  YHZ: 'Галифакс',
	  KTD: 'Kitadaito',
	  GHA: 'Гардайя',
	  PKA: 'Напаскиак',
	  CDJ: 'Conceicao Do Araguaia',
	  BDA: 'Бермуда',
	  XLY: 'Олдершот',
	  HET: 'Хоххот',
	  JSH: 'Сития',
	  HNB: 'Хантингбург',
	  LJA: 'Lodja',
	  BFX: 'Бафуссам',
	  WRO: 'Вроцлав',
	  YYR: 'Гус-Бей',
	  CON: 'Конкорд',
	  HRL: 'Харлинген',
	  ZKW: 'Wetzikon',
	  PXS: 'Пуэрто-де-Санта-Мария',
	  RAY: 'Ротсей',
	  KNS: 'Кинг-Айленд',
	  KIS: 'Кисуму',
	  ING: 'Lago Argentino',
	  BII: 'Атолл Бикини',
	  AUK: 'Алаканук',
	  MNU: 'Моламьяин',
	  BAV: 'Баоту',
	  QUQ: 'Касерес',
	  MOC: 'Монтес-Кларос',
	  RBX: '',
	  CNY: 'Моаб',
	  SGH: 'Спрингфилд',
	  GUT: 'Гютерсло',
	  LBL: 'Либерал',
	  BZE: 'Белиз-Сити',
	  QZO: 'Ареццо',
	  HAD: 'Халмстад',
	  LAJ: 'Лажис',
	  TUU: 'Табук',
	  HME: 'Хасси Мессаоуд',
	  ITR: 'Итумбиара',
	  BZK: 'Брянск',
	  QMB: 'Панамби',
	  PNJ: 'Пэнлай',
	  QCE: 'Коппер-Маунтин',
	  QJC: '',
	  DIE: 'Диего Суарез',
	  ZBZ: 'Bad Salzungen',
	  KHY: 'Хой',
	  LMK: 'Лимерик',
	  DOY: 'Дуньин',
	  ZOW: 'Nordhorn',
	  HDM: 'Хамадан',
	  JLR: 'Jabalpur',
	  CZF: 'Cape Romanzof',
	  BJP: 'Браганца Паулиста',
	  VIC: 'Виченца',
	  YQK: 'Кенора',
	  XXR: 'Al No',
	  NTT: 'Ниуатопутапу',
	  KMW: 'Кострома',
	  CTW: 'Коттонвуд',
	  GKT: 'Гатлинбург',
	  DEM: 'Dembidollo',
	  XBJ: 'Бирджанд',
	  DGE: 'Муджи',
	  RGE: 'Porgera',
	  SAC: 'Сакраменто',
	  XSB: 'Сен-Мало',
	  RDB: 'Red Dog',
	  FDH: 'Фридрихшафен',
	  ZOU: 'Neuwied',
	  TGH: 'Тонгоа',
	  SJF: 'Сент-Джон',
	  OTS: 'Анакортес',
	  RBV: 'Рамата',
	  CZA: 'Чинчен Ица',
	  HAK: 'Хайкоу',
	  LUH: 'Лудхиана',
	  PPK: 'Петропавловск',
	  ISG: 'Исигаки',
	  YNG: 'Янгстаун',
	  LTL: 'Ластурвиль',
	  CPE: 'Кампече',
	  YPL: 'Пикль Лэйк',
	  CIS: 'Кентон Айленд',
	  DAN: 'Дэнвилл',
	  ODL: 'Кордилло-Даунс',
	  KLA: 'Кампала',
	  LSA: 'Losuia',
	  CSN: 'Карсон-Сити',
	  NAC: 'Наракурте',
	  TQL: '',
	  QWP: 'Winter Park',
	  GIS: 'Гизборн',
	  XSY: 'Сет',
	  RBJ: 'Ребун',
	  YRR: 'Stuart Island',
	  AAX: 'Аракса',
	  YDC: 'Drayton Valley',
	  XAI: 'Экс ле Бен',
	  DVN: 'Давенпорт',
	  YGR: 'Ильс де Мадель',
	  LUO: 'Луэна',
	  HLM: 'Холланд',
	  PGM: 'Порт Грэм',
	  IRJ: 'Ла-Риоха',
	  AOJ: 'Аомори',
	  AKK: 'Ахиок',
	  OSI: 'Осиек',
	  WTL: 'Tuntatuliak',
	  NBC: 'Набережные Челны',
	  YQF: 'Ред-Дир',
	  QPR: 'Прато',
	  YHP: 'Poplar Hill',
	  ZVH: '',
	  SME: 'Сомерсет',
	  QUT: 'Уцуномия',
	  FYG: '',
	  PKR: 'Покхара',
	  ZGV: 'Wavre',
	  LMT: 'Кламмат Фоллз',
	  BNY: 'Беллона',
	  HJJ: 'Цзи Цзянь',
	  EJA: 'Барранкабермежа',
	  VDZ: 'Вальдес',
	  XZJ: 'Rail',
	  ENY: 'Яньань',
	  KLN: 'Ларсен Бэй',
	  CAL: 'Кемпбелтаун',
	  KKH: 'Конгиганак',
	  FMI: 'Kalemie',
	  XKD: 'Halden',
	  TLQ: '',
	  ACT: 'Вако',
	  RUE: '',
	  YZX: 'Гринвуд',
	  JBS: 'Плезентон',
	  PNR: 'Пуэнт-Нуар',
	  EYP: 'Эль-Йопал',
	  PGS: 'Пич-Спрингс',
	  ORS: 'Орфеус-Айленд',
	  JIL: 'Цзилинь',
	  YGZ: 'Гриз Фиорд',
	  QTK: 'Ротенбург',
	  FBA: '',
	  UNT: 'Анст Шетландские о-ва',
	  TEZ: 'Тезпур',
	  TCT: 'Такотна',
	  BWO: 'Балаково',
	  GAM: 'Гамбелл',
	  GOB: 'Гоба',
	  KHM: 'Хамти',
	  COJ: 'Кунабарабран',
	  ZWY: '',
	  DIR: 'Дире Дауа',
	  SOG: 'Сондал',
	  LAA: 'Ламар',
	  AGP: 'Малага',
	  DMS: '',
	  MEA: 'Макаэ',
	  ULP: 'Квилпай',
	  BHL: 'Байя-де-Лос-Анджелес',
	  QRV: 'Аррас',
	  BQB: 'Басселтон',
	  KZO: 'Кзыл-Орда',
	  SLQ: 'Слитмут',
	  TCR: 'Тутикорин',
	  MLS: 'Майлз-Сити',
	  FNR: 'Funter Bay',
	  CIK: 'Чалкиитзик',
	  YGG: 'Ганг Харбор',
	  UMU: 'Умуарама',
	  WMH: 'Маунтин Хоум',
	  MUO: 'Маунтин Хоум',
	  STA: 'Stauning',
	  TKQ: 'Кигома',
	  SIS: 'Сишен',
	  KWG: 'Кривой Рог',
	  KQT: 'Куронтеппа',
	  NME: 'Найтмут',
	  OMB: 'Омбуэ',
	  LSW: 'Lhoksumawe',
	  ZOR: 'Neustadt An Der Weinstrasse',
	  MWZ: 'Мванза',
	  OTG: 'Уортингтон',
	  TAB: 'Тобаго',
	  PAV: 'Paulo Afonso',
	  MUH: 'Мерса-Матрух',
	  NOB: 'Носара-Бич',
	  YPJ: 'Aupaluk',
	  BNN: 'Броннойзунд',
	  WXF: 'Брэйнтри',
	  ALH: 'Олбани',
	  HTF: 'Хэтфилд',
	  KOK: 'Коккола',
	  TOR: 'Торрингтон',
	  YMI: 'Минаки',
	  VIK: 'Кавик',
	  OAI: '',
	  GNR: 'General Roca',
	  USN: 'Ульсан',
	  SYY: 'Сторновей',
	  OPS: 'Синоп',
	  SQC: 'Саутерн-Кросс',
	  NAW: 'Наратхиват',
	  DKR: 'Дакар',
	  CER: 'Шербур',
	  YEA: 'Эдмонтон',
	  XVB: 'Стаффорд',
	  CUR: 'Кюрасао',
	  QLR: 'Leiria',
	  YWB: 'Kangiqsujuaq',
	  CMQ: 'Клермонт',
	  PHX: 'Феникс',
	  AJL: 'Айзавл',
	  LBU: 'Лабуан',
	  SAN: 'Сан-Диего',
	  NVS: 'Неверс',
	  TEB: 'Тетерборо',
	  YAC: 'Cat Lake',
	  GOT: 'Готенбург',
	  TZN: 'Саут Андрос',
	  YCS: 'Честерфилд-Инлет',
	  WNZ: 'Венчжоу',
	  XWP: 'Hassleholm',
	  PEX: 'ПЕЧОРА',
	  AFT: 'Афутара',
	  ZOC: 'Ludenscheid',
	  OZZ: 'Урзазат',
	  NRD: 'Norderney',
	  GUC: 'Ганнисон',
	  TEG: 'Tenkodogo',
	  BCT: 'Boca Raton',
	  XCD: 'Шалон-сюр-Саон',
	  'Сиануквиль': 'Сиануквиль',
	  GAO: 'Гуантанамо',
	  KMI: 'Миязаки',
	  YPZ: 'Бернс-Лейк',
	  PED: 'Пардубице',
	  PFN: 'Панама-Сити',
	  BPC: 'Баменда',
	  NUX: 'Новый Уренгой',
	  GDL: 'Гвадалахара',
	  XSC: 'Саут-Кайкос',
	  OWD: 'Норвуд',
	  SZP: 'Санта-Паула',
	  XPH: 'Порт-Хоуп',
	  KLD: 'Тверь',
	  MLT: 'Миллинокет',
	  TOV: 'Тортола Вестенд',
	  BLJ: 'Батна',
	  BUC: 'Бурктаун',
	  KSR: '',
	  TLS: 'Тулуза',
	  BZT: 'Бразория',
	  PLN: 'Пеллстон',
	  YIN: 'Инин',
	  OHH: '',
	  EDO: 'Эдремит',
	  QOW: '',
	  HEZ: 'Натчез',
	  YPT: 'Pender Harbor',
	  PGF: 'Перпиньян',
	  ISN: 'Уиллистон',
	  PBU: 'Путао',
	  BYU: 'Байройт',
	  CUA: 'Сиудад-Конститусьон',
	  EQS: 'Эскель',
	  NLA: 'Ндола',
	  THS: 'Сухотай',
	  KTG: 'Ketapang',
	  LBT: 'Ламбертон',
	  QTZ: 'Коатцакоалькос',
	  POC: 'Ла Верне',
	  LOV: 'Монклова',
	  HST: 'Хоумстед',
	  VHM: 'Вильхельмина',
	  BMC: 'Бригам-Сити',
	  SFJ: 'Кангерлуссуак',
	  KPY: 'Порт Бейли',
	  CVM: 'Сиудад-Виктория',
	  UIN: 'Квинси',
	  CKC: 'Черкассы',
	  PUF: 'По',
	  SBK: 'Сен Брие',
	  JEJ: 'Джех',
	  NAQ: 'Qaanaaq',
	  TMT: 'Тромбетас',
	  RKH: 'Rock Hill',
	  QFE: 'Форт-Беннинг',
	  YXL: 'Сиу-Лукаут',
	  MCN: 'Макон',
	  ZLR: 'Линарес',
	  MCU: 'Монлюсон',
	  MZX: 'Masslo',
	  ULB: 'Улей',
	  ZBF: 'Батхерст',
	  ANB: 'Аннистон',
	  SZV: 'Сучжоу',
	  CNG: 'Коньяк',
	  VLV: 'Валера',
	  KGK: 'Колиганек',
	  FRG: 'Фармингдейл',
	  BJU: 'Bajura',
	  PIP: 'Pilot Point',
	  YYE: 'Форт-Нельсон',
	  RJK: 'Риека',
	  CIC: 'Чико',
	  SKE: 'Скиен',
	  HYN: 'Хуанань',
	  RAC: 'Рацине',
	  BAS: 'Балале',
	  HQM: 'Hoquaim',
	  GAN: 'Ген-Айленд',
	  CXL: 'Калексико',
	  QTY: 'Цу Япония',
	  QEO: 'Бельско-Бяла',
	  GMR: 'Гамбир, остров',
	  TUZ: 'Тукума',
	  ISQ: 'Мэнистик',
	  OER: 'Орнсколдсвик',
	  WSX: 'Westsound',
	  SIQ: 'Singkep',
	  ADH: 'Алдан',
	  QXP: 'Struga',
	  IXP: 'Патханкот',
	  THP: 'Термополис',
	  ILO: 'Йоило',
	  XRT: 'Рамбуйе',
	  BUW: 'Baubau',
	  KVK: 'Кировск',
	  PNP: 'Попондетта',
	  YKU: 'Чисасиби',
	  BBR: 'Бас Тера',
	  RZA: 'Santa Cruz',
	  BOV: 'Boang',
	  SVD: 'Сент-Винсент',
	  CBX: 'Кондоболин',
	  INU: 'о. Науру',
	  FLC: 'Фоллз-Крик',
	  HOV: 'Орста-Волда',
	  YGK: 'Кингстон',
	  ISS: 'Уискассет',
	  WGT: 'Вангаратта',
	  BQK: 'Брансвик',
	  SSR: 'Сара',
	  ONX: 'Колон',
	  HIO: 'Хиллсборо',
	  STX: 'Сент Кроикс',
	  BXE: 'Bakel',
	  BRE: 'Бремен',
	  NDV: 'Анакосеия',
	  GKA: 'Горока',
	  ZEU: 'Геттинген',
	  KSL: 'Кассала',
	  NLD: 'Нуэво-Ларедо',
	  GVT: 'Гринвилль',
	  KRG: '',
	  FRL: 'Форли',
	  RSB: 'Роузберт',
	  RRA: 'Ронда',
	  YRB: 'Resolute',
	  EGP: 'Игл-Пасс',
	  YVA: 'Морони',
	  KRE: 'Kirundo',
	  ZCO: 'Темуко',
	  YUY: 'Руин-Норанда',
	  MGQ: 'Могадишу',
	  CYC: 'Кайе-Чапел',
	  GMZ: 'Сан-Себастиан-де-ла-Гомера',
	  NGB: 'Нинбо',
	  PZY: 'Пештаны',
	  ARZ: 'Н Зето',
	  YCN: 'Кокран',
	  AEL: 'Альберт-Ли',
	  ALJ: 'Александер-бей',
	  RUN: 'Реюньон-Айленд',
	  DAB: 'Дайтона Бич',
	  XZV: '',
	  MCW: 'Мэйсон Сити',
	  SGF: 'Спрингфилд',
	  PVE: 'Эль-Порвенир',
	  YES: 'Yasouj',
	  ZED: 'Euskirchen',
	  JCU: 'Сеута',
	  GNU: 'Гудньюс Бэй',
	  FIH: 'Киншаса',
	  SBM: 'Шебойган',
	  SGY: 'Скагвэй',
	  WAE: 'Вади-ад-Давасир',
	  MYW: 'Мтвара',
	  SHR: 'Шеридан',
	  KLR: 'Кальмар',
	  ELV: 'Элфин Ков',
	  MCY: 'Марухидор',
	  SDN: 'Сандан',
	  GTR: 'Колумбус',
	  RRO: 'Сорренто',
	  YUX: 'Холл-Бич',
	  ZBJ: 'Фредерика',
	  GAX: 'Гамба',
	  SLL: 'Салала',
	  VIS: 'Вайзалиа',
	  YYB: 'Норт-Бей',
	  ESR: 'Эль-Сальвадор',
	  MGJ: 'Монтгомери',
	  SOB: 'Сармеллек',
	  VHZ: 'Вахитахи',
	  HZK: 'Хусавик',
	  CHM: 'Чимботе',
	  ALF: 'Алта',
	  CTC: 'Катамарка',
	  LDG: 'Лешуконское',
	  SIG: 'Исла Гранде',
	  AMV: 'Амдерма',
	  PUQ: 'Пунта-Аренас',
	  XFS: 'Александрия',
	  EWK: 'Ньютон',
	  GCW: '',
	  IOW: 'Айова-Сити',
	  DUE: 'Dundo',
	  ZPV: 'Швабский Гмюнд',
	  GPS: 'Галапагосские острова',
	  POJ: 'Патос де Минас',
	  FSC: 'Фигари',
	  PHB: 'Парнаиба',
	  BSI: 'Блэрсвилль',
	  DNR: 'Динард',
	  SUH: 'Сур',
	  MDK: 'Мбандака',
	  KBZ: 'Кайкура',
	  RKZ: '',
	  WYA: 'Вайалла',
	  QEZ: 'Pomezia',
	  GJA: 'Guanaja',
	  QBH: 'Леваллуа',
	  TOM: 'Тимбукту',
	  MPR: 'МакФерсон',
	  SDB: 'Салданха-Бей',
	  KLS: 'Kelso',
	  EGX: 'Эгиджик',
	  XRU: 'Регби',
	  TPH: 'Тонопах',
	  XFO: 'Taschereau',
	  CTU: 'Чэнду',
	  ZCA: 'Arnsberg',
	  EPL: 'Эпиналь',
	  YVQ: 'Норман Уэллс',
	  VEE: 'Венеция',
	  YXY: 'Уайтхорс',
	  SAF: 'Санта-Фе',
	  XGD: 'Arendal',
	  FDE: 'Форде',
	  YSL: 'Сент-Леонард',
	  LMS: 'Луисвилль',
	  KWZ: 'Колвези',
	  HUX: 'Санта Круз Хуатулко',
	  QWL: 'Крэкенбэк',
	  PVR: 'Пуэрто-Валларта',
	  BBC: 'Bay City',
	  GAE: 'Габес',
	  WEA: 'Уэтерфорд',
	  YCZ: 'Фэирмонт-Спрингс',
	  XJQ: 'Jonquiere',
	  VEJ: 'Вежле',
	  ZJN: 'Суон-Ривер',
	  RVA: 'Фарафангана',
	  TOX: 'Тобольск',
	  PUU: 'Пуэрто-Асис',
	  PAH: 'Падука',
	  YCC: 'Корнуолл',
	  GWO: 'Гринвуд',
	  SAT: 'Сан-Антонио',
	  KET: 'Кенгтунг',
	  FBM: 'Лубумбаши',
	  VAN: 'Ван Турция',
	  ZTB: 'Тет Лa Балайн',
	  OGA: 'Огаллала',
	  RLD: 'Ричленд',
	  TKF: 'Траки',
	  TFM: 'Телефомин',
	  BMK: 'Borkum',
	  EDR: 'Эдвард-Ривер',
	  PWE: 'Певек',
	  SKY: 'Сандуски',
	  EAT: 'Уэнатчи',
	  AND: 'Андерсон',
	  ZPU: 'Salzgitter',
	  DSC: 'Дшанг',
	  IAR: 'Ярославль',
	  BOH: 'Борнмут',
	  YEY: 'Эймос',
	  SQM: 'San Miguel Doaraguaia',
	  CUG: 'Кудал',
	  QPO: 'Потенца',
	  CEQ: 'Канн',
	  JUO: 'Журадо',
	  BWW: '',
	  PTJ: 'Портленд',
	  DHR: 'Ден Хелдер',
	  QYM: 'Амерсфорт',
	  TSN: 'Тяньцзинь',
	  RCL: 'Редклифф',
	  EGV: 'Игл-Ривер',
	  SCI: 'Сан-Кристобаль',
	  PKD: 'Парк Рэпидз',
	  YPG: 'Портидж-ла-Прери',
	  TLM: 'Тлемсен',
	  YBW: 'Бедвелл-Харбор',
	  SSG: 'Малабо',
	  IXI: 'Тилабари',
	  IKO: 'Nikolski',
	  LXN: 'Лексингтон',
	  ISM: 'Киссимми',
	  KOO: 'Конголо',
	  EBJ: 'Эсбьерг',
	  FAC: 'Faaite',
	  NKY: 'Nkayi',
	  ZFI: 'Честерфилд',
	  LNV: 'Лихир-Айленд',
	  YRJ: 'Роберваль',
	  ZTE: 'Рочестер',
	  LFK: 'Луфкин',
	  ZGU: 'Gaua',
	  ABU: 'Атамбуа',
	  HIS: 'Хейман-Айленд',
	  WIU: 'Witu',
	  YYI: 'Rivers',
	  NRL: 'Норт Роландсей',
	  PAG: 'Пагадиан',
	  JIJ: 'Джиджига',
	  HMT: 'Хемет',
	  XWI: 'Виган',
	  CBB: 'Кочабамба',
	  LAN: 'Лансинг',
	  SWD: 'Севард',
	  RRR: '',
	  DRK: '',
	  ZSR: 'Шверин',
	  GWE: 'Гверу',
	  UDD: 'Палм-Дезерт',
	  NJF: '',
	  MPL: 'Монпелье',
	  ZQK: 'Viersen',
	  FNE: 'Fane',
	  TTN: 'Трентон',
	  KCH: 'Кучинг',
	  LPI: 'Линчепинг',
	  HAS: 'Хаиль',
	  ERZ: 'Эрзурум',
	  QWX: 'Мерида',
	  HBE: 'Борк Эль Араб',
	  XSV: 'Сенлис',
	  ZFV: 'Железнодорожная станция Филадельфия',
	  QOM: 'Omiya',
	  QSA: 'Сабадель',
	  REN: 'Оренбург',
	  SDD: 'Лубанго',
	  XBH: 'Бетюн',
	  EMT: 'Эль Монте',
	  JSU: 'Манитсок',
	  NNK: 'Накнек',
	  KKJ: 'Кита Кюсю',
	  ETD: 'Этадунна',
	  OUR: 'Батури',
	  BGM: 'Бингемтон',
	  CJN: 'Эль Кахон',
	  KMO: 'Манокотак',
	  IKK: 'Канкаки',
	  TCX: 'Табас',
	  FYU: 'Форт-Юкон',
	  XMJ: 'Монт де Марсан',
	  RIF: 'Ричфилд',
	  QWU: 'Вюрцбург',
	  MLL: 'Маршалл',
	  DUC: 'Дункан',
	  KMU: 'Kismayu',
	  LKA: 'Ларантука',
	  RNL: 'Реннелл',
	  ETR: 'Etrepagny',
	  XSS: 'Суассон',
	  JIW: 'Jiwani',
	  LDH: 'Лорд Хоув-Айленд',
	  TTL: 'Turtle Island',
	  BOU: 'Борже',
	  JUJ: 'Хухуй',
	  GEV: 'Галливаре',
	  TST: 'Транг',
	  BES: 'Брест',
	  YLL: 'Ллойдминстер',
	  ZSM: 'Santa Clara',
	  MSB: 'Мариго Сен-Мартен',
	  AYK: '',
	  RAW: 'Арава',
	  MUE: 'Камуэла',
	  KRT: 'Хартум',
	  OSP: 'Слупск',
	  QCN: 'Канела',
	  BKI: 'Кота-Кинабалу',
	  MRC: 'Колумбия',
	  ZHF: 'Фрибур',
	  UNG: 'Киунга',
	  JON: 'Johnston Island',
	  JLD: 'Ландскруна',
	  UTW: 'Куинстаун',
	  SJW: 'Шицзяжун',
	  ZFR: 'Франкфурт-на-Одере',
	  TNO: 'Тамариндо',
	  DIN: 'Дьен Бьен Фу',
	  PSP: 'Палм-Спрингс',
	  MKP: 'Макемо',
	  RVK: 'Рорвик',
	  IMF: 'Имфал',
	  RUA: 'Аруа',
	  CGA: 'Креиг',
	  WKA: 'Ванака',
	  IXG: 'Белгаум',
	  BTU: 'Бинтулу',
	  ORH: 'Ворсестер',
	  CST: 'Castaway',
	  CLM: 'Порт Анджелес',
	  CSM: 'Клинтон',
	  BTZ: 'Бурса',
	  ZYQ: 'Сиракус',
	  QCJ: 'Botucatu',
	  KKX: 'Kikaiga Shima',
	  KED: 'Kaedi',
	  LTA: 'Цанин',
	  YIB: 'Атикокан',
	  AAN: 'Аль-Айн',
	  TIM: 'Тимика',
	  YVE: 'Вернон',
	  DEF: 'Dezful',
	  GFF: 'Гриффит',
	  BWD: 'Браунвуд',
	  SRX: 'Серт',
	  NAV: 'Невсехир',
	  DIQ: 'Divinopolis',
	  EUG: 'Юджин',
	  OVA: 'Бекили',
	  MXM: 'Моромбе',
	  ZJM: 'Martigny',
	  TID: 'Тиарет',
	  NDU: 'Рунда',
	  SAH: 'Сана',
	  GGW: 'Глазго',
	  LOS: 'Лагос',
	  IMT: 'Айрон-Маунтин',
	  RMK: 'Ренмарк',
	  DGO: 'Дуранго',
	  BHO: 'Бхопал',
	  YAP: 'Яп',
	  DKS: '',
	  RAQ: 'Raha',
	  LCC: 'Лечче',
	  URO: 'Руан',
	  CIL: 'Каунсил',
	  YQW: 'Норт-Батлфорд',
	  BGZ: 'Брага',
	  TED: 'Тистед',
	  TGK: 'Таганрог',
	  XPX: 'Pointe Aux Trembles',
	  CPN: 'Cape Rodney',
	  XGK: 'Кото',
	  QHV: 'Ново-Хамбурго',
	  QCQ: 'Карагуататуба',
	  LCF: 'Рио Дульче',
	  XTB: 'Тарбе',
	  LVA: 'Лаваль',
	  ISC: 'Сцилли, острова',
	  XCV: 'Шантийи',
	  YKL: 'Шеффервилл',
	  TEO: 'Терапо',
	  HLQ: 'Agrinion',
	  YRV: 'Ревелстоук',
	  LKB: 'Лакеба',
	  DEH: 'Декора',
	  EMO: 'Emo PG',
	  MRG: 'Мариба',
	  YWL: 'Уильямз-Лейк',
	  MMD: 'Minami Daito',
	  ZBM: 'Бромонт',
	  CTD: 'Читре',
	  OVS: 'СОВЕТСКИЙ ',
	  TXK: 'Тексаркана',
	  YBA: 'Банфф',
	  NVI: 'Навои',
	  FCN: 'Куксхафен',
	  DKK: 'Dunkirk',
	  JAG: 'Джакобабад',
	  LPO: 'Ла Порте',
	  LYG: 'Лианьюнан',
	  BKC: 'Бакленд',
	  YKT: 'Klemtu',
	  MWH: 'Мозес Лейк',
	  QWR: 'Donauwoerth',
	  ZWV: '',
	  XEP: 'Эперне',
	  QLH: 'Kelsterbach',
	  TKJ: 'Ток',
	  SIT: 'Ситка',
	  BZI: 'Баликесир',
	  CRI: 'Крукед-Айленд',
	  VLL: 'Вальядолид',
	  AXD: 'Александруполис',
	  TNE: 'Танегасима',
	  CBW: 'Campo Mourao',
	  RAS: 'Рашт',
	  DLG: 'Диллингам',
	  WMC: 'Виннемукка',
	  HLB: 'Бейтсвилль',
	  CNX: 'Чианг-Май',
	  HUI: 'Гуе',
	  PMG: 'Понта-Пора',
	  QKQ: 'Анклам',
	  SMW: 'Смара',
	  POW: 'Портороз',
	  BXJ: 'Бурундай',
	  LAB: 'Лаблаб',
	  PYH: 'Пуэрто-Аякучо',
	  YYT: 'Сент-Джонс',
	  RTG: 'Ruteng',
	  EBH: 'Hoevenen',
	  BNC: 'Бени',
	  GRD: 'Гринвуд',
	  GLO: 'Глостер',
	  SDR: 'Сантандер',
	  JSY: 'Сирос-Айленд',
	  KKT: 'Кентланд',
	  SZX: 'Шэньчжэнь',
	  SBV: 'Сабах',
	  YXP: 'Пангниртунг',
	  DGP: 'Даугавпилс',
	  SBS: 'Стимбоут-Спрингз',
	  CWT: 'Коура',
	  UBA: 'Убераба',
	  TNG: 'Танжер',
	  SUL: 'Sui PK',
	  EPR: 'Эсперанс',
	  XTH: 'Тионвилль',
	  XVF: 'Вильфранш-сюр-Сон',
	  RIC: 'Ричмонд',
	  GNV: 'Гейнсвилль',
	  HKD: 'Хакодате',
	  NAM: 'Намлеа',
	  UVF: 'Вье Форт Сан Люсия',
	  MNB: 'Моанда',
	  LFW: 'Ломе',
	  XKI: 'Лиллестром',
	  POZ: 'Познань',
	  VVZ: 'Иллизи',
	  BOX: 'Борролула',
	  INO: 'Инонго',
	  IMP: 'Императриз',
	  SEO: 'Seguela',
	  KDO: 'Kadhdhoo',
	  TSM: 'Таос',
	  MOT: 'Минот',
	  DJE: 'Джерба',
	  SDK: 'Сандакан',
	  ZPJ: 'Ратинген',
	  BBN: 'Барио',
	  RYO: 'Rio Turbio',
	  FOB: 'Fort Bragg',
	  YND: 'Гатино Халл',
	  PGK: 'Пангкалпинанг',
	  UKI: 'Укайа',
	  PCT: 'Принстон',
	  CAZ: 'Кобар',
	  KFP: 'Фолс-Пасс',
	  SJC: 'Сан-Хосе',
	  YXS: 'Принс Джордж',
	  XCP: 'Компьен',
	  TMM: 'Таматаве',
	  QJV: 'Скаген',
	  KTB: 'Торн Бэй',
	  OVD: 'Астуриас',
	  UKB: 'Кобе',
	  HSZ: 'Схинчу',
	  MKO: 'Маскоги',
	  VME: 'Вилья-Мерседес',
	  SLY: 'Салехард',
	  CDD: 'Cauquira',
	  LCJ: 'Лодзь',
	  LWR: 'Лиуварден',
	  TIH: 'Тикехау',
	  YNM: 'Матагами',
	  RDD: 'Реддинг',
	  RGI: 'о.Рангироа',
	  BNX: 'Баня-Лука',
	  QGS: 'Alagoinhas',
	  BFO: 'Buffalo Range',
	  RDV: 'Ред-Девил',
	  UTC: 'Утрехт',
	  YJT: 'Стефенвилль',
	  SCT: 'Сокотра',
	  XCW: 'Шамон',
	  JOS: 'Йос NG',
	  OLH: 'Олд-Харбор',
	  YHE: 'Хоуп',
	  QAQ: 'Лаквила',
	  LJN: 'Лейк Джексон',
	  RDT: 'Richard Toll',
	  ZOM: 'Minden',
	  RJA: 'Раджахмундри',
	  QRL: 'Марбелья',
	  NQU: 'Nuqui',
	  ITB: 'Itaituba',
	  HIB: 'Хиббинг',
	  FLO: 'Флоренс',
	  OBZ: '',
	  XCZ: 'Шарлевиль-Мезьер',
	  TJQ: 'Таньюнг-Пендан',
	  KMR: 'Karimui',
	  BNU: 'Блюменау',
	  GXQ: 'Койайке',
	  BSP: 'Bensbach',
	  GDX: 'Магадан',
	  BDJ: 'Банджармасин',
	  BFL: 'Бейкерсфилд',
	  YDF: 'Дир-Лейк',
	  SVB: 'Самбава',
	  ZQC: 'Speyer',
	  NDY: 'Сендэй',
	  KIY: 'Килва',
	  KVR: 'Кавалерово',
	  ZFZ: 'Баффало',
	  ZWE: '',
	  JYV: 'Ювяскюля',
	  ABE: 'Аллентаун',
	  ZHY: '',
	  TCD: 'Тарапака',
	  QIQ: 'Рио-Кларо',
	  PDU: 'Пайсанду',
	  BBK: 'Касане',
	  QSS: 'Сассари',
	  XDL: 'Чандлер',
	  PHS: 'Пхитсанулок',
	  GUR: 'Алотау',
	  DRB: 'Дерби',
	  BQU: 'Port Elizabeth',
	  CCC: 'Кайо Коко',
	  PBP: 'Пунта Ислита',
	  ROH: 'Робинхуд',
	  KER: 'Керман',
	  BDR: 'Бриджпорт',
	  MRQ: 'Мариндюк',
	  OBM: 'Моробе',
	  KHE: 'Херсон',
	  YXK: 'Римуски',
	  LDM: 'Ludington',
	  NOA: 'Новра',
	  ZNV: 'Кобленц',
	  LGK: 'Лангкави',
	  EGO: 'Белгород',
	  HFD: 'Хартфорд',
	  MPN: 'Маунт-Плезант',
	  SBN: 'Соут Бенд',
	  CBS: 'Кабимас',
	  GAI: 'Гейтерсбург',
	  CYI: 'Чиайи',
	  CCU: 'Колката',
	  LIL: 'Лилль',
	  MVM: 'Monument Valley',
	  DIS: 'Loubomo',
	  MOL: 'Молде',
	  SHW: 'Шарура',
	  TGM: 'Тиргу Мурес',
	  EMD: 'Эмералд',
	  FRM: 'Фэрмонт',
	  XZK: 'Амхерст',
	  FLF: 'Фленсбург',
	  FTX: 'Овандо',
	  VLD: 'Вальдоста',
	  AUQ: 'Атуона',
	  ZUM: 'Черчхил Фолз',
	  TTD: 'Траутдейл',
	  MBD: 'Ммабато',
	  KYA: 'Конья',
	  IGR: 'Игуасу',
	  NBR: 'Намбур',
	  NLL: 'Нулладжин',
	  EEK: 'Eek',
	  YHS: 'Сечельт',
	  SBU: 'Спрингбок',
	  LCG: 'Ла-Корунья',
	  SWF: 'Ньюбург',
	  PWT: 'Бремертон',
	  HSM: 'Оршам',
	  NKU: 'Нкаус',
	  QPS: 'Пирассунунга',
	  NTI: 'Бинтуни',
	  NGA: 'Янг',
	  LSQ: 'Лос-Анджелес',
	  EUM: 'Neumuenster',
	  CAJ: 'Канайма',
	  SZC: 'Санта-Крус',
	  WRE: 'Вангарей',
	  AET: 'Аллакакет',
	  GYN: 'Гояния',
	  TTA: 'Тан-Тан',
	  FWA: 'Форт-Уэйн',
	  CPB: 'Капургана',
	  TEP: 'Тептеп',
	  CFB: 'Кабо Фрио',
	  NSN: 'Нельсон',
	  LBR: 'Лабрея',
	  LUV: 'Langgur',
	  NLC: 'Лемор',
	  PKW: 'Селеби-Фикве',
	  KXF: '',
	  NMG: 'Сан-Мигель',
	  AMI: 'Матарам',
	  RTS: 'Роттнест',
	  MTP: 'Монтаук',
	  AFA: 'Сан-Рафаэль',
	  EGN: 'Генейне',
	  VKG: 'Рач-Дже',
	  PPP: 'Прозерпине',
	  EGE: 'Игл',
	  CZE: 'Коро',
	  ZLO: 'Манзанилло',
	  CCE: 'Сен-Мартен',
	  GWT: 'Вестерленд',
	  HHQ: 'Хуа Хин',
	  WIC: 'Вик',
	  VYS: 'Перу',
	  TIN: 'Тиндуф',
	  YZG: 'Саллуит',
	  SZS: 'Stewart Island',
	  QSJ: 'Sao Joao Del Rei',
	  PYJ: 'Полярный',
	  KIR: 'Керри-Канти',
	  MHQ: 'Мариехамн',
	  QFB: 'Фрайбург',
	  MWO: 'Мидлтаун',
	  BHR: 'Бхаратпур',
	  PIH: 'Покателло',
	  CCK: 'Кокосовы о-ва',
	  GBQ: 'Мухаррак',
	  PQI: 'Преск Исл',
	  TRG: 'Тауранга',
	  ZQJ: 'Velbert',
	  AKN: 'Кинг Салмон',
	  RFD: 'Рокфор',
	  TRA: 'Taramajima',
	  MRW: 'Марибо',
	  XTL: 'Tadoule Lake',
	  YLI: 'Юливиеска',
	  PTO: 'Пату-Бранку',
	  OWB: 'Овенсборо',
	  ZYS: 'Сандефьорд',
	  NDO: 'Ла-Пальма-дель-Кондадо',
	  SOQ: 'Соронг',
	  GRZ: 'Грац',
	  MDS: 'Middle Caicos',
	  YQL: 'Летбридж',
	  ORM: 'Нортхэмптон',
	  PTF: 'Малололайлай',
	  GLZ: 'Бреда',
	  YRL: 'Ред-Лейк',
	  CLC: 'Клеар Лейк Сити',
	  LBW: 'Лонг-Баван',
	  JHQ: 'Шут-Харбор',
	  YAY: 'Сент Антони',
	  TOW: 'Толедо',
	  YQM: 'Монктон',
	  LCI: 'Лакониа',
	  UTO: 'Утопия',
	  DYA: 'Дайсарт',
	  PGZ: 'Понта-Гросса',
	  CEO: 'Waco Kungo',
	  BJW: 'Баджава',
	  CJJ: 'Чеонджу',
	  CCX: 'Caceres',
	  YPB: 'Порт-Алберни',
	  BET: 'Бетел',
	  THQ: '',
	  TCG: 'Tacheng',
	  SSM: 'Со Сен Мари',
	  QDH: 'Эшфорд',
	  TAH: 'Танна',
	  QOA: 'Мокока',
	  APZ: 'Запала',
	  CXO: 'Конроу',
	  LZR: 'Лизард-Айленд',
	  PRH: 'Пхраэ',
	  MKD: 'Metekel',
	  XMS: '',
	  LTK: 'Латакия',
	  XGJ: 'Кобург',
	  GQQ: 'Галион',
	  AUZ: 'Аврора',
	  MHY: 'Morehead',
	  KOA: 'Кона',
	  YKA: 'Камлупс',
	  KCA: 'Куча',
	  JLO: 'Jesolo',
	  OES: 'San Antonio Oeste',
	  AUF: 'Осер',
	  DRT: 'Дель Рио',
	  TMG: 'Томангонг',
	  SBL: 'Санта-Ана',
	  NZH: 'Маньчжурия',
	  PXU: 'Плейку',
	  PEF: 'Пенемюнде',
	  MKY: 'Маккей',
	  ZCJ: 'Bottrop',
	  PSN: 'Палестина',
	  MVN: 'Маунт-Вернон',
	  RNJ: 'Yoronjima',
	  EFL: 'Кефалония',
	  INW: 'Уинслоу',
	  YEN: 'Эстеван',
	  BMI: 'Блумингтон',
	  MVR: 'Мароуа',
	  KOJ: 'Кагосима',
	  EPS: 'Самана',
	  DMU: 'Димапур',
	  QIS: 'Мито',
	  YEI: 'Бурса',
	  URG: 'Уругуайна',
	  KUS: 'Кулусук',
	  CWB: 'Куритиба',
	  AFS: 'Зарафшан',
	  ODN: 'Лонг-Серидан',
	  YHK: 'Йоа-Хайвен',
	  PBS: 'Патонг-Бич',
	  XYZ: 'Harnosand',
	  DPL: 'Диполог',
	  SNN: 'Шеннон',
	  GGR: 'Garoe',
	  WEF: 'Вэйфан',
	  EJH: 'Веджх',
	  SXP: 'Шелдон Пойнт',
	  AUP: 'Агаун',
	  QHP: 'Таубате',
	  QEI: 'Crailsheim',
	  CPD: 'Кубер Педи',
	  TUB: 'Tubuai',
	  RNA: 'Арона',
	  GUU: 'Грундарфьордур',
	  IZO: 'Изумо',
	  BLX: 'Беллуно',
	  VEL: 'Вернал',
	  OTD: 'Контадора',
	  YER: 'Форт Северн',
	  OSM: 'Мосул',
	  ONB: 'Ононге',
	  ANR: 'Антверпен',
	  NGC: 'North Grand Canyon',
	  NHA: 'Нха Транг',
	  CBI: '',
	  SHY: 'Шиньянга',
	  KCZ: 'Кочи',
	  ABR: 'Абердин',
	  MUX: 'Мултан',
	  REH: 'Rehoboth Beach',
	  WNA: 'Напакиак',
	  SKC: 'Суки',
	  SEF: 'Себринг',
	  DKI: 'Данк-Айленд',
	  JOL: '',
	  SPP: 'Менонге',
	  IPT: 'Уильямспорт',
	  IZT: 'Ихтепек',
	  KKQ: '',
	  PCM: 'Плайя-дель-Кармен',
	  WTP: 'Войтапе',
	  BJX: 'Леон',
	  WVI: 'Ватсонвилль',
	  PFQ: 'Парсабад',
	  BYR: 'Лаесо-Айленд',
	  DAT: 'Датон',
	  BZM: 'Берген Оп Зоом',
	  VLG: 'Вилья-Хесель',
	  BWC: 'Броули',
	  HCR: 'Holy Cross',
	  QOT: 'Отару',
	  MTS: 'Манзини',
	  MWK: 'Матак',
	  IQN: 'Qingyang',
	  HGO: 'Корого',
	  POX: 'Понтуаз',
	  SCO: 'Актау',
	  HOF: 'Хофуф',
	  KRO: 'Курган',
	  YQZ: 'Квеснел',
	  PUX: 'Пуэрто-Варас',
	  ORE: 'Орлеан',
	  RJL: 'Логроно',
	  YUT: '',
	  WUN: 'Вилуна',
	  XAP: 'Чапеко',
	  KLF: 'Калуга',
	  KBJ: 'Кингз Каньон',
	  SJL: 'Сан-Габриэл',
	  BSG: 'Бата',
	  LOD: 'Лонгана',
	  XVH: 'Питерборо',
	  BRV: 'Бремерхафен',
	  BZC: 'Бузиос',
	  SOP: 'Пайнхерст',
	  AKA: 'Анькан',
	  JAQ: 'Jacquinot',
	  RTN: 'Ратон',
	  VIJ: 'Виржин-Горда',
	  SEP: 'Стефенвилль',
	  SXL: 'Слайго',
	  MWD: 'Mianwali',
	  FUT: 'Футуна',
	  DGK: 'Dugong',
	  BDD: 'о. Баду',
	  QHA: 'Hasselt',
	  ANG: 'Ангулем',
	  IWA: 'Иваново',
	  QRW: 'Warri',
	  HSL: 'Хаслия',
	  MOR: 'Морристаун',
	  WJR: 'Ваджир',
	  WHS: 'Whalsay',
	  QWJ: 'Americana',
	  XBM: 'Болье сюр Мер',
	  FLT: 'Flat',
	  BME: 'Брум',
	  ATS: 'Artesia',
	  MXT: 'Майнтирано',
	  SAI: 'Сан-Марино',
	  PBF: 'Пайн Блафф',
	  BXI: 'Boundiali',
	  REG: 'Реджо Калабрия',
	  BLG: 'Белага',
	  YCH: 'Мирамичи',
	  TEM: 'Темора',
	  NUK: 'Нукутаваке',
	  TAE: 'Даегу',
	  FJR: 'Аль Фуджейра',
	  ROK: 'Рокхэмптон',
	  YLH: 'Лансдаун',
	  QHU: 'Хусум',
	  KYX: 'Yalumet',
	  QRP: 'Грамадо',
	  EDW: 'Edwards',
	  ZVE: 'Нью-Хейвен',
	  DMD: 'Думаджи',
	  XCX: 'Шателлеро',
	  JNS: 'Нарсак',
	  OPA: 'Копаскер',
	  RKC: 'Yreka',
	  QGN: 'Таррагона',
	  GIF: 'Уинтер-Хейвен',
	  BMP: 'Бремптон-Айленд',
	  GLI: 'Глен-Иннс',
	  ZOY: 'Оберхаузен',
	  INQ: 'Inisheer',
	  XQH: 'Дерби',
	  AGF: 'Аген',
	  KBL: 'Кабул',
	  XFQ: 'Weymont',
	  QLC: 'Gliwice',
	  AZG: 'Апацинган',
	  PJB: 'Пейсон',
	  LUP: 'Калаупапа',
	  SBR: 'Сайбей-Айленд',
	  STC: 'Сент Клуд',
	  BWA: 'Бхайрава',
	  KKD: 'Кокода',
	  LOM: 'Лагос-Де-Морено',
	  QNM: 'Намуре',
	  PJM: 'Пуэрто-Хименес',
	  MZW: 'Mechria',
	  CIW: 'Canouan Island',
	  AES: 'Олесунд',
	  EER: '',
	  CJM: 'Чампхон',
	  FRB: 'Форбс',
	  XBW: 'Киллинек',
	  AXT: 'Акита',
	  HKB: 'Хили Лейк',
	  ROR: 'Корор',
	  DTI: '',
	  UEE: 'Квинстон',
	  YHI: 'Холман Айленд',
	  TNC: 'Тин Сити',
	  AKX: 'Актюбинск',
	  NUS: 'Norsup',
	  TND: 'Тринидад',
	  KCD: 'Kamur',
	  COK: 'Кочи',
	  ISU: 'Сулеймании',
	  GTA: 'Gatokae',
	  YLQ: 'Ла-Тюк',
	  QAM: 'Амьен',
	  RUI: 'Руидозо',
	  PVU: 'Прово',
	  STS: 'Санта-Роза',
	  PSC: 'Паско',
	  AAL: 'Альборг',
	  NSV: 'Нусавилль',
	  MVW: 'Маунт-Вернон',
	  RNO: 'Рено',
	  FCY: 'Форест-Сити',
	  PSY: 'Порт-Стэнли',
	  MXZ: 'Мейсян',
	  BPY: 'Besalampy',
	  DGT: 'Dumaguete',
	  XAW: 'Капреол',
	  EVG: 'Свег',
	  KPI: 'Капит',
	  ZQA: 'Singen',
	  TLD: 'Тули-Блок',
	  QPB: 'Кампобассо',
	  ZPA: 'Offenburg',
	  UET: 'Кветта',
	  SNS: 'Салинас',
	  CKG: 'Чонсинь',
	  DOA: 'Doany',
	  XCM: 'Четэм',
	  ANE: 'Ангерс',
	  QYR: 'Труа',
	  JBQ: '',
	  KYP: 'Кьяукпью',
	  KXU: 'Katiu',
	  GCK: 'Гарден-Сити',
	  POO: 'Pocos De Caldas',
	  FTU: 'Форт-Дофин',
	  XZD: 'Kongsvinger',
	  QUO: '',
	  XTU: 'Тюль',
	  GNA: 'Гродно',
	  KVL: 'Кивалина',
	  IGH: 'Ингэм',
	  XIA: 'Guelph',
	  VAW: 'Вардо',
	  MWY: 'Миранда-Даунз',
	  DJJ: 'Джаяпура',
	  SKU: 'Скирос',
	  EIS: 'Биф-Айленд',
	  KMM: 'Кимам',
	  KKC: 'Кхон Каен',
	  TDD: 'Тринидад',
	  HEV: 'Хуэльва',
	  AQP: 'Арекуипа',
	  XAV: 'Альбервиль',
	  PET: 'Пелотас',
	  TBB: 'Туй-Хоа',
	  GDE: 'Годе',
	  MNT: 'Минто',
	  SNA: 'Санта-Ана',
	  BLH: 'Блит',
	  BGR: 'Бангор',
	  SSZ: 'Сантос',
	  KEY: 'Керичо',
	  ZBG: 'Elblag',
	  XIC: 'Хайчэн',
	  GUI: 'Guiria',
	  RVN: 'Рованиеми',
	  FOT: 'Форстер',
	  JIM: 'Джимма',
	  IYK: 'Иниокерн',
	  KSA: 'Косраэ',
	  LGL: 'Лонг-Лелланг',
	  DJO: 'Daloa',
	  QND: 'Нови-Сад',
	  BHG: 'Брюс-Лагуна',
	  LKO: 'Лакнау',
	  LBV: 'Либревилль',
	  ACD: 'Аканди',
	  GRS: 'Гросето',
	  MID: 'Мерида',
	  YFX: 'Фокс Харбор',
	  SUF: 'Ламезия Терме',
	  UAQ: 'Сан-Хуан',
	  IPI: 'Ипиалес',
	  SYX: 'Санья',
	  OFJ: 'Олафсфьордур',
	  TSB: 'Цумеб',
	  MXX: 'Мора',
	  KLX: 'Каламата',
	  ENU: 'Энугу',
	  LPH: 'Лохгилпхед',
	  XZU: '',
	  SBY: 'Салисбери Оушен Сити',
	  GBJ: 'Marie Galante',
	  YTM: 'Мон Тремблан',
	  OAG: 'Оранж',
	  QUR: 'Muriae',
	  IQA: 'Икалуит',
	  JQE: 'Жак',
	  DNB: 'Данбар',
	  QLZ: '',
	  OTM: 'Оттумва',
	  AZN: 'Андижан',
	  WWY: 'Вест-Вьялонг',
	  RKD: 'Рокленд',
	  XJL: 'Joliette',
	  SWJ: 'Сауз-Вест-Бей',
	  DBQ: 'Дюбюк',
	  ATQ: 'Амритсар',
	  QTO: 'Скитубе',
	  BLT: 'Блэкуотер',
	  UIR: 'Куиринди',
	  LSO: 'Лес Саблес',
	  UWP: 'Вупперталь',
	  ZOV: 'Norderstedt',
	  XEM: 'Нью-Ричмонд',
	  BGA: 'Букараманга',
	  TOC: 'Toccoa',
	  MNG: 'Манингрида',
	  LWM: 'Lawrence',
	  VAP: 'Вальпараисо',
	  IHU: 'Ihu Pg',
	  PSU: 'Putussibau',
	  JKV: 'Джексонвилл',
	  ZDL: 'Brig',
	  HLG: 'Уилинг',
	  PTW: 'Поттстаун',
	  HOQ: 'Хоф',
	  MKX: 'Мукалла',
	  BBI: 'Бхубанесвар',
	  BRL: 'Берлингтон',
	  XKK: 'Ларвик',
	  IVL: 'Ивало',
	  LIP: 'Lins',
	  CHT: 'Чатем острова',
	  DEZ: 'Дейр Эз Зор',
	  RZZ: 'Роанок Рэпидз',
	  LNB: 'Ламен Бэй',
	  NNT: 'Нан Таиланд',
	  JDO: 'Жуазейру-ду-Норти',
	  ESD: 'Ист Саунд',
	  MCV: 'Макартур-Ривер',
	  CRX: 'Коринф',
	  XOK: 'Оуквиль',
	  UIP: 'Куимпер',
	  PGU: '',
	  BEH: 'Бентон-Харбор',
	  LNX: 'Смоленск',
	  VOZ: 'Воронеж',
	  VOL: 'Волос',
	  ETS: 'Энтерпрайз',
	  MBE: 'Monbetsu',
	  RMG: 'Рим',
	  ATY: 'Уотертаун',
	  MIV: 'Миллвилль',
	  MMJ: 'Мацумото',
	  YNB: 'Янбо',
	  PLP: 'Ля Пальма',
	  LBC: 'Любек',
	  SYZ: 'Шираз',
	  NST: 'Нахон Си Таммарат',
	  SFL: 'Сан-Филипе',
	  IMK: 'Симикот',
	  IOM: 'Айл-оф-Мэн',
	  JUM: 'Джумла',
	  ENV: 'Wendover',
	  GYS: 'Гуаньюань',
	  SFZ: 'Смитфилд',
	  CMR: 'Колмар',
	  PVN: 'Плевен',
	  TUG: 'Тугуэгарао',
	  QIA: 'Итауна',
	  ZJS: 'Йена',
	  UTS: '',
	  TEN: 'Тонгрен',
	  WWP: 'Whale Pass',
	  KIH: 'Киш, остров',
	  ERF: 'Эрфурт',
	  YSC: 'Шербрук',
	  XWO: 'Уокинг',
	  GSQ: 'Shark Elowainat',
	  MSZ: 'Намибе',
	  GVL: 'Гейнсвилль',
	  CSI: 'Касино',
	  VDI: 'Видалия',
	  OSS: 'Ош',
	  LMY: 'Лейк Мюррей',
	  ZMG: 'Магдебург',
	  ZCL: 'Закатекас',
	  ABK: 'Кабри-дар',
	  VDD: '',
	  LLB: '',
	  KSG: 'Kisengan',
	  ACS: 'Ачинск',
	  BPB: 'Boridi',
	  PTK: 'Понтиак',
	  YXH: 'Медисин-Хат',
	  RYN: 'Ройан',
	  YXZ: 'Вава',
	  OTU: 'Оту Колумбия',
	  GSP: 'Гринвилль',
	  PCD: 'Прейри-дю-Чиен',
	  PNV: 'Panevezys',
	  SAL: 'Сан-Сальвадор',
	  BUO: 'Бурао',
	  SNO: 'Сакон-Нахон',
	  HVS: 'Hartsville',
	  LPY: 'Ле Пуй',
	  TKD: 'Такоради',
	  SXD: 'София Антиполис',
	  TMS: 'Сан-Томе',
	  LQM: 'Пуэрто-Легуизамо',
	  BQN: 'Агуадилла',
	  PTU: 'Платинум',
	  KKY: 'Килкенни',
	  ISK: 'Насик',
	  SCM: 'Скаммон-Бей',
	  VAL: 'Valenca',
	  ZEY: 'Hagen',
	  JDF: 'Жуис-ди-Фора',
	  AAM: 'Мала Мала',
	  AJA: 'Аяччо',
	  APO: 'Апартадо',
	  CAY: 'Кайенна',
	  RIS: 'Рисири',
	  EDA: 'Эдна Бэй',
	  AOL: 'Пасо де лос Либрес',
	  KMT: 'Кампот',
	  TAT: 'Татры Попрад',
	  KNG: 'Kaimana',
	  BRT: 'Батерст-Айленд',
	  FCA: 'Калиспелл',
	  KGH: 'Йонгай',
	  NDE: 'Мандера',
	  QAL: 'Алессандрия',
	  UUU: 'мануму',
	  VRO: 'Матанзас',
	  UVL: 'Харга',
	  AMA: 'Амарилло',
	  CYB: 'Кайман-Брак',
	  SRG: 'Семаранг',
	  BUK: 'Albuq',
	  AAO: 'Анако',
	  MUU: 'Маунт-Юнион',
	  TLO: 'Tol PG',
	  RBI: 'Раби',
	  UTN: 'Апингтон',
	  RAN: 'Равенна',
	  ATK: 'Аткасук',
	  EZS: 'Элязыг',
	  HKA: 'Блайтвилль',
	  BBA: 'Балмаседа',
	  FRC: 'Franca',
	  DMB: 'Тараз',
	  ZEE: 'Fulda',
	  CUQ: 'Коэн',
	  NOC: 'Нок',
	  OAM: 'Оамару',
	  VAS: 'Сивас',
	  JFM: 'Фримантл',
	  RVT: 'Равенсторп',
	  WLS: 'о.Уоллис',
	  YIH: 'Ичанг',
	  QHG: 'Sete Lagoas',
	  GCM: 'Большой Кайман',
	  TBT: 'Табатинга',
	  YBR: 'Брэндон',
	  EGC: 'Бержерак',
	  UNK: 'Уналаклит',
	  MFH: 'Мескит',
	  KRI: 'Кикори',
	  IBA: 'Ibadan',
	  DJB: 'Джамби',
	  MCH: 'Machala',
	  ZOS: 'Осорно',
	  LWT: 'Льюистоун',
	  AEI: 'Альгесирас',
	  NSZ: '',
	  BQG: 'Богородское',
	  JAD: 'Джандакот',
	  SRA: 'Санта-Роса',
	  RVD: 'Рио-Верде',
	  OCV: 'Окана',
	  PSE: 'Понс',
	  YQE: 'Кимберли',
	  MZK: 'Маракай',
	  GTN: 'Маунт-Кук',
	  XZZ: '',
	  SFD: 'Сан-Фернандо',
	  MJA: 'Маня',
	  BSR: 'Басра',
	  AOI: 'Анкона',
	  RCU: 'Рио-Кварто',
	  QWQ: 'Струер',
	  ZAU: 'Aue De',
	  TBH: 'Таблас',
	  VLO: 'Валлехо',
	  JQA: 'Qaarsut',
	  QGU: 'Гифу',
	  MPQ: 'Маан',
	  TIU: 'Тимару',
	  EOI: 'Эдей',
	  SNP: 'St Paul Island',
	  SCW: 'Сыктывкар',
	  JOI: 'Джойнвилль',
	  XDI: 'Динь',
	  XWV: 'Varberg',
	  QKN: 'Каируан',
	  CAB: 'Кабинда',
	  TWA: 'Твин-Хиллз',
	  YZS: 'Корал-Харбор',
	  HBA: 'Хобарт',
	  DNQ: 'Дениликуин',
	  HSK: 'Уеска',
	  OKI: 'Oki Island',
	  MLN: 'Мелилья',
	  PZK: 'о.Пукапука',
	  NIB: 'Николай',
	  YZP: 'Сандспит',
	  PMR: 'Пальмерстон',
	  HYS: 'Хэйс',
	  SCE: 'Стейт-Колледж',
	  RBW: 'Уолтерборо',
	  XPN: 'Брэмптон',
	  SUX: 'Сиокс Сити',
	  WGN: 'Ваитанги',
	  NFL: 'Фаллон',
	  CLU: 'Колумбус',
	  OGS: 'Огденсбург',
	  CSF: 'Крей',
	  MMO: 'Майу',
	  MZI: 'Мопти',
	  BSD: 'Баошань',
	  TFL: 'Teofilo Otoni',
	  FMS: 'Форт-Мэдисон',
	  KJK: 'Кортрийк',
	  ARL: 'Арли',
	  BLE: 'Борланж',
	  TIS: 'Терсдей-Айленд',
	  PRA: 'Парана',
	  JHE: 'Хельсингборг',
	  OTR: 'Coto 47',
	  SZO: 'Шаньчжоу',
	  EGM: 'Сиж',
	  LVI: 'Ливингстон',
	  UBS: 'Columbus',
	  LKN: 'Лекнес',
	  ZCZ: 'Эрланген',
	  LWS: 'Льюистон',
	  BKB: 'Биканер',
	  LSC: 'Ла Серена',
	  TER: 'Терсейра',
	  MBS: 'Сэгино',
	  SLH: 'Сола',
	  LRT: 'Лоринт',
	  DOO: 'Dorobisoro',
	  IKS: 'Тикси',
	  DMO: 'Седалиа',
	  TMA: 'Тифтон',
	  RMT: '',
	  GLR: 'Гейлорд',
	  CDZ: 'Кадис',
	  KEW: 'Keewaywin',
	  QOG: 'Хомбург',
	  PEL: 'Pelaneng',
	  BZH: 'Буми-Хиллз',
	  KNT: 'Кеннет',
	  SDI: 'Saidor',
	  GPT: 'Галфпорт',
	  LMC: 'Lamacarena',
	  RHE: 'Реймс',
	  COW: 'Coquimbo',
	  YSY: 'Сакс Харбор',
	  BEI: 'Beica',
	  AFL: 'Алта-Флореста',
	  URE: 'Курессааре',
	  TAN: 'Тангалума',
	  GIL: 'Гилгит',
	  MAM: 'Матаморос',
	  XXP: 'Потсдам',
	  YHO: 'Хопедал',
	  PUL: 'Poulsbo',
	  PBM: 'Парамарибо',
	  MLX: 'Малатия',
	  NNY: 'Наньян',
	  PFB: 'Пассо-Фундо',
	  TLH: 'Таллахасси',
	  BNM: 'Bodinumu',
	  MQB: 'Макомб',
	  VAF: 'Валенсия',
	  YCD: 'Нанаймо',
	  TOA: 'Торранс',
	  YCL: 'Шарло',
	  FLR: 'Флоренция',
	  MMU: 'Морристаун',
	  LLI: 'Лалибела',
	  GEG: 'Спокане',
	  LTI: 'Алтай',
	  THL: 'Тачилек',
	  XVV: 'Бельвилль',
	  IJK: 'Ижевск',
	  YYN: 'Свифт-Каррент',
	  UKT: 'Квакертаун',
	  QGY: 'Gyor',
	  YVZ: 'Дир-Лейк',
	  HRI: 'Хамбантота',
	  PLV: 'Полтава',
	  HMV: 'Хемаван',
	  FAR: 'Фарго',
	  FST: 'Форт-Стоктон',
	  UTH: 'Удон-Тхани',
	  MXG: 'Марлборо',
	  KAT: 'Кайтайа',
	  BFJ: 'Ба',
	  KSZ: 'Котлас',
	  KYL: 'Ки Ларго',
	  LAQ: 'Бейда',
	  XBR: 'Броквиль',
	  NTN: 'Нормантон',
	  XHK: '',
	  NCA: 'Норт-Кайкос',
	  MTR: 'Монтерия',
	  KFS: 'Кастамону',
	  YRO: '',
	  QBC: 'Белла Кула',
	  IGO: 'Чигородо',
	  ARI: 'Арика',
	  MMG: 'Маунт-Магнет',
	  APC: 'Напа',
	  XQU: 'Кволикум',
	  PHE: 'Порт Хедленд',
	  YEL: 'Эллиот-Лейк',
	  PLQ: 'Паланга',
	  PBB: 'Паранаиба',
	  CCM: 'Крикиума',
	  AKV: 'Акуливик',
	  GGM: '',
	  ZKK: 'Schwyz',
	  BTV: 'Берлингтон',
	  YKM: 'Якима',
	  UKU: 'Нуку',
	  DKA: '',
	  ODY: 'Удомхей',
	  SOU: 'Саутгемптон',
	  ZGF: 'Grand Forks',
	  SDT: 'Саиду-Шариф',
	  BEL: 'Белем',
	  PKY: 'Палангкарайя',
	  AUC: 'Араука',
	  BVO: 'Бартлесвилль',
	  ACZ: 'Забол',
	  QFQ: 'Мелой',
	  MLW: 'Монровия',
	  XLM: '',
	  PEM: 'Пуэрто-Мальдонадо',
	  ILR: 'Ilorin',
	  PHF: 'Ньюпорт-Ньюс',
	  KDI: 'Кендари',
	  SPN: 'Сайпан',
	  GYE: 'Гуайяквил',
	  AIY: 'Атлантик-Сити',
	  NUL: 'Нулато',
	  ZDK: 'Biel Bienne',
	  APN: 'Альпена',
	  XMT: 'Ментон',
	  ONH: 'Онеонта',
	  BIK: 'Биак',
	  ELQ: 'Гассим',
	  MHV: 'Моджаве',
	  SHI: 'Shimojishima',
	  ESL: 'Элиста',
	  QNC: 'Neuchalet',
	  BIY: 'Бишо',
	  XPS: 'Провинс',
	  WLM: 'Уолтем',
	  CIP: 'Chipata',
	  AMY: 'Амбатомаинти',
	  ANW: 'Эйнсуорт',
	  MAO: 'Манаус',
	  MRX: 'Bandar Mahshahr',
	  RKU: 'Yule Island',
	  WTD: 'West End',
	  SDJ: 'Сендаи',
	  TCU: 'Таба Нчу',
	  WEL: 'Велком',
	  KNA: 'Вина-дель-Мар',
	  CVJ: 'Куернавака',
	  NZE: 'Нзерекоре',
	  QPZ: 'Пьяченца',
	  PKB: 'Паркерсбург',
	  FIE: 'Фейр-Айсл',
	  TYL: 'Талара',
	  MBI: 'Мбея',
	  BHV: 'Бехавалпур',
	  YIW: 'Иву',
	  BCI: 'Баркалдин',
	  ARB: 'Анн-Арбор',
	  CZS: 'Крузейро-ду-Сул',
	  YPA: 'Принс-Альберт',
	  MCD: 'Mackinac Island',
	  PGD: 'Пунта-Горда',
	  KDU: 'Скарду',
	  KOE: 'Купанг',
	  EXI: 'Экскуршен Инлет',
	  IKI: 'Ики Япония',
	  XFL: 'Шавиниган',
	  TUS: 'Туксон',
	  VER: 'Веракрус',
	  PSO: 'Пасто',
	  HOT: 'Хот Спрингз',
	  KLJ: 'Клайпеда',
	  BLO: 'Блондуос',
	  AFK: 'Ампара',
	  VNE: 'Ваннес',
	  AAA: 'Анаа',
	  MFC: 'Mafeteng',
	  XKJ: 'Steinkjer',
	  CFE: 'Клермон-Ферран',
	  QGC: 'Lencois Paulista',
	  GME: 'Гомель',
	  BSA: 'Боссасо',
	  JOT: 'Joliet',
	  OLA: 'Орленд',
	  MWU: 'Mussau',
	  NNR: 'Спиддал',
	  DTL: 'Детройт-Лейкс',
	  CAM: 'Камири',
	  ZLJ: 'Ивердон',
	  YPD: 'Парри-Саунд',
	  EIB: 'Айзенах',
	  HVB: 'Херви Бей',
	  YCY: 'Клайд-Ривер',
	  YOS: 'Оуэн-Саунд',
	  AAK: 'Аранука',
	  SEU: 'Серонера',
	  JPR: 'Джи-Парана',
	  LPS: 'Лопез-Айленд',
	  BAA: 'Биалла',
	  BYI: 'Берли',
	  SHH: 'Shismaref',
	  QYV: 'Девентер',
	  KEH: 'Кенмор',
	  EAR: 'Кеарней',
	  LHA: 'Лар',
	  LRA: 'Лариса',
	  WSN: 'Южный Нейкнек',
	  JIK: 'о-в Икария',
	  DLS: 'Даллас',
	  KAO: 'Куусамо',
	  SHX: 'Шагелек',
	  ICT: 'Вичита',
	  LEL: 'Лэйк-Эвелла',
	  OMC: 'Ормок',
	  RGS: '',
	  MJL: 'Муила',
	  FNA: 'Фритаун',
	  BAP: 'Байбара',
	  CWW: 'Короуа',
	  MFK: 'Мацу',
	  ERM: 'Erechim',
	  QSZ: 'Шицуока',
	  BCV: '',
	  PQM: 'Паленке',
	  MDQ: 'Мар-дель-Плата',
	  TNN: 'Тайнань',
	  IIS: 'Ниссан',
	  GOS: 'Госфорд',
	  STY: 'Сальто',
	  ROP: 'Рота',
	  MEG: 'Malange',
	  BHU: 'Бхавнагар',
	  YCB: 'Кембридж-Бей',
	  TWT: '',
	  BJR: 'Бахар-Дар',
	  KGE: 'Kagau',
	  KLH: 'Колхапур',
	  IOA: 'Иоаннина',
	  RIJ: 'Риоха',
	  KAE: 'Кейк',
	  RCH: 'Риохача',
	  QLW: 'Lavras',
	  OCE: 'Оушен-Сити',
	  GJL: 'Jijel',
	  SXB: 'Страсбург',
	  ERD: 'Бердянск',
	  WUS: 'Вуйшань',
	  GIB: 'Гибралтар',
	  LVK: 'Ливермор',
	  GZO: 'Гизо',
	  TKK: 'Трук',
	  XAZ: 'Кемпбелтон',
	  MYE: 'Miyakejima',
	  KBY: 'Стрики-Бэй',
	  TGO: 'Тонглян',
	  QVU: 'Вадуц',
	  YMM: 'Форт Мак-Мюррей',
	  JMO: 'Джомсом',
	  VIB: 'Вилья-Конститусьон',
	  EWD: 'Wildman Lake',
	  CTG: 'Картегена',
	  ABT: 'Эль-Баха',
	  BYN: 'Bayankhongor',
	  ARR: 'Alto Rio Senguerr',
	  BEK: 'Rae Bareli',
	  CAE: 'Колумбия',
	  JRO: 'Килиманджаро',
	  BQO: 'Bouna',
	  XZC: 'Гленко',
	  BTI: 'Бартер Айленд',
	  KWM: 'Кованьяма',
	  ANF: 'Антофагаста',
	  HEA: 'Герат',
	  NCI: 'Некокли',
	  CZM: 'Коцумел',
	  CJC: 'Калама',
	  SIF: 'Симара',
	  GOU: 'Гаруа',
	  PAD: 'Падерборн',
	  VBY: 'Висбю',
	  BJZ: 'Бадахос',
	  SGD: 'Сондерборг',
	  TRI: 'Бристоль',
	  ATA: 'Анта',
	  SRY: 'Сари',
	  EVM: 'Эвелет',
	  SFP: 'Серферз-Парадайз',
	  GHB: 'Говернорс-Харбор',
	  AML: 'Пуэрто-Армуэллес',
	  JGR: 'Groennedal',
	  KPB: 'Point Baker',
	  SPM: 'Шпангдалем',
	  CWL: 'Кардифф',
	  JAL: 'Халапа',
	  BWQ: 'Бруаррина',
	  XDO: 'Grande Riviere',
	  CBN: 'Чиребон',
	  MBQ: 'Мбарара',
	  AVU: 'Avu Avu',
	  ENK: 'Эннискиллен',
	  UAS: 'Самбуру',
	  BFQ: 'Баия Пинар',
	  NCN: 'New Chenega',
	  JHC: 'Гарден-Сити',
	  CCZ: 'Чуб Кей',
	  CEJ: 'Чернигов',
	  BMY: 'о.Белеп',
	  LST: 'Лончестон',
	  QWW: 'Навальмораль-де-ла-Мата',
	  FHU: 'Форт-Уачука',
	  BUX: 'Буниа',
	  FAB: 'Фарнборо Гемпшир',
	  KRN: 'Кируна',
	  UDN: 'Удине',
	  RIY: 'Риян',
	  TAM: 'Тампико',
	  SLW: 'Сальтильо',
	  ZEI: 'Garmisch Partenkirchen',
	  'Владимир': 'Владимир',
	  DLZ: 'Dalanzadgad',
	  CIG: 'Крейг',
	  YSO: 'Поствилль',
	  ISO: 'Кинстон',
	  NOG: 'Ногалес',
	  OGX: 'Уаргла',
	  ADZ: 'Сан-Андрес',
	  BNO: 'Бернс',
	  AUY: 'Анейтьюм',
	  PGX: 'Периге',
	  MQL: 'Милдура',
	  PNL: 'Пантеллерия',
	  HIJ: 'Хиросима',
	  GOV: 'Гоув',
	  ZSG: 'Зоннеберг',
	  SBA: 'Санта-Барбара',
	  BYK: 'Bouake',
	  ART: 'Уотертаун',
	  DOD: 'Додома',
	  KRF: 'Крамфорс',
	  SRN: 'Страхан',
	  KEL: 'Киль',
	  KRY: 'Karamay',
	  OBX: 'Обо',
	  PMV: 'Порламар',
	  PLZ: 'Порт-Элизабет',
	  YNO: 'North Spirit Lake, ON',
	  ETE: 'Метемма',
	  DER: 'Derim',
	  OZI: 'Бомбадилья',
	  QWI: 'Schleswig',
	  LET: 'Летисиа',
	  MLB: 'Мельбурн',
	  AMQ: 'Амбон',
	  RST: 'Рочестер',
	  ZXX: 'Раде',
	  AXK: 'Ataq',
	  MLO: 'Милос',
	  POV: '',
	  QWY: 'Олбани',
	  EDM: 'Ла Рош',
	  DEC: 'Декейтер',
	  IGU: 'Игуасу-Фоллз',
	  YBX: 'Бланк-Саблон',
	  AGS: 'Аугуста',
	  KNH: 'Кинмень',
	  CBE: 'Кумберленд',
	  QOS: 'Ористано',
	  XBV: 'Бон',
	  SFA: 'Сфакс',
	  BJT: '',
	  ZRV: 'Провиденс',
	  CZU: 'Корозал',
	  ODB: 'Кордова',
	  TAP: 'Тапачула',
	  DUI: 'Дуйсбург',
	  IXZ: 'Порт-Блер',
	  TOH: 'Торрес',
	  NLP: 'Нельспрут',
	  BNI: 'Бенин-Сити',
	  ELP: 'Эль-Пасо',
	  CEE: 'Череповец',
	  LDS: '',
	  WAT: 'Уэтерфорд',
	  AHS: 'Ахуас',
	  TGZ: 'Тухтла-Гутьеррес',
	  RBF: 'Биг Бер',
	  YMB: 'Меррит',
	  BGC: 'Браганца',
	  SZA: '',
	  MOF: 'Маумере',
	  BEB: 'Бенбекула',
	  NYR: 'Нюрба',
	  CPC: 'Чапелко',
	  GTF: 'Грейт Фоллз',
	  MTV: 'Мота-Лава',
	  DGD: 'Далгаранга',
	  PEV: 'Печ',
	  QGJ: 'Нова-Фрибурго',
	  EVV: 'Эвансвилль',
	  GST: 'Гласьер-Бей',
	  MKZ: 'Малакка',
	  HIR: 'Хониара',
	  KNX: 'Кунунурра',
	  TAO: 'Циндао',
	  KMP: 'Keetmanshoop',
	  BKM: 'Бакалалан',
	  QIZ: 'Бизерте',
	  HDN: 'Хайден',
	  MTJ: 'Монтроз',
	  SEV: '',
	  BPX: 'Bangda',
	  SPS: 'Вичита-Фоллз',
	  SVE: 'Susanville',
	  PCN: 'Пиктон',
	  CQM: 'Сьюдад-Реаль',
	  ULO: 'Улаангом',
	  ORI: 'Порт Лайонс',
	  KHD: 'Хоррамабад',
	  MYQ: 'Майсор',
	  NRA: 'Наррандера',
	  RID: 'Ричмонд',
	  PNI: 'Понпеи',
	  TSJ: 'Цусима',
	  UTT: 'Умтата',
	  CID: 'Сидар-Рапидс',
	  PEN: 'Пенанг',
	  QEB: 'Маебаси',
	  XOJ: '',
	  LLA: 'Лули',
	  JEV: 'Эври',
	  VDH: 'Донг Хой',
	  KSD: 'Карлстад',
	  WGA: 'Вагга-Вагга',
	  EAU: 'О-Клэр',
	  QZB: 'Церматт',
	  ATW: 'Аплтон',
	  SOM: 'Сан-Томе',
	  YVC: 'Лак-Ла-Ронж',
	  THA: 'Таллахома',
	  ISE: 'Испарта',
	  YTL: 'Биг-Траут',
	  XIM: 'Сен-Гиацинт',
	  SYM: 'Симао',
	  UAP: 'Ua Pou',
	  BYM: 'Bayamo',
	  GGE: 'Джорджтаун',
	  UER: 'Пуэртольяно',
	  QTL: 'Каратинга',
	  YRA: 'Rae Lakes',
	  NNB: 'Санта-Ана',
	  NTQ: 'Wajima',
	  ENL: 'Сентралия',
	  RMA: 'Рим',
	  DAR: 'Дар-эс-Салам',
	  HLT: 'Гамильтон',
	  PSV: 'Папа Стор',
	  SEY: 'Селибаби',
	  KXE: 'Клерксдорп',
	  SAB: 'Саба-Айленд',
	  YYW: 'Армстронг',
	  WEI: 'Веипа',
	  BUG: 'Benguela',
	  IAO: 'Дель Кармин',
	  BRF: 'Брадфорт',
	  MBX: 'Марибор',
	  RKP: 'Рокпорт',
	  TNJ: 'Танджунг Пинанг',
	  AQI: 'Кайсумах',
	  XKF: 'Фредрикстад',
	  ZIS: 'Zhongshan',
	  TEQ: 'Текиргад',
	  TVU: 'Тавеуни',
	  TBI: 'Зе-Байт',
	  PVK: 'Превеза',
	  SZD: 'Шеффилд',
	  SQA: 'Санта-Инес',
	  RHP: 'Рамечхап',
	  LFT: 'Лафайетт',
	  TEA: 'Тела',
	  BWF: 'Барроу-ин-Фернесс',
	  JQO: '',
	  DOR: 'Дори',
	  PHO: 'Пойнт-Хоуп',
	  NHK: 'Patuxent River',
	  BSH: 'Брайтон',
	  YQN: 'Накина',
	  LLW: 'Лилонгве',
	  MTL: 'Мейтленд',
	  ADT: 'Ада Оклахома',
	  YXX: 'Абботсфорд',
	  MEQ: 'Меулабох',
	  MJK: 'Манки Миа',
	  IGS: 'Ингольштадт',
	  MBL: 'Мэнисти',
	  VRB: 'Веро-Бич',
	  ZJI: 'Locarno',
	  KEB: 'Nanwalek',
	  BKU: 'Бетиоки',
	  OKA: 'Окинава',
	  ZAM: 'Замбоанга',
	  PPY: 'Pouso Alegre',
	  KIB: 'Ivanof Bay',
	  SXR: 'Шринагар',
	  HIA: 'Томасвилл',
	  ADV: 'Андовер',
	  BXG: 'Бендиго',
	  KID: 'Кристианстад',
	  WJF: 'Ланкастер',
	  QYN: 'Байрон-бей',
	  LVS: 'Лас-Вегас',
	  GBN: 'Сан Джованни Ротондо',
	  MPI: 'Мамитупо',
	  COI: 'Какао Метро Ареа',
	  MYV: 'Мерисвилль',
	  WBU: 'Боулдер',
	  NWT: 'Новата',
	  MUB: 'Маун',
	  SCK: 'Стоктон',
	  HGN: 'Ма Хонг Сон',
	  QAJ: 'Ajman City',
	  VMU: 'Балимуру',
	  SWG: 'Satwag',
	  HGR: 'Хайгерстоун',
	  OLM: 'Олимпия',
	  YDP: 'Нен',
	  QYZ: 'Херенвен',
	  TSQ: 'Торрес',
	  PUM: 'Помала',
	  STT: 'Сент Томас',
	  UDR: 'Удайспур',
	  OKN: 'Оконджа',
	  ITK: 'Итокама',
	  EMK: 'Эммонак',
	  SRP: 'Сторд',
	  TNF: 'Тюссо-ле-Нобль',
	  QLG: 'Ландсхут',
	  APF: 'Неаполь',
	  XZY: 'Альцей',
	  ACK: 'Нантакет',
	  GLK: 'Галкайо',
	  TPP: 'Тарапото',
	  MUZ: 'Мусома',
	  ANP: 'Аннаполис',
	  NRK: 'Норчепинг',
	  KGG: 'Кедугу',
	  SZY: 'Шимани',
	  YWG: 'Виннипег',
	  BTL: 'Бэттл-Крик',
	  GPN: 'Гарден-Пойнт',
	  MAR: 'Маракаибо',
	  XVJ: 'Стивенедж',
	  TFI: 'Туфи',
	  YRQ: 'Труа-Ривьер',
	  BBJ: 'Битбург',
	  GFD: 'Гринфилд',
	  ADD: 'Аддис-Абеба',
	  KAD: 'Кадуна',
	  HLN: 'Хелена',
	  HPN: 'Уайт Плеинс',
	  QHT: 'Терезополис',
	  HOP: 'Хопкинсвилль',
	  EVX: 'Эвре',
	  TUE: 'Тупиле',
	  CSU: 'Санта Круз ду Сул',
	  PIU: 'Пиура',
	  LWC: 'Лоренс',
	  XAS: 'Алес',
	  KIT: 'Китира',
	  PMA: 'Пемба',
	  RBH: 'Брукс-Лодж',
	  BON: 'Бонайре',
	  WPB: 'Port Berge',
	  KGI: 'Калгурли',
	  HYG: 'Хайдаберг',
	  QWM: 'Longmont',
	  RWP: 'Равалпинди',
	  KWN: 'Квинхагак',
	  GAF: 'Гафса',
	  GSB: 'Голдсборо',
	  IQT: 'Икуитос',
	  FKI: 'Кисангани',
	  SJZ: 'Сан Хорхе Айленд',
	  ZDB: 'Adelboden',
	  EAP: 'Мюлуз Базель',
	  YMP: 'Макнейл',
	  YBK: 'Бейкер-Лейк',
	  UYL: 'Ньяла',
	  SUU: 'Ферфилд',
	  AIE: 'Aiome',
	  MNR: 'Монгу',
	  IIA: 'Inishmaan',
	  OLP: 'Олимпик-Дэм',
	  EWY: 'Ньюбери',
	  GLX: 'Галела',
	  UDI: 'Уберландиа',
	  KYK: 'Карлук',
	  AHE: 'Ахе',
	  AXL: 'Александрия',
	  TON: 'Тону',
	  MRM: 'Манаре',
	  QOB: 'Ансбах',
	  MPO: 'Маунт-Поконо',
	  KDM: 'Kaadedhdhoo',
	  ZGQ: 'Tournai',
	  ZCN: 'Celle',
	  PSF: 'Питтсфилд',
	  YHG: 'Шарлоттаун',
	  AEO: 'Аюн-эль-Атрус',
	  LZN: 'Nangan',
	  XNN: 'Синин',
	  YYF: 'Пентиктон',
	  CLZ: 'Calabozo',
	  ZOZ: 'Оффенбах',
	  TKX: 'Такароа',
	  GND: 'Гренада',
	  QZD: 'Шегед',
	  WLH: 'Валаха',
	  MEO: 'Manteo',
	  ZZV: 'Занесвилль',
	  YDS: 'Desolation Sound',
	  PVZ: 'Пейнисвилль',
	  BCA: 'Баракоа',
	  AWB: 'Аваба',
	  FRK: 'Fregate Island',
	  IXA: 'Агартала',
	  GOM: 'Гома',
	  PIW: 'Pikwitonei',
	  PSD: 'Порт-Саид',
	  SZF: '',
	  ZVJ: '',
	  YSP: 'Маратон',
	  ZYL: 'Силхет',
	  ABA: 'Абакан',
	  MCZ: 'Масейо',
	  LAW: 'Лотон',
	  YQX: 'Гандер',
	  WTK: 'Ноатак',
	  OTZ: 'Коцебу',
	  ZPC: 'Пукон',
	  MZL: 'Манизалес',
	  PPC: 'Проспект-Крик',
	  PQQ: 'Порт Макгуайр',
	  VIG: 'Эль-Вигиа',
	  KIE: 'Kieta',
	  COL: 'Coll Island',
	  HPH: 'Хейфонг',
	  OZH: 'Запорожье',
	  ZNK: 'Herford',
	  CNP: 'Neerlerit Inaat',
	  BDS: 'Бриндизи',
	  POA: 'Порто Алегре',
	  BKY: 'Букаву',
	  ENS: 'Энсхеде',
	  POP: 'Пуэрто-Плата',
	  GTI: 'Гюттин',
	  QOI: 'Cotia',
	  CGJ: 'Chingola',
	  TNL: 'Тернополь',
	  DIK: 'Дикинсон',
	  BEJ: 'Берау',
	  FGL: 'Фокс-Гласьер',
	  QYX: 'Уппсала',
	  KCE: 'Коллинсвиль',
	  AZR: 'Адрар',
	  ZHR: 'Kandersteg',
	  BLF: 'Блюфилд',
	  CYZ: 'Кауаян',
	  WBQ: 'Бивер',
	  SLE: 'Салем',
	  TTQ: 'Тортукеро',
	  PTB: 'Петерсбург',
	  HRE: 'Хараре',
	  THG: 'Тангул',
	  WVL: 'Уотервилль',
	  FRJ: 'Фрехус',
	  YBG: 'Баготвилль',
	  HDS: 'Ходспрут',
	  PKE: 'Паркес',
	  ZGS: 'Gethsemanie',
	  LGI: 'Дэдменз-Кей',
	  AKR: 'Акуре',
	  AJU: 'Аракайю',
	  HBT: 'Хафр-Альбатин',
	  MRD: 'Мерида',
	  SVP: 'Куито',
	  JER: 'Джерси',
	  IIL: 'Ilaam',
	  BXS: 'Боррего Спрингз',
	  MAZ: 'Маягуэз',
	  VDE: 'Валверде',
	  XRE: 'Ридинг',
	  HUW: '',
	  DVO: 'Давао',
	  SSQ: 'La Sarre',
	  XBS: 'Булонь-сюр-Мер',
	  TRO: 'Тари',
	  UVE: 'Ouvea',
	  FDU: 'Бандунду',
	  SBJ: 'Сан-Матеус',
	  ZNO: 'Хильдесхайм',
	  YFH: 'Форт-Хоуп',
	  PSL: 'Перт',
	  KLT: 'Кайзерслаутерн',
	  WHL: 'Велшпул',
	  KNW: 'Новый Стуиахок',
	  AAY: 'Эль-Гайда',
	  HON: 'Гурон',
	  GVR: 'Говернадор-Валадарес',
	  BPS: 'Порто Сегуро',
	  PAE: 'Эверетт',
	  YGP: 'Гаспе',
	  AYP: 'Айякучо',
	  JCB: 'Жоакаба',
	  GIZ: 'Джазан',
	  CUU: 'Чихуахуа',
	  AKU: 'Аксу',
	  UPG: 'Уджунг-Панданг',
	  XCT: 'Ла Сиота',
	  ARP: 'Aragip',
	  IXE: 'Мангалор',
	  RSN: 'Растон',
	  YQI: 'Ярмут',
	  ZTU: '',
	  ZGP: 'Mechelen',
	  YKN: 'Янктон',
	  AIA: 'Элайенс',
	  IGA: 'Инагуа',
	  MQZ: 'Маргарет-Ривер',
	  HTV: 'Хантсвилль',
	  YYU: 'Капускасинг',
	  ZIG: 'Зигиншор',
	  MHG: 'Мангейм Германия',
	  EDE: 'Эдентон',
	  ULU: 'Гулу',
	  LER: 'Лейнстер',
	  QFH: 'Фредериксхавн',
	  VCT: 'Виктория',
	  NHX: 'Foley',
	  GDN: 'Гданьск',
	  BDP: 'Бхадрапур',
	  ZKT: 'Комотини',
	  ZOI: 'Марбург-на -Лане',
	  HSG: 'Сага',
	  WAC: 'Waca',
	  COG: 'Кондото',
	  FNT: 'Флинт',
	  QIC: 'Сиракузы',
	  TBJ: 'Табарка',
	  PSQ: '',
	  KOZ: 'Ouzinkie',
	  HNY: 'Хеньян',
	  COO: 'Котону',
	  MVL: 'Stowe',
	  CIT: 'Шымкент',
	  XLG: 'Лонье',
	  WYN: 'Виндхем',
	  DIJ: 'Дижон',
	  STB: 'Санта-Барбара',
	  CAD: 'Кадиллак',
	  IOR: 'Inishmore',
	  MSV: 'Монтичелло',
	  OIM: 'Осима',
	  CWI: 'Клинтон',
	  YYG: 'Шарлоттаун',
	  WMK: 'Майерс Чак',
	  PHW: 'Фалаборва',
	  XXL: 'Лиллехаммер',
	  TEF: 'Телфер',
	  GLC: 'Гелади',
	  UEL: 'Келимане',
	  SHL: 'Шилун',
	  SML: 'Стелла Марис',
	  ZLA: 'Villars',
	  ZCS: 'Дармштадт',
	  RKT: 'Рас-аль-Хайма',
	  MPH: 'Катиклан',
	  OKG: 'Okoyo',
	  VRK: 'Варкаус',
	  OLI: 'Олафсвик',
	  SMP: 'Стокгольм',
	  KKB: 'Китой Бэй',
	  IMA: 'Iamalele',
	  XBD: 'Бар-Ле-Дюк',
	  QLF: 'Лахти',
	  KIK: 'Киркук',
	  CRD: 'Комодоро-Ривадавиа',
	  LYU: 'Эли Мн',
	  BRR: 'Барра',
	  CCR: 'Конкорд',
	  SOY: 'Стронсей',
	  CYO: 'Кайо Ларго Дель Сур',
	  QMO: 'Монс',
	  WXN: 'Ваньсянь',
	  YMT: 'Чибугамау',
	  RSU: 'Йосу',
	  TGI: 'Тинго-Мария',
	  BRD: 'Браинерд',
	  APL: 'Нампула',
	  DNH: 'Дунхуань',
	  PTI: 'Порт-Дуглас',
	  ZOD: 'Ludwigsburg',
	  HAA: 'Хасвик',
	  YKY: 'Киндерсли',
	  QLU: 'Люблин',
	  TNK: 'Тунунак',
	  DBO: 'Дуббо',
	  MFR: 'Медфорд',
	  XSJ: 'Сен Кантен',
	  PKN: 'Пангкаланбун',
	  TVY: 'Тавой',
	  PMY: 'Пуэрто Мадрин',
	  YQQ: 'Комокс',
	  KCK: 'Канзас-Сити',
	  AUU: 'Aurukun Mission',
	  JKR: 'Джанакпур',
	  GTE: 'Грут-Эйландт',
	  ZSX: 'Stralsund',
	  SVJ: 'Сволваер',
	  AOE: 'Эскишехир',
	  HVD: 'Ховд',
	  MKA: 'Марианске Лазне',
	  MSG: 'Matsaile',
	  UYN: 'Фулин',
	  GGT: 'Джордж-Таун',
	  ABH: 'Альфа',
	  ZAN: 'Aghios Nicolaos',
	  URC: 'Урумчи',
	  TCP: 'Таба',
	  ULG: 'Ulgit',
	  ZPE: 'Оснабрукк',
	  CEC: 'Кресент-Сити',
	  DCU: 'Декейтер',
	  FUB: 'Фуллеборн',
	  MNE: 'Мангеранье',
	  NCH: 'Начингвеа',
	  OBN: 'Обан',
	  FAI: 'Фэрбанкс',
	  SUE: 'Стуржен Бэй',
	  XSZ: 'Сетубал',
	  DUJ: 'Дюбуа',
	  KUM: 'Якусима',
	  XWH: 'Сток-он-Трент',
	  RQW: '',
	  COD: 'Коди',
	  YIK: 'Ivujivik',
	  LGC: 'Ла-Гранде',
	  CUD: 'Калундра',
	  RNH: 'Нью-Ричмонд',
	  VAK: 'Чевак',
	  YPN: 'Порт-Меньер',
	  PAP: 'Порт-о-Пренс',
	  JOG: 'Йогджакарта',
	  XEE: 'Лак Эдуард',
	  MLH: 'Мюлуз',
	  KPV: 'Перривилл',
	  MVQ: 'Могилев',
	  KSU: 'Кристиансанд',
	  JCK: 'Джулиа-Крик',
	  LIG: 'Лимож',
	  RAJ: 'Раджкот',
	  PHC: 'Порт-Харкорт',
	  CNC: 'Коконат-Айленд',
	  XAG: 'Агд',
	  HTY: '',
	  XRO: 'Ла Рош Сюр Йон',
	  LIF: 'Лифу',
	  RUD: '',
	  RMP: 'Рампарт',
	  HVR: 'Гавр',
	  AVP: 'Скрантон',
	  UKX: 'Усть-Кут',
	  QKF: 'Krefeld',
	  SBZ: 'Сибиу',
	  XBT: 'Булонь-Бийанкур',
	  XPB: 'Parksville',
	  QLT: 'Латина',
	  ZCY: 'Dueren',
	  KKU: 'Экук',
	  XUZ: 'Сюйчжоу',
	  PME: 'Портсмут',
	  YET: 'Эдсон',
	  ZDT: 'Chur',
	  KMJ: 'Кумамото',
	  YMH: 'Mary\'s Harbour',
	  TUL: 'Талса',
	  KZD: 'Кракор',
	  BRS: 'Бристоль',
	  KMA: 'Керема',
	  TBU: 'Нукуалофа',
	  INV: 'Инвернесс',
	  WHK: 'Вакатан',
	  GAD: 'Гадсден',
	  OLF: 'Wolf Point',
	  MAE: 'Мадера',
	  YGH: 'Форт Доброй Надежды',
	  UAH: 'Ua Huka',
	  YNC: 'Wemindji',
	  SHO: 'Сокчо',
	  GXF: 'Сейюн',
	  GCY: 'Гринвилль',
	  DHB: '',
	  LTD: 'Гадамес',
	  XQD: 'Бедфорт',
	  CKV: 'Кларксвилль',
	  IRK: 'Кирксвилль',
	  WSZ: 'Вестпорт',
	  SNW: 'Сандовей',
	  GOQ: 'Голмуд',
	  ZSO: 'Suhl',
	  QCZ: 'Катанцаро',
	  TWU: 'Тавау',
	  CAW: 'Кампос',
	  HFN: 'Хорнафьордур',
	  ZWL: '',
	  IAG: 'Ниагара Фоллз',
	  EBA: 'Эльба остров',
	  TKE: 'Тенейки',
	  TTB: 'Тортоли',
	  FOG: 'Фоджия',
	  YUS: '',
	  KGB: 'Konge',
	  AGL: 'Ванигела',
	  GAT: 'Гап',
	  EZV: '',
	  WNR: 'Виндора',
	  NIC: 'Никосия',
	  OUK: 'Outer Skerries',
	  YSK: 'Саникилуак',
	  SKD: 'Самарканд',
	  TBG: 'Табубил',
	  XVA: 'Стокпорт',
	  XRG: 'Рагли',
	  SFU: 'Safia',
	  YHA: 'Порт Хоуп Симпсон',
	  XTG: 'Таргоминда',
	  TPL: 'Темпль',
	  BXR: 'Бам',
	  BPG: 'Barra Do Garcas',
	  MJU: 'Mamuju',
	  MKW: 'Маноквари',
	  CDR: 'Чадрон',
	  MGT: 'Milingimbi',
	  BVC: 'Боавишта',
	  BXB: 'Бабо',
	  OBL: 'Zoersel',
	  OEA: 'Винсеннес',
	  SGS: '',
	  CIF: 'Чифэн',
	  LTU: '',
	  MNJ: 'Mananjary',
	  PAY: 'Памол',
	  SMM: 'Семпорна',
	  MKT: 'Манкато',
	  YQD: 'Те-Пас',
	  SAY: 'Сиена',
	  MVP: 'Миту',
	  BPT: 'Бомонт',
	  ZPR: 'Розенхайм',
	  QAZ: 'Закопане',
	  KMV: 'Калемьо',
	  PSS: 'Посадас',
	  XFB: 'Фонтенбло',
	  SLD: 'Шлияк',
	  MOI: 'о.Митиаро',
	  KZF: 'Каинтиба',
	  QYC: 'Драхтен',
	  RYK: 'Рахим-Яр-Хан',
	  SVU: 'Савусаву',
	  HMJ: 'Хмельницкий',
	  CAH: 'Ка-Мау',
	  LBJ: 'Лабуан-Баджо',
	  SJJ: 'Сараево',
	  PLX: 'Семипалатинск',
	  DLY: 'Диллонс Бэй',
	  KYN: 'Милтон-Кейнс',
	  MDG: 'Муданьцзян',
	  MBC: 'Мбигу',
	  PLJ: 'Пласенсиа',
	  SCQ: 'Сантьяго-де-Компостела',
	  KOU: 'Куламуту',
	  GDT: 'Гранд Турк',
	  CVF: 'Куршавель',
	  GRV: 'Грозный',
	  DGA: 'Dangriga',
	  GRJ: 'Джордж',
	  EWN: 'Нью Берн',
	  PGO: 'Пагоса-Спрингс',
	  TOL: 'Толедо',
	  GAU: 'Гавахати',
	  TCE: 'Tulcea',
	  MDN: 'Мадисон',
	  TTE: 'Тернате',
	  GCC: 'Жиллет',
	  NIN: '',
	  LZC: 'Лацаро-Карденас Михоакен',
	  YHN: 'Hornepayne',
	  FDF: 'Форт де Франс',
	  CUP: 'Carupano',
	  QIG: 'Игуату',
	  BQL: 'Булиа',
	  SXE: 'Сейл',
	  AXP: 'Спринг-Пойнт',
	  NTY: 'Сан-Сити',
	  XFG: 'Perce',
	  NOV: 'Хуамбо',
	  AMH: 'Арба-Минч',
	  CDP: 'Куддапа',
	  ZNB: 'Hamm',
	  LBI: 'Алби',
	  SAX: 'Самбу',
	  DOL: 'Даовилль',
	  MSL: 'Масл-Шоалз',
	  IWJ: 'Ивами',
	  DSE: 'Dessie',
	  NAN: 'Нади',
	  QEV: 'Курбевуа',
	  YTG: 'Салливан Бэй',
	  GJR: 'Gjogur',
	  BBS: '',
	  CEI: 'Чианг-Рай',
	  DLH: 'Дулут',
	  MBB: 'Мабл-Бар',
	  KRP: 'Каруп',
	  KDV: 'Кандаву',
	  YPO: 'Peawanuck',
	  QIV: 'Исмаилия',
	  BHH: 'Бисха',
	  WWD: 'Кейп-Мэй',
	  SRQ: 'Сарасота',
	  PLH: 'Плимут',
	  YGJ: 'Йонаго',
	  QIU: 'Сиудадела',
	  ZQL: 'Филлинген-Швеннинген',
	  YWP: 'Webequie',
	  COF: 'Cocoa',
	  LCH: 'Лейк Чарльз',
	  TYD: 'Тында',
	  BSJ: 'Бэйрнсдейл',
	  FLP: 'Флиппин',
	  BKG: 'Брэнсон',
	  QUZ: 'Пуэрто-де-ла-Лус',
	  YKX: 'Киркленд',
	  TLY: '',
	  TVS: '',
	  ILI: 'Иламна',
	  CEY: 'Мюррей',
	  PKU: 'Пеканбару',
	  UCK: 'Луцк',
	  SCC: 'Прудо Бэй Дедхорс',
	  TYF: 'Торсби',
	  DZA: 'Дзаудзи',
	  LYX: 'Лидд',
	  NIU: '',
	  DLA: 'Дуала',
	  ZRJ: 'Round Lake',
	  FAY: 'Файетвилль',
	  ARX: 'Эсбери-Парк',
	  KGW: 'Kagi',
	  DZN: 'Жезказган',
	  BEZ: 'Беру',
	  MOG: 'Mong Hsat',
	  YPE: 'Пис-Ривер',
	  ENI: 'Эль Нидо',
	  SFB: 'Санфорд',
	  KEE: 'Kelle',
	  EUN: 'Лаайюн',
	  MRK: 'Марко-Айленд',
	  ZBX: 'Шомбатели',
	  BTT: 'Бетлс',
	  SWH: 'Суон Хилл',
	  VTZ: 'Вишакхапатанам',
	  SWO: 'Стиллуотер',
	  QYD: 'Гдыня',
	  XFE: 'Parent',
	  JAC: 'Джексон',
	  MSW: 'Массава',
	  SHM: 'Нанки Ширахама',
	  NYU: 'Няунг',
	  YOG: 'Ogoki',
	  HWD: 'Хейвард',
	  SMV: 'Сен Мориц',
	  YPC: 'Paulatuk',
	  MYJ: 'Мацуяма',
	  ZOO: 'Muelheim An Der Ruhr',
	  BSS: 'Balsas',
	  CND: 'Константа',
	  GTT: 'Джорджтаун',
	  FEC: 'Feira De Santana',
	  ROO: 'Рондонополис',
	  OCH: 'Nacogdoches',
	  CZH: 'Corozal',
	  CFQ: 'Крестон',
	  CUM: 'Кумана',
	  OSB: 'Osage Beach',
	  AIN: 'Уэйнрайт',
	  GUG: 'Гуари',
	  KMN: 'Камина',
	  WHO: 'Franz Josef',
	  HLD: 'Хайлар',
	  GWY: 'Голуэй',
	  XAC: 'Аркашон',
	  YBL: 'Кэмпбелл Ривер',
	  SWA: 'Шанту',
	  YRM: 'Rocky Mountain House',
	  YVO: 'Валь-д\'Ор',
	  ZFD: '',
	  KWA: 'Кваджлейн',
	  NTG: 'Наньтун',
	  RGT: 'Ренгат',
	  BHJ: 'Бхудж',
	  ZNI: 'Heidenheim',
	  PSH: 'St Peter',
	  XBK: 'Бург ан Бресс',
	  SAR: 'Спарта',
	  SYO: 'Сонаи',
	  FUO: 'Фушань',
	  LIT: 'Литл-Рок',
	  SNY: 'Сидней',
	  YDG: 'Дигби',
	  COQ: 'Чойбалсан',
	  OBO: 'Обихиро',
	  XVI: 'Вена',
	  INI: 'Ниш, Сербия',
	  UTM: 'Tunica',
	  EAL: 'Атолла Кваджалейн',
	  CKY: 'Конакри',
	  ZQH: 'Тюбинген',
	  CSV: 'Crossville',
	  ABM: 'Бамага',
	  QXW: 'Alfenas',
	  QQY: 'Йорк',
	  COH: 'Cooch Behar',
	  ELM: 'Эльмира',
	  ANS: 'Андахуайлас',
	  PEC: 'Пеликан',
	  PGL: 'Паскагула',
	  JCH: 'Qasigiannguit',
	  EDL: 'Элдорет',
	  HXX: 'Хей',
	  SBW: 'Сибу',
	  HNS: 'Хейнс',
	  KBV: 'Краби',
	  YXE: 'Саскатун',
	  XVO: 'Весул',
	  ARD: 'Алор-айленд',
	  WJA: 'Воджа',
	  CKB: 'Кларксбург',
	  CBL: 'Сиудад-Боливар',
	  BSK: 'Бискра',
	  PHN: 'Порт-Гурон',
	  ZQS: 'Куин Шарлотте Айленд',
	  VIQ: 'Викеке',
	  BEV: 'Беершеба',
	  MFE: 'МакАллен',
	  KPD: 'Кинг оф Пруссия',
	  UBP: 'Убон Рачатани',
	  GNS: 'Гунунгситоли',
	  NWI: 'Норвич',
	  GTS: 'Granites',
	  SSA: 'Сальвадор',
	  ZQV: 'Вормс',
	  NTO: 'Санто-Антао',
	  BAQ: 'Барранкуилла',
	  LRS: 'Лерос',
	  HLY: 'Холихед',
	  NGD: 'Анегада',
	  TJA: 'Тариха',
	  CWR: 'Коуари',
	  GLH: 'Гринвилль',
	  OBF: 'Оберпфаффенхофен',
	  ESA: 'Esa Ala',
	  OSZ: 'Koszalin',
	  SOC: 'Соло',
	  VIF: 'Виесте',
	  OXR: 'Окснард',
	  LOO: 'Laghouat',
	  SKZ: 'Суккур',
	  OKQ: 'Окаба',
	  QOX: 'Мемминген',
	  AUO: 'Оберн',
	  RPR: 'Райпур',
	  VLI: 'Порт-Вила',
	  SIM: 'Simbai',
	  MLG: 'Маланг',
	  GSH: 'Goshen',
	  MMH: 'Маммот Лейкс',
	  IXJ: 'Джамму',
	  SNU: 'Санта-Клара',
	  LCX: 'Лунянь',
	  TUN: 'Тунис',
	  YYQ: 'Черчилл',
	  REL: 'Трелев',
	  YRF: 'Картрайт',
	  XSF: 'Санс',
	  CTL: 'Чарлевилль',
	  BYF: '',
	  BAT: 'Барретос',
	  SKH: 'Surkhet',
	  LBA: 'Лидс',
	  XKM: 'Moss',
	  LZU: 'Лоренсевилл',
	  GUF: 'Галф Шорз',
	  SFY: 'Спрингфилд',
	  BZY: 'Бельцы',
	  FTE: 'Эль-Калафате',
	  LMP: 'Лампедуза',
	  ZPL: 'Рекклинхаузен',
	  CSS: 'Cassilandia',
	  SUA: 'Стюарт',
	  PPT: 'Папеэте',
	  XZI: '',
	  GVI: 'Грин-Ривер',
	  MUP: 'Малга-Парк',
	  MCK: 'МакКук',
	  HVA: 'Аналалава',
	  CJB: 'Коимбаторе',
	  VSA: 'Вильяхермоза',
	  CPQ: 'Кампинас',
	  AEA: 'Абемама',
	  POR: 'Пори',
	  PPI: 'Порт-Пири',
	  RSH: 'Русская Миссия',
	  QCB: 'Чиба',
	  AVX: 'о. Каталина',
	  KYS: 'Кайес',
	  DRO: 'Дуранго',
	  MKB: 'Мекамбо',
	  GFR: 'Гранвиль',
	  DPO: 'Девонпорт',
	  SRJ: 'Сан-Борха',
	  XCF: 'Шамони-Монблан',
	  AQA: 'Араракуара',
	  KZB: 'Zachar Bay',
	  VAG: 'Варжинья',
	  BUA: 'Buka Island',
	  YOK: 'Йокогама',
	  QAD: 'Порденон',
	  WGO: 'Винчестер',
	  HZG: 'Hanzhong',
	  PJA: 'Пайала',
	  SDL: 'Сундсваль',
	  TGU: 'Тегусигальпа',
	  ZNJ: 'Хайльбронн',
	  ZRS: 'Zurs Lech',
	  FKS: 'Фукусима',
	  KRB: 'Карумба',
	  PAZ: 'Поза-Рика',
	  ELY: 'Эли Нв',
	  TUW: 'Тубала',
	  YHF: 'Херст',
	  CCF: 'Каркасон',
	  ASK: 'Ямосукро',
	  YME: 'Матэйн',
	  YLC: 'Киммирут Лейк-Харбор',
	  VDM: 'Виедма',
	  KAW: 'Котонг',
	  BEQ: 'Бери-Сент-Эдмундс',
	  AAE: 'Аннаба',
	  LDX: 'Сен-Лоран-дю-Марони',
	  ZQD: 'Stade',
	  FRE: 'Остров Фера',
	  LGD: 'Ла-Гранде',
	  YLY: 'Лэнгли',
	  XDR: 'Дре',
	  QPE: 'Петрополис',
	  CAN: 'Гуанчжоу',
	  DIW: '',
	  EKB: '',
	  TII: 'Таринкот',
	  SGV: 'Сьерра-Гранде',
	  KHU: 'Кременчуг',
	  JGN: 'Цзяюйгуань',
	  ZYK: 'Секо',
	  YMU: '',
	  RZE: 'Жешов',
	  YTA: 'Пемброк',
	  JAP: 'Пунта Ренес',
	  ZPM: 'Регенсбург',
	  SGJ: 'Sagarai',
	  GSM: 'Gheshm',
	  HOI: 'Хао-Айленд',
	  ZEM: 'Ист Мейн',
	  PSM: 'Портсмут',
	  SFQ: 'Санли Урфа',
	  KCC: 'Кофман Ков',
	  UII: 'Utila',
	  STM: 'Сантарекм',
	  PLO: 'Порт-Линкольн',
	  ZSE: 'Сан Пьер де ла Реюньон',
	  OAZ: 'Зарандж',
	  KMS: 'Кумаси',
	  ISP: 'Ислип',
	  BBH: 'Барт',
	  ZFN: 'Tulita/Fort Norman',
	  ISH: 'Искья о-в',
	  GLF: 'Голфито',
	  YNT: 'Янтай',
	  MGH: 'Мангейм',
	  PGV: 'Гринвилль',
	  OMR: 'Орадеа',
	  PUS: 'Бусан',
	  BXV: 'Брейддалсвик',
	  KZI: 'Козани',
	  LMA: 'Озеро Минчумина',
	  KUK: 'Кейсиглек',
	  GPI: 'Гуапи',
	  KEI: 'Кепи',
	  NLK: 'Норфолк-Айленд',
	  GUJ: 'Guaratingueta',
	  QEA: 'Терамо',
	  WKN: 'Wakunai',
	  ZRO: 'Reggio Nellemilia',
	  DEE: '',
	  DLE: 'Доле',
	  QKB: 'Брекенридж',
	  DEA: 'Дера Гази Хан',
	  JGS: 'Ji an',
	  YBD: 'Нью-Уэстминстер',
	  BWE: 'Брауншвейг',
	  MCQ: 'Мишкольц',
	  UPF: 'Пфорцхайм',
	  ZRC: 'Сан-Педро-де-Алькантара',
	  NON: 'Ноноути',
	  YFT: '',
	  YYJ: 'Виктория',
	  TKV: 'Татакото',
	  IPL: 'Эль Сентро',
	  SDG: 'Санандадж',
	  WRL: 'Уорланд',
	  VAA: 'Вааса',
	  ZAF: 'Арль',
	  SDX: 'Седона',
	  MSO: 'Миссула',
	  ZQW: 'Цвайбрюкен',
	  XCO: 'Колак',
	  MQP: 'Нельспрут',
	  BYW: 'Блэкли-Айленд',
	  CDH: 'Камден',
	  XEL: 'Нью-Карлайл',
	  TBN: 'Форт Леонард Вуд',
	  ROY: 'Rio Mayo',
	  RBR: 'Рио Бранко',
	  BSO: 'Баско',
	  ERC: 'Эрзинджан',
	  LAD: 'Луанда',
	  LUD: 'Людериц',
	  ZTJ: 'Принстон',
	  GUQ: 'Гуанаре',
	  ZWB: 'Hampton',
	  TOG: 'Тожиак',
	  XXD: 'Дегерфорс',
	  QJB: 'Джубайл',
	  USK: 'Усинск',
	  LBD: 'Худжанд',
	  LLE: 'Малелейн',
	  NOR: 'Нордфьордур',
	  GTB: 'Гентинг',
	  XGZ: 'Bregenz',
	  CRU: 'Carriacou',
	  ADX: 'Сэнт-Эндрюс',
	  INL: 'Интернешнл Фоллз',
	  TBP: 'Тумбес',
	  LTN: 'Лутон',
	  BMH: 'Bomai',
	  XBN: 'Бинигуни',
	  CHR: 'Шатору',
	  CCT: 'Colonial Catriel',
	  DOP: 'Дольпа',
	  HLA: 'Лансерия',
	  QON: 'Arlon',
	  NDR: 'Надор',
	  GVX: 'Гавле',
	  XLZ: 'Труро',
	  GOR: 'Gore',
	  CEZ: 'Кортес',
	  ZSJ: 'Сэнди Лейк',
	  SCF: 'Скоттсдейл',
	  VPY: 'Шимойо',
	  KUD: 'Кудат',
	  HKN: 'Хоскинс',
	  ELD: 'Эль Дорадо',
	  RRS: 'Ророс',
	  FLI: 'Flateyri',
	  GOH: 'Нуук',
	  ZEB: 'Эсслинген',
	  JVL: 'Белойт',
	  ELF: 'Эль-Фашер',
	  LZY: '',
	  BMM: 'Битам',
	  DDG: 'Даньдон',
	  LPB: 'Ла-Пас',
	  JAX: 'Джексонвилл',
	  BRQ: 'Брно',
	  VLU: 'Великие Луки',
	  QKR: 'Куру',
	  MVD: 'Монтевидео',
	  BXH: 'Балхаш',
	  GTO: 'Горонтало',
	  OKE: 'Окино Эрабу',
	  XAB: 'Аббевиль',
	  UNN: 'Ранонг',
	  ARU: 'Аракатуба',
	  YWJ: 'Deline',
	  RLA: 'Ролла',
	  NIT: 'Ниорт',
	  QBM: 'Бурж Сен Морис',
	  MNK: 'Миайана',
	  HFS: 'Хагфорс',
	  XBF: 'Бельгард',
	  ZEF: 'Fuerth',
	  FNB: 'Neubrandenburg',
	  TKS: 'Токусима',
	  CSX: 'Чанша',
	  STK: 'Стерлинг',
	  ALR: 'Alexandra',
	  SUZ: 'Suria',
	  ALZ: 'Алитак',
	  ZQO: 'Waiblingen',
	  ENF: 'Енонтекио',
	  GEX: 'Гелонг',
	  JJU: 'Какорток',
	  QKI: 'Kielce',
	  XIP: 'Вудсток',
	  IBR: 'Omitami Iba',
	  LKY: 'Лейк-Маньяра',
	  NIM: 'Ниамей',
	  ZXU: 'Ригге',
	  BWK: 'Бол',
	  LSS: 'Les Saintes',
	  SAQ: 'San Andros',
	  KAJ: 'Каяне',
	  MZG: 'Макунг',
	  MIW: 'Маршалтоун',
	  PLS: 'Провиденсиалес',
	  BNJ: 'Бонн',
	  IFN: 'Исфахан',
	  INF: 'Guezzam',
	  AGE: 'Ванегероге',
	  SWI: 'Суиндон',
	  KLL: 'Левелок',
	  RUS: 'Марау Айленд',
	  YPH: 'Инукджуак',
	  ADF: 'Адияман',
	  SQV: 'Sequim',
	  OTI: 'Моротаи-Айленд',
	  RGR: 'Рейнджер',
	  BRW: 'Барроу',
	  QNJ: 'Аннемассе',
	  KBM: 'Kabwum',
	  IXD: 'Аллахабад',
	  TPA: 'Тампа',
	  ILG: 'Вилмингтон',
	  XXA: 'Алвеста',
	  MPA: 'Мпача',
	  ZCF: 'Bergheim',
	  HDG: '',
	  ULK: 'Ленск',
	  SNQ: 'Сан-Кинтин',
	  CIH: 'Чанжи',
	  EHL: 'Эль-Больсон',
	  YZF: 'Йеллоунайф',
	  LHW: 'Ланчжоу',
	  SNH: 'Станторп',
	  SLT: 'Салида',
	  IMD: 'Imonda',
	  CBG: 'Кембридж',
	  MZM: 'Мец',
	  RAO: 'Рибейран-Прету',
	  XRP: 'Ривьер Пьер',
	  KKR: 'Каукура',
	  LEX: 'Лексингтон',
	  YXJ: 'Форт-Сент-Джон',
	  TEU: 'Те Анау',
	  MBT: 'Масбате',
	  GRR: 'Гранд Рапидс',
	  KSN: 'Кустанай',
	  ATN: 'Наматанаи',
	  HUZ: 'Хуйджоу',
	  RNB: 'Роннебю',
	  MVB: 'Франсвиль Mvengue',
	  HPV: 'Принсвилль',
	  ZOB: 'Lippstadt',
	  KCF: 'Kadanwari',
	  BRA: 'Баррейрас',
	  VST: 'Вастерас',
	  NAR: 'Наре',
	  TGJ: 'Тига',
	  BJD: 'Баккафьордур',
	  XCB: 'Камбре',
	  NSE: 'Милтон',
	  TAI: 'Таис',
	  TKT: 'Так Таиланд',
	  OFK: 'Норфолк',
	  HAG: 'Гаага',
	  DLD: 'Гейло',
	  QIH: 'Трес-Риос',
	  YPY: '',
	  PUD: 'Puerto Deseado',
	  WMO: 'Уайт Маунтин',
	  BHI: 'Байя-Бланка',
	  MFF: 'Моанда',
	  CHV: 'Chaves',
	  MBW: 'Мураббин',
	  OHO: 'Охотск',
	  CAU: 'Каруару',
	  PHM: 'Boeblingen',
	  MYD: 'Малинди',
	  RET: 'Рост',
	  YBC: 'Байе-Комо',
	  SIJ: 'Siglufjordur',
	  VGZ: 'Виллагарсон',
	  HUE: 'Хумере',
	  PUC: 'Прайс',
	  OHT: 'Кохат',
	  KFA: 'Киффа',
	  TRW: 'Тарава',
	  LGB: 'Лонг-Бич',
	  BPF: 'Батуна',
	  QLP: 'La Spezia',
	  YXR: 'Эрлтон',
	  ALV: 'Андорра-ла-Велла',
	  SSI: 'Сент Симон Айленд',
	  QLY: 'Плайя-Бланка',
	  PCR: 'Пуэрто-Каррено',
	  GBE: 'Габороне',
	  ZCC: 'Баден-Баден',
	  FZO: 'Филтон',
	  YYD: 'Смитерс',
	  GRM: 'Grand Marais',
	  HNA: 'Мориока',
	  MGE: 'Мариетта',
	  UMR: 'Вумера',
	  CET: 'Шоле',
	  AWN: 'Элтон-Даунс',
	  LZH: 'Лучжоу',
	  GUY: 'Гаймон',
	  URT: 'Сурат-Тхани',
	  LDU: 'Лахад-Дату',
	  CZX: 'Чангзу',
	  TBW: 'Тамбов',
	  FSP: 'Сен-Пьер',
	  LEE: 'Лизбург',
	  HKK: 'Хокитика',
	  CRA: 'Крайова',
	  XQG: 'Бервик-апон-Твид',
	  KGT: '',
	  MIG: 'Мьян Янг',
	  MJM: 'Мбужи Майи',
	  BCD: 'Баколод',
	  ONG: 'Морнингтон',
	  XGW: 'Гананок',
	  QNG: 'Нагано',
	  UVO: 'Uvol',
	  DAG: 'Daggett',
	  CRK: 'Лусон-Айленд',
	  LNE: 'Lonorore',
	  HNM: 'Гана',
	  HYF: 'Hayfields',
	  AZD: 'Язд',
	  GLW: 'Глазго',
	  KKN: 'Киркенес',
	  SHQ: 'Саузпорт',
	  LEQ: 'Лендс-Энд',
	  MIS: 'Мисима',
	  CXJ: 'Касьяс ду Суль',
	  ZPD: 'Ольденбург',
	  AUR: 'Орийак',
	  RDZ: 'Родез',
	  JSO: 'Sodertalje',
	  LCR: 'Ла-Хоррера',
	  ESE: 'Энсенада',
	  TML: 'Тамале',
	  CSA: 'Колонсей, остров',
	  MJW: 'Mahenye',
	  QVC: 'Викоза',
	  GEE: 'Джордж-Таун',
	  IRP: 'Исиро',
	  SVZ: 'Сан-Антонио',
	  ZEJ: 'Гельзенкирхен',
	  SWT: 'Стрежевой',
	  JOK: 'Йошкар-Ола',
	  PZO: 'Пуэрто Ордас',
	  PAO: 'Пало-Альто',
	  ZYN: '',
	  SNL: 'Шони',
	  OSY: 'Намсос',
	  KLU: 'Клагенфурт',
	  SLS: '',
	  TOD: 'Тиоман',
	  ROU: 'Ruse',
	  JEG: 'Аусиаит',
	  ULE: 'Sule',
	  VTG: 'Вунг Тау',
	  BHN: 'Бейхан',
	  OAX: 'Оксака',
	  YPR: 'Принс Руперт',
	  YNL: '',
	  EUA: 'Eua To',
	  CKX: 'Чикен',
	  YSM: 'Форт-Смит',
	  XLL: 'Лон ле Сонье',
	  JGO: 'Qeqertarsuaq',
	  TAR: 'Таранто',
	  UIH: 'Куинхон',
	  CTN: 'Куктаун',
	  YDA: 'Доусон-Сити',
	  YPS: 'Порт Хоксбери',
	  RVE: 'Саравена',
	  CED: 'Седуна',
	  HIN: 'Чинджу',
	  KND: 'Кинду',
	  ZUA: 'Ютика',
	  FRR: 'Франт-Ройял',
	  BRM: 'Баркисимето',
	  QGL: 'Санкт-Галлен',
	  KCL: 'Чигник Лагун',
	  OOL: 'Голд-Коуст',
	  BAY: 'Байя-Маре',
	  MTA: 'Матамата',
	  XSU: 'Сомур',
	  BZZ: 'Брайз Нортон',
	  CIZ: 'Коари',
	  TRD: 'Трондхейм',
	  ITH: 'Итака',
	  SNB: 'Снейк-Бей',
	  TUV: 'Тукупита',
	  TUR: 'Тукуруи',
	  XRY: 'Херес-де-ла-Фронтера',
	  CNS: 'Кернс',
	  LPM: 'Ламап',
	  LTS: 'Альтус',
	  MKQ: 'Мерауке',
	  MML: 'Маршалл',
	  VDA: 'Овда',
	  IRZ: '',
	  MMB: 'Мемамбецу',
	  YCO: 'Куглуктук-Коппермайн',
	  ZAK: 'Chiusa Klausen',
	  SMR: 'Санта-Марта',
	  PIX: 'Пико-Айленд',
	  GDQ: 'Гондар',
	  TCL: 'Тускалуза',
	  BLZ: 'Блантайр',
	  VSF: 'Спрингфилд',
	  RUT: 'Рутленд',
	  SIX: 'Синглтон',
	  ALN: 'Alton',
	  VRL: 'Вила-Реал',
	  AIS: 'о. Арорае',
	  GBD: 'Грейт Бенд',
	  KIM: 'Кимберли',
	  ONO: 'Онтарио',
	  QVM: 'Hameenlinna',
	  NAA: 'Наррабри',
	  KRA: 'Керанг',
	  QIJ: 'Гижон',
	  KEK: 'Эквок',
	  WSY: 'Эрли-Бич',
	  ZLM: 'Zug Ch',
	  YZH: 'Слэйв Лэйк',
	  TIX: 'Титусвилль',
	  ZAZ: 'Сарагоса',
	  JZH: 'Сунпань',
	  FOD: 'Форт-Додж',
	  YKI: 'Кенносао-Лейк',
	  HSV: 'Хантсвилль',
	  ZAQ: '',
	  YUM: 'Юма',
	  NQN: 'Неукен',
	  CCW: 'Коуэлл',
	  HSN: 'Жушань',
	  FRD: 'Фрайди Харбор',
	  RSA: 'Санта-Роза',
	  CYX: 'Черский',
	  KSM: 'Санкт-Мэрис',
	  QNR: 'Santa Cruz Rio Pardo',
	  AHB: 'Абха',
	  DAX: 'Дэцзян',
	  PNO: 'Пинотепа-Насионал',
	  XUT: '',
	  MUA: 'Мунда',
	  CZL: 'Константин',
	  YGV: 'Гавр Сен-Пьер',
	  CBT: 'Катумбела',
	  BMD: 'Бело',
	  BUZ: 'Бушехр',
	  ARW: 'Арад',
	  UBJ: 'Убе Япония',
	  XIF: 'Напани',
	  SPI: 'Спрингфилд',
	  CGD: 'Чандэ',
	  YSR: 'Нэнисивик',
	  CXP: 'Чилачап',
	  PAW: 'Памбва',
	  FON: 'Фортуна',
	  KGC: 'Кингскот',
	  LUN: 'Лусака',
	  LVB: 'Ливраменто',
	  ZFM: 'Форт-Макферсон',
	  PPQ: 'Парапарауму',
	  SOD: 'Сорокаба',
	  ROW: 'Розуэлл',
	  MEY: 'Meghauli',
	  KGY: 'Кингарой',
	  MDV: 'Медунеу',
	  CAX: 'Карлисл',
	  YDN: 'Дофин',
	  ZSU: 'Dessau',
	  RSD: 'Рок Саунд',
	  PDE: 'Панди-Панди',
	  LHG: 'Лайтнинг-Ридж',
	  LYB: 'Малый Кайман',
	  TMR: 'Таманрассет',
	  FNJ: 'Пхеньян',
	  AKI: 'Акиак',
	  DCG: '',
	  BIH: 'Бишоп',
	  TRC: 'Торреон',
	  XCK: 'Сен Ди',
	  GDV: 'Глендайв',
	  VLA: 'Vandalia',
	  FMM: 'Мемминген',
	  RAE: 'Арар',
	  CJU: 'Чеджу Сити',
	  CHX: 'Чангвинола',
	  MGZ: 'Myeik',
	  ZHA: 'Чжаньчзян',
	  YYH: 'Талойоак',
	  FOR: 'Форталеса',
	  DBS: '',
	  ONQ: 'Зонгулдак',
	  AXM: 'Армения',
	  CSH: 'Соловецкий',
	  PNC: 'Понка-Сити',
	  MST: 'Маастрихт',
	  ZMA: 'Мэнсфилд',
	  TGR: 'Туггурт',
	  FDK: 'Фредерик',
	  CNB: 'Кунамбл',
	  NLF: 'Дарнли-Айленд',
	  UPN: 'Уруапан',
	  KUO: 'Куопио',
	  EIK: '',
	  SUW: 'Сьюпириор',
	  QAK: 'Барбасена',
	  YHR: 'Чевери',
	  ZPQ: 'Rheine',
	  NHV: 'Нуку-Хива',
	  RSG: 'Serra Pelada',
	  WGP: 'Вайнгапу',
	  MGW: 'Моргантаун',
	  VVK: 'Вастервик',
	  ZFP: 'Veszprem',
	  AJF: 'Аль-Жуф',
	  CVN: 'Кловис',
	  OAL: 'Cacoal',
	  OUS: 'Ourinhos',
	  PTS: 'Питтсбург',
	  TMC: 'Тамболака',
	  ZTP: 'Itapetininga',
	  HTS: 'Хантингтон',
	  LRU: 'Лас Крусес',
	  YFJ: 'Snare Lake',
	  VBV: 'Вануабалаву',
	  INZ: 'Ин Салах',
	  PRS: 'Параси',
	  PNK: 'Понтианак',
	  ZLU: 'Ludwigslust',
	  ZGJ: 'Brugge',
	  ZMM: 'Замора',
	  MAQ: 'Мае Сот',
	  SLV: 'Симла',
	  SKO: 'Сокото',
	  YTE: 'Кейп-Дорсет',
	  SRE: 'Сукре',
	  LRM: 'Ла-Романа',
	  QQD: 'Дувр',
	  XLR: 'Либурн',
	  DIU: 'Диу Инд',
	  YVB: 'Бонавентуре',
	  MLQ: 'Малалауа',
	  LIX: '',
	  DBY: 'Делби',
	  ZOG: 'Lueneburg',
	  CKS: 'Каражас',
	  BZV: 'Браззавилль',
	  VTL: 'Виттель',
	  ULM: 'Нью Ульм',
	  SLK: 'Саранак Лейк',
	  KOC: 'Кумак',
	  MZH: 'Мерзифон',
	  NPE: 'Напье-Хастингс',
	  ZNW: 'Лимбург',
	  BXD: 'Bade',
	  QRG: 'Рагуза',
	  TCW: 'Токумвал',
	  SUB: 'Сурабая',
	  CNJ: 'Клонкарри',
	  ZNN: 'Hilden',
	  KDN: 'Нденде',
	  JIB: 'Джибути',
	  AHI: 'Амахаи',
	  SJY: 'Сейнайоки',
	  GNM: '',
	  FWM: 'Форт-Уильям',
	  VAI: 'Ванимо',
	  ZSA: 'Сан-Сальвадор',
	  ZHD: 'Fluelen',
	  CNQ: 'Корриентес',
	  TCA: 'Теннант Крик',
	  TRK: 'Таракан',
	  ZPB: 'Sachigo Lake',
	  MGM: 'Монтгомери',
	  HUF: 'Терре-От',
	  NAU: 'Напука-Айленд',
	  GPA: 'Патрас',
	  LBS: 'Лабаса',
	  YCF: 'Кортес Бэй',
	  MAX: 'Матам',
	  NYT: '',
	  LPG: 'Ла-Плата',
	  HOB: 'Хоббс',
	  ZQF: 'Трир',
	  SXM: 'Сент-Маартен',
	  NTB: 'Нутодден',
	  VHC: 'Сауримо',
	  TUM: 'Тумут',
	  QNU: 'Нуоро',
	  XYH: '',
	  IBE: 'Ибаги',
	  CPR: 'Каспер',
	  XSH: 'Сен-Пьер-де-Корп',
	  IEG: 'Зелена-Гора',
	  LKL: 'Лакселв',
	  SVG: 'Ставангер',
	  PBZ: 'Плеттенберг-Бэй',
	  HIX: 'Хива-Оа',
	  CNN: 'Чульман',
	  MKR: 'Микатарра',
	  NHZ: 'Брансвик',
	  MHH: 'Марш-Харбор',
	  EEM: '',
	  TXF: 'Teixeira De Freitas',
	  QHZ: 'Хофддорп',
	  SYA: 'Shemya Island',
	  HZH: 'Liping City',
	  MFJ: 'Моала',
	  ASJ: 'Амами О Сима',
	  JNB: 'Йоханнесбург',
	  PZU: 'Порт-Судан',
	  MZF: 'Мзамба',
	  BVV: '',
	  GRP: 'Гурупи',
	  KAX: 'Калбарри',
	  LDI: 'Линди',
	  WAG: 'Вангануи',
	  OCN: 'Оушнсайд',
	  FRS: 'Флорес',
	  QFA: 'Аалсмеер',
	  LDE: 'Лурд',
	  LPQ: 'Луанг Прабанг',
	  VLS: 'Вейлсдир',
	  BFV: 'Бури-Рам',
	  AXV: 'Вапаконета',
	  PXO: 'Порто-Санто',
	  SLU: 'Сент-Люсия',
	  VGD: 'Вологда',
	  KDZ: '',
	  VAZ: 'Валь Д\'Изере',
	  EBU: 'Сент-Этьен',
	  IPN: 'Ипатинга',
	  ATT: 'Atmautluak',
	  QSO: 'Сусс',
	  LOE: 'Лоэй',
	  BHM: 'Бирмингем',
	  WWR: 'Вудвард',
	  QBI: 'Битола',
	  BDO: 'Бандунг',
	  CRV: 'Кротон',
	  LEN: 'Леон',
	  HTG: 'Хатанга',
	  EXT: 'Эксетер',
	  GOC: 'Гора',
	  BDH: 'Бандар-Ленгех',
	  NUQ: 'Маунтин Вью',
	  MAP: 'Mamai',
	  POF: 'Поплар Блафф',
	  HYL: 'Холлис',
	  JXA: '',
	  ZEK: 'Gladbeck',
	  PPM: 'Помпано-Бич',
	  TCZ: '',
	  PDT: 'Пендлетон',
	  KIC: 'Кинг-Сити',
	  SJE: 'Сан-Хосе Гуавьяре',
	  YSF: '',
	  HTN: 'Хотан',
	  XWG: '',
	  PSB: 'Филипсбург',
	  SPG: 'Санкт-Петербург',
	  RNZ: 'Rensselaer',
	  MLZ: 'Мело',
	  SGR: 'Шугар Ленд',
	  DAK: 'Дахла Оазис',
	  KSE: 'Kasese',
	  TTJ: 'Тоттори',
	  NBE: 'Enfidha',
	  UTA: 'Мутаре',
	  KXA: 'Kasaan',
	  QCF: 'Birigui',
	  MZT: 'Мазатлан',
	  PRB: 'Пасо-Роблес',
	  WPM: 'Випим',
	  ABS: 'Абу-Симбел',
	  SSE: 'Шолапур',
	  AOO: 'Алтуна',
	  PSZ: 'Пуэрто-Суарес',
	  SLN: 'Салина',
	  YMJ: 'Мус-Джо',
	  MOZ: 'Моореа',
	  XIB: 'Ingersoll',
	  QMN: 'Мбабане',
	  OGD: 'Огден',
	  RGN: 'Яньгунь',
	  NER: 'Нерюнгри',
	  SPK: 'Саппоро',
	  HOE: 'Хоуисей',
	  IQQ: 'Икуике',
	  CGE: 'Кембридж',
	  YAJ: 'Лиолл Харбор',
	  TME: 'Таме',
	  QXJ: 'Порвоо',
	  LAF: 'Лафайетт',
	  CXB: 'Коксс Базар',
	  HKY: 'Хикори',
	  VTU: 'Лас-Тунас',
	  TCB: 'Трежер-Кей',
	  MRY: 'Монтерей',
	  YWK: 'Вабуш',
	  KEM: 'Кеми',
	  OPB: 'Open Bay',
	  KWP: 'Вест-Пойнт',
	  KMC: 'Кинг Халид Мил. Сити',
	  CMX: 'Хауфтон',
	  TUI: 'Тураиф',
	  KEN: 'Кенема',
	  CMU: 'Kundiawa',
	  KPC: 'Порт Кларенс',
	  XLE: 'Ланс',
	  ROA: 'Роанок',
	  WUV: 'Вувулу-Айленд',
	  AFZ: 'Sabzevar',
	  TCQ: 'Такна',
	  ALW: 'Валла-Валла',
	  OLS: 'Ногалес',
	  WUX: 'Вуси',
	  ZIA: 'Тренто',
	  AYR: 'Эйр Австралия',
	  KDH: 'Кандагар',
	  ATD: 'Атоифи',
	  GFN: 'Графтон',
	  CEU: 'Клемсон',
	  VYD: 'Врихейд',
	  SCY: 'Сан-Кристобаль',
	  NDC: 'Нандед',
	  MUK: 'о.Мауке',
	  QJO: 'Campos Do Jordao',
	  SKK: 'Шейктулик',
	  EBL: 'Эрбил',
	  MYS: 'Moyale',
	  BJB: 'Bojnord',
	  SHV: 'Шревспорт',
	  CHG: 'Чаоян',
	  MSA: 'Маскрэт Дэм',
	  CMP: 'Campo Alegre',
	  PIA: 'Пеория',
	  AEH: 'Абече',
	  LNS: 'Ланкастер',
	  IFJ: 'Исафьордур',
	  ANM: 'Анталаха',
	  URA: 'Уральск',
	  NCU: 'Нукус',
	  ALP: 'Алеппо',
	  NFG: 'Нефтеюганск',
	  VXO: 'Ваксьо',
	  SHG: 'Шангнак',
	  CLA: 'Комилла',
	  DDN: 'Дельта Даунс',
	  MSC: 'Меса',
	  EAS: 'Сан-Себастьян',
	  MXL: 'Мехикали',
	  SRZ: 'Санта-Крус',
	  KWT: 'Кветлук',
	  SUN: 'Сан-Вэлли',
	  MYL: 'Мак-Колл',
	  CPA: 'Кейп-Палмас',
	  POE: 'Форт-Полк',
	  BUR: 'Бурбанк',
	  PMO: 'Палермо',
	  PUK: 'Пукаруа',
	  ZYP: 'Нью-Йорк Пенн Стейшн',
	  LPT: 'Лампанг',
	  ZCB: 'Aschaffenburg',
	  YKG: 'Кангирсук',
	  OPU: 'Балимо',
	  MUF: 'Мутинг',
	  XNM: 'Ноттингем',
	  LCE: 'Ла-Сейба',
	  FEZ: 'Фес',
	  ILM: 'Вилмингтон',
	  YCM: 'Санкт-Кэтринс',
	  YNA: 'Наташкуан',
	  LNG: 'Lese',
	  YQS: 'Сент-Томас',
	  XID: 'Максвилл',
	  BQT: 'Брест',
	  CVT: 'Ковентри',
	  LME: 'Ле-Ман',
	  WMB: 'Варрнамбул',
	  BSU: 'Basankusu',
	  MDH: 'Карбондейл',
	  VAV: 'Вавау',
	  MMY: 'Мияко Джима',
	  OKL: 'Оксибил',
	  KAC: 'Камешли',
	  MTK: 'Макин-Айленд',
	  PKG: 'Пангкор',
	  BED: 'Бедфорт',
	  YYM: 'Каули',
	  IRS: 'Стуржис',
	  SKV: 'Санта-Катарина',
	  EKS: '',
	  SOJ: 'Sorkjosen',
	  MQM: 'Мардин',
	  OMH: 'Urmieh',
	  BBG: 'Бутаритари',
	  SZT: 'Сан-Кристобаль-де-лас-Касас',
	  UKS: 'Севастополь',
	  LCV: 'Лукка',
	  LTR: 'Letterkenny',
	  BGF: 'Бангуи',
	  CVL: 'Cape Vogel',
	  ZAX: 'Angermuende',
	  ZKG: 'Кегаска',
	  DEB: 'Дебрецен',
	  XWA: 'Ватфорд',
	  PBL: 'Пуэрто-Кабелло',
	  PPB: 'Президенте-Пруденте',
	  GLS: 'Гелвстон',
	  ZIN: 'Interlaken',
	  SPH: 'Сопу',
	  SJI: 'Сан-Хосе',
	  TZL: 'Тузла',
	  HHH: 'Хилтон-Хед',
	  LMN: 'Лимбанг',
	  'Шымкент': 'Шымкент',
	  VDC: 'Витория-Да-Конкиста',
	  QVR: 'Volta Redonda',
	  GBT: 'Gorgon',
	  BOI: 'Бойсе',
	  PRM: 'Портимао',
	  DUQ: 'Дункан',
	  FES: 'Сан-Фернандо',
	  SUG: 'Суригао',
	  WNN: 'Wunnummin Lake',
	  BUF: 'Буффало',
	  BZR: 'Безьер',
	  MEE: 'Маре',
	  FAE: 'Фарерские о-ва',
	  RIB: 'Riberalta',
	  UTL: 'Торремолинос',
	  NBX: 'Набире',
	  ICI: 'Cicia',
	  NBS: '',
	  EKI: 'Элькхарт',
	  SHT: 'Шеппартон',
	  YMF: 'Montagne Harbor',
	  OST: 'Остенде',
	  MDF: 'Медфорд',
	  QUB: 'Убари',
	  MXY: 'МакКарти',
	  YPF: 'Эскималт',
	  NDG: 'Цицихар',
	  SIO: 'Смиттон',
	  OYE: 'Оем',
	  KNF: 'Кингс Линн',
	  QKZ: 'Констанц',
	  YPQ: 'Питерборо',
	  BPL: '',
	  YAM: 'Со Сен Мари',
	  AKP: 'Анактувук-Пасс',
	  AKG: 'Ангуганак',
	  HWN: 'Хванге',
	  JTC: '',
	  ZYO: 'Росендал',
	  ATI: 'Артигас',
	  ALM: 'Аламогордо',
	  BGY: 'Бергамо',
	  KRQ: 'Краматорск',
	  PVW: 'Плейнвью',
	  FGU: 'Фангатау',
	  BJI: 'Бемиджи',
	  ENC: 'Нанси',
	  AMU: 'Аманаб',
	  CLP: 'Кларкс Пойнт',
	  GBR: 'Грейт Баррингтон',
	  YQG: 'Уинсор',
	  YUB: 'Tuktoyaktuk',
	  ILF: 'Илфорд',
	  ZSN: 'Stendal',
	  MYI: 'Мюррей-Айленд',
	  LAM: 'Лос-Аламос',
	  XLI: 'Сен-Луи',
	  AJR: 'Арвидсьяур',
	  AUG: 'Аугуста',
	  XMW: 'Монтаубан',
	  ANI: 'Аниак',
	  XCY: 'Шато-Тьерри',
	  HFT: 'Хаммерфест',
	  LTO: 'Лорето',
	  GTY: 'Геттисбург',
	  JLP: 'Жуан ле Пен',
	  LAO: 'Лаоаг',
	  WFK: 'Френчвилль',
	  EBG: 'Эль Багре',
	  LAR: 'Ларамье',
	  AQG: 'Аньцин',
	  DHM: 'Дхарамсала',
	  MXH: 'Moro',
	  LJG: 'Лицзян',
	  BHE: 'Бленхайм',
	  PSJ: 'Посо',
	  GBG: 'Галесбург',
	  VIV: 'Вивигани',
	  NPL: 'Нью-Плимут',
	  KRX: 'Каркар',
	  BNA: 'Нашвилл',
	  PHG: 'Порт-Харкорт',
	  NQI: 'Кингсвилль',
	  QFU: 'Корралехо',
	  ZRN: 'Nyon',
	  LIQ: 'Lisala',
	  YOD: 'Колд-Лейк',
	  HDB: 'Хайдельберг',
	  ZGD: 'Гротон',
	  OKC: 'Оклахома-Сити',
	  TVF: 'Тиф-Ривер-Фолз',
	  POS: 'Порт-оф-Спейн',
	  MYA: 'Моруя',
	  TUO: 'Таупо',
	  JJN: 'Цзиньцзян',
	  MEC: 'Манта',
	  YYC: 'Калгари',
	  GWD: 'Гвадар',
	  MDE: 'Медельин',
	  RWI: 'Роки Маунт',
	  LYC: 'Ликселе',
	  IWD: 'Айенвуд',
	  BZO: 'Больцано',
	  FOU: 'Фугаму',
	  FNI: 'Нимес',
	  BIL: 'Биллингс',
	  FAV: 'Факарава',
	  AEB: '',
	  OIA: 'Оуриландиа',
	  AOG: 'Аншан',
	  MZV: 'Мулу',
	  EIN: 'Эйндховен',
	  YWR: 'Уайт-Ривер',
	  VHY: 'Виши',
	  PBI: 'Уэст Пальм Бич',
	  DND: 'Данди',
	  IXK: 'Кешод',
	  XWE: 'Веллингборо',
	  YEB: 'Бар Ривер',
	  PKJ: 'Плайя-Гранде',
	  XSR: 'Солсбери',
	  FKQ: 'Фак-Фак',
	  PLM: 'Палембанг',
	  ISJ: 'Айла Мухерес',
	  SUJ: 'Сату-Маре',
	  TLE: 'Тулеар',
	  RWL: 'Роулинс',
	  XKS: 'Kasabonika',
	  IVA: 'Ambanja',
	  SKT: 'Сиалкот',
	  PDP: 'Пунта-дель-Эсте',
	  RMQ: 'Шалу',
	  PBD: 'Порбандар',
	  BWT: 'Бурни',
	  CJS: 'Сиудад-Хуарес',
	  NNG: 'Наннин',
	  PRJ: 'Капри',
	  SMX: 'Санта-Мария',
	  CLO: 'Кали',
	  DGM: 'Дунгуан',
	  ZRW: 'Rastatt',
	  COU: 'Колумбия',
	  KEO: 'Одиенне',
	  WRG: 'Вранжелл',
	  RCQ: 'Реконкиста',
	  YZA: 'Ашкрофт',
	  SMQ: '',
	  XYN: 'Кристинехамн',
	  YNZ: 'Яньчэн',
	  GUW: 'Атырау',
	  CHS: 'Чарльзтон',
	  UGC: 'Ургенч',
	  YVP: 'Кууджуак',
	  AEG: 'Эк-Годан',
	  EPU: 'Пярну',
	  SOO: 'Содерхамн',
	  BNB: 'Boende',
	  YMG: 'Манитувейдж',
	  QTU: 'Иту Бразилия',
	  SIC: 'Синоп',
	  LYI: 'Линьи',
	  QNX: 'Макон',
	  KSH: 'Керманшах',
	  MMC: 'Сиудад-Манте',
	  VGA: 'Виджаявада',
	  HBZ: 'Хебер',
	  LEY: 'Лелистад',
	  ESN: 'Истон',
	  TIJ: 'Тихуана',
	  ONJ: 'Одате-Носиро',
	  BDQ: 'Вадодара',
	  ZME: '',
	  AOR: 'Алор-Сетар',
	  SRV: 'Стони Ривер',
	  HLJ: '',
	  VJI: 'Абингдон',
	  KRW: 'Туркменбаши',
	  ZHS: 'Klosters',
	  ITJ: 'Итаджаи',
	  SKB: 'Сент-Киттс',
	  JNN: 'Нанорталик',
	  BKW: 'Бекли',
	  BYC: 'Якуиба',
	  DDM: 'Dodoima',
	  HRD: 'Harstad',
	  OZC: 'Озамис-Сити',
	  YKS: 'Якутск',
	  OLJ: 'Олпой',
	  ZWT: 'Wittenberg',
	  FBS: 'Friday Harbor',
	  WVN: 'Вильхельмсхавен',
	  YZV: 'Сет-Иль',
	  BWU: 'Бэнкстаун',
	  WEW: 'Ви-Ваа',
	  ZGA: 'Gera',
	  KTS: 'Бревиг Миссион',
	  SDZ: 'Зона Шетландских островов',
	  QII: 'Lindau',
	  JUZ: 'Juzhou',
	  HOM: 'Хомер',
	  SFH: 'Сан-Фелипе',
	  EKO: 'Элко',
	  VNS: 'Варанаси',
	  UKY: 'Киото',
	  MGF: 'Маринга',
	  KGJ: 'Каронга',
	  QLJ: 'Люцерн',
	  RAI: 'Прая',
	  XDA: 'Дакс ле Терм',
	  YQH: 'Уотсон Лейк',
	  GHT: 'Гат',
	  ZPS: 'Ruesselsheim',
	  MYP: 'Мары',
	  KRL: 'Корла',
	  RBM: 'Штраубинг',
	  VPN: 'Вопнафьордур',
	  NCS: 'Ньюкасл',
	  VNX: 'Виланкулос',
	  ZVD: 'Дрангедал',
	  XBY: 'Байонна',
	  YMO: 'Мусони',
	  IJX: 'Джексонвилл',
	  IRC: 'Circle',
	  KOY: 'Olga Bay',
	  UHE: 'Угерске-Градиште',
	  FSZ: 'Сидзуока',
	  MYY: 'Мири',
	  ELI: 'Элим',
	  CRM: 'Катарман',
	  OZU: 'Монтилья',
	  VBM: '',
	  DHI: 'Дхангархи',
	  UGT: '',
	  HFE: 'Хефей',
	  ZAV: 'Aveiro',
	  LAP: 'Ла-Пас',
	  PNT: 'Пуэрто Наталес',
	  CEM: 'Central',
	  MGV: 'Маргарет-Ривер-Стейшн',
	  YAZ: 'Тофино',
	  MTI: 'Mosteiros',
	  EKX: 'Элизабеттаун',
	  EVE: 'Харстад-Нарвик',
	  VFA: 'Виктория-Фоллз',
	  UTU: 'Ustupo',
	  KGO: 'Кировоград',
	  LRB: 'Leribe',
	  RCP: 'Синдер Парк',
	  ZAH: 'Захедан',
	  USL: 'Юслесс-Луп',
	  GRI: 'Гранд-Айленд',
	  ZMO: 'Модена',
	  MPK: 'Мокпо',
	  TET: 'Тете',
	  AVL: 'Эшвилль',
	  TYS: 'Ноксвилл',
	  YAO: 'Яунде',
	  GLE: 'Гейнсвилль',
	  THV: 'Йорк',
	  GAY: 'Гайя',
	  MHU: 'Маунт-Хотхем',
	  ZJJ: 'Прочида',
	  VCA: 'Кантхо',
	  TAY: 'Тарту',
	  HAP: 'Лонг-Айленд',
	  MKG: 'Маскегон',
	  ADW: 'Кэмп-Спрингс',
	  YWH: 'Виктория-Харбор',
	  KIJ: 'Ниигата',
	  IHR: 'Иран Шахр',
	  XVZ: 'Верзон',
	  KRU: 'Керау',
	  ADU: 'Ардабил',
	  ZHZ: 'Халле',
	  OCC: 'Кока',
	  GRB: 'Грин Бэй',
	  SOI: 'Саут Молл',
	  SAK: 'Саударкрокур',
	  XLP: 'Матапедиа',
	  RLG: 'Росток Лаге',
	  BIQ: 'Биарриц',
	  PBJ: 'Паама',
	  SKX: 'Саранск',
	  LYH: 'Линчбург',
	  CZW: 'Czestochowa',
	  MJT: 'Митилини',
	  JMU: 'Цзямусы',
	  LLU: 'Аллуитсуп-Паа',
	  MVZ: 'Масвинго',
	  XFI: 'Port Daniel',
	  IGG: 'Igiugig',
	  PTA: 'Порт Алсворт',
	  AMD: 'Ахмедабад',
	  NKC: 'Нуакшот',
	  WEH: 'Вейхай',
	  RAM: 'Рамингининг',
	  PKP: 'Пукапука',
	  QVP: 'Avare',
	  PPS: 'Пуэрто-Принцеса',
	  BKX: 'Брукингс',
	  ZCK: 'Bruehl',
	  SWQ: 'Сумбава',
	  QOR: 'Орду',
	  ZBR: 'Ча-Бахар',
	  UME: 'Умеа',
	  HYC: 'High Wycombe',
	  ORB: 'Оребро Бофорс',
	  ATM: 'Альтамира',
	  WDH: 'Виндхук',
	  IHN: 'Qishn',
	  YSJ: 'Сент-Джон',
	  CDA: 'Куинда',
	  LGQ: 'Лаго Агрио',
	  QLQ: 'Лерида',
	  ESM: 'Эсмеральдас',
	  SGX: 'Songea',
	  YIO: 'Pond Inlet',
	  GNE: 'Гент',
	  ZCD: 'Бамберг',
	  OXB: 'Биссау',
	  LBP: 'Логн-Банга',
	  GUH: 'Ганнеда',
	  TPS: 'Трапани',
	  MUR: 'Маруди',
	  YOL: 'Yola',
	  CBH: 'Бешар',
	  MZY: 'Mossel Bay',
	  HPB: 'Hooper Bay',
	  BYA: 'Баундари',
	  SFX: 'Сан-Феликс',
	  BSC: 'Бахья-Солано',
	  MQX: 'Макале',
	  YPW: 'Пауэлл-Ривер',
	  ONA: 'Винона',
	  COR: 'Кордова',
	  TYR: 'Тайлер',
	  QVL: 'остров Виктория',
	  JRS: 'Иерусалим',
	  DNM: 'Денхем',
	  QJA: 'Jaragua Do Sul',
	  STG: 'Санкт-Джордж, остров',
	  SHP: 'Циньхуандао',
	  NOO: 'Наоро',
	  AIF: 'Ассиз',
	  ZBV: 'Beaver Creek',
	  CAC: 'Каскавел',
	  BHY: 'Бейхай',
	  ARY: 'Арарат',
	  WJU: 'Вонджу',
	  HCN: 'Хенчун',
	  IAS: 'Иаси',
	  TRE: 'Тири',
	  SNR: 'Сен-Назер',
	  GDZ: 'Геленджик',
	  PMW: 'Пальмас',
	  ZJP: 'Montreux',
	  OUA: 'Уагадугу',
	  ZYV: 'Вегаршеи',
	  JUV: 'Упернавик',
	  XHE: 'Йере',
	  SPQ: 'Сан-Педро',
	  CFA: '',
	  KVX: 'Киров',
	  GLV: 'Головин',
	  MOP: 'Маунт-Плезант',
	  ALY: 'Александрия',
	  XWD: 'Вейкфилд-Вестгейт',
	  VLK: 'Волгодонск',
	  PLW: 'Палу',
	  YQU: 'Гранд-Прери',
	  TUP: 'Тупело',
	  OKK: 'Кокомо',
	  KEX: 'Канабеа',
	  CAR: 'Карибу',
	  UGI: 'Уганик',
	  KOP: 'Накхон Патхом',
	  RHI: 'Райнелендер',
	  UOX: 'Университет Оксфорд',
	  IDN: 'Indagen',
	  MYU: 'Меориек',
	  OUL: 'Оулу',
	  LGP: 'Легаспи',
	  TMX: 'Тимимун',
	  HAU: 'Хаугезунд',
	  GCN: 'Гранд-Каньон',
	  MKK: 'Хулехуа',
	  PDS: 'Пиедрас-Неграс',
	  YWM: 'Уильямс Харбор',
	  XDK: 'Дюнкерк',
	  XHU: 'Хантингдон',
	  WNH: '',
	  FUL: 'Фуллертон',
	  LWY: 'Лавас',
	  MKM: 'Муках',
	  YNN: 'Яндикугина',
	  BTJ: 'Банда Асе',
	  WYS: 'Уэст Иелоустоун',
	  KMY: 'Мозер Бэй',
	  VXC: 'Lichinga',
	  ALE: 'Альпайн',
	  QVE: 'Форсса',
	  DSK: 'Дера Исмаил Хан',
	  PAN: 'Паттани',
	  MNM: 'Меномини',
	  VEY: 'Вейстменнейяр',
	  FOO: 'Numfoor',
	  PDV: 'Пловдив',
	  ZQB: 'Золинген',
	  HAW: 'Haverfordwest',
	  SQG: '',
	  JHM: 'Капалуа',
	  MTH: 'Марафон',
	  YOP: 'Рейнбоу Лейк',
	  WGE: 'Валгетт',
	  EMS: 'Embessa',
	  PSR: 'Пескара',
	  NQY: 'Ньюкуэй',
	  MNQ: 'Монто',
	  YZT: 'Порт Харди',
	  IAA: 'Игарка',
	  FNL: 'Форт-Коллинс',
	  KNQ: 'Коне',
	  KWF: 'Waterfall',
	  BWG: 'Боулинг-Грин',
	  TSV: 'Таунсвилль',
	  TIQ: 'Тиниан',
	  KZS: 'Кастелоризо',
	  TSF: 'Тревизо',
	  OSK: 'Оскарсхамн',
	  DQA: '',
	  GBZ: 'Грейт-Барьер-Айленд',
	  AGO: 'Магнолиа',
	  YAG: 'Форт-Франсис',
	  TLJ: 'Таталина',
	  KGX: 'Grayling',
	  ZOH: 'Luenen',
	  BIC: '',
	  ASA: 'Ассаб',
	  LMJ: '',
	  RAL: 'Риверсайд',
	  VPG: '',
	  TNP: 'Twenty Nine Palms',
	  SHJ: 'Шарджа',
	  HDY: 'Хат Яи',
	  TIZ: 'Тари',
	  JSM: 'Jose De San Martin',
	  AKJ: 'Асахикава',
	  SHD: 'Стаунтон',
	  HAR: 'Харрисбург',
	  HHA: 'Хуанхуа',
	  QVN: 'Авеллино',
	  LDJ: 'Линден',
	  NVK: 'Нарвик',
	  BIX: 'Билокси',
	  ZPF: 'Пассау',
	  ILP: 'Иль де Пэн',
	  WUH: 'Вухан',
	  NLG: 'Нельсон Лагун',
	  TIW: 'Такома',
	  KTL: 'Китале',
	  AGT: 'Сиудад-дель-Эсте',
	  AHU: 'Аль-Хосейма',
	  XFK: 'Senneterre',
	  LRR: 'Лар',
	  XFN: 'Сяньфань',
	  ZHO: 'Хьюстон',
	  GFK: 'Гранд Форкс',
	  NLT: '',
	  OKP: 'Oksapmin',
	  VIL: 'Дахла',
	  ITP: 'Итаперуна',
	  PYY: '',
	  LAH: 'Labuha',
	  DLI: 'Далат',
	  LAE: 'Лае',
	  SPR: 'Сан-Педро',
	  CCD: 'Сенчури-Сити',
	  HID: 'Хорн-Айленд',
	  UUA: 'Бугульма',
	  ILE: 'Килин',
	  RGK: 'Горно-Алтайск',
	  ATZ: 'Асьют',
	  WOL: 'Воллонгонг',
	  AGR: 'Агра',
	  COV: 'Covilha',
	  MAH: 'Менорка',
	  KVD: 'Гянджа',
	  PPE: 'Пуэрто-Пенаско',
	  PZI: 'Панжихуа',
	  GMB: 'Gambela',
	  CIY: 'Комисо',
	  HUV: 'Худиксвалл',
	  CGS: 'Колледж-Парк',
	  ISW: 'Висконсин-Рэпидз',
	  PVS: 'Бухта Провидения',
	  PGA: 'Паг',
	  ZOQ: 'Neuss',
	  XER: '',
	  MTT: 'Минатитлан',
	  MJZ: 'Мирный',
	  CHY: 'Шуазель Bay',
	  CMV: 'Коромандель',
	  SCV: 'Сучава',
	  LBQ: 'Ламбарене',
	  LPC: 'Lompoc',
	  BHB: 'Бар-Харбор',
	  RDN: 'Реданг',
	  QTR: 'Тартус',
	  ASX: 'Эшланд',
	  DLU: 'Дали',
	  SUM: 'Самтер',
	  CAP: 'Кеп-Гаитиен',
	  MKS: 'Mekane',
	  HHR: 'Хауторн',
	  LAL: 'Лакленд',
	  CCV: 'Крейг-Кав',
	  WNS: 'Навабшах',
	  GYI: 'Гисеный',
	  SOV: 'Селдовиа',
	  MFI: 'Маршфилд',
	  ODW: 'Оук-Харбор',
	  FTW: 'Форт-Уорт',
	  ZPP: 'Reutlingen',
	  TGC: 'Трентон',
	  MVC: 'Монровилль',
	  RCO: 'Рошфор',
	  XKV: 'Sackville',
	  QBO: 'Бохум',
	  KDL: 'Кярдла',
	  HBI: 'Харбор-Айленд',
	  BTR: 'Батон-Руж',
	  FUE: 'Пуэрто-Дель-Росарио',
	  MNS: 'Манса',
	  KTW: 'Катовице',
	  IQM: 'Qiemo',
	  PXM: 'Пуэрто-Эскондидо',
	  IRI: 'Иринга',
	  JVA: 'Анкавандра',
	  KBC: 'Берч-Крик',
	  KPK: 'Паркс',
	  DLC: 'Далянь',
	  CDQ: 'Кройдон',
	  BVH: 'Вилена',
	  GMA: 'Гемене',
	  ZOE: 'Людвигсхафен',
	  QJN: 'Jounieh',
	  MYR: 'Миртл Бич',
	  URY: 'Гураят',
	  DHA: 'Дхахран',
	  SNE: 'Сан Николау',
	  TPQ: 'Тепик',
	  BND: 'Бендер-Аббас',
	  SRI: '',
	  XWB: 'Стирлинд',
	  ZNS: 'Kempten',
	  XYY: 'Arvika',
	  CMY: 'Спарта',
	  EHM: 'Cape Newenham',
	  ORV: 'Норвик',
	  YTH: 'Томпсон',
	  JJM: '',
	  NAK: 'Нахон-Ратчасима',
	  BVB: 'Боа Виста',
	  NAT: 'Наталь',
	  OME: 'Ном',
	  LSP: 'Лас-Пьедрас',
	  XKH: 'Сиангхуанг',
	  RAB: 'Рабаул',
	  MQQ: 'Мунду',
	  MAY: 'Mangrove Cay',
	  KUT: 'Кутаиси',
	  PJG: 'Панджгур',
	  LVM: 'Ливингстон',
	  BRC: 'Сан Карлос де Барилоче',
	  TLT: 'Тулуксак',
	  OHE: '',
	  QBW: 'Бейтсмен-Бэй',
	  SPW: 'Спенсер',
	  NTX: 'Натуна-Ранай',
	  XMY: 'Ям-Айленд',
	  TWF: 'Твин-Фоллс',
	  PIE: 'Санкт-Петербург',
	  JKL: 'о-в Калимнос',
	  IDR: 'Индор',
	  ZKO: 'Sierre',
	  KME: 'Камембе',
	  XII: 'Прескотт',
	  SYB: 'Seal Bay',
	  EKN: 'Элкинс',
	  NLV: 'Николаев',
	  BEU: 'Бедури',
	  VCV: 'Викторвилль',
	  MOA: 'Моа Куба',
	  GLT: 'Глэдстон',
	  ZGN: 'Наньнин',
	  BMV: 'Буонматхуот',
	  BKQ: 'Блэколл',
	  WMN: 'Мароанцетра',
	  RVR: 'Грин Ривер',
	  ATR: 'Атар',
	  GBK: 'Гбангбаток',
	  BIA: 'Бастия',
	  REA: 'Реао',
	  KYZ: 'Кызыл',
	  QVW: 'Котка',
	  CXI: 'о. Рождества',
	  POL: 'Пемба',
	  BAL: 'Batman',
	  TKM: 'Тикал',
	  RTB: 'Роатан',
	  BJL: 'Банджул',
	  DOC: 'Дорноч',
	  UKA: 'Ukunda',
	  NYM: 'Надым',
	  NNL: 'Нондалтон',
	  ELT: 'El Tor',
	  CBP: 'Коимбра',
	  QCK: 'Cabo Frio Br',
	  CLQ: 'Колима',
	  RTY: 'Мерти',
	  TTU: 'Тетуан',
	  BYO: 'Бонито',
	  HDD: 'Хидерабад',
	  MNA: 'Melangguane',
	  HLZ: 'Гамильтон',
	  LKG: 'Локичоггио',
	  HBG: 'Хаттисберг',
	  ONS: 'Онслов',
	  YXN: 'Уэйл-Ков',
	  JLN: 'Джоплин',
	  ANT: 'St Anton',
	  GWL: 'Гвалиор',
	  KAZ: 'Кау Индонезия',
	  CDW: 'Колдвелл',
	  GUZ: 'Гуарапари',
	  MAK: 'Малакал',
	  OIT: 'Оита',
	  YZR: 'Сарния',
	  UAI: 'Суаи',
	  YQC: 'Quaqtaq',
	  LIA: 'Ляньпин',
	  TGN: 'Траралгон',
	  VRY: 'Vaeroy',
	  XCC: 'Ле Крезо',
	  XHS: 'Чемайнус',
	  MRB: 'Мартинсбург',
	  YHY: 'Хей Ривер',
	  AIK: 'Эйкен',
	  IXY: 'Кандия',
	  SPZ: 'Спрингдейл',
	  ZAJ: 'Зарандж',
	  PSI: 'Pasni',
	  SYK: 'Stykkisholmur',
	  MUN: 'Матурин',
	  DUR: 'Дурбан',
	  JUI: 'Юист',
	  RNN: 'Борнхольм',
	  AJN: 'Анжуан',
	  YSN: 'Сэмон-Арм',
	  NMA: 'Наманган',
	  PSK: 'Дублин',
	  XVE: 'Версаль',
	  ZEP: '',
	  BDB: 'Бундаберг',
	  EUX: 'Сент-Эстатиус',
	  NOS: 'Носси-Бе',
	  GED: 'Джорджтаун',
	  GAL: 'Галена',
	  AOH: 'Лима',
	  AXU: 'Аксум',
	  SZZ: 'Щецин',
	  WKJ: 'Вакканаи',
	  YQV: 'Йорктон',
	  LPD: 'Ла-Педрера',
	  EOK: 'Кеокук',
	  JHB: 'Джохор-Бару',
	  MZO: 'Манзанильо',
	  LAZ: '',
	  DSM: 'Де-Мойн',
	  GPB: 'Guarapuava',
	  WUA: 'Wuhai',
	  XLN: 'Лаон',
	  CUC: 'Кукута',
	  TAK: 'Такамацу',
	  CPX: 'Кулебра',
	  AID: 'Андерсон',
	  ZBO: 'Боуэн',
	  IRA: 'Киракира',
	  TUC: 'Тукуман',
	  SJK: 'Сан-Хосе Дос Кампос',
	  TMI: 'Tumling Tar',
	  YQA: 'Мускока',
	  BBX: 'Блю-Белл',
	  ZRI: 'Serui',
	  MWT: 'Мулаватана',
	  EYW: 'Ки-Уэст',
	  NCT: 'Никойя',
	  HMI: 'Хами',
	  XKQ: 'Сарпсборг',
	  RWS: 'Сумаре',
	  HSI: 'Гастингс',
	  GFL: 'Гленз Фоллз',
	  RHN: 'Рош Пина',
	  ZBL: 'Билоэла',
	  NYE: 'Ньери',
	  TOK: 'Торокина',
	  TSP: 'Техачапи',
	  BNT: 'Bundi',
	  WLD: 'Уинфилд',
	  LEW: 'Lewiston',
	  XAH: 'Силкеборг',
	  BOW: 'Бартоу',
	  VIT: 'Витория',
	  ABF: 'Абайянг',
	  IXS: 'Силчар',
	  ESU: 'Эссаурия',
	  KRJ: 'Каравари',
	  MNY: 'Моно',
	  WAL: 'Чинкотик',
	  YQY: 'Сидней',
	  FOA: 'Foula',
	  MIU: 'Майдугури',
	  JAB: 'Джабиру',
	  YMK: '',
	  ONI: 'Моанамани',
	  QDM: 'Shek Mum',
	  OND: 'Ондангва',
	  BZG: 'Быдгошч',
	  MJC: 'Man CI',
	  KOV: 'Кокшетау',
	  ERU: 'Erume',
	  SRL: 'Санта-Росалия',
	  OAK: 'Окленд',
	  WAA: 'Уэльс',
	  TRU: 'Трухильо',
	  LUR: 'Мыс Лисберн',
	  MDL: 'Мандалей',
	  MZC: 'Мицик',
	  ATX: 'Атбасар',
	  SAM: 'Саламо',
	  UDJ: 'Ужгород',
	  HTI: 'Хамильтон-Айленд',
	  KTN: 'Кетчикан',
	  TDL: 'Тандиль',
	  BVG: 'Berlevag',
	  WDG: 'Инид',
	  SUR: 'Самер Бивер',
	  ACX: 'Синъи',
	  THO: 'Thorshofn',
	  TNR: 'Антананариву',
	  FLB: 'Флориано',
	  BAG: 'Багуйо',
	  YDQ: 'Доусон-Крик',
	  PVO: 'Портовиехо',
	  TEE: 'Тбесса',
	  GMO: '',
	  VPE: 'Ongiva',
	  AGB: 'Аугсбург',
	  HCQ: 'Холлз Крик',
	  LUL: 'Лорел',
	  JUB: 'Джуба',
	  YOC: 'Олд-Кроу',
	  PLD: 'Playa Samara',
	  YMS: 'Юримагуас',
	  CYP: 'Калбайог',
	  BLV: 'Бельвилль',
	  XMH: 'Манихи',
	  DYL: 'Доилестоун',
	  RKA: '',
	  USQ: 'Усак',
	  KTR: 'Кэтрин',
	  CWC: 'Черновцы',
	  SCZ: 'Остров Санта Круз',
	  DJG: 'Джанет',
	  QFL: 'Freilassing',
	  SLX: 'Солт-Кей',
	  JNX: 'Наксос',
	  LMI: 'Луми',
	  JRH: 'Jorhat',
	  QCS: 'Козенца',
	  RUR: 'Рурути',
	  TRZ: 'Тиручираппали',
	  OKT: 'Октябрьский',
	  QLI: 'Лимасол',
	  AAT: 'Алтай',
	  QBK: 'Betim',
	  QDV: 'Jundiai',
	  WRY: 'Вестрей',
	  NVT: 'Навегантес',
	  LXS: 'Лимнос',
	  XHY: 'Андай',
	  TJU: 'Куляб',
	  ACP: 'Cаханд',
	  OXF: 'Оксфорд',
	  FHZ: 'Факахина',
	  PDG: 'Паданг',
	  KOW: 'Ганджоу',
	  CEG: 'Честер',
	  DXR: 'Данбери',
	  SCN: 'Саарбрюккен',
	  ZOJ: 'Marl',
	  KKI: 'Акичак',
	  SJP: 'Сан-Хосе До Рио-Прето',
	  PMN: 'Пумани',
	  MEU: 'Монте-Дурадо',
	  RMS: 'Рамштайн',
	  ADE: 'Аден',
	  SWP: 'Свакопмунд',
	  LYP: 'Faisalabad',
	  WKK: 'Алекнагик',
	  XME: 'Мобеж',
	  MAU: 'Маупити',
	  NVA: 'Нейва',
	  YJA: 'Джаспер',
	  AEX: 'Александрия',
	  XDU: 'Херви',
	  THE: 'Терезина',
	  LTX: 'Латакунда',
	  MTM: 'Метлакатла',
	  MVT: 'Матаива',
	  PYM: 'Плимут',
	  YGL: 'Ла Гранд',
	  SFS: 'Субик Бэй',
	  DSA: 'Донкастер',
	  BFF: 'Скоттсблафф',
	  CMA: 'Куннамулла',
	  FCB: 'Фиксбург',
	  CGQ: 'Чанчун',
	  SVH: 'Стейтсвилль',
	  MGC: 'Мичиган-Сити',
	  ZWW: 'Hampton',
	  FTA: 'Футуна-Айленд',
	  JNU: 'Джуно',
	  CIO: 'Консепсьон',
	  TKU: 'Турку',
	  BDU: 'Бардуфосс',
	  LIH: 'Лиху',
	  XGC: 'Лунд C',
	  IRG: 'Локхард-Ривер',
	  MEK: 'Мекнес',
	  MCE: 'Мерсед',
	  BFG: 'Буллфрог Базин',
	  CKD: 'Крукед-Крик',
	  GPO: 'Хенераль-Пико',
	  FYN: 'Fuyun',
	  BFP: 'Бивер Фоллз',
	  ANX: 'Анденес',
	  PTH: 'Порт Гейден',
	  YNS: 'Nemiscau',
	  MNI: 'Монтсеррат',
	  GSY: 'Гримсби',
	  YPX: 'Puvirnituq',
	  YMA: 'Mayo',
	  PMT: 'Paramakatoi',
	  GYR: 'Гудиер',
	  EMI: 'Emirau',
	  RNR: 'Robinson River',
	  MEJ: 'Meadville',
	  MME: 'Тиссайд',
	  JSR: 'Джессор',
	  PNG: 'Паранагуа',
	  XPK: 'Пукатаваган',
	  PIN: 'Паринтинс',
	  REU: 'Реус',
	  XDN: 'Дуэ',
	  JMS: 'Джеймстаун',
	  BCH: 'Баукау',
	  INA: 'Инта',
	  SMK: 'St Michael',
	  JSI: 'Скиатлос',
	  MDJ: '',
	  LIW: 'Лойкау',
	  WIN: 'Винтон',
	  SGU: 'Сент Джордж',
	  XDM: 'Драммондвиль',
	  ATP: 'Аитапе',
	  ULY: 'Ульяновск',
	  KPN: 'Кипнек',
	  CCP: 'Консепсьон',
	  KWK: 'Квигиллингок',
	  MRO: 'Мастертон',
	  PFJ: 'Патрексфьордур',
	  XEJ: 'Лангфорд',
	  ISA: 'Маунт-Иса',
	  LWN: 'Ленинакан',
	  JNZ: 'Цзиньчжоу',
	  IDI: 'Индиана',
	  SVL: 'Савонлинна',
	  FFT: 'Frankfort',
	  MIM: 'Меримбула',
	  ELS: 'Ист-Лондон',
	  LNO: 'Леонора',
	  SOW: 'Шоу Ло',
	  BRO: 'Браунсвилль',
	  GTZ: '',
	  CRC: 'Картаго',
	  RAP: 'Рэпид Сити',
	  HEK: 'Хейхе',
	  MKL: 'Джексон',
	  ZNC: 'Nyac',
	  HVK: 'Holmavik',
	  LEV: 'Левука',
	  ULZ: 'Uliastai',
	  OGN: 'Йонагуни',
	  YFB: 'Икалуит',
	  LGH: 'Лей-Крик',
	  QEW: 'Лестер',
	  NAJ: 'Нахичевань',
	  PLB: 'Платтсбург',
	  WNP: 'Нага',
	  YPM: 'Сен-Пьер',
	  QLE: 'Литон',
	  ORF: 'Норфолк',
	  BEA: 'Берейна',
	  IPW: 'Ипсуич',
	  CGB: 'Куиаба',
	  YAV: 'Miner\'s Bay',
	  BNP: 'Bannu',
	  INM: 'Иннаминка',
	  QHB: 'Пиракикаба',
	  HPP: 'Poipet',
	  ZEC: 'Секунда',
	  SZJ: 'Сигуанеа',
	  BLA: 'Барселона',
	  MKN: 'Malekolon',
	  MJR: 'Мирамар',
	  NEG: 'Негрил',
	  HWA: 'Хавабанго',
	  AOC: 'Альтенбург',
	  PCL: 'Пукаллпа',
	  STD: 'Санто-Доминго',
	  DUM: 'Думаи',
	  OMD: 'Ораньемунд',
	  TQD: '',
	  BTH: 'Батам',
	  MDR: 'Медфра',
	  FOK: 'Уэстхемптон',
	  MKU: 'Макоку',
	  CRP: 'Корпус-Кристи',
	  PTP: 'Поинте-А-Питре',
	  NPT: 'Ньюпорт',
	  RSJ: 'Росарио',
	  DWB: 'Soalala',
	  OYG: 'Мойо',
	  LRD: 'Ларедо',
	  DRG: 'Диринг',
	  ASM: 'Асмэра',
	  TAG: 'Tagbilaran',
	  QYP: 'Апелдорн',
	  PBG: 'Платтсбург',
	  YZZ: 'Трейл',
	  NYI: 'Sunyani',
	  SIR: 'Сион',
	  JGA: 'Джамнагар',
	  CGO: 'Чженьчжоу',
	  HUM: 'Хоума',
	  KQA: 'Акутан',
	  KVC: 'Кинг Ков',
	  PVA: 'Провиденсиа',
	  MXN: 'Морлэ',
	  NYK: 'Нануки',
	  AMW: 'Амес',
	  RBY: 'Руби',
	  PUO: 'Prudhoe Bay',
	  JJI: 'Хуанхуй',
	  JCN: 'Incheon',
	  PZH: 'Zhob',
	  CUF: 'Кунео',
	  ZEL: 'Bella Bella',
	  XMN: 'Сямэнь',
	  XLV: 'Ниагара-Фолз',
	  KIQ: 'Кира',
	  XLQ: '',
	  ZRL: 'Ланкастер',
	  MDI: 'Макурди',
	  NAH: 'Наха',
	  WBM: 'Вапенаманда',
	  RBG: 'Розбург',
	  PER: 'Перт',
	  GLQ: 'Glennallen',
	  HHZ: 'Хикуэру',
	  PEW: 'Пешавар',
	  IFP: 'Буллхед',
	  SKS: 'Skrydstrup',
	  ROG: 'Роджерс',
	  ENE: 'Ende',
	  VUS: 'Великий Устюг',
	  KGA: 'Кананга',
	  PPW: 'Папа-Вестрей',
	  THR: 'Тегеран',
	  BAZ: 'Barbelos',
	  IAN: 'Киана',
	  RGL: 'Рио-Гальегос',
	  KEQ: 'Кебар',
	  GAR: 'Гараина',
	  XGV: 'Сен Жиль Круа де Ви',
	  AAB: 'Аррабери',
	  YNY: 'Янян',
	  XAK: 'Хернинг',
	  FPR: 'Форт-Пирс',
	  ULD: 'Улунди',
	  CTM: 'Четумал',
	  INT: 'Уинстон-Салем',
	  RTA: 'Rotuma Island',
	  HLH: 'Уланьхот',
	  WUG: 'Wau PG',
	  GSO: 'Гринсборо',
	  QUL: 'Ульм',
	  SXQ: 'Солдотна',
	  EAE: 'Эмей',
	  TCO: 'Тумако',
	  VUP: 'Валледупар',
	  ARC: 'Арктик Виллидж',
	  AXA: 'Ангилья',
	  OLU: 'Колумбус',
	  KUP: 'Купиано',
	  BGK: 'Биг-Крик',
	  KJI: '',
	  HUU: 'Уануко',
	  GUL: 'Гоулбурн',
	  FPO: 'Фрипорт',
	  ZXO: 'Fauske',
	  KEP: 'Непалгандж',
	  DRR: 'Дери',
	  TZX: 'Трабзон',
	  BGJ: 'Боргарфьордур',
	  ELU: 'Эль-Уэд',
	  QPA: 'Падуя',
	  BLI: 'Беллингем',
	  PRP: 'Проприано',
	  JSS: 'Spetsai Island',
	  PIS: 'Пуатье',
	  BQS: 'Благовещенск',
	  GLN: 'Гулимим',
	  PLY: 'Плимут',
	  SFN: 'Санта-Фе',
	  LNK: 'Линкольн',
	  ZNF: 'Hanau',
	  BIS: 'Бисмарк',
	  QVA: 'Варезе',
	  KOI: 'Киркволл',
	  QQX: 'Бат',
	  ATC: 'Артурс-Таун',
	  CUZ: 'Куско',
	  NVD: 'Невада',
	  MAL: 'Mangole',
	  ZAO: 'Каор',
	  BGX: 'Bage',
	  JIQ: '',
	  FRW: 'Френсистаун',
	  PNS: 'Пенсакола',
	  NNX: '',
	  AUS: 'Остин',
	  KUU: 'Кулу',
	  IRM: '',
	  IVC: 'Инверкаргилл',
	  SFG: 'Санкт-Мартин',
	  DIY: 'Диярбакыр',
	  ITN: 'Итабуна',
	  JDR: '',
	  ZRD: 'Ричмонд',
	  FSD: 'Сиу-Фоллс',
	  PDL: 'Понта-Делгада',
	  YRI: 'Ривьер-дю-Лу',
	  LPL: 'Ливерпуль',
	  TOE: 'Тозер',
	  TEX: 'Теллурайд',
	  XFD: 'Стратфорд',
	  KUH: 'Кусиро',
	  KVG: 'Кавьенг',
	  DRW: 'Дарвин',
	  AGH: 'Ангелхольм',
	  MSH: 'Масира',
	  LBB: 'Луббок',
	  VKS: 'Виксбург',
	  XNR: 'Аабенраа',
	  STP: 'Сент-Пол',
	  RZP: '',
	  XMM: 'Монако',
	  ZTV: '',
	  AGU: 'Агуаскалиентес',
	  ZTF: 'Вестчестер Каунти',
	  DYG: 'Дэань',
	  TWB: 'Тувумба',
	  GER: 'Нуэва-Герона',
	  CQD: 'Шахр-Корд',
	  LUW: 'Лувук',
	  LSH: 'Лашио',
	  MLI: 'Молин',
	  YRT: 'Ранкин-Инлет',
	  SQL: 'Сан-Карлос',
	  LMM: 'Лос-Мошис',
	  SKN: 'Стокмарнес',
	  BEW: 'Бейра',
	  SMA: 'Санта-Мария',
	  ZBW: 'Атибайя',
	  MSE: 'Мэнстон',
	  XMF: 'Монтбелье',
	  YTJ: 'Террейс Бэй',
	  ABD: 'Абадан',
	  CRZ: 'Туркменабад',
	  DOX: 'Донгара',
	  ENW: 'Кеноша',
	  MLC: 'МакАлестер',
	  TOS: 'Тромсе',
	  TKP: 'Такапото',
	  HMB: '',
	  FLS: 'Флиндерз-Айленд',
	  KOR: 'Кокоро',
	  TFF: 'Тефе',
	  REX: 'Рейноса',
	  DOV: 'Дувр',
	  JIU: 'Цзюцзянь',
	  TUY: 'Тулум',
	  KOB: 'Кутаба',
	  ZGK: 'Левен',
	  QTH: 'Тредбо',
	  BRX: 'Барахона',
	  JUL: 'Хулиака',
	  ZQU: 'Wolfsburg',
	  BTA: 'Бертуа',
	  TTR: 'Тана-Тораджа',
	  XFV: 'Брэнтфорд',
	  PDA: 'Пуэрто-Инирида',
	  XHF: 'Honefoss',
	  IOP: 'Ioma',
	  ECG: 'Элизабет-Сити',
	  CES: 'Сесснок',
	  RKS: 'Рок-Спрингс',
	  OMA: 'Омаха',
	  ABV: 'Абуджа',
	  RAT: 'Радужный',
	  OOK: 'Токсук Бэй',
	  GEL: 'Санто-Анджело',
	  BCX: 'Белорецк',
	  LWE: 'Леволеба',
	  AKS: 'Акуи',
	  QRO: 'Керетаро',
	  URJ: 'Урай',
	  BUL: 'Булоло',
	  LLK: 'Ленкорань',
	  LUA: 'Лукла',
	  CEB: 'Себу',
	  DRS: 'Дрезден',
	  PIF: 'Пиндун',
	  XON: 'Карлетон',
	  QYI: 'Хилверсюм',
	  ZLT: 'Ла Табатьер',
	  KWE: 'Гуйянг',
	  UKK: 'Усть-Каменогорск',
	  LUQ: 'Сан-Луис',
	  QBV: 'Беневенто',
	  HAC: 'Хачиджо Джима',
	  XSK: 'Сент-Рафаэль',
	  DBA: 'Далбандин',
	  XAT: 'Антиб',
	  ZPZ: 'Sindelfingen',
	  LEC: 'Ленкуа',
	  PUB: 'Пуэбло',
	  PHR: 'Писифик-Харбор',
	  YTF: 'Альма',
	  WED: 'Wedau',
	  TKG: 'Бандар-Лампунг',
	  YBB: 'Пелли-Бей',
	  AGX: 'Агатти-Айленд',
	  YHM: 'Гамильтон',
	  BGO: 'Берген',
	  MYC: 'Маракай',
	  ZTZ: 'Chemnitz',
	  MSX: 'Моссенджо',
	  CWG: 'Callaway Gardens',
	  HSP: 'Хот Спрингз',
	  VIN: 'Винница',
	  TBO: 'Табора',
	  MRE: 'Мара-Лоджес',
	  ZCQ: 'Курико',
	  QNP: 'Айя-Напа',
	  RBQ: 'Rurrenabaque',
	  PEI: 'Перейра',
	  ENA: 'Кенаи',
	  IAM: 'In Amenas',
	  BLL: 'Биллунд',
	  SSS: 'Siassi',
	  BRP: 'Biaru',
	  GGS: 'Gobernador Gregores',
	  XEI: 'Цукуба',
	  YYY: 'Монт-Джоли',
	  YFS: 'Форт-Симпсон',
	  UCT: 'Ухта',
	  AHN: 'Атенс',
	  XPL: 'Комаягуа',
	  LBY: 'Ла Боль',
	  MQT: 'Маркетт',
	  ADM: 'Ардмор',
	  BBF: 'Берлингтон',
	  JOE: 'Йоэнсуу',
	  GYA: 'Guayaramerin',
	  DAY: 'Дейтон',
	  LAI: 'Ланнион',
	  KUB: 'Куала-Белаит',
	  TEY: 'Thingeyri',
	  WLL: 'Воллогорэнг',
	  CFK: 'Члеф',
	  AIT: 'Аитутаки',
	  NDD: 'Сумбе',
	  MYG: 'Мейагуана',
	  BCE: 'Брайс',
	  SOT: 'Sodankyla',
	  MEH: 'Мехамн',
	  XAN: 'Аленкон',
	  AST: 'Астория',
	  OSH: 'Ошкош',
	  GOE: 'Gonalia',
	  LPP: 'Лаппенранта',
	  EME: 'Emden',
	  EFG: 'Efogi',
	  KMQ: 'Комацу',
	  XOR: 'Otta',
	  CBK: 'Колби',
	  QGB: 'Лимейра',
	  XAL: 'Аламос',
	  TNA: 'Цзинань',
	  DSD: 'Ла-Дезираде',
	  EHT: 'Ист Хартфорд',
	  SRU: 'Санта-Крус',
	  HVN: 'Нью-Хейвен',
	  ICK: 'Nieuw Nickerie',
	  QIE: 'Истрес',
	  WHT: 'Уартон',
	  ZYW: 'Сандвика',
	  XPT: 'Престон',
	  YTS: 'Тимминс',
	  DYU: 'Душанбе',
	  JAU: 'Хауха',
	  GRW: 'Грасиоса-Айленд',
	  XNC: '',
	  RCS: 'Рочестер',
	  AKB: 'Атка',
	  KSO: 'Кастория',
	  LLY: 'Моунт Холли',
	  WLK: 'Селавик',
	  LKC: 'Лекана',
	  IOK: 'Iokea',
	  AWZ: 'Ахваз',
	  PPN: 'Попайан',
	  TRL: 'Террелл',
	  ASW: 'Асуан',
	  OUD: 'Уджда',
	  ADA: 'Адана',
	  TIF: 'Таиф',
	  PBR: 'Пуэрто Барриос',
	  STZ: 'Santa Terezinha',
	  MSR: 'Муш',
	  LES: 'Лесобенг',
	  BTM: 'Бутте',
	  CUK: 'Caye Caulker',
	  ZKE: 'Kaschechewan',
	  TUD: 'Тамбакунда',
	  QAR: 'Арнем',
	  LFM: 'Lamerd',
	  XUY: 'Оре',
	  YHD: 'Драйден',
	  RDG: 'Ридинг',
	  KCG: 'Чигник',
	  ERL: 'Эрлданда',
	  PJZ: 'Puerto Juarez',
	  ALB: 'Олбани',
	  YKF: 'Китченер',
	  QNO: 'Ascoli Piceno',
	  BZN: 'Боземан',
	  NVR: 'Новгород',
	  YTQ: 'Tasiujaq',
	  YAX: 'Angling Lake',
	  VKT: 'Воркута',
	  MAF: 'Мидленд',
	  MFD: 'Менсфилд',
	  RIL: 'Райфл',
	  XFF: '',
	  ROC: 'Рочестер',
	  TMW: 'Тамворт',
	  DRD: 'Дорунда',
	  LUM: 'Луси',
	  VCS: 'Кон Дао',
	  MPV: 'Монпелье',
	  QTJ: 'Шартр',
	  LII: 'Мулиа',
	  BOY: 'Бобо-Диуласо',
	  STE: 'Стивенс Поинт',
	  XMK: 'Монтелимар',
	  DIO: 'Diomede Island',
	  CDB: 'Колд-Бей',
	  FSM: 'Форт-Смит',
	  YFC: 'Фредериктон',
	  VLN: 'Валенсия',
	  KUG: 'Кубин-Айленд',
	  KPS: 'Кемпси',
	  QKS: 'Кейстоун',
	  AGV: 'Акаригуа',
	  PPF: 'Парсонс',
	  KUV: 'Кунсан',
	  MGP: 'Манга',
	  GOZ: 'Gorna Orjahovica',
	  PMF: 'Парма',
	  LIO: 'Лимон',
	  ZDV: 'Давос',
	  SXV: 'Сейлем',
	  PIR: 'Пьер',
	  BBO: 'Бербера',
	  HMR: 'Хамар',
	  ERN: 'Eirunepe',
	  JDZ: 'Цзиньдэчжэнь',
	  QTC: 'Казерта',
	  NSH: 'Now Shahr',
	  CMH: 'Колумбус',
	  APK: 'Апатаки',
	  PRY: 'Претория',
	  APS: 'Анаполис',
	  NEC: 'Necochea',
	  MEI: 'Меридиан',
	  TXN: 'Тунси',
	  NAO: 'Наньчон',
	  QNT: 'Нитерой',
	  HMA: 'Ханты-Мансийск',
	  PAS: 'Парос',
	  GUP: 'Галлап',
	  VTN: 'Valentine',
	  BMU: 'Бима',
	  DTA: 'Delta',
	  RHO: 'Родос',
	  BBQ: 'Барбуда',
	  VCL: 'Тамки',
	  IOS: 'Илеус',
	  NSO: 'Сконе',
	  VDB: 'Фагернес',
	  XWY: 'Вайоминг',
	  AVN: 'Авиньон',
	  NOU: 'Ноумеа',
	  TGL: 'Тагула',
	  YQT: 'Тандер-Бей',
	  ZEH: 'Garbsen',
	  ACI: 'Олдерней',
	  NAG: 'Нагпур',
	  PNZ: 'Петролина',
	  YSU: 'Саммерсайд',
	  ENH: 'Энши',
	  GEN: 'Пуэнте-Хениль',
	  SWC: 'Ставелл',
	  MDP: 'Миндиптана',
	  MQY: 'Смирна',
	  AAF: 'Апалачикола',
	  MDZ: 'Мендоса',
	  FUJ: 'Фукуэ',
	  BGV: 'Бенто-Гонсалвес',
	  QDQ: 'Duque De Caxias',
	  CGR: 'Кампогранде',
	  ZKZ: 'Vevey',
	  MWA: 'Марион',
	  ALO: 'Ватерлоо',
	  OCF: 'Окала',
	  EEP: '',
	  MZZ: 'Марион',
	  KRS: 'Кристианстад',
	  LXH: 'Люшон',
	  PMZ: 'Палмар',
	  KMF: 'Камина',
	  PVC: 'Провинстаун',
	  JHW: 'Джеймстаун',
	  NGK: 'Ноглики',
	  GOP: 'Горахпур',
	  QKW: 'Каназава',
	  QMI: 'Mogi Das Cruzes',
	  FRO: 'Флоро',
	  AUW: 'Ваусау',
	  JNG: 'Цзинин',
	  HLS: 'Сент Хеленс',
	  CSE: 'Crested Butte',
	  YNP: 'Натуашиш',
	  EMP: 'Emporia',
	  COS: 'Колорадо-Спрингс',
	  FEN: 'Фернандо-де-Норонья',
	  HEI: 'Хайде-Бюсум',
	  GPZ: 'Гранд Рапидс',
	  IXR: 'Ранчи',
	  SJB: 'San Joaquin',
	  SWS: 'Суони',
	  JWN: 'Зенджан',
	  SRF: 'Сан-Рафаэль',
	  BTP: 'Butler',
	  EIE: 'Енисейск',
	  YFA: 'Форт Олбани',
	  LEA: 'Леармонс',
	  XXM: 'Мьольбю',
	  VQS: 'Вьекес',
	  MXV: 'Moron',
	  KSY: 'Карс',
	  YWS: 'Вистлер',
	  HGH: 'Ханчжоу',
	  WBB: 'Стеббинс',
	  RSK: 'Рансики',
	  QCC: 'Камасари',
	  RCE: 'Рош Харбор',
	  KAL: 'Kaltag',
	  DCN: '',
	  XIZ: '',
	  JPA: 'Жоао Пессоа',
	  MYX: 'Menyamya',
	  HHE: 'Хатинохэ',
	  ZTI: 'Humen',
	  LSM: 'Лонг-Семадо',
	  ZYY: 'Марнардал',
	  JHG: 'Цзиньхонг',
	  KLG: 'Калскаг',
	  WAH: 'Wahpeton',
	  QBA: 'Будва',
	  MCG: 'Mcgrath',
	  KGL: 'Кигали',
	  MTW: 'Манитовок',
	  XHV: 'Брасов',
	  VII: 'Винь',
	  GRX: 'Гранада',
	  ARE: 'Аресибо',
	  MGB: 'Маунт-Гамбьер',
	  ELC: 'Элко',
	  RES: 'Ресистенсия',
	  PNA: 'Памплона',
	  RUM: 'Rumjatar',
	  XST: 'Сент',
	  ALU: 'Алула',
	  RIA: 'Санта-Мария',
	  PDB: 'Педро-Бэй',
	  PBE: 'Пуэрто-Беррио',
	  MEV: 'Минден',
	  OTH: 'Норт Бенд',
	  VPZ: 'Вальпараисо',
	  PQC: 'Пхукуок',
	  FDY: 'Findlay',
	  MGD: 'Магдалена',
	  HPA: 'Хаапай',
	  YAB: '',
	  QRM: 'Нарромайн',
	  VGO: 'Виго',
	  QYG: '',
	  QWT: 'Талавера де ла Реина',
	  XGF: 'Percex',
	  DWD: '',
	  MHD: 'Масхад',
	  HUH: 'Хуахин',
	  NSK: 'Норильск',
	  MMZ: 'Меймене',
	  BUI: 'Bokondini',
	  PYX: 'Паттайа',
	  PML: 'Порт-Моллер',
	  GRO: 'Херона',
	  DSN: 'Донгшенг',
	  XEH: 'Ледисмит',
	  BRK: 'Бурке',
	  GOO: 'Гундивинди',
	  OZP: 'Морон',
	  ABY: 'Олбани',
	  YTD: 'Тикет Портедж',
	  MBH: 'Мэриборо',
	  FLL: 'Форт-Лодердейл',
	  WUZ: 'Вучжоу',
	  KVA: 'Кавалла',
	  PEU: 'Пуэрто-Лемпира',
	  EMN: 'Nema',
	  CJL: 'Читрал',
	  RKE: 'Роскильде',
	  BOA: 'Бома',
	  MMM: 'Мидлмаунт',
	  FLA: 'Флоренция',
	  KTE: 'Кертех',
	  YQR: 'Реджайна',
	  CFH: 'Клифтон-Хиллз',
	  FUN: 'Фунафути',
	  MLU: 'Монро',
	  RCB: 'Ричардс Бэй',
	  HRB: 'Харбин',
	  YSB: 'Садбери',
	  NEV: 'Невис',
	  SAP: 'Сан Педро Сула',
	  KMG: 'Куньмин',
	  AYS: 'Вэйкросс',
	  GYP: 'Гимпи',
	  MOQ: 'Морондава',
	  XWQ: 'Енкопинг',
	  UCA: 'Ютика',
	  KUA: 'Куантан',
	  EVW: 'Эванстон',
	  YFO: 'Флин-Флон',
	  PKK: 'Пакокку',
	  LKH: 'Лонг-Аках',
	  QDE: 'Катандува',
	  YOJ: 'Хай Левел',
	  SPF: 'Сперфиш',
	  GYL: 'Аргайл',
	  BNS: 'Венесуэла',
	  ASD: 'Эндрос-Таун',
	  RTP: 'Рутланд-Плейнз',
	  EED: 'Нидл',
	  CBQ: 'Калабар',
	  HNK: 'Хинчинбрук-Айленд',
	  YCG: 'Каслгар',
	  ACO: 'Аскона',
	  TSR: 'Тимисоара',
	  XQA: '',
	  OGL: '',
	  CFN: 'Донегаль',
	  BZD: 'Балранальд',
	  TUK: 'Турбат',
	  XHM: 'Джорджтаун',
	  HRT: 'Херрогейт',
	  SGO: 'Сент Джордж',
	  QVH: 'Vila Velha',
	  BOC: 'Бокас-Дель-Торо',
	  KNU: 'Канпур',
	  XWN: 'Уоррингтон',
	  QAO: 'Агридженто',
	  MOB: 'Мобайл',
	  ETB: 'Уэст Бенд',
	  MVV: 'Мегеве',
	  TOP: 'Топека',
	  ANJ: 'Занага',
	  FOC: 'Фучжоу',
	  AUJ: 'Амбунти',
	  KHH: 'Гаосюн',
	  BMB: 'Бумба',
	  DCM: 'Кастре',
	  CKZ: 'Канаккале',
	  MRZ: 'Мори',
	  DDI: 'Дэйдрим-Айленд'
	};

/***/ },
/* 32 */
/***/ function(module, exports) {

	'use strict';
	/**
	 *
	 * @param {string} part
	 * @param {Object.<string,string>} obj
	 * @return {Array.<*>}
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports["default"] = findKeys;
	function findKeys(part, obj) {
	  return Object.keys(obj).filter(function ( /*string*/item) {
	    return obj[item].toLowerCase().indexOf(part.toLowerCase()) > -1;
	  });
	}

/***/ },
/* 33 */
/***/ function(module, exports) {

	'use strict';
	/**
	 *
	 * @param {PlaceData} placeDataA
	 * @param {PlaceData} placeDataB
	 * @param {string} part
	 * @return {number}
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports["default"] = sortPlaceData;
	function sortPlaceData(placeDataA, placeDataB, part) {
	  var partLow = part.toLowerCase(),
	      typeA = placeDataA.type,
	      typeB = placeDataB.type;
	
	  var indexA = placeDataA[typeA].ru.indexOf(part),
	      indexALow = placeDataA[typeA].ru.toLowerCase().indexOf(partLow);
	
	  var indexB = placeDataB[typeB].ru.indexOf(part),
	      indexBLow = placeDataB[typeB].ru.toLowerCase().indexOf(partLow);
	
	  var index = 0;
	
	  if (indexA > -1 && indexB > -1) {
	    index = indexA - indexB;
	  } else if (indexA > -1) {
	    index = -1;
	  } else if (indexB > -1) {
	    index = 1;
	  } else {
	    index = indexALow - indexBLow;
	  }
	
	  if (index == 0) {
	
	    if (placeDataA[typeA].ru > placeDataB[typeB].ru) {
	      index = 1;
	    } else if (placeDataA[typeA].ru < placeDataB[typeB].ru) {
	      index = -1;
	    }
	  }
	
	  return index;
	}

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.byIATA = byIATA;
	exports.byPart = byPart;
	
	var _countries = __webpack_require__(35);
	
	var _countries2 = _interopRequireDefault(_countries);
	
	var _countries3 = __webpack_require__(36);
	
	var _countries4 = _interopRequireDefault(_countries3);
	
	var _findKeys = __webpack_require__(32);
	
	var _findKeys2 = _interopRequireDefault(_findKeys);
	
	var _sortPlaceData = __webpack_require__(33);
	
	var _sortPlaceData2 = _interopRequireDefault(_sortPlaceData);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	/**
	 *
	 * @param {string} iata
	 * @return {PlaceData}
	 */
	function byIATA(iata) {
	  return __getCountryEN(iata);
	}
	
	/**
	 *
	 * @param {string} iata
	 * @return {PlaceData|null}
	 * @private
	 */
	function __getCountryEN(iata) {
	  if (_countries2["default"].hasOwnProperty(iata)) {
	    //1|Czech Republic|49.82|15.47
	    var countryData = _countries2["default"][iata].split('|');
	    var countryRu = __getCountryRU(iata);
	
	    if (!countryRu) return null;
	
	    return {
	      iata: iata,
	      type: 'country',
	      country: {
	        iata: iata,
	        en: countryData[1],
	        ru: countryRu
	      }
	    };
	  }
	
	  return null;
	}
	/**
	 *
	 * @param {string} iata
	 * @return {string|null}
	 * @private
	 */
	function __getCountryRU(iata) {
	  if (_countries4["default"].hasOwnProperty(iata)) return _countries4["default"][iata];
	  return null;
	}
	
	/**
	 *
	 * @param part
	 * @return {Array.<*>}
	 */
	function byPart(part) {
	  return (0, _findKeys2["default"])(part, _countries4["default"]).map(function ( /*string*/iata) {
	    return byIATA(iata);
	  }).filter(function ( /*(PlaceData|null)*/countryData) {
	    return !!countryData;
	  }).sort(function ( /*PlaceData*/countryDataA, /*PlaceData*/countryDataB) {
	    return (0, _sortPlaceData2["default"])(countryDataA, countryDataB, part);
	  });
	}

/***/ },
/* 35 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	
	https://www.onetwotrip.com/ru/aviabilety/to-ak_AK/
	https://www.onetwotrip.com/ru/aviabilety/to-bonaire-saint-eustatius-and-saba_BQ/
	https://www.onetwotrip.com/ru/aviabilety/to-ss_SS/
	https://www.onetwotrip.com/ru/aviabilety/to-saint-barthelemy_BL/
	https://www.onetwotrip.com/ru/aviabilety/to-sint-maarten_SX/
	https://www.onetwotrip.com/ru/aviabilety/to-virgin-islands-us_VI/
	https://www.onetwotrip.com/ru/aviabilety/to-australia_AU/
	https://www.onetwotrip.com/ru/aviabilety/to-austria_AT/
	https://www.onetwotrip.com/ru/aviabilety/to-azerbaijan_AZ/
	https://www.onetwotrip.com/ru/aviabilety/to-aland-islands_AX/
	https://www.onetwotrip.com/ru/aviabilety/to-albania_AL/
	https://www.onetwotrip.com/ru/aviabilety/to-algeria_DZ/
	https://www.onetwotrip.com/ru/aviabilety/to-samoa-american_AS/
	https://www.onetwotrip.com/ru/aviabilety/to-anguilla_AI/
	https://www.onetwotrip.com/ru/aviabilety/to-angola_AO/
	https://www.onetwotrip.com/ru/aviabilety/to-andorra_AD/
	https://www.onetwotrip.com/ru/aviabilety/to-antigua-and-barbuda_AG/
	https://www.onetwotrip.com/ru/aviabilety/to-netherland-antilles_AN/
	https://www.onetwotrip.com/ru/aviabilety/to-argen_AR/
	https://www.onetwotrip.com/ru/aviabilety/to-armenia_AM/
	https://www.onetwotrip.com/ru/aviabilety/to-aruba_AW/
	https://www.onetwotrip.com/ru/aviabilety/to-afghanistan_AF/
	https://www.onetwotrip.com/ru/aviabilety/to-bahamas_BS/
	https://www.onetwotrip.com/ru/aviabilety/to-bangladesh_BD/
	https://www.onetwotrip.com/ru/aviabilety/to-barbados_BB/
	https://www.onetwotrip.com/ru/aviabilety/to-bahrain_BH/
	https://www.onetwotrip.com/ru/aviabilety/to-belarus_BY/
	https://www.onetwotrip.com/ru/aviabilety/to-belize_BZ/
	https://www.onetwotrip.com/ru/aviabilety/to-belgium_BE/
	https://www.onetwotrip.com/ru/aviabilety/to-benin_BJ/
	https://www.onetwotrip.com/ru/aviabilety/to-bermuda_BM/
	https://www.onetwotrip.com/ru/aviabilety/to-bulgaria_BG/
	https://www.onetwotrip.com/ru/aviabilety/to-bolivia_BO/
	https://www.onetwotrip.com/ru/aviabilety/to-bosnia-herzegovina_BA/
	https://www.onetwotrip.com/ru/aviabilety/to-botswana_BW/
	https://www.onetwotrip.com/ru/aviabilety/to-brazil_BR/
	https://www.onetwotrip.com/ru/aviabilety/to-british-indian-ocean-territory_IO/
	https://www.onetwotrip.com/ru/aviabilety/to-brunei_BN/
	https://www.onetwotrip.com/ru/aviabilety/to-burkina-faso_BF/
	https://www.onetwotrip.com/ru/aviabilety/to-burundi_BI/
	https://www.onetwotrip.com/ru/aviabilety/to-bhutan_BT/
	https://www.onetwotrip.com/ru/aviabilety/to-vanuatu_VU/
	https://www.onetwotrip.com/ru/aviabilety/to-vatican-city-state_VA/
	https://www.onetwotrip.com/ru/aviabilety/to-united-kingdom_GB/
	https://www.onetwotrip.com/ru/aviabilety/to-hungary_HU/
	https://www.onetwotrip.com/ru/aviabilety/to-venezuela_VE/
	https://www.onetwotrip.com/ru/aviabilety/to-british-virgin-islands_VG/
	https://www.onetwotrip.com/ru/aviabilety/to-timor-leste_TL/
	https://www.onetwotrip.com/ru/aviabilety/to-vietnam_VN/
	https://www.onetwotrip.com/ru/aviabilety/to-gabon_GA/
	https://www.onetwotrip.com/ru/aviabilety/to-haiti_HT/
	https://www.onetwotrip.com/ru/aviabilety/to-guyana_GY/
	https://www.onetwotrip.com/ru/aviabilety/to-gambia_GM/
	https://www.onetwotrip.com/ru/aviabilety/to-ghana_GH/
	https://www.onetwotrip.com/ru/aviabilety/to-guadeloupe_GP/
	https://www.onetwotrip.com/ru/aviabilety/to-guatemala_GT/
	https://www.onetwotrip.com/ru/aviabilety/to-french-guiana_GF/
	https://www.onetwotrip.com/ru/aviabilety/to-guinea_GN/
	https://www.onetwotrip.com/ru/aviabilety/to-guinea-bissau_GW/
	https://www.onetwotrip.com/ru/aviabilety/to-germany_DE/
	https://www.onetwotrip.com/ru/aviabilety/to-guernsey_GG/
	https://www.onetwotrip.com/ru/aviabilety/to-gibraltar_GI/
	https://www.onetwotrip.com/ru/aviabilety/to-honduras_HN/
	https://www.onetwotrip.com/ru/aviabilety/to-hong-kong_HK/
	https://www.onetwotrip.com/ru/aviabilety/to-grenada_GD/
	https://www.onetwotrip.com/ru/aviabilety/to-greenland_GL/
	https://www.onetwotrip.com/ru/aviabilety/to-greece_GR/
	https://www.onetwotrip.com/ru/aviabilety/to-georgia_GE/
	https://www.onetwotrip.com/ru/aviabilety/to-guam_GU/
	https://www.onetwotrip.com/ru/aviabilety/to-denmark_DK/
	https://www.onetwotrip.com/ru/aviabilety/to-djibouti_DJ/
	https://www.onetwotrip.com/ru/aviabilety/to-dominica_DM/
	https://www.onetwotrip.com/ru/aviabilety/to-dominican-republic_DO/
	https://www.onetwotrip.com/ru/aviabilety/to-egypt_EG/
	https://www.onetwotrip.com/ru/aviabilety/to-zambia_ZM/
	https://www.onetwotrip.com/ru/aviabilety/to-western-sahara_EH/
	https://www.onetwotrip.com/ru/aviabilety/to-zimbabwe_ZW/
	https://www.onetwotrip.com/ru/aviabilety/to-israel_IL/
	https://www.onetwotrip.com/ru/aviabilety/to-india_IN/
	https://www.onetwotrip.com/ru/aviabilety/to-indonesia_ID/
	https://www.onetwotrip.com/ru/aviabilety/to-jordan_JO/
	https://www.onetwotrip.com/ru/aviabilety/to-iraq_IQ/
	https://www.onetwotrip.com/ru/aviabilety/to-iran_IR/
	https://www.onetwotrip.com/ru/aviabilety/to-ireland-republic-of_IE/
	https://www.onetwotrip.com/ru/aviabilety/to-iceland_IS/
	https://www.onetwotrip.com/ru/aviabilety/to-spain_ES/
	https://www.onetwotrip.com/ru/aviabilety/to-italy_IT/
	https://www.onetwotrip.com/ru/aviabilety/to-yemen-republic-of_YE/
	https://www.onetwotrip.com/ru/aviabilety/to-cape-verde-republic-of_CV/
	https://www.onetwotrip.com/ru/aviabilety/to-kazakstan_KZ/
	https://www.onetwotrip.com/ru/aviabilety/to-cayman-islands_KY/
	https://www.onetwotrip.com/ru/aviabilety/to-cambodia_KH/
	https://www.onetwotrip.com/ru/aviabilety/to-cameroon-united-republic-of_CM/
	https://www.onetwotrip.com/ru/aviabilety/to-canada_CA/
	https://www.onetwotrip.com/ru/aviabilety/to-qatar_QA/
	https://www.onetwotrip.com/ru/aviabilety/to-kenya_KE/
	https://www.onetwotrip.com/ru/aviabilety/to-cyprus_CY/
	https://www.onetwotrip.com/ru/aviabilety/to-kyrgyzstan_KG/
	https://www.onetwotrip.com/ru/aviabilety/to-kiribati_KI/
	https://www.onetwotrip.com/ru/aviabilety/to-china_CN/
	https://www.onetwotrip.com/ru/aviabilety/to-cocos-islands_CC/
	https://www.onetwotrip.com/ru/aviabilety/to-colombia_CO/
	https://www.onetwotrip.com/ru/aviabilety/to-comoros_KM/
	https://www.onetwotrip.com/ru/aviabilety/to-congo-republic_CG/
	https://www.onetwotrip.com/ru/aviabilety/to-congo-democratic-republic-of_CD/
	https://www.onetwotrip.com/ru/aviabilety/to-korea-democratic-peoples-republic_KP/
	https://www.onetwotrip.com/ru/aviabilety/to-korea-republic-of_KR/
	https://www.onetwotrip.com/ru/aviabilety/to-costa-rica_CR/
	https://www.onetwotrip.com/ru/aviabilety/to-cote-divoire_CI/
	https://www.onetwotrip.com/ru/aviabilety/to-cuba_CU/
	https://www.onetwotrip.com/ru/aviabilety/to-kuwait_KW/
	https://www.onetwotrip.com/ru/aviabilety/to-curacao_CW/
	https://www.onetwotrip.com/ru/aviabilety/to-lao-peoples-dem-rep_LA/
	https://www.onetwotrip.com/ru/aviabilety/to-latvia_LV/
	https://www.onetwotrip.com/ru/aviabilety/to-lesotho_LS/
	https://www.onetwotrip.com/ru/aviabilety/to-liberia_LR/
	https://www.onetwotrip.com/ru/aviabilety/to-lebanon_LB/
	https://www.onetwotrip.com/ru/aviabilety/to-libyan-arab-jamahiriya_LY/
	https://www.onetwotrip.com/ru/aviabilety/to-lithuania_LT/
	https://www.onetwotrip.com/ru/aviabilety/to-liechtenstein_LI/
	https://www.onetwotrip.com/ru/aviabilety/to-luxembourg_LU/
	https://www.onetwotrip.com/ru/aviabilety/to-mauritius_MU/
	https://www.onetwotrip.com/ru/aviabilety/to-mauritania_MR/
	https://www.onetwotrip.com/ru/aviabilety/to-madagascar-malagasy_MG/
	https://www.onetwotrip.com/ru/aviabilety/to-mayotte_YT/
	https://www.onetwotrip.com/ru/aviabilety/to-macau_MO/
	https://www.onetwotrip.com/ru/aviabilety/to-macedonia_MK/
	https://www.onetwotrip.com/ru/aviabilety/to-malawi_MW/
	https://www.onetwotrip.com/ru/aviabilety/to-malaysia_MY/
	https://www.onetwotrip.com/ru/aviabilety/to-mali_ML/
	https://www.onetwotrip.com/ru/aviabilety/to-united-states-minor-outlying-islands_UM/
	https://www.onetwotrip.com/ru/aviabilety/to-maldives_MV/
	https://www.onetwotrip.com/ru/aviabilety/to-malta_MT/
	https://www.onetwotrip.com/ru/aviabilety/to-marianna-islands_MP/
	https://www.onetwotrip.com/ru/aviabilety/to-morocco_MA/
	https://www.onetwotrip.com/ru/aviabilety/to-martinique_MQ/
	https://www.onetwotrip.com/ru/aviabilety/to-marshall-islands_MH/
	https://www.onetwotrip.com/ru/aviabilety/to-mexico_MX/
	https://www.onetwotrip.com/ru/aviabilety/to-micronesia_FM/
	https://www.onetwotrip.com/ru/aviabilety/to-mozambique_MZ/
	https://www.onetwotrip.com/ru/aviabilety/to-moldova_MD/
	https://www.onetwotrip.com/ru/aviabilety/to-monaco_MC/
	https://www.onetwotrip.com/ru/aviabilety/to-mongolia_MN/
	https://www.onetwotrip.com/ru/aviabilety/to-montserrat_MS/
	https://www.onetwotrip.com/ru/aviabilety/to-myanmar_MM/
	https://www.onetwotrip.com/ru/aviabilety/to-namibia_NA/
	https://www.onetwotrip.com/ru/aviabilety/to-nauru_NR/
	https://www.onetwotrip.com/ru/aviabilety/to-nepal_NP/
	https://www.onetwotrip.com/ru/aviabilety/to-niger_NE/
	https://www.onetwotrip.com/ru/aviabilety/to-nigeria_NG/
	https://www.onetwotrip.com/ru/aviabilety/to-netherlands_NL/
	https://www.onetwotrip.com/ru/aviabilety/to-nicaragua_NI/
	https://www.onetwotrip.com/ru/aviabilety/to-niue_NU/
	https://www.onetwotrip.com/ru/aviabilety/to-new-zealand_NZ/
	https://www.onetwotrip.com/ru/aviabilety/to-new-caledonia_NC/
	https://www.onetwotrip.com/ru/aviabilety/to-norway_NO/
	https://www.onetwotrip.com/ru/aviabilety/to-united-arab-emirates_AE/
	https://www.onetwotrip.com/ru/aviabilety/to-oman-sultanate-of_OM/
	https://www.onetwotrip.com/ru/aviabilety/to-bouvet-island_BV/
	https://www.onetwotrip.com/ru/aviabilety/to-isle-of-man_IM/
	https://www.onetwotrip.com/ru/aviabilety/to-norfolk-island_NF/
	https://www.onetwotrip.com/ru/aviabilety/to-christmas-island_CX/
	https://www.onetwotrip.com/ru/aviabilety/to-ascension-islandst-helena_SH/
	https://www.onetwotrip.com/ru/aviabilety/to-jersey-island_JE/
	https://www.onetwotrip.com/ru/aviabilety/to-cook-islands_CK/
	https://www.onetwotrip.com/ru/aviabilety/to-turks-and-caicos-islands_TC/
	https://www.onetwotrip.com/ru/aviabilety/to-wallis-and-futuna-islands_WF/
	https://www.onetwotrip.com/ru/aviabilety/to-heard-island-and-mcdonald-islands_HM/
	https://www.onetwotrip.com/ru/aviabilety/to-svalbard-and-jan-mayen-is_SJ/
	https://www.onetwotrip.com/ru/aviabilety/to-pakistan_PK/
	https://www.onetwotrip.com/ru/aviabilety/to-palau_PW/
	https://www.onetwotrip.com/ru/aviabilety/to-occupied-palestinian-territory_PS/
	https://www.onetwotrip.com/ru/aviabilety/to-panama_PA/
	https://www.onetwotrip.com/ru/aviabilety/to-papua-new-guinea-niugini_PG/
	https://www.onetwotrip.com/ru/aviabilety/to-paraguay_PY/
	https://www.onetwotrip.com/ru/aviabilety/to-peru_PE/
	https://www.onetwotrip.com/ru/aviabilety/to-pitcairn_PN/
	https://www.onetwotrip.com/ru/aviabilety/to-french-polynesia_PF/
	https://www.onetwotrip.com/ru/aviabilety/to-poland_PL/
	https://www.onetwotrip.com/ru/aviabilety/to-portugal_PT/
	https://www.onetwotrip.com/ru/aviabilety/to-puerto-rico_PR/
	https://www.onetwotrip.com/ru/aviabilety/to-reunion_RE/
	https://www.onetwotrip.com/ru/aviabilety/to-russian-federation_RU/
	https://www.onetwotrip.com/ru/aviabilety/to-rwanda_RW/
	https://www.onetwotrip.com/ru/aviabilety/to-romania_RO/
	https://www.onetwotrip.com/ru/aviabilety/to-united-states_US/
	https://www.onetwotrip.com/ru/aviabilety/to-samoa-independent-state-of_WS/
	https://www.onetwotrip.com/ru/aviabilety/to-san-marino_SM/
	https://www.onetwotrip.com/ru/aviabilety/to-sao-tome-and-principe_ST/
	https://www.onetwotrip.com/ru/aviabilety/to-saint-martin_MF/
	https://www.onetwotrip.com/ru/aviabilety/to-saudi-arabia_SA/
	https://www.onetwotrip.com/ru/aviabilety/to-swaziland_SZ/
	https://www.onetwotrip.com/ru/aviabilety/to-seychelles-islands_SC/
	https://www.onetwotrip.com/ru/aviabilety/to-senegal_SN/
	https://www.onetwotrip.com/ru/aviabilety/to-saint-vincent-and-the-grenadines_VC/
	https://www.onetwotrip.com/ru/aviabilety/to-st-kitts---nevis_KN/
	https://www.onetwotrip.com/ru/aviabilety/to-saint-lucia_LC/
	https://www.onetwotrip.com/ru/aviabilety/to-st-pierre-and-miquelon_PM/
	https://www.onetwotrip.com/ru/aviabilety/to-serbia_RS/
	https://www.onetwotrip.com/ru/aviabilety/to-singapore_SG/
	https://www.onetwotrip.com/ru/aviabilety/to-syrian-arab-rep_SY/
	https://www.onetwotrip.com/ru/aviabilety/to-slovakia_SK/
	https://www.onetwotrip.com/ru/aviabilety/to-slovenia_SI/
	https://www.onetwotrip.com/ru/aviabilety/to-solomon-islands_SB/
	https://www.onetwotrip.com/ru/aviabilety/to-somalia_SO/
	https://www.onetwotrip.com/ru/aviabilety/to-sudan_SD/
	https://www.onetwotrip.com/ru/aviabilety/to-suriname_SR/
	https://www.onetwotrip.com/ru/aviabilety/to-sierra-leone_SL/
	https://www.onetwotrip.com/ru/aviabilety/to-tajikistan_TJ/
	https://www.onetwotrip.com/ru/aviabilety/to-thailand_TH/
	https://www.onetwotrip.com/ru/aviabilety/to-taiwan-republic-of-china_TW/
	https://www.onetwotrip.com/ru/aviabilety/to-tanzania_TZ/
	https://www.onetwotrip.com/ru/aviabilety/to-togo_TG/
	https://www.onetwotrip.com/ru/aviabilety/to-tokelau_TK/
	https://www.onetwotrip.com/ru/aviabilety/to-tonga_TO/
	https://www.onetwotrip.com/ru/aviabilety/to-trinidad-and-tobago_TT/
	https://www.onetwotrip.com/ru/aviabilety/to-tuvalu_TV/
	https://www.onetwotrip.com/ru/aviabilety/to-tunisia_TN/
	https://www.onetwotrip.com/ru/aviabilety/to-turkmenistan_TM/
	https://www.onetwotrip.com/ru/aviabilety/to-turkey_TR/
	https://www.onetwotrip.com/ru/aviabilety/to-uganda_UG/
	https://www.onetwotrip.com/ru/aviabilety/to-uzbekistan-sum_UZ/
	https://www.onetwotrip.com/ru/aviabilety/to-ukraine_UA/
	https://www.onetwotrip.com/ru/aviabilety/to-uruguay_UY/
	https://www.onetwotrip.com/ru/aviabilety/to-faeroe-islands_FO/
	https://www.onetwotrip.com/ru/aviabilety/to-fiji-islands_FJ/
	https://www.onetwotrip.com/ru/aviabilety/to-philippines_PH/
	https://www.onetwotrip.com/ru/aviabilety/to-finland_FI/
	https://www.onetwotrip.com/ru/aviabilety/to-falkland-islands_FK/
	https://www.onetwotrip.com/ru/aviabilety/to-france_FR/
	https://www.onetwotrip.com/ru/aviabilety/to-french-southern-territories_TF/
	https://www.onetwotrip.com/ru/aviabilety/to-croatia_HR/
	https://www.onetwotrip.com/ru/aviabilety/to-central-african-republic_CF/
	https://www.onetwotrip.com/ru/aviabilety/to-chad_TD/
	https://www.onetwotrip.com/ru/aviabilety/to-montenegro_ME/
	https://www.onetwotrip.com/ru/aviabilety/to-czech-republic_CZ/
	https://www.onetwotrip.com/ru/aviabilety/to-chile_CL/
	https://www.onetwotrip.com/ru/aviabilety/to-switzerland_CH/
	https://www.onetwotrip.com/ru/aviabilety/to-sweden_SE/
	https://www.onetwotrip.com/ru/aviabilety/to-sri-lanka_LK/
	https://www.onetwotrip.com/ru/aviabilety/to-ecuador_EC/
	https://www.onetwotrip.com/ru/aviabilety/to-equatorial-guinea_GQ/
	https://www.onetwotrip.com/ru/aviabilety/to-el-salvador_SV/
	https://www.onetwotrip.com/ru/aviabilety/to-eritrea_ER/
	https://www.onetwotrip.com/ru/aviabilety/to-estonia_EE/
	https://www.onetwotrip.com/ru/aviabilety/to-ethiopia_ET/
	https://www.onetwotrip.com/ru/aviabilety/to-south-africa-republic_ZA/
	https://www.onetwotrip.com/ru/aviabilety/to-south-georgia-and-s-sandwich-island_GS/
	https://www.onetwotrip.com/ru/aviabilety/to-jamaica_JM/
	https://www.onetwotrip.com/ru/aviabilety/to-japan_JP/
	
	*/
	
	exports["default"] = {
	  LA: '|Lao, People\'s Dem. Rep.',
	  GQ: '|Equatorial Guinea',
	  TH: '1|Thailand|15.87|100.99',
	  BG: '1|Bulgaria|42.73|25.49',
	  AO: '1|Angola',
	  GW: '|Guinea Bissau',
	  MY: '1|Malaysia',
	  'DO': '1|Dominican Republic',
	  LB: '1|Lebanon',
	  PW: '|Palau',
	  ER: '|Eritrea',
	  SK: '1|Slovakia',
	  KP: '1|Korea, Democratic Peoples Republic',
	  LY: '1|Libyan Arab Jamahiriya',
	  CA: '1|Canada|56.13|-106.35',
	  ET: '|Ethiopia',
	  NE: '|Niger',
	  AW: '|Aruba',
	  AF: '1|Afghanistan',
	  AT: '1|Austria|47.52|14.55',
	  TC: '|Turks And Caicos Islands',
	  'IN': '1|India|20.59|78.96',
	  VU: '|Vanuatu',
	  CW: '|Curacao',
	  GM: '1|Gambia',
	  AG: '|Antigua And Barbuda',
	  HK: '1|Hong Kong',
	  LU: '1|Luxembourg',
	  ST: '|Sao Tome and Principe',
	  NR: '|Nauru',
	  MW: '|Malawi',
	  TR: '1|Turkey|38.96|35.24',
	  NO: '1|Norway|60.47|8.47',
	  BS: '|Bahamas',
	  RO: '1|Romania',
	  DZ: '1|Algeria',
	  GT: '1|Guatemala',
	  BI: '|Burundi',
	  CY: '1|Cyprus|35.13|33.43',
	  GI: '|Gibraltar',
	  LK: '1|Sri Lanka',
	  ES: '1|Spain|40.46|-3.75',
	  AM: '1|Armenia|40.07|45.04',
	  ID: '1|Indonesia|-0.79|113.92',
	  SL: '1|Sierra Leone',
	  AU: '1|Australia|-25.27|133.78',
	  SG: '1|Singapore',
	  EE: '1|Estonia',
	  MX: '1|Mexico|23.63|-102.55',
	  IR: '1|Iran',
	  BB: '|Barbados',
	  GU: '|Guam',
	  NL: '1|Netherlands',
	  BT: '|Bhutan',
	  PF: '|French Polynesia',
	  AK: '||55.08|38.80',
	  KZ: '1|Kazakstan|48.02|66.92',
	  WS: '|Samoa, Independent State Of',
	  IL: '1|Israel|31.05|34.85',
	  EG: '1|Egypt|26.82|30.80',
	  NG: '1|Nigeria',
	  FJ: '|Fiji Islands',
	  BA: '1|Bosnia Herzegovina',
	  KR: '1|Korea, Republic Of',
	  BJ: '|Benin',
	  SI: '1|Slovenia',
	  YE: '1|Yemen, Republic Of',
	  TT: '|Trinidad and Tobago',
	  KN: '|St. Kitts - Nevis',
	  HU: '1|Hungary',
	  CM: '1|Cameroon, United Republic Of',
	  CH: '1|Switzerland|46.82|8.23',
	  BO: '1|Bolivia',
	  TO: '|Tonga',
	  BQ: '|Bonaire, Saint Eustatius and Saba',
	  MD: '1|Moldova|47.41|28.37',
	  GH: '1|Ghana',
	  'IT': '1|Italy|41.87|12.57',
	  CI: '1|Cote d\'Ivoire',
	  GY: '1|Guyana',
	  AB: '|Abkhazia',
	  JO: '1|Jordan',
	  EC: '1|Ecuador',
	  SX: '|Sint Maarten',
	  HN: '1|Honduras',
	  CZ: '1|Czech Republic|49.82|15.47',
	  LT: '1|Lithuania|55.17|23.88',
	  CK: '|Cook Islands',
	  LV: '1|Latvia',
	  MR: '|Mauritania',
	  PT: '1|Portugal|39.40|-8.22',
	  KH: '1|Cambodia',
	  PK: '1|Pakistan',
	  GB: '1|United Kingdom|55.38|-3.44',
	  AI: '|Anguilla',
	  SC: '|Seychelles Islands',
	  QA: '1|Qatar',
	  KI: '|Kiribati',
	  SB: '|Solomon Islands',
	  JP: '1|Japan|36.20|138.25',
	  SV: '1|El Salvador',
	  SA: '1|Saudi Arabia',
	  MT: '1|Malta',
	  KG: '1|Kyrgyzstan',
	  PH: '1|Philippines',
	  HR: '1|Croatia|45.10|15.20',
	  CL: '1|Chile',
	  TN: '1|Tunisia',
	  SY: '1|Syrian Arab Rep.|34.80|39.00',
	  MS: '|Montserrat',
	  ZW: '1|Zimbabwe',
	  GN: '|Guinea',
	  MV: '|Maldives',
	  GF: '|French Guiana',
	  JM: '1|Jamaica|18.11|-77.30',
	  AE: '1|United Arab Emirates|24.24|54.88',
	  BY: '1|Belarus',
	  TL: '|Timor Leste',
	  DM: '|Dominica',
	  BN: '|Brunei',
	  PM: '|St. Pierre and Miquelon',
	  NZ: '1|New Zealand|-40.90|174.89',
	  CN: '1|China|35.86|104.20',
	  AR: '1|Argen',
	  CV: '|Cape Verde, Republic Of',
	  SO: '|Somalia',
	  VE: '1|Venezuela',
	  TD: '1|Chad',
	  PY: '1|Paraguay',
	  LC: '|Saint Lucia',
	  NI: '1|Nicaragua',
	  VN: '1|Vietnam|14.06|108.28',
	  ZA: '1|South Africa Republic|-30.79|22.94',
	  BZ: '|Belize',
	  VI: '|Virgin Islands U.S.',
	  KE: '1|Kenya',
	  VG: '|British Virgin Islands',
	  BR: '1|Brazil|-14.24|-51.93',
	  BF: '|Burkina Faso',
	  NP: '1|Nepal',
	  MQ: '|Martinique',
	  TM: '1|Turkmenistan',
	  FR: '1|France|46.23|2.21',
	  GP: '|Guadeloupe',
	  BD: '1|Bangladesh',
	  UY: '1|Uruguay',
	  CD: '|Congo Democratic Republic Of',
	  MP: '|Marianna Islands',
	  FK: '|Falkland Islands',
	  PL: '1|Poland',
	  CR: '1|Costa Rica',
	  RU: '1|Russian Federation|55.08|38.80',
	  TG: '1|Togo',
	  TW: '1|Taiwan, Republic of China',
	  KM: '|Comoros',
	  PA: '1|Panama',
	  GE: '1|Georgia|42.32|43.36',
	  MF: '|Saint Martin',
	  CF: '|Central African Republic',
	  CC: '|Cocos Islands',
	  ML: '1|Mali',
	  MO: '1|Macau',
	  SR: '|Suriname',
	  GL: '|Greenland',
	  GA: '|Gabon',
	  UZ: '1|Uzbekistan|41.38|64.59',
	  IQ: '1|Iraq',
	  SH: '|Ascension Island/St. Helena',
	  TJ: '1|Tajikistan|38.86|71.28',
	  KW: '1|Kuwait',
	  RW: '1|Rwanda',
	  SE: '1|Sweden',
	  HT: '1|Haiti',
	  MA: '1|Morocco|31.79|-7.09',
	  DJ: '|Djibouti',
	  GR: '1|Greece|39.07|21.82',
	  PR: '1|Puerto Rico',
	  MM: '|Myanmar',
	  CU: '1|Cuba|21.52|-77.78',
	  CX: '|Christmas Island',
	  RE: '|Reunion',
	  SN: '1|Senegal',
	  MN: '1|Mongolia',
	  TZ: '1|Tanzania',
	  WF: '|Wallis and Futuna Islands',
	  BL: '|Saint Barthelemy',
	  AL: '1|Albania',
	  TV: '|Tuvalu',
	  VC: '1|Saint Vincent And The Grenadines',
	  NU: '|Niue',
	  FI: '1|Finland',
	  CG: '1|Congo Republic',
	  SZ: '|Swaziland',
	  ZM: '1|Zambia',
	  YT: '|Mayotte',
	  PG: '|Papua New Guinea (Niugini)',
	  'AS': '|Samoa, American',
	  RS: '1|Serbia',
	  BE: '1|Belgium',
	  GD: '|Grenada',
	  NF: '|Norfolk Island',
	  MZ: '1|Mozambique',
	  MC: '1|Monaco',
	  UG: '1|Uganda',
	  DE: '1|Germany|51.17|10.45',
	  MH: '|Marshall Islands',
	  IE: '1|Ireland, Republic Of',
	  BM: '|Bermuda',
	  UA: '1|Ukraine|48.38|31.17',
	  MU: '1|Mauritius',
	  CO: '1|Colombia|4.57|-74.30',
	  FO: '|Faeroe Islands',
	  KY: '|Cayman Islands',
	  DK: '1|Denmark|56.26|9.50',
	  LS: '|Lesotho',
	  NA: '1|Namibia',
	  FM: '|Micronesia',
	  NC: '|New Caledonia',
	  SD: '|Sudan',
	  ME: '1|Montenegro|48.02|66.92',
	  BH: '1|Bahrain',
	  AZ: '1|Azerbaijan|40.14|47.58',
	  MG: '1|Madagascar (Malagasy)',
	  BW: '1|Botswana',
	  PE: '1|Peru',
	  'IS': '1|Iceland',
	  OM: '1|Oman, Sultanate Of',
	  MK: '1|Macedonia',
	  US: '1|United States|37.09|-95.71',
	  LR: '|Liberia',
	  PS: '1|Occupied Palestinian Territory'
	};

/***/ },
/* 36 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports["default"] = {
	  LA: 'Лаос',
	  GQ: 'Экваториальная Гвинея',
	  TH: 'Таиланд',
	  BG: 'Болгария',
	  AO: 'Ангола',
	  GW: 'Гвинея-Биссау',
	  MY: 'Малайзия',
	  DO: 'Доминиканская Республика',
	  LB: 'Ливан',
	  PW: 'Палау',
	  ER: 'Эритрея',
	  SK: 'Словакия',
	  KP: 'Корея (Северная), КНДР',
	  LY: 'Ливия',
	  CA: 'Канада',
	  ET: 'Эфиопия',
	  NE: 'Нигер',
	  AW: 'Аруба',
	  AF: 'Афганистан',
	  AT: 'Австрия',
	  TF: 'Французские Южные территории',
	  TC: 'Острова Теркс и Кайкос',
	  IN: 'Индия',
	  VU: 'Вануату',
	  SJ: 'Острова Шпицберген и Ян-Майен',
	  CW: 'Куракао',
	  GM: 'Гамбия',
	  AG: 'Антигуа и Барбуда',
	  HK: 'Гонконг',
	  LU: 'Люксембург',
	  ST: 'Сан-Томе и Принсипи',
	  NR: 'Науру',
	  MW: 'Малави',
	  TR: 'Турция',
	  NO: 'Норвегия',
	  BS: 'Багамы',
	  GG: 'Гернси',
	  RO: 'Румыния',
	  TK: 'Токелау',
	  PN: 'Питкерн',
	  DZ: 'Алжир',
	  GT: 'Гватемала',
	  'Гана': 'Республика Гана',
	  BI: 'Бурунди',
	  CY: 'Кипр',
	  GI: 'Гибралтар',
	  LK: 'Шри-Ланка',
	  ES: 'Испания',
	  AM: 'Армения',
	  ID: 'Индонезия',
	  SL: 'Сьерра-Леоне',
	  AU: 'Австралия',
	  SG: 'Сингапур',
	  EE: 'Эстония',
	  GS: 'Южная Джорджия и Южные Сэндвичевы острова',
	  VA: 'Ватикан',
	  MX: 'Мексика',
	  IR: 'Иран',
	  BB: 'Барбадос',
	  GU: 'Гуам',
	  NL: 'Нидерланды',
	  BT: 'Бутан',
	  PF: 'Полинезия',
	  AK: '',
	  KZ: 'Казахстан',
	  WS: 'Самоа',
	  IL: 'Израиль',
	  EG: 'Египет',
	  NG: 'Нигерия',
	  FJ: 'Фиджи',
	  JE: 'Острова Джерси',
	  BA: 'Босния и Герцеговина',
	  KR: 'Корея (Южная)',
	  EH: 'Зап Сахара',
	  BJ: 'Бенин',
	  AX: 'Аландские острова',
	  SI: 'Словения',
	  YE: 'Йемен',
	  TT: 'Тринидад и Тобаго',
	  KN: 'Сент-Китс и Невис',
	  HU: 'Венгрия',
	  CM: 'Камерун',
	  CH: 'Швейцария',
	  BO: 'Боливия',
	  TO: 'Тонга',
	  BQ: '',
	  MD: 'Молдавия',
	  GH: 'Гана',
	  IO: 'Британские Территории в Индийском Океане',
	  IT: 'Италия',
	  CI: 'Кот-д\'Ивуар',
	  GY: 'Гайана',
	  AB: 'Абхазия',
	  HM: 'Острова Херда и МакДоналда',
	  JO: 'Иордания',
	  EC: 'Эквадор',
	  SX: '',
	  HN: 'Гондурас',
	  CZ: 'Чехия',
	  LT: 'Литва',
	  CK: 'Острова Кука',
	  LV: 'Латвия',
	  MR: 'Мавритания',
	  PT: 'Португалия',
	  BV: 'Остров Буве',
	  KH: 'Камбоджа',
	  PK: 'Пакистан',
	  GB: 'Великобритания',
	  AI: 'Ангилья',
	  SC: 'Сейшельские острова',
	  QA: 'Катар',
	  KI: 'Кирибати',
	  zw: 'Зимбабве',
	  SB: 'Соломоновы острова',
	  JP: 'Япония',
	  SV: 'Эль-Сальвадор',
	  SA: 'Саудовская Аравия',
	  MT: 'Мальта',
	  KG: 'Киргизия',
	  PH: 'Филиппины',
	  HR: 'Хорватия',
	  CL: 'Чили',
	  TN: 'Тунис',
	  SY: 'Сирия',
	  MS: 'Монтсеррат',
	  ZW: 'Зимбабве',
	  GN: 'Гвинея',
	  MV: 'Мальдивы',
	  GF: 'Гвиана',
	  JM: 'Ямайка',
	  AE: 'ОАЭ',
	  BY: 'Беларусь',
	  TL: 'Восточный Тимор',
	  DM: 'Доминика',
	  BN: 'Бруней',
	  SM: 'Сан-Марино',
	  PM: 'Сент-Пьер и Микелон',
	  NZ: 'Новая Зеландия',
	  CN: 'Китай',
	  AR: 'Аргентина',
	  CV: 'Кабо-Верде',
	  SO: 'Сомали',
	  VE: 'Венесуэла',
	  TD: 'Чад',
	  PY: 'Парагвай',
	  LC: 'Сент-Люсия',
	  NI: 'Никарагуа',
	  VN: 'Вьетнам',
	  ZA: 'ЮАР',
	  BZ: 'Белиз',
	  VI: '',
	  AD: 'Андорра',
	  KE: 'Кения',
	  VG: 'Виргинские острова',
	  BR: 'Бразилия',
	  BF: 'Буркина-Фасо',
	  NP: 'Непал',
	  MQ: 'Мартиника',
	  TM: 'Туркменистан',
	  FR: 'Франция',
	  GP: 'Гваделупа',
	  BD: 'Бангладеш',
	  UY: 'Уругвай',
	  CD: 'Конго, Демократическая Республика',
	  MP: 'Марианские острова',
	  FK: 'Фолклендские острова',
	  PL: 'Польша',
	  AN: 'Антильские острова',
	  CR: 'Коста-Рика',
	  RU: 'Россия',
	  TG: 'Того',
	  TW: 'Тайвань',
	  KM: 'Коморские острова',
	  PA: 'Панама',
	  GE: 'Грузия',
	  MF: 'Санкт Мартин',
	  CF: 'Центральноафриканская Республика',
	  CC: 'Кокосовые острова',
	  ML: 'Мали',
	  MO: 'Макао',
	  SR: 'Суринам',
	  GL: 'Гренландия',
	  GA: 'Габон',
	  UZ: 'Узбекистан',
	  IQ: 'Ирак',
	  SH: 'Остров Святой Елены',
	  TJ: 'Таджикистан',
	  KW: 'Кувейт',
	  RW: 'Руанда',
	  UM: 'Малые острова США',
	  SE: 'Швеция',
	  HT: 'Гаити',
	  MA: 'Марокко',
	  DJ: 'Джибути',
	  GR: 'Греция',
	  PR: 'Пуэрто-Рико',
	  MM: 'Мьянма',
	  CU: 'Куба',
	  CX: 'Остров Рождества',
	  RE: 'Реюньон',
	  SN: 'Сенегал',
	  MN: 'Монголия',
	  TZ: 'Танзания',
	  WF: 'Острова Уоллис и Футуна',
	  BL: '',
	  AL: 'Албания',
	  TV: 'Тувалу',
	  VC: 'Сент-Винсент и Гренадины',
	  NU: 'Ниуэ',
	  FI: 'Финляндия',
	  CG: 'Конго Республика',
	  SZ: 'Свазиленд',
	  ZM: 'Замбия',
	  YT: 'Майотт',
	  PG: 'Папуа-Новая Гвинея',
	  AS: 'Американское Самоа',
	  LI: 'Лихтенштейн',
	  RS: 'Сербия',
	  BE: 'Бельгия',
	  SS: '',
	  GD: 'Гренада',
	  NF: 'Остров Норфолк',
	  MZ: 'Мозамбик',
	  MC: 'Монако',
	  UG: 'Уганда',
	  DE: 'Германия',
	  MH: 'Маршалловы острова',
	  IE: 'Ирландия',
	  BM: 'Бермуды',
	  UA: 'Украина',
	  MU: 'Маврикий',
	  CO: 'Колумбия',
	  FO: 'Фарерские острова',
	  KY: 'Каймановы острова',
	  DK: 'Дания',
	  LS: 'Лесото',
	  NA: 'Намибия',
	  FM: 'Микронезия',
	  IM: 'Остров Мэн',
	  NC: 'Новая Каледония',
	  SD: 'Судан',
	  ME: 'Черногория',
	  BH: 'Бахрейн',
	  AZ: 'Азербайджан',
	  MG: 'Мадагаскар',
	  BW: 'Ботсвана',
	  PE: 'Перу',
	  IS: 'Исландия',
	  OM: 'Оман',
	  MK: 'Македония',
	  US: 'США',
	  LR: 'Либерия',
	  PS: 'Палестина'
	};

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.byIATA = byIATA;
	
	var _airlines = __webpack_require__(38);
	
	var _airlines2 = _interopRequireDefault(_airlines);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	/**
	 *
	 * @param {string} iata
	 * @return {string}
	 */
	function byIATA(iata) {
	  if (_airlines2["default"][iata]) return _airlines2["default"][iata].split('|')[0];
	  return '';
	}

/***/ },
/* 38 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports["default"] = {
	  '6I': 'Air Alsie|0',
	  '7T': 'Aero Express del Ecuador - Trans AM|0',
	  QC: 'Camair-Co|0',
	  '5D': 'AeromÃ©xico Connect|0',
	  JI: 'Meraj Air|0',
	  '1N': 'Navitaire|0',
	  JS: 'Air Koryo|0',
	  YV: 'Mesa Airlines|0',
	  WB: 'RwandAir|0',
	  '9E': 'Endeavor Air|0',
	  KW: 'Kharkiv Airlines|0',
	  UT: 'UTair|1',
	  I9: 'Air Italy|0',
	  X5: 'Ten Airways|0',
	  R5: 'Jordan Aviation|0',
	  OF: 'Air Finland|0',
	  KF: 'Blue1|1',
	  SE: 'XL Airways France|0',
	  FT: 'FlyEgypt|0',
	  V1: 'IBS Software Services Americas|0',
	  X8: 'Airmax S.A.|0',
	  '2H': 'Thalys International|0',
	  T6: '1Time|0',
	  CX: 'Cathay Pacific|1',
	  S8: 'Skywise Airlines|0',
	  '6M': 'EuroAir|0',
	  PD: 'Porter Airlines|0',
	  K1: 'Topas|0',
	  MN: 'Kulula.com|0',
	  TZ: 'Scoot|0',
	  TV: 'Tibet Airlines|0',
	  VC: 'Charter Air Transport.|0',
	  FI: 'Icelandair|1',
	  '8C': 'Air Transport International|0',
	  B6: 'JetBlue Airways|0',
	  '1R': 'Bird Information Systems Private Li|0',
	  ZM: 'Pegasus Asia|0',
	  H1: 'Hahn Air Systems|0',
	  RR: 'Royal Air Force|0',
	  HV: 'Transavia|0',
	  KQ: 'Kenya Airways|0',
	  BE: 'Flybe|0',
	  '8M': 'Myanmar Airways International|0',
	  BGT: 'Bergen Air|0',
	  'Ð®Ð’': 'UVT aero|1',
	  VIV: 'Vivaaerobus|0',
	  J6: 'Larry\'s Flying Service|0',
	  YB: 'Borajet|0',
	  '5K': 'Hi Fly Transportes Aereos S.A.|0',
	  W6: 'Wizz Air|0',
	  IZ: 'Arkia Israeli Airlines|1',
	  TE: 'Skytaxi|0',
	  VM: 'Viaggio Air|0',
	  RE2: 'Aer Arann|0',
	  NC: 'Northern Air Cargo|0',
	  TOM: 'Thomson fly|0',
	  '7D': 'Donbassaero Airlines|0',
	  '4Q': 'Safi Airways|0',
	  VJ: 'VietJetAir|0',
	  KFG: 'Kullaflyg|0',
	  '3U': 'Sichuan Airlines|0',
	  VY: 'Vueling|1',
	  XS: 'SITA - Airlines Worldwide|0',
	  L3: 'DHL de Guatemala S.A.|0',
	  FE: 'Far Eastern Air Transport|0',
	  GQ: 'Sky Express (Greece)|0',
	  HA: 'Hawaiian Airlines|0',
	  NN: 'VIM Airlines|1',
	  RG: 'Rotana Jet Aviation|0',
	  VP: 'FlyMe|0',
	  AQ: '9 Air|0',
	  SQ: 'Singapore Airlines|1',
	  AO: 'Air One Aviation Pvt|0',
	  C0: 'Centralwings|0',
	  W7: 'Wings of Lebanon S.a.l|0',
	  OI: 'Hinterland Aviation Pty|0',
	  V4: 'Vieques Air Link Inc|0',
	  LY: 'El Al|1',
	  QM: 'Air Malawi Limited|0',
	  J8: 'Berjaya Air|0',
	  E8: 'City Airways Company Limited|0',
	  '4R': 'Royal Flight|0',
	  WT: 'Wasaya Airways|0',
	  ZH: 'Shenzhen Airlines|0',
	  AC: 'Air Canada|0',
	  XR: 'Virgin Australia Regional|0',
	  U2: 'EasyJet|0',
	  '2Q': 'AviTrans|0',
	  AA: 'American Airlines|1',
	  I5: 'AirAsia India|0',
	  R6: 'DOT - Danu oro transportas|0',
	  VQ: 'Novoair Limited|0',
	  KNE: 'Nas Air|0',
	  XO: 'South East Asian Airlines (SEAIR)|0',
	  RO: 'Tarom|0',
	  '1G': 'Travelport Global Distribution System|0',
	  '5H': 'Fly540|0',
	  '6T': 'Air Mandalay|0',
	  '6B': 'TUIfly Nordic|0',
	  GI: 'Itek Air|0',
	  UB: 'Myanmar National Airlines|0',
	  ES: 'DHL International|0',
	  '4G': 'Gazpromavia|0',
	  '4L': 'Georgian Int. Airlines|0',
	  AU: 'Austral|0',
	  C5: 'Champlain Enterprises|0',
	  EE: 'Eastern SkyJets|0',
	  '3Q': 'Carib Aviation|0',
	  '8J': 'Linea Aerea Eco Jet S.A.|0',
	  FP: 'TAG Aviation|0',
	  ZY: 'Ada Air|0',
	  OE: 'Cairo Aviation|0',
	  Z2: 'AirAsia Zest|0',
	  V3: 'Carpatair|0',
	  H4: 'Aero4M d.o.o|0',
	  G4: 'Allegiant Air|0',
	  AJ: 'Aztec Worldwide Airlines|0',
	  GU: 'Aviateca|0',
	  OZ: 'Asiana Airlines|1',
	  M8: 'Trans Maldivian Airways|0',
	  VH: 'Aeropostal Alas de Venezuela C.A.|0',
	  PF: 'Primera Air|0',
	  KZ: 'Nippon Cargo Airlines|0',
	  '6U': 'Unique Air|0',
	  '6C': 'Air Timor S.A.|0',
	  HF: 'Air Cote d\'Ivoire|0',
	  '241': '|0',
	  UO: 'Hong Kong Express|1',
	  E9: 'Evelop Airlines|0',
	  M0: 'Aero Mongolia|0',
	  '5A': 'Alpine Air Express|0',
	  YR: 'Grand Canyon Airlines|0',
	  C7: 'Saffron Aviation (Pvt)|0',
	  HO: 'Juneyao Airlines|0',
	  M7: 'Aerotransportes Mas de Carga|0',
	  '1P': 'Worldspan|0',
	  JE: 'Mango|0',
	  DP: 'Pobeda|0',
	  AP: 'Air One|0',
	  BA: 'British Airways|1',
	  EH: 'ANA Wings|0',
	  M6: 'Amerijet International|0',
	  AX: 'Trans States Airlines|0',
	  CH: 'Bemidji Aviation Services|0',
	  K8: 'Kannithi Aviation Company Limited d|0',
	  UQ: 'Urumqi Airlines|0',
	  '1H': 'Sirena-Travel|0',
	  TO: 'Transavia France|0',
	  '2X': 'XEROX|0',
	  D4: 'Alidaunia|0',
	  PO: 'Polar Air Cargo|0',
	  YI: 'Yunnan Yingan Airline|0',
	  EF: 'Eznis Airways|0',
	  '8B': 'Business Air|0',
	  K9: 'KrasAvia|1',
	  TU: 'Tunisair|0',
	  '8A': 'Atlas Blue|0',
	  CZ: 'China Southern Airlines|1',
	  LT: 'JSC Air Lituanica|0',
	  GC: 'Global Feeder Services|0',
	  EN: 'Air Dolomiti|0',
	  '8T': 'Air Tindi|0',
	  CK: 'China Cargo Airlines|0',
	  LV: 'Mega Maldives|0',
	  LM: 'Loganair|0',
	  HD: 'Air Do|0',
	  MR: 'Hunnu Air|0',
	  U5: 'USA 3000 Airlines|0',
	  O4: 'Antrak Air|0',
	  ZS: 'Sama Airlines|0',
	  AI: 'Air India|1',
	  KH: 'Aloha Air Cargo|0',
	  '4O': 'Interjet|0',
	  KI: 'KrasAvia|0',
	  GZ: 'Air Rarotonga|0',
	  SB: 'Aircalin|0',
	  NH: 'All Nippon Airways|1',
	  TY: 'Air Caledonie|0',
	  F5: 'Aerotranscargo|0',
	  X4: 'Vanair Limited|0',
	  YP: 'Perimeter Aviation|0',
	  SFG: 'SundsvallsFlyg|0',
	  SP: 'SATA Air Acores|0',
	  FX: 'FedEx|0',
	  N4: 'Nordwind Airlines|0',
	  HR: 'Hahn Air Lines|0',
	  B3: 'Bhutan Airlines|0',
	  '0J': 'Jetclub|0',
	  '5C': 'Nature Air|0',
	  '2P': 'Air Philippines Corporation|0',
	  CL: 'Lufthansa CityLine|0',
	  '4X': 'Romavia|0',
	  FN: 'Fastjet|0',
	  D3: 'Daallo Airlines|0',
	  WOW: 'Air Southwest|0',
	  '9J': 'Dana Air|0',
	  JM: 'Air Jamaica|0',
	  MV: 'Maldivian Air Taxi|0',
	  C2: 'Ceiba Intercontinental|0',
	  T5: 'Turkmenistan Airlines|0',
	  P2: 'AirKenya Express|0',
	  DM: 'Asian Air Company Limited|0',
	  NY: 'Air Iceland|0',
	  FY: 'Firefly|0',
	  PM: 'CanaryFly|0',
	  AVR: 'Avro Flights|0',
	  NZ: 'Air New Zealand|1',
	  SO: 'Apex Airline Public Company Limited|0',
	  WJ: 'Air Labrador|0',
	  CV: 'Cargolux|0',
	  PY: 'Surinam Airways|0',
	  TD: 'Atlantis European Airways|0',
	  ZX: 'Air Georgian|0',
	  E3: 'Sabaidee Airways|0',
	  VI: 'Volga-Dnepr Airlines|0',
	  KE: 'Korean Air|1',
	  '6V': 'MRK Airlines|0',
	  NP: 'Nile Air|0',
	  XM: 'Alitalia Express|1',
	  MQ: 'American Eagle|1',
	  W4: 'LC Peru|0',
	  EIW: 'Aer Lingus|0',
	  FR: 'Ryanair|1',
	  FH: 'Freebird Airlines|0',
	  NQ: 'Air Japan Company|0',
	  LF: 'FlyNordic|0',
	  '7G': 'Starflyer|0',
	  FK: 'Keewatin Air|0',
	  '2J': 'Air Burkina|0',
	  ISK: 'Istanbul|0',
	  RU: 'AirBridgeCargo Airlines|0',
	  L6: 'Mauritania Airlines International|0',
	  '7B': 'Atlant-Soyuz Airlines|0',
	  AJA: 'AnadoluJet|0',
	  SU: 'Aeroflot|1',
	  '6L': 'Aklak|0',
	  PA: 'AirBlue|0',
	  W8: 'Cargojet Airways|0',
	  D0: 'DHL Air|0',
	  '8O': 'West Coast Air|0',
	  VT: 'Air Tahiti|0',
	  GE: 'TransAsia Airways|0',
	  WZ: 'Red Wings|0',
	  '6F': 'Falcon Air Express|0',
	  CF: 'China Postal Airlines|0',
	  CC: 'Air Atlanta Icelandic|0',
	  DF: 'Condor Berlin|0',
	  '8S': 'Shun Tak-China Travel Ship Mgmt.|0',
	  MO: 'Calm Air|0',
	  YL: 'Libyan Wings|0',
	  T4: 'Trip Linhas Aereas|0',
	  GL: 'Air Greenland|0',
	  WE: 'Thai Smile|0',
	  UZ: 'Buraq Air|0',
	  OS: 'Austrian Airlines|1',
	  '4U': 'Germanwings|0',
	  N6: 'Nomad Aviation (PTY)|0',
	  '7K': 'Kolavia|0',
	  KS: 'Penair|0',
	  DJ: 'Virgin Blue|1',
	  GR: 'Aurigny Air Services|0',
	  CU: 'Cubana|0',
	  SW: 'Air Namibia|0',
	  '8K': 'K-Mile Air|0',
	  XF: 'Vladivostok Air|1',
	  A6: 'Air Alps|0',
	  BL: 'Pacific Airlines|0',
	  '8P': 'Pacific Coastal Airlines|0',
	  AL: 'Air Leisure|0',
	  V5: 'Danube Wings|0',
	  '1D': 'Radixx Solutions International|0',
	  '7F': 'First Air|0',
	  '6Q': 'Slovak Airlines|0',
	  '3T': 'Tarco Airlines|0',
	  RS: 'Sky Regional Airlines|0',
	  NM: 'Mount Cook Airline|0',
	  V2: 'Avialeasing Aviation Company|0',
	  P5: 'Copa Airlines Colombia|0',
	  A4: 'Aerocomercial Oriente Norte|0',
	  UG: 'Tunisair Express|0',
	  DC: 'Braathens Regional|0',
	  KU: 'Kuwait Airways|0',
	  QI: 'Cimber Sterling|0',
	  GFG: 'Gotlandsflyg|0',
	  WY: 'Oman Air|0',
	  QK: 'Jazz Air|0',
	  P6: 'Pascan Aviation|0',
	  UA: 'United Airlines|1',
	  '2Z': 'Passaredo Linhas Aereas|0',
	  YN: 'Air Creebec|0',
	  FO: 'Felix Airways|0',
	  KB: 'Druk Air|0',
	  NA: 'North American Airlines|0',
	  DL: 'Delta Air Lines|1',
	  RL: 'Royal Falcon|0',
	  B5: 'East African Safari Air Express|0',
	  SD: 'Sudan Airways|0',
	  EQ: 'TAME|0',
	  V7: 'Volotea|0',
	  ME: 'Middle East Airlines|0',
	  MG: 'Midex Airlines|0',
	  VX: 'Virgin America|0',
	  IS: 'AIS Airlines|0',
	  H7: 'Eagle Aviation Uganda|0',
	  FC: 'Fast Colombia S.A.S|0',
	  EUP: 'OnAir|0',
	  LR: 'LINEAS AEREAS COSTARRICENSES S.A.|0',
	  ZT: 'Titan Airways|0',
	  FA: 'Safair|0',
	  HW: 'North-Wright Airways|0',
	  '5F': 'Fly One|0',
	  V9: 'Van Air Europe a.s.|0',
	  '3O': 'Air Arabia Maroc|0',
	  '3M': 'Silver Airways|0',
	  JW: 'Vanilla Air|0',
	  TH: 'Raya Airways|0',
	  '2I': 'Star Peru|0',
	  '4B': 'Boutique Air|0',
	  '8Z': 'Wizz Air Bulgaria|0',
	  M2: 'Sunrise Airlines|0',
	  LB: 'Air Costa|0',
	  J5: 'Alaska Seaplane Service L.L.C.|0',
	  '5W': 'WESTbahn Management|0',
	  HL: 'Hotels/Motels|0',
	  '9U': 'Air Moldova|0',
	  B9: 'Iran Air Tours|0',
	  NE: 'Nesma Airlines|0',
	  AW: 'Venezolana|0',
	  AT: 'Royal Air Maroc|1',
	  JQ: 'Jetstar Airways|0',
	  '4S': 'Solar Cargo C.A.|0',
	  '6H': 'Israir Airlines|0',
	  IN: 'PT. Nam Air|0',
	  T8: 'Telair International|0',
	  JN: 'Excel Airways|0',
	  GM: 'Germania Flug|0',
	  ZD: 'EWA Air|0',
	  LU: 'Lan Express|0',
	  D5: 'DHL Aero Expreso S.A.|0',
	  NV: 'Naft Airlines|0',
	  GG: 'Sky Lease I|0',
	  CQ: 'Charterlines|0',
	  IJ: 'Spring Airlines Japan|0',
	  J7: 'Centre-Avia Airlines|0',
	  '6O': 'Orbest|0',
	  '4Z': 'Airlink|0',
	  OG: 'Air Onix|0',
	  UI: 'Auric Air Services Limited|0',
	  '5N': 'Nordavia|0',
	  SG: 'SpiceJet|0',
	  SL: 'Thai Lion Air|0',
	  GS: 'Tianjin Airlines|0',
	  VA: 'Virgin Australia|0',
	  '8Q': 'Onur Air|0',
	  BP: 'Air Botswana|0',
	  F1: 'Farelogix|0',
	  SF: 'Tassili Airlines|0',
	  DG: 'Tigerair Philippines|0',
	  J2: 'Azerbaijan Airlines|1',
	  DW: 'Aero-Charter Airlines|0',
	  A0: 'L Avion Elysair|0',
	  '0Y': 'FlyYeti|0',
	  '4K': 'Kenn Borek Air|0',
	  N7: 'Neptune Air Sdn Bhd|0',
	  V8: 'Atran|0',
	  '1U': 'ITA Software|0',
	  '1K': 'Sutra|0',
	  BT: 'Air Baltic|1',
	  JOR: 'Blue Air|0',
	  WP: 'Island Air (USA)|0',
	  AK: 'AirAsia|0',
	  PQ: 'Philippines AirAsia|0',
	  Q3: 'Anguilla Air Services|0',
	  IL: 'Trigana Air Service|0',
	  WG: 'Sunwing Airlines|0',
	  EO: 'Air Go Egypt|0',
	  '9W': 'Jet Airways|0',
	  B7: 'UNI Air|0',
	  FJ: 'Fiji Airways|0',
	  OH: 'Comair|0',
	  '8H': 'BH Air|0',
	  MI: 'SilkAir|1',
	  LYD: 'Lydd Air|0',
	  '7C': 'Jeju Air|0',
	  TT: 'Tigerair Australia|0',
	  KN: 'China United Airlines|0',
	  R3: 'Yakutia Airlines|0',
	  S2: 'JetLite|0',
	  OD: 'Malindo Air|0',
	  KC: 'Air Astana|1',
	  O8: 'Siam Air Transport|0',
	  GH: 'Globus|0',
	  HJ: 'Tasman Cargo Airlines PTY|0',
	  D7: 'AirAsia X|1',
	  AB: 'Air Berlin|1',
	  QQ: 'Alliance Airlines|0',
	  HM: 'Air Seychelles|0',
	  '3D': 'Denim Air|0',
	  '8V': 'Astral Aviation|0',
	  SX: 'SkyWork Airlines|0',
	  L7: 'Linea Aerea Carguera de Colombia S.A.|0',
	  '7U': 'Aviaenergo|0',
	  IY: 'Yemenia|0',
	  J9: 'Jazeera Airways|0',
	  '4J': 'Flydamas Airlines|0',
	  '1J': 'Axess International Network|0',
	  V6: 'Clairmont Holdings|0',
	  BV: 'Blue Panorama Airlines|0',
	  NTJ: 'Nextjet|0',
	  J3: 'Northwestern Air|0',
	  GB: 'ABX Air|0',
	  PK: 'Pakistan Int. Airlines|0',
	  D6: 'Interair South Africa|0',
	  '7O': 'Travel Service Hungary|0',
	  DH: 'Norwegian Air Norway AS|0',
	  DR: 'Ruili Airlines|0',
	  JL: 'Japan Airlines|1',
	  '5R': 'Rutas Aereas C.A.|0',
	  '4D': 'Air Sinai|0',
	  '3C': 'Corporate Express Airlines|0',
	  SV: 'Saudi Arabian Airlines|0',
	  '9D': 'Toumai Air Tchad|0',
	  FW: 'Ibex Airlines|0',
	  DD: 'Nok Air|0',
	  '2M': 'Moldavian Airlines|0',
	  FHE: 'Iceland Express|0',
	  '4H': 'United Airways|0',
	  PH: 'PHOENIX AIR GROUP|0',
	  WN: 'Southwest Airlines|1',
	  '8E': 'Bering Air|0',
	  U3: 'Avies|0',
	  T1: 'Tik Systems (Thailand)|0',
	  PV: 'Privatair SA|0',
	  MS: 'EgyptAir|1',
	  QW: 'Qingdao Airlines|0',
	  A3: 'Aegean Airlines|1',
	  OT: 'Aeropelican|0',
	  '1A': 'Amadeus IT Group SA|0',
	  RH: 'Robin Hood Aviation|0',
	  HH: 'Taban Air|0',
	  SM: 'Air Cairo|0',
	  '3E': 'Multi-Aero|0',
	  T7: 'Twin Jet|0',
	  YD: 'Mauritania Airways|0',
	  M4: 'Smart Aviation|0',
	  UR: 'Utair - Express Limited Company|1',
	  C3: 'Trade Air|0',
	  Z3: 'Avient Aviation|0',
	  '0A': 'Amber Air|0',
	  NI: 'Portugalia|0',
	  V0: 'Conviasa|0',
	  UH: 'AtlasGlobal UA|0',
	  VG: 'VLM Airlines N.V|0',
	  '4T': 'Belair Airlines|0',
	  '9X': 'Executive Express Aviation|0',
	  BR: 'EVA Air|0',
	  M9: 'Motor Sich Airlines|0',
	  KJ: 'Air Incheon Co|0',
	  BF: 'Bluebird Cargo|0',
	  AH: 'Air Algerie|0',
	  '1I': 'NetJets|0',
	  '5T': 'Canadian North|0',
	  HG: 'Niki|1',
	  '5E': 'Siam General Aviation|0',
	  CB: 'Caribbean Air Export Import|0',
	  LH: 'Lufthansa|0',
	  WK: 'Edelweiss Air|0',
	  '6K': 'Sky King|0',
	  TW: 'T\'way Air|0',
	  '2W': 'Welcome Air|0',
	  IA: 'Iraqi Airways|0',
	  '7H': 'Ravn Alaska|0',
	  YJ: 'Asian Wings Airways|0',
	  '9K': 'Cape Air|0',
	  HY: 'Uzbekistan Airways|1',
	  DS: 'EasyJet Switzerland|0',
	  '5B': 'Euro-Asia Air|0',
	  QZ: 'Indonesia AirAsia|0',
	  VL: 'Med-View Airline|0',
	  '5Y': 'Skybus|0',
	  VIL: 'Vildanden|0',
	  OU: 'Croatia Airlines|1',
	  ZP: 'Silk Way Airlines|0',
	  BX: 'Air Busan|0',
	  '2Y': 'PT. My Indo Airlines|0',
	  '2O': 'Island Air Service|0',
	  '9H': 'Dutch Antilles Express|0',
	  G3: 'VRG Linhas Aereas S.A.|0',
	  II: 'IBC Airways|0',
	  JB: 'Helijet International|0',
	  YF: 'Department of National Defence|0',
	  '9R': 'Satena|0',
	  RT: 'RAK Airways|0',
	  SH: 'Sharp Aviation Pty.|0',
	  '3S': 'Air Antilles Express|0',
	  HI: 'Papillon Airways|0',
	  Y9: 'Kish Airlines|0',
	  I3: 'ATA Airlines|0',
	  S3: 'SBA Airlines|0',
	  '3G': 'Gading Sari Aviation Sdn Bhd.|0',
	  R9: 'Camai Air|0',
	  'ÐšÐ¤': 'Ð¢ÑƒÐ²Ð¸Ð½ÑÐºÐ¸Ðµ Ð°Ð²Ð¸Ð°Ñ†Ð¸Ð¾Ð½Ð½Ñ‹Ðµ Ð»Ð¸Ð½Ð¸Ð¸|0',
	  UF: 'Ukrainian Mediterranean Airlines|0',
	  DN: 'Senegal Airlines|0',
	  UK: 'Vistara|0',
	  PR: 'Philippine Airlines|0',
	  IB: 'Iberia|1',
	  WV: 'Aero Vip|0',
	  '7N': 'Pan American World Airways|0',
	  '7Q': 'Elite Airways|0',
	  YO: 'Heli Air Monaco|0',
	  WF: 'Wideroe|0',
	  F6: 'Faroe Jet|0',
	  H2: 'Sky Airline|0',
	  '3L': 'InterSky|0',
	  '8W': 'Private Wings|0',
	  NU: 'Japan Transocean Air|0',
	  FV: 'Rossiya|1',
	  XX: 'Aerodynamics|0',
	  ZU: 'Helios Airways|0',
	  KA: 'Hong Kong Dragon Airlines|0',
	  ZE: 'Eastar Jet|0',
	  'Ð”Ð–': 'Tez Jet|0',
	  SZ: 'Somon Air|0',
	  A9: 'Georgian Airways|1',
	  Y3: 'Driessen Services|0',
	  J0: 'Jetlink Express|0',
	  PG: 'Bangkok Airways|1',
	  '2F': 'Frontier Flying Service|0',
	  PU: 'Pluna|0',
	  Q4: 'Qatar Airways Amiri Flight|0',
	  LI: 'LIAT|0',
	  RY: 'Royal Wings|0',
	  '3Z': 'Travel Service Polska Sp. z.o.o.|0',
	  XG: 'SunExpress Deutschland|0',
	  I6: 'Air Indus|0',
	  R4: 'RUS Aviation|0',
	  '7L': 'Silk Way West Airlines|0',
	  TA: 'TACA Airlines|0',
	  OO: 'SkyWest Airlines|0',
	  F9: 'Frontier Airlines|0',
	  '7V': 'Pelican Air|0',
	  Q5: '40-Mile Air|0',
	  KY: 'Kunming Airlines|0',
	  DK: 'Thomas Cook Airlines Scandinavia|0',
	  E0: 'Eos Airlines|0',
	  '1B': 'Abacus International Pte.|0',
	  VZ: 'MyTravel Airways|0',
	  IM: 'Mint Airways|0',
	  C6: 'Canjet Airlines|0',
	  ZN: 'Naysa|0',
	  O2: 'Jet Air|0',
	  IW: 'Wings Air|0',
	  NX: 'Air Macau|0',
	  OJ: 'Fly Jamaica|0',
	  UD: 'Hex Air|0',
	  QS: 'Travel Service|0',
	  OM: 'MIAT Mongolian Airlines|1',
	  '7M': 'Mistral Air|0',
	  PP: 'Jet Aviation Business Jets AG|0',
	  F3: 'FAI rent-a-jet Aktiengesellschaft|0',
	  FOG: 'FlyOeger|0',
	  TCW: 'Thomas Cook|0',
	  IX: 'Air India Express|0',
	  VK: 'Air Vallee S.p.A.|0',
	  Q6: 'Skytrans|0',
	  BG: 'Biman Bangladesh Airlines|0',
	  YZ: 'Alas Uruguay - Dyrus S.A.|0',
	  '3R': 'Moskovia Airlines|0',
	  '1S': 'Sabre|0',
	  M3: 'North Flying|0',
	  PW: 'Precision Air|0',
	  ED: 'AirExplore|0',
	  S9: 'Starbow Airlines|0',
	  KP: 'PT. Asialink Cargo Express|0',
	  ET: 'Ethiopian Airlines|0',
	  '7W': 'Windrose Air|0',
	  '5I': 'Alsa Grupo SLU|0',
	  XW: 'NokScoot Airlines Company Limited|1',
	  N5: 'Skagway Air Service|0',
	  H8: 'Dalavia Far East Airways|0',
	  DU: 'Hemus Air|0',
	  ZJ: 'Zambezi Airlines|0',
	  YW: 'Air Nostrum|0',
	  AG: 'Air Contractors|0',
	  RX: 'Regent Airways|0',
	  '7Z': 'Ameristar Jet Charter|0',
	  A5: 'HOP!|0',
	  HK: 'Hamburg Airways|0',
	  Y4: 'Volaris|0',
	  '7E': 'Sylt Air|0',
	  VR: 'TACV Cape Verde Airlines|0',
	  ST: 'Germania|0',
	  ZV: 'V Air|0',
	  LD: 'Air Hong Kong|0',
	  YS: 'Regional|0',
	  '9G': '9G Rail Limited|0',
	  UL: 'SriLankan Airlines|1',
	  '2N': 'NextJet|0',
	  '5U': 'Transportes Aereos Guatemaltecos S.|0',
	  PN: 'China West Air|0',
	  '3X': 'Premier Trans Aire|0',
	  '4E': 'Stabo Air Limited|0',
	  DEW: 'Condor|0',
	  B8: 'Eritrean Airlines|0',
	  CY: 'Cyprus Airways|1',
	  '2C': 'SNCF|0',
	  LK: 'Kyrgyz Airlines|0',
	  QL: 'Laser Airlines|0',
	  LG: 'Luxair|0',
	  '1F': 'INFINI Travel Information|0',
	  VF: 'Valuair|0',
	  A8: 'Ameriflight|0',
	  F2: 'Safarilink Aviation Limited|0',
	  VS: 'Virgin Atlantic Airways|1',
	  IR: 'Iran Air|0',
	  B0: 'La Compagnie|0',
	  '9S': 'Southern Air|0',
	  I4: 'Scott Air|0',
	  '4W': 'Allied Air|0',
	  EV: 'ExpressJet|0',
	  NL: 'Shaheen Air International|0',
	  IF: 'Islas Airways|0',
	  WX: 'Cityjet|0',
	  '5P': 'SkyEurope Airlines Hungary|0',
	  ZC: 'Korongo Airlines|0',
	  '2V': 'Amtrak|0',
	  QT: 'Avianca Cargo|0',
	  X3: 'TUIfly|0',
	  F4: 'Air Charter|0',
	  JU: 'AirSERBIA|1',
	  EU: 'Chengdu Airlines|0',
	  NG: 'Aero Contractors|0',
	  QB: 'Qeshm Airlines|0',
	  '9C': 'Spring Airlines|0',
	  '3W': 'Malawian Airlines|0',
	  K4: 'Kalitta Air|0',
	  '2U': 'Sun d\'Or International Airlines|0',
	  QG: 'Citilink|0',
	  '5X': 'UPS Airlines|0',
	  ZI: 'Aigle Azur|0',
	  YE: 'YanAir|0',
	  XJ: 'Thai AirAsia X|0',
	  SI: 'Blue Islands|0',
	  F8: 'Flair Airlines|0',
	  NW: 'Northwest Airlines Inc|0',
	  RP: 'Chautauqua Airlines|0',
	  '7Y': 'Mann Yadanarpon Airlines|0',
	  H5: 'I-Fly|0',
	  Q1: 'PT. Sqiva Sistem|0',
	  EY: 'Etihad Airways|1',
	  YH: 'Sunsplash Aviation .|0',
	  '6N': 'Al-Naser Airlines|0',
	  CT: 'Alitalia CityLiner|0',
	  U7: 'Air Uganda|0',
	  L4: 'Wings of Lebanon|0',
	  NK: 'Spirit Airlines|0',
	  UX: 'Air Europa|1',
	  EC: 'Open Skies|0',
	  E5: 'Air Arabia Egypt|0',
	  WH: 'Webjet|0',
	  G5: 'China Express Airlines|0',
	  UW: 'Uni-Top Airlines|0',
	  OQ: 'Chongqing Airlines|0',
	  '3H': 'Air Inuit|0',
	  '3A': 'Kenosha Aero|0',
	  K5: 'SeaPort Airlines|0',
	  QR: 'Qatar Airways|1',
	  JV: 'Bearskin Airlines|0',
	  ZB: 'Monarch Airlines|0',
	  '1X': 'Fly Branson Travel|0',
	  '8R': 'Sol Lineas Aereas|0',
	  '1E': 'Travelsky Technology Limited|0',
	  XP: 'TEM Enterprises|0',
	  '3V': 'TNT Airways|0',
	  YU: 'EuroAtlantic Airways|0',
	  LE: 'Lugansk Airlines|0',
	  K7: 'Air KBZ|0',
	  T0: 'Trans American Airlines S.A. TACA PERU|0',
	  QA: 'Cimber|0',
	  DV: 'SCAT|0',
	  UFG: 'Umeaflyg|0',
	  C9: 'Cirrus Airlines|0',
	  JP: 'Adria Airways|1',
	  CE: 'Chalair Aviation|0',
	  H3: 'Hermes Airlines|0',
	  '9I': 'Airline Allied Services Limited|0',
	  Y0: 'Yellow Air Taxi|0',
	  P8: 'P.L.A.S. S/A|0',
	  NB: 'Skypower Express Airways|0',
	  MT: 'Thomas Cook Airlines|0',
	  ZL: 'Regional Express|0',
	  KG: 'Key Lime Air|0',
	  '4Y': 'Flight Alaska|0',
	  O3: 'SF Airlines|0',
	  C4: 'Conquest Air|0',
	  '2E': 'AVE.com|0',
	  '6E': 'IndiGo Airlines|0',
	  '4I': 'IHY Izmir Havayollari A.S.|0',
	  AEU: 'Iceland Express|0',
	  L2: 'Lynden Air Cargo|0',
	  NS: 'Hebei Airlines|0',
	  G7: 'KAPO Avia|0',
	  '1V': 'Galileo International|0',
	  ZR: 'Aviacon Zitotrans|0',
	  D9: 'Donavia|0',
	  '4N': 'Air North|0',
	  I7: 'Paramount Airways|0',
	  '4F': 'Air City|0',
	  AE: 'Mandarin Airlines|0',
	  FL: 'AirTran|0',
	  N2: 'Trans-Mediterranean Airways|0',
	  B4: 'ZanAir Limited|0',
	  U4: 'Buddha Air|0',
	  DB: 'Brit Air|0',
	  LQ: 'Air Guinea Cargo|0',
	  AR: 'Aerolineas Argentinas|0',
	  L9: 'Bristow U.S.|0',
	  R1: 'Zapways|0',
	  Y8: 'Yangtze River Express|0',
	  VN: 'Vietnam Airlines|0',
	  XZ: 'South African Express|0',
	  LC: 'Equatorial Congo Airlines (ECAIR)|0',
	  '1Y': 'Electronic Data Systems Corporation|0',
	  FG: 'Ariana Afghan Airlines|0',
	  X9: 'Avion Express|0',
	  I8: 'Izhavia|0',
	  EL: 'Ellinair|0',
	  QN: 'Air Armenia|0',
	  IK: 'Ikar|0',
	  TM: 'LAM Mozambique Airlines|0',
	  C1: 'Tectimes Sudamericana S.A.|0',
	  GK: 'Jetstar Japan|0',
	  BD: 'Cambodia Bayon Airlines Limited|1',
	  CD: 'Dutch Corendon Airlines|0',
	  UY: 'Air Caucasus|0',
	  JK: 'Spanair|1',
	  EP: 'Iran Aseman Airlines|0',
	  S7: 'S7 Airlines|1',
	  XK: 'Air Corsica|0',
	  PL: 'Southern Air Charter Limited|0',
	  FS: 'Syphax Airlines|0',
	  TG: 'Thai Airways|1',
	  VB: 'VivaAerobus|0',
	  VIK: 'Viking Airlines|0',
	  W3: 'Arik Air|0',
	  OC: 'Omni|0',
	  OY: 'Omni Air International|0',
	  RA: 'Royal Nepal Airlines|0',
	  L5: 'Lufttransport|0',
	  '3J': 'Jubba Airways|0',
	  WI: 'White|0',
	  EK: 'Emirates|1',
	  '8F': 'STP Airways|0',
	  ML: 'Air Mediterranee|0',
	  S0: 'Slok Air Intl|0',
	  '8D': 'FitsAir|0',
	  LN: 'Libyan Airlines|0',
	  '3Y': 'Baseops International|0',
	  DY: 'Norwegian|0',
	  IQ: 'Augsburg Airways|0',
	  '4M': 'LAN Argentina|0',
	  TJ: 'Tradewind Aviation|0',
	  RW: 'RAS Fluggesellschaft|0',
	  WM: 'Winair|0',
	  HE: 'Luftfahrtgesellschaft Walter|0',
	  G0: 'Ghana Intl|0',
	  UM: 'Air Zimbabwe|0',
	  UN: 'Transaero Airlines|1',
	  T3: 'Eastern Airways|0',
	  '5G': 'Skyservice Airlines|0',
	  C8: 'Cronos Airlines|0',
	  HT: 'Ningxia Cargo Airlines|0',
	  WR: 'WestJet Encore|0',
	  N9: 'Novair|0',
	  '2K': 'Avianca Ecuador|0',
	  B1: 'Bravo Passenger Solution Pte.|0',
	  '9L': 'Colgan Air|0',
	  SN: 'Brussels Airlines|1',
	  KK: 'Atlasjet|1',
	  O6: 'Avianca Brasil|0',
	  CAI: 'Corendon|0',
	  XE: 'ExpressJet Airlines|0',
	  '2L': 'Helvetic Airways|0',
	  X7: 'Exec Air of Naples|0',
	  XI: 'Aeronautical Telecommunications|0',
	  '1Z': 'Sabre Pacific|0',
	  U6: 'Ural Airlines|1',
	  E6: 'Bringer Air Cargo Taxi Aereo|0',
	  '6G': 'Sun Air Express|0',
	  '5L': 'Aerosur|0',
	  CG: 'Airlines of PNG|0',
	  R8: 'Kyrgyzstan Airlines|0',
	  BK: 'Okay Airways|0',
	  W5: 'Mahan Air|0',
	  YT: 'Yeti Airlines|0',
	  YC: 'Yamal Airlines|0',
	  NF: 'Air Vanuatu|0',
	  EJ: 'European Cargo Services BV|0',
	  SS: 'Corsair|0',
	  EB: 'Wamos Air|0',
	  JZ: 'Skyways AB|0',
	  ZG: 'Grozny Avia|0',
	  GV: 'Sky Gabon S.A.|0',
	  MZ: 'Merpati|0',
	  MC: 'Air Mobility Command|0',
	  IE: 'Solomon Airlines|0',
	  BM: 'BMI Regional|0',
	  A2: 'Astra Airlines|0',
	  CO: 'Continental Airlines|1',
	  MU: 'China Eastern Airlines|1',
	  LS: 'Jet2|0',
	  FM: 'Shanghai Airlines|1',
	  Y7: 'NordStar|0',
	  Q2: 'Maldivian|0',
	  '6Z': 'Euro-Asia Air|0',
	  S4: 'SATA International|1',
	  '9M': 'Central Mountain Air|0',
	  U9: 'Tatarstan Air|1',
	  YIN: 'Yining Arpt|0',
	  JJ: 'TAM Linhas Aereas|0',
	  '8U': 'Afriqiyah Airways|0',
	  AZ: 'Alitalia|1',
	  BW: 'Caribbean Airlines|0',
	  Z4: 'ZagrosJet|0',
	  YK: 'Avia Traffic|0',
	  EA: 'Eastern Air Lines|0',
	  PB: 'Provincial Airlines|0',
	  HQ: 'Thomas Cook Airlines Belgium|0',
	  PE: 'People\'s Viennaline|0',
	  MK: 'Air Mauritius|1',
	  US: 'US Airways|1',
	  P0: 'Proflight Air Services|0',
	  '7R': 'Rusline|0',
	  OV: 'Estonian Air|1',
	  JT: 'Lion Airlines|0',
	  PS: 'Ukraine Int. Airlines|1',
	  '9V': 'Avior Airlines|0',
	  CP: 'Compass Airlines|0',
	  TI: 'Tailwind Airlines|0',
	  RI: 'PT. Mandella Airlines|0',
	  K6: 'Kosmos Airlines|1',
	  'Ð˜Ð“': 'Komiaviatrans|1',
	  '9F': 'Eurostar International Limited|0',
	  CA: 'Air China|1',
	  '6R': 'Alrosa|0',
	  TF: 'Malmo Aviation|0',
	  '0B': 'Blue Air|0',
	  FF: 'Airshop B.V.|0',
	  UJ: 'Almasria Universal Airlines|0',
	  TB: 'Jetairfly|0',
	  CW: 'Air Marshall Islands|0',
	  H6: 'Bulgarian Air Charter|0',
	  AY: 'Finnair|1',
	  MW: 'Mokulele Flight Service|0',
	  JD: 'Capital Airlines|0',
	  ON: 'Our Airline|0',
	  U8: 'Armavia|1',
	  RV: 'Air Canada rouge|0',
	  LL: 'Miami Air International|0',
	  DZ: 'Donghai Airlines|0',
	  F7: 'Darwin Airline|0',
	  AM: 'Aeromexico|1',
	  LZ: 'Belle Air|0',
	  MX: 'Mexicana|0',
	  VV: 'Aerosvit Airlines|1',
	  O7: 'ÐžÑ€ÐµÐ½Ð±ÑƒÑ€Ð¶ÑŒÐµ|0',
	  T2: 'Nakina Air Service|0',
	  'Ð­Ðš': 'KrasAvia|0',
	  LO: 'LOT|1',
	  EZ: 'Sun-Air|0',
	  EW: 'Eurowings|0',
	  OL: 'OLT Ostfriesische Luftransport|0',
	  KR: 'Air Bishkek|0',
	  P1: 'Aviation Technologies|0',
	  HU: 'Hainan Airlines|1',
	  CM: 'Copa Airlines|0',
	  JH: 'Fuji Dream Airlines|0',
	  L8: 'Afric Aviation|0',
	  MD: 'Air Madagascar|0',
	  JR: 'Joy Air|0',
	  YG: 'South Airlines|0',
	  IT: 'Tigerair Taiwan|0',
	  IO: 'IrAero|0',
	  CI: 'China Airlines|0',
	  MJ: 'Mihin Lanka|0',
	  KV: 'Asian Express Airline|0',
	  JO: 'Jet Time OY|0',
	  DI: 'DBA Luftfahrtgesellschaft|0',
	  ND: 'FMI Air|0',
	  QE: 'Crossair Europe|0',
	  RB: 'Syrian Air|0',
	  GX: 'Guangxi Beibu Gulf Airlines|0',
	  QX: 'Horizon Air|0',
	  MB: 'MNG Airlines|0',
	  K2: 'EuroLot S.A.|0',
	  '9N': 'Tropic Air Limited|0',
	  '5Q': 'LOS CIPRESES S.A.|0',
	  IC: 'Indian Airlines|0',
	  XH: 'Special Ground Handling Service -XH|0',
	  OW: 'Executive Airlines|0',
	  UE: 'Ultimate Jetcharters|0',
	  IV: 'Caspian Airlines|0',
	  ZQ: 'Caribbean Sun Airlines|0',
	  '7P': 'Air Panama|0',
	  JC: 'Japan Air Commuter|0',
	  ZF: 'Azur Air|0',
	  TN: 'Air Tahiti Nui|0',
	  G8: 'Go Air|0',
	  XN: 'Express Air|0',
	  '4V': 'Birdy Airlinesce|0',
	  W9: 'Air Bagan|0',
	  '5V': 'Everts Air Alaska|0',
	  BY: 'Thomson Airways|1',
	  JA: 'B&H Airlines|0',
	  '0D': 'Darwin Airlines|0',
	  TL: 'Airnorth|0',
	  'Ð®Ð“': 'Pioneer|1',
	  WW: 'WOW air|0',
	  ZA: 'Sky Angkor Airlines|0',
	  PI: 'Polar Airlines OJSC|0',
	  JG: 'Jetgo Australia Holdings Pty|0',
	  WO: 'World Airways|0',
	  KT: 'Katmai Air|0',
	  '6P': 'Clubair Sixgo|0',
	  E7: 'Estafeta Carga Aerea|0',
	  QV: 'Lao Airlines|0',
	  XB: 'International Air Transport Association.|0',
	  Q0: 'Quebecair Express|0',
	  KX: 'Cayman Airways|0',
	  '2B': 'Ak Bars Aero|0',
	  '0V': 'Vietnam Air Services Company|0',
	  RF: 'Florida West International Airways|0',
	  BC: 'Skymark Airlines|0',
	  PX: 'Air Niugini|0',
	  XV: 'MR Lines|0',
	  '7S': 'Arctic Transportation Services|0',
	  NR: 'Al-Naser Airlines|0',
	  BB: 'Seaborne Airlines|0',
	  QJ: 'Jet Airways|0',
	  IG: 'Meridiana|1',
	  '4P': 'Viking Airlines|0',
	  A7: 'Air Comet|0',
	  PT: 'West Air Sweden|0',
	  '8L': 'Lucky Air|0',
	  HZ: 'Aurora|0',
	  '9B': 'AccesRail and Partner Railways|0',
	  LA: 'LAN Airlines|1',
	  IH: 'Irtysh-Air|0',
	  AD: 'Azul Brazilian Airlines|1',
	  Z7: 'NU-Aero (Pvt)|0',
	  QF: 'Qantas|1',
	  EXT: 'Kalmarflyg|0',
	  UP: 'Bahamasair|0',
	  DE: 'Condor|0',
	  IP: 'Apsara International Air|0',
	  VU: 'Vuelos Economicos Centroamericanos|0',
	  ZK: 'Great Lakes Aviation|0',
	  HX: 'Hong Kong Airlines|1',
	  FZ: 'FlyDubai|0',
	  '7A': 'Air Jet|0',
	  Y5: 'Golden Myanmar Airlines|0',
	  FQ: 'Fenix Airways of Florida Inc|0',
	  DT: 'TAAG Angola Airlines|0',
	  MP: 'Martinair Holland|1',
	  BU: 'Compagnie Africaine d\'Aviation|0',
	  MM: 'Peach Aviation|0',
	  RK: 'Skyview Airways Company Limited|0',
	  BH: 'Hawkair Aviation Services|0',
	  O9: 'Nova Airways|0',
	  G2: 'Alianza Glancelot C.A.|0',
	  XY: 'Flynas|0',
	  BS: 'British Intl|0',
	  OP: 'Air Pegasus Pvt.|0',
	  '1C': 'Hewlett-Packard (Schweiz)|0',
	  EG: 'East Air|0',
	  LJ: 'Jin Air|0',
	  NT: 'Binter Canarias|0',
	  HN: 'Afghan Jet International|0',
	  Q8: 'Trans Air Congo|0',
	  SY: 'Sun Country Airlines|0',
	  GF: 'Gulf Air|0',
	  FD: 'Thai AirAsia|0',
	  XA: 'ARINC|0',
	  '4A': 'Royal Bengal Airlines|0',
	  Z8: 'Compania de Servicios de Transporte|0',
	  E4: 'Enter Air|0',
	  '6D': 'Travel Service Slovakia|0',
	  GW: 'Skygreece Airlines|0',
	  DO: 'Discovery Air|0',
	  '5S': 'Servicios Aereos Profesionales S.A.|0',
	  OK: 'Czech Airlines|1',
	  M5: 'Kenmore Air|0',
	  ER: 'Astar Air Cargo|0',
	  KO: 'Komiaviatrans|0',
	  SK: 'SAS|1',
	  AF: 'Air France|1',
	  RJ: 'Royal Jordanian|1',
	  '3K': 'Jetstar Asia|0',
	  TC: 'Air Tanzania|0',
	  J1: 'One Jet|0',
	  P4: 'Air Charter Africa|0',
	  SJ: 'Sriwijaya Air|0',
	  TX: 'Air Caraibes|0',
	  B2: 'Belavia|1',
	  YQ: 'Polet Airlines|0',
	  KL: 'KLM|1',
	  TR: 'Tigerair|0',
	  NO: 'Neos|0',
	  EM: 'Empire Airlines|0',
	  TK: 'Turkish Airlines|1',
	  UU: 'Air Austral|0',
	  Q7: 'Swift Air|0',
	  '9O': 'National Airways Cameroon|0',
	  LP: 'LAN Peru|0',
	  GT: 'GB Airways|0',
	  EI: 'Aer Lingus|0',
	  IU: 'SW Italia s.p.a.|0',
	  GJ: 'Zhejiang Loong Airlines|0',
	  RM: 'Regional Air|1',
	  '6A': 'Armenia Airways|0',
	  QU: 'UTair Ukraine|0',
	  '9A': 'Eaglexpress Air Charter Sdn.Bhd.|0',
	  PZ: 'TAM - Transportes Aereos del|0',
	  QO: 'Quikjet Cargo Airlines Pvt.|0',
	  LW: 'Pacific Wings L.L.C.|0',
	  HP: 'Amapola Flyg AB|0',
	  HB: 'Asia Atlantic Airlines|0',
	  XQ: 'SunExpress|0',
	  YM: 'Montenegro Airlines|1',
	  OR: 'TUIFLY NETHERLANDS|0',
	  '9P': 'Air Arabia Jordan|0',
	  RQ: 'Kam-Air|0',
	  N3: 'Aerolineas Mas|0',
	  '8I': 'MyAir|0',
	  HC: 'Aero Tropics Air Service|0',
	  XU: 'African Express Airways|0',
	  T9: 'Trans Meridian Airlines|0',
	  '7J': 'Tajik Air|0',
	  JF: 'Jet Asia Airways|0',
	  CS: 'Continental Micronesia|0',
	  BJ: 'Nouvelair Tunisie|0',
	  EX: 'Thai Express Air|0',
	  P9: 'Peruvian Airlines|0',
	  QY: 'EAT Leipzig|0',
	  Y1: 'Travel Technology Interactive SA|0',
	  GO: 'ULS Airlines Cargo|0',
	  BQ: 'Aeromar Airlines|0',
	  RC: 'Atlantic Airways|0',
	  '3N': 'Air Urga|0',
	  'Ð›ÐŸ': 'Pskovavia|1',
	  '3F': 'Pacific Airways|0',
	  XD: 'Universal Air Travel Plan (UATP)|0',
	  XC: 'Corendon Airlines|0',
	  '5J': 'CEBU Pacific Air|0',
	  R7: 'Aserca Airlines|0',
	  TS: 'Air Transat|0',
	  UC: 'LAN Cargo|0',
	  I2: 'Iberia Express|0',
	  D2: 'Severstal|0',
	  S1: 'Lufthansa Systems AG|0',
	  K3: 'Venture Travel|0',
	  FU: 'Fuzhou Airlines|0',
	  '1T': 'Bulgarian Air Charter|0',
	  SC: 'Shandong Airlines|0',
	  G6: 'Angkor Airways Corporation|0',
	  D8: 'Norwegian Air International|0',
	  SA: 'South African Airways|1',
	  TQ: 'Tandem Aero|0',
	  PJ: 'Air Saint-Pierre|0',
	  Z9: 'Bek Air|0',
	  LX: 'Swiss|1',
	  TP: 'TAP Portugal|1',
	  H9: 'Himalaya Airlines Pvt.|0',
	  WC: 'Islena Airlines|0',
	  G9: 'Air Arabia|0',
	  CN: 'Grand China Air|0',
	  OX: 'Orient Thai Airlines|0',
	  GSM: 'flyglobespan|0',
	  '2G': 'Angara Airlines|0',
	  WD: 'Dairo Air Services|0',
	  DX: 'Danish Air Transport|0',
	  '2A': 'Deutsche Bahn AG|0',
	  YX: 'Republic Airlines|0',
	  '2D': 'Dynamic Airways|0',
	  J4: 'BADR AIRLINES|0',
	  CR: 'UBM Aviation - OAG|0',
	  '3B': 'Central Connect Airlines|0',
	  R2: 'Orenair|0',
	  KM: 'Air Malta|1',
	  Z5: 'GMG Airlines|0',
	  '6Y': 'SmartLynx Airlines Estonia|0',
	  KD: 'KalStar Aviation|0',
	  MF: 'Xiamen Airlines|0',
	  '5Z': 'CemAir|0',
	  S6: 'Sunrise Airways|0',
	  '1M': 'JSC Transport Automated Information|0',
	  W1: 'WDL Aviation|0',
	  '5O': 'Europe Airpost|0',
	  XT: 'Indonesia AirAsia X|0',
	  '4C': 'LAN Colombia|0',
	  WA: 'KLM cityhopper|0',
	  '8N': 'Aerodynamics|0',
	  XL: 'LAN Ecuador|0',
	  BI: 'Royal Brunei Airlines|0',
	  '7I': 'Insel Air|0',
	  P7: 'Small Planet Airlines Polska|0',
	  WS: 'WestJet|1',
	  PC: 'Pegasus Airlines|0',
	  '6J': 'Solaseed Air|0',
	  AV: 'Avianca|0',
	  OB: 'Boliviana de Aviacion|0',
	  '5M': 'Montserrat Airways|0',
	  NJ: 'Nordic Global Airlines|0',
	  VE: 'Volare Spa|0',
	  '6W': 'Saravia|0',
	  S5: 'Small Planet Airlines|0',
	  W2: 'Flex Flight|0',
	  MA: 'Malev|1',
	  RE: 'Stobart Air|0',
	  AS: 'Alaska Airlines|0',
	  MH: 'Malaysia Airlines|1',
	  OA: 'Olympic Air|0',
	  RZ: 'Sansa Airlines|0',
	  Z6: 'Dniproavia|1',
	  QP: 'Starlight Airline|0',
	  WU: 'Wizz Air Ukraine|0',
	  GY: 'Gabon Airlines|0',
	  ZW: 'Air Wisconsin Airlines Corporation|0',
	  BZ: 'Bluebird Airways|0',
	  GP: 'Gadair|0',
	  GA: 'Garuda Indonesia|1',
	  N8: 'National Air Cargo Group|0',
	  JY: 'interCaribbean Airways|0',
	  MY: 'Maya Island Air|0',
	  BCI: 'Blue Islands|0',
	  CJ: 'BA CityFlyer|0',
	  DQ: 'Air Direct Connect|0',
	  QH: 'Air Kyrgyzstan|0',
	  ID: 'Batik Air|0',
	  JAF: 'TUI Airlines|0',
	  FB: 'Bulgaria Air|1',
	  '6S': 'Kato Airline|0',
	  VW: 'Aeromar|0',
	  VO: 'Tyrolean Airways|1',
	  '9Q': 'Caicos Express Airways|0',
	  '9T': 'ACT Airlines|0',
	  HS: 'Direktflyg|0'
	};

/***/ },
/* 39 */
/***/ function(module, exports) {

	'use strict';
	
	// import  from '';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var DEALS = exports.DEALS = [{
	  'currency': 'RUB',
	  'price': 18045,
	  'dateCreated': 1490218242200,
	  'is2OW4RT': false,
	  'fareType': 'direct',
	  'departDate': '20171010',
	  'from': 'MOW',
	  'to': 'NYC',
	  'to_country': 'US',
	  'directions': [[{
	    'from': 'SVO',
	    'to': 'JFK',
	    'continued': false,
	    'startDate': '20171010',
	    'startTerminal': 'D',
	    'startTime': '1425',
	    'endDate': '',
	    'endTerminal': '1',
	    'endTime': '1720',
	    'eTkAvail': true,
	    'airCompany': 'SU',
	    'flightNumber': '102',
	    'fic': 'NVULA',
	    'flightTime': '0955',
	    'journeyTime': '0955',
	    'fromGeo': {
	      'lat': 55.7522,
	      'lon': 37.6156,
	      'country': 'RU'
	    },
	    'toGeo': {
	      'lat': 40.7143,
	      'lon': -74.006,
	      'country': 'US'
	    }
	  }], [{
	    'from': 'JFK',
	    'to': 'SVO',
	    'continued': false,
	    'startDate': '20171016',
	    'startTerminal': '1',
	    'startTime': '0100',
	    'endDate': '',
	    'endTerminal': 'D',
	    'endTime': '1720',
	    'eTkAvail': true,
	    'airCompany': 'SU',
	    'flightNumber': '123',
	    'fic': 'NVULA',
	    'flightTime': '0920',
	    'journeyTime': '0920',
	    'fromGeo': {
	      'lat': 40.7143,
	      'lon': -74.006,
	      'country': 'US'
	    },
	    'toGeo': {
	      'lat': 55.7522,
	      'lon': 37.6156,
	      'country': 'RU'
	    }
	  }]],
	  'returnDate': '20171016'
	}, {
	  'currency': 'RUB',
	  'price': 25459,
	  'dateCreated': 1490216301003,
	  'is2OW4RT': false,
	  'fareType': 'direct',
	  'departDate': '20171010',
	  'from': 'MOW',
	  'to': 'LAX',
	  'to_country': 'US',
	  'directions': [[{
	    'from': 'SVO',
	    'to': 'LAX',
	    'continued': false,
	    'startDate': '20171010',
	    'startTerminal': 'D',
	    'startTime': '1155',
	    'endDate': '',
	    'endTerminal': 'B',
	    'endTime': '1405',
	    'eTkAvail': true,
	    'airCompany': 'SU',
	    'flightNumber': '106',
	    'fic': 'NVULA',
	    'flightTime': '1210',
	    'journeyTime': '1210',
	    'fromGeo': {
	      'lat': 55.7522,
	      'lon': 37.6156,
	      'country': 'RU'
	    },
	    'toGeo': {
	      'lat': 34.0522,
	      'lon': -118.244,
	      'country': 'US'
	    }
	  }], [{
	    'from': 'LAX',
	    'to': 'SVO',
	    'continued': false,
	    'startDate': '20171020',
	    'startTerminal': 'B',
	    'startTime': '1605',
	    'endDate': '',
	    'endTerminal': 'D',
	    'endTime': '1345',
	    'eTkAvail': true,
	    'airCompany': 'SU',
	    'flightNumber': '107',
	    'fic': 'NVULA',
	    'flightTime': '1140',
	    'journeyTime': '1140',
	    'dayChg': 1,
	    'fromGeo': {
	      'lat': 34.0522,
	      'lon': -118.244,
	      'country': 'US'
	    },
	    'toGeo': {
	      'lat': 55.7522,
	      'lon': 37.6156,
	      'country': 'RU'
	    }
	  }]],
	  'returnDate': '20171020'
	}, {
	  'currency': 'RUB',
	  'price': 33399,
	  'dateCreated': 1490211833984,
	  'is2OW4RT': false,
	  'fareType': 'direct',
	  'departDate': '20171002',
	  'from': 'MOW',
	  'to': 'WAS',
	  'to_country': 'US',
	  'directions': [[{
	    'from': 'SVO',
	    'to': 'IAD',
	    'continued': false,
	    'startDate': '20171002',
	    'startTerminal': 'D',
	    'startTime': '0925',
	    'endDate': '',
	    'endTerminal': '',
	    'endTime': '1250',
	    'eTkAvail': true,
	    'airCompany': 'SU',
	    'flightNumber': '104',
	    'fic': 'NVUA',
	    'flightTime': '1025',
	    'journeyTime': '1025',
	    'fromGeo': {
	      'lat': 55.7522,
	      'lon': 37.6156,
	      'country': 'RU'
	    },
	    'toGeo': {
	      'lat': 38.8929,
	      'lon': -77.0057,
	      'country': 'US'
	    }
	  }], [{
	    'from': 'IAD',
	    'to': 'SVO',
	    'continued': false,
	    'startDate': '20171016',
	    'startTerminal': '',
	    'startTime': '1445',
	    'endDate': '',
	    'endTerminal': 'D',
	    'endTime': '0720',
	    'eTkAvail': true,
	    'airCompany': 'SU',
	    'flightNumber': '105',
	    'fic': 'NVUA',
	    'flightTime': '0935',
	    'journeyTime': '0935',
	    'dayChg': 1,
	    'fromGeo': {
	      'lat': 38.8929,
	      'lon': -77.0057,
	      'country': 'US'
	    },
	    'toGeo': {
	      'lat': 55.7522,
	      'lon': 37.6156,
	      'country': 'RU'
	    }
	  }]],
	  'returnDate': '20171016'
	}];
	
	var FLIGHTS = exports.FLIGHTS = {
	  'status': 'success',
	  'data': {}
	};
	
	//
	// import Format, {zeroNumber} from '../mixins/Format';
	// import AIRLINES from '../libs/ref/airlines';
	// import CITIES from '../libs/ref/cities';
	//
	// let airline = [], cities = [];
	//
	// function generateAirlines() {
	//   airline = Object.keys(AIRLINES);
	// }
	//
	// function generateCities() {
	//   cities = Object.keys(CITIES);
	// }
	//
	// function initTemps() {
	//   generateAirlines();
	//   generateCities();
	// }
	// /**
	//  *
	//  * @param {Array.<string>} not
	//  * @param {number} [index]
	//  * @return {string}
	//  */
	// function getIATA(not = [], index = (Math.random() * cities.length) | 0) {
	//   index = index >= cities.length ? 0 : index;
	//   const city = cities[index];
	//   if (not.indexOf(city) > -1)
	//     return getIATA(not, index + 1);
	//   return city;
	// }
	//
	// function generateFare(from, to, startDate, endDate, continued) {
	//   const time = (endDate.getTime() - startDate.getTime()) / 1000 / 60;
	//
	//   let minutes = time % 60
	//     , hours = (time - minutes) / 60;
	//   return {
	//     'from'      : from,
	//     'to'        : to,
	//     'continued' : continued,
	//
	//     'startDate' : Format.query.bestDeals.dateToString(startDate).replace(/-/g, ''),
	//     'startTime' : Format.query.bestDeals.dateToTimeString(startDate).replace(':', ''),
	//     'endDate'   : Format.query.bestDeals.dateToString(endDate).replace(/-/g, ''),
	//     'endTime'   : Format.query.bestDeals.dateToTimeString(endDate).replace(':', ''),
	//
	//     'airCompany' : airline[((Math.random() * airline.length) | 0)],
	//     'flightTime' : `${zeroNumber(hours)}${zeroNumber(minutes)}`,
	//   }
	// }
	// initTemps();
	// console.log(generateFare('MOW', 'NYC', new Date(2017, 4, 10, 10, 0), new Date(2017, 4, 11, 7, 25), false));
	//
	// function generateDirection(origin, destination, date) {
	//   const transfers = [0, 0, 1, 2][((Math.random() * 4) | 0)];
	//   const duration = (60) + Math.random() * (22 * 60);
	//
	//
	//
	//   switch (transfers) {
	//     case 0:
	//
	//       break;
	//     case 1:
	//       break;
	//     case 2:
	//       break;
	//   }
	//
	//   return [];
	// }
	//
	// console.log(generateDirection());
	//
	// function generateDeal() {
	//   let deal = {
	//     'currency'    : 'RUB',
	//     'price'       : 1228,
	//     'dateCreated' : 1490188502642,
	//     'is2OW4RT'    : false,
	//     'fareType'    : 'direct',
	//     'departDate'  : '20170810',
	//     'from'        : 'MOW',
	//     'to'          : 'KVX',
	//     'to_country'  : 'RU',
	//     'directions'  : [
	//       [
	//         {
	//           'from'          : 'DME',
	//           'to'            : 'KVX',
	//           'continued'     : false,
	//           'startDate'     : '20170810',
	//           'startDateTime' : '2017-08-10T05:35:00.000Z',
	//           'startTime'     : '0835',
	//           'endDate'       : '20170810',
	//           'endDateTime'   : '2017-08-10T07:00:00.000Z',
	//           'endTime'       : '1000',
	//           'airCompany'    : '6W',
	//           'flightTime'    : '0125',
	//           'journeyTime'   : '0125'
	//         }
	//       ]
	//     ],
	//     'returnDate'  : '20170810'
	//   };
	// }
	//
	// export function generateDeals() {
	//   const days = 365;
	// }
	//

/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.generateMonthList = exports.monthList = exports.currentDate = undefined;
	exports.normalizeDateToDay = normalizeDateToDay;
	exports.isOneDay = isOneDay;
	exports.smallerThenDay = smallerThenDay;
	exports.biggerThenDay = biggerThenDay;
	exports.nextDay = nextDay;
	exports.addDays = addDays;
	exports.isDisabledDay = isDisabledDay;
	exports.setDateInRange = setDateInRange;
	exports.inDateRange = inDateRange;
	exports.isOneDayOrNotExist = isOneDayOrNotExist;
	exports.days_between = days_between;
	
	var _Format = __webpack_require__(17);
	
	/**
	 *
	 * @type {Date}
	 */
	var currentDate = exports.currentDate = new Date();
	
	/**
	 *
	 * @param {Date} date
	 * @return {Date}
	 */
	function normalizeDateToDay(date) {
	  var d = new Date(date);
	  d.setHours(0);
	  d.setMinutes(0);
	  d.setSeconds(0);
	  d.setMilliseconds(0);
	  return d;
	}
	/**
	 *
	 * @param {Date} currentDate
	 * @param {Date} selectDate
	 * @return {boolean}
	 */
	function isOneDay(currentDate, selectDate) {
	  if (!selectDate || !(selectDate instanceof Date) || !currentDate || !(currentDate instanceof Date)) {
	    return false;
	  }
	  return selectDate.getMonth() == currentDate.getMonth() && selectDate.getFullYear() == currentDate.getFullYear() && selectDate.getDate() == currentDate.getDate();
	}
	/**
	 *
	 * @param {Date} currentDate
	 * @param {Date} minDate
	 * @return {boolean}
	 */
	function smallerThenDay(currentDate, minDate) {
	
	  if (!currentDate || !minDate || !(currentDate instanceof Date) || !(minDate instanceof Date)) return false;
	
	  return normalizeDateToDay(currentDate) < normalizeDateToDay(minDate);
	}
	
	function biggerThenDay(currentDate, maxDate) {
	  if (!currentDate || !maxDate || !(currentDate instanceof Date) || !(maxDate instanceof Date)) return false;
	
	  return normalizeDateToDay(currentDate) > normalizeDateToDay(maxDate);
	}
	/**
	 *
	 * @param {Date} date
	 * @return {Date}
	 */
	function nextDay(date) {
	  if (!date || !(date instanceof Date)) return date;
	  return addDays(date, 1);
	}
	/**
	 *
	 * @param {Date} date
	 * @param {number} days
	 * @return {Date}
	 */
	function addDays(date, days) {
	  if (!date || !(date instanceof Date)) return date;
	  return new Date(new Date(date).setDate(date.getDate() + days));
	}
	
	/**
	 *
	 * @param {Date} currentDate
	 * @param {Date} minDate
	 * @param {Date} maxDate
	 * @return {boolean}
	 */
	function isDisabledDay(currentDate, minDate, maxDate) {
	  if (!currentDate || !(currentDate instanceof Date)) return true;
	
	  if (smallerThenDay(currentDate, minDate)) return true;
	
	  return biggerThenDay(currentDate, maxDate);
	}
	/**
	 *
	 * @param {Date} currentDate
	 * @param {Date} minDate
	 * @param {Date} maxDate
	 * @return {Date}
	 */
	
	function setDateInRange(currentDate, minDate, maxDate) {
	  if (!currentDate || !(currentDate instanceof Date)) return currentDate;
	
	  if (smallerThenDay(currentDate, minDate)) return new Date(minDate);
	
	  if (biggerThenDay(currentDate, maxDate)) return new Date(maxDate);
	
	  return currentDate;
	}
	/**
	 *
	 * @param {Date} currentDate
	 * @param {Date} minDate
	 * @param {Date} maxDate
	 * @return {number}
	 */
	function inDateRange(currentDate, minDate, maxDate) {
	  if (!minDate || !(minDate instanceof Date) || !maxDate || !(maxDate instanceof Date) || isOneDay(minDate, maxDate)) return -1; // Range не задан
	  if (isOneDay(currentDate, minDate)) return 1; // старт
	  if (isOneDay(currentDate, maxDate)) return 3; // конец
	  if (smallerThenDay(currentDate, maxDate) && biggerThenDay(currentDate, minDate)) return 2; // посередине
	  return 0;
	}
	
	/**
	 *
	 * @param {Date} currentDate
	 * @param {Date} withDate
	 * @return {boolean}
	 */
	function isOneDayOrNotExist(currentDate, withDate) {
	  if (!currentDate || !(currentDate instanceof Date)) return false;
	  if (!withDate || !(withDate instanceof Date)) return true;
	  return isOneDay(currentDate, withDate);
	}
	/**
	 * @type {Array.<{year:number,month:number}>}
	 */
	var monthList = exports.monthList = function () {
	  var arrMonth = [{
	    month: currentDate.getMonth(),
	    year: currentDate.getFullYear()
	  }];
	
	  var date = new Date(currentDate);
	
	  for (var i = 0; i < 9; i++) {
	    date.setMonth(date.getMonth() + 1);
	    arrMonth.push({
	      month: date.getMonth(),
	      year: date.getFullYear()
	    });
	  }
	
	  return arrMonth;
	}();
	
	/**
	 * @function
	 * @param {Array} [results]
	 * @return {{month: number, year: number, price: string, [prices]: number[]}[]}
	 */
	var generateMonthList = exports.generateMonthList = function () {
	
	  function sa(numbers) {
	    var len = numbers.length;
	    if (len == 0) return 0;
	    var sum = 0;
	    for (var i = 0; i < len; i++) {
	      sum += +numbers[i];
	    }
	    return Math.abs(sum / len);
	  }
	
	  /**
	   *
	   * @param {number} price
	   * @param {number} low
	   * @param {number} middle
	   * @param {number} high
	   * @param {Array<*>} [returns=[0, 1, 2]]
	   * @return {*}
	   */
	  function priceIN(price, low, middle, high) {
	    var returns = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : [0, 1, 2];
	
	    var delLowMiddle = low + (middle - low) / 2,
	        delMiddleHigh = middle + (high - middle) / 2;
	
	    if (price < delLowMiddle) return returns[0];else if (price < delMiddleHigh) return returns[1];else return returns[2];
	  }
	
	  /**
	   *
	   * @param {Array} results
	   * @param {{month: number, year: number, price: string, [prices]: number[]}[]} monthList
	   * @return {*}
	   */
	  function arrayMonthFilter(results, monthList) {
	    if (!results) return monthList;
	
	    var temp = {};
	
	    var prices = results.map(function (item) {
	      var date = (0, _Format.dateFromString)(item.departDate),
	          year = date.getFullYear(),
	          month = date.getMonth();
	
	      if (!temp[year]) temp[year] = {};
	      if (!temp[year][month]) temp[year][month] = { prices: [] };
	
	      temp[year][month].prices.push(item.price);
	
	      return item.price;
	    });
	
	    var low_price = Math.min.apply(Math, prices);
	    var high_price = Math.max.apply(Math, prices);
	    var middle_price = sa(prices);
	
	    monthList = monthList.map(function (item) {
	      if (temp[item.year] && temp[item.year][item.month]) {
	        item.price = priceIN(sa(temp[item.year][item.month].prices), low_price, middle_price, high_price, ['low', 'middle', 'high']);
	      }
	      item.prices = [low_price, middle_price, high_price];
	      return item;
	    });
	
	    return monthList;
	  }
	
	  return function (results) {
	
	    var date = new Date();
	
	    date.setDate(1);
	    /**
	     *
	     * @type {{month:number, year:number, price:string, [prices]: number[]}[]}
	     */
	    var monthList = [{
	      month: date.getMonth(),
	      year: date.getFullYear(),
	      price: 'none'
	    }];
	
	    for (var i = 0; i < 9; i++) {
	      date.setMonth(date.getMonth() + 1);
	      monthList.push({
	        month: date.getMonth(),
	        year: date.getFullYear(),
	        price: 'none'
	      });
	    }
	
	    monthList = arrayMonthFilter(results, monthList);
	
	    return monthList;
	  };
	}();
	
	function days_between(date1, date2) {
	
	  // The number of milliseconds in one day
	  var ONE_DAY = 1000 * 60 * 60 * 24;
	
	  // Convert both dates to milliseconds
	  var date1_ms = date1.getTime();
	  var date2_ms = date2.getTime();
	
	  // Calculate the difference in milliseconds
	  var difference_ms = Math.abs(date1_ms - date2_ms);
	
	  // Convert back to days and return
	  return Math.round(difference_ms / ONE_DAY);
	}
	
	// export const monthly = (function () {
	//   let currentDate = new Date()
	//     , date = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1)
	//     , months = [];
	//
	//   for (let i = 0; i < 10; i++) {
	//     months.push({
	//       year  : date.getFullYear(),
	//       month : date.getMonth()
	//     });
	//     date.setMonth(date.getMonth() + 1);
	//   }
	//
	//   return months;
	// }());
	
	exports["default"] = {
	  normalizeDateToDay: normalizeDateToDay,
	  isOneDay: isOneDay,
	  isOneDayOrNotExist: isOneDayOrNotExist,
	  smallerThenDay: smallerThenDay,
	  biggerThenDay: biggerThenDay,
	  nextDay: nextDay,
	  addDays: addDays,
	  isDisabledDay: isDisabledDay,
	  setDateInRange: setDateInRange,
	  inDateRange: inDateRange
	};

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.Store = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Prices = __webpack_require__(42);
	
	var _Prices2 = _interopRequireDefault(_Prices);
	
	var _Request = __webpack_require__(43);
	
	var _Request2 = _interopRequireDefault(_Request);
	
	var _Response = __webpack_require__(44);
	
	var _Response2 = _interopRequireDefault(_Response);
	
	var _Date = __webpack_require__(40);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 *
	 * @typedef {Object}                    RequestOptions
	 * @property {number}                     [count]
	 * @property {boolean}                    [monthly]
	 * @property {Object.<string, number>}    [months]
	 * @property {string[]}                   [fromDates]
	 * @property {string[]}                   [done]
	 */
	var Store = exports.Store = function () {
	  function Store() {
	    _classCallCheck(this, Store);
	
	    this._id = 'store_' + Date.now();
	    /**
	     *
	     * @type {{prices: (Object.<string,{request:Request,store:StorePrices}>), responses: (Object.<string,Response>)}}
	     * @private
	     */
	    this._storage = {
	      prices: {},
	      responses: {}
	    };
	  }
	
	  /**
	   *
	   * @param {string} key
	   * @return {{request: Request, store: StorePrices}}
	   * @private
	   */
	
	
	  _createClass(Store, [{
	    key: '__getStore',
	    value: function __getStore(key) {
	      if (!this._storage.prices.hasOwnProperty(key)) this._storage.prices[key] = {
	        request: new _Request2["default"](),
	        store: new _Prices2["default"]()
	      };
	      return this._storage.prices[key];
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {Request}
	     */
	
	  }, {
	    key: 'getRequest',
	    value: function getRequest(query) {
	      var tempStore = this.__getStore(query.$key);
	      return tempStore.request;
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @param responseData
	     * @return {*}
	     */
	
	  }, {
	    key: 'addData',
	    value: function addData(query, responseData) {
	      return this.addResponse(query, _Response2["default"].create(responseData));
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @param {Response} response
	     */
	
	  }, {
	    key: 'addResponse',
	    value: function addResponse(query, response) {
	      var keyQuery = query.$key,
	          keyResponse = response.$key;
	
	      var departureDate = response.$departure,
	          departureYear = departureDate.getFullYear(),
	          departureMonth = departureDate.getMonth();
	
	      // проверяем находится ли даты отлета в промежутке "открытых месяцев"
	      if (!_Date.monthList.some(function ( /* {year:number,month:number} */item) {
	        return item.year == departureYear && item.month == departureMonth;
	      })) {
	        return;
	      }
	
	      var tempStore = this.__getStore(keyQuery);
	      // проверяем на количество пересадок
	      if (query.$countFlares < response.$countFlares) {
	        // let newQuery = query.clone({transfer})
	
	        return this;
	      }
	
	      this._storage.responses[keyResponse] = response;
	
	      var args = [response.$key, response.$price];
	      tempStore.store.addKey.apply(tempStore.store, args.concat(response.$indexes));
	
	      return this;
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {StorePrices}
	     */
	
	  }, {
	    key: 'getPrices',
	    value: function getPrices(query) {
	      var key = query.$key,
	          prices = void 0;
	      // console.log(key);
	      var tempStore = this.__getStore(key);
	      if (!query.$isComplete && query.$departure) {
	        // если есть дата отправления но запрос не полный
	        // то показываем прайсы с даты отправления
	        if (query.$inMonth) {
	          var month = query.$month;
	          // с даты отаравления до текущего месяца
	          prices = tempStore.store.getBy(query.$departureYear + '_' + query.$departureMonth, '' + query.$departureDay, month.year + '_' + month.month);
	        } else {
	          // console.log('!query.complete && query.departure', 'else query.inMonth');
	          prices = tempStore.store.getBy(query.$departureYear + '_' + query.$departureMonth, '' + query.$departureDay);
	        }
	      } else {
	        // console.log('else !query.complete && query.departure');
	        // если запрос полный (выбрвно все что нужно) или наоборот без дат
	        // возвращаем все прайсы или прайсы за месяц
	        if (query.$inMonth) {
	          var _month = query.$month;
	          prices = tempStore.store.getBy(_month.year + '_' + _month.month);
	        } else {
	          prices = tempStore.store;
	        }
	      }
	
	      return prices || new _Prices2["default"]();
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {StorePrices}
	     */
	
	  }, {
	    key: 'getClosestPrices',
	    value: function getClosestPrices(query) {
	      var key = query.$key,
	          prices = void 0;
	      /**
	       *
	       * @type {{request: Request, store: StorePrices}}
	       */
	      var tempStore = this.__getStore(key);
	
	      if (query.$departure) {
	        if (query.$return) {
	          prices = tempStore.store.getByOrThis(query.$departureYear + '_' + query.$departureMonth, '' + query.$departureDay, query.$returnYear + '_' + query.$returnMonth, '' + query.$returnDay);
	        } else {
	          prices = tempStore.store.getByOrThis(query.$departureYear + '_' + query.$departureMonth, '' + query.$departureDay);
	        }
	      } else {
	        prices = tempStore.store;
	      }
	
	      return prices || new _Prices2["default"]();
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {StorePrices}
	     */
	
	  }, {
	    key: 'getRealPrices',
	    value: function getRealPrices(query) {
	      var key = query.$key,
	          prices = void 0;
	      /**
	       *
	       * @type {{request: Request, store: StorePrices}}
	       */
	      var tempStore = this.__getStore(key);
	
	      if (query.$departure) {
	        if (query.$return) {
	          prices = tempStore.store.getBy(query.$departureYear + '_' + query.$departureMonth, '' + query.$departureDay, query.$returnYear + '_' + query.$returnMonth, '' + query.$returnDay);
	        } else {
	          prices = tempStore.store.getBy(query.$departureYear + '_' + query.$departureMonth, '' + query.$departureDay);
	        }
	      } else {
	        prices = tempStore.store;
	      }
	
	      return prices || new _Prices2["default"]();
	    }
	
	    /**
	     *
	     * @param {Query} query
	     * @return {Array.<Response>}
	     */
	
	  }, {
	    key: 'getResponse',
	    value: function getResponse(query) {
	      var key = query.$key,
	          indexes = [],
	          globalIndexes = [];
	      if (query.$departure) {
	        indexes.push(query.$departureYear + '_' + query.$departureMonth, '' + query.$departureDay);
	        globalIndexes.push(query.$departureYear + '_' + query.$departureMonth, '' + query.$departureDay);
	        if (query.$return) {
	          indexes.push(query.$returnYear + '_' + query.$returnMonth, '' + query.$returnDay);
	          globalIndexes.push(query.$returnYear + '_' + query.$returnMonth, '' + query.$returnDay);
	        } else {
	          if (query.$isRoundTrip && query.$inMonth) {
	            var month = query.$month;
	            indexes.push(month.year + '_' + month.month);
	          }
	        }
	      } else {
	        if (query.$inMonth) {
	          var _month2 = query.$month;
	          indexes.push(_month2.year + '_' + _month2.month);
	        }
	      }
	      /**
	       *
	       * @type {StorePrices}
	       */
	      var mainStore = this.__getStore(key).store,
	          priceStore = mainStore.getBy.apply(mainStore, indexes),
	          globalStore = mainStore.getBy.apply(mainStore, globalIndexes);
	
	      if (query.$inMonth && !priceStore) {
	        return [];
	      }
	
	      if (priceStore) {
	        var keys = priceStore.$keys;
	        if (!key || key.length == 0) keys = globalStore.$keys;
	
	        return keys.map(function (responseKey) {
	          return this._storage.responses[responseKey] || null;
	        }, this).filter(function (response) {
	          return !!response;
	        });
	      } else if (globalStore) {
	        return globalStore.$keys.map(function (responseKey) {
	          return this._storage.responses[responseKey] || null;
	        }, this).filter(function (response) {
	          return !!response;
	        });
	      }
	      return [];
	    }
	
	    /**
	     *
	     * @param {Query} query
	     */
	
	  }, {
	    key: 'getCities',
	    value: function getCities(query) {
	      /**
	       *
	       * @type {StorePrices}
	       */
	      var resultPrices = this.getRealPrices(query);
	      var resultKeys = resultPrices.$keys;
	      /**
	       *
	       * @type {StorePrices}
	       */
	      var closestPrice = this.getClosestPrices(query);
	      /**
	       *
	       * @type {StorePrices}
	       */
	      var globalPrices = closestPrice.$global;
	      /**
	       *
	       * @type {Array.<string>}
	       */
	      var globalKeys = globalPrices.$keys;
	
	      var globalCities = {},
	          cities = [];
	
	      resultKeys.forEach(function (responseKey) {
	        if (this._storage.responses[responseKey]) {
	          var city_iata = this._storage.responses[responseKey].$to;
	          if (cities.indexOf(city_iata) < 0) cities.push(city_iata);
	        }
	      }.bind(this));
	
	      if (cities.length > 2) return [];
	
	      globalKeys.forEach(function (responseKey) {
	        /**
	         * @this {Store}
	         */
	        if (this._storage.responses[responseKey]) {
	          var city_iata = this._storage.responses[responseKey].$to;
	          if (cities.indexOf(city_iata) >= 0) return;
	
	          if (!globalCities[city_iata]) globalCities[city_iata] = 0;
	          globalCities[city_iata]++;
	        }
	      }, this);
	
	      // console.log(resultPrices, closestPrice, globalPrices, globalKeys, globalCities);
	
	      var temp = {};
	      for (var iata in globalCities) {
	        if (!temp[globalCities[iata]]) {
	          temp[globalCities[iata]] = [];
	        }
	        temp[globalCities[iata]].push(iata);
	      }
	
	      var t = [];
	      for (var num in temp) {
	        t.push(temp[num]);
	      }
	
	      t.reverse();
	
	      return Array.prototype.concat.apply([], t);
	    }
	  }]);
	
	  return Store;
	}();
	
	exports["default"] = Store;

/***/ },
/* 42 */
/***/ function(module, exports) {

	'use strict';
	
	/**
	 * @typedef {Object} PriceOptions
	 * @property {string} childType
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var treePriceStore = {
	  global: 'year_month',
	  year_month: 'day',
	  day: 'to_year_month',
	  to_year_month: 'to_day'
	};
	
	var StorePrices = function () {
	  /**
	   *
	   * @param {StorePrices|null} [parent=null]
	   */
	  function StorePrices() {
	    var parent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
	
	    _classCallCheck(this, StorePrices);
	
	    /**
	     *
	     * @type {StorePrices|null}
	     * @private
	     */
	    this._parent = parent;
	    /**
	     *
	     * @type {string}
	     * @private
	     */
	    this._type = parent ? treePriceStore[parent.$type] : 'global';
	    /**
	     *
	     * @type {Array.<number>}
	     * @private
	     */
	    this._prices = [];
	    /**
	     *
	     * @type {Array.<string>}
	     * @private
	     */
	    this._dataKeys = [];
	    /**
	     *
	     * @type {Object.<(number|string),StorePrices>}
	     * @private
	     */
	    this._children = {};
	
	    this._cache = {
	      minPrices: null,
	
	      minLower: null,
	      minMiddle: null,
	      minHigher: null
	    };
	  }
	
	  /**
	   *
	   * @return {StorePrices|null}
	   */
	
	
	  _createClass(StorePrices, [{
	    key: '__clearCache',
	
	
	    // Methods
	
	    value: function __clearCache() {
	      this._cache = {
	        minPrices: null,
	
	        minLower: null,
	        minMiddle: null,
	        minHigher: null
	      };
	      return this;
	    }
	
	    /**
	     *
	     * @param {string} key
	     * @param {number} price
	     * @param {...string|...number} indexes
	     * @return {StorePrices}
	     */
	
	  }, {
	    key: 'addKey',
	    value: function addKey(key, price) {
	      for (var _len = arguments.length, indexes = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
	        indexes[_key - 2] = arguments[_key];
	      }
	
	      this.__addThisKey(key, price).__addChildKey(key, price, indexes);
	      return this;
	    }
	
	    /**
	     *
	     * @param {string} key
	     * @param {number} price
	     * @return {StorePrices}
	     * @private
	     */
	
	  }, {
	    key: '__addThisKey',
	    value: function __addThisKey(key, price) {
	      if (this._dataKeys.indexOf(key) < 0) {
	        this._dataKeys.push(key);
	        this._prices.push(price);
	        this.__clearCache();
	      }
	      return this;
	    }
	
	    /**
	     *
	     * @param {string} key
	     * @param {number} price
	     * @param {string[]|number[]} indexes
	     * @return {StorePrices}
	     * @private
	     */
	
	  }, {
	    key: '__addChildKey',
	    value: function __addChildKey(key, price, indexes) {
	
	      if (indexes.length > 0) {
	        var index = indexes.shift();
	
	        if (!this._children[index]) {
	          this._children[index] = new StorePrices(this);
	        }
	        this._children[index].addKey.apply(this._children[index], [key, price].concat(indexes));
	      }
	
	      return this;
	    }
	
	    /**
	     *
	     * @param indexes
	     * @return {null|StorePrices}
	     */
	
	  }, {
	    key: 'getBy',
	    value: function getBy() {
	      for (var _len2 = arguments.length, indexes = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
	        indexes[_key2] = arguments[_key2];
	      }
	
	      // если нет аргументов возвращаем текущее хранилище
	      if (indexes.length > 0) {
	        var index = indexes.shift();
	        // если есть внутреннее хранилище с индексом возвращаем его
	        if (this._children[index]) {
	          return this._children[index].getBy.apply(this._children[index], indexes);
	        }
	        return null;
	      }
	      return this;
	    }
	
	    /**
	     *
	     * @param indexes
	     * @return {null|StorePrices}
	     */
	
	  }, {
	    key: 'getByOrThis',
	    value: function getByOrThis() {
	      for (var _len3 = arguments.length, indexes = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
	        indexes[_key3] = arguments[_key3];
	      }
	
	      // если нет аргументов возвращаем текущее хранилище
	      if (indexes.length > 0) {
	        var index = indexes.shift();
	        // если есть внутреннее хранилище с индексом возвращаем его
	        if (this._children[index]) {
	          return this._children[index].getByOrThis.apply(this._children[index], indexes);
	        }
	        return this;
	      }
	      return this;
	    }
	  }, {
	    key: '$parent',
	    get: function get() {
	      return this._parent;
	    }
	
	    /**
	     *
	     * @return {StorePrices}
	     */
	
	  }, {
	    key: '$global',
	    get: function get() {
	      if (this._parent) return this._parent.$global;
	      return this;
	    }
	
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: '$type',
	    get: function get() {
	      return this._type;
	    }
	
	    /**
	     *
	     * @return {Array.<numbernull>}
	     */
	
	  }, {
	    key: '$minPrices',
	    get: function get() {
	      if (this._cache.minPrices) return this._cache.minPrices;
	
	      var prices = [];
	      for (var prop in this._children) {
	        if (this._children.hasOwnProperty(prop)) {
	          prices.push(this._children[prop].$minLower);
	        }
	      }
	
	      if (prices.length == 0 && this._prices.length > 0) prices = this._prices;
	
	      this._cache.minPrices = prices;
	      return prices;
	    }
	
	    /**
	     *
	     * @return {number|null}
	     */
	
	  }, {
	    key: '$minLower',
	    get: function get() {
	      if (!this._cache.minLower) this._cache.minLower = Math.min.apply(null, this.$minPrices) || null;
	      return this._cache.minLower;
	    }
	
	    /**
	     *
	     * @return {number|null}
	     */
	
	  }, {
	    key: '$minMiddle',
	    get: function get() {
	      if (!this._cache.minMiddle) {
	        var price = this.$minPrices;
	        if (price.length < 0) return null;
	        this._cache.minMiddle = price.reduce(function (sa, num) {
	          return sa + num;
	        }, 0) / price.length;
	      }
	      return this._cache.minMiddle;
	    }
	
	    /**
	     *
	     * @return {number|null}
	     */
	
	  }, {
	    key: '$minHigher',
	    get: function get() {
	      if (!this._cache.minHigher) this._cache.minHigher = Math.max.apply(null, this.$minPrices) || null;
	      return this._cache.minHigher;
	    }
	
	    /**
	     *
	     * @return {[(number|null)(number|null)(number|null)]}
	     */
	
	  }, {
	    key: '$minRange',
	    get: function get() {
	      return [this.$minLower, this.$minMiddle, this.$minHigher];
	    }
	
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '$delta',
	    get: function get() {
	      if (!this.$parent || this.$parent.$minPrices.length == 0 || this.$minPrices.length == 0) return 0;
	      return (this.$minLower - this.$parent.$minLower) / (this.$parent.$minHigher - this.$parent.$minLower);
	    }
	
	    /**
	     *
	     * @return {Array.<string>}
	     */
	
	  }, {
	    key: '$keys',
	    get: function get() {
	      return this._dataKeys;
	    }
	  }]);
	
	  return StorePrices;
	}();
	
	exports["default"] = StorePrices;

/***/ },
/* 43 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Request = function () {
	  function Request() {
	    _classCallCheck(this, Request);
	
	    /**
	     *
	     * @type {number}
	     * @private
	     */
	    this._count = 0;
	    /**
	     *
	     * @type {boolean}
	     * @private
	     */
	    this._monthly = false;
	    /**
	     *
	     * @type {Object.<string,(undefined|boolean)>}
	     * @private
	     */
	    this._months = {};
	    // this._fromDates = [];
	    /**
	     *
	     * @type {Array.<string>}
	     * @private
	     */
	    this._done = [];
	    /**
	     *
	     * @type {Array.<string>}
	     * @private
	     */
	    this._load = [];
	  }
	
	  /**
	   *
	   * @return {number}
	   */
	
	
	  _createClass(Request, [{
	    key: 'add',
	
	
	    /**
	     *
	     * @return {Request}
	     */
	    value: function add() {
	      this._count++;
	      return this;
	    }
	
	    /**
	     *
	     * @return {Request}
	     */
	
	  }, {
	    key: 'done',
	    value: function done() {
	      this._count--;
	      if (this._count < 0) {
	        this._count = 0;
	      }
	      return this;
	    }
	
	    /**
	     * @param {function} [callback]
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'isFull',
	    value: function isFull(callback) {
	      var temp = this._count == 0;
	      if (temp && callback) callback();
	      return temp;
	    }
	
	    /**
	     *
	     * @param {function} [callback]
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'isMonthly',
	    value: function isMonthly(callback) {
	      var temp = !!this.$monthly;
	      if (temp && callback) callback();
	      return temp;
	    }
	
	    /**
	     *
	     * @return {Request}
	     */
	
	  }, {
	    key: 'initMonthly',
	    value: function initMonthly() {
	      this._monthly = true;
	      return this;
	    }
	
	    /**
	     *
	     * @return {Request}
	     */
	
	  }, {
	    key: 'emptyMonthly',
	    value: function emptyMonthly() {
	      this._monthly = false;
	      return this;
	    }
	
	    /**
	     *
	     * @param {string} key
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'isDoneMonth',
	    value: function isDoneMonth(key) {
	      return !!this._months[key];
	    }
	
	    /**
	     *
	     * @param {string} key
	     * @return {Request}
	     */
	
	  }, {
	    key: 'doneMonth',
	    value: function doneMonth(key) {
	      this._months[key] = true;
	      return this;
	    }
	  }, {
	    key: 'loadRequest',
	    value: function loadRequest(params) {
	      this._load.push(JSON.stringify(params));
	      return this;
	    }
	
	    /**
	     *
	     * @param {Object} params
	     * @return {Request}
	     */
	
	  }, {
	    key: 'doneRequest',
	    value: function doneRequest(params) {
	      /**
	       * @type {string}
	       */
	      var key = JSON.stringify(params);
	      /**
	       *
	       * @type {number}
	       */
	      var index = this._load.indexOf(key);
	      if (index > -1) this._load.splice(index, 1);
	      this._done.push(key);
	      return this;
	    }
	
	    /**
	     *
	     * @param {Object} params
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'isLoadRequest',
	    value: function isLoadRequest(params) {
	      return this._load.indexOf(JSON.stringify(params)) > -1;
	    }
	
	    /**
	     *
	     * @param {Object} params
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'isDoneRequest',
	    value: function isDoneRequest(params) {
	      return this._done.indexOf(JSON.stringify(params)) > -1;
	    }
	  }, {
	    key: '$count',
	    get: function get() {
	      return this._count;
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: '$monthly',
	    get: function get() {
	      return this._monthly;
	    }
	  }]);
	
	  return Request;
	}();
	
	exports["default"] = Request;

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Format = __webpack_require__(17);
	
	var _Format2 = _interopRequireDefault(_Format);
	
	var _OneTwoTrip = __webpack_require__(23);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Response = function () {
	  /**
	   *
	   * @param {TripOptions} data_bestDealsResponse
	   */
	  function Response(data_bestDealsResponse) {
	    _classCallCheck(this, Response);
	
	    /**
	     *
	     * @type {TripOptions}
	     * @private
	     */
	    this._data = data_bestDealsResponse;
	  }
	
	  /**
	   *
	   * @return {string}
	   */
	
	
	  _createClass(Response, [{
	    key: '$from',
	    get: function get() {
	      return this._data.from;
	    }
	
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: '$to',
	    get: function get() {
	      return this._data.to;
	    }
	
	    /**
	     *
	     * @return {?Date}
	     */
	
	  }, {
	    key: '$departure',
	    get: function get() {
	      return _Format2["default"].query.bestDeals.stringToDate(this._data.departDate);
	    }
	
	    /**
	     *
	     * @return {?Date}
	     */
	
	  }, {
	    key: '$return',
	    get: function get() {
	      return _Format2["default"].query.bestDeals.stringToDate(this._data.returnDate);
	    }
	
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '$duration',
	    get: function get() {
	      return (0, _Format.dateDurations)(this.$departure, this.$return);
	    }
	
	    /**
	     *
	     * @return {Array.<DirectionOptions>[]}
	     */
	
	  }, {
	    key: '$directions',
	    get: function get() {
	      return this._data.directions;
	    }
	
	    /**
	     *
	     * @return {Array.<stringnumber>}
	     */
	
	  }, {
	    key: '$indexes',
	    get: function get() {
	      var departureDate = this.$departure,
	          returnDate = this.$return;
	      var indexes = [departureDate.getFullYear() + '_' + departureDate.getMonth(), '' + departureDate.getDate()];
	      if (departureDate != returnDate) return indexes.concat([returnDate.getFullYear() + '_' + returnDate.getMonth(), '' + returnDate.getDate()]);
	      return indexes;
	    }
	
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '$price',
	    get: function get() {
	      return this._data.price;
	    }
	
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: '$currency',
	    get: function get() {
	      return this._data.currency;
	    }
	
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '$countFlares',
	    get: function get() {
	      return Math.max(this.$countFlaresTo, this.$countFlaresBack);
	    }
	
	    /**
	     *
	     * @return {Number}
	     */
	
	  }, {
	    key: '$countFlaresTo',
	    get: function get() {
	      return this._data.directions[0].length;
	    }
	
	    /**
	     *
	     * @return {Number}
	     */
	
	  }, {
	    key: '$countFlaresBack',
	    get: function get() {
	      return this._data.directions[1] ? this._data.directions[1].length : 0;
	    }
	
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: '$key',
	    get: function get() {
	      return this.$from + '_' + this.$to + '_' + this._data.departDate + '_' + this._data.returnDate + this.$price;
	    }
	
	    // CreateStatic
	
	    /**
	     *
	     * @param {SearchOptions|TripOptions} options
	     * @return {Response}
	     */
	
	  }, {
	    key: '$route',
	
	
	    /**
	     *
	     * @return {string}
	     */
	    get: function get() {
	      var depDate = this.$departure;
	      var retDate = this.$return;
	
	      var str = '';
	
	      if (depDate) {
	        var month = depDate.getMonth() + 1,
	            day = depDate.getDate();
	        str += '' + (day < 10 ? '0' + day : day) + (month < 10 ? '0' + month : month);
	
	        str += '' + this.$from + this.$to;
	
	        if (retDate) {
	          var _month = retDate.getMonth() + 1,
	              _day = retDate.getDate();
	          str += '' + (_day < 10 ? '0' + _day : _day) + (_month < 10 ? '0' + _month : _month);
	        }
	      }
	
	      return str;
	    }
	  }], [{
	    key: 'create',
	    value: function create(options) {
	      if (options.dirs) return Response.createFromSearch(options);
	      return Response.createFromBestDeals(options);
	    }
	
	    /**
	     *
	     * @param {TripOptions} bestDealsResponseData
	     * @return {Response}
	     */
	
	  }, {
	    key: 'createFromBestDeals',
	    value: function createFromBestDeals(bestDealsResponseData) {
	      return new Response(bestDealsResponseData);
	    }
	
	    /**
	     *
	     * @param {SearchOptions} searchResponseData
	     * @return {Response}
	     */
	
	  }, {
	    key: 'createFromSearch',
	    value: function createFromSearch(searchResponseData) {
	      return new Response((0, _OneTwoTrip.convertSearchFareToBestDeal)(searchResponseData));
	    }
	  }]);
	
	  return Response;
	}();
	
	exports["default"] = Response;

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	/*eslint no-unused-vars: ["error", { "args": "none" }]*/
	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Controller2 = __webpack_require__(15);
	
	var _Controller3 = _interopRequireDefault(_Controller2);
	
	var _Query = __webpack_require__(16);
	
	var _Query2 = _interopRequireDefault(_Query);
	
	var _ControllerResults = __webpack_require__(46);
	
	var _ControllerResults2 = _interopRequireDefault(_ControllerResults);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var DemoController = function (_Controller) {
	  _inherits(DemoController, _Controller);
	
	  /**
	   *
	   * @param {EventStore} eventStore
	   */
	  function DemoController(eventStore) {
	    _classCallCheck(this, DemoController);
	
	    /**
	     *
	     * @type {EventStore}
	     * @private
	     */
	    var _this = _possibleConstructorReturn(this, (DemoController.__proto__ || Object.getPrototypeOf(DemoController)).call(this, eventStore));
	
	    _this._eventStore = eventStore;
	    return _this;
	  }
	
	  /**
	   *
	   * @return {DemoController}
	   * @private
	   */
	
	
	  _createClass(DemoController, [{
	    key: '__init',
	    value: function __init() {
	      this._results = new _ControllerResults2["default"](this._eventStore);
	      return this;
	    }
	
	    /**
	     *
	     * @private
	     */
	
	  }, {
	    key: '__updateResults',
	    value: function __updateResults() {
	      var query = new _Query2["default"](this._formData);
	
	      this._eventStore.dispatch('Controller::show:results', this._results.show(query));
	    }
	
	    /**
	     *
	     * @return {Controller}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      var _this2 = this;
	
	      this._eventStore.on(['Form::change'], function (event) {
	        _this2._formData = event.detail;
	        _this2.__updateResults();
	      });
	
	      return this;
	    }
	  }]);
	
	  return DemoController;
	}(_Controller3["default"]);
	
	exports["default"] = DemoController;

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	/*eslint no-unused-vars: ["error", { "args": "none" }]*/
	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _demo = __webpack_require__(39);
	
	var _Results = __webpack_require__(22);
	
	var _Results2 = _interopRequireDefault(_Results);
	
	var _Response = __webpack_require__(44);
	
	var _Response2 = _interopRequireDefault(_Response);
	
	var _StorePrices = __webpack_require__(47);
	
	var _StorePrices2 = _interopRequireDefault(_StorePrices);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var DemoControllerResults = function (_ControllerResults) {
	  _inherits(DemoControllerResults, _ControllerResults);
	
	  function DemoControllerResults() {
	    _classCallCheck(this, DemoControllerResults);
	
	    var _this = _possibleConstructorReturn(this, (DemoControllerResults.__proto__ || Object.getPrototypeOf(DemoControllerResults)).call(this, null));
	
	    _this._store = null;
	    return _this;
	  }
	
	  /**
	   *
	   * @param {Query} query
	   * @return {ResultsShow}
	   */
	
	
	  _createClass(DemoControllerResults, [{
	    key: 'show',
	    value: function show(query) {
	      /**
	       *
	       * @type {Array.<Response>}
	       */
	      var results = _demo.DEALS.map(function (data) {
	        return new _Response2["default"](data);
	      });
	      return {
	        loading: false,
	        store: {},
	        query: {},
	        price: new _StorePrices2["default"](),
	        results: results
	      };
	    }
	  }]);
	
	  return DemoControllerResults;
	}(_Results2["default"]);
	
	exports["default"] = DemoControllerResults;

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	/*eslint no-unused-vars: ["error", { "args": "none" }]*/
	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Prices = __webpack_require__(42);
	
	var _Prices2 = _interopRequireDefault(_Prices);
	
	var _demo = __webpack_require__(39);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var DemoStorePrices = function (_StorePrices) {
	  _inherits(DemoStorePrices, _StorePrices);
	
	  function DemoStorePrices() {
	    _classCallCheck(this, DemoStorePrices);
	
	    return _possibleConstructorReturn(this, (DemoStorePrices.__proto__ || Object.getPrototypeOf(DemoStorePrices)).call(this, null));
	  }
	
	  /**
	   *
	   * @return {[(number|null),(number|null),(number|null)]}
	   */
	
	
	  _createClass(DemoStorePrices, [{
	    key: 'addKey',
	
	
	    /**
	     *
	     * @param {string} key
	     * @param {number} price
	     * @param {...string|...number} indexes
	     * @return {StorePrices}
	     */
	    value: function addKey() {
	      return this;
	    }
	
	    /**
	     *
	     * @param indexes
	     * @return {DemoStorePrices}
	     */
	
	  }, {
	    key: 'getBy',
	    value: function getBy() {
	      return this;
	    }
	
	    /**
	     *
	     * @param indexes
	     * @return {DemoStorePrices}
	     */
	
	  }, {
	    key: 'getByOrThis',
	    value: function getByOrThis() {
	      return this;
	    }
	  }, {
	    key: '$minRange',
	    get: function get() {
	      return _demo.DEALS.filter(function (data, index) {
	        return index < 3;
	      }).map(function (data) {
	        return data.price;
	      });
	    }
	
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '$delta',
	    get: function get() {
	      return Math.random();
	    }
	
	    /**
	     *
	     * @return {Array.<string>}
	     */
	
	  }, {
	    key: '$keys',
	    get: function get() {
	      return [];
	    }
	  }]);
	
	  return DemoStorePrices;
	}(_Prices2["default"]);
	
	exports["default"] = DemoStorePrices;

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _classList = __webpack_require__(49);
	
	var _classList2 = _interopRequireDefault(_classList);
	
	var _constant = __webpack_require__(51);
	
	var _Color = __webpack_require__(52);
	
	var _Color2 = _interopRequireDefault(_Color);
	
	var _header = __webpack_require__(55);
	
	var _header2 = _interopRequireDefault(_header);
	
	var _footer = __webpack_require__(69);
	
	var _footer2 = _interopRequireDefault(_footer);
	
	var _Deep = __webpack_require__(9);
	
	var _default = __webpack_require__(21);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Form = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {EventStore} eventStore
	   * @param {Theme} theme
	   */
	  function Form(container, eventStore, theme) {
	    _classCallCheck(this, Form);
	
	    this._eventStore = eventStore;
	    this._theme = theme;
	    /**
	     *
	     * @type {HTMLElement}
	     * @private
	     */
	    this._container = container;
	
	    this._query = {};
	    this.initHTML().__initEvents();
	
	    this._value = (0, _Deep.deepExtend)(_default.defaultFormData);
	
	    this._eventStore.dispatch('Form::init', this.$value);
	
	    this.resize();
	  }
	
	  /**
	   *
	   * @return {Form}
	   */
	
	
	  _createClass(Form, [{
	    key: 'initHTML',
	    value: function initHTML() {
	
	      this._node = (0, _Element.docCreate)('div', ['ottbp-form']);
	
	      this._containerHeader = (0, _Element.docCreate)('div', 'ottbp-form__header');
	
	      this._containerFooter = (0, _Element.docCreate)('div', 'ottbp-form__footer');
	
	      this._header = new _header2["default"](this._containerHeader, this._eventStore);
	      this._footer = new _footer2["default"](this._containerFooter, this._eventStore, this._theme);
	
	      this._node.appendChild(this._containerHeader);
	      this._node.appendChild(this._containerFooter);
	
	      this._container.appendChild(this._node);
	
	      return this;
	    }
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      var _this = this;
	
	      var hovered = null;
	
	      function hover(target) {
	        if (hovered == target) return;
	        if (hovered) unHover();
	
	        hovered = target;
	        var back = target.style.backgroundColor;
	        target.setAttribute('data-back', back);
	        target.setAttribute('style', 'background-color: ' + _Color2["default"].shadeRGBString(back, -.10) + ' !important;');
	      }
	
	      function unHover() {
	        if (hovered) {
	          hovered.setAttribute('style', 'background-color: ' + hovered.getAttribute('data-back') + ' !important;');
	        }
	        hovered = null;
	      }
	
	      document.addEventListener('mouseover', function (event) {
	        var target = event.target;
	
	        while (target && target !== document.body && target !== _this._node) {
	          if ((0, _classList2["default"])(target).contains('ottbp-js-hover')) {
	            return hover(target);
	          }
	          target = target.parentNode;
	        }
	        unHover();
	      });
	
	      this._eventStore.on('Control::set', function (event) {
	        var target = event.detail.target;
	
	        var targets = Array.prototype.slice.call(_this._node.querySelectorAll('input.ottbp-input__field, button.ottbp-select__btn'));
	
	        targets.forEach(function (tr, index) {
	          if (tr == target && targets[index + 1]) {
	            targets[index + 1].focus();
	          }
	        });
	      });
	
	      var date = 0;
	
	      this._eventStore.on(['Inputs::change', 'Selects::change', 'DatePicker::changed', 'DateSelect::view:months', 'DateSelect::view:calendar'], function () {
	        // deepExtend(this._value, event.detail);
	        var temp = Date.now();
	        date = temp;
	        setTimeout(function () {
	          if (temp == date) {
	            _this._eventStore.dispatch('Form::change', _this.$value);
	          }
	        }, 100);
	      });
	
	      // this._eventStore.on('Inputs::change', ::this.__whenInputsChange);
	      // this._eventStore.on('Selects::change', ::this.__whenSelectorsChange);
	
	      this._eventStore.on('Window::resize', this.resize.bind(this));
	      this._eventStore.on('Form::update:to:city', function (event) {
	        _this.update({ inputs: { to: event.detail.value } });
	      });
	
	      return this;
	    }
	  }, {
	    key: 'resize',
	    value: function resize() {
	      var containerWidth = this._container.offsetWidth;
	
	      var classes = ['ottbp-big', 'ottbp-more-big', 'ottbp-medium', 'ottbp-more-medium', 'ottbp-small', 'ottbp-more-small', 'ottbp-extrasmall', 'ottbp-more-extrasmall'];
	
	      if (containerWidth >= _constant.CONTAINER_SIZE_BIG) {
	        (0, _Element.classAddRemove)(this._node, ['ottbp-big', 'ottbp-more-extrasmall', 'ottbp-more-small', 'ottbp-more-medium', 'ottbp-more-big'], classes);
	      } else if (containerWidth >= _constant.CONTAINER_SIZE_MEDIUM) {
	        (0, _Element.classAddRemove)(this._node, ['ottbp-medium', 'ottbp-more-extrasmall', 'ottbp-more-small', 'ottbp-more-medium'], classes);
	      } else if (containerWidth >= _constant.CONTAINER_SIZE_SMALL) {
	        (0, _Element.classAddRemove)(this._node, ['ottbp-small', 'ottbp-more-extrasmall', 'ottbp-more-small'], classes);
	      } else if (containerWidth >= _constant.CONTAINER_SIZE_EXTRA_SMALL) {
	        (0, _Element.classAddRemove)(this._node, ['ottbp-extrasmall', 'ottbp-more-extrasmall'], classes);
	      } else {
	        (0, _Element.classAddRemove)(this._node, [], classes);
	      }
	
	      this._header.resize(containerWidth);
	      this._footer.resize(containerWidth);
	    }
	
	    /**
	     *
	     * @param {FormatMainForm} options
	     */
	
	  }, {
	    key: 'update',
	    value: function update(options) {
	      /**
	       * @type {FormatMainForm}
	       */
	      var newData = (0, _Deep.deepExtend)(this.$value, options);
	
	      this._header.update(newData);
	      this._footer.update(newData);
	    }
	
	    /**
	     * @return {FormatMainForm}
	     */
	
	  }, {
	    key: '$value',
	    get: function get() {
	      return (0, _Deep.deepExtend)({}, this._header.$value, this._footer.$value);
	    }
	  }]);
	
	  return Form;
	}();
	
	exports["default"] = Form;

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	// IE9 SUPPORT
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _String = __webpack_require__(50);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 *
	 * @type {Function}
	 * @param {HTMLElement} el
	 * @return {ClassList|{}|DOMTokenList}
	 */
	var classList = function () {
	  if (!('classList' in document.createElement('_')) || document.createElementNS && !('classList' in document.createElementNS('http://www.w3.org/2000/svg', 'g'))) {
	    return function (el) {
	      return new ClassList(el);
	    };
	  } else {
	    return function (el) {
	      if (!el.classList) return new ClassList(null);
	      return el.classList;
	    };
	  }
	}();
	
	exports["default"] = classList;
	
	var ClassList = function () {
	  function ClassList(element) {
	    _classCallCheck(this, ClassList);
	
	    this._element = element;
	
	    if (element && element.getAttribute) {
	
	      var _className = (0, _String.trim)(element.getAttribute('class') || '');
	
	      /**
	       * @type {Array.<string>}
	       * @private
	       */
	      this._classes = _className ? _className.split(/\s+/) : [];
	    } else {
	      this._element = null;
	      this._classes = [];
	    }
	    // @TODO Array[]
	    // for (let i = 0; i < this._classes.length; i++) {
	    //   this[i] = this._classes[i];
	    // }
	    //
	    // this.length = this._classes.length;
	  }
	
	  _createClass(ClassList, [{
	    key: '__update',
	    value: function __update() {
	      if (this._element && this._element.setAttribute) this._element.setAttribute('class', this._classes.join(' '));
	    }
	  }, {
	    key: 'contains',
	    value: function contains(str) {
	      return this._classes.indexOf(str) > -1;
	    }
	  }, {
	    key: 'add',
	    value: function add(str) {
	      if (!this.contains(str)) {
	        this._classes.push(str);
	        this.__update();
	      }
	    }
	  }, {
	    key: 'remove',
	    value: function remove(str) {
	      if (this.contains(str)) {
	        this._classes.splice(this._classes.indexOf(str), 1);
	        this.__update();
	      }
	    }
	
	    // @TODO doesnot use
	    // toggle(str) {
	    //   if (this.contains(str))
	    //     this.remove(str);
	    //   else
	    //     this.add(str);
	    // }
	
	    // @TODO item()
	
	  }]);
	
	  return ClassList;
	}();

/***/ },
/* 50 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.trim = trim;
	function trim(str) {
	  if (typeof str !== 'string') return '';
	
	  if (str.trim) return str.trim();
	  return str.replace(/^\s+|\s+$/g, '');
	}

/***/ },
/* 51 */
/***/ function(module, exports) {

	'use strict';
	/**
	 *
	 * @type {number}
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var CONTAINER_SIZE_EXTRA_SMALL = exports.CONTAINER_SIZE_EXTRA_SMALL = 475,
	    CONTAINER_SIZE_SMALL = exports.CONTAINER_SIZE_SMALL = 560,
	    CONTAINER_SIZE_MEDIUM = exports.CONTAINER_SIZE_MEDIUM = 768,
	    CONTAINER_SIZE_BIG = exports.CONTAINER_SIZE_BIG = 960,
	    CONTAINER_SIZE_LARGE = exports.CONTAINER_SIZE_LARGE = 1200,
	    CONTAINER_SIZE_EXTRA_LARGE = exports.CONTAINER_SIZE_EXTRA_LARGE = 1600;
	
	var CONTAINER_WIDTH = exports.CONTAINER_WIDTH = {
	  extra_small: CONTAINER_SIZE_EXTRA_SMALL,
	  small: CONTAINER_SIZE_SMALL,
	  medium: CONTAINER_SIZE_MEDIUM,
	  big: CONTAINER_SIZE_BIG,
	  large: CONTAINER_SIZE_LARGE,
	  extra_large: CONTAINER_SIZE_EXTRA_LARGE
	};
	
	// export const STYLE_PREFIX = 'ottbp-';

/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.rgbToHex = rgbToHex;
	exports.rgbToString = rgbToString;
	exports.rbgToStringWithAlpha = rbgToStringWithAlpha;
	
	var _mixins = __webpack_require__(53);
	
	var _Gradient = __webpack_require__(54);
	
	var _Gradient2 = _interopRequireDefault(_Gradient);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	/**
	 *
	 * @param {number} r
	 * @param {number} g
	 * @param {number} b
	 * @return {string}
	 */
	function rgbToHex(r, g, b) {
	  return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	}
	
	/**
	 *
	 * @param {number} r
	 * @param {number} g
	 * @param {number} b
	 * @return {string}
	 */
	function rgbToString(r, g, b) {
	  return 'rgb(' + r + ', ' + b + ', ' + b + ')';
	}
	
	/**
	 *
	 * @param {number} r
	 * @param {number} g
	 * @param {number} b
	 * @param {number} alpha
	 * @return {string}
	 */
	function rbgToStringWithAlpha(r, g, b, alpha) {
	  return 'rgba(' + r + ', ' + b + ', ' + b + ', ' + alpha + ')';
	}
	/**
	 *
	 * @param color
	 * @return {{r: Number, b: Number, g: Number}}
	 */
	function parseRGB(color) {
	  var f = color.split(','),
	      R = parseInt(f[0].slice(4)),
	      G = parseInt(f[1]),
	      B = parseInt(f[2]);
	  return {
	    r: R,
	    b: B,
	    g: G
	  };
	}
	
	function shadeRGBString(color, percent) {
	  var f = color.split(','),
	      t = percent < 0 ? 0 : 255,
	      p = percent < 0 ? percent * -1 : percent,
	      R = parseInt(f[0].slice(4)),
	      G = parseInt(f[1]),
	      B = parseInt(f[2]);
	  return 'rgb(' + (Math.round((t - R) * p) + R) + ',' + (Math.round((t - G) * p) + G) + ',' + (Math.round((t - B) * p) + B) + ')';
	}
	
	function shadeRGB(rgb, percent) {
	  var t = percent < 0 ? 0 : 255,
	      p = percent < 0 ? percent * -1 : percent;
	  return 'rgb(' + (Math.round((t - rgb.r) * p) + rgb.r) + ',' + (Math.round((t - rgb.g) * p) + rgb.g) + ',' + (Math.round((t - rgb.b) * p) + rgb.b) + ')';
	}
	/**
	 *
	 * @param colorString
	 * @return {?{r: Number, b: Number, g: Number}}
	 */
	function toRGB(colorString) {
	  if (colorString.r && colorString.b && colorString.g) return colorString;
	  if (typeof colorString === 'string') {
	    if (colorString.indexOf('#') == 0 || colorString.length == 6) return (0, _mixins.hexToRgb)(colorString);else if (colorString.indexOf('rgb') == 0) return parseRGB(colorString);
	  }
	  return null;
	}
	
	function setAlphaToRGB(rgb, alpha) {
	  return 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', ' + alpha + ')';
	}
	
	exports["default"] = {
	
	  toRGB: toRGB,
	  setAlphaToRGB: setAlphaToRGB,
	
	  shadeRGBString: shadeRGBString,
	  shadeRGB: shadeRGB,
	
	  isLight: function isLight(colorString) {
	    var rgb = toRGB(colorString);
	    return 0.213 * rgb.r + 0.715 * rgb.g + 0.072 * rgb.b > 255 / 2;
	  },
	  /**
	   *
	   * @param gradientOptions
	   * @return {Gradient}
	   */
	  gradient: function gradient(gradientOptions) {
	    return new _Gradient2["default"](gradientOptions);
	  },
	  inGradient: function inGradient(gradientOptions, delta) {
	    var gr = new _Gradient2["default"](gradientOptions);
	    return gr.find(delta);
	  }
	};

/***/ },
/* 53 */
/***/ function(module, exports) {

	'use strict';
	
	/**
	 *
	 * @param {string} hexString
	 * @return {{r:number,g:number,b:number}}
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.hexToRgb = hexToRgb;
	exports.betweenRGB = betweenRGB;
	function hexToRgb(hexString) {
	  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexString);
	  return result ? {
	    r: parseInt(result[1], 16),
	    g: parseInt(result[2], 16),
	    b: parseInt(result[3], 16)
	  } : null;
	}
	/**
	 *
	 * @param {number} a
	 * @param {number} b
	 * @param {number} delta
	 * @return {number}
	 * @private
	 */
	function __colorNumberBetween(a, b, delta) {
	  return a + (b - a) * delta;
	}
	
	function betweenRGB(rgbA, rgbB, delta) {
	  return {
	    r: __colorNumberBetween(rgbA.r, rgbB.r, delta),
	    g: __colorNumberBetween(rgbA.g, rgbB.g, delta),
	    b: __colorNumberBetween(rgbA.b, rgbB.b, delta)
	  };
	}

/***/ },
/* 54 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _mixins = __webpack_require__(53);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Gradient = function () {
	  /**
	   *
	   * @param {Array<{range:number,color:string,[rgb]:{r:number,b:number,g:number}}>} options
	   */
	  function Gradient(options) {
	    _classCallCheck(this, Gradient);
	
	    this._options = options.sort(function (a, b) {
	      if (a.range > b.range) return 1;else if (a.range < b.range) return -1;
	      return 0;
	    }).map(function (item) {
	      item.rgb = (0, _mixins.hexToRgb)(item.color);
	      return item;
	    });
	  }
	
	  /**
	   *
	   * @param {number} delta
	   * @return {*}
	   */
	
	
	  _createClass(Gradient, [{
	    key: 'find',
	    value: function find(delta) {
	
	      if (this._options.length == 2) {
	        return (0, _mixins.betweenRGB)(this._options[0].rgb, this._options[1].rgb, delta);
	      }
	
	      var start = this._options[0];
	      var range = this._options[0].range,
	          index = 0;
	
	      while (delta >= range && index < this._options.length - 1) {
	        start = this._options[index];
	        index++;
	        range = this._options[index].range;
	      }
	
	      if (!this._options[index]) {
	        return start.rgb;
	      }
	
	      var end = this._options[index];
	
	      return (0, _mixins.betweenRGB)(start.rgb, end.rgb, (delta - start.range) / (end.range - start.range) || 0);
	    }
	  }]);
	
	  return Gradient;
	}();
	
	exports["default"] = Gradient;

/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _inputs = __webpack_require__(56);
	
	var _inputs2 = _interopRequireDefault(_inputs);
	
	var _selects = __webpack_require__(64);
	
	var _selects2 = _interopRequireDefault(_selects);
	
	var _Element = __webpack_require__(7);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	// import Eve from './../mixins/Event';
	
	var Header = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {EventStore} eventStore
	   */
	  function Header(container, eventStore) {
	    _classCallCheck(this, Header);
	
	    this._eventStore = eventStore;
	    this._container = container;
	    this.__initHTML();
	  }
	
	  /**
	   *
	   * @return {Header}
	   * @private
	   */
	
	
	  _createClass(Header, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	
	      this._node = (0, _Element.docCreate)('div', 'ottbp-form-header');
	      this._containerInputs = (0, _Element.docCreate)('div', 'ottbp-form-header__inputs');
	      this._containerSelects = (0, _Element.docCreate)('div', 'ottbp-form-header__selectors');
	
	      this.__initItems().__initEvents();
	
	      this._node.appendChild(this._containerInputs);
	      this._node.appendChild(this._containerSelects);
	
	      this._container.appendChild(this._node);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Header}
	     * @private
	     */
	
	  }, {
	    key: '__initItems',
	    value: function __initItems() {
	      this._inputs = new _inputs2["default"](this._containerInputs, this._eventStore);
	      this._selectors = new _selects2["default"](this._containerSelects, this._eventStore);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Header}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      //
	      // this._eventStore.on('Inputs::change', ::this.__onFormChange);
	      // this._eventStore.on('Selects::change', ::this.__onFormChange);
	
	      return this;
	    }
	  }, {
	    key: '__onFormChange',
	    value: function __onFormChange() {}
	    // this._eventStore.dispatch('Form::main:update', this.value);
	
	
	    /**
	     *
	     * @param {number} containerWidth
	     */
	
	  }, {
	    key: 'resize',
	    value: function resize(containerWidth) {
	      this._inputs.resize(containerWidth);
	      this._selectors.resize(containerWidth);
	    }
	
	    /**
	     *
	     * @param {FormatMainForm} options
	     */
	
	  }, {
	    key: 'update',
	    value: function update(options) {
	      this._inputs.update(options.inputs);
	      this._selectors.update(options.selectors);
	    }
	
	    /**
	     *
	     * @return {FormatMainForm}
	     */
	
	  }, {
	    key: '$value',
	    get: function get() {
	      return {
	        inputs: this._inputs.$value,
	        selectors: this._selectors.$value
	      };
	    }
	  }]);
	
	  return Header;
	}();
	
	exports["default"] = Header;

/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _classList = __webpack_require__(49);
	
	var _classList2 = _interopRequireDefault(_classList);
	
	var _InputSelect = __webpack_require__(57);
	
	var _InputSelect2 = _interopRequireDefault(_InputSelect);
	
	var _SearchButton = __webpack_require__(62);
	
	var _SearchButton2 = _interopRequireDefault(_SearchButton);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	// import Eve from './../mixins/Event';
	
	var Inputs = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {EventStore} eventStore
	   */
	  function Inputs(container, eventStore) {
	    _classCallCheck(this, Inputs);
	
	    this._eventStore = eventStore;
	    this._container = container;
	    this._options = {};
	    this.__initHTML().__initEvents();
	
	    this._container.appendChild(this._node);
	  }
	
	  /**
	   *
	   * @return {Inputs}
	   */
	
	
	  _createClass(Inputs, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      this._node = (0, _Element.docCreate)('div', 'ottbp-inputs');
	      this._fromNode = (0, _Element.docCreate)('div', 'ottbp-inputs__field ottbp-inputs--from');
	      this._toNode = (0, _Element.docCreate)('div', 'ottbp-inputs__field ottbp-inputs--to');
	      this._searchNode = (0, _Element.docCreate)('div', 'ottbp-inputs__field ottbp-inputs--search');
	
	      this.__initInputs().__initBtn();
	
	      this._node.appendChild(this._fromNode);
	      this._node.appendChild(this._toNode);
	      this._node.appendChild(this._searchNode);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Inputs}
	     * @private
	     */
	
	  }, {
	    key: '__initInputs',
	    value: function __initInputs() {
	      /**
	       *
	       * @type {InputSelect}
	       * @private
	       */
	      this._from = new _InputSelect2["default"](this._fromNode, { placeholderDefault: 'Откуда' }, this._eventStore);
	      /**
	       *
	       * @type {InputSelect}
	       * @private
	       */
	      this._to = new _InputSelect2["default"](this._toNode, {
	        placeholderDefault: 'Куда: город или страна',
	        country: true
	      }, this._eventStore);
	
	      this._from.whenSelect(function (value) {
	        this.__changeData('from', value);
	      }.bind(this));
	
	      this._to.whenSelect(function (value) {
	        this.__changeData('to', value);
	      }.bind(this));
	
	      return this;
	    }
	  }, {
	    key: '__initBtn',
	    value: function __initBtn() {
	      this._btn = new _SearchButton2["default"](this._searchNode, this._eventStore);
	    }
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      var _this = this;
	
	      this._eventStore.on('SearchButton::click', function () {
	        if (_this._to.$empty) {
	          _this._to.setError();
	        }
	        if (_this._from.$empty) {
	          _this._from.setError();
	        }
	      });
	      return this;
	    }
	
	    /**
	     *
	     * @param {string} type
	     * @param {PlaceData} value
	     * @private
	     */
	
	  }, {
	    key: '__changeData',
	    value: function __changeData(type, value) {
	      this._options[type] = value;
	
	      //@TODO удалить type and value
	      this._eventStore.dispatch('Inputs::change', {
	        type: 'inputs',
	        value: this.$value,
	        inputs: this.$value
	      });
	    }
	  }, {
	    key: 'resize',
	    value: function resize(mainWidth) {
	      this.__setStyle(mainWidth);
	    }
	  }, {
	    key: '__setStyle',
	    value: function __setStyle(mainWidth) {
	      if (mainWidth < 488) {
	        (0, _classList2["default"])(this._node).remove('ottbp--big');
	      } else {
	        (0, _classList2["default"])(this._node).add('ottbp--big');
	      }
	    }
	
	    /**
	     *
	     * @param {FormatMainForm_Inputs} options
	     */
	
	  }, {
	    key: 'update',
	    value: function update(options) {
	
	      this._from.update(options.from);
	
	      if (options.to) {
	        this._to.update(options.to, false);
	      } else if (options.toCountry) {
	        this._to.update(options.toCountry, true);
	      }
	    }
	
	    /**
	     *
	     * @return {{from: string|null to: string|null toCountry: string|null}}
	     */
	
	  }, {
	    key: '$value',
	    get: function get() {
	      /**
	       *
	       * @type {PlaceData}
	       */
	      var from = this._from.$value || {},
	          to = this._to.$value || {};
	
	      return {
	        from: from.iata ? from.iata : null,
	        to: to.type == 'country' || !to.iata ? null : to.iata,
	        toCountry: to.type !== 'country' || !to.iata ? null : to.iata
	      };
	    }
	  }]);
	
	  return Inputs;
	}();
	
	exports["default"] = Inputs;

/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _helper = __webpack_require__(58);
	
	var _helper2 = _interopRequireDefault(_helper);
	
	var _classList = __webpack_require__(49);
	
	var _classList2 = _interopRequireDefault(_classList);
	
	var _Reference = __webpack_require__(28);
	
	var _city = __webpack_require__(29);
	
	var _country = __webpack_require__(34);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var InputSelect = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {Object} [options={}]
	   * @param {string} [options.placeholder]
	   * @param {string} [options.placeholderDefault]
	   * @param {string} [options.value]
	   * @param {boolean|undefined} [options.country]
	   * @param {EventStore} eventStore
	   */
	  function InputSelect(container) {
	    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	    var eventStore = arguments[2];
	
	    _classCallCheck(this, InputSelect);
	
	    this._eventStore = eventStore;
	    /**
	     * @type {HTMLElement}
	     */
	    this._container = container;
	    /**
	     *
	     * @type {string}
	     * @private
	     */
	    this._placeholderDefault = options.placeholderDefault || 'Placeholder';
	    /**
	     *
	     * @type {string}
	     * @private
	     */
	    this._placeholder = options.placeholder || '';
	    /**
	     *
	     * @type {string}
	     * @private
	     */
	    this._value = options.value || '';
	    /**
	     *
	     * @type {boolean}
	     * @private
	     */
	    this._country = !!options.country;
	
	    this._data = {};
	
	    this.initHTML();
	  }
	
	  /**
	   *
	   * @return {InputSelect}
	   */
	
	
	  _createClass(InputSelect, [{
	    key: 'initHTML',
	    value: function initHTML() {
	
	      this._node = (0, _Element.docCreate)('div', ['ottbp-control']);
	
	      var fragment = document.createDocumentFragment();
	
	      this.__initPlaceholderHTML(fragment).__initInputHTML(fragment).__initHelperHTML(fragment);
	
	      this._node.appendChild(fragment);
	
	      this._container.appendChild(this._node);
	      return this;
	    }
	  }, {
	    key: '__initHelperHTML',
	    value: function __initHelperHTML(fragment) {
	      var helper = (0, _Element.docCreate)('div', 'ottbp-control__helper');
	      this._helper = new _helper2["default"](helper, {});
	      this._helper.whenHover(this.__onHoverHelperList.bind(this));
	      this._helper.whenSelect(this.__onSelectHelperList.bind(this));
	      fragment.appendChild(helper);
	
	      return this;
	    }
	
	    /**
	     *
	     * @param {DocumentFragment} fragment
	     * @return {InputSelect}
	     * @private
	     */
	
	  }, {
	    key: '__initInputHTML',
	    value: function __initInputHTML(fragment) {
	      var inputWrapper = (0, _Element.docCreate)('div', 'ottbp-control__input'),
	          inputOver = (0, _Element.docCreate)('div', 'ottbp-input');
	      /**
	       *
	       * @type {HTMLInputElement}
	       * @private
	       */
	      this._input = (0, _Element.docCreate)('input', ['ottbp-input__field', 'ottbp-texts--textfield']);
	
	      this._input.setAttribute('type', 'text');
	      this._input.setAttribute('autocomplete', 'off');
	      this._input.setAttribute('placeholder', '');
	
	      this.__initInputEvents();
	
	      inputWrapper.appendChild(inputOver);
	      inputOver.appendChild(this._input);
	
	      fragment.appendChild(inputWrapper);
	
	      return this;
	    }
	  }, {
	    key: '__initInputEvents',
	    value: function __initInputEvents() {
	
	      this._input.addEventListener('input', this.onInput.bind(this));
	      this._input.addEventListener('focus', this.onFocus.bind(this));
	      this._input.addEventListener('blur', this.onBlur.bind(this));
	      this._input.addEventListener('keydown', this.onKeyDown.bind(this));
	
	      return this;
	    }
	  }, {
	    key: 'onKeyDown',
	    value: function onKeyDown(event) {
	      this.clearError();
	      switch (event.keyCode) {
	        //down
	        case 40:
	          event.stopPropagation();
	          event.preventDefault();
	          this._helper.highLight(+1);
	          break;
	        //up
	        case 38:
	          event.stopPropagation();
	          event.preventDefault();
	          this._helper.highLight(-1);
	          break;
	        //enter
	        case 13:
	          /**
	           * Если нажали ентер до того как получили запрос от автокомплита
	           * @type {boolean}
	           * @private
	           */
	          this._entered = true;
	          event.stopPropagation();
	          event.preventDefault();
	          this.__onSelectHelperList(this._helper.$currentItem);
	
	          this._eventStore.dispatch('Control::set', {
	            obj: this,
	            target: this._input
	          });
	
	          break;
	      }
	    }
	  }, {
	    key: '__initInputValue',
	    value: function __initInputValue() {
	      this._input.value = this._value;
	      return this.__initPlaceHolderValue();
	    }
	
	    /**
	     *
	     * @param {PlaceData} item
	     * @return {InputSelect}
	     * @private
	     */
	
	  }, {
	    key: '__placeHolderFromItem',
	    value: function __placeHolderFromItem(item) {
	
	      switch (item.type) {
	        case 'country':
	          this._placeholder = '' + item.country.ru;
	          break;
	        case 'city':
	          this._placeholder = item.city.ru + ',&nbsp;' + item.country.ru;
	      }
	      return this;
	    }
	
	    /**
	     *
	     * @param {PlaceData} item
	     * @private
	     */
	
	  }, {
	    key: '__onHoverHelperList',
	    value: function __onHoverHelperList(item) {
	      var cache = JSON.stringify(item);
	      if (this._cacheHoverItemList == cache) return;
	      this._cacheHoverItemList = cache;
	      this.__placeHolderFromItem(item).__initPlaceHolderValue();
	    }
	
	    /**
	     *
	     * @param {PlaceData} item
	     * @private
	     */
	
	  }, {
	    key: '__onSelectHelperList',
	    value: function __onSelectHelperList(item) {
	      this._cacheHoverItemList = null;
	      if (!item) return;
	
	      this.__setValue(item);
	      this.__placeHolderFromItem(item);
	      this._helper.update({ items: [] });
	      this.hideHelper();
	      this.__initInputValue();
	
	      this.onSelect();
	
	      this._eventStore.dispatch('Control::set', {
	        obj: this,
	        target: this._input
	      });
	    }
	
	    /**
	     *
	     * @param {DocumentFragment} fragment
	     * @return {InputSelect}
	     * @private
	     */
	
	  }, {
	    key: '__initPlaceholderHTML',
	    value: function __initPlaceholderHTML(fragment) {
	      var placeholderWrapper = (0, _Element.docCreate)('div', ['ottbp-control__placeholder']),
	          placeholderOver = (0, _Element.docCreate)('div', ['ottbp-placeholder', 'ottbp-texts--textfield']);
	
	      this._placeholderHidden = (0, _Element.docCreate)('span', 'ottbp-placeholder__hidden');
	      this._placeholderVisible = (0, _Element.docCreate)('span', 'ottbp-placeholder__visible');
	
	      placeholderWrapper.appendChild(placeholderOver);
	      placeholderOver.appendChild(this._placeholderHidden);
	      placeholderOver.appendChild(this._placeholderVisible);
	
	      fragment.appendChild(placeholderWrapper);
	      return this.__initPlaceHolderValue();
	    }
	
	    /**
	     *
	     * @return {InputSelect}
	     * @private
	     */
	
	  }, {
	    key: '__initPlaceHolderValue',
	    value: function __initPlaceHolderValue() {
	      if (!this._value || this._value.length == 0) {
	        this.__setPlaceholder('', this._placeholderDefault);
	        return this;
	      } else {
	        if (this._placeholder && this._value.length > 1) {
	          this.__setPlaceholder(this._value, this._placeholder.slice(this._value.length));
	        } else this.__setPlaceholder('', '');
	      }
	      return this;
	    }
	
	    /**
	     *
	     * @param {string} hidden
	     * @param {string} visible
	     * @return {InputSelect}
	     * @private
	     */
	
	  }, {
	    key: '__setPlaceholder',
	    value: function __setPlaceholder(hidden, visible) {
	
	      if (this._lastPlaceholder == hidden + '/' + visible) return this;
	
	      this._lastPlaceholder = hidden + '/' + visible;
	
	      this._placeholderHidden.innerHTML = hidden;
	      this._placeholderVisible.innerHTML = visible;
	      return this;
	    }
	  }, {
	    key: 'onFocus',
	    value: function onFocus() {
	      this._focused = true;
	      this._entered = false;
	      (0, _classList2["default"])(this._node).add('ottbp-blur');
	    }
	  }, {
	    key: 'onBlur',
	    value: function onBlur() {
	      this._focused = false;
	      setTimeout(function () {
	        (0, _classList2["default"])(this._node).remove('ottbp-blur');
	        if (!this.$value.iata) {
	          this._placeholder = '';
	          this.__initPlaceHolderValue();
	        }
	        this.hideHelper();
	      }.bind(this), 100);
	    }
	  }, {
	    key: 'showHelper',
	    value: function showHelper() {
	      (0, _classList2["default"])(this._node).add('ottbp-helper');
	    }
	  }, {
	    key: 'hideHelper',
	    value: function hideHelper() {
	      (0, _classList2["default"])(this._node).remove('ottbp-helper');
	    }
	  }, {
	    key: 'onInput',
	    value: function onInput() {
	      this._value = this._input.value;
	
	      this._data = {};
	
	      this._placeholder = '';
	
	      if (this._value.length > 1) {
	        if (this._country) {
	          this.__onFind_v2((0, _Reference.getAllByPart)(this._value));
	        } else {
	          this.__onFind_v2((0, _city.byPart)(this._value));
	        } // findPlaceAsync(this._value, this.__onFind.bind(this));
	        this.showHelper();
	      } else {
	        this.hideHelper();
	      }
	
	      this.__initPlaceHolderValue();
	    }
	
	    /**
	     *
	     * @param {PlaceData[]} data
	     * @private
	     */
	
	  }, {
	    key: '__onFind_v2',
	    value: function __onFind_v2(data) {
	
	      if (data) {
	        if (!this._focused) {
	          if (this._entered) {
	            this.__setItem(data[0]);
	          }
	          return;
	        }
	        this._helper.update({ items: data });
	      }
	    }
	
	    /**
	     *
	     * @param {PlaceData} item
	     * @return {InputSelect}
	     * @private
	     */
	
	  }, {
	    key: '__setValue',
	    value: function __setValue(item) {
	      this._data = item;
	      switch (item.type) {
	        case 'country':
	          this._value = item.country.ru;
	          break;
	        case 'city':
	          this._value = item.city.ru;
	          break;
	      }
	      return this;
	    }
	
	    /**
	     *
	     * @param {PlaceData} item
	     * @private
	     */
	
	  }, {
	    key: '__setItem',
	    value: function __setItem(item) {
	      if (!item) return;
	
	      this.__setValue(item);
	      this.__placeHolderFromItem(item);
	      this.__initInputValue();
	
	      this.onSelect();
	
	      // this._eventStore.dispatch('Control::set', {
	      //   obj    : this,
	      //   target : this._input
	      // });
	    }
	  }, {
	    key: 'clean',
	    value: function clean() {
	      this._data = {};
	      this._value = '';
	      this.__initInputValue();
	      this.onSelect();
	    }
	
	    /**
	     * @callback onInputSelect
	     * @param {PlaceData} item
	     */
	    /**
	     *
	     * @param {onInputSelect} callback
	     */
	
	  }, {
	    key: 'whenSelect',
	    value: function whenSelect(callback) {
	      /**
	       *
	       * @type {onInputSelect}
	       * @private
	       */
	      this._onSelect = callback;
	    }
	  }, {
	    key: 'onSelect',
	    value: function onSelect() {
	      if (this._onSelect) this._onSelect(this.$value);
	    }
	
	    /**
	     *
	     * @param {string} [iata]
	     * @param {boolean} [isCountry=false]
	     */
	
	  }, {
	    key: 'update',
	    value: function update(iata) {
	      var isCountry = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
	
	
	      if (iata) {
	        if (isCountry) {
	
	          var countryData = (0, _country.byIATA)(iata);
	          if (countryData) {
	            this.__setItem(countryData);
	          } else {
	            this.clean();
	          }
	        } else {
	
	          var cityData = (0, _city.byIATA)(iata);
	
	          if (cityData) {
	            this.__setItem(cityData);
	          } else {
	            this.clean();
	          }
	        }
	      }
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'setError',
	    value: function setError() {
	      (0, _classList2["default"])(this._node.parentNode).add('ottbp-control--error');
	      (0, _classList2["default"])(this._node).add('ottbp-control--error');
	    }
	  }, {
	    key: 'clearError',
	    value: function clearError() {
	      (0, _classList2["default"])(this._node).remove('ottbp-control--error');
	      (0, _classList2["default"])(this._node.parentNode).remove('ottbp-control--error');
	    }
	  }, {
	    key: '$empty',
	    get: function get() {
	      return !this.$value.iata;
	    }
	
	    /**
	     *
	     * @return {null|PlaceData}
	     */
	
	  }, {
	    key: '$value',
	    get: function get() {
	      return this._data;
	    }
	  }]);
	
	  return InputSelect;
	}();
	
	exports["default"] = InputSelect;

/***/ },
/* 58 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _classList = __webpack_require__(49);
	
	var _classList2 = _interopRequireDefault(_classList);
	
	var _earth = __webpack_require__(59);
	
	var _earth2 = _interopRequireDefault(_earth);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 *
	 * @type {Earth}
	 */
	var EARTH_ICON = new _earth2["default"]().resize(12, 12).fill('#9E9E9E');
	
	var helpers = [];
	
	document.addEventListener('mousemove', function (event) {
	  var target = event.target;
	
	  while (target && target != document.body) {
	    if ((0, _classList2["default"])(target).contains('ottbp-input-helper__list')) return true;
	    target = target.parentNode;
	  }
	
	  onBlur();
	});
	
	function onBlur() {
	  helpers.forEach(function (helper) {
	    helper.onBlur();
	  });
	}
	
	var InputHelper = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {Object} [options = {}]
	   * @param {PlaceData[]} [options.items]
	   */
	  function InputHelper(container) {
	    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	    _classCallCheck(this, InputHelper);
	
	    /**
	     *
	     * @type {HTMLElement}
	     * @private
	     */
	    this._container = container;
	
	    this._items = options.items || [];
	
	    this._selected = false;
	
	    helpers.push(this);
	
	    this.initHTML();
	  }
	
	  _createClass(InputHelper, [{
	    key: 'initHTML',
	    value: function initHTML() {
	
	      this._wrapper = (0, _Element.docCreate)('div', ['ottbp-input-helper']);
	      this._listNode = (0, _Element.docCreate)('ul', ['ottbp-input-helper__list']);
	
	      this._wrapper.addEventListener('mousewheel', this.onScroll.bind(this));
	
	      this.__initItemsHTML();
	
	      this._wrapper.appendChild(this._listNode);
	
	      this._shadow = (0, _Element.docCreate)('div', 'ottbp-control__helper__shadow');
	      this._shadowId = Date.now();
	      this._shadow.id = this._shadowId;
	
	      this._container.appendChild(this._shadow);
	      this._container.appendChild(this._wrapper);
	
	      this._listNode.setAttribute('tabindex', '1');
	
	      this._listNode.addEventListener('click', this.__onClick.bind(this));
	      this._listNode.addEventListener('mousemove', this.__onHover.bind(this));
	
	      return this;
	    }
	  }, {
	    key: '__onClick',
	    value: function __onClick(event) {
	      var target = event.target;
	      while (target && target != this._listNode) {
	        if ((0, _classList2["default"])(target).contains('ottbp-input-helper-link') && target.hasAttributes('data-index')) {
	
	          event.stopPropagation();
	          event.preventDefault();
	
	          this.__preSelect(parseInt(target.getAttribute('data-index'), 10));
	        }
	        target = target.parentNode;
	      }
	    }
	  }, {
	    key: '__preSelect',
	    value: function __preSelect(index) {
	      this._selected = true;
	      this.onSelect(this._items[index]);
	    }
	
	    /**
	     *
	     * @callback onClickHelper
	     * @param {PlaceData} item
	     */
	
	    /**
	     *
	     * @param {onClickHelper} callback
	     * @return {InputHelper}
	     */
	
	  }, {
	    key: 'whenSelect',
	    value: function whenSelect(callback) {
	      /**
	       *
	       * @type {onClickHelper}
	       * @private
	       */
	      this._callbackSelect = callback;
	      return this;
	    }
	
	    /**
	     *
	     * @param {PlaceData} item
	     */
	
	  }, {
	    key: 'onSelect',
	    value: function onSelect(item) {
	      if (this._callbackSelect) this._callbackSelect(item);
	    }
	  }, {
	    key: '__onHover',
	    value: function __onHover(event) {
	
	      if (this._selected) return;
	
	      this._hovered = true;
	      var target = event.target;
	      while (target != this._listNode) {
	        if ((0, _classList2["default"])(target).contains('ottbp-input-helper-link') && target.hasAttributes('data-index')) {
	          this.__preHover(parseInt(target.getAttribute('data-index'), 10));
	        }
	        target = target.parentNode;
	      }
	    }
	
	    /**
	     *
	     * @param {number} index
	     * @return {InputHelper}
	     * @private
	     */
	
	  }, {
	    key: '__preHover',
	    value: function __preHover(index) {
	      if (this._lastHover && this._lastHover == index && !this._items[index]) return this;
	      this._lastHover = index;
	      this.onHover(this._items[index]);
	      this.__updateItemsStyles();
	    }
	
	    /**
	     *
	     * @callback onHoverHelper
	     * @param {PlaceData} item
	     */
	
	    /**
	     *
	     * @param {onHoverHelper} callback
	     */
	
	  }, {
	    key: 'whenHover',
	    value: function whenHover(callback) {
	      /**
	       *
	       * @type {onHoverHelper}
	       * @private
	       */
	      this._callbackHover = callback;
	    }
	
	    /**
	     *
	     * @param {PlaceData} item
	     */
	
	  }, {
	    key: 'onHover',
	    value: function onHover(item) {
	      if (this._callbackHover) this._callbackHover(item);
	    }
	  }, {
	    key: 'onBlur',
	    value: function onBlur() {
	      if (!this._hovered) return;
	      this._lastHover = -1;
	      this._hovered = false;
	
	      if (this._items && this._items.length > 0) {
	        if (this._highlight < 0) this.onHover(this._items[0]);else this.onHover(this._items[this._highlight]);
	
	        this.__updateItemsStyles();
	      }
	    }
	  }, {
	    key: 'onScroll',
	    value: function onScroll(event) {
	      event.preventDefault();
	      event.stopPropagation();
	
	      var scrollTop = this._wrapper.scrollTop + event.deltaY;
	
	      this._wrapper.scrollTop = scrollTop;
	
	      this.__setScrollWrapper(scrollTop);
	    }
	
	    /**
	     *
	     * @param {number} scrollTop
	     * @return {InputHelper}
	     * @private
	     */
	
	  }, {
	    key: '__setScrollWrapper',
	    value: function __setScrollWrapper(scrollTop) {
	      this._wrapper.scrollTop = scrollTop;
	
	      if (scrollTop > 0) {
	        this.__setShadowStyle(scrollTop > 20 ? '1' : scrollTop / 20, scrollTop > 20 ? '10px' : scrollTop / 20 * 10 + 'px');
	      } else {
	        this.__setShadowStyle(0, 0);
	      }
	      return this;
	    }
	  }, {
	    key: '__setShadowStyle',
	    value: function __setShadowStyle(opacity, height) {
	      // listShadow(this._shadowId, opacity, height);
	      // this._shadow.style.opacity = opacity;
	      // this._shadow.style.height = height;
	
	      this._shadow.setAttribute('style', 'opacity:' + opacity + ' !important; height:' + height + ' !important');
	      return this;
	    }
	  }, {
	    key: '__initItemsHTML',
	    value: function __initItemsHTML() {
	      this._itemsNode = [];
	      this._fragment = document.createDocumentFragment();
	
	      this._listNode.innerHTML = '';
	      this._items.forEach(this.__createItemHTML, this);
	      this._listNode.appendChild(this._fragment);
	
	      return this;
	    }
	
	    /**
	     *
	     * @param {PlaceData} item
	     * @param {number} index
	     * @private
	     */
	
	  }, {
	    key: '__createItemHTML',
	    value: function __createItemHTML(item, index) {
	      var li = (0, _Element.docCreate)('li', 'ottbp-input-helper__list__item'),
	          a = (0, _Element.docCreate)('a', 'ottbp-input-helper-link');
	
	      if (index == this._highlight) (0, _classList2["default"])(a).add('ottbp-highlight');
	
	      a.tabIndex = '-1';
	      a.setAttribute('data-index', index);
	
	      var icon = '',
	          label = '',
	          place = '';
	
	
	      switch (item.type) {
	        case 'country':
	          icon = '<span class="ottbp-helper-icon">' + EARTH_ICON.toString() + '</span>';
	          label = item.country.ru;
	          break;
	        case 'city':
	          label = item.city.ru + ',';
	          place = item.country.ru;
	          break;
	      }
	
	      a.innerHTML = icon + '<span class="ottbp-helper-strong">' + label + '</span>&nbsp;<span class="ottbp-helper-gray">' + place + '</span><span class="ottbp-helper-iata">' + item.iata + '</span>';
	
	      li.appendChild(a);
	      this._itemsNode.push(a);
	      this._fragment.appendChild(li);
	    }
	
	    /**
	     *
	     * @return {InputHelper}
	     * @private
	     */
	
	  }, {
	    key: '__updateItemsStyles',
	    value: function __updateItemsStyles() {
	      /**
	       * @this {InputHelper}
	       */
	      this._itemsNode.forEach(function (item, index) {
	        if (!this._hovered && index == this._highlight) (0, _classList2["default"])(item).add('ottbp-highlight');else (0, _classList2["default"])(item).remove('ottbp-highlight');
	      }, this);
	
	      return this;
	    }
	  }, {
	    key: 'highLight',
	    value: function highLight(delta) {
	      var current = this._highlight || this._highlight === 0 ? this._highlight : -1;
	      // current = (current < 0) ? 0 : current;
	
	      if (delta < 0) {
	        this._highlight = current <= 0 ? this._items.length - 1 : current - 1;
	      } else if (delta > 0) {
	        this._highlight = current >= this._items.length - 1 ? 0 : current + 1;
	      }
	
	      if (this._highlight < 5) this.__setScrollWrapper(0);else this.__setScrollWrapper((this._highlight - 4) * 52);
	
	      this.__updateItemsStyles().onHover(this._items[this._highlight]);
	    }
	
	    /**
	     *
	     * @param {Object} [options = {}]
	     * @param {PlaceData[]} [options.items]
	     */
	
	  }, {
	    key: 'update',
	    value: function update() {
	      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	      this._items = options.items || [];
	      this._lastHover = -1;
	      this._highlight = -1;
	
	      this.__initItemsHTML().__setScrollWrapper(0);
	
	      if (this._items && this._items.length > 0) this.onHover(this._items[0]);
	    }
	  }, {
	    key: '$currentItem',
	    get: function get() {
	      var currentIndex = this._highlight;
	      currentIndex = currentIndex < 0 ? 0 : currentIndex;
	      if (this._items && this._items.length > currentIndex) {
	        return this._items[currentIndex];
	      }
	      return null;
	    }
	  }]);
	
	  return InputHelper;
	}();
	
	exports["default"] = InputHelper;

/***/ },
/* 59 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Svg2 = __webpack_require__(60);
	
	var _Svg3 = _interopRequireDefault(_Svg2);
	
	__webpack_require__(61);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Earth = function (_Svg) {
	  _inherits(Earth, _Svg);
	
	  function Earth() {
	    _classCallCheck(this, Earth);
	
	    var _this = _possibleConstructorReturn(this, (Earth.__proto__ || Object.getPrototypeOf(Earth)).call(this));
	
	    _this._attr = {
	      width: 14,
	      height: 14
	    };
	
	    _this._options = {
	      fill: '#616161'
	    };
	
	    _this.update();
	    return _this;
	  }
	
	  /**
	   *
	   * @return {Earth}
	   */
	
	
	  _createClass(Earth, [{
	    key: 'update',
	    value: function update() {
	      this._inner = '<use xlink:href="#ottbp-s-earth" fill="' + this._options.fill + '"  class="ottbp-sprite-svg__earth"></use>';
	      return this.render();
	    }
	
	    /**
	     *
	     * @param {number} [width]
	     * @param {number} [height]
	     * @return {Earth}
	     */
	
	  }, {
	    key: 'resize',
	    value: function resize() {
	      var width = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._attr.width;
	      var height = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._attr.height;
	
	      return this.attr({
	        width: width,
	        height: height
	      });
	    }
	
	    /**
	     *
	     * @param fill
	     * @return {Earth}
	     */
	
	  }, {
	    key: 'fill',
	    value: function fill(_fill) {
	      this._options.fill = _fill;
	      return this.update();
	    }
	  }]);
	
	  return Earth;
	}(_Svg3["default"]);
	
	exports["default"] = Earth;

/***/ },
/* 60 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Deep = __webpack_require__(9);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 *
	 * @param {string} innerSVG
	 * @param {Object.<string,string>} [attrs = []]
	 * @return {string}
	 */
	function createSVGString(innerSVG) {
	  var attrs = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	
	  var attr = Object.keys(attrs).reduce(function (last, key) {
	    if (attrs[key]) return last += key + '="' + attrs[key] + '" ';
	    return last;
	  }, 'xmlns="http://www.w3.org/2000/svg"' + ' ');
	
	  // let attr = reduce(Object.keys(attrs), (last, key) => {
	  //   if (attrs[key])
	  //     return last += `${key}="${attrs[key]}" `;
	  //   return last;
	  // }, 'xmlns="http://www.w3.org/2000/svg" ');
	
	  return '<svg ' + attr + '>' + innerSVG + '</svg>';
	}
	
	// /**
	//  *
	//  * @param {string} innerSVG
	//  * @param {Object.<string,string>} [attrs = {}]
	//  * @return {Document}
	//  */
	// function createSVG(innerSVG, attrs = {}) {
	//   let domParser = new DOMParser();
	//   return domParser.parseFromString(createSVGString(innerSVG, attrs), 'application/xml');
	// }
	//
	// /**
	//  *
	//  * @param {Document} svgDocument
	//  * @param {HTMLElement} parentNode
	//  */
	// function appendSVG(svgDocument, parentNode) {
	//   parentNode.appendChild(parentNode.ownerDocument.importNode(svgDocument.documentElement, true));
	// }
	
	var SVG = function () {
	  function SVG() {
	    _classCallCheck(this, SVG);
	
	    this._inner = '';
	    this._attr = {};
	  }
	
	  /**
	   *
	   * @param {Object} attrs
	   * @return {SVG}
	   */
	
	
	  _createClass(SVG, [{
	    key: 'attr',
	    value: function attr(attrs) {
	      this._attr = (0, _Deep.deepExtend)({}, this._attr, attrs);
	      return this.update();
	    }
	
	    /**
	     *
	     * @return {SVG}
	     */
	
	  }, {
	    key: 'update',
	    value: function update() {
	      this._inner = '';
	      return this.render();
	    }
	
	    /**
	     *
	     * @return {SVG}
	     */
	
	  }, {
	    key: 'render',
	    value: function render() {
	
	      // this._document = createSVG(this._inner, this._attr);
	
	      // Object.keys(this._attr).forEach(function (key) {
	      //   if (this._attr[key])
	      //     this._node.setAttribute(key, this._attr[key]);
	      //   else
	      //     this._node.removeAttribute(key);
	      // }, this);
	      //
	      // this._node.innerHTML = this.inner;
	      return this;
	    }
	
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: 'toString',
	    value: function toString() {
	      return createSVGString(this._inner, this._attr);
	    }
	  }]);
	
	  return SVG;
	}();
	
	exports["default"] = SVG;

/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Svg2 = __webpack_require__(60);
	
	var _Svg3 = _interopRequireDefault(_Svg2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var symbol_air = '<symbol id="ottbp-s-air" viewBox="0 0 14 12">\n        <path d="M9.113611,5.239766 L12.95,5.239766 C13.51,5.239766 14,5.698246 14,6.222222 C14,6.746199 13.51,7.204678 12.95,7.204678 L9.1,7.204678 L5.6,12.444444 L4.2,12.444444 L5.95,7.204678 L2.1,7.204678 L1.05,8.51462 L0,8.51462 L0.7,6.222222 L0,3.929825 L1.05,3.929825 L2.1,5.239766 L5.95,5.239766 C5.95,5.239766 5.949862,5.239352 5.949591,5.23854 L5.964925,5.239983 L9.113487,5.239983 L9.113611,5.239766 Z M8.449043,4.265235 C7.464068,2.790651 5.6,0 5.6,0 L4.2,0 C4.2,0 5.127627,2.777457 5.624241,4.264393 L8.450065,4.266764 Z"></path>\n      </symbol>';
	
	var symbol_earth = '<symbol id="ottbp-s-earth" viewBox="0 0 48.6 48.6">\n        <path d="M35.4 10.8v.4H35v1h.5l1-.2.3-.6h-.5L36 11l-.3-.7-.2-1-.7.3-.2.3v.3l.3.2"/>\n        <path d="M34.8 11v-.4l-.4-.2-.6.2-.4.7v.5h.5M22.5 13.2l-.2.3h-.6v.3l.2.2h.3l.2-.2v-.3h.4v-.3h-.5zM20.8 13.8v.3h.5v-.3l-.3-.2"/>\n        <path d="M48.6 24v-2c-.3-2.4-1-4.6-1.7-6.7-.2 0-.2-.3-.3-.4-1-2.7-2.6-5-4.5-7.2-.2 0-.3-.2-.4-.4l-1-1C36.2 2.3 30.5 0 24.2 0 18 0 12.3 2.4 8 6.4c-1.2 1-2 2-3 3-3 4.2-5 9.3-5 15 0 13.3 11 24.2 24.3 24.2 9.4 0 17.6-5.4 21.7-13.2.8-1.7 1.5-3.5 2-5.4l.2-1.4c.3-1.4.4-2.8.4-4.3V24zM44 14.4h.2l.5 1H44v-1zM40.5 10V9c.4.4.8.8 1 1.3l-.3.6h-1.5v-.4l.8-.5zM11.2 7.5h.5v-.2h.8v.3l-.2.3h-1v-.4zm.8 1h.5V9h-1l-.3-.3.8-.2zm33.6 9.7h-1.8l-1-.7h-1.2v.8h-.8l-2-.6V16l-2.4.3-.8.4H34l-1 .6v1.3l-2.6 1.8.2.7h.5v.8l-.4.2v2l2.2 2.3h1V26h1.6l.5-.3h.8l.6.5 1.4.2-.2 1.8L40 31l-.7 1.6v.7l.7.7v1.7l1 1.2v1.4h.6c-4 5-10.3 8.2-17.3 8.2C12 46.6 2 36.6 2 24.3c0-3 .6-6 1.8-8.7V15l.8-1 1-1.6v.4l-1 1-.8 1.8V17l1 .3V19l.8 1.6h.7v-.4L5.7 19l-.2-1.4H6V19l1.3 1.7-.3.6.8 1.2 2 .5v-.4h.7v.7h.6l1 .4 1.3 1.5H15l.2 1.5-1 .8-.2 1.3-.2.7 1.7 2v1h.8l1.4 1v3.8l.5.2-.3 1.7.6 1V42l1 1.8 1.2 1.2h1.4v-.4l-1-.8.2-.5.2-.5v-.5H21l-.4-.4.5-.6.2-.4-.6-.2v-.4h1l1.2-.7.5-1 1.4-1.6-.3-1.3.4-.8H26l1-.7.2-2.7 1-1.2v-.8l-.7-.3-.6-1h-2l-1.6-.5v-1l-.6-1h-1.4l-1-1.3-.6-.4v.5h-1.3L18 24l-1.4-.4-1 1.3-2-.4v-2l-1.3-.2.5-1-.2-.6L11 22h-1l-.5-1 .2-.8.6-1 1.4-.8h2.6v.8l1 .4v-1.4l.5-.6 1.4-1V16l1.5-1.4 1.4-.8 1-1h.4v.2l.5-.4h-.4l-.4-.2V12l.2-.2h.5l.3.5h.2l.8-.2V12h.5v.5l-.4.2v.4l1.3.3h.3v-.5l-1-.5V12l.8-.2V11l-.8-.5V9.2l-1.3.5h-.4v-1L21 8.4l-.6.5v1.4l-1.2.3-.5 1H18v-1l-1-.3-.7-.4-.2-.8 2-1 1-.4.2.6h.5V8h.6l-.2-.2v-.4h.7l.5-.5v-.3H23l.6.4-1.7 1 2.2.5.2-.7h1l.3-.6-.7-.2v-.8l-2-1-1.6.3-.8.4v1l-.8-.2v-.6l.7-.7h-1.9l-.2.5h.6v.7h-1l-.2.4h-1.4v-.6h1l.8-1h-.4l-.6.5-1-.2-.6-.8H14L13 6H14v.4l-.2.3H15l.4.5h-1.6v-.5l-1-.2-.5-.2h-1c3.6-2.7 8-4.3 13-4.3C30 2 35 4 39 7.6l-.2.4-1 .5-.5.4v.5h.6l.3.8 1-.3v1h-1.7l-.8 1.2-1.2.2-.2 1h.5v.7h-2.3l-.2.7.2 1.2.6.2H36l.2-.6 1-1.4h.8l.7-.5.2.5 1.7 1.2-.2.3h-.8l.3.3.5.2.6-.3V15h.2l-.2-.3-1.2-.8-.3-.8h1l.3.3.8.7v1l1 .8.3-1.3.6-.4v1l.6.7h1l.7 1.7zm-32.3-7l.5-.2h.6l-.2.8-.6.2-.3-.8zm3 1.7v.5H15l-.5-.2.2-.3.6-.2h1v.2zm.7.7v.4l-.4.2h-.4v-.6h.8zm-.4-.2v-.6l.5.5-.4.2zm.2 1v.5l-.3.3h-.7v-.5h.4v-.2h.6zm-1.8-.8h.8l-1 1.3-.4-.2V14l.6-.6zm3 .7v.5h-.6l-.2-.3v-.4l1 .3zm-.6-.5l.2-.2.3.2-.3.2-.3-.2zM46 19.3v.3-.3z"/>\n        <path d="M3.8 15v.6l.8-1.7-.8 1z"/>\n      </symbol>';
	
	var symbol_arrow = '<symbol id="ottbp-s-arrow" viewBox="0 0 7 10">\n        <path fill="#FFF" fill-rule="evenodd" d="M1.333 0L.158 1.175 3.975 5 .158 8.825 1.333 10l5-5"/>\n      </symbol>';
	
	var symbol_search = '<symbol id="ottbp-s-search" viewBox="0 0 600 598">\n        <path d="M213 .6c-1.9.2-7.3.9-12 1.5-94 11.5-171.8 81.1-194.4 174C-13 256.7 12.5 343 72.3 398.4c35.7 33.1 75.2 52.2 124.2 60.2 15.5 2.6 52.5 2.6 68 0 28.1-4.6 54.7-13.4 77.1-25.7l7.6-4.2 6.1 6c3.4 3.3 37.9 36.8 76.7 74.4 38.8 37.6 73.4 71 76.9 74.2 22.1 20.2 53.8 19.5 74.6-1.7 19.1-19.4 21.8-47.2 6.9-70.1-1.9-2.9-36.4-37.1-83.2-82.4l-80-77.4 3.5-6.1c13.3-23.3 22.4-49.1 27.5-77.5 3-16.8 3.2-55.7.5-72-4.8-28.3-12.8-52.3-24.5-74.1C399.2 57 338 13.9 264.5 2.5 254.9 1 220.3-.3 213 .6zm46 94.9c59.6 13.4 103.7 63.2 108.9 123.2 3.7 41.9-11.7 81.9-42.7 111.5-35.8 34-85.9 46.1-133.7 32.3-41.2-11.9-77.3-46.7-91.3-88-5.5-16-6.7-24.1-6.7-44s1.2-28 6.7-44c11.9-34.9 38.7-64.8 72.8-80.9 10-4.7 22.6-8.7 35-11.1 10.2-1.9 40.6-1.3 51 1z"/>\n      </symbol>';
	
	var Sprite = function (_Svg) {
	  _inherits(Sprite, _Svg);
	
	  function Sprite() {
	    _classCallCheck(this, Sprite);
	
	    var _this = _possibleConstructorReturn(this, (Sprite.__proto__ || Object.getPrototypeOf(Sprite)).call(this));
	
	    _this.__init();
	    return _this;
	  }
	
	  _createClass(Sprite, [{
	    key: 'update',
	    value: function update() {
	      this._inner = [symbol_air, symbol_earth, symbol_arrow, symbol_search].join('');
	      return this.render();
	    }
	  }, {
	    key: '__init',
	    value: function __init() {
	      this.attr({ style: 'display:none !important;' });
	    }
	  }]);
	
	  return Sprite;
	}(_Svg3["default"]);
	
	exports["default"] = Sprite;
	
	
	if (!window.init_sprite) {
	
	  var tempDiv = document.createElement('div');
	
	  tempDiv.innerHTML = new Sprite().toString();
	
	  document.getElementsByTagName('body')[0].appendChild(tempDiv);
	
	  window.init_sprite = true;
	}

/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _search = __webpack_require__(63);
	
	var _search2 = _interopRequireDefault(_search);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	// import Eve from '../mixins/Event';
	
	var SearchButton = function () {
	  /**
	   *
	   * @param container
	   * @param {EventStore} eventStore
	   */
	  function SearchButton(container, eventStore) {
	    _classCallCheck(this, SearchButton);
	
	    this._eventStore = eventStore;
	    this._container = container;
	    this.__initHTML().__initEvents();
	    this._container.appendChild(this._btn);
	  }
	
	  /**
	   *
	   * @return {SearchButton}
	   * @private
	   */
	
	
	  _createClass(SearchButton, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      this._node = (0, _Element.docCreate)('div', 'ottbp-search');
	      this._btn = (0, _Element.docCreate)('button', 'ottbp-search__btn');
	
	      this.__initICON();
	
	      this._node.appendChild(this._btn);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {SearchButton}
	     * @private
	     */
	
	  }, {
	    key: '__initICON',
	    value: function __initICON() {
	      var icon = new _search2["default"]();
	      icon.resize(20, 20).fill('#616161');
	      this._btn.innerHTML = '<span class="ottbp-search__btn__icon">' + icon.toString() + '</span><span class="ottbp-search__btn__text">\u0418\u0441\u043A\u0430\u0442\u044C</span>';
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {SearchButton}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      var _this = this;
	
	      this._eventStore.delegateDocument('click', this._btn, function () {
	        _this._eventStore.dispatch('SearchButton::click');
	      });
	
	      return this;
	    }
	  }]);
	
	  return SearchButton;
	}();
	
	exports["default"] = SearchButton;

/***/ },
/* 63 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Svg2 = __webpack_require__(60);
	
	var _Svg3 = _interopRequireDefault(_Svg2);
	
	__webpack_require__(61);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Search = function (_Svg) {
	  _inherits(Search, _Svg);
	
	  function Search() {
	    _classCallCheck(this, Search);
	
	    var _this = _possibleConstructorReturn(this, (Search.__proto__ || Object.getPrototypeOf(Search)).call(this));
	
	    _this._attr = {
	      width: 39,
	      height: 39
	    };
	
	    _this._options = {
	      fill: '#FFFFFF'
	    };
	
	    _this.update();
	    return _this;
	  }
	
	  /**
	   *
	   * @return {Earth}
	   */
	
	
	  _createClass(Search, [{
	    key: 'update',
	    value: function update() {
	      this._inner = '<use xlink:href="#ottbp-s-search" fill="' + this._options.fill + '"  class="ottbp-sprite-svg__search"></use>';
	      return this.render();
	    }
	
	    /**
	     *
	     * @param {number} [width]
	     * @param {number} [height]
	     * @return {Earth}
	     */
	
	  }, {
	    key: 'resize',
	    value: function resize() {
	      var width = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._attr.width;
	      var height = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._attr.height;
	
	      return this.attr({
	        width: width,
	        height: height
	      });
	    }
	
	    /**
	     *
	     * @param fill
	     * @return {Earth}
	     */
	
	  }, {
	    key: 'fill',
	    value: function fill(_fill) {
	      this._options.fill = _fill;
	      return this.update();
	    }
	  }]);
	
	  return Search;
	}(_Svg3["default"]);
	
	exports["default"] = Search;

/***/ },
/* 64 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	// import Eve from './../mixins/Event';
	
	var _Element = __webpack_require__(7);
	
	var _DropDownSelect = __webpack_require__(65);
	
	var _DropDownSelect2 = _interopRequireDefault(_DropDownSelect);
	
	var _configSelects = __webpack_require__(67);
	
	var _configSelects2 = _interopRequireDefault(_configSelects);
	
	var _constant = __webpack_require__(51);
	
	var _width = __webpack_require__(68);
	
	var _width2 = _interopRequireDefault(_width);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Selects = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {EventStore} eventStore
	   */
	  function Selects(container, eventStore) {
	    _classCallCheck(this, Selects);
	
	    this._eventStore = eventStore;
	    /**
	     *
	     * @type {HTMLElement}
	     * @private
	     */
	    this._container = container;
	    /**
	     *
	     * @type {{route?: string, type?: string, transfer?: string, duration?: string}.<string, string>}
	     * @private
	     */
	    this._options = {};
	
	    /**
	     *
	     * @type {{route?: DropDownSelect, type?: DropDownSelect, transfer?: DropDownSelect, duration?: DropDownSelect}.<string,DropDownSelect>}
	     * @private
	     */
	    this._selectors = {};
	    /**
	     * Показывать (1) или не показывать (0) элементы по порядку
	     * @type {Array.<number>}
	     * @private
	     */
	    this._displays = [1, 0, 1, 1];
	
	    this._size = 'big';
	
	    this.__initHTML();
	  }
	
	  /**
	   *
	   * @return {Selects}
	   * @private
	   */
	
	
	  _createClass(Selects, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      /**
	       *
	       * @type {HTMLElement}
	       * @private
	       */
	      this._node = (0, _Element.docCreate)('div', 'ottbp-selects');
	
	      this.__initSelectsHTML().__initStyles();
	
	      this._container.appendChild(this._node);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Selects}
	     * @private
	     */
	
	  }, {
	    key: '__initSelectsHTML',
	    value: function __initSelectsHTML() {
	
	      this._containers = {};
	      /**
	       * @this {Selects}
	       */
	      _configSelects2["default"].forEach(function (item) {
	        var temp = (0, _Element.docCreate)('div', ['ottbp-selects__cell', 'ottbp-selects--' + item.type]);
	        var wrapper = (0, _Element.docCreate)('div', 'ottbp-selects__cell__wrap');
	
	        var tempDropDown = new _DropDownSelect2["default"](item.selects, Selects.itemIndexByValue(item, this._options[item.type]), wrapper, undefined, this._eventStore);
	        tempDropDown.whenSelect(function (value) {
	          this.__onSelectsChange(item.type, value);
	        }.bind(this));
	
	        this._options[item.type] = tempDropDown.$value.value;
	
	        this._selectors[item.type] = tempDropDown;
	
	        temp.appendChild(wrapper);
	        this._node.appendChild(temp);
	
	        this._containers[item.type] = temp;
	      }, this);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Selects}
	     * @private
	     */
	
	  }, {
	    key: '__initStyles',
	    value: function __initStyles() {
	
	      _width2["default"][this._displays.join('')][this._size].items.forEach(function (item) {
	        /**
	         * @this {Selects}
	         */
	        this._containers[item.type].setAttribute('style', 'width:' + item.percent + '% !important;');
	      }, this);
	
	      return this;
	    }
	
	    /**
	     *
	     * @param item
	     * @param value
	     * @return {number}
	     */
	
	  }, {
	    key: '__onSelectsChange',
	
	
	    /**
	     *
	     * @param {string} type
	     * @param {DropDownSelectOptions} value
	     * @private
	     */
	    value: function __onSelectsChange(type, value) {
	      if (this._options[type] !== value.value) {
	        this._options[type] = value.value;
	        this.__changeChain();
	
	        // if (this._onChange)
	        //   this._onChange();
	        //@TODO удалить type and value
	        this._eventStore.dispatch('Selects::change', {
	          type: 'selectors',
	          value: this.$value,
	          selectors: this.$value
	        });
	      }
	    }
	
	    /**
	     *
	     * @param {FormatMainForm_Selects} options
	     */
	
	  }, {
	    key: 'update',
	    value: function update(options) {
	      var _this = this;
	
	      Object.keys(options).forEach(function (key) {
	        _this._selectors[key].update(options[key]);
	      });
	    }
	
	    /**
	     *
	     * @return {{duration:string route:boolean transfer:string|number type:string|number}}
	     */
	
	  }, {
	    key: '__changeChain',
	    value: function __changeChain() {
	
	      if (this._options.route) {
	        this._displays[3] = 1;
	        this._selectors.duration.enable();
	      } else {
	        this._displays[3] = 0;
	        this._selectors.duration.disable();
	      }
	
	      this.__initStyles();
	    }
	  }, {
	    key: 'resize',
	    value: function resize(containerWidth) {
	      if (containerWidth > _constant.CONTAINER_WIDTH.medium) {
	        this._size = 'big';
	      } else if (containerWidth > _constant.CONTAINER_WIDTH.small) {
	        this._size = 'medium';
	      } else if (containerWidth > _constant.CONTAINER_WIDTH.extra_small) {
	        this._size = 'small';
	      } else {
	        this._size = 'extra_small';
	      }
	
	      this.render();
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var styles = _width2["default"][this._displays.join('')][this._size];
	      this._displays.forEach(function (bool, index) {
	        /**
	         * @this {Selects}
	         */
	        if (bool) {
	          this._selectors[styles.items[index].type].enable();
	        } else {
	          this._selectors[styles.items[index].type].disable();
	        }
	      }, this);
	      this.__initStyles();
	    }
	  }, {
	    key: '$value',
	    get: function get() {
	      var val = {};
	
	      for (var type in this._selectors) {
	        if (this._selectors.hasOwnProperty(type)) {
	          /**
	           * @type {DropDownSelect}
	           */
	          var sel = this._selectors[type];
	          val[type] = sel.$value.value;
	        }
	      }
	
	      return val;
	    }
	  }], [{
	    key: 'itemIndexByValue',
	    value: function itemIndexByValue(item, value) {
	      return item.selects.reduce(function (defIndex, it, index) {
	        if (it && typeof it.value !== 'undefined' && it.value === value) return index;
	        return defIndex;
	      }, 0);
	    }
	  }]);
	
	  return Selects;
	}();
	
	exports["default"] = Selects;

/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _helper = __webpack_require__(66);
	
	var _helper2 = _interopRequireDefault(_helper);
	
	var _classList = __webpack_require__(49);
	
	var _classList2 = _interopRequireDefault(_classList);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var DropDownSelect = function () {
	  /**
	   *
	   * @param {DropDownSelectOptions[]} [items]
	   * @param {number} [selected]
	   * @param {HTMLElement} container
	   * @param {Object} [options={}]
	   * @param {string} [options.name]
	   * @param {boolean|undefined} [options.disabled]
	   * @param {EventStore} eventStore
	   */
	  function DropDownSelect() {
	    var items = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	    var selected = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
	    var container = arguments[2];
	    var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
	    var eventStore = arguments[4];
	
	    _classCallCheck(this, DropDownSelect);
	
	    this._eventStore = eventStore;
	    this._container = container;
	    /**
	     *
	     * @type {DropDownSelectOptions[]}
	     * @private
	     */
	    this._items = items || [];
	    /**
	     *
	     * @type {number}
	     * @private
	     */
	    this._selected = selected;
	    /**
	     *
	     * @type {boolean}
	     * @private
	     */
	    this._disabled = !!options.disabled;
	    /**
	     *
	     * @type {?string}
	     * @private
	     */
	    this._name = options.name || null;
	
	    this.initHTML().initEvents();
	  }
	
	  _createClass(DropDownSelect, [{
	    key: 'initHTML',
	    value: function initHTML() {
	      // .ottbp-select(class=(selected ? 'selected' : ''))
	      //   button.ottbp-select__btn.ottbp-texts--btn
	      //    span.ottbp-select__btn__inner!= value
	      //    .ottbp-select__helper
	
	      var item = this._items[this._selected];
	      this._node = (0, _Element.docCreate)('div', ['ottbp-select', item.orange ? 'ottbp-selected' : '']);
	
	      this.__initButtonHTML().__initHelperHTML();
	
	      this._container.appendChild(this._node);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {DropDownSelect}
	     * @private
	     */
	
	  }, {
	    key: '__initClassHTML',
	    value: function __initClassHTML() {
	      var item = this._items[this._selected];
	
	      var add = [],
	          remove = [];
	      item.orange ? add.push('ottbp-selected') : remove.push('ottbp-selected');
	      this._disabled ? add.push('ottbp-disabled') : remove.push('ottbp-disabled');
	      (0, _Element.classAddRemove)(this._node, add, remove);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {DropDownSelect}
	     * @private
	     */
	
	  }, {
	    key: '__initButtonHTML',
	    value: function __initButtonHTML() {
	      var btn = (0, _Element.docCreate)('button', 'ottbp-select__btn ottbp-texts--btn');
	      this._btnInner = (0, _Element.docCreate)('span', 'ottbp-select__btn__inner');
	
	      btn.appendChild(this._btnInner);
	
	      this._node.appendChild(btn);
	      return this.__initButtonInnerHTML();
	    }
	
	    /**
	     *
	     * @return {DropDownSelect}
	     * @private
	     */
	
	  }, {
	    key: '__initButtonInnerHTML',
	    value: function __initButtonInnerHTML() {
	      var item = this._items[this._selected];
	      this._btnInner.innerHTML = '' + (item ? item.title || '' : '');
	      return this;
	    }
	  }, {
	    key: '__initHelperHTML',
	    value: function __initHelperHTML() {
	      this._helperNode = (0, _Element.docCreate)('div', 'ottbp-select__helper');
	      this._node.appendChild(this._helperNode);
	      return this.__initHelper();
	    }
	  }, {
	    key: '__initHelper',
	    value: function __initHelper() {
	      if (this._items.length > 2) {
	        this._helper = new _helper2["default"](this._items, this._selected, this._helperNode, this._eventStore);
	        this._helper.whenSelect(this.onSelectFromList.bind(this));
	      }
	      return this;
	    }
	  }, {
	    key: 'onSelectFromList',
	    value: function onSelectFromList(index) {
	
	      this.hideHelper();
	      if (this._selected == index) return this;
	      this._selected = index;
	      this.onSelect();
	      this.__initButtonInnerHTML().__initClassHTML();
	    }
	  }, {
	    key: 'initEvents',
	    value: function initEvents() {
	      var _this = this;
	
	      // Eve.addEvent(this._node, 'click', ::this.__onClick);
	      this._eventStore.delegateDocument('click', this._node, this.__onClick.bind(this), function () {
	        _this.hideHelper();
	      });
	    }
	  }, {
	    key: '__onClick',
	    value: function __onClick() {
	      // event.stopPropagation();
	      // event.preventDefault();
	
	      if (this._disabled) return;
	
	      if (this._items.length <= 2) this.toggle();else {
	        this.toggleHelper();
	      }
	    }
	  }, {
	    key: 'toggle',
	    value: function toggle() {
	      if (this._selected == 0) this._selected = 1;else this._selected = 0;
	
	      this.onSelect();
	
	      return this.__initButtonInnerHTML().__initClassHTML();
	    }
	  }, {
	    key: 'toggleHelper',
	    value: function toggleHelper() {
	      if (this._helperDisplay) this.hideHelper();else this.showHelper();
	    }
	  }, {
	    key: 'showHelper',
	    value: function showHelper() {
	      (0, _classList2["default"])(this._node).add('ottbp--open');
	      this._helperNode.setAttribute('style', '');
	      this._helperDisplay = true;
	      return this;
	    }
	  }, {
	    key: 'hideHelper',
	    value: function hideHelper() {
	      this._helperNode.setAttribute('style', 'display: none !important;');
	      this._helperDisplay = false;
	      return this;
	    }
	
	    /**
	     *
	     * @param callback
	     * @return {DropDownSelect}
	     */
	
	  }, {
	    key: 'whenSelect',
	    value: function whenSelect(callback) {
	      this._whenSelect = callback;
	      return this;
	    }
	
	    /**
	     *
	     */
	
	  }, {
	    key: 'onSelect',
	    value: function onSelect() {
	      if (this._whenSelect) this._whenSelect(this.$value);
	    }
	
	    /**
	     *
	     * @return {DropDownSelect}
	     */
	
	  }, {
	    key: 'disable',
	    value: function disable() {
	      if (this._disabled) return this;
	      this._disabled = true;
	      return this.__initClassHTML();
	    }
	
	    /**
	     *
	     * @return {DropDownSelect}
	     */
	
	  }, {
	    key: 'enable',
	    value: function enable() {
	      if (!this._disabled) return this;
	      this._disabled = false;
	      return this.__initClassHTML();
	    }
	
	    // /**
	    //  *
	    //  * @return {?string}
	    //  */
	    // get $name() {
	    //   return this._name;
	    // }
	
	    /**
	     *
	     * @return {DropDownSelectOptions}
	     */
	
	  }, {
	    key: 'update',
	
	
	    /**
	     *
	     * @param newValue
	     */
	    value: function update(newValue) {
	      var _this2 = this;
	
	      this._items.forEach(function (item, index) {
	        if (item.value === newValue) {
	          _this2.onSelectFromList(index);
	        }
	      });
	    }
	  }, {
	    key: '$value',
	    get: function get() {
	      return this._items[this._selected];
	    }
	  }]);
	
	  return DropDownSelect;
	}();
	
	exports["default"] = DropDownSelect;

/***/ },
/* 66 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	// import addEvent from '../../mixins/event/addEvent';
	
	var DropDownHelper = function () {
	  /**
	   *
	   * @param {DropDownSelectOptions[]} options
	   * @param {number} selected
	   * @param {HTMLElement} container
	   * @param {EventStore} eventStore
	   */
	  function DropDownHelper(options, selected, container, eventStore) {
	    _classCallCheck(this, DropDownHelper);
	
	    this._eventStore = eventStore;
	    /**
	     *
	     * @type {DropDownSelectOptions}
	     * @private
	     */
	    this._items = options;
	    /**
	     *
	     * @type {number}
	     * @private
	     */
	    this._selected = selected;
	    /**
	     *
	     * @type {HTMLElement}
	     * @private
	     */
	    this._container = container;
	    this.__initHTML().__initEvents();
	  }
	
	  /**
	   *
	   * @return {DropDownHelper}
	   */
	
	
	  _createClass(DropDownHelper, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      // ul.ottbp-select-helper
	      //   li.ottbp-select-helper__item.selected
	      //     a.ottbp-select-helper-item.ottbp-texts--btn
	      //       span.ottbp-select-helper__inner!= value
	      this._node = (0, _Element.docCreate)('ul', 'ottbp-select-helper');
	
	      this.__initItemsHTML();
	
	      this._container.appendChild(this._node);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {DropDownHelper}
	     * @private
	     */
	
	  }, {
	    key: '__initItemsHTML',
	    value: function __initItemsHTML() {
	
	      this._indexes = [this._selected];
	      /**
	       * @this {DropDownHelper}
	       * @type {Array.<*>}
	       */
	      var arr = [this._items[this._selected]].concat(this._items.filter(function (item, index) {
	        if (index !== this._selected) this._indexes.push(index);
	        return index !== this._selected;
	      }, this));
	
	      /**
	       * @type {DocumentFragment}
	       */
	      var fragment = (0, _Element.appendChildren)(document.createDocumentFragment(), arr.map(function (item, index) {
	        var li = (0, _Element.docCreate)('li', ['ottbp-select-helper__item', index == 0 && item.orange ? 'ottbp-selected' : '']);
	        li.innerHTML = '<a data-index="' + index + '" class="ottbp-select-helper-item ottbp-texts--btn"><span class="ottbp-select-helper__inner">' + item.title + '</span></a>';
	        return li;
	      }, this));
	
	      this._node.appendChild(fragment);
	      return this;
	    }
	
	    /**
	     *
	     * @return {DropDownHelper}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      this._eventStore.delegateDocument('click', '.ottbp-select-helper-item', this.__onClick_v2.bind(this), undefined, this._node);
	      // addEvent(this._node, 'click', ::this.__onClick);
	      return this;
	    }
	  }, {
	    key: '__onClick',
	    value: function __onClick(event) {
	      event.stopPropagation();
	      event.preventDefault();
	      var target = event.target;
	      while (target && this._node !== target) {
	        if (target.className.indexOf('ottbp-select-helper-item') >= 0 && target.hasAttributes('data-index')) {
	          this.onSelect(parseInt(target.getAttribute('data-index'), 10));
	          break;
	        }
	        target = target.parentNode;
	      }
	    }
	  }, {
	    key: '__onClick_v2',
	    value: function __onClick_v2(event) {
	      var originalEvent = event.detail.original;
	      var target = event.detail.target;
	
	      originalEvent.stopPropagation();
	      originalEvent.preventDefault();
	
	      if (target.hasAttributes('data-index')) {
	        this.onSelect(parseInt(target.getAttribute('data-index'), 10));
	      }
	    }
	
	    /**
	     *
	     * @param {function} callback
	     * @return {DropDownHelper}
	     */
	
	  }, {
	    key: 'whenSelect',
	    value: function whenSelect(callback) {
	      this._whenSelect = callback;
	      return this;
	    }
	  }, {
	    key: 'onSelect',
	    value: function onSelect(index) {
	      var i = this._indexes[index];
	      if (this._whenSelect) this._whenSelect(i);
	      return this.select(i);
	    }
	
	    /**
	     *
	     * @param {number} index
	     * @return {DropDownHelper}
	     */
	
	  }, {
	    key: 'select',
	    value: function select(index) {
	
	      this._selected = index;
	      this._node.innerHTML = '';
	      this.__initItemsHTML();
	      return this;
	    }
	
	    /**
	     *
	     * @param {DropDownSelectOptions[]} items
	     * @param {number} index
	     * @return {DropDownHelper}
	     */
	
	  }, {
	    key: 'update',
	    value: function update(items, index) {
	      this._items = items;
	      return this.select(index);
	    }
	  }]);
	
	  return DropDownHelper;
	}();
	
	exports["default"] = DropDownHelper;

/***/ },
/* 67 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var routeSelect = {
	  type: 'route',
	  selects: [{
	    title: 'Туда-обратно',
	    orange: true,
	    value: true
	  }, {
	    title: 'В один конец',
	    orange: true,
	    value: false
	  }]
	};
	
	var typeSelect = {
	  type: 'type',
	  selects: [{
	    title: 'Эконом',
	    orange: true,
	    value: 'E'
	  }, {
	    title: 'Бизнес',
	    orange: true,
	    value: 'B'
	  }]
	};
	
	var transferSelect = {
	  type: 'transfer',
	  selects: [{
	    title: 'Любые пересадки',
	    orange: false,
	    value: 'any'
	  }, {
	    title: 'Без пересадок',
	    orange: true,
	    value: 'none'
	  }, {
	    title: 'Не больше одной',
	    orange: true,
	    value: 1
	  }]
	};
	
	var durationSelect = {
	  type: 'duration',
	  selects: [{
	    title: 'Любое количество дней',
	    orange: false,
	    value: 'any'
	  }, {
	    title: 'На 3 дня',
	    orange: true,
	    value: 3
	  }, {
	    title: 'На 5 дней',
	    orange: true,
	    value: 5
	  }, {
	    title: 'На 7 дней',
	    orange: true,
	    value: 7
	  }]
	};
	
	var selects = [routeSelect, typeSelect, transferSelect, durationSelect];
	
	exports["default"] = selects;

/***/ },
/* 68 */
/***/ function(module, exports) {

	'use strict';
	
	// @widthRoute: 220px + (2 * @gutter);
	// @widthType: 136px + (2 * @gutter);
	// @widthTransfer: 220px + (2 * @gutter);
	// @widthDuration: 220px + (2 * @gutter);
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var GUTTER = 6;
	
	var WIDTH = 220 + 136 + 220 + 220 + GUTTER * 4 * 2;
	
	function element(width, type) {
	  var gutter = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : GUTTER;
	
	  return {
	    type: type,
	    width: width,
	    percent: Math.floor((width + gutter * 2) / WIDTH * 10000) / 100
	  };
	}
	
	var WIDTHS = {
	  1011: {
	    extra_small: {
	      row: 4,
	      items: [element(WIDTH - 2 * GUTTER, 'route'), element(0, 'type', 0), element(WIDTH - 2 * GUTTER, 'transfer'), element(WIDTH - 2 * GUTTER, 'duration')]
	    },
	    small: {
	      row: 2,
	      items: [element(WIDTH / 2 - 2 * GUTTER, 'route'), element(0, 'type', 0), element(WIDTH / 2 - 2 * GUTTER, 'transfer'), element(WIDTH - 2 * GUTTER, 'duration')]
	    },
	    medium: {
	      row: 1,
	      items: [element(WIDTH / 3 - 2 * GUTTER, 'route'), element(0, 'type', 0), element(WIDTH / 3 - 2 * GUTTER, 'transfer'), element(WIDTH / 3 - 2 * GUTTER, 'duration')]
	    },
	    big: {
	      row: 1,
	      items: [element(WIDTH / 3 - 2 * GUTTER - 10, 'route'), element(0, 'type', 0), element(WIDTH / 3 - 2 * GUTTER + 20, 'transfer'), element(WIDTH / 3 - 2 * GUTTER - 10, 'duration')]
	    }
	  },
	  1010: {
	    extra_small: {
	      row: 4,
	      items: [element(WIDTH - 2 * GUTTER, 'route'), element(0, 'type', 0), element(WIDTH - 2 * GUTTER, 'transfer'), element(0, 'duration', 0)]
	    },
	    small: {
	      row: 2,
	      items: [element(WIDTH / 2 - 2 * GUTTER, 'route'), element(0, 'type', 0), element(WIDTH / 2 - 2 * GUTTER, 'transfer'), element(0, 'duration', 0)]
	    },
	    medium: {
	      row: 1,
	      items: [element(WIDTH / 2 - 2 * GUTTER, 'route'), element(0, 'type', 0), element(WIDTH / 2 - 2 * GUTTER, 'transfer'), element(0, 'duration', 0)]
	    },
	    big: {
	      row: 1,
	      items: [element(WIDTH / 2 - 2 * GUTTER, 'route'), element(0, 'type', 0), element(WIDTH / 2 - 2 * GUTTER, 'transfer'), element(0, 'duration', 0)]
	    }
	  },
	  1111: {
	    extra_small: {
	      row: 4,
	      items: [element(WIDTH - 2 * GUTTER, 'route'), element(WIDTH - 2 * GUTTER, 'type'), element(WIDTH - 2 * GUTTER, 'transfer'), element(WIDTH - 2 * GUTTER, 'duration')]
	    },
	    small: {
	      row: 2,
	      items: [element(WIDTH / 2 - 2 * GUTTER, 'route'), element(WIDTH / 2 - 2 * GUTTER, 'type'), element(WIDTH / 2 - 2 * GUTTER, 'transfer'), element(WIDTH / 2 - 2 * GUTTER, 'duration')]
	    },
	    medium: {
	      row: 1,
	      items: [element(220 - 50, 'route'), element(136 - 10, 'type'), element(220 + 5, 'transfer'), element(220 + 55, 'duration')]
	    },
	    big: {
	      row: 1,
	      items: [element(220, 'route'), element(136, 'type'), element(220, 'transfer'), element(220, 'duration')]
	    }
	  },
	  1110: {
	    extra_small: {
	      row: 3,
	      items: [element(WIDTH - 2 * GUTTER, 'route'), element(WIDTH - 2 * GUTTER, 'type'), element(WIDTH - 2 * GUTTER, 'transfer'), element(0, 'duration', 0)]
	    },
	    small: {
	      row: 2,
	      items: [element(WIDTH / 2 - 2 * GUTTER, 'route'), element(WIDTH / 2 - 2 * GUTTER, 'type'), element(WIDTH - 2 * GUTTER, 'transfer'), element(0, 'duration', 0)]
	    },
	    medium: {
	      row: 1,
	      items: [element(220 + (220 + GUTTER * 2) / 3, 'route'), element(136 + (220 + GUTTER * 2) / 3, 'type'), element(220 + (220 + GUTTER * 2) / 3, 'transfer'), element(0, 'duration', 0)]
	    },
	    big: {
	      row: 1,
	      items: [element(220 + (220 + GUTTER * 2) / 3, 'route'), element(136 + (220 + GUTTER * 2) / 3, 'type'), element(220 + (220 + GUTTER * 2) / 3, 'transfer'), element(0, 'duration', 0)]
	    }
	  }
	};
	
	exports["default"] = WIDTHS;

/***/ },
/* 69 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	// import Results from '../blocks/results';
	
	var _loader = __webpack_require__(70);
	
	var _loader2 = _interopRequireDefault(_loader);
	
	var _PickerContainer = __webpack_require__(71);
	
	var _PickerContainer2 = _interopRequireDefault(_PickerContainer);
	
	var _ResultsContainer = __webpack_require__(80);
	
	var _ResultsContainer2 = _interopRequireDefault(_ResultsContainer);
	
	var _constant = __webpack_require__(51);
	
	var _Element = __webpack_require__(7);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	// import Eve from '../mixins/Event';
	
	var Footer = function () {
	  /**
	   *
	   * @param container
	   * @param {EventStore} eventStore
	   * @param {Theme} theme
	   */
	  function Footer(container, eventStore, theme) {
	    _classCallCheck(this, Footer);
	
	    this._eventStore = eventStore;
	    this._theme = theme;
	    this._container = container;
	    this._visible = false;
	
	    this._loaded = false;
	
	    this._showDate = false;
	    this._showResults = false;
	
	    this.__initHTML();
	
	    // this._visible = true;
	  }
	
	  _createClass(Footer, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      this._node = (0, _Element.docCreate)('div', 'ottbp-footer');
	
	      this._containerLoader = (0, _Element.docCreate)('div', 'ottbp-footer__loader');
	      this._containerDatePicker = (0, _Element.docCreate)('div', 'ottbp-footer__date');
	      this._containerResults = (0, _Element.docCreate)('div', 'ottbp-footer__results');
	
	      this.__initItems().__initEvents();
	
	      this._node.appendChild(this._containerLoader);
	      this._node.appendChild(this._containerDatePicker);
	      this._node.appendChild(this._containerResults);
	
	      this.render();
	
	      this._container.appendChild(this._node);
	    }
	
	    /**
	     *
	     * @return {Footer}
	     * @private
	     */
	
	  }, {
	    key: '__initItems',
	    value: function __initItems() {
	      this._loader = new _loader2["default"](this._containerLoader);
	      this._picker = new _PickerContainer2["default"](this._containerDatePicker, this._eventStore, this._theme);
	      this._results = new _ResultsContainer2["default"](this._containerResults, this._eventStore);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Footer}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      this._eventStore.on(['Controller::load:results', 'Controller::show:results', 'DatePicker::resize', 'Results::resize'], function (event) {
	        var object = event && event.detail ? event.detail.object : {};
	
	        if (this._visible || !object || !object.$empty) {
	          this._visible = true;
	        }
	        this.render();
	      }.bind(this));
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'show',
	    value: function show() {
	      this.render();
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'render',
	    value: function render() {
	
	      var height = 0;
	      var resultsWidth = this._medium ? 'width: ' + (this._medium - 400) + 'px !important;' : '';
	      if (this.$isVisible) {
	        if (this.$isLoading) {
	
	          height = this._containerLoader.offsetHeight;
	
	          if (this._medium) {
	            this._containerDatePicker.setAttribute('style', 'display: none !important;');
	            this._containerLoader.setAttribute('style', '');
	          } else {
	            this._containerDatePicker.setAttribute('style', '');
	            height += this._picker.$height;
	            this._containerLoader.setAttribute('style', 'margin-top: ' + this._picker.$height + 'px !important');
	          }
	
	          this._containerResults.setAttribute('style', 'display: none !important; ' + resultsWidth);
	          this._loader.show();
	        } else {
	          this._containerDatePicker.setAttribute('style', '');
	          this._containerResults.setAttribute('style', resultsWidth);
	
	          if (this._medium) {
	            height = Math.max( /*this._date.height +*/this._picker.$height, this._results.$height);
	          } else {
	            height = this._results.$height + this._picker.$height;
	          }
	
	          this._loader.hide(function () {
	            /**
	             * @this {Footer}
	             */
	            this._containerLoader.setAttribute('style', 'display: none !important');
	          }.bind(this));
	        }
	      } else {
	        if (this._medium) this._containerDatePicker.setAttribute('style', 'display: none !important;');else this._containerDatePicker.setAttribute('style', '');
	
	        this._containerResults.setAttribute('style', 'display: none !important; ' + resultsWidth);
	
	        this._loader.hide();
	      }
	
	      this._node.setAttribute('style', 'height: ' + height + 'px !important;');
	    }
	  }, {
	    key: 'resize',
	    value: function resize(containerWidth) {
	      if (containerWidth >= _constant.CONTAINER_SIZE_MEDIUM) {
	        this._medium = containerWidth;
	      } else {
	        this._medium = false;
	      }
	
	      this._picker.resize(containerWidth);
	      this.render();
	    }
	
	    /**
	     *
	     * @return {FormatMainForm}
	     */
	
	  }, {
	    key: 'update',
	
	
	    /**
	     *
	     * @param {FormatMainForm} options
	     */
	    value: function update(options) {
	      this._picker.update(options);
	    }
	  }, {
	    key: '$isVisible',
	    get: function get() {
	      return this._visible;
	    }
	  }, {
	    key: '$isLoading',
	    get: function get() {
	      return this._results.$isLoading && this._picker.$isLoading;
	    }
	  }, {
	    key: '$value',
	    get: function get() {
	      return {
	        date: this._picker.$value
	      };
	    }
	  }]);
	
	  return Footer;
	}();
	
	exports["default"] = Footer;

/***/ },
/* 70 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _search = __webpack_require__(63);
	
	var _search2 = _interopRequireDefault(_search);
	
	var _classList = __webpack_require__(49);
	
	var _classList2 = _interopRequireDefault(_classList);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Loader = function () {
	  function Loader(container) {
	    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	    _classCallCheck(this, Loader);
	
	    this._continer = container;
	    this._options = options;
	
	    this.initHTML();
	  }
	
	  _createClass(Loader, [{
	    key: 'initHTML',
	    value: function initHTML() {
	      var classes = ['ottbp-loader', 'ottbp-loader--hide'];
	      if (this._options.small) classes.push('ottbp-loader--small');
	      this._node = (0, _Element.docCreate)('div', classes);
	
	      var icon = '';
	      if (this._options.small) icon = new _search2["default"]().resize(.4 * 39, .4 * 39).toString();else icon = new _search2["default"]().resize(.8 * 39, .8 * 39).toString();
	
	      var inner = '<div class="ottbp-loader__back"><div class="ottbp-loader__back__over"></div></div>';
	      inner += '<div class="ottbp-loader__front"><div class="ottbp-loader__front__icon">' + icon + '</div></div>';
	
	      this._node.innerHTML = inner;
	
	      this._continer.appendChild(this._node);
	      return this;
	    }
	  }, {
	    key: 'hide',
	    value: function hide(callback) {
	      (0, _classList2["default"])(this._node).add('ottbp-loader--hide');
	      if (callback) setTimeout(callback, 500);
	    }
	  }, {
	    key: 'show',
	    value: function show(callback) {
	      if (callback) {
	        callback(this.__show.bind(this));
	      } else this.__show();
	    }
	  }, {
	    key: '__show',
	    value: function __show() {
	      (0, _classList2["default"])(this._node).remove('ottbp-loader--hide');
	    }
	  }]);
	
	  return Loader;
	}();
	
	exports["default"] = Loader;

/***/ },
/* 71 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _DatePicker = __webpack_require__(72);
	
	var _DatePicker2 = _interopRequireDefault(_DatePicker);
	
	var _constant = __webpack_require__(51);
	
	var _classList = __webpack_require__(49);
	
	var _classList2 = _interopRequireDefault(_classList);
	
	var _Element = __webpack_require__(7);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var PickerContainer = function () {
	  /**
	   *
	   * @param container
	   * @param {EventStore} eventStore
	   * @param {Theme} theme
	   */
	  function PickerContainer(container, eventStore, theme) {
	    _classCallCheck(this, PickerContainer);
	
	    this._eventStore = eventStore;
	    this._theme = theme;
	
	    this._container = container;
	
	    this._mini = false;
	    this._open = false;
	
	    this.__initHTML().__initEvents();
	
	    this._container.appendChild(this._node);
	  }
	
	  /**
	   *
	   * @return {PickerContainer}
	   * @private
	   */
	
	
	  _createClass(PickerContainer, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      /**
	       *
	       * @type {HTMLElement}
	       * @private
	       */
	      this._node = (0, _Element.docCreate)('div', 'ottbp-container-picker');
	
	      var _wrapper = (0, _Element.docCreate)('div', 'ottbp-container-picker__wrap');
	
	      var _btn = (0, _Element.docCreate)('div', 'ottbp-container-picker__btn');
	      this._containerPicker = (0, _Element.docCreate)('div', 'ottbp-container-picker__picker');
	
	      this._btn = (0, _Element.docCreate)('button', 'ottbp-container-picker-btn');
	      this._btnClean = (0, _Element.docCreate)('button', 'ottbp-container-picker-btn ottbp-container-picker-btn--clean');
	      this._btnClean.innerHTML = 'сбросить';
	      this._btn.innerText = 'Выбрать дату';
	
	      this._btn.setAttribute('style', 'width:100% !important; border-radius: 2px !important;');
	      this._btnClean.setAttribute('style', 'width:0 !important;');
	
	      _btn.appendChild(this._btn);
	      _btn.appendChild(this._btnClean);
	
	      _wrapper.appendChild(_btn);
	      _wrapper.appendChild(this._containerPicker);
	      /**
	       *
	       * @type {DatePicker}
	       * @private
	       */
	      this._picker = new _DatePicker2["default"](this._containerPicker, this._eventStore, this._theme);
	
	      this._node.appendChild(_wrapper);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {PickerContainer}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      var _this = this;
	
	      // Eve.addEvent(this._btn, 'click', ::this.__onClickBtn_v2);
	      // Eve.addEvent(this._btnClean, 'click', ::this.__onClean);
	
	      this._eventStore.delegateDocument('click', this._node, function (event) {
	        var target = event.detail.original.target;
	
	        while (target != _this._node) {
	          if (target === _this._btn) _this.__onClickBtn_v2(event.detail.original);else if (target === _this._btnClean) _this.__onClean(event.detail.original);
	          target = target.parentNode;
	        }
	      }, this.__onClickAnyWhere.bind(this));
	
	      this._eventStore.on('DatePicker::title:change', this.__onChangeText.bind(this));
	      return this;
	    }
	  }, {
	    key: '__onChangeText',
	    value: function __onChangeText(event) {
	      var text = event.detail.text;
	      this._canRemove = event.detail.canRemove;
	
	      this._btn.innerHTML = text;
	
	      this.render();
	    }
	  }, {
	    key: '__onClickAnyWhere',
	    value: function __onClickAnyWhere() {
	
	      if (this._mini) {
	        this._open = false;
	        this.render();
	      }
	    }
	  }, {
	    key: '__onClickBtn_v2',
	    value: function __onClickBtn_v2(event) {
	
	      event.stopPropagation();
	      event.preventDefault();
	
	      if (this._mini) {
	        this._open = !this._open;
	      }
	      this.render();
	    }
	  }, {
	    key: '__onClean',
	    value: function __onClean() {
	      this._eventStore.dispatch('DatePicker::do:clean');
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'render',
	    value: function render() {
	      if (this._mini) {
	        if (this._open) {
	          this._containerPicker.setAttribute('style', 'display: block !important;');
	          (0, _classList2["default"])(this._btn).add('ottbp-container-picker-btn--focus');
	        } else {
	          this._containerPicker.setAttribute('style', '');
	          (0, _classList2["default"])(this._btn).remove('ottbp-container-picker-btn--focus');
	        }
	      } else {
	        (0, _classList2["default"])(this._btn).remove('ottbp-container-picker-btn--focus');
	        this._containerPicker.setAttribute('style', '');
	      }
	
	      var rad = '2px';
	
	      if (this._canRemove) {
	        var w1 = this._extraSmall ? 70 : this._small ? 77 : 85,
	            w2 = 100 - w1;
	        this._btn.setAttribute('style', 'width:' + w1 + '%!important; border-radius: ' + (this._open ? rad + ' 0 0 0' : rad + ' 0 0 ' + rad) + '!important; ' + (this._open ? 'box-shadow: inset 1px 1px 0 0 #E0E0E0, inset 0 -1px 0 0 #E0E0E0!important;' : ''));
	        this._btnClean.setAttribute('style', 'width:' + w2 + '%!important; border-radius: ' + (this._open ? '0 ' + rad + ' 0 0' : '0 ' + rad + ' ' + rad + ' 0') + '!important; ' + (this._open ? 'box-shadow: inset -1px 1px 0 0 #E0E0E0, inset 0 -1px 0 0 #E0E0E0!important;' : ''));
	      } else {
	        this._btn.setAttribute('style', 'width:100%!important; border-radius: ' + (this._open ? rad + ' ' + rad + ' 0 0' : '' + rad) + '!important; ' + (this._open ? 'box-shadow: inset 0 0 0 1px #E0E0E0!important;' : ''));
	        this._btnClean.setAttribute('style', 'width:0% !important;');
	      }
	
	      return this;
	    }
	  }, {
	    key: 'resize',
	    value: function resize(containerWidth) {
	      if (containerWidth >= _constant.CONTAINER_SIZE_MEDIUM) {
	        this._mini = false;
	        this._open = false;
	      } else {
	
	        this._extraSmall = containerWidth < _constant.CONTAINER_SIZE_EXTRA_SMALL;
	        this._small = containerWidth < _constant.CONTAINER_SIZE_SMALL;
	
	        this._mini = true;
	      }
	
	      this.render();
	    }
	
	    /**
	     *
	     * @param {FormatMainForm} options
	     */
	
	  }, {
	    key: 'update',
	    value: function update(options) {
	      this._picker.updateValue(options);
	    }
	  }, {
	    key: '$isLoading',
	    get: function get() {
	      return this._picker.$isLoading;
	    }
	
	    /**
	     *
	     * @return {{start: ?Date inMonth: ({year: number month: number}|false) end: ?Date}}
	     */
	
	  }, {
	    key: '$value',
	    get: function get() {
	      return this._picker.$value;
	    }
	
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '$height',
	    get: function get() {
	      if (this._mini) return this._node.offsetHeight;
	      return this._picker.$height;
	    }
	  }]);
	
	  return PickerContainer;
	}();
	
	exports["default"] = PickerContainer;

/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _Date = __webpack_require__(40);
	
	var _Title = __webpack_require__(73);
	
	var _Title2 = _interopRequireDefault(_Title);
	
	var _Calendar = __webpack_require__(75);
	
	var _Calendar2 = _interopRequireDefault(_Calendar);
	
	var _MonthsList = __webpack_require__(79);
	
	var _MonthsList2 = _interopRequireDefault(_MonthsList);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var DatePicker = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {EventStore} eventStore
	   * @param {Theme} theme
	   */
	  function DatePicker(container, eventStore, theme) {
	    _classCallCheck(this, DatePicker);
	
	    this._eventStore = eventStore;
	    this._theme = theme;
	    /**
	     *
	     * @type {HTMLElement}
	     * @private
	     */
	    this._container = container;
	
	    this.__initHTML().__initInner().__initEvents();
	    this._container.appendChild(this._node);
	    /**
	     *
	     * @type {?CalendarValue}
	     * @private
	     */
	    this._date = null;
	  }
	
	  /**
	   *
	   * @return {DatePicker}
	   * @private
	   */
	
	
	  _createClass(DatePicker, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      this._node = (0, _Element.docCreate)('div', 'ottbp-date');
	
	      this.__initHeaderHTML().__initBodyHTML();
	
	      this._node.appendChild(this._header);
	      this._node.appendChild(this._body);
	
	      this.__initHeaderInnerHTML();
	
	      return this;
	      // return this.initMonthList().showMonth();
	    }
	
	    /**
	     *
	     * @return {DatePicker}
	     * @private
	     */
	
	  }, {
	    key: '__initHeaderHTML',
	    value: function __initHeaderHTML() {
	      this._header = (0, _Element.docCreate)('header', 'ottbp-date__header');
	      this._title = new _Title2["default"](this._header, this._eventStore);
	      return this;
	    }
	
	    /**
	     *
	     * @return {DatePicker}
	     * @private
	     */
	
	  }, {
	    key: '__initHeaderInnerHTML',
	    value: function __initHeaderInnerHTML() {
	      this._title.update(this._date, this._inMonth, this._isRange);
	      return this;
	    }
	
	    /**
	     *
	     * @return {DatePicker}
	     * @private
	     */
	
	  }, {
	    key: '__initBodyHTML',
	    value: function __initBodyHTML() {
	      this._body = (0, _Element.docCreate)('section', 'ottbp-date__body');
	      var inner = (0, _Element.docCreate)('div', 'ottbp-date__body__inner');
	
	      this._containerLoader = (0, _Element.docCreate)('div', 'ottbp-date__loader');
	      this._containerCalendar = (0, _Element.docCreate)('div', 'ottbp-date__calendar');
	      this._containerMonth = (0, _Element.docCreate)('div', 'ottbp-date__months');
	
	      // this._loader = new Loader(this._containerLoader);
	
	      // inner.appendChild(this._containerLoader);
	      inner.appendChild(this._containerCalendar);
	      inner.appendChild(this._containerMonth);
	
	      // new CalendarV2(inner);
	
	      this._body.appendChild(inner);
	      return this;
	    }
	
	    /**
	     *
	     * @return {DatePicker}
	     * @private
	     */
	
	  }, {
	    key: '__initInner',
	    value: function __initInner() {
	      this.__initCalendar().__initMonthList();
	      // скрытваем календарь
	      this._containerMonth.style.display = '';
	      this._containerCalendar.setAttribute('style', 'display:none !important;');
	      return this;
	    }
	
	    /**
	     *
	     * @return {DatePicker}
	     * @private
	     */
	
	  }, {
	    key: '__initCalendar',
	    value: function __initCalendar() {
	      this._calendar = new _Calendar2["default"](_Date.currentDate.getMonth(), _Date.currentDate.getFullYear(), this._containerCalendar, {
	        isRange: this._isRange,
	        duration: this._duration
	      }, this._eventStore, this._theme);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {DatePicker}
	     * @private
	     */
	
	  }, {
	    key: '__initMonthList',
	    value: function __initMonthList() {
	
	      /**
	       *
	       * @type {MonthsList}
	       * @private
	       */
	      this._monthList = new _MonthsList2["default"](this._containerMonth, this._eventStore, this._theme);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {DatePicker}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	
	      this._calendar.whenSelect(this.__onDateSelect.bind(this));
	      this._calendar.whenHover(this.__onDateHover.bind(this));
	      this._calendar.whenMonthClick(this.__onMonthBack.bind(this));
	
	      this._monthList.whenSelect(this.__onMonthSelect.bind(this));
	
	      // this._title.whenClean(::this.__onRemoveSelected);
	      this._eventStore.on('DatePicker::do:clean', this.__onRemoveSelected.bind(this));
	
	      this._eventStore.on([/*`Form::init`,*/'Selects::change'], function (event) {
	        var value = event.detail.value;
	        this.update({
	          isRange: value.route,
	          duration: value.duration !== 'any' ? value.duration : null
	        });
	      }.bind(this), 1);
	
	      this._eventStore.on(['Form::init'], function (event) {
	        var data = event.detail;
	        this.update({
	          isRange: data.selectors.route,
	          duration: data.selectors.duration !== 'any' ? data.selectors.duration : null
	        });
	      }.bind(this));
	
	      return this;
	    }
	
	    // EVENTS
	
	    /***
	     *
	     * @param {MonthsOptions} item
	     * @private
	     */
	
	  }, {
	    key: '__onMonthSelect',
	    value: function __onMonthSelect(item) {
	      // this.initCalendar(item.month + 1, item.year).showCalendar();
	      // this._eventStore.dispatch('DateSelect::select:month');
	
	      this._inMonth = {
	        year: item.year,
	        month: item.month
	      };
	      this._eventStore.dispatch('DatePicker::changed', { date: this.$value });
	      this.__updateCalendar().__showCalendar();
	    }
	
	    /**
	     *
	     * @param {CalendarValue} dateData
	     * @private
	     */
	
	  }, {
	    key: '__onDateHover',
	    value: function __onDateHover(dateData) {
	      this._date = dateData;
	      this.__initHeaderInnerHTML();
	    }
	
	    /**
	     *
	     * @private
	     */
	
	  }, {
	    key: '__onMonthBack',
	    value: function __onMonthBack() {
	      this._inMonth = null;
	      this._eventStore.dispatch('DatePicker::changed', { date: this.$value });
	      this.__initHeaderInnerHTML().__showMonth();
	    }
	
	    /**
	     *
	     * @param {CalendarValue} dateData
	     * @private
	     */
	
	  }, {
	    key: '__onDateSelect',
	    value: function __onDateSelect(dateData) {
	      var same = JSON.stringify(dateData) == JSON.stringify(this._date);
	      this._date = dateData;
	
	      if (!same) {
	        this._eventStore.dispatch('DatePicker::changed', { date: this.$value });
	        this.__initHeaderInnerHTML();
	      }
	    }
	
	    /**
	     *
	     * @private
	     */
	
	  }, {
	    key: '__onRemoveSelected',
	    value: function __onRemoveSelected() {
	      this._calendar.clear();
	      this._date = null;
	
	      this._eventStore.dispatch('DatePicker::changed', { date: this.$value });
	    }
	
	    // some updates
	
	    /**
	     *
	     * @param {{[isRange]:?boolean [duration]:boolean|null [string]:*}} [options]
	     * @return {DatePicker}
	     * @private
	     */
	
	  }, {
	    key: '__parseOptions',
	    value: function __parseOptions() {
	      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	      /**
	       *
	       * @type {boolean}
	       * @private
	       */
	      this._isRange = !!options.isRange;
	      /**
	       * @type {string|number|null}
	       */
	      this._duration = this._isRange ? options.duration || null : null;
	
	      return this;
	    }
	
	    /**
	     *
	     * @param {{[isRange]:boolean [duration]:boolean|null [string]:*}} options
	     * @return {DatePicker}
	     */
	
	  }, {
	    key: 'update',
	    value: function update(options) {
	      this.__parseOptions(options).__updateCalendar().__initHeaderInnerHTML();
	      return this;
	    }
	
	    // Calendar
	    /**
	     *
	     * @param {number} [month]
	     * @param {number} [year]
	     * @return {DatePicker}
	     * @private
	     */
	
	  }, {
	    key: '__updateCalendar',
	    value: function __updateCalendar(month, year) {
	
	      if (this._inMonth) {
	        if (typeof month === 'undefined') {
	          month = this._inMonth.month;
	        }
	        if (typeof year === 'undefined') {
	          year = this._inMonth.year;
	        }
	      } else {
	        if (typeof month === 'undefined') {
	          month = _Date.currentDate.getMonth() + 1;
	        }
	        if (typeof year === 'undefined') {
	          year = _Date.currentDate.getFullYear();
	        }
	      }
	
	      this._calendar.update(month, year, {
	        isRange: this._isRange,
	        duration: this._duration
	
	      });
	
	      return this;
	    }
	
	    // DOM
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '__showMonth',
	
	
	    /**
	     *
	     * @return {DatePicker}
	     */
	    value: function __showMonth() {
	      this._containerMonth.style.display = '';
	      this._containerCalendar.setAttribute('style', 'display:none !important;');
	      this._eventStore.dispatch('DatePicker::resize', { object: this });
	      return this;
	    }
	
	    /**
	     *
	     * @return {DatePicker}
	     */
	
	  }, {
	    key: '__showCalendar',
	    value: function __showCalendar() {
	      this._containerMonth.setAttribute('style', 'display:none !important;');
	      this._containerCalendar.style.display = '';
	      this._eventStore.dispatch('DatePicker::resize', { object: this });
	      return this;
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: 'updateValue',
	
	
	    /**
	     *
	     * @param {FormatMainForm} options
	     */
	    value: function updateValue(options) {
	
	      /**
	       *
	       * @type {CalendarOptions}
	       */
	      var calendarOptions = {
	        isRange: options.selectors.route,
	        dateStart: options.date.start,
	        dateEnd: options.date.end,
	        duration: options.selectors.duration
	      };
	
	      var month = options.date.inMonth;
	
	      if (month) {
	        this.__onMonthSelect(month);
	        this._calendar.update(month.month, month.year, calendarOptions);
	      }
	    }
	  }, {
	    key: '$height',
	    get: function get() {
	      return !this._loading ? this._node.offsetHeight : this._containerLoader.offsetHeight + 30;
	    }
	  }, {
	    key: '$isLoading',
	    get: function get() {
	      return !this._monthList.$isInit /*&& !this._monthList.isLoading*/;
	    }
	  }, {
	    key: '$empty',
	    get: function get() {
	      /**
	       *
	       * @type {{start: Date, inMonth: (*), end: Date}}
	       */
	      var value = this.$value;
	      return !(value.start && value.end);
	    }
	
	    // Value
	    /**
	     *
	     * @return {{start: ?Date inMonth: ({year: number month: number}|false) end: ?Date}}
	     */
	
	  }, {
	    key: '$value',
	    get: function get() {
	      /**
	       *
	       * @type {CalendarValue}
	       */
	      var val = this._calendar.$value;
	
	      /**
	       * @type {{year:number,month:number}|false}
	       */
	      var inMonth = this._inMonth ? this._inMonth : false;
	      /**
	       *
	       * @type {{start: (?Date), inMonth: ({year:number,month:number}|false), end: (?Date)}}
	       */
	      var value = {
	        start: val.select,
	        inMonth: inMonth,
	        end: null
	      };
	
	      if (this._isRange) value = {
	        start: val.range.start,
	        inMonth: inMonth,
	        end: val.range.end
	      };
	
	      this._date = val;
	
	      return value;
	    }
	  }]);
	
	  return DatePicker;
	}();
	
	exports["default"] = DatePicker;

/***/ },
/* 73 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _Format = __webpack_require__(17);
	
	var _l8n = __webpack_require__(74);
	
	var _l8n2 = _interopRequireDefault(_l8n);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Title = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {EventStore} eventStore
	   */
	  function Title(container, eventStore) {
	    _classCallCheck(this, Title);
	
	    this._eventStore = eventStore;
	    this._container = container;
	    this.__initHTML().__initEvents();
	    this._container.appendChild(this._node);
	  }
	
	  /**
	   *
	   * @return {Title}
	   * @private
	   */
	
	
	  _createClass(Title, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      this._node = (0, _Element.docCreate)('div');
	      return this;
	    }
	
	    /**
	     *
	     * @return {Title}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      this._node.addEventListener('click', function (event) {
	        var target = event.target;
	        while (target && target !== this._node) {
	          if (target.className.indexOf('ottbp-btn-delete') > -1) {
	            this.__clearDateSelect();
	            return false;
	          }
	          target = target.parentNode;
	        }
	        return true;
	      }.bind(this));
	      return this;
	    }
	
	    // whenClean(callback) {
	    //   this._onClean = callback;
	    //   return this;
	    // }
	
	  }, {
	    key: '__clearDateSelect',
	    value: function __clearDateSelect() {
	      this.update();
	      this._eventStore.dispatch('DatePicker::do:clean');
	      // if (this._onClean)
	      //   this._onClean();
	    }
	
	    /**
	     *
	     * @param {CalendarValue} [date]
	     * @param {null|{year:number month:number}} [inMonth]
	     * @param {boolean} [isRange=false]
	     * @return {Title}
	     */
	
	  }, {
	    key: 'update',
	    value: function update(date, inMonth) {
	      var isRange = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
	
	      var text = '<span>' + _l8n2["default"].DatePicker.title.empty + '</span>',
	          defaultText = text;
	      var canRemove = false;
	
	      if (date) {
	
	        if (isRange) {
	
	          var start = '<span class="ottbp-gray">туда</span>',
	              end = '<span class="ottbp-gray">обратно</span>';
	          var selectRange = date.range,
	              hoverRange = date.rangeHover;
	
	          if (selectRange.start) {
	            canRemove = true;
	          }
	          if (hoverRange.start) {
	            start = '<span class="ottbp-gray">' + (0, _Format.dateString)(hoverRange.start) + '</span>';
	          } else if (selectRange.start) {
	            start = '<span class="ottbp-black">' + (0, _Format.dateString)(selectRange.start) + '</span>';
	          }
	
	          if (hoverRange.end) {
	            if (this._duration) {
	              start = '<span class="ottbp-gray">' + (0, _Format.dateString)(hoverRange.start) + '</span>';
	            } else {
	              start = '<span class="ottbp-black">' + (0, _Format.dateString)(hoverRange.start) + '</span>';
	            }
	            end = '<span class="ottbp-gray">' + (0, _Format.dateString)(hoverRange.end) + '</span>';
	          } else if (selectRange.end && !hoverRange.start) {
	            end = '<span class="ottbp-black">' + (0, _Format.dateString)(selectRange.end) + '</span>';
	          }
	
	          var newStart = start !== '<span class="ottbp-gray">туда</span>',
	              newEnd = end !== '<span class="ottbp-gray">обратно</span>';
	
	          if (newStart || newEnd) {
	            text = start + ' &mdash; ' + end;
	          }
	        } else {
	          if (date.hover) {
	            text = '<span class="ottbp-gray">' + (0, _Format.dateString)(date.hover) + '</span>';
	          } else if (date.select) {
	
	            canRemove = true;
	            text = '<span class="ottbp-black">' + (0, _Format.dateString)(date.select) + '</span>';
	          }
	        }
	
	        // if (selectedStart /*&& selectedEnd*/) {
	        //   canRemove = true;
	        // }
	
	        // if (data.st)
	        //
	      }
	
	      if (text === defaultText && inMonth) {
	        text = '<span>\u0412 ' + _l8n2["default"].month.praepositionalis[inMonth.month] + '</span>';
	      }
	
	      if (this._stringCache != text) {
	        this._stringCache = text;
	        this._node.innerHTML = '<div class="ottbp-date__title ottbp-texts--title"><span class="ottbp-date__title__inner">' + text + (canRemove ? '<button class="ottbp-btn ottbp-btn-delete">&times;</button>' : '') + '</span></div>';
	        this._eventStore.dispatch('DatePicker::title:change', {
	          text: text,
	          canRemove: canRemove
	        });
	      }
	      return this;
	    }
	  }]);
	
	  return Title;
	}();
	
	exports["default"] = Title;

/***/ },
/* 74 */
/***/ function(module, exports) {

	'use strict';
	/**
	 *
	 * @type {{full: string[], short: string[], praepositionalis: string[], genetive: string[]}}
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var month = exports.month = {
	  full: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
	  "short": ['янв', 'фев', 'март', 'апр', 'май', 'июнь', 'июль', 'авг', 'сен', 'окт', 'ноя', 'дек'],
	  praepositionalis: ['январе', 'феврале', 'марте', 'апреле', 'мае', 'июне', 'июле', 'августе', 'сентябре', 'октябре', 'ноябре', 'декабре'],
	  genetive: ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']
	};
	/**
	 *
	 * @type {{abbr: string[]}}
	 */
	var week = exports.week = {
	  abbr: ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПН', 'СБ', 'ВС']
	};
	
	exports["default"] = {
	  month: month,
	  week: week,
	  DatePicker: {
	    title: {
	      empty: 'Выберите дату путешествия'
	    }
	  },
	  Direction: {
	    accross: {
	      none: 'без&nbsp;пересадок',
	      one_two_five: ['пересадка', 'пересадки', 'пересадок']
	    },
	    time: {
	      hour: 'ч',
	      min: 'мин'
	    }
	  }
	};

/***/ },
/* 75 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	// import Eve from './../../mixins/Event';
	
	var _Element = __webpack_require__(7);
	
	var _Date = __webpack_require__(40);
	
	var _Date2 = _interopRequireDefault(_Date);
	
	var _Prices = __webpack_require__(42);
	
	var _Prices2 = _interopRequireDefault(_Prices);
	
	var _PriceRange = __webpack_require__(76);
	
	var _PriceRange2 = _interopRequireDefault(_PriceRange);
	
	var _PriceGradient = __webpack_require__(77);
	
	var _PriceGradient2 = _interopRequireDefault(_PriceGradient);
	
	var _classList = __webpack_require__(49);
	
	var _classList2 = _interopRequireDefault(_classList);
	
	var _day = __webpack_require__(78);
	
	var _day2 = _interopRequireDefault(_day);
	
	var _l8n = __webpack_require__(74);
	
	var _l8n2 = _interopRequireDefault(_l8n);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Calendar = function () {
	  /**
	   *
	   * @param month
	   * @param year
	   * @param container
	   * @param {CalendarOptions} [options={}]
	   * @param {EventStore} eventStore
	   * @param {Theme} theme
	   */
	  function Calendar(month, year, container) {
	    var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
	    var eventStore = arguments[4];
	    var theme = arguments[5];
	
	    _classCallCheck(this, Calendar);
	
	    this._eventStore = eventStore;
	    /**
	     *
	     * @type {Theme}
	     * @private
	     */
	    this._theme = theme;
	
	    this._month = month;
	    this._year = year;
	
	    this._startMonth = new Date(year, month, 1);
	    this._days = [];
	    this._container = container;
	
	    this._cacheDay = '';
	
	    this.__parseOptions(options).__initHTML().__initEvents();
	  }
	
	  _createClass(Calendar, [{
	    key: '__initHTML',
	
	
	    /**
	     *
	     * @return {Calendar}
	     */
	    value: function __initHTML() {
	      this._over = (0, _Element.docCreate)('div');
	      // let fragment = document.createDocumentFragment();
	      this._node = (0, _Element.docCreate)('table', 'ottbp-calendar');
	      this._nodePriceRange = (0, _Element.docCreate)('div', 'ottbp-calendar-price_range');
	
	      this._tbody = (0, _Element.docCreate)('tbody');
	      this._thead = (0, _Element.docCreate)('thead');
	
	      this.__createSelectMonth().__createDaysOfWeekNames().__createMonthName().__createPriceRange().__createWeeks();
	
	      this._node.appendChild(this._thead);
	      this._node.appendChild(this._tbody);
	
	      this._over.appendChild(this._node);
	      this._over.appendChild(this._nodePriceRange);
	      this._container.appendChild(this._over);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Calendar}
	     * @private
	     */
	
	  }, {
	    key: '__createPriceRange',
	    value: function __createPriceRange() {
	      this._priceRange = new _PriceRange2["default"](this._nodePriceRange);
	      return this;
	    }
	
	    /**
	     *
	     * @return {Calendar}
	     * @private
	     */
	
	  }, {
	    key: '__createSelectMonth',
	    value: function __createSelectMonth() {
	      // let tr = docCreate('tr', 'ottbp-cal-month');
	      // tr.innerHTML = `<td class="ottbp-cal-month__select" colspan="7"><a href="javascript:void(0);">Выбрать месяц</a></td>`;
	      // this._tbody.appendChild(tr);
	      return this;
	    }
	
	    /**
	     *
	     * @return {Calendar}
	     * @private
	     */
	
	  }, {
	    key: '__createDaysOfWeekNames',
	    value: function __createDaysOfWeekNames() {
	      var tr = (0, _Element.docCreate)('tr', 'ottbp-cal-days ottbp-texts--week-days');
	
	      tr.innerHTML = _l8n2["default"].week.abbr.map(function (name, index) {
	        return '<td class="ottbp-cal-day ' + (index >= 5 ? 'ottbp-cal-weekend' : '') + '">' + name + '</td>';
	      }).join('');
	
	      this._thead.appendChild(tr);
	      // this._tbody.appendChild(tr);
	      return this;
	    }
	
	    /**
	     *
	     * @return {Calendar}
	     * @private
	     */
	
	  }, {
	    key: '__createMonthName',
	    value: function __createMonthName() {
	      var tr = (0, _Element.docCreate)('tr', 'ottbp-cal-month');
	      var monthString = _l8n2["default"].month.full[this._startMonth.getMonth()] + (this._startMonth.getFullYear() !== new Date().getFullYear() ? ' ' + this._startMonth.getFullYear() : '');
	
	      tr.innerHTML = '<td class="ottbp-cal-month__name" colspan="7"><span>' + monthString + '</span><a href="javascript:void(0);">\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u043C\u0435\u0441\u044F\u0446</a></td>';
	      this._tbody.appendChild(tr);
	      return this;
	    }
	
	    /**
	     *
	     * @return {Calendar}
	     * @private
	     */
	
	  }, {
	    key: '__createWeeks',
	    value: function __createWeeks() {
	      // Дни месяца
	      var date = new Date(this._startMonth),
	          max = 10;
	      while (date.getMonth() == this._startMonth.getMonth() && max > 0) {
	        date = this.__createWeek(date);
	        max++;
	      }
	
	      return this;
	      // return this.__updatePrices();
	    }
	
	    /**
	     *
	     * @param {Date} date
	     * @return {Date}
	     * @private
	     */
	
	  }, {
	    key: '__createWeek',
	    value: function __createWeek(date) {
	
	      // Дни одной недели
	      var tr = (0, _Element.docCreate)('tr', 'ottbp-cal-week');
	      for (var dayIndex = 0; dayIndex < 7; dayIndex++) {
	        var td = (0, _Element.docCreate)('td', 'ottbp-cal-day');
	
	        var currentDateIndex = date.getDay() - 1;
	        currentDateIndex = currentDateIndex >= 7 ? 0 : currentDateIndex;
	        currentDateIndex = currentDateIndex < 0 ? 6 : currentDateIndex;
	
	        if (currentDateIndex == dayIndex && date.getMonth() == this._startMonth.getMonth()) {
	          var dayDate = new Date(date);
	          var inRange = _Date2["default"].inDateRange(dayDate, this._startDate, this._endDate),
	              selected = inRange > 0,
	              selectedAfter = selected ? inRange < 3 : false,
	              selectedBefore = selected ? inRange > 1 : false;
	
	          var day = new _day2["default"](dayDate, this._days.length, {
	            disabled: _Date2["default"].isDisabledDay(dayDate, this._minDate, this._maxDate),
	            selected: selected,
	            selectedAfter: selectedAfter,
	            selectedBefore: selectedBefore
	          });
	
	          this._days.push(day);
	          td.appendChild(day.$node);
	          date.setDate(date.getDate() + 1);
	        }
	        tr.appendChild(td);
	      }
	      this._tbody.appendChild(tr);
	      return date;
	    }
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	
	      this._eventStore.delegateDocument('click', '.ottbp-btn-day', this.__onClickDay_v2.bind(this));
	      this._eventStore.delegateDocument('click', '.ottbp-cal-month', this.__onClickMonth_v2.bind(this));
	
	      this._eventStore.on('Document::mousemove', this.__onHover.bind(this));
	
	      this._eventStore.on('Controller::load:results', this.__update_v2.bind(this));
	      this._eventStore.on('Controller::show:results', this.__update_v2.bind(this));
	    }
	
	    /**
	     *
	     * @private
	     */
	
	  }, {
	    key: '__clear_v2',
	    value: function __clear_v2() {
	      this.__updatePrices_v2().__updatePriceRange_v2();
	    }
	
	    /**
	     *
	     * @param event
	     * @private
	     */
	
	  }, {
	    key: '__update_v2',
	    value: function __update_v2( /*{detail:{price:Price,results:[]}*/event) {
	      this.__updatePrices_v2(event.detail.price).__updatePriceRange_v2(event.detail.price);
	    }
	
	    // __onClick(event) {
	    //   event.stopPropagation();
	    //   event.preventDefault();
	    //   let target = event.target;
	    //   while (target && target !== this._container) {
	    //     if (target.className.indexOf('ottbp-btn-day') >= 0 && target.hasAttribute('data-id')) {
	    //       /**
	    //        * @type {CalendarDay}
	    //        */
	    //       this.__selectDay(this._days[target.getAttribute('data-id')]);
	    //       break;
	    //     }
	    //     if (target.className.indexOf('ottbp-cal-month') >= 0) {
	    //       this.onMonthClick();
	    //       break;
	    //     }
	    //     target = target.parentNode;
	    //   }
	    //
	    //   this.onSelect();
	    //   this.__render();
	    // }
	
	  }, {
	    key: '__onClickDay_v2',
	    value: function __onClickDay_v2(event) {
	      var target = event.detail.target;
	
	      if (target.hasAttribute('data-id')) {
	        this.__selectDay(this._days[target.getAttribute('data-id')]);
	      }
	
	      this.onSelect();
	      this.__render();
	    }
	  }, {
	    key: '__onClickMonth_v2',
	    value: function __onClickMonth_v2() {
	      this.onMonthClick();
	      this.onSelect();
	      this.__render();
	    }
	
	    /**
	     *
	     * @param {CalendarDay} day
	     * @private
	     */
	
	  }, {
	    key: '__selectDay',
	    value: function __selectDay(day) {
	      var date = new Date(day.$date);
	
	      if (this._duration) {
	        this._startDate = date;
	        this._endDate = _Date2["default"].addDays(date, this._duration);
	      } else {
	
	        if (!this._startDate || !this._isRange || this._endDate || !this._endDate && _Date2["default"].smallerThenDay(date, this._startDate)) {
	          this._startDate = date;
	          this._endDate = null;
	        } else {
	          this._endDate = date;
	        }
	      }
	    }
	  }, {
	    key: '__onBlur',
	    value: function __onBlur() {
	      // this._hoverDate = null;
	      // this.onSelect();
	      // this.__render();
	    }
	  }, {
	    key: '__onHover',
	    value: function __onHover(event) {
	
	      // event.stopPropagation();
	      // event.preventDefault();
	
	      this._hoverDate = null;
	      // if (this._isRange && this._startDate && !this._endDate) {
	
	      var target = event.detail.original.target;
	
	      if ((0, _classList2["default"])(target).contains('ottbp-cal-day')) {
	        target = target.children[0];
	      }
	
	      while (target && target.parentNode && target !== this._container) {
	        if ((0, _classList2["default"])(target).contains('ottbp-btn-day') && target.hasAttribute('data-id')) {
	          /**
	           *
	           * @type {Date}
	           * @private
	           */
	          this._hoverDate = this._days[target.getAttribute('data-id')].$date;
	        }
	        target = target.parentNode;
	      }
	
	      // if (!this._hoverDate) {
	      //   this.onSelect();
	      // }
	
	      // }
	      this.__render();
	    }
	  }, {
	    key: '__onHover_v2',
	    value: function __onHover_v2(event) {
	
	      // event.stopPropagation();
	      // event.preventDefault();
	
	      this._hoverDate = null;
	      // if (this._isRange && this._startDate && !this._endDate) {
	
	      var target = event.target;
	      if (target.className.indexOf('ottbp-cal-day') >= 0) {
	        target = target.children[0];
	      }
	      while (target && target.parentNode && target !== this._container) {
	        if (target && target.className && target.className.indexOf('ottbp-btn-day') >= 0 && target.hasAttribute('data-id')) {
	          // /**
	          //  * @type {CalendarDay}
	          //  */
	          this._hoverDate = this._days[target.getAttribute('data-id')].$date;
	        }
	        target = target.parentNode;
	      }
	
	      // if (!this._hoverDate) {
	      //   this.onSelect();
	      // }
	
	      // }
	      this.__render();
	    }
	  }, {
	    key: '__render',
	    value: function __render() {
	      var ident = (this._startDate ? this._startDate.toString() : 'null') + '_' + (this._endDate ? this._endDate.toString() : 'null') + '_' + (this._hoverDate ? this._hoverDate.toString() : 'null');
	
	      if (this._cacheDay === ident) {
	        return;
	      }
	      this._cacheDay = ident;
	
	      this.__generateHoverRange().onHover(this._hoverDate, this._hoverRange.start, this._hoverRange.end);
	
	      /**
	       * @this {Calendar}
	       */
	      this._days.forEach(this.__highlightDay.bind(this));
	    }
	  }, {
	    key: '__generateHoverRange',
	    value: function __generateHoverRange() {
	
	      var start = null,
	          end = null;
	
	      if (this._duration && this._hoverDate) {
	        start = this._hoverDate;
	        end = _Date2["default"].addDays(this._hoverDate, this._duration);
	      } else if (this._isRange) {
	
	        if (!this._startDate) {
	          start = this._hoverDate;
	        } else if (this._startDate && !this._endDate) {
	
	          if (!_Date2["default"].biggerThenDay(this._hoverDate, this._startDate)) {
	            start = this._hoverDate;
	          } else {
	            start = this._startDate;
	            end = this._hoverDate;
	          }
	        } else {
	          start = this._hoverDate || null;
	          end = null;
	        }
	      }
	
	      this._hoverRange = {
	        start: start,
	        end: end
	      };
	
	      return this;
	    }
	
	    /**
	     *
	     * @param {CalendarDay} day
	     * @private
	     */
	
	  }, {
	    key: '__highlightDay',
	    value: function __highlightDay(day) {
	
	      var day_date = day.$date;
	
	      var start = this._hoverRange.start,
	          end = this._hoverRange.end;
	
	      var inRange = {
	        hover: _Date2["default"].inDateRange(day_date, start, end),
	        select: _Date2["default"].inDateRange(day_date, this._startDate, this._endDate)
	      };
	
	      if (inRange.select != 0 && inRange.select != -1) {
	        day.select(inRange.select == 3 || inRange.select == 2, inRange.select == 1 || inRange.select == 2);
	      } else if (inRange.select == -1 && _Date2["default"].isOneDay(day_date, this._startDate)) {
	        day.select();
	      } else day.unSelect();
	
	      if (inRange.hover != 0 && inRange.hover != -1) {
	        day.hover(inRange.hover == 3 || inRange.hover == 2, inRange.hover == 1 || inRange.hover == 2);
	      } else {
	        day.unHover();
	      }
	    }
	
	    /**
	     *
	     * @param {function(CalendarValue)} callback
	     * @return {Calendar}
	     */
	
	  }, {
	    key: 'whenSelect',
	    value: function whenSelect(callback) {
	      this._whenSelect = callback;
	      return this;
	    }
	  }, {
	    key: 'onSelect',
	    value: function onSelect() {
	
	      if (this._whenSelect) this._whenSelect(this.$value);
	    }
	
	    /**
	     *
	     * @param {function} callback
	     * @return {Calendar}
	     */
	
	  }, {
	    key: 'whenMonthClick',
	    value: function whenMonthClick(callback) {
	      this._whenMonthClick = callback;
	      return this;
	    }
	  }, {
	    key: 'onMonthClick',
	    value: function onMonthClick() {
	      if (this._whenMonthClick) this._whenMonthClick();
	    }
	
	    /**
	     *
	     * @param {function(CalendarValue)} callback
	     * @return {Calendar}
	     */
	
	  }, {
	    key: 'whenHover',
	    value: function whenHover(callback) {
	      this._whenHover = callback;
	      return this;
	    }
	  }, {
	    key: 'onHover',
	    value: function onHover() {
	      if (this._whenHover) {
	        this._whenHover(this.$value);
	      }
	    }
	
	    /**
	     *
	     * @return {CalendarValue}
	     */
	
	  }, {
	    key: 'clear',
	    value: function clear() {
	
	      this._startDate = null;
	      this._endDate = null;
	      this._hoverDate = null;
	      this._hoverRange = {};
	      return this.__render();
	    }
	
	    /**
	     *
	     * @param {StorePrices} [prices=new StorePrices()]
	     * @return {Calendar}
	     * @private
	     */
	
	  }, {
	    key: '__updatePrices_v2',
	    value: function __updatePrices_v2() {
	      var prices = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new _Prices2["default"]();
	
	
	      if (prices && prices.$type) {
	
	        var gradient = _PriceGradient2["default"].getPriceColor(prices.$minRange, this._theme);
	
	        this._days.forEach(function ( /*CalendarDay*/day, /*number*/index) {
	          var price = prices.getBy(index + 1);
	          if (price) {
	            day.color = gradient.find(price.$delta);
	          } else {
	            day.color = null;
	            day.price = 'none';
	          }
	        });
	      }
	
	      return this;
	    }
	
	    /**
	     *
	     * @param {StorePrices} [prices=new StorePrices()]
	     * @return {Calendar}
	     * @private
	     */
	
	  }, {
	    key: '__updatePriceRange_v2',
	    value: function __updatePriceRange_v2() {
	      var prices = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new _Prices2["default"]();
	
	      if (prices) this._priceRange.update(prices.$minRange);else this._priceRange.update();
	      return this;
	    }
	
	    /**
	     *
	     * @param [month]
	     * @param year
	     * @param {CalendarOptions} [options={}]
	     */
	
	  }, {
	    key: 'update',
	    value: function update() {
	      var month = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._month;
	      var year = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._year;
	      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	
	      this._month = month;
	      this._year = year;
	
	      this._startMonth = new Date(year, month, 1);
	      /**
	       *
	       * @type {Array.<Day>}
	       * @private
	       */
	      this._days = [];
	
	      this._cacheDay = '';
	
	      this._tbody.innerHTML = '';
	
	      this.__parseOptions(options).__createMonthName().__createWeeks();
	
	      this.__render();
	    }
	  }, {
	    key: 'show',
	    value: function show() {
	      this._over.setAttribute('style', '');
	      // this._over.style.display = '';
	    }
	  }, {
	    key: 'hide',
	    value: function hide() {
	      this._over.setAttribute('style', 'display:none !important;');
	      // this._over.style.display = 'none';
	    }
	
	    /**
	     *
	     * @param {CalendarOptions} options
	     * @return {Calendar}
	     * @private
	     */
	
	  }, {
	    key: '__parseOptions',
	    value: function __parseOptions() {
	      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	      /**
	       *
	       * @type {Date|null}
	       * @private
	       */
	      this._startDate = options.dateStart || this._startDate || null;
	      /**
	       *
	       * @type {boolean}
	       * @private
	       */
	      this._isRange = !!options.isRange;
	      /**
	       *
	       * @type {number|null}
	       * @private
	       */
	      this.$duration = options.duration;
	
	      if (this._isRange && this._duration) {
	        this._endDate = _Date2["default"].addDays(this._startDate, this._duration);
	      } else if (this._isRange) {
	        /**
	         *
	         * @type {Date|null}
	         * @private
	         */
	        this._endDate = options.dateEnd || this._endDate || null;
	      } else {
	        this._endDate = null;
	      }
	      /**
	       *
	       * @type {Date}
	       * @private
	       */
	      this._minDate = options.minDate || new Date();
	      /**
	       *
	       * @type {Date|null}
	       * @private
	       */
	      this._maxDate = options.maxDate || null;
	      /**
	       *
	       * @type {Object|null}
	       * @private
	       */
	      this._query = options.query || null;
	      return this;
	    }
	  }, {
	    key: '$duration',
	    set: function set(duration) {
	      duration = parseInt(duration, 10);
	      if (typeof duration === 'number' && !isNaN(duration) && !isFinite(duration)) {
	        this._duration = duration;
	      } else {
	        this._duration = null;
	      }
	    }
	  }, {
	    key: '$value',
	    get: function get() {
	      return {
	        select: this._startDate,
	        range: {
	          start: this._isRange ? this._startDate : null,
	          end: this._isRange ? this._endDate : null
	        },
	        hover: this._hoverDate,
	        rangeHover: {
	          start: this._isRange && this._hoverRange ? this._hoverRange.start : this._hoverDate,
	          end: this._isRange && this._hoverRange ? this._hoverRange.end : null
	        }
	      };
	    }
	  }]);
	
	  return Calendar;
	}();
	
	exports["default"] = Calendar;

/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _Format = __webpack_require__(17);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var PriceRange = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {number[]} [options=[]]
	   */
	  function PriceRange(container) {
	    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
	
	    _classCallCheck(this, PriceRange);
	
	    this._container = container;
	    this._options = options;
	
	    this.initHTML();
	  }
	
	  _createClass(PriceRange, [{
	    key: 'initHTML',
	    value: function initHTML() {
	      this._node = (0, _Element.docCreate)('div');
	      this.initItems();
	      this._container.appendChild(this._node);
	    }
	  }, {
	    key: 'initItems',
	    value: function initItems() {
	
	      this._node.innerHTML = '';
	      var fragment = document.createDocumentFragment();
	
	      var _isFinite = this._options ? this._options.reduce(function (last, num) {
	        return last && isFinite(num);
	      }, true) : false;
	
	      if (this._options && this._options.length == 3 && _isFinite) {
	
	        var min = this._options[0];
	
	        this._options.forEach(function (pr, index) {
	          if (min == pr && index > 0) return;
	
	          var item = (0, _Element.docCreate)('div', 'ottbp-prices__item');
	
	          var low = index + 1 < this._options.length / 2,
	              middle = index + 1 < this._options.length;
	
	          var innerHTML = '<div class="ottbp-price">';
	          innerHTML += '<div class="ottbp-price__color"><div class="ottbp-price__color__circle ottbp--' + (low ? 'low' : middle ? 'middle' : 'high') + '_price"></div></div>';
	          innerHTML += '<div class="ottbp-price__cost ottbp-texts--price"><span class="ottbp-price__cost__inner">' + (0, _Format.price)(pr, 'RUB') + '</span></div>';
	          innerHTML += '</div>';
	
	          item.innerHTML = innerHTML;
	          fragment.appendChild(item);
	        }, this);
	      } /* else {
	        }*/
	
	      this._node.appendChild(fragment);
	    }
	
	    /**
	     *
	     * @param {number[]} [options=[]]
	     * @return {PriceRange}
	     */
	
	  }, {
	    key: 'update',
	    value: function update() {
	      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
	      this._options = options;
	      this.initItems();
	      return this;
	    }
	  }]);
	
	  return PriceRange;
	}();
	
	exports["default"] = PriceRange;

/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Color = __webpack_require__(52);
	
	var _Color2 = _interopRequireDefault(_Color);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var PriceGradient = function () {
	  /**
	   *
	   * @param priceRange
	   * @param {Theme} theme
	   */
	  function PriceGradient(priceRange, theme) {
	    _classCallCheck(this, PriceGradient);
	
	    this._range = priceRange;
	
	    this._gradientOptions = [{
	      range: 0,
	      color: theme.price().low
	    }, {
	      range: (priceRange[1] - priceRange[0]) / (priceRange[2] - priceRange[0]),
	      color: theme.price().middle
	    }, {
	      range: 1,
	      color: theme.price().high
	    }];
	  }
	
	  _createClass(PriceGradient, [{
	    key: 'find',
	    value: function find(delta) {
	      if (!this._gradient) this._gradient = _Color2["default"].gradient(this._gradientOptions);
	      return this._gradient.find(delta);
	    }
	  }]);
	
	  return PriceGradient;
	}();
	
	exports["default"] = {
	  /**
	   *
	   * @param priceRange
	   * @param {Theme} theme
	   * @return {PriceGradient}
	   */
	  getPriceColor: function getPriceColor(priceRange, theme) {
	    return new PriceGradient(priceRange, theme);
	  }
	};

/***/ },
/* 78 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var classes = {
	  disabled: 'ottbp-btn-day--disabled',
	  selected: 'ottbp-btn-day--selected',
	  hover: 'ottbp-btn-day--hover',
	
	  selectedAfter: 'ottbp-btn-day--selected--after',
	  selectedBefore: 'ottbp-btn-day--selected--before',
	
	  hoverAfter: 'ottbp-btn-day--hover--after',
	  hoverBefore: 'ottbp-btn-day--hover--before'
	};
	
	var CalendarDay = function () {
	  /**
	   *
	   * @param {Date} date
	   * @param {string|number} id
	   * @param {CalendarDayOption} [options={}]
	   */
	  function CalendarDay(date, id) {
	    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	
	    _classCallCheck(this, CalendarDay);
	
	    /**
	     *
	     * @type {Date}
	     * @private
	     */
	    this._date = date;
	    /**
	     *
	     * @type {CalendarDayOption}
	     * @private
	     */
	    this._options = options;
	    /**
	     *
	     * @type {*}
	     * @private
	     */
	    this._id = id;
	
	    this.__initBtn();
	  }
	
	  /**
	   *
	   * @return {CalendarDay}
	   * @private
	   */
	
	
	  _createClass(CalendarDay, [{
	    key: '__initBtn',
	    value: function __initBtn() {
	      this._btn = document.createElement('div');
	      this._btn.setAttribute('data-id', this._id);
	
	      return this.__initClasses().__initInner();
	    }
	
	    /**
	     *
	     * @return {CalendarDay}
	     * @private
	     */
	
	  }, {
	    key: '__initClasses',
	    value: function __initClasses() {
	      var _this = this;
	
	      var addClasses = ['ottbp-btn-day'],
	          removeClasses = [];
	
	      Object.keys(classes).forEach(function (prop) {
	        if (_this._options[prop]) addClasses.push(classes[prop]);else removeClasses.push(classes[prop]);
	      }, this);
	
	      (0, _Element.classAddRemove)(this._btn, addClasses, removeClasses);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {CalendarDay}
	     * @private
	     */
	
	  }, {
	    key: '__initInner',
	    value: function __initInner() {
	
	      this._btn.innerHTML = '<div class="ottbp-btn-day__back ottbp-btn-day__t"></div><div class="ottbp-btn-day__hover ottbp-btn-day__t"></div>';
	
	      this._cachePriceClassName = 'ottbp-btn-day__price  ottbp-btn-day__t ' + (this._options.price ? 'ottbp--' + this._options.price + '_price' : 'ottbp--none_price');
	
	      this._priceNode = (0, _Element.docCreate)('div', this._cachePriceClassName);
	      var dayNode = (0, _Element.docCreate)('div', ['ottbp-btn-day__inner', 'ottbp-btn-day__t']);
	      dayNode.innerHTML = this._date.getDate();
	
	      this._btn.appendChild(this._priceNode);
	      this._btn.appendChild(dayNode);
	      return this;
	    }
	
	    /**
	     *
	     * @return {*}
	     * @private
	     */
	
	  }, {
	    key: '__initPriceClass',
	    value: function __initPriceClass() {
	
	      var className = 'ottbp-btn-day__price ottbp-btn-day__t ' + (this._options.price ? 'ottbp--' + this._options.price + '_price' : 'ottbp--none_price');
	      if (this._cachePriceClassName == className) return this.__initPriceColor();
	
	      this._cachePriceClassName = className;
	      this._priceNode.className = className;
	      return this.__initPriceColor();
	    }
	
	    /**
	     *
	     * @return {CalendarDay}
	     * @private
	     */
	
	  }, {
	    key: '__initPriceColor',
	    value: function __initPriceColor() {
	
	      var backStyle = '';
	
	      if (this._options && this._options.color) {
	        backStyle = 'background-color: rgb(' + (this._options.color.r | 0) + ',' + (this._options.color.g | 0) + ',' + (this._options.color.b | 0) + ') !important;';
	      }
	      if (this._cacheBackStyle == backStyle) return this;
	      this._cacheBackStyle = backStyle;
	      this._priceNode.setAttribute('style', backStyle);
	      return this;
	    }
	
	    /**
	     *
	     * @return {Date}
	     */
	
	  }, {
	    key: 'select',
	
	
	    /**
	     *
	     * @param {boolean||undefined} [before]
	     * @param {boolean||undefined} [after]
	     * @return {CalendarDay}
	     */
	    value: function select(before, after) {
	      if (!this._options) this._options = {};
	      this._options.selected = true;
	      this._options.selectedAfter = !!after;
	      this._options.selectedBefore = !!before;
	      return this.__initClasses();
	    }
	
	    /**
	     *
	     * @return {CalendarDay}
	     */
	
	  }, {
	    key: 'unSelect',
	    value: function unSelect() {
	      if (!this._options) this._options = {};
	      this._options.selected = false;
	      this._options.selectedAfter = false;
	      this._options.selectedBefore = false;
	      return this.__initClasses();
	    }
	
	    // get $id() {
	    //   return this._id;
	    // }
	
	    // /**
	    //  *
	    //  * @return {boolean}
	    //  */
	    // isDisabled() {
	    //   return this._options ? this._options.disabled : false;
	    // }
	
	    // /**
	    //  *
	    //  * @return {boolean}
	    //  */
	    // isSelected() {
	    //   return this._options ? this._options.selected : false;
	    // }
	
	    /**
	     *
	     * @param {boolean||undefined} [before]
	     * @param {boolean||undefined} [after]
	     * @return {CalendarDay}
	     */
	
	  }, {
	    key: 'hover',
	    value: function hover(before, after) {
	      if (!this._options) this._options = {};
	      this._options.hover = true;
	      this._options.hoverBefore = !!before;
	      this._options.hoverAfter = !!after;
	      return this.__initClasses();
	    }
	  }, {
	    key: 'unHover',
	    value: function unHover() {
	      if (!this._options) this._options = {};
	      this._options.hover = false;
	      this._options.hoverAfter = false;
	      this._options.hoverBefore = false;
	      return this.__initClasses();
	    }
	  }, {
	    key: '$date',
	    get: function get() {
	      return this._date;
	    }
	
	    /**
	     *
	     * @return {Element}
	     */
	
	  }, {
	    key: '$node',
	    get: function get() {
	      return this._btn;
	    }
	  }, {
	    key: 'price',
	    set: function set(price) {
	      if (!this._options) this._options = {};
	      this._options.price = price;
	      this.__initPriceClass();
	    }
	  }, {
	    key: 'delta',
	    set: function set(delta) {
	      if (!this._options) this._options = {};
	      this._options.delta = delta;
	      this._options.price = 'custom';
	      this.__initPriceClass();
	    }
	
	    /**
	     *
	     * @param {{r:numberg:numberb:number}} color
	     */
	
	  }, {
	    key: 'color',
	    set: function set(color) {
	      if (!this._options) this._options = {};
	      this._options.color = color;
	      this._options.price = 'custom';
	      this.__initPriceClass();
	    }
	  }]);
	
	  return CalendarDay;
	}();
	
	exports["default"] = CalendarDay;

/***/ },
/* 79 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _l8n = __webpack_require__(74);
	
	var _l8n2 = _interopRequireDefault(_l8n);
	
	var _Date = __webpack_require__(40);
	
	var _PriceRange = __webpack_require__(76);
	
	var _PriceRange2 = _interopRequireDefault(_PriceRange);
	
	var _PriceGradient = __webpack_require__(77);
	
	var _PriceGradient2 = _interopRequireDefault(_PriceGradient);
	
	var _Prices = __webpack_require__(42);
	
	var _Prices2 = _interopRequireDefault(_Prices);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var MonthsList = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {EventStore} eventStore
	   * @param {Theme} theme
	   */
	  function MonthsList(container, eventStore, theme) {
	    _classCallCheck(this, MonthsList);
	
	    this._eventStore = eventStore;
	    this._theme = theme;
	    /**
	     *
	     * @type {MonthsOptions[]}
	     * @private
	     */
	    this._options = (0, _Date.generateMonthList)();
	    /**
	     *
	     * @type {HTMLElement}
	     * @private
	     */
	    this._container = container;
	    /**
	     *
	     * @type {number}
	     * @private
	     */
	    this._listYear = this._currentYear = new Date().getFullYear();
	
	    this.__initHTML().__initEvents();
	
	    /**
	     *
	     * @type {boolean}
	     * @private
	     */
	    this._init = false;
	  }
	
	  /**
	   *
	   * @return {boolean}
	   */
	
	
	  _createClass(MonthsList, [{
	    key: '__updateList_v2',
	
	
	    /**
	     *
	     * @param {{detail:(ResultsShow)}} event
	     * @private
	     */
	    value: function __updateList_v2( /*{detail:(ResultsShow)}*/event) {
	      this._loading = !!event.detail.loading;
	
	      if (!this._init && !this._loading) this._init = true;
	      this.__updateMonths(event.detail.price).__updatePriceRange(event.detail.price);
	    }
	
	    /**
	     *
	     * @param {StorePrices} prices
	     * @return {MonthsList}
	     * @private
	     */
	
	  }, {
	    key: '__updateMonths',
	    value: function __updateMonths() {
	      var prices = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new _Prices2["default"]();
	
	      var gradient = _PriceGradient2["default"].getPriceColor(prices.$minRange, this._theme);
	
	      var options = this._options.map(function ( /*MonthsOptions*/item) {
	        /*eslint no-unexpected-multiline: "error"*/
	        /**
	         *
	         * @type {MonthsOptions}
	         */
	        var newItem = _extends({}, item);
	        /**
	         *
	         * @type {StorePrices}
	         */
	        var price = prices.getBy(newItem.year + '_' + newItem.month);
	        if (price) {
	          newItem.color = gradient.find(price.$delta);
	          newItem.price = 'custom';
	        } else {
	          newItem.color = null;
	          newItem.price = 'none';
	        }
	        return newItem;
	      }, this);
	
	      if (JSON.stringify(options) !== JSON.stringify(this._options)) {
	        this._options = options;
	        this.__initItems();
	      }
	
	      return this;
	    }
	
	    /**
	     *
	     * @param {StorePrices} prices
	     * @return {MonthsList}
	     * @private
	     */
	
	  }, {
	    key: '__updatePriceRange',
	    value: function __updatePriceRange() {
	      var prices = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new _Prices2["default"]();
	
	      this._priceRange.update(prices.$minRange);
	      return this;
	    }
	
	    /**
	     *
	     * @return {MonthsList}
	     * @private
	     */
	
	  }, {
	    key: '__initHTML',
	    value: function __initHTML() {
	
	      this._over = (0, _Element.docCreate)('div');
	
	      this._node = (0, _Element.docCreate)('div', 'ottbp-months');
	      // this._nodeLoader = docCreate('div', 'ottbp-months-loader');
	      this._nodePriceRange = (0, _Element.docCreate)('div', 'ottbp-months-price_range');
	
	      // this._loader = new Loader(this._nodeLoader);
	
	      this.__initItems().__initPriceRange();
	
	      // this._over.appendChild(this._nodeLoader);
	      this._over.appendChild(this._node);
	      this._over.appendChild(this._nodePriceRange);
	      this._container.appendChild(this._over);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {MonthsList}
	     * @private
	     */
	
	  }, {
	    key: '__initPriceRange',
	    value: function __initPriceRange() {
	      this._priceRange = new _PriceRange2["default"](this._nodePriceRange);
	      return this;
	    }
	
	    /**
	     *
	     * @return {MonthsList}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      // this._eventStore.on(['Query::update:monthly', 'Query::update:main'], ::this.__updateList, 0);
	      // this._eventStore.on(['Controller::load:results'], ::this.__clearList_v2, 0);
	      this._eventStore.on(['Controller::load:results'], this.__updateList_v2.bind(this), 0);
	      this._eventStore.on(['Controller::show:results'], this.__updateList_v2.bind(this), 0);
	      // this._eventStore.addEvent(this._container, 'click', ::this.__onClick);
	
	      this._eventStore.delegateDocument('click', '.ottbp-month', this.__onClick_v2.bind(this));
	      return this;
	    }
	
	    /**
	     *
	     * @return {MonthsList}
	     * @return {MonthsList}
	     * @private
	     */
	
	  }, {
	    key: '__initItems',
	    value: function __initItems() {
	
	      this._node.innerHTML = '';
	      this._listYear = this._currentYear = new Date().getFullYear();
	      /**
	       *
	       * @type {DocumentFragment}
	       */
	      var fragment = (0, _Element.appendChildren)(document.createDocumentFragment(), this._options.map(this.__initItem.bind(this)));
	      this._node.appendChild(fragment);
	      return this;
	    }
	
	    /**
	     *
	     * @param {MonthsOptions} item
	     * @param {number} index
	     * @return {HTMLElement}
	     * @private
	     */
	
	  }, {
	    key: '__initItem',
	    value: function __initItem(item, index) {
	      var itemNode = (0, _Element.docCreate)('div', 'ottbp-months__item');
	      /**
	       *
	       * @type {string}
	       */
	      var title = _l8n2["default"].month.full[item.month];
	
	      if (this._currentYear !== item.year && this._listYear !== item.year) {
	        title += ' ' + item.year;
	        this._listYear = item.year;
	      }
	
	      var cl = item.price ? 'ottbp--' + item.price + '_price' : 'ottbp--none_price';
	      var color = item.color ? 'rgb(' + (item.color.r | 0) + ',' + (item.color.g | 0) + ',' + (item.color.b | 0) + ')' : '';
	
	      itemNode.innerHTML = '<button style="background-color: ' + color + ' !important;" data-index="' + index + '" class="ottbp-js-hover ottbp-month ottbp-texts--month ottbp-btn ' + cl + '"><span class="ottbp-month__inner">' + title + '</span></button>';
	
	      return itemNode;
	    }
	
	    // /**
	    //  *
	    //  * @param {MouseEvent} event
	    //  * @private
	    //  */
	    // __onClick(event) {
	    //   event.stopPropagation();
	    //   event.preventDefault();
	    //
	    //   let target = event.target;
	    //   while (target && target != this._node) {
	    //     if (target.className.indexOf('ottbp-month') >= 0 && target.hasAttribute('data-index')) {
	    //       this.onSelect(this._options[parseInt(target.getAttribute('data-index'), 10)]);
	    //     }
	    //     target = target.parentNode;
	    //   }
	    // }
	
	    /**
	     *
	     * @param {{detail:{target:HTMLElement original:MouseEvent}}} event
	     * @private
	     */
	
	  }, {
	    key: '__onClick_v2',
	    value: function __onClick_v2(event) {
	
	      var target = event.detail.target;
	      var originalEvent = event.detail.original;
	
	      if (target.hasAttribute('data-index')) {
	        originalEvent.stopPropagation();
	        originalEvent.preventDefault();
	
	        this.onSelect(this._options[parseInt(target.getAttribute('data-index'), 10)]);
	      }
	    }
	
	    /**
	     *
	     * @param {function} callback
	     * @return {MonthsList}
	     */
	
	  }, {
	    key: 'whenSelect',
	    value: function whenSelect(callback) {
	      this._onSelect = callback;
	      return this;
	    }
	
	    /**
	     *
	     * @param {MonthsOptions} item
	     */
	
	  }, {
	    key: 'onSelect',
	    value: function onSelect(item) {
	      if (this._onSelect) this._onSelect(item);
	    }
	  }, {
	    key: 'show',
	    value: function show() {
	      this._over.setAttribute('style', '');
	      // this._over.style.display = '';
	    }
	  }, {
	    key: 'hide',
	    value: function hide() {
	      this._over.setAttribute('style', 'display:none !important;');
	      // this._over.style.display = 'none';
	    }
	
	    // /**
	    //  *
	    //  * @param {MonthsOptions[]} options
	    //  * @return {MonthsList}
	    //  */
	    // update(options) {
	    //   // @TODO something
	    //   return this;
	    // }
	
	  }, {
	    key: '$isInit',
	    get: function get() {
	      return this._init;
	    }
	
	    /**
	     *
	     * @return {boolean|*}
	     */
	
	  }, {
	    key: '$isLoading',
	    get: function get() {
	      return this._loading;
	    }
	  }]);
	
	  return MonthsList;
	}();
	
	exports["default"] = MonthsList;

/***/ },
/* 80 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _results = __webpack_require__(81);
	
	var _results2 = _interopRequireDefault(_results);
	
	var _Element = __webpack_require__(7);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var ResultsContainer = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {EventStore} eventStore
	   *
	   */
	  function ResultsContainer(container, eventStore) {
	    _classCallCheck(this, ResultsContainer);
	
	    this._eventStore = eventStore;
	    /**
	     *
	     * @type {HTMLElement}
	     * @private
	     */
	    this._container = container;
	    this.__initHTML();
	    this._container.appendChild(this._node);
	  }
	
	  _createClass(ResultsContainer, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	
	      /**
	       *
	       * @type {HTMLElement}
	       * @private
	       */
	      this._node = (0, _Element.docCreate)('div', 'ottbp-container-results');
	
	      var _wrap = (0, _Element.docCreate)('div', 'ottbp-container-results__wrap');
	
	      this._node.appendChild(_wrap);
	      this._results = new _results2["default"](_wrap, undefined, undefined, this._eventStore);
	      return this;
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: '$isLoading',
	    get: function get() {
	      return this._results.$isLoading;
	    }
	
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '$height',
	    get: function get() {
	      var style = window.getComputedStyle(this._node),
	          padding_top = parseInt(style.getPropertyValue('padding-top'), 10);
	
	      return this._results.$height + 2 * padding_top;
	    }
	  }]);
	
	  return ResultsContainer;
	}();
	
	exports["default"] = ResultsContainer;

/***/ },
/* 81 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _loader = __webpack_require__(70);
	
	var _loader2 = _interopRequireDefault(_loader);
	
	var _trip = __webpack_require__(82);
	
	var _trip2 = _interopRequireDefault(_trip);
	
	var _classList = __webpack_require__(49);
	
	var _classList2 = _interopRequireDefault(_classList);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	// import Eve from './../mixins/Event';
	
	var Results = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {Array<TripOptions>|null} [options=null]
	   * @param {OttDealsQuery} [searchOptions]
	   * @param {EventStore} eventStore
	   */
	  function Results(container) {
	    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	    var searchOptions = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	    var eventStore = arguments[3];
	
	    _classCallCheck(this, Results);
	
	    /**
	     *
	     * @type {EventStore}
	     * @private
	     */
	    this._eventStore = eventStore;
	
	    /**
	     *
	     * @type {HTMLElement}
	     * @private
	     */
	    this._container = container;
	    /**
	     *
	     * @type {Array<TripOptions>|null}
	     * @private
	     */
	    this._options = options;
	    this._searchOptions = searchOptions;
	    this.__initHTML().__initEvents();
	
	    this._loading = true;
	    // this._eventStore.on('Data::query:send', ::this.load);
	    // this._eventStore.on('Data::query:response', ::this.__updateResults);
	
	    // DataDeals.beforeQuerySend(::this.load);
	    // DataDeals.afterQuerySend(::this.__updateResults);
	  }
	
	  /**
	   *
	   * @return {Results}
	   */
	
	
	  _createClass(Results, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      this._node = (0, _Element.docCreate)('div', 'ottbp-results');
	      this._list = (0, _Element.docCreate)('ul', 'ottbp-results__list');
	      this._containerLoader = (0, _Element.docCreate)('div', 'ottbp-results__loader');
	      this._containerAfterLoader = (0, _Element.docCreate)('div', 'ottbp-results__after-loader');
	      this._containerText = (0, _Element.docCreate)('div', 'ottbp-results__text');
	
	      this._loader = new _loader2["default"](this._containerLoader);
	      this._loaderAfter = new _trip.TripLoader(this._containerAfterLoader);
	      this._text = new _trip.TripText(this._containerText, this._eventStore);
	
	      this._node.appendChild(this._containerLoader);
	      this._node.appendChild(this._list);
	      this._node.appendChild(this._containerText);
	      this._node.appendChild(this._containerAfterLoader);
	      this._container.appendChild(this._node);
	
	      this.__initItemsHTML();
	
	      return this;
	    }
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      // this._eventStore.on(['Query::init', 'Query::update', 'Query::update:date'], ::this.__loading);
	      // this._eventStore.on('Query::update:find', ::this.__updateResults);
	      // this._eventStore.on('Results::load', ::this.__loading);
	      this._eventStore.on('Controller::load:results', this.__updateResults_v2.bind(this));
	      this._eventStore.on('Controller::show:results', this.__updateResults_v2.bind(this));
	    }
	
	    /**
	     *
	     * @return {Results}
	     * @private
	     */
	
	  }, {
	    key: '__initItemsHTML',
	    value: function __initItemsHTML() {
	      this._fragment = document.createDocumentFragment();
	
	      if (this._options && this._options.length > 0) {
	        var items = this._options.map(function (item) {
	          var li = (0, _Element.docCreate)('li', 'ottbp-results__list__item');
	          new _trip2["default"](li, item);
	          return li;
	        }.bind(this));
	        (0, _Element.appendChildren)(this._fragment, items);
	      } else if (this._options && this._options.length == 0) {
	        var empty = (0, _Element.docCreate)('li', 'ottbp-results__list__item');
	        empty.innerHTML = '<div class="ottbp-results__list__item--empty">По заданным параметрам поиска перелётов не найдено</div>';
	        this._list.appendChild(empty);
	      }
	
	      this._list.appendChild(this._fragment);
	
	      return this;
	    }
	
	    /**
	     *
	     * @param {Array<TripOptions>} [items=[]]
	     * @private
	     */
	
	  }, {
	    key: '__updateItems',
	    value: function __updateItems() {
	      var items = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
	
	      var cache = JSON.stringify(items);
	      if (this._cache && this._cache == cache) return this;
	
	      this._cache = cache;
	
	      /**
	       *
	       * @type {DocumentFragment}
	       * @private
	       */
	      var _fragment = document.createDocumentFragment();
	      this._list.innerHTML = '';
	      /**
	       *
	       * @type {Array.<HTMLElement>}
	       */
	      var liItems = items.map(function (item) {
	        /**
	         *
	         * @type {HTMLElement}
	         */
	        var li = (0, _Element.docCreate)('li', 'ottbp-results__list__item');
	        new _trip2["default"](li, item);
	        return li;
	      }.bind(this));
	
	      (0, _Element.appendChildren)(_fragment, liItems);
	      this._list.appendChild(_fragment);
	      // this._eventStore.dispatch('Results::resize');
	
	      return this;
	    }
	  }, {
	    key: '__loading',
	    value: function __loading() {
	      this._loading = true;
	      (0, _classList2["default"])(this._node).add('ottbp-loading');
	      this._containerLoader.style.display = '';
	      this._loader.show();
	      // this._eventStore.dispatch('Results::resize');
	    }
	  }, {
	    key: '__loaded',
	    value: function __loaded() {
	      this._loading = false;
	      (0, _classList2["default"])(this._node).remove('ottbp-loading');
	      this._loader.hide(function () {
	        this._containerLoader.setAttribute('style', 'display: none !important;');
	      }.bind(this));
	    }
	  }, {
	    key: '__updateResults_v2',
	    value: function __updateResults_v2( /*{detail:(ResultsShow)}*/event) {
	      this.update_v2(event.detail);
	      // if(event.detail.loading)
	      //   this.__loading();
	      // else
	      //   this.__loaded();
	      // this._eventStore.dispatch('Results::resize');
	    }
	
	    /**
	     *
	     * @param {Array<TripOptions>|null} [options=null]
	     * @return {Results}
	     */
	
	  }, {
	    key: 'update',
	    value: function update() {
	      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
	
	      var cache = JSON.stringify(options);
	      if (this._cache && this._cache == cache) return this;
	
	      this._cache = cache;
	      this._options = options;
	      this._list.innerHTML = '';
	      this.__initItemsHTML();
	      return this;
	    }
	
	    /**
	     *
	     * @param {ResultsShow} [options={}]
	     */
	
	  }, {
	    key: 'update_v2',
	    value: function update_v2() {
	      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	      var loaderAfter = false,
	          loader = false /*, results = false*/;
	
	      if (options.loading) {
	        loaderAfter = true;
	        loader = true;
	        if (options.results && options.results.length > 0) {
	          loader = false;
	          // results = true;
	        } else {
	          loaderAfter = false;
	        }
	      } /* else {
	         if (options.results && options.results.length > 0) {
	           results = true;
	         }
	        }*/
	
	      if (loader) {
	        this.__loading();
	      } else {
	        this.__loaded();
	      }
	      if (!loader && loaderAfter) {
	        this._afterLoading = true;
	        this._loaderAfter.show();
	      } else {
	        this._afterLoading = false;
	        this._loaderAfter.hide();
	      }
	
	      this.update(options.results);
	
	      this._text.test(options);
	
	      this._eventStore.dispatch('Results::resize');
	    }
	
	    /**
	     *
	     * @return {boolean}
	     */
	
	  }, {
	    key: '$isLoading',
	    get: function get() {
	      return this._loading;
	    }
	
	    /**
	     *
	     * @return {number}
	     */
	
	  }, {
	    key: '$height',
	    get: function get() {
	      if (this.$isLoading) {
	        return this._containerLoader.offsetHeight + 30;
	      }
	      return this._list.offsetHeight + this._containerText.offsetHeight + (this._afterLoading ? this._containerAfterLoader.offsetHeight : 0);
	    }
	  }]);
	
	  return Results;
	}();
	
	exports["default"] = Results;

/***/ },
/* 82 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.TripText = exports.TripLoader = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _direction = __webpack_require__(83);
	
	var _direction2 = _interopRequireDefault(_direction);
	
	var _Element = __webpack_require__(7);
	
	var _Format = __webpack_require__(17);
	
	var _OneTwoTrip = __webpack_require__(23);
	
	var _city = __webpack_require__(29);
	
	var _texts = __webpack_require__(85);
	
	var _arrow = __webpack_require__(86);
	
	var _arrow2 = _interopRequireDefault(_arrow);
	
	var _loader = __webpack_require__(70);
	
	var _loader2 = _interopRequireDefault(_loader);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var TripLoader = exports.TripLoader = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   */
	  function TripLoader(container) {
	    _classCallCheck(this, TripLoader);
	
	    /**
	     *
	     * @type {HTMLElement}
	     * @private
	     */
	    this._container = container;
	    this.__initHTML();
	    this._container.appendChild(this._node);
	  }
	
	  _createClass(TripLoader, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      /**
	       *
	       * @type {HTMLElement}
	       * @private
	       */
	      this._node = (0, _Element.docCreate)('div', 'ottbp-trip-loader');
	      this._loader = new _loader2["default"](this._node, { small: true });
	
	      return this;
	    }
	  }, {
	    key: 'show',
	    value: function show() {
	      this._loader.show();
	    }
	  }, {
	    key: 'hide',
	    value: function hide() {
	      this._loader.hide();
	    }
	  }]);
	
	  return TripLoader;
	}();
	
	var TripText = exports.TripText = function () {
	  /**
	   *
	   * @param container
	   * @param {EventStore} eventStore
	   */
	  function TripText(container, eventStore) {
	    _classCallCheck(this, TripText);
	
	    this._eventStore = eventStore;
	    this._container = container;
	    this.__initHTML().__initEvents();
	    this._container.appendChild(this._node);
	  }
	
	  /**
	   *
	   * @return {TripText}
	   * @private
	   */
	
	
	  _createClass(TripText, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      this._node = (0, _Element.docCreate)('div', 'ottbp-trip-text');
	      return this;
	    }
	
	    /**
	     *
	     * @return {TripText}
	     * @private
	     */
	
	  }, {
	    key: '__initEvents',
	    value: function __initEvents() {
	      this._node.addEventListener('click', function (event) {
	        var target = event.target;
	        while (target && target !== this._node) {
	          if (target.hasAttribute && target.hasAttribute('data-iata')) {
	            event.stopPropagation();
	            event.preventDefault();
	            // this._eventStore.dispatch('Results::add:city', {iata : target.getAttribute('data-iata')});
	            this._eventStore.dispatch('Form::update:to:city', { value: target.getAttribute('data-iata') });
	            return;
	          }
	          target = target.parentNode;
	        }
	      });
	
	      return this;
	    }
	  }, {
	    key: 'clean',
	    value: function clean() {
	      return this.insert('');
	    }
	
	    /**
	     *
	     * @param {ResultsShow} options
	     */
	
	  }, {
	    key: 'test',
	    value: function test(options) {
	      if (options.loading) this.clean();else {
	        var insert = '';
	
	        if (options.query.$isDestinationCountry) {
	          // страна!
	          insert = (0, _texts.countryResults)(options.store, options.query, options.results);
	        } else {
	          // город!
	          insert = (0, _texts.cityResults)(options.query, options.results);
	        }
	
	        this.insert(insert);
	      }
	    }
	  }, {
	    key: 'insert',
	    value: function insert(text) {
	      this._node.innerHTML = text;
	      if (!text || text.length <= 0) this._node.setAttribute('style', 'display: none !important;');else this._node.style.display = '';
	      return this;
	    }
	  }]);
	
	  return TripText;
	}();
	
	var Trip = function () {
	  /**
	   *
	   * @param {HTMLElement} container
	   * @param {Response} options
	   */
	  function Trip(container) {
	    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	    _classCallCheck(this, Trip);
	
	    this._container = container;
	    /**
	     *
	     * @type {Response|{}}
	     * @private
	     */
	    this._options = {};
	
	    this.__parseOptions_v2(options).initHTML();
	  }
	
	  /**
	   *
	   * @param {Response} response
	   * @return {Trip}
	   * @private
	   */
	
	
	  _createClass(Trip, [{
	    key: '__parseOptions_v2',
	    value: function __parseOptions_v2(response) {
	      /**
	       *
	       * @type {Response}
	       * @private
	       */
	      this._response = response;
	
	      this._options.price = (0, _Format.price)(response.$price, response.$currency);
	      var origin = (0, _city.byIATA)(response.$from);
	      this._options.origin = origin ? origin.city.ru : '';
	      var destination = (0, _city.byIATA)(response.$to);
	      this._options.destinationCity = destination ? destination.city.ru : '';
	
	      this._options.duration = response.$duration;
	      this._options.durationString = (0, _Format.declOfNum)(this._options.duration, ['день', 'дня', 'дней']);
	
	      var directions = response.$directions;
	
	      if (directions[0]) this._options.directionThere = directions[0];
	
	      if (directions[1]) this._options.directionBack = directions[1];
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Trip}
	     */
	
	  }, {
	    key: 'initHTML',
	    value: function initHTML() {
	
	      this._node = (0, _Element.docCreate)('div', 'ottbp-trip');
	      this.__initHeaderHTML().__initBodyHTML().__initFooterHTML();
	      this._container.appendChild(this._node);
	      return this;
	    }
	
	    /**
	     *
	     * @return {Trip}
	     * @private
	     */
	
	  }, {
	    key: '__initHeaderHTML',
	    value: function __initHeaderHTML() {
	      var header = (0, _Element.docCreate)('div', 'ottbp-trip__header');
	
	      var innerHTML = '<div class="ottbp-trip__title">';
	      innerHTML += '<span class="ottbp-trip__title__inner ottbp-black">' + this._options.destinationCity + '</span>';
	      if (this._options.duration > 0) {
	        innerHTML += '<span class="ottbp-trip__title__inner ottbp-gray">' + (', \u043D\u0430 ' + this._options.duration + ' ' + this._options.durationString) + '</span>';
	      }
	      innerHTML += '</div>';
	
	      header.innerHTML = innerHTML;
	
	      this._node.appendChild(header);
	      return this;
	    }
	
	    /**
	     *
	     * @return {Trip}
	     * @private
	     */
	
	  }, {
	    key: '__initBodyHTML',
	    value: function __initBodyHTML() {
	      var body = (0, _Element.docCreate)('div', 'ottbp-trip__body'),
	          list = (0, _Element.docCreate)('div', 'ottbp-trip__directions');
	
	      var firstColumn = (0, _Element.docCreate)('div', 'ottbp-trip__directions__item');
	
	      this._startDate = null;
	      this._endDate = null;
	      /**
	       *
	       * @type {Direction}
	       */
	      var tempDirection = new _direction2["default"](firstColumn, {
	        directionName: 'Туда',
	        type: 'from'
	      }, { direction: this._options.directionThere });
	
	      list.appendChild(firstColumn);
	      this._startDate = tempDirection.$startDate;
	
	      this._from = tempDirection.$from;
	      this._to = tempDirection.$to;
	
	      if (this._options.directionBack) {
	        var secondColumn = (0, _Element.docCreate)('div', 'ottbp-trip__directions__item');
	        /**
	         *
	         * @type {Direction}
	         */
	        var _tempDirection = new _direction2["default"](secondColumn, {
	          directionName: 'Обратно',
	          type: 'to'
	        }, { direction: this._options.directionBack });
	        list.appendChild(secondColumn);
	        this._endDate = _tempDirection.$startDate;
	      } else {
	        list.className = 'ottbp-trip__directions ottbp-trip__directions--only_one';
	      }
	
	      body.appendChild(list);
	
	      this._node.appendChild(body);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Trip}
	     * @private
	     */
	
	  }, {
	    key: '__initFooterHTML',
	    value: function __initFooterHTML() {
	      var url = (0, _OneTwoTrip.link)({}, this._response);
	      // let url = generateLink(this._from, this._to, this._startDate, this._endDate, 'E'/*Query.options().selects.type*/);
	
	      var arrow = new _arrow2["default"]().toString();
	
	      var footer = (0, _Element.docCreate)('div', 'ottbp-trip__footer'),
	          price = '<div class="ottbp-trip__price"><span class="ottbp-trip__price__inner">' + this._options.price + '</span></div>',
	          btn = '<div class="ottbp-trip__btn"><a href="' + url + '" type="button" target="_blank" class="ottbp-btn-link"><span class="ottbp-btn-link__inner">\u0412\u044B\u0431\u0440\u0430\u0442\u044C</span>' + arrow + '</a></div>';
	
	      footer.innerHTML = price + btn;
	      this._node.appendChild(footer);
	
	      return this;
	    }
	  }]);
	
	  return Trip;
	}();
	
	exports["default"] = Trip;

/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Element = __webpack_require__(7);
	
	var _Format = __webpack_require__(17);
	
	var _l8n = __webpack_require__(74);
	
	var _l8n2 = _interopRequireDefault(_l8n);
	
	var _airplane = __webpack_require__(84);
	
	var _airplane2 = _interopRequireDefault(_airplane);
	
	var _airline = __webpack_require__(37);
	
	var _Time = __webpack_require__(92);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function duration(startDirection, endDirection) {
	  var startDate = (0, _Format.dateFromString)(startDirection.endDate ? startDirection.endDate : endDirection.startDate),
	      startTime = (0, _Time.split)(startDirection.endTime);
	
	  var endDate = (0, _Format.dateFromString)(endDirection.startDate),
	      endTime = (0, _Time.split)(endDirection.startTime);
	
	  startDate.setHours(parseInt(startTime[0], 10), parseInt(startTime[1], 10));
	  // startDate.setMinutes(parseInt(startTime[1], 10));
	
	  endDate.setHours(parseInt(endTime[0], 10), parseInt(endTime[1], 10));
	  // endDate.setMinutes(parseInt(endTime[1], 10));
	
	  return endDate.getTime() - startDate.getTime();
	}
	/**
	 *
	 * @param {Array} directions
	 * @return {string}
	 */
	function parseDuration(directions) {
	
	  if (directions.length == 1) return (0, _Time.toFormat)(directions[0].flightTime);
	
	  var t = directions.reduce(function (time, direction, index) {
	    if (index == 0) {
	      return (0, _Time.toSeconds)(direction.flightTime);
	    }
	
	    return time + (0, _Time.toSeconds)(direction.flightTime) + duration(directions[index - 1], direction) / 1000;
	  }, 0);
	
	  var hour = Math.floor(t / 60 / 60),
	      min = Math.floor((t - hour * 60 * 60) / 60);
	
	  if (hour) return hour + ' ' + _l8n2["default"].Direction.time.hour + ' ' + min + ' ' + _l8n2["default"].Direction.time.min;
	  return min + ' ' + _l8n2["default"].Direction.time.min;
	}
	
	var Direction = function () {
	  _createClass(Direction, [{
	    key: '$startDate',
	
	    /**
	     *
	     * @return {Date}
	     */
	    get: function get() {
	      return this._options.startDate;
	    }
	
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: '$from',
	    get: function get() {
	      return this._options.from;
	    }
	
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: '$to',
	    get: function get() {
	      return this._options.to;
	    }
	  }]);
	
	  function Direction(container) {
	    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	    var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	
	    _classCallCheck(this, Direction);
	
	    this._container = container;
	    this._options = options;
	
	    this.__parseData(data).initHTML();
	  }
	
	  /**
	   *
	   * @param data
	   * @return {Direction}
	   * @private
	   */
	
	
	  _createClass(Direction, [{
	    key: '__parseData',
	    value: function __parseData(data) {
	
	      this._options.accross = _l8n2["default"].Direction.accross.none;
	      if (data.direction.length > 1) {
	        var num = data.direction.length - 1;
	        this._options.accross = num + '&nbsp;' + (0, _Format.declOfNum)(num, _l8n2["default"].Direction.accross.one_two_five);
	      }
	
	      this._options.aircompany_id = data.direction[0].airCompany;
	      this._options.aircompany_logo = 'https://www.gstatic.com/flights/airline_logos/32px/' + data.direction[0].airCompany + '.png';
	      this._options.aircompany = (0, _airline.byIATA)(data.direction[0].airCompany);
	      this._options.startDate = (0, _Format.dateFromString)(data.direction[0].startDate);
	
	      this._duration = parseDuration(data.direction);
	
	      this._options.from = data.direction[0].from;
	      this._options.to = data.direction[data.direction.length - 1].to;
	
	      this._options.startTime = (0, _Time.toColon)(data.direction[0].startTime);
	      this._options.endTime = (0, _Time.toColon)(data.direction[data.direction.length - 1].endTime);
	
	      return this;
	    }
	
	    /**
	     *
	     * @return {Direction}
	     */
	
	  }, {
	    key: 'initHTML',
	    value: function initHTML() {
	
	      this._node = (0, _Element.docCreate)('div', 'ottbp-direction');
	
	      var innerTitle = '<table class="ottbp-direction__title">';
	
	      var air = new _airplane2["default"]();
	
	      air.resize(18, 16).fill('#616161').mirror(this._options.type == 'to');
	
	      innerTitle += '<tr>';
	      innerTitle += '<td>';
	      innerTitle += air.toString();
	      innerTitle += '</td>';
	
	      // innerTitle += `<span class="ottbp-direction__title__icon ottbp-icon-fly ottbp-icon-fly--${this._options.type || "from"}"></span>`;
	      innerTitle += '<td class="ottbp-direction__title__inner">';
	      innerTitle += '<span class="ottbp-direction__title__inner">' + this._options.directionName + ',&nbsp;</span>';
	      innerTitle += '<span class="ottbp-direction__title__inner">' + (0, _Format.dateToShotString)(this._options.startDate, true) + '</span>';
	      innerTitle += '</td>';
	      innerTitle += '</tr>';
	      innerTitle += '</table>';
	
	      var innerTime = '<div class="ottbp-direction__time">';
	      innerTime += '<span class="ottbp-direction__time__inner">' + this._options.startTime + ' \u2014 ' + this._options.endTime + '</span></div>';
	
	      var innerTimeSpent = '<div class="ottbp-direction__timespent">';
	      innerTimeSpent += '<span class="ottbp-direction__timespent__inner">' + this._duration + ', ' + this._options.accross + '</span></div>';
	
	      var innerCompany = '<div class="ottbp-direction__company">';
	      innerCompany += '<img class="ottbp-direction__company__logo" src="' + this._options.aircompany_logo + '" alt="' + this._options.aircompany + '">';
	      innerCompany += '<span class="ottbp-direction__company__inner">' + this._options.aircompany + '</span>';
	      innerCompany += '</div>';
	
	      var innerHTML = '<div class="ottbp-direction__main">' + innerTitle + innerTime + '</div>';
	      innerHTML += '<div class="ottbp-direction__add">' + innerTimeSpent + innerCompany + '</div>';
	
	      this._node.innerHTML = innerHTML;
	      this._container.appendChild(this._node);
	
	      return this;
	    }
	  }]);
	
	  return Direction;
	}();
	
	exports["default"] = Direction;

/***/ },
/* 84 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Svg2 = __webpack_require__(60);
	
	var _Svg3 = _interopRequireDefault(_Svg2);
	
	__webpack_require__(61);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Airplane = function (_Svg) {
	  _inherits(Airplane, _Svg);
	
	  function Airplane() {
	    _classCallCheck(this, Airplane);
	
	    var _this = _possibleConstructorReturn(this, (Airplane.__proto__ || Object.getPrototypeOf(Airplane)).call(this));
	
	    _this._attr = {
	      width: 14,
	      height: 12
	    };
	
	    _this._options = {
	      fill: '#616161'
	    };
	
	    _this._mirror = false;
	
	    _this.update();
	    return _this;
	  }
	
	  /**
	   *
	   * @return {Airplane}
	   */
	
	
	  _createClass(Airplane, [{
	    key: 'update',
	    value: function update() {
	      this._inner = '<use xlink:href="#ottbp-s-air" fill="' + this._options.fill + '" transform="scale(' + (this._mirror ? '-1' : '1') + ',1) translate(' + (this._mirror ? -1 * this._attr.width : '0') + ',0)" class="ottbp-sprite-svg__air"></use>';
	      return this.render();
	    }
	
	    /**
	     *
	     * @param {boolean|null} [bool=false]
	     * @return {Airplane}
	     */
	
	  }, {
	    key: 'mirror',
	    value: function mirror() {
	      var bool = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	
	      this._mirror = !!bool;
	      return this.update();
	    }
	
	    /**
	     *
	     * @param {number} [width]
	     * @param {number} [height]
	     * @return {Airplane}
	     */
	
	  }, {
	    key: 'resize',
	    value: function resize() {
	      var width = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._attr.width;
	      var height = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._attr.height;
	
	      return this.attr({
	        width: width,
	        height: height
	      });
	    }
	
	    /**
	     *
	     * @param fill
	     * @return {Airplane}
	     */
	
	  }, {
	    key: 'fill',
	    value: function fill(_fill) {
	      this._options.fill = _fill;
	      return this.update();
	    }
	  }]);
	
	  return Airplane;
	}(_Svg3["default"]);
	
	exports["default"] = Airplane;

/***/ },
/* 85 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.countryResults = countryResults;
	exports.cityResults = cityResults;
	exports.emptyResults = emptyResults;
	exports.needDates = needDates;
	
	var _OneTwoTrip = __webpack_require__(23);
	
	var _city = __webpack_require__(29);
	
	/**
	 *
	 * @param {string} iata
	 * @return {string}
	 */
	function nodeLinkCityByIata(iata) {
	  var city = (0, _city.byIATA)(iata);
	  if (city) return '<a href="javascript:void(0)" data-iata="' + iata + '">' + city.city.ru + '</a>';
	  return '';
	}
	
	/**
	 *
	 * @param {Store} store
	 * @param {Query} query
	 * @param {Array.<Response>} results
	 * @return {string}
	 */
	function countryResults(store, query, results) {
	  var text = '';
	  if (query.$isComplete) {
	    var cities = store.getCities(query).splice(0, 3).map(nodeLinkCityByIata).join(', ');
	
	    if (results.length == 0) {
	      text = emptyResults();
	      if (cities.length > 0) {
	        text += '<div class="ottbp-info">\u041F\u043E\u043F\u0440\u043E\u0431\u0443\u0439\u0442\u0435 \u0443\u0442\u043E\u0447\u043D\u0438\u0442\u044C \u0433\u043E\u0440\u043E\u0434 \u043F\u0440\u0438\u0431\u044B\u0442\u0438\u044F \u0438\u043B\u0438 \u0434\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u043E\u0434\u0438\u043D \u0438\u0437 \u0441\u0430\u043C\u044B\u0445 \u043F\u043E\u043F\u0443\u043B\u044F\u0440\u043D\u044B\u0445: ' + cities + '.</div>';
	      } else {
	        text += '<div class="ottbp-info">Уточните город прибытия или выберите другие даты путешествия.</div>';
	      }
	    } else {
	      if (cities.length > 0) {
	        text = '<div class="ottbp-info">\u041F\u043E\u0438\u0441\u043A \u043F\u043E \u043F\u043E\u043F\u0443\u043B\u044F\u0440\u043D\u044B\u043C \u0433\u043E\u0440\u043E\u0434\u0430\u043C: ' + cities + '.</div>';
	      }
	    }
	    text += '<div class="ottbp-more"><a href="' + (0, _OneTwoTrip.linkCountry)(query) + '" target="_blank">\u0418\u0441\u043A\u0430\u0442\u044C \u043F\u043E\u0434\u0440\u043E\u0431\u043D\u0435\u0435</a></div>';
	  } else {
	    if (results.length == 0) {
	      text = emptyResults();
	      text += needDates(query.$isRoundTrip, query.$departure);
	    }
	  }
	
	  return text;
	}
	
	/**
	 *
	 * @param {Query} query
	 * @param {Array.<Response>} results
	 * @return {string}
	 */
	function cityResults(query, results) {
	  var text = '';
	  if (query.$isComplete) {
	    if (results.length == 0) {
	
	      text = emptyResults();
	      text += '<div class="ottbp-info">Выберите другие даты путешествия</div>';
	    } else {
	
	      text = '<div class="ottbp-more"><a href="' + (0, _OneTwoTrip.linkCity)(query) + '" target="_blank">\u0412\u0441\u0435 \u043F\u0435\u0440\u0435\u043B\u0451\u0442\u044B</a></div>';
	    }
	  } else {
	    if (results.length == 0) {
	      text = emptyResults();
	      text += needDates(query.$isRoundTrip, query.$departure);
	    }
	  }
	
	  return text;
	}
	
	/**
	 *
	 * @return {string}
	 */
	function emptyResults() {
	  return '<div class="ottbp-error">Быстро найти хороший вариант не удалось.</div>';
	}
	
	/**
	 *
	 * @param {boolean} isRoundTrip
	 * @param {Date|null} [departureDate = null]
	 * @return {string}
	 */
	function needDates(isRoundTrip) {
	  var departureDate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	
	  if (isRoundTrip) {
	    if (departureDate) return '<div class="ottbp-info">Выберите дату окончания путешествия.</div>';else return '<div class="ottbp-info">Выберите даты путешествия.</div>';
	  }
	  return '<div class="ottbp-info">Выберите дату отправления.</div>';
	}

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Svg2 = __webpack_require__(60);
	
	var _Svg3 = _interopRequireDefault(_Svg2);
	
	__webpack_require__(61);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Arrow = function (_Svg) {
	  _inherits(Arrow, _Svg);
	
	  function Arrow() {
	    _classCallCheck(this, Arrow);
	
	    var _this = _possibleConstructorReturn(this, (Arrow.__proto__ || Object.getPrototypeOf(Arrow)).call(this));
	
	    _this._attr = {
	      width: 7,
	      height: 10
	    };
	
	    _this._options = {
	      fill: '#FFFFFF'
	    };
	
	    _this.update();
	    return _this;
	  }
	
	  /**
	   *
	   * @return {Earth}
	   */
	
	
	  _createClass(Arrow, [{
	    key: 'update',
	    value: function update() {
	      this._inner = '<use xlink:href="#ottbp-s-arrow" fill="' + this._options.fill + '"  class="ottbp-sprite-svg__arrow"></use>';
	      return this.render();
	    }
	
	    /**
	     *
	     * @param {number} [width]
	     * @param {number} [height]
	     * @return {Earth}
	     */
	
	  }, {
	    key: 'resize',
	    value: function resize() {
	      var width = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._attr.width;
	      var height = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._attr.height;
	
	      return this.attr({
	        width: width,
	        height: height
	      });
	    }
	
	    /**
	     *
	     * @param fill
	     * @return {Earth}
	     */
	
	  }, {
	    key: 'fill',
	    value: function fill(_fill) {
	      this._options.fill = _fill;
	      return this.update();
	    }
	  }]);
	
	  return Arrow;
	}(_Svg3["default"]);
	
	exports["default"] = Arrow;

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Color = __webpack_require__(52);
	
	var _Color2 = _interopRequireDefault(_Color);
	
	var _Deep = __webpack_require__(9);
	
	var _default = __webpack_require__(21);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Theme = function () {
	  /**
	   *
	   * @param {ThemeOptions} theme
	   */
	  function Theme() {
	    var theme = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	    _classCallCheck(this, Theme);
	
	    /**
	     * @type {ThemeOptions}
	     */
	    this._current = (0, _Deep.deepExtend)({}, _default.defaultTheme, theme);
	    this.__initHTML();
	  }
	
	  /**
	   *
	   * @return {Theme}
	   * @private
	   */
	
	
	  _createClass(Theme, [{
	    key: '__initHTML',
	    value: function __initHTML() {
	      this._styleNode = document.createElement('style');
	      this._styleNode.setAttribute('type', 'text/css');
	      return this;
	    }
	
	    /**
	     *
	     * @return {string}
	     */
	
	  }, {
	    key: '__setStyles',
	
	
	    /**
	     *
	     * @param {ThemeOptions} [styles]
	     * @return {Theme}
	     */
	    value: function __setStyles() {
	      var styles = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	      this._current = (0, _Deep.deepExtend)({}, this._current, styles);
	      this._styleNode.textContent = this.$styles;
	      return this;
	    }
	
	    /**
	     *
	     * @param {ThemeOptions} [styles]
	     * @return {Theme}
	     */
	
	  }, {
	    key: 'init',
	    value: function init() {
	      var styles = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	      this.__setStyles(styles);
	      document.head.appendChild(this._styleNode);
	      return this;
	    }
	
	    /**
	     *
	     * @param {ThemeOptions} styles
	     * @return {Theme}
	     */
	
	  }, {
	    key: 'replace',
	    value: function replace() {
	      var styles = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	      this.__setStyles(styles);
	      return this;
	    }
	
	    /**
	     *
	     * @return {{low:string middle:string high:string}}
	     */
	
	  }, {
	    key: 'price',
	    value: function price() {
	      return this._current.color.WidgetPriceGradient;
	    }
	  }, {
	    key: '$styles',
	    get: function get() {
	
	      var darkenBtnColor = _Color2["default"].shadeRGB(_Color2["default"].toRGB(this._current.color.WidgetBtn), -.02),
	          lightenBtnColor = _Color2["default"].shadeRGB(_Color2["default"].toRGB(this._current.color.WidgetBtn), .15);
	
	      var btnHover = _Color2["default"].isLight(this._current.color.WidgetBtn) ? _Color2["default"].shadeRGB(_Color2["default"].toRGB(this._current.color.WidgetBtn), -.15) : _Color2["default"].shadeRGB(_Color2["default"].toRGB(this._current.color.WidgetBtn), .15);
	
	      var width = this._current.width ? 'div.ottbp-widget {width: ' + this._current.width + 'px !important; }' : '';
	
	      return 'div.ottbp-form-header {' + ('background: ' + this._current.color.WidgetBG + ' !important;') + '}' + 'div.ottbp-trip__header {' + ('background: ' + this._current.color.WidgetBG + ' !important;') + '}' + 'div.ottbp-container-picker{' + ('background: ' + this._current.color.WidgetBG + ' !important;') + '}' + 'div.ottbp-trip__header span.ottbp-black{' + ('color: ' + this._current.color.WidgetTX + ' !important;') + '}' + 'div.ottbp-trip__header span.ottbp-gray{' + ('color: ' + this._current.color.WidgetTX + ' !important;') + 'opacity: .5 !important;' + '}' + '.ottbp-select.ottbp-selected button.ottbp-select__btn, button.ottbp-search__btn {' + ('background: ' + this._current.color.WidgetBtn + ' !important;') + ('color: ' + this._current.color.WidgetBtnTX + ' !important;') + '}' + 'button.ottbp-search__btn svg > use.ottbp-sprite-svg__search{' + ('fill: ' + this._current.color.WidgetBtnTX + ' !important;') + '}' + '.ottbp-select.ottbp-selected button.ottbp-select__btn:hover, button.ottbp-search__btn:hover {' + ('background: ' + btnHover + ' !important;') + '}' + '.ottbp-select.ottbp-disabled button.ottbp-select__btn,' + '.ottbp-select.ottbp-disabled button.ottbp-select__btn:hover {' + 'background: rgb(100, 100, 100) !important;' + 'opacity: .6 !important;' + 'color: rgba(50, 50, 50, 0.3) !important;' + '}' + 'div.ottbp-loader__back__over {' + ('background: ' + this._current.color.WidgetBtn + ' !important;') + '}' + 'div.ottbp-loader__front {' + ('background: ' + this._current.color.WidgetBtn + ' !important;') + ('background: -moz-linear-gradient(45deg, ' + darkenBtnColor + ' 0%, ' + this._current.color.WidgetBtn + ' 50%, ' + lightenBtnColor + ' 100%) !important;') + ('background: -webkit-linear-gradient(45deg, ' + darkenBtnColor + ' 0%, ' + this._current.color.WidgetBtn + ' 50%, ' + lightenBtnColor + ' 100%) !important;') + ('background: linear-gradient(45deg, ' + darkenBtnColor + ' 0%, ' + this._current.color.WidgetBtn + ' 50%, ' + lightenBtnColor + ' 100%) !important;') + '}' + 'div.ottbp--low_price, button.ottbp--low_price {' + ('background: ' + this._current.color.WidgetPriceGradient.low + ' !important;') + '}' + 'div.ottbp--middle_price, button.ottbp--middle_price {' + ('background: ' + this._current.color.WidgetPriceGradient.middle + ' !important;') + '}' + 'div.ottbp--high_price, button.ottbp--high_price {' + ('background: ' + this._current.color.WidgetPriceGradient.high + ' !important;') + '}' + width;
	    }
	  }]);
	
	  return Theme;
	}();
	
	exports["default"] = Theme;

/***/ },
/* 88 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _addEvent = __webpack_require__(89);
	
	var _addEvent2 = _interopRequireDefault(_addEvent);
	
	var _delegate = __webpack_require__(90);
	
	var _delegate2 = _interopRequireDefault(_delegate);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 *
	 * @param {string} text
	 * @param {Array.<string>} strings
	 * @return {boolean}
	 */
	function hasStrings(text, strings) {
	  return strings.some(function (str) {
	    return text.indexOf(str) > -1;
	  });
	}
	
	var EventStore = function () {
	  function EventStore() {
	    _classCallCheck(this, EventStore);
	
	    /**
	     *
	     * @type {Object.<string, {runs: number, callbacks: Array<Function[5]>}>}
	     * @private
	     */
	    this._store = {};
	    this._DOMEvents = [];
	
	    this.__initDOMEvents();
	  }
	
	  _createClass(EventStore, [{
	    key: 'on',
	
	
	    /**
	     *
	     * @param {string|string[]} names
	     * @param {function} callback
	     * @param {number} [priority=0]
	     * @param {boolean} [startIfRan=false]
	     * @return {*}
	     */
	    value: function on(names, callback) {
	      var priority = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
	      var startIfRan = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
	
	      if (Array.isArray(names)) names.forEach(function (name) {
	        this.on(name, callback, priority, startIfRan);
	      }, this);else if (typeof names === 'string') {
	        this.__on(names, callback, priority, startIfRan);
	      }
	    }
	
	    /**
	     *
	     * @param {string} name
	     * @param {function} callback
	     * @param {number} priority
	     * @param {boolean} [startIfRan]
	     * @private
	     */
	
	  }, {
	    key: '__on',
	    value: function __on(name, callback, priority, startIfRan) {
	      priority = priority > 2 ? 2 : priority < -2 ? -2 : priority;
	      var index = 2 + priority;
	
	      this.__createEventStore(name, false, hasStrings(name, ['Document', 'Window', 'Element']));
	
	      this.$store[name].callbacks[index].push(callback);
	
	      if (startIfRan && this.$store[name].run > 0) {
	        callback({
	          type: name,
	          detail: this.$store[name].details[this.$store[name].details.length - 1]
	        });
	      }
	    }
	
	    /**
	     *
	     * @param {string|string[]} names
	     * @param {*} [detail]
	     */
	
	  }, {
	    key: 'dispatch',
	    value: function dispatch(names, detail) {
	      // console.warn("START ", names);
	      if (Array.isArray(names)) names.forEach(function (name) {
	        this.dispatch(name, detail);
	      }, this);else if (typeof names === 'string') {
	        this.__dispatch(names, detail);
	      }
	    }
	
	    /**
	     *
	     * @param {string} name
	     * @param {*} detail
	     * @private
	     */
	
	  }, {
	    key: '__dispatch',
	    value: function __dispatch(name, detail) {
	      var currentStore = this.$store[name];
	      if (currentStore) {
	        currentStore.runs++;
	        currentStore.details.push(detail);
	        currentStore.callbacks.forEach(function ( /*Function[]*/callbacks) {
	          return callbacks.forEach(function (callback) {
	            callback({
	              type: name,
	              detail: detail
	            });
	          });
	        });
	      } else {
	        this.__createEventStore(name, true, hasStrings(name, ['Document', 'Window', 'Element']));
	      }
	    }
	
	    /**
	     *
	     * @param {string} name
	     * @param {boolean} [started=false]
	     * @param {boolean} [dom=false]
	     * @private
	     */
	
	  }, {
	    key: '__createEventStore',
	    value: function __createEventStore(name) {
	      var started = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
	      var dom = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
	
	      if (typeof this.$store[name] === 'undefined') this.$store[name] = {
	        runs: started ? 1 : 0,
	        dom: dom,
	        details: [],
	        callbacks: [[], [], [], [], []]
	      };
	      return this;
	    }
	  }, {
	    key: '__addDomEvents',
	    value: function __addDomEvents(element, type, func) {
	      this._DOMEvents.push({
	        type: type,
	        element: element,
	        callback: func
	      });
	
	      (0, _addEvent2["default"])(element, type, func);
	    }
	  }, {
	    key: '__initDOMEvents',
	    value: function __initDOMEvents() {
	      var self = this;
	
	      this.__addDomEvents(window, 'resize', function (event) {
	        self.dispatch('Window::resize', {
	          width: window.innerWidth,
	          original: event
	        });
	      });
	
	      this.__addDomEvents(document, 'click', function (event) {
	        self.dispatch('Document::click', {
	          original: event
	        });
	      });
	
	      this.__addDomEvents(document, 'mousemove', function (event) {
	        self.dispatch('Document::mousemove', {
	          original: event
	        });
	      });
	
	      this.__addDomEvents(document, 'keydown', function (event) {
	        self.dispatch('Document::keydown', {
	          keyCode: event.keyCode,
	          original: event
	        });
	      });
	    }
	
	    /**
	     *
	     * @param {string} eventMethod
	     * @param {string|HTMLElement} elementSelector
	     * @param {function(event:{detail:{original:Event}})} callback
	     * @param {HTMLElement} [parent=document]
	     * @param {function(event:{detail:{original:Event}})} [notCallback]
	     */
	
	  }, {
	    key: 'delegateDocument',
	    value: function delegateDocument(eventMethod, elementSelector, callback, notCallback) {
	      var parent = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : document.body;
	
	      this.on('Document::' + eventMethod, function (event) {
	        var originatEvent = event.detail.original;
	
	        (0, _delegate2["default"])(originatEvent, elementSelector, parent, function (target) {
	          callback({
	            type: event.type,
	            detail: {
	              target: target,
	              original: originatEvent
	            }
	          });
	        }, function (event) {
	          if (notCallback) notCallback({
	            type: event.type,
	            detail: {
	              original: originatEvent
	            }
	          });
	        });
	      });
	    }
	  }, {
	    key: '$store',
	    get: function get() {
	      return this._store;
	    }
	  }]);
	
	  return EventStore;
	}();
	
	exports["default"] = EventStore;

/***/ },
/* 89 */
/***/ function(module, exports) {

	'use strict';
	/**
	 *
	 * @param {HTMLElement|HTMLDocument|Window} element
	 * @param {string} type
	 * @param {function(event:Event)} callback
	 * @param {boolean} [useCapture=false]
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports["default"] = addEvent;
	function addEvent(element, type, callback) {
	  var useCapture = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
	
	  if (element.addEventListener) {
	    element.addEventListener(type, callback, useCapture);
	  } else {
	    element.attachEvent('on' + type, callback);
	  }
	}

/***/ },
/* 90 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports["default"] = delegate;
	
	var _Element = __webpack_require__(91);
	
	var _Element2 = __webpack_require__(7);
	
	/**
	 *
	 * @param {HTMLElement} target
	 * @param {HTMLElement|Node|null} parent
	 * @param {string} DOMString
	 * @return {boolean|HTMLElement}
	 * @private
	 */
	function __isDelegateDOMString(target, parent, DOMString) {
	  var el = (0, _Element.closest)(target, DOMString);
	
	  if (!el || parent && !parent.contains(el)) {
	    return false;
	  }
	
	  return el;
	}
	/**
	 *
	 * @param {HTMLElement} target
	 * @param {HTMLElement|Node|null} parent
	 * @param {HTMLElement} element
	 * @return {boolean|HTMLElement}
	 * @private
	 */
	function __isDelegateElement(target, parent, element) {
	  while (target != parent) {
	
	    if (parent && !parent.contains(target)) {
	      return false;
	    }
	
	    if (target === element) {
	      return target;
	    }
	
	    target = target.parentNode;
	  }
	  return false;
	}
	/**
	 *
	 * @param {Event}                         event
	 * @param {string|null}                   selector
	 * @param {HTMLElement|Node|null}         parent
	 * @param {function(target:HTMLElement)}  callback
	 * @param {function(event:Event)}         [notCallback]
	 */
	function delegate(event, selector, parent, callback, notCallback) {
	  /**
	   *
	   * @type {EventTarget|HTMLElement}
	   */
	  var target = event.target;
	
	  /**
	   *
	   * @type {boolean|HTMLElement}
	   */
	  var doCallback = false;
	
	  if (typeof selector === 'string') {
	    doCallback = __isDelegateDOMString(target, parent, selector);
	  } else if ((0, _Element2.isElement)(selector) || (0, _Element2.isNode)(selector)) {
	    doCallback = __isDelegateElement(target, parent, selector);
	  }
	  if (doCallback) callback(doCallback);else if (notCallback) notCallback(event);
	}

/***/ },
/* 91 */
/***/ function(module, exports) {

	'use strict';
	/* eslint no-empty: "off" */
	/**
	 *
	 * @return {function(element:HTMLElement, selector:string):(HTMLElement|null)}
	 * @private
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	function _closest() {
	  if (window.Element && Element.prototype.closest) {
	    return function (element, selector) {
	      return Element.prototype.closest.call(element, selector);
	    };
	  }
	  return function (element, selector) {
	    var matches = (window.document || window.ownerDocument).querySelectorAll(selector),
	        i = void 0,
	        el = element;
	    do {
	      i = matches.length;
	      while (--i >= 0 && matches.item(i) !== el) {}
	    } while (i < 0 && (el = el.parentElement));
	    return el;
	  };
	}
	/**
	 *
	 * @type {function(element:HTMLElement, selector:string):(HTMLElement|null)}
	 */
	var closest = exports.closest = _closest();

/***/ },
/* 92 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.toColon = toColon;
	exports.toFormat = toFormat;
	exports.split = split;
	exports.toSeconds = toSeconds;
	
	var _l8n = __webpack_require__(74);
	
	var _l8n2 = _interopRequireDefault(_l8n);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	/**
	 *
	 * @param {string} timeString = 1010
	 * @return {string} = 10:10
	 */
	function toColon(timeString) {
	  var time = split(timeString);
	  return time[0] + ':' + time[1];
	}
	
	/**
	 *
	 * @param {string} timeString = 1010
	 * @return {string} = {10 ч 10  мин}
	 */
	function toFormat(timeString) {
	  var time = split(timeString);
	  var hour = 0,
	      min = 0;
	  if (time.length > 1) {
	    hour = parseInt(time[0], 10);
	    min = parseInt(time[1]);
	  }
	
	  if (hour) return hour + ' ' + _l8n2["default"].Direction.time.hour + ' ' + min + ' ' + _l8n2["default"].Direction.time.min;
	  return min + ' ' + _l8n2["default"].Direction.time.min;
	}
	
	/**
	 *
	 * @param {string} timeString = 1010
	 * @return {Array.<number>|Boolean} = ['10', '10']
	 */
	function split(timeString) {
	  return timeString.match(/.{1,2}/g);
	}
	
	/**
	 *
	 * @param {string} timeString = 1010
	 * @return {number} = 36600
	 */
	function toSeconds(timeString) {
	  var time = split(timeString);
	  return parseInt(time[0], 10) * 60 * 60 + parseInt(time[1], 10) * 60;
	}

/***/ }
/******/ ]);
//# sourceMappingURL=bestPrice-widget.map