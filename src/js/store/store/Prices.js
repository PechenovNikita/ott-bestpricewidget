'use strict';

/**
 * @typedef {Object} PriceOptions
 * @property {string} childType
 */

const treePriceStore = {
  global        : 'year_month',
  year_month    : 'day',
  day           : 'to_year_month',
  to_year_month : 'to_day'
};

export default class StorePrices {
  /**
   *
   * @param {StorePrices|null} [parent=null]
   */
  constructor(parent = null) {
    /**
     *
     * @type {StorePrices|null}
     * @private
     */
    this._parent = parent;
    /**
     *
     * @type {string}
     * @private
     */
    this._type = (parent ? treePriceStore[parent.$type] : 'global');
    /**
     *
     * @type {Array.<number>}
     * @private
     */
    this._prices = [];
    /**
     *
     * @type {Array.<string>}
     * @private
     */
    this._dataKeys = [];
    /**
     *
     * @type {Object.<(number|string),StorePrices>}
     * @private
     */
    this._children = {};

    this._cache = {
      minPrices : null,

      minLower  : null,
      minMiddle : null,
      minHigher : null
    };
  }

  /**
   *
   * @return {StorePrices|null}
   */
  get $parent() {
    return this._parent;
  }

  /**
   *
   * @return {StorePrices}
   */
  get $global() {
    if (this._parent)
      return this._parent.$global;
    return this;
  }

  /**
   *
   * @return {string}
   */
  get $type() {
    return this._type;
  }

  /**
   *
   * @return {Array.<number,null>}
   */
  get $minPrices() {
    if (this._cache.minPrices)
      return this._cache.minPrices;

    let prices = [];
    for (let prop in this._children) {
      if (this._children.hasOwnProperty(prop)) {
        prices.push(this._children[prop].$minLower);
      }
    }

    if (prices.length == 0 && this._prices.length > 0)
      prices = this._prices;

    this._cache.minPrices = prices;
    return prices;
  }

  /**
   *
   * @return {number|null}
   */
  get $minLower() {
    if (!this._cache.minLower)
      this._cache.minLower = Math.min.apply(null, this.$minPrices) || null;
    return this._cache.minLower;
  }

  /**
   *
   * @return {number|null}
   */
  get $minMiddle() {
    if (!this._cache.minMiddle) {
      let price = this.$minPrices;
      if (price.length < 0)
        return null;
      this._cache.minMiddle = price.reduce((sa, num) => (sa + num), 0) / price.length;
    }
    return this._cache.minMiddle;
  }

  /**
   *
   * @return {number|null}
   */
  get $minHigher() {
    if (!this._cache.minHigher)
      this._cache.minHigher = Math.max.apply(null, this.$minPrices) || null;
    return this._cache.minHigher;
  }

  /**
   *
   * @return {[(number|null),(number|null),(number|null)]}
   */
  get $minRange() {
    return [this.$minLower, this.$minMiddle, this.$minHigher];
  }

  /**
   *
   * @return {number}
   */
  get $delta() {
    if (!this.$parent || this.$parent.$minPrices.length == 0 || this.$minPrices.length == 0)
      return 0;
    return (this.$minLower - this.$parent.$minLower) / (this.$parent.$minHigher - this.$parent.$minLower);
  }

  /**
   *
   * @return {Array.<string>}
   */
  get $keys() {
    return this._dataKeys;
  }

  // Methods

  __clearCache() {
    this._cache = {
      minPrices : null,

      minLower  : null,
      minMiddle : null,
      minHigher : null
    };
    return this;
  }

  /**
   *
   * @param {string} key
   * @param {number} price
   * @param {...string|...number} indexes
   * @return {StorePrices}
   */
  addKey(key, price, ...indexes) {
    this.__addThisKey(key, price).__addChildKey(key, price, indexes);
    return this;
  }

  /**
   *
   * @param {string} key
   * @param {number} price
   * @return {StorePrices}
   * @private
   */
  __addThisKey(key, price) {
    if (this._dataKeys.indexOf(key) < 0) {
      this._dataKeys.push(key);
      this._prices.push(price);
      this.__clearCache();
    }
    return this;
  }

  /**
   *
   * @param {string} key
   * @param {number} price
   * @param {string[]|number[]} indexes
   * @return {StorePrices}
   * @private
   */
  __addChildKey(key, price, indexes) {

    if (indexes.length > 0) {
      let index = indexes.shift();

      if (!this._children[index]) {
        this._children[index] = new StorePrices(this);
      }
      this._children[index].addKey.apply(this._children[index], [key, price].concat(indexes));
    }

    return this;
  }

  /**
   *
   * @param indexes
   * @return {null|StorePrices}
   */
  getBy(...indexes) {
    // если нет аргументов возвращаем текущее хранилище
    if (indexes.length > 0) {
      let index = indexes.shift();
      // если есть внутреннее хранилище с индексом возвращаем его
      if (this._children[index]) {
        return this._children[index].getBy.apply(this._children[index], indexes);
      }
      return null;
    }
    return this;
  }

  /**
   *
   * @param indexes
   * @return {null|StorePrices}
   */
  getByOrThis(...indexes) {
    // если нет аргументов возвращаем текущее хранилище
    if (indexes.length > 0) {
      let index = indexes.shift();
      // если есть внутреннее хранилище с индексом возвращаем его
      if (this._children[index]) {
        return this._children[index].getByOrThis.apply(this._children[index], indexes);
      }
      return this;
    }
    return this;
  }
}