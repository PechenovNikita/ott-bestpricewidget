'use strict';


// @widthRoute: 220px + (2 * @gutter);
// @widthType: 136px + (2 * @gutter);
// @widthTransfer: 220px + (2 * @gutter);
// @widthDuration: 220px + (2 * @gutter);

const GUTTER = 6;

const WIDTH = 220 + 136 + 220 + 220 + (GUTTER * 4 * 2);

function element(width, type, gutter = GUTTER) {
  return {
    type    : type,
    width   : width,
    percent : Math.floor(((width + (gutter * 2)) / WIDTH ) * 10000) / 100
  };
}

const WIDTHS = {
  1011 : {
    extra_small : {
      row   : 4,
      items : [
        element(WIDTH - 2 * GUTTER, 'route'),
        element(0, 'type', 0),
        element(WIDTH - 2 * GUTTER, 'transfer'),
        element(WIDTH - 2 * GUTTER, 'duration')
      ]
    },
    small       : {
      row   : 2,
      items : [
        element(WIDTH / 2 - 2 * GUTTER, 'route'),
        element(0, 'type', 0),
        element(WIDTH / 2 - 2 * GUTTER, 'transfer'),
        element(WIDTH - 2 * GUTTER, 'duration')
      ]
    },
    medium      : {
      row   : 1,
      items : [
        element(WIDTH / 3 - 2 * GUTTER, 'route'),
        element(0, 'type', 0),
        element(WIDTH / 3 - 2 * GUTTER, 'transfer'),
        element(WIDTH / 3 - 2 * GUTTER, 'duration')
      ]
    },
    big         : {
      row   : 1,
      items : [
        element(WIDTH / 3 - 2 * GUTTER - 10, 'route'),
        element(0, 'type', 0),
        element(WIDTH / 3 - 2 * GUTTER + 20, 'transfer'),
        element(WIDTH / 3 - 2 * GUTTER - 10, 'duration')
      ]
    }
  },
  1010 : {
    extra_small : {
      row   : 4,
      items : [
        element(WIDTH - 2 * GUTTER, 'route'),
        element(0, 'type', 0),
        element(WIDTH - 2 * GUTTER, 'transfer'),
        element(0, 'duration', 0)
      ]
    },
    small       : {
      row   : 2,
      items : [
        element(WIDTH / 2 - 2 * GUTTER, 'route'),
        element(0, 'type', 0),
        element(WIDTH / 2 - 2 * GUTTER, 'transfer'),
        element(0, 'duration', 0)
      ]
    },
    medium      : {
      row   : 1,
      items : [
        element(WIDTH / 2 - 2 * GUTTER, 'route'),
        element(0, 'type', 0),
        element(WIDTH / 2 - 2 * GUTTER, 'transfer'),
        element(0, 'duration', 0)
      ]
    },
    big         : {
      row   : 1,
      items : [
        element(WIDTH / 2 - 2 * GUTTER, 'route'),
        element(0, 'type', 0),
        element(WIDTH / 2 - 2 * GUTTER, 'transfer'),
        element(0, 'duration', 0)
      ]
    }
  },
  1111 : {
    extra_small : {
      row   : 4,
      items : [
        element(WIDTH - 2 * GUTTER, 'route'),
        element(WIDTH - 2 * GUTTER, 'type'),
        element(WIDTH - 2 * GUTTER, 'transfer'),
        element(WIDTH - 2 * GUTTER, 'duration')
      ]
    },
    small       : {
      row   : 2,
      items : [
        element(WIDTH / 2 - 2 * GUTTER, 'route'),
        element(WIDTH / 2 - 2 * GUTTER, 'type'),
        element(WIDTH / 2 - 2 * GUTTER, 'transfer'),
        element(WIDTH / 2 - 2 * GUTTER, 'duration')
      ]
    },
    medium      : {
      row   : 1,
      items : [
        element(220 - 50, 'route'),
        element(136 - 10, 'type'),
        element(220 + 5, 'transfer'),
        element(220 + 55, 'duration')
      ]
    },
    big         : {
      row   : 1,
      items : [
        element(220, 'route'),
        element(136, 'type'),
        element(220, 'transfer'),
        element(220, 'duration')
      ]
    }
  },
  1110 : {
    extra_small : {
      row   : 3,
      items : [
        element(WIDTH - 2 * GUTTER, 'route'),
        element(WIDTH - 2 * GUTTER, 'type'),
        element(WIDTH - 2 * GUTTER, 'transfer'),
        element(0, 'duration', 0)
      ]
    },
    small       : {
      row   : 2,
      items : [
        element(WIDTH / 2 - 2 * GUTTER, 'route'),
        element(WIDTH / 2 - 2 * GUTTER, 'type'),
        element(WIDTH - 2 * GUTTER, 'transfer'),
        element(0, 'duration', 0)
      ]
    },
    medium      : {
      row   : 1,
      items : [
        element(220 + (220 + GUTTER * 2) / 3, 'route'),
        element(136 + (220 + GUTTER * 2) / 3, 'type'),
        element(220 + (220 + GUTTER * 2) / 3, 'transfer'),
        element(0, 'duration', 0)
      ]
    },
    big         : {
      row   : 1,
      items : [
        element(220 + (220 + GUTTER * 2) / 3, 'route'),
        element(136 + (220 + GUTTER * 2) / 3, 'type'),
        element(220 + (220 + GUTTER * 2) / 3, 'transfer'),
        element(0, 'duration', 0)
      ]
    }
  }
};


export default WIDTHS;