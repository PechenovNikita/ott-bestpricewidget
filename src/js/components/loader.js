'use strict';
import {docCreate} from './../mixins/Element';
import SearchSVG from '../mixins/svg/search';
import classList from '../mixins/polyfill/element/classList';

export default class Loader {
  constructor(container, options = {}) {
    this._continer = container;
    this._options = options;

    this.initHTML();
  }

  initHTML() {
    let classes = ['ottbp-loader', 'ottbp-loader--hide'];
    if (this._options.small)
      classes.push('ottbp-loader--small');
    this._node = docCreate('div', classes);

    let icon = '';
    if (this._options.small)
      icon = (new SearchSVG()).resize(.4 * 39, .4 * 39).toString();
    else
      icon = (new SearchSVG()).resize(.8 * 39, .8 * 39).toString();

    let inner = '<div class="ottbp-loader__back"><div class="ottbp-loader__back__over"></div></div>';
    inner += `<div class="ottbp-loader__front"><div class="ottbp-loader__front__icon">${icon}</div></div>`;

    this._node.innerHTML = inner;

    this._continer.appendChild(this._node);
    return this;
  }

  hide(callback) {
    classList(this._node).add('ottbp-loader--hide');
    if (callback)
      setTimeout(callback, 500);
  }

  show(callback) {
    if (callback) {
      callback(this.__show.bind(this));
    } else
      this.__show();
  }

  __show() {
    classList(this._node).remove('ottbp-loader--hide');
  }
}