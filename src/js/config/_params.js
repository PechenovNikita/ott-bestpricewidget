'use strict';

import {deepExtend} from '../mixins/Deep';

let params = {
  marker : 'none',
  demo   : false,
  id     : Date.now()
};

let init = false;

export default {
  get(prop){
    return params[prop];
  },
  updateMarker(marker){
    params.marker = marker;
  },
  init(data){
    if (init)
      return;
    init = true;
    deepExtend(params, data);
  }
};
