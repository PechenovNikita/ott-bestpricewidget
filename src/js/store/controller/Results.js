/*eslint no-unused-vars: ["error", { "args": "none" }]*/
'use strict';

import {bestDealsAsync, searchingFlights, convertSearchFareToBestDeal} from './../../libs/OneTwoTrip';
import {monthList} from './../../mixins/Date';
import Store from './../Store';

/**
 *
 * @typedef {Object} ResultsOptions
 * @property {number} [count]
 * @property {boolean} [monthly]
 * @property {Object.<string, number>} [months]
 * @property {string[]} [fromDates]
 * @property {string[]} [done]
 */

/**
 *
 * @param {Response} responseA
 * @param {Response} responseB
 * @return {number}
 */
function sortResponseByPrice(/*Response*/responseA, /*Response*/responseB) {
  return responseA.$price - responseB.$price;
}

export default class ControllerResults {
  /**
   *
   * @param {EventStore} eventStore
   */
  constructor(eventStore) {
    this._eventStore = eventStore;
    /**
     *
     * @type {Object.<string,{count:number, monthly:boolean}>}
     * @private
     */
    this._requests = {};
    /**
     *
     * @type {Store}
     * @private
     */
    this._store = new Store();
  }

  __complete(query) {
    this._eventStore.dispatch('Results::done:request', {
      results : this,
      query   : query
    });
  }

  /**
   *
   * @param {Query} query
   * @return {boolean}
   */
  common(query) {
    return this.__bestDeals(query, query.$commonBestDealsQuery);
  }

  /**
   *
   * @param {Query} query
   */
  bestDeals(query) {
    return this.__bestDeals(query, query.$bestDealsQuery);
  }

  /**
   *
   * Возвращает boolean (true - будем грузить результаты, false - результаты загружены)
   * @param {Query} query
   * @param {OttDealsQuery} params
   * @param {function} [callback]
   * @return {boolean}
   * @private
   */
  __bestDeals(query, params, callback) {

    this._query = query;

    let request = this._store.getRequest(query);

    if (request.isLoadRequest(params)) {
      return true;
    }

    if (request.isDoneRequest(params)) {
      return false;
    }

    request.add();

    request.loadRequest(params);

    /**
     * @this {ControllerResults}
     */
    bestDealsAsync(params, {
      success : function (response) {
        this.__parseDealsResponse(response, query);
        request.doneRequest(params);
        if (callback)
          callback();
      }.bind(this),
      always  : function () {
        // this.__removeRequest(query);
        request
          .done()
          .isFull(this.__complete.bind(this, query));
      }.bind(this)
    });

    return true;

  }

  /**
   * Загружаем данные по месяцам и дням
   * @param {Query} query
   * @return {boolean}
   */
  monthly(query) {
    this._query = query;

    let request = this._store.getRequest(query);
    return monthList.reduce(function (/*boolean*/loaded, /*({year:number,month:number})*/month) {
      /**
       * @this {ControllerResults}
       */
      if (request.isDoneMonth(`${month.year}_${month.month}`)) {
        // если месяц уже загружен
        return loaded || false;
      } else {
        return this.__bestDeals(query, query.bestDealsQueryMonthly(month.year, month.month)) || loaded;
      }
    }.bind(this), false);

  }

  /**
   *
   * @param {Query} query
   * @return {boolean}
   */
  fromDateToMonthly(query) {
    this._query = query;

    if (!query.$needFromDayToMonthly)
      return false;

    return monthList.reduce(function (/*boolean*/loaded, /*{year:number,month:number}*/month) {
      /**
       * @this {ControllerResults}
       */
      return this.__bestDeals(query, query.bestDealsQueryReturnMonthly(month.year, month.month)) || loaded;
    }.bind(this), false);
  }

  /**
   *
   * @param {Query} query
   * @return {boolean}
   */
  fullSearch(query) {
    this._query = query;

    if (query.$searchingQuery) {

      let request = this._store.getRequest(query)
        , params = query.$searchingQuery;

      if (request.isDoneRequest(params))
        return false;

      request.add();
      // this.__addRequest(query);

      searchingFlights(params, {
        success : function (response) {
          /**
           * @this {ControllerResults}
           */
          this.__parseSearchingString(response, query);
          request.doneRequest(params);
        }.bind(this),
        always  : function () {
          /**
           * @this {ControllerResults}
           */
          request.done().isFull(this.__complete.bind(this, query));
        }.bind(this)
      });

      return true;
    }
    return false;

  }

  /**
   *
   * @param {Query} query
   * @param {string} cityIata
   * @return {boolean}
   */
  addCity(query, cityIata) {

    let cityQuery = query.clone({
      inputs : {
        toCountry : null,
        to        : cityIata
      }
    });

    let cityParams = cityQuery.$searchingQuery;

    if (cityParams) {
      let request = this._store.getRequest(query);
      if (request.isDoneRequest(cityParams))
        return false;
      request.add();

      searchingFlights(cityParams, {
        success : function (response) {
          /**
           * @this {ControllerResults}
           */
          this.__parseSearchingString(response, query, cityIata);
          request.doneRequest(cityParams);
        }.bind(this),
        always  : function () {
          /**
           * @this {ControllerResults}
           */
          request.done().isFull(this.__complete.bind(this, query));
        }.bind(this)
      });
      return true;
    }
    return false;

  }

  /**
   *
   * @param {{[status]:string, [data]:{[frs]:SearchOptions[]}}} response
   * @param {Query} query
   * @param {string} [cityIata]
   * @private
   */
  __parseSearchingString(response, query, cityIata = query.$destinationCity) {
    // let jsonResponse = JSON.parse(response);
    if (!response || !response || !response.data || response.status !== 'success' || !response.data.frs)
      return;

    response.data.frs.forEach(function (/* SearchOptions */fare) {
      /**
       *
       * @type {TripOptions}
       */
      let bestDealFare = convertSearchFareToBestDeal(fare);

      bestDealFare.from = query.$origin;
      bestDealFare.to = cityIata;

      this._store.addData(query, bestDealFare);

      // this._result.addBestDeals(query, bestDealFare);
    }.bind(this));
  }

  /**
   *
   * @param response
   * @param query
   * @private
   */
  __parseDealsResponse(response, query) {
    if (!response || typeof response === 'string')
      return;

    response.forEach(function (item) {
      this._store.addData(query, item);
      // this._result.addBestDeals(query, item);
    }, this);

  }

  /**
   *
   * @typedef {Object}            ResultsShow
   * @property {boolean}            loading
   * @property {Store}              store
   * @property {Query}              query
   * @property {StorePrices}        price
   * @property {Array.<Response>}   results
   */

  /**
   *
   * @param {Query} query
   * @return {ResultsShow}
   */
  show(query) {
    // return this._result.response(query);
    /**
     *
     * @type {number}
     */
    let count = 0;
    /**
     *
     * @type {boolean}
     */
    let isCountry = query.$isDestinationCountry;
    /**
     *
     * @type {Array<string>}
     */
    let cities = [];
    /**
     *
     * @type {Array<Response>}
     */
    let minCity = [];
    /**
     *
     * @type {Array.<Response>}
     */
    let results = this._store.getResponse(query)
    // Сортируем выдачу
      .sort(sortResponseByPrice)
      // Фильтруем выдачу
      .filter(function (/*Response*/response) {

        if (count >= 3)
          return false;

        if (!isCountry) {
          count++;
          return (count < 4);
        }

        if (cities.indexOf(response.$to) < 0) {
          count++;
          cities.push(response.$to);
          return (count < 4);
        }

        minCity.push(response);

        return false;

      }, this);

    if (minCity.length > 0) {
      switch (results.length) {
        case 0:
          results = results.concat(minCity.splice(0, 3)).sort(sortResponseByPrice);
          break;
        case 1:
          results = results.concat(minCity.splice(0, 2)).sort(sortResponseByPrice);
          break;
        case 2:
          results = results.concat(minCity.splice(0, 1)).sort(sortResponseByPrice);
          break;
      }
    }

    return {
      store   : this._store,
      loading : !this._store.getRequest(query).isFull(),
      query   : query,
      price   : this._store.getPrices(query),
      results : results
    };
  }

  price(query) {

  }

}