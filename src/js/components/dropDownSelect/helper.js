'use strict';
import {docCreate, appendChildren} from './../../mixins/Element';
// import addEvent from '../../mixins/event/addEvent';

export default class DropDownHelper {
  /**
   *
   * @param {DropDownSelectOptions[]} options
   * @param {number} selected
   * @param {HTMLElement} container
   * @param {EventStore} eventStore
   */
  constructor(options, selected, container, eventStore) {
    this._eventStore = eventStore;
    /**
     *
     * @type {DropDownSelectOptions}
     * @private
     */
    this._items = options;
    /**
     *
     * @type {number}
     * @private
     */
    this._selected = selected;
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._container = container;
    this.__initHTML().__initEvents();
  }

  /**
   *
   * @return {DropDownHelper}
   */
  __initHTML() {
    // ul.ottbp-select-helper
    //   li.ottbp-select-helper__item.selected
    //     a.ottbp-select-helper-item.ottbp-texts--btn
    //       span.ottbp-select-helper__inner!= value
    this._node = docCreate('ul', 'ottbp-select-helper');

    this.__initItemsHTML();

    this._container.appendChild(this._node);

    return this;
  }

  /**
   *
   * @return {DropDownHelper}
   * @private
   */
  __initItemsHTML() {

    this._indexes = [this._selected];
    /**
     * @this {DropDownHelper}
     * @type {Array.<*>}
     */
    let arr = [this._items[this._selected]].concat(this._items.filter(function (item, index) {
      if (index !== this._selected)
        this._indexes.push(index);
      return index !== this._selected;
    }, this));

    /**
     * @type {DocumentFragment}
     */
    let fragment = appendChildren(document.createDocumentFragment(), arr.map(function (item, index) {
      let li = docCreate('li', ['ottbp-select-helper__item', (index == 0 && item.orange ? 'ottbp-selected' : '')]);
      li.innerHTML = `<a data-index="${index}" class="ottbp-select-helper-item ottbp-texts--btn"><span class="ottbp-select-helper__inner">${item.title}</span></a>`;
      return li;
    }, this));

    this._node.appendChild(fragment);
    return this;
  }

  /**
   *
   * @return {DropDownHelper}
   * @private
   */
  __initEvents() {
    this._eventStore.delegateDocument('click', '.ottbp-select-helper-item', ::this.__onClick_v2, undefined, this._node);
    // addEvent(this._node, 'click', ::this.__onClick);
    return this;
  }

  __onClick(event) {
    event.stopPropagation();
    event.preventDefault();
    let target = event.target;
    while (target && this._node !== target) {
      if (target.className.indexOf('ottbp-select-helper-item') >= 0 && target.hasAttributes('data-index')) {
        this.onSelect(parseInt(target.getAttribute('data-index'), 10));
        break;
      }
      target = target.parentNode;
    }
  }

  __onClick_v2(event) {
    const originalEvent = event.detail.original;
    const target = event.detail.target;

    originalEvent.stopPropagation();
    originalEvent.preventDefault();

    if (target.hasAttributes('data-index')) {
      this.onSelect(parseInt(target.getAttribute('data-index'), 10));
    }
  }

  /**
   *
   * @param {function} callback
   * @return {DropDownHelper}
   */
  whenSelect(callback) {
    this._whenSelect = callback;
    return this;
  }

  onSelect(index) {
    let i = this._indexes[index];
    if (this._whenSelect)
      this._whenSelect(i);
    return this.select(i);
  }

  /**
   *
   * @param {number} index
   * @return {DropDownHelper}
   */
  select(index) {

    this._selected = index;
    this._node.innerHTML = '';
    this.__initItemsHTML();
    return this;
  }

  /**
   *
   * @param {DropDownSelectOptions[]} items
   * @param {number} index
   * @return {DropDownHelper}
   */
  update(items, index) {
    this._items = items;
    return this.select(index);
  }
}