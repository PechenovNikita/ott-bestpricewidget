'use strict';

export default class Request {
  constructor() {
    /**
     *
     * @type {number}
     * @private
     */
    this._count = 0;
    /**
     *
     * @type {boolean}
     * @private
     */
    this._monthly = false;
    /**
     *
     * @type {Object.<string,(undefined|boolean)>}
     * @private
     */
    this._months = {};
    // this._fromDates = [];
    /**
     *
     * @type {Array.<string>}
     * @private
     */
    this._done = [];
    /**
     *
     * @type {Array.<string>}
     * @private
     */
    this._load = [];
  }

  /**
   *
   * @return {number}
   */
  get $count() {
    return this._count;
  }

  /**
   *
   * @return {boolean}
   */
  get $monthly() {
    return this._monthly;
  }

  /**
   *
   * @return {Request}
   */
  add() {
    this._count++;
    return this;
  }

  /**
   *
   * @return {Request}
   */
  done() {
    this._count--;
    if (this._count < 0) {
      this._count = 0;
    }
    return this;
  }

  /**
   * @param {function} [callback]
   * @return {boolean}
   */
  isFull(callback) {
    let temp = this._count == 0;
    if (temp && callback)
      callback();
    return temp;
  }

  /**
   *
   * @param {function} [callback]
   * @return {boolean}
   */
  isMonthly(callback) {
    let temp = !!this.$monthly;
    if (temp && callback)
      callback();
    return temp;
  }

  /**
   *
   * @return {Request}
   */
  initMonthly() {
    this._monthly = true;
    return this;
  }

  /**
   *
   * @return {Request}
   */
  emptyMonthly() {
    this._monthly = false;
    return this;
  }

  /**
   *
   * @param {string} key
   * @return {boolean}
   */
  isDoneMonth(key) {
    return !!this._months[key];
  }

  /**
   *
   * @param {string} key
   * @return {Request}
   */
  doneMonth(key) {
    this._months[key] = true;
    return this;
  }

  loadRequest(params) {
    this._load.push(JSON.stringify(params));
    return this;
  }

  /**
   *
   * @param {Object} params
   * @return {Request}
   */
  doneRequest(params) {
    /**
     * @type {string}
     */
    let key = JSON.stringify(params);
    /**
     *
     * @type {number}
     */
    let index = this._load.indexOf(key);
    if (index > -1)
      this._load.splice(index, 1);
    this._done.push(key);
    return this;
  }

  /**
   *
   * @param {Object} params
   * @return {boolean}
   */
  isLoadRequest(params) {
    return this._load.indexOf(JSON.stringify(params)) > -1;
  }

  /**
   *
   * @param {Object} params
   * @return {boolean}
   */
  isDoneRequest(params) {
    return this._done.indexOf(JSON.stringify(params)) > -1;
  }
}
