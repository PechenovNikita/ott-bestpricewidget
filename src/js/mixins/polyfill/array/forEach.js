'use strict';

import {isNative} from '../../Format';


const forEach = (() => {
  if (Array.prototype.forEach && isNative(Array.prototype.forEach)) {
    return (arr, callback, thisArg) => {
      Array.prototype.forEach.call(arr, callback, thisArg);
    }
  }
  return (arr, callback, thisArg) => {
    for (let i = 0; i < arr.length; i++) {
      callback.apply(thisArg, [arr[i], i, arr]);
    }
  }
})();

export default forEach;
