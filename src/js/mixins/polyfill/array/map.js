'use strict';

import {isNative} from '../../Format';

const map = (() => {
  if (Array.prototype.map && isNative(Array.prototype.map)) {
    return (arr, callback, thisArg) => {
      return Array.prototype.map.call(arr, callback, thisArg);
    }
  }
  return (arr, callback, thisArg) => {
    let newArr = [];

    for (let i = 0; i < arr.length; i++) {
      newArr.push(callback.apply(thisArg, [arr[i], i, arr]));
    }
    return newArr;
  }
})();

export default map;