/**
 *
 * @type {FormatMainForm}
 */
export default {
  inputs    : {
    from      : 'MOW',
    to        : null,
    toCountry : 'US'
  },
  selectors : {
    duration : 'any',
    route    : true,
    transfer : 'any',
    type     : 'E'
  },
  date      : {
    start   : null,
    end     : null,
    inMonth : null
  }
};