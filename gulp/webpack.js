const webpack = require('webpack')
  , autoprefixer = require('autoprefixer')
  , precss = require('precss');

var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {

  entry  : {'app' : './src/js/app.js'},
  output : {

    filename          : 'ott-widget.js',
    sourceMapFilename : 'ott-widget.map'
  },

  module    : {
    // preLoaders : [
    //   {
    //     test   : /.jsx?$/,
    //     loader : 'regexp',
    //     rules  : dev_rules
    //   }
    // ],
    loaders     : [
      {
        test    : /.jsx?$/,
        loader  : 'babel-loader',
        exclude : /node_modules/,

        query : {
          compact : false,
          presets : ['es2015', /*'babili', */'stage-0'],
        }
      },
      // {
      //   test: /\.css$/,
      //   loader: 'raw-loader'
      // },
      /*{
       test: /\.css$/,
       loader: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader"})
       },*/
      {
        test   : /\.css$/,
        loader : "style-loader!css-loader!postcss-loader",
      },
      // {
      //   test   : /\.css$/,
      //   loader : "css-loader",
      //   options: {
      //     minimize: true || {/* CSSNano Options */}
      //   }
      // },

      {
        test   : /\.less$/,
        loader : "style-loader!css-loader!postcss-loader!less-loader"
      }

    ],
    postLoaders : [
      {
        test   : /\.jsx?$/,
        loader : 'es3ify-loader'
      }
    ]

    /*,
     postLoaders:[
     {
     test    : /.jsx?$/,
     loader  : 'babel-loader',
     exclude : /node_modules/,

     query : {
     compact : false,
     presets : ['es2015', 'stage-0'],
     }
     },
     ]*/

  },
  postcss   : function () {
    return [autoprefixer, precss];
  },
  devtool   : 'source-map',
  externals : {
    // 'react-dom' : 'ReactDOM',
    // 'react'     : 'React',
    // 'pikaday'   : 'Pikaday',
    'loader'      : 'Loader',
    'logger'      : 'Logger',
    'lib-Airline' : 'Airline',
    'lib-refData' : 'refData'
    // 'stations'  : 'Stations'
  },
  // Use the plugin to specify the resulting filename (and add needed behavior to the compiler)
  // plugins: [
  //   new ExtractTextPlugin("ott-widget.css")
  // ],
  plugins   : [
    new webpack.optimize.UglifyJsPlugin({
      compress : {
        warnings : true
      }
    })
  ],
  resolve   : {
    extensions : ['', '.js']
  }
};

// [01:52:47] Using gulpfile ~/Documents/Projects/ott/widget/GulpFile.js
//   [01:52:47] Starting 'js:build'...
// [01:53:00] Version: webpack 1.14.0
// Asset     Size  Chunks             Chunk Names

// ott-widget.js   874 kB       0  [emitted]  app
// ott-widget.map  1.28 MB       0  [emitted]  app

//   [01:53:00] Finished 'js:build' after 13 s