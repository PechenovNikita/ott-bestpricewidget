'use strict';

import map from './array/map';
import forEach from './array/forEach';
import reduce from './array/reduce';
import filter from './array/filter';

export default {
  map     : map,
  forEach : forEach,
  reduce  : reduce,
  filter  : filter
};


