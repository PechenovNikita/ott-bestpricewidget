import {betweenRGB, hexToRgb} from './mixins';


export default class Gradient {
  /**
   *
   * @param {Array<{range:number,color:string,[rgb]:{r:number,b:number,g:number}}>} options
   */
  constructor(options) {
    this._options = options.sort(function (a, b) {
      if (a.range > b.range)
        return 1;
      else if (a.range < b.range)
        return -1;
      return 0;
    }).map(function (item) {
      item.rgb = hexToRgb(item.color);
      return item;
    });
  }

  /**
   *
   * @param {number} delta
   * @return {*}
   */
  find(delta) {

    if (this._options.length == 2) {
      return betweenRGB(this._options[0].rgb, this._options[1].rgb, delta);
    }

    let start = this._options[0];
    let range = this._options[0].range, index = 0;

    while (delta >= range && index < this._options.length - 1) {
      start = this._options[index];
      index++;
      range = this._options[index].range;
    }

    if (!this._options[index]) {
      return start.rgb;
    }

    let end = this._options[index];

    return betweenRGB(start.rgb, end.rgb, (delta - start.range) / (end.range - start.range) || 0);

  }
}