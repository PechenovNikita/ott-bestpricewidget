'use strict';
/**
 *
 * @param {PlaceData} placeDataA
 * @param {PlaceData} placeDataB
 * @param {string} part
 * @return {number}
 */
export default function sortPlaceData(placeDataA, placeDataB, part){
  let partLow = part.toLowerCase()
    , typeA = placeDataA.type
    , typeB = placeDataB.type;

  let indexA = placeDataA[typeA].ru.indexOf(part)
    , indexALow = placeDataA[typeA].ru.toLowerCase().indexOf(partLow);

  let indexB = placeDataB[typeB].ru.indexOf(part)
    , indexBLow = placeDataB[typeB].ru.toLowerCase().indexOf(partLow);

  let index = 0;

  if (indexA > -1 && indexB > -1) {
    index = indexA - indexB;
  } else if (indexA > -1) {
    index = -1;
  } else if (indexB > -1) {
    index = 1;
  } else {
    index = indexALow - indexBLow;
  }

  if (index == 0) {

    if (placeDataA[typeA].ru > placeDataB[typeB].ru) {
      index = 1;
    } else if (placeDataA[typeA].ru < placeDataB[typeB].ru) {
      index = -1;
    }
  }

  return index;
}
