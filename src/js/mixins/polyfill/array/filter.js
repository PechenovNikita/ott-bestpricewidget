'use strict';

import {isNative} from '../../Format';

const filter = (() => {
  if (Array.prototype.filter && isNative(Array.prototype.filter))
    return (arr, callback, thisArg) => {
      return Array.prototype.filter.call(arr, callback, thisArg);
    };

  return (arr, callback, thisArg) => {
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
      if (callback.apply(thisArg, [arr[i], i, arr]))
        newArr.push(arr[i]);
    }
    return newArr;
  };
})();

export default filter;
