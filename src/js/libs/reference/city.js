'use strict';

import citiesEN from '../ref/cities';
import citiesRU from '../refRu/cities';

import findKeys from './mixins/findKeys';
import sortPlaceData from './mixins/sortPlaceData';

import {byIATA as countryByIATA} from './country';

/**
 *
 * @param {string} iata
 * @return {PlaceData|null}
 */
export function byIATA(iata) {
  return __getCity(iata);
}

/**
 *
 * @param {string} iata
 * @return {PlaceData|null}
 * @private
 */
function __getCity(iata) {
  if (citiesEN.hasOwnProperty(iata)) {

    let cityData = citiesEN[iata].split('|');
    let cityRuName = __getCityRU(iata);

    if (!cityRuName)
      return null;

    let country = countryByIATA(cityData[1]);

    if(!country)
      return null;

    return {
      iata    : iata,
      type    : 'city',
      city    : {
        iata : iata,
        en   : cityData[0],
        ru   : cityRuName
      },
      country : {
        iata : country.iata,
        ru   : country.country.ru,
        en   : country.country.en
      }
    };
  }
  return null;
}
/**
 *
 * @param {string} iata
 * @return {null|string}
 * @private
 */
function __getCityRU(iata) {
  if (citiesRU.hasOwnProperty(iata)) {
    return citiesRU[iata];
  }
  return null;
}
/**
 *
 * @param {string} part
 * @return {Array.<PlaceData>}
 */
export function byPart(part) {
  return findKeys(part, citiesRU)
    .map(function (/*string*/iata) {
      return byIATA(iata);
    }).filter(function (/*(PlaceData|null)*/cityData) {
      return !!cityData;
    }).sort(function (/*PlaceData*/cityDataA, /*PlaceData*/cityDataB) {
      return sortPlaceData(cityDataA, cityDataB, part);
    });
}


