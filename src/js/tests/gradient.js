// #test-gradient
import Color from './../mixins/Color';

function createTd(colorRGB, html) {
  let td = document.createElement('td');
  // td.style.width = '20px';
  td.style.height = '10px';
  td.style.backgroundColor = `rgb(${colorRGB.r | 0},${colorRGB.g | 0},${colorRGB.b | 0})`;

  // td.innerHTML = html;
  return td;
}

let table = document.createElement('table')
  , tr = document.createElement('tr')
  , tr2 = document.createElement('tr')
  , td2 = document.createElement('td');

table.style.width = '100%';

let i = 0, countTd = 20, gradient = [{
  range : 0,
  // color : '#EFF9E8'
  color : '#a5e3af'
}, {
  range : .5,
  color : '#EFF9E8'
}, {
  range : 1,
  // color : '#000000'
  color : '#ffc2a3'
}];

while (i <= countTd) {
  tr.appendChild(createTd(Color.inGradient(gradient, i / countTd), i / countTd));
  i++;
}

td2.setAttribute('colspan', countTd + 1);
td2.style.height = '10px';
td2.style.backgroundImage = `linear-gradient(to right, ${gradient.map(function (color) {
  return `${color.color} ${(color.range * 100) | 0}%`
}).join(', ')})`;

tr2.appendChild(td2);
table.appendChild(tr);
table.appendChild(tr2);

document.getElementById('test-gradient').appendChild(table);

(function () {
  let btn = document.getElementById('test-gradient-btn')
    , container = document.getElementById('test-gradient');
  let input = document.getElementById('test-gradient-prices');

  let prices = [], count = 20;

  let table = document.createElement('table')
    , trPrices = document.createElement('tr')
    , trGradientCenter = document.createElement('tr')
    , trGradientMiddle = document.createElement('tr')
    , trPricesMiddle = document.createElement('tr')
    , trPricesMiddleAll = document.createElement('tr')
    , trPricesCenter = document.createElement('tr')
    , trPricesCenterAll = document.createElement('tr')
    , trPricesSort = document.createElement('tr');

  table.style.width = '100%';

  btn.addEventListener('click', function () {
    clean();
    generateRandomNumber();
    generatePriceTd();
    sortPriceTD();
    middleTD();
    centerTD();
  });

  function generateRandomNumber() {
    prices = [];
    for (let i = 0; i < count; i++) {
      prices.push((Math.random() * 4000 | 0));
    }
  }

  function clean() {
    trPrices.innerHTML = '';
    trGradientCenter.innerHTML = '';
    trGradientMiddle.innerHTML = '';
    trPricesSort.innerHTML = '';
    trPricesCenter.innerHTML = '';
    trPricesMiddle.innerHTML = '';
    trPricesCenterAll.innerHTML = '';
    trPricesMiddleAll.innerHTML = '';
  }

  function generatePriceTd() {
    for (let i = 0; i < count; i++) {
      let td = document.createElement('td');
      td.innerHTML = prices[i];
      td.setAttribute('colspan', 3);
      trPrices.appendChild(td);
    }
  }

  function sortPriceTD() {
    let sortPrice = prices.sort((a, b) => a - b);

    for (let i = 0; i < count; i++) {
      let td = document.createElement('td');
      td.innerHTML = sortPrice[i];
      td.setAttribute('colspan', 3);
      trPricesSort.appendChild(td);
    }
  }

  function middleTD() {
    function createTD(inner, color) {
      let tempTD = document.createElement('td');
      tempTD.innerHTML = inner;
      tempTD.style.backgroundColor = color;
      tempTD.setAttribute('colspan', count);
      return tempTD;
    }

    let min = Math.min.apply(null, prices), max = Math.max.apply(null, prices);

    trPricesMiddle.appendChild(createTD(min, '#a5e3af'));
    trPricesMiddle.appendChild(createTD(((max + min) / 2 | 0), '#EFF9E8'));
    trPricesMiddle.appendChild(createTD(max, '#ffc2a3'));

    let td = document.createElement('td');
    td.setAttribute('colspan', count * 3);
    td.style.height = '10px';
    td.style.backgroundImage = `linear-gradient(to right, #a5e3af 0%, #EFF9E8 49.5%, black 49.5%, black 50%, black 50.5%, #EFF9E8 50.5%, #ffc2a3 100%)`;
    trGradientMiddle.appendChild(td);

    let gradient = [{
      range : 0,
      color : '#a5e3af'
    }, {
      range : .5,
      color : '#EFF9E8'
    }, {
      range : 1,
      color : "#ffc2a3"
    }];

    prices.sort((a, b) => a - b).forEach(function (price) {
      let td = document.createElement('td');
      td.innerHTML = price;
      td.setAttribute('colspan', 3);
      let color = Color.inGradient(gradient, (price - min) / (max - min));
      td.style.backgroundColor = `rgb(${color.r | 0},${color.g | 0},${color.b | 0})`;
      trPricesMiddleAll.appendChild(td);
    })
  }

  function centerTD() {

    function sa() {
      let len = arguments.length;
      if (len == 0)
        return "X3";
      let sum = 0;
      for (let i = 0; i < len; i++) {
        sum += +arguments[i];

      }
      return sum / len;
    }

    function createTD(inner, color) {
      let tempTD = document.createElement('td');
      tempTD.innerHTML = inner;
      tempTD.style.backgroundColor = color;
      tempTD.setAttribute('colspan', count);
      return tempTD;
    }

    let min = Math.min.apply(null, prices), max = Math.max.apply(null, prices);

    let center = (sa.apply(null, prices) | 0);
    trPricesCenter.appendChild(createTD(min, '#a5e3af'));
    trPricesCenter.appendChild(createTD(center, '#EFF9E8'));
    trPricesCenter.appendChild(createTD(max, '#ffc2a3'));

    let td = document.createElement('td');
    td.setAttribute('colspan', count * 3);
    td.style.height = '10px';
    let percent = (((center - min) / (max - min) * 100) | 0);
    let delta = .3;
    td.style.backgroundImage = `linear-gradient(to right, #a5e3af 0%, #EFF9E8 ${percent - delta}%, black ${percent - delta}%, black ${percent + delta}%, #EFF9E8 ${percent + delta}%, #ffc2a3 100%)`;
    trGradientCenter.appendChild(td);

    let gradient = [{
      range : 0,
      color : '#a5e3af'
    }, {
      range : (center - min) / (max - min),
      color : '#EFF9E8'
    }, {
      range : 1,
      color : "#ffc2a3"
    }];

    prices.sort((a, b) => a - b).forEach(function (price) {
      let td = document.createElement('td');
      td.innerHTML = price;
      td.setAttribute('colspan', 3);
      let color = Color.inGradient(gradient, (price - min) / (max - min));
      td.style.backgroundColor = `rgb(${color.r | 0},${color.g | 0},${color.b | 0})`;
      trPricesCenterAll.appendChild(td);
    })

  }

  table.appendChild(trPrices);
  table.appendChild(trPricesSort);
  table.appendChild(trGradientMiddle);
  table.appendChild(trPricesMiddle);
  table.appendChild(trPricesMiddleAll);
  table.appendChild(trGradientCenter);
  table.appendChild(trPricesCenter);
  table.appendChild(trPricesCenterAll);

  container.appendChild(table);
}());