const gulp = require('gulp')
  , liverelod = require('gulp-livereload');

const pug = require('./gulp/pug')
  , less = require('./gulp/less')
  , js = require('./gulp/js');

gulp.task('watch', gulp.parallel(function (done) {
  liverelod.listen();
}, pug.watch, less.watch, js.watch));

gulp.task('default', gulp.parallel(pug.build, less.build, js.build));