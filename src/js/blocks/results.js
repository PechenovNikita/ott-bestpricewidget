'use strict';

import {docCreate} from './../mixins/Element';
import {appendChildren} from './../mixins/Element';
import Loader from './../components/loader';
import Trip, {TripLoader, TripText} from './../components/trip';

import classList from '../mixins/polyfill/element/classList';

// import Eve from './../mixins/Event';

export default class Results {
  /**
   *
   * @param {HTMLElement} container
   * @param {Array<TripOptions>|null} [options=null]
   * @param {OttDealsQuery} [searchOptions]
   * @param {EventStore} eventStore
   */
  constructor(container, options = null, searchOptions = {}, eventStore) {
    /**
     *
     * @type {EventStore}
     * @private
     */
    this._eventStore = eventStore;

    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._container = container;
    /**
     *
     * @type {Array<TripOptions>|null}
     * @private
     */
    this._options = options;
    this._searchOptions = searchOptions;
    this.__initHTML().__initEvents();

    this._loading = true;
    // this._eventStore.on('Data::query:send', ::this.load);
    // this._eventStore.on('Data::query:response', ::this.__updateResults);

    // DataDeals.beforeQuerySend(::this.load);
    // DataDeals.afterQuerySend(::this.__updateResults);
  }

  /**
   *
   * @return {Results}
   */
  __initHTML() {
    this._node = docCreate('div', 'ottbp-results');
    this._list = docCreate('ul', 'ottbp-results__list');
    this._containerLoader = docCreate('div', 'ottbp-results__loader');
    this._containerAfterLoader = docCreate('div', 'ottbp-results__after-loader');
    this._containerText = docCreate('div', 'ottbp-results__text');

    this._loader = new Loader(this._containerLoader);
    this._loaderAfter = new TripLoader(this._containerAfterLoader);
    this._text = new TripText(this._containerText, this._eventStore);

    this._node.appendChild(this._containerLoader);
    this._node.appendChild(this._list);
    this._node.appendChild(this._containerText);
    this._node.appendChild(this._containerAfterLoader);
    this._container.appendChild(this._node);

    this.__initItemsHTML();

    return this;

  }

  __initEvents() {
    // this._eventStore.on(['Query::init', 'Query::update', 'Query::update:date'], ::this.__loading);
    // this._eventStore.on('Query::update:find', ::this.__updateResults);
    // this._eventStore.on('Results::load', ::this.__loading);
    this._eventStore.on('Controller::load:results', ::this.__updateResults_v2);
    this._eventStore.on('Controller::show:results', ::this.__updateResults_v2);
  }

  /**
   *
   * @return {Results}
   * @private
   */
  __initItemsHTML() {
    this._fragment = document.createDocumentFragment();

    if (this._options && this._options.length > 0) {
      let items = this._options.map(function (item) {
        let li = docCreate('li', 'ottbp-results__list__item');
        new Trip(li, item);
        return li;
      }.bind(this));
      appendChildren(this._fragment, items);
    } else if (this._options && this._options.length == 0) {
      let empty = docCreate('li', 'ottbp-results__list__item');
      empty.innerHTML = '<div class="ottbp-results__list__item--empty">По заданным параметрам поиска перелётов не найдено</div>';
      this._list.appendChild(empty);
    }

    this._list.appendChild(this._fragment);

    return this;
  }

  /**
   *
   * @param {Array<TripOptions>} [items=[]]
   * @private
   */
  __updateItems(items = []) {

    let cache = JSON.stringify(items);
    if (this._cache && this._cache == cache)
      return this;

    this._cache = cache;

    /**
     *
     * @type {DocumentFragment}
     * @private
     */
    let _fragment = document.createDocumentFragment();
    this._list.innerHTML = '';
    /**
     *
     * @type {Array.<HTMLElement>}
     */
    let liItems = items.map(function (item) {
      /**
       *
       * @type {HTMLElement}
       */
      let li = docCreate('li', 'ottbp-results__list__item');
      new Trip(li, item);
      return li;
    }.bind(this));

    appendChildren(_fragment, liItems);
    this._list.appendChild(_fragment);
    // this._eventStore.dispatch('Results::resize');

    return this;
  }

  __loading() {
    this._loading = true;
    classList(this._node).add('ottbp-loading');
    this._containerLoader.style.display = '';
    this._loader.show();
    // this._eventStore.dispatch('Results::resize');
  }

  __loaded() {
    this._loading = false;
    classList(this._node).remove('ottbp-loading');
    this._loader.hide(function () {
      this._containerLoader.setAttribute('style', 'display: none !important;');
    }.bind(this));
  }

  __updateResults_v2(/*{detail:(ResultsShow)}*/event) {
    this.update_v2(event.detail);
    // if(event.detail.loading)
    //   this.__loading();
    // else
    //   this.__loaded();
    // this._eventStore.dispatch('Results::resize');
  }

  /**
   *
   * @param {Array<TripOptions>|null} [options=null]
   * @return {Results}
   */
  update(options = null) {
    let cache = JSON.stringify(options);
    if (this._cache && this._cache == cache)
      return this;

    this._cache = cache;
    this._options = options;
    this._list.innerHTML = '';
    this.__initItemsHTML();
    return this;
  }

  /**
   *
   * @param {ResultsShow} [options={}]
   */
  update_v2(options = {}) {
    let loaderAfter = false, loader = false/*, results = false*/;

    if (options.loading) {
      loaderAfter = true;
      loader = true;
      if (options.results && options.results.length > 0) {
        loader = false;
        // results = true;
      } else {
        loaderAfter = false;
      }
    }/* else {
      if (options.results && options.results.length > 0) {
        results = true;
      }
    }*/

    if (loader) {
      this.__loading();
    } else {
      this.__loaded();

    }
    if (!loader && loaderAfter) {
      this._afterLoading = true;
      this._loaderAfter.show();
    } else {
      this._afterLoading = false;
      this._loaderAfter.hide();
    }

    this.update(options.results);

    this._text.test(options);

    this._eventStore.dispatch('Results::resize');
  }

  /**
   *
   * @return {boolean}
   */
  get $isLoading() {
    return this._loading;
  }

  /**
   *
   * @return {number}
   */
  get $height() {
    if (this.$isLoading) {
      return this._containerLoader.offsetHeight + 30;
    }
    return this._list.offsetHeight + this._containerText.offsetHeight + (this._afterLoading ? this._containerAfterLoader.offsetHeight : 0);
  }
}