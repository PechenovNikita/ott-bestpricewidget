/*eslint-env node*/
'use strict';

const fontsCSS = require('../css/fonts.css');
const indexCSS = require('../css/index.css');

(() => {
  if (!document.getElementById('ottbp-widget-styles')) {
    const style = document.createElement('style');
    style.setAttribute('id', 'ottbp-widget-styles');
    style.setAttribute('style', 'text/css');
    style.innerHTML = fontsCSS + indexCSS;

    const head = document.head || document.getElementsByTagName('head')[0];
    head.appendChild(style);
  }
})();

import {docCreate} from './mixins/Element';
import {deepExtend} from './mixins/Deep';
import {defaultFormData} from './config/_default';

import Params from './config/_params';
import Controller from './store/Controller';
import DemoController from './store/demo/Controller';
import Form from './blocks/form';
import Theme from './store/Theme_v2';

import EventStore from './mixins/Event_v2';

class Widget {
  /**
   *
   * @param {string} id
   * @param {{marker:string, type:string}} params
   * @param {ThemeOptions} [theme]
   * @param {FormatMainForm} [data]
   */
  constructor(id, params, theme = {}, data = {}) {
    this._container = document.getElementById(id);

    this._themeOptions = theme;
    this._data = deepExtend({}, defaultFormData, data);
    params.id = id;
    Params.init(params);

    this.__init();

  }

  __init() {
    this._node = docCreate('div', 'ottbp-widget');
    this._container.appendChild(this._node);
    /**
     *
     * @type {EventStore}
     * @private
     */
    this._eventStore = new EventStore();

    if (Params.get('demo')) {
      /**
       *
       * @type {Controller}
       * @private
       */
      this._store = new DemoController(this._eventStore);
    } else {
      this._store = new Controller(this._eventStore);
    }
    this._theme = new Theme();
    this._theme.init(this._themeOptions);
    /**
     *
     * @type {Form}
     * @private
     */
    this._form = new Form(this._node, this._eventStore, this._theme);

    this._form.update(this._data);

  }

  /**
   *
   * @param {string} markerId
   * @return {Widget}
   */
  marker(markerId) {
    Params.updateMarker(markerId);
    return this;
  }

  /**
   *
   * @param {ThemeOptions} theme
   * @return {Widget}
   */
  theme(theme) {

    if (theme.width || theme.width === 0) {
      theme.width = parseInt(theme.width, 10);
      if (theme.width < 320)
        theme.width = 320;
    }

    this._theme.replace(theme);
    this._form.resize();
    return this;
  }

  /**
   *
   * @param {ThemeColor} colors
   * @return {Widget}
   */
  colors(colors) {

    this.theme({color : colors});
    return this;
  }

  /**
   *
   * @param {number|null} width
   * @return {Widget}
   */
  width(width) {
    this.theme({width : width});
    return this;
  }

  /**
   *
   * @param {FormatMainForm} data
   * @return {Widget}
   */
  update(data) {
    this._data = deepExtend({}, this._data, data);
    this._form.update(this._data);
    return this;
  }
}

module.exports = Widget;