/**
 * @typedef {Object}    ThemeColor
 * @property {string}     [WidgetBG = #878D96]
 * @property {string}     [WidgetTX = #444444]
 * @property {string}     [WidgetBtn  = #FFD41E]
 * @property {string}     [WidgetBtnTX = #616161]
 * @property {Object}     [WidgetPriceGradient]
 * @property {string}       [WidgetPriceGradient.low  = #a5e3af]
 * @property {string}       [WidgetPriceGradient.middle = #EFF9E8]
 * @property {string}       [WidgetPriceGradient.high = #ffc2a3]
 */

/**
 * @typedef {Object}                ThemeOptions
 * @property {number|string|null}     [width]
 * @property {ThemeColor}             [color]
 */


/**
 * @typedef {Object} DropDownSelectOptions
 * @property {string} title
 * @property {boolean} [orange]
 * @property {string} value
 */



/**
 * @typedef {Object} CalendarOptions
 * @property {boolean|undefined} [isRange]
 * @property {Date} [minDate]
 * @property {Date} [maxDate]
 * @property {Date} [dateStart]
 * @property {Date} [dateEnd]
 * @property {number} [duration]
 * @property {Object} [query]
 */

/**
 * @typedef {Object} CalendarValue
 * @property {?Date} select
 * @property {Object} range
 * @property {?Date} range.start
 * @property {?Date} range.end
 * @property {?Date} hover
 * @property {Object} rangeHover
 * @property {?Date} rangeHover.start
 * @property {?Date} rangeHover.end
 */


/**
 * @typedef {Object} MonthsOptions
 * @property {number} month
 * @property {number} year
 * @property {{r:number,g:number,b:number}|null} [color]
 * @property {string} [price]
 * @property {Price} [prices]
 * @property {Date} [date]
 */


/**
 * @typedef {Object} CalendarDayOption
 * @property {boolean} [disabled]
 * @property {string} [price]
 *
 * @property {boolean} [hover]
 * @property {boolean} [hoverAfter]
 * @property {boolean} [hoverBefore]
 *
 * @property {boolean} [selected]
 * @property {boolean} [selectedAfter]
 * @property {boolean} [selectedBefore]
 */


/**
 * @typedef {Object} ottFindItem
 * @property {string} [city]
 * @property {string} [country]
 * @property {boolean} [isCountry]
 * @property {string} [label]
 * @property {string} value
 */

/**
 * Формат данных из формы Откуда/Куда
 * @typedef {Object}        FormatMainForm_Inputs
 * @property {null|string}    [from]
 * @property {null|string}    [to]
 * @property {null|string}    [toCountry]
 */

/**
 * Формат данных из формы Селекторов ()
 * @typedef {Object}          FormatMainForm_Selects
 * @property {string|number}    [duration]
 * @property {boolean}          [route]
 * @property {string|number}    [transfer]
 * @property {string}           [type]
 */

/**
 * Формат данных из формы выбора даты ()
 * @typedef {Object}                            FormatMainForm_Date
 * @property {null|Date}                          [start]
 * @property {null|Date}                          [end]
 * @property {null|{year:number,month:number}}    [inMonth]
 */


/**
 * Формат данных из формы
 * @typedef {Object}                            FormatMainForm
 * @property {FormatMainForm_Inputs}              [inputs]
 * @property {FormatMainForm_Selects}             [selectors]
 * @property {FormatMainForm_Date}                [date]
 */


/**
 * Формат данных города/страны/аиропорта
 * @typedef {Object}                                  PlaceData
 * @property {string}                                   iata
 * @property {"city"|"country"|"airport"}               type
 * @property {{iata:string, ru:string, [en]:string}}    [airport]
 * @property {{iata:string, ru:string, [en]:string}}    [city]
 * @property {{iata:string, ru:string, [en]:string}}    [country]
 */


/**
 * @typedef {Object}        OttDealsQuery
 * @property {boolean}        [add_locale_top] : true
 * @property {number}         [deals_limit] : <999
 *
 * @property {string}         [departure_date_from] : 2017-01-17 - --//--
 * @property {string}         [departure_date_to] : 2017-01-31  - Интервал дат вылета, для которых искать самые дешевые перелеты. Если ничего не выбрано, то даты не ограничены. Если выбрано, то поиск только в рамках этих дат
 * @property {string}         [departure_days_of_week] - Дни недели вылета. Можно искать по дням недели, например хочу вылететь в пятницу и субботу "fri,sat". Вылететь в рабочие дни "mon,tue,wed,thu,fri"

 * @property {string}         [destinations] - Города прилёта. Если не выбрано ничего, то осуществляется поиск самых дешевых направлений. Можно вводить один или несколько городов назначения. Например "MAD,BAR,LON"
 * @property {string}         [destination_countries] - Страны прилёта. Можно вводить одну или несколько стран назначения. Например "RU,ES,DE"
 * @property {boolean}        [direct_flights] - Искать только прямые. Если не отмечено, то поиск идёт по всем вариантам

 * @property {boolean}        [group_by_date] - Искать лучшее предложение за каждый день в интервале заданном в departure. Пример: если выбрать в departure целый месяц, в destinations один город, и поставить этот флаг, то будет 30 дилзов с ценой на каждый день. (Календарь цен)
 * @property {boolean}        [group_by_country] - Искать лучшее предложение сгрупированное по странам
 * @property {string}         [locale] - Указывается страна, для которой надо найти топ популярных направлений, и добавить эти направления в поле destinations. Примеры значений: "ru", "us", "es", "de"
 * @property {string}          origin - Город вылета. Может быть только 1 код города.
 * @property {string}         [return_date_from] : 2016-12-08 -
 * @property {string}         [return_date_to] : 2016-12-23 - Интервал дат прилета обратно для roundtrip. Если указано, то поиск по самым дешевым предложениям будет учитывать дату возвращения.
 * @property {string}         [return_days_of_week] - Дни вылета обратно для roundtrip
 * @property {boolean}        [roundtrip_flights] - Искать roundtrip или oneway
 * @property {string}         [source] - Канал для ценообразования. Если не заполнено выводятся tarif+tax - ускоренная выдача, для массовых запросов, где цена нужна примерно. Если ввести "aviasales" - после получения дилзов будет применено ценообразование канала авиасейлз. Для получения директ канала используйте значение "12trip"

 * @property {int}            [stay_from] - --//--
 * @property {int}            [stay_to] - Интервал длительности пребываения для roundtrip. Пример "1-4", "7-10", "5-20". Если значнение не указано, параметр не учитвается

 * @property {string}         [timelimit] : 43200
 * нет в оригинале
 * @property {string}         [transfer] - вспомогательное
 */