'use strict';

import {docCreate, classAddRemove} from './../../../mixins/Element';

const classes = {
  disabled : 'ottbp-btn-day--disabled',
  selected : 'ottbp-btn-day--selected',
  hover    : 'ottbp-btn-day--hover',

  selectedAfter  : 'ottbp-btn-day--selected--after',
  selectedBefore : 'ottbp-btn-day--selected--before',

  hoverAfter  : 'ottbp-btn-day--hover--after',
  hoverBefore : 'ottbp-btn-day--hover--before'
};

export default class CalendarDay {
  /**
   *
   * @param {Date} date
   * @param {string|number} id
   * @param {CalendarDayOption} [options={}]
   */
  constructor(date, id, options = {}) {
    /**
     *
     * @type {Date}
     * @private
     */
    this._date = date;
    /**
     *
     * @type {CalendarDayOption}
     * @private
     */
    this._options = options;
    /**
     *
     * @type {*}
     * @private
     */
    this._id = id;

    this.__initBtn();
  }

  /**
   *
   * @return {CalendarDay}
   * @private
   */
  __initBtn() {
    this._btn = document.createElement('div');
    this._btn.setAttribute('data-id', this._id);

    return this.__initClasses().__initInner();
  }

  /**
   *
   * @return {CalendarDay}
   * @private
   */
  __initClasses() {

    let addClasses = ['ottbp-btn-day'], removeClasses = [];

    Object.keys(classes).forEach((prop) => {
      if (this._options[prop])
        addClasses.push(classes[prop]);
      else
        removeClasses.push(classes[prop]);
    }, this);

    classAddRemove(this._btn, addClasses, removeClasses);

    return this;
  }

  /**
   *
   * @return {CalendarDay}
   * @private
   */
  __initInner() {

    this._btn.innerHTML = '<div class="ottbp-btn-day__back ottbp-btn-day__t"></div><div class="ottbp-btn-day__hover ottbp-btn-day__t"></div>';

    this._cachePriceClassName = `ottbp-btn-day__price  ottbp-btn-day__t ${this._options.price ? `ottbp--${this._options.price}_price` : 'ottbp--none_price'}`;

    this._priceNode = docCreate('div', this._cachePriceClassName);
    let dayNode = docCreate('div', ['ottbp-btn-day__inner', 'ottbp-btn-day__t']);
    dayNode.innerHTML = this._date.getDate();

    this._btn.appendChild(this._priceNode);
    this._btn.appendChild(dayNode);
    return this;
  }

  /**
   *
   * @return {*}
   * @private
   */
  __initPriceClass() {

    let className = `ottbp-btn-day__price ottbp-btn-day__t ${this._options.price ? `ottbp--${this._options.price}_price` : 'ottbp--none_price'}`;
    if (this._cachePriceClassName == className)
      return this.__initPriceColor();

    this._cachePriceClassName = className;
    this._priceNode.className = className;
    return this.__initPriceColor();
  }

  /**
   *
   * @return {CalendarDay}
   * @private
   */
  __initPriceColor() {

    let backStyle = '';

    if (this._options && this._options.color) {
      backStyle = `background-color: rgb(${this._options.color.r | 0},${this._options.color.g | 0},${this._options.color.b | 0}) !important;`;
    }
    if (this._cacheBackStyle == backStyle)
      return this;
    this._cacheBackStyle = backStyle;
    this._priceNode.setAttribute('style', backStyle);
    return this;
  }

  /**
   *
   * @return {Date}
   */
  get $date() {
    return this._date;
  }

  /**
   *
   * @return {Element}
   */
  get $node() {
    return this._btn;
  }

  set price(price) {
    if (!this._options)
      this._options = {};
    this._options.price = price;
    this.__initPriceClass();
  }

  set delta(delta) {
    if (!this._options)
      this._options = {};
    this._options.delta = delta;
    this._options.price = 'custom';
    this.__initPriceClass();
  }

  /**
   *
   * @param {{r:number,g:number,b:number}} color
   */
  set color(color) {
    if (!this._options)
      this._options = {};
    this._options.color = color;
    this._options.price = 'custom';
    this.__initPriceClass();

  }

  /**
   *
   * @param {boolean||undefined} [before]
   * @param {boolean||undefined} [after]
   * @return {CalendarDay}
   */
  select(before, after) {
    if (!this._options)
      this._options = {};
    this._options.selected = true;
    this._options.selectedAfter = !!after;
    this._options.selectedBefore = !!before;
    return this.__initClasses();
  }

  /**
   *
   * @return {CalendarDay}
   */
  unSelect() {
    if (!this._options)
      this._options = {};
    this._options.selected = false;
    this._options.selectedAfter = false;
    this._options.selectedBefore = false;
    return this.__initClasses();
  }

  // get $id() {
  //   return this._id;
  // }

  // /**
  //  *
  //  * @return {boolean}
  //  */
  // isDisabled() {
  //   return this._options ? this._options.disabled : false;
  // }

  // /**
  //  *
  //  * @return {boolean}
  //  */
  // isSelected() {
  //   return this._options ? this._options.selected : false;
  // }

  /**
   *
   * @param {boolean||undefined} [before]
   * @param {boolean||undefined} [after]
   * @return {CalendarDay}
   */
  hover(before, after) {
    if (!this._options)
      this._options = {};
    this._options.hover = true;
    this._options.hoverBefore = !!before;
    this._options.hoverAfter = !!after;
    return this.__initClasses();
  }

  unHover() {
    if (!this._options)
      this._options = {};
    this._options.hover = false;
    this._options.hoverAfter = false;
    this._options.hoverBefore = false;
    return this.__initClasses();
  }

}