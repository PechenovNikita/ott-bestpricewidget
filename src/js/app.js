'use strict';

let Widget = null;

import {bindReady} from './mixins/Element';

function parentScript() {
  for (var t = /ott-widget\.js/, script = document.getElementsByTagName("script"), i = [],
         n = 0; n < script.length; n++)
    script[n].src.match(t) && i.push(script[n]);
  return i;
}

bindReady(function () {

  let currentPlace = parentScript()
    , parent = currentPlace[0].parentNode;
  let div = document.createElement('div');
  div.setAttribute('id', 'test-9');
  parent.insertBefore(div, currentPlace[0]);

  window.NEWWidget = new OTTBPWidget('test-9', {
    marker : '1-1884-0-2',
    demo   : false
  }, {}, {
    inputs    : {
      from : "MOW",
      // to   : "NYC"
    },
    selectors : {
      route    : true,
      transfer : 1
    },
    // date      : {
    //   start   : new Date(2017, 3, 27),
    //   inMonth : {
    //     year  : 2017,
    //     month : 3
    //   },
    //   end     : new Date(2017, 4, 2)
    // }
  });

});

import './tests';


