'use strict';

export function bindReady(callback) {
  if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', function FuncDOMContentLoaded() {
      callback();
      document.removeEventListener('DOMContentLoaded', FuncDOMContentLoaded, false);
    }, false);
  } else if (document.attachEvent) {
    document.attachEvent('onreadystatechange', function Funconreadystatechange() {
      if (document.readyState === 'complete') {
        callback();
        document.detachEvent('onreadystatechange', Funconreadystatechange);
      }

    });
  }
}

/**
 *
 * @param {string} tagName
 * @param {string[]|string} [classes]
 * @return {HTMLElement}
 */
export function docCreate(tagName, classes) {
  /**
   *
   * @type {HTMLElement}
   */
  let tempNode = document.createElement(tagName);
  if (Array.isArray(classes))
    tempNode.className = classes.join(' ');
  else if (typeof classes == 'string')
    tempNode.className = classes;
  return tempNode;
}
/**
 *
 * @param {HTMLElement} element
 * @param {string|Object.<string,string>} styles
 * @param {boolean} [important=true]
 */
export function setStyle(element, styles, important = true) {

  if (!element)
    return;
  if (typeof styles === 'string') {
    element.setAttribute('style', `${styles} ${important ? '!important' : ''};`);
  } else if (styles) {
    let style = '';
    Object.keys(styles).forEach(function (styleName) {
      style += `${styleName}: ${styles[styleName]} ${important ? '!important' : ''};`;
    });
    element.setAttribute('style', style);
  } else {
    element.setAttribute('style', '');
  }
}
/**
 *
 * @param {HTMLElement|DocumentFragment} container
 * @param {HTMLElement[]} children
 * @return {HTMLElement|DocumentFragment}
 */
export function appendChildren(container, children) {
  children.forEach(function (child) {
    container.appendChild(child);
  });
  return container;
}

/**
 *
 * Class Add
 *
 * @param {HTMLElement} node
 * @param {Array.<string>} addClasses
 * @param {Array.<string>} [removeClasses=[]]
 */
export function classAddRemove(node, addClasses, removeClasses = []) {
  let className = node.className
    , oldClassName = className;

  removeClasses.forEach(function (cl) {
    let reg = new RegExp(`${cl}`, 'gm');
    className = className.replace(reg, '');
  });

  addClasses.forEach(function (cl) {
    if (className.indexOf(cl) < 0)
      className += ` ${cl}`;
  });

  className = className.trim().replace(/[\s]{2,}/g, ' ');

  if (className == oldClassName)
    return;

  node.className = className.replace(/[\s]{2,}/g, ' ');
}

//Returns true if it is a DOM node
export function isNode(o) {
  return (
    typeof Node === 'object' ? o instanceof Node :
    o && typeof o === 'object' && typeof o.nodeType === 'number' && typeof o.nodeName === 'string'
  );
}

//Returns true if it is a DOM element
export function isElement(o) {
  return (
    typeof HTMLElement === 'object' ? o instanceof HTMLElement : //DOM2
    o && typeof o === 'object' && o !== null && o.nodeType === 1 && typeof o.nodeName === 'string'
  );
}

