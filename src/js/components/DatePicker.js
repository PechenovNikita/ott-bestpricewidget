'use strict';

import {docCreate} from './../mixins/Element';

import {currentDate} from './../mixins/Date';

import Title from './datePicker/Title';

import Calendar from './datePicker/Calendar';

import MonthList from './datePicker/MonthsList';

export default class DatePicker {
  /**
   *
   * @param {HTMLElement} container
   * @param {EventStore} eventStore
   * @param {Theme} theme
   */
  constructor(container, eventStore, theme) {
    this._eventStore = eventStore;
    this._theme = theme;
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._container = container;

    this.__initHTML().__initInner().__initEvents();
    this._container.appendChild(this._node);
    /**
     *
     * @type {?CalendarValue}
     * @private
     */
    this._date = null;

  }

  /**
   *
   * @return {DatePicker}
   * @private
   */
  __initHTML() {
    this._node = docCreate('div', 'ottbp-date');

    this.__initHeaderHTML().__initBodyHTML();

    this._node.appendChild(this._header);
    this._node.appendChild(this._body);

    this.__initHeaderInnerHTML();

    return this;
    // return this.initMonthList().showMonth();
  }

  /**
   *
   * @return {DatePicker}
   * @private
   */
  __initHeaderHTML() {
    this._header = docCreate('header', 'ottbp-date__header');
    this._title = new Title(this._header, this._eventStore);
    return this;
  }

  /**
   *
   * @return {DatePicker}
   * @private
   */
  __initHeaderInnerHTML() {
    this._title.update(this._date, this._inMonth, this._isRange);
    return this;
  }

  /**
   *
   * @return {DatePicker}
   * @private
   */
  __initBodyHTML() {
    this._body = docCreate('section', 'ottbp-date__body');
    let inner = docCreate('div', 'ottbp-date__body__inner');

    this._containerLoader = docCreate('div', 'ottbp-date__loader');
    this._containerCalendar = docCreate('div', 'ottbp-date__calendar');
    this._containerMonth = docCreate('div', 'ottbp-date__months');

    // this._loader = new Loader(this._containerLoader);

    // inner.appendChild(this._containerLoader);
    inner.appendChild(this._containerCalendar);
    inner.appendChild(this._containerMonth);

    // new CalendarV2(inner);

    this._body.appendChild(inner);
    return this;
  }

  /**
   *
   * @return {DatePicker}
   * @private
   */
  __initInner() {
    this.__initCalendar().__initMonthList();
    // скрытваем календарь
    this._containerMonth.style.display = '';
    this._containerCalendar.setAttribute('style', 'display:none !important;');
    return this;
  }

  /**
   *
   * @return {DatePicker}
   * @private
   */
  __initCalendar() {
    this._calendar = new Calendar(
      currentDate.getMonth(),
      currentDate.getFullYear(),
      this._containerCalendar,
      {
        isRange  : this._isRange,
        duration : this._duration
      },
      this._eventStore, this._theme);

    return this;
  }

  /**
   *
   * @return {DatePicker}
   * @private
   */
  __initMonthList() {

    /**
     *
     * @type {MonthsList}
     * @private
     */
    this._monthList = new MonthList(this._containerMonth, this._eventStore, this._theme);

    return this;
  }

  /**
   *
   * @return {DatePicker}
   * @private
   */
  __initEvents() {

    this._calendar.whenSelect(::this.__onDateSelect);
    this._calendar.whenHover(::this.__onDateHover);
    this._calendar.whenMonthClick(::this.__onMonthBack);

    this._monthList.whenSelect(::this.__onMonthSelect);

    // this._title.whenClean(::this.__onRemoveSelected);
    this._eventStore.on('DatePicker::do:clean', ::this.__onRemoveSelected);

    this._eventStore.on([/*`Form::init`,*/ 'Selects::change'], function (event) {
      let value = event.detail.value;
      this.update({
        isRange  : value.route,
        duration : (value.duration !== 'any' ? value.duration : null)
      });

    }.bind(this), 1);

    this._eventStore.on(['Form::init'], function (event) {
      let data = event.detail;
      this.update({
        isRange  : data.selectors.route,
        duration : (data.selectors.duration !== 'any' ? data.selectors.duration : null)
      });
    }.bind(this));

    return this;

  }

  // EVENTS

  /***
   *
   * @param {MonthsOptions} item
   * @private
   */
  __onMonthSelect(item) {
    // this.initCalendar(item.month + 1, item.year).showCalendar();
    // this._eventStore.dispatch('DateSelect::select:month');

    this._inMonth = {
      year  : item.year,
      month : item.month
    };
    this._eventStore.dispatch('DatePicker::changed', {date : this.$value});
    this.__updateCalendar().__showCalendar();

  }

  /**
   *
   * @param {CalendarValue} dateData
   * @private
   */
  __onDateHover(dateData) {
    this._date = dateData;
    this.__initHeaderInnerHTML();
  }

  /**
   *
   * @private
   */
  __onMonthBack() {
    this._inMonth = null;
    this._eventStore.dispatch('DatePicker::changed', {date : this.$value});
    this.__initHeaderInnerHTML().__showMonth();
  }

  /**
   *
   * @param {CalendarValue} dateData
   * @private
   */
  __onDateSelect(dateData) {
    let same = JSON.stringify(dateData) == JSON.stringify(this._date);
    this._date = dateData;

    if (!same) {
      this._eventStore.dispatch('DatePicker::changed', {date : this.$value});
      this.__initHeaderInnerHTML();
    }
  }

  /**
   *
   * @private
   */
  __onRemoveSelected() {
    this._calendar.clear();
    this._date = null;

    this._eventStore.dispatch('DatePicker::changed', {date : this.$value});
  }

  // some updates

  /**
   *
   * @param {{[isRange]:?boolean, [duration]:boolean|null, [string]:*}} [options]
   * @return {DatePicker}
   * @private
   */
  __parseOptions(options = {}) {
    /**
     *
     * @type {boolean}
     * @private
     */
    this._isRange = !!(options.isRange);
    /**
     * @type {string|number|null}
     */
    this._duration = (this._isRange ? (options.duration || null) : null);

    return this;
  }

  /**
   *
   * @param {{[isRange]:boolean, [duration]:boolean|null, [string]:*}} options
   * @return {DatePicker}
   */
  update(options) {
    this.__parseOptions(options).__updateCalendar().__initHeaderInnerHTML();
    return this;
  }

  // Calendar
  /**
   *
   * @param {number} [month]
   * @param {number} [year]
   * @return {DatePicker}
   * @private
   */
  __updateCalendar(month, year) {

    if (this._inMonth) {
      if (typeof month === 'undefined') {
        month = this._inMonth.month;
      }
      if (typeof year === 'undefined') {
        year = this._inMonth.year;
      }
    } else {
      if (typeof month === 'undefined') {
        month = currentDate.getMonth() + 1;
      }
      if (typeof year === 'undefined') {
        year = currentDate.getFullYear();
      }
    }

    this._calendar.update(month, year, {
      isRange  : this._isRange,
      duration : this._duration

    });

    return this;
  }

  // DOM
  /**
   *
   * @return {number}
   */
  get $height() {
    return (!this._loading) ? this._node.offsetHeight : this._containerLoader.offsetHeight + 30;
  }

  /**
   *
   * @return {DatePicker}
   */
  __showMonth() {
    this._containerMonth.style.display = '';
    this._containerCalendar.setAttribute('style', 'display:none !important;');
    this._eventStore.dispatch('DatePicker::resize', {object : this});
    return this;
  }

  /**
   *
   * @return {DatePicker}
   */
  __showCalendar() {
    this._containerMonth.setAttribute('style', 'display:none !important;');
    this._containerCalendar.style.display = '';
    this._eventStore.dispatch('DatePicker::resize', {object : this});
    return this;
  }

  /**
   *
   * @return {boolean}
   */
  get $isLoading() {
    return !(this._monthList.$isInit /*&& !this._monthList.isLoading*/);
  }

  get $empty() {
    /**
     *
     * @type {{start: Date, inMonth: (*), end: Date}}
     */
    let value = this.$value;
    return !(value.start && value.end);
  }

  // Value
  /**
   *
   * @return {{start: ?Date, inMonth: ({year: number, month: number}|false), end: ?Date}}
   */
  get $value() {
    /**
     *
     * @type {CalendarValue}
     */
    let val = this._calendar.$value;

    /**
     * @type {{year:number,month:number}|false}
     */
    let inMonth = (this._inMonth ? this._inMonth : false);
    /**
     *
     * @type {{start: (?Date), inMonth: ({year:number,month:number}|false), end: (?Date)}}
     */
    let value = {
      start   : val.select,
      inMonth : inMonth,
      end     : null
    };

    if (this._isRange)
      value = {
        start   : val.range.start,
        inMonth : inMonth,
        end     : val.range.end,
      };

    this._date = val;

    return value;
  }

  /**
   *
   * @param {FormatMainForm} options
   */
  updateValue(options) {

    /**
     *
     * @type {CalendarOptions}
     */
    let calendarOptions = {
      isRange   : options.selectors.route,
      dateStart : options.date.start,
      dateEnd   : options.date.end,
      duration  : options.selectors.duration
    };

    let month = options.date.inMonth;

    if (month) {
      this.__onMonthSelect(month);
      this._calendar.update(month.month, month.year, calendarOptions);
    }

  }
}
