'use strict';
import {docCreate} from '../mixins/Element';
import SearchButton from '../components/SearchButton';

export default class Buttons{
  constructor(container){
    this._container = container;
    this.__initHTML();
    this._container.appendChild(this._node);
  }

  /**
   *
   * @return {Buttons}
   * @private
   */
  __initHTML(){
    this._node = docCreate('div','ottbp-buttons');
    this.__initBtn();
    return this;
  }

  /**
   *
   * @private
   */
  __initBtn(){
    this._btn = new SearchButton(this._node);
  }
}