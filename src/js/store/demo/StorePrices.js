/*eslint no-unused-vars: ["error", { "args": "none" }]*/
'use strict';

import StorePrices from '../store/Prices';
import {DEALS as DEMO_DEALS} from '../../config/_demo';

export default class DemoStorePrices extends StorePrices {
  constructor() {
    super(null);
  }

  /**
   *
   * @return {[(number|null),(number|null),(number|null)]}
   */
  get $minRange() {
    return DEMO_DEALS.filter((data, index) => index < 3).map((data) => data.price);
  }

  /**
   *
   * @return {number}
   */
  get $delta() {
    return Math.random();
  }

  /**
   *
   * @return {Array.<string>}
   */
  get $keys() {
    return [];
  }

  /**
   *
   * @param {string} key
   * @param {number} price
   * @param {...string|...number} indexes
   * @return {StorePrices}
   */
  addKey() {
    return this;
  }

  /**
   *
   * @param indexes
   * @return {DemoStorePrices}
   */
  getBy(...indexes) {
    return this;
  }

  /**
   *
   * @param indexes
   * @return {DemoStorePrices}
   */
  getByOrThis(...indexes) {
    return this;
  }
}
