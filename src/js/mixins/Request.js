'use strict';

import JSONP from './request/JSONP';
import Ajax from './request/Ajax';
/**
 *
 * @param   {string}          url
 * @param   {RequestOptions}  options
 * @return  {number|string}
 */
export function jsonp(url, options) {
  let request = new JSONP(url, options);
  request.send();
  return request.$id;
}

/**
 *
 * @param   {string}          url
 * @param   {RequestOptions}  options
 * @return  {number|string}
 */
export function ajax(url, options) {
  let request = new Ajax(url, options);
  request.send();
  return request.$id;
}
