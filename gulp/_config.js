let path = '/';

let routes = {
  dest   : './dest/',
  src    : './src/',
  js     : 'js/',
  css    : 'css/',
  images : 'images/'
};

module.exports = {
  routes : routes,
  path   : {
    main          : path,
    css           : path + routes.css,
    js            : path + routes.js,
    images        : path + routes.images,
    imagesForLess : '/dest/images/'
  }
};