'use strict';
import {closest} from '../polyfill/Element';

import {isElement, isNode} from '../Element';

/**
 *
 * @param {HTMLElement} target
 * @param {HTMLElement|Node|null} parent
 * @param {string} DOMString
 * @return {boolean|HTMLElement}
 * @private
 */
function __isDelegateDOMString(target, parent, DOMString) {
  let el = closest(target, DOMString);

  if (!el || (parent && !parent.contains(el))) {
    return false;
  }

  return el;
}
/**
 *
 * @param {HTMLElement} target
 * @param {HTMLElement|Node|null} parent
 * @param {HTMLElement} element
 * @return {boolean|HTMLElement}
 * @private
 */
function __isDelegateElement(target, parent, element) {
  while (target != parent) {

    if (parent && !parent.contains(target)) {
      return false;
    }

    if (target === element) {
      return target;
    }


    target = target.parentNode;
  }
  return false;
}
/**
 *
 * @param {Event}                         event
 * @param {string|null}                   selector
 * @param {HTMLElement|Node|null}         parent
 * @param {function(target:HTMLElement)}  callback
 * @param {function(event:Event)}         [notCallback]
 */
export default function delegate(event, selector, parent, callback, notCallback) {
  /**
   *
   * @type {EventTarget|HTMLElement}
   */
  let target = event.target;

  /**
   *
   * @type {boolean|HTMLElement}
   */
  let doCallback = false;

  if (typeof selector === 'string') {
    doCallback = __isDelegateDOMString(target, parent, selector);
  } else if (isElement(selector) || isNode(selector)) {
    doCallback = __isDelegateElement(target, parent, selector);
  }
  if (doCallback)
    callback(doCallback);
  else if (notCallback)
    notCallback(event);
}
