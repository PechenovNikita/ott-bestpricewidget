'use strict';
/**
 *
 * @param {string} part
 * @param {Object.<string,string>} obj
 * @return {Array.<*>}
 */
export default function findKeys(part, obj) {
  return Object.keys(obj).filter(function (/*string*/item) {
    return obj[item].toLowerCase().indexOf(part.toLowerCase()) > -1;
  });
}