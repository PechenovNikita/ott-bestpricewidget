'use strict';

function compareString(strA, strB) {
  let icmp = strA.toLowerCase().localeCompare(strB.toLowerCase());
  if (icmp != 0) {
    // spotted a difference when considering the locale
    return icmp;
  }
  // no difference found when considering locale, let's see whether
  // capitalization matters
  if (strA > strB) {
    return 1;
  } else if (strA < strB) {
    return -1;
  } else {
    // the characters are equal.
    return 0;
  }
}

function compareIndexOf(indexA, indexB, a, b) {
  if (indexA < 0)
    return 1;
  else if (indexB < 0)
    return -1;

  if (indexA > indexB) {
    return 1;
  } else if (indexA < indexB) {
    return -1;
  }

  return compareString(a, b);
}

export function comparePlaceItem(aString, bString, value, valueLow) {

  let aLabelIndex = aString.indexOf(value)
    , bLabelIndex = bString.indexOf(value)
    , aLabelLowIndex = aString.toLowerCase().indexOf(valueLow)
    , bLabelLowIndex = bString.toLowerCase().indexOf(valueLow);

  if (aLabelIndex > -1 || bLabelIndex > -1) {
    return compareIndexOf(aLabelIndex, bLabelIndex, aString, bString);
  }

  if (aLabelLowIndex > -1 || bLabelLowIndex > -1) {
    return compareIndexOf(aLabelLowIndex, bLabelLowIndex, aString, bString);
  }

  return compareString(aString, bString);
}

