'use strict';

import Loader from '../components/loader';

import PickerContainer from './footer/PickerContainer';
import ResultsContainer from './footer/ResultsContainer';

import {CONTAINER_SIZE_MEDIUM} from '../config/_constant';

// import Results from '../blocks/results';

import {docCreate} from '../mixins/Element';

// import Eve from '../mixins/Event';

export default class Footer {
  /**
   *
   * @param container
   * @param {EventStore} eventStore
   * @param {Theme} theme
   */
  constructor(container, eventStore, theme) {
    this._eventStore = eventStore;
    this._theme = theme;
    this._container = container;
    this._visible = false;

    this.__initHTML();

  }

  __initHTML() {
    this._node = docCreate('div', 'ottbp-footer');

    this._containerLoader = docCreate('div', 'ottbp-footer__loader');
    this._containerDatePicker = docCreate('div', 'ottbp-footer__date');
    this._containerResults = docCreate('div', 'ottbp-footer__results');

    this.__initItems().__initEvents();

    this._node.appendChild(this._containerLoader);
    this._node.appendChild(this._containerDatePicker);
    this._node.appendChild(this._containerResults);

    this.render();

    this._container.appendChild(this._node);
  }

  /**
   *
   * @return {Footer}
   * @private
   */
  __initItems() {
    this._loader = new Loader(this._containerLoader);
    this._picker = new PickerContainer(this._containerDatePicker, this._eventStore, this._theme);
    this._results = new ResultsContainer(this._containerResults, this._eventStore);

    return this;
  }

  /**
   *
   * @return {Footer}
   * @private
   */
  __initEvents() {
    this._eventStore.on(['Controller::load:results', 'Controller::show:results', 'DatePicker::resize', 'Results::resize'], function (event) {
      let object = event && event.detail ? event.detail.object : {};

      if (this._visible || !object || !object.$empty) {
        this._visible = true;
      }
      this.render();
    }.bind(this));

    return this;
  }

  /**
   *
   * @return {boolean}
   */
  get $isVisible() {
    return this._visible;
  }

  show() {
    this.render();
  }

  /**
   *
   * @return {boolean}
   */
  get $isLoading() {
    return (this._results.$isLoading && this._picker.$isLoading);
  }

  render() {

    let height = 0;
    let resultsWidth = (this._medium ? `width: ${this._medium - 400}px !important;` : '');
    if (this.$isVisible) {
      if (this.$isLoading) {

        height = this._containerLoader.offsetHeight;

        if (this._medium) {
          this._containerDatePicker.setAttribute('style', 'display: none !important;');
          this._containerLoader.setAttribute('style', '');
        } else {
          this._containerDatePicker.setAttribute('style', '');
          height += this._picker.$height;
          this._containerLoader.setAttribute('style', `margin-top: ${this._picker.$height}px !important`);
        }

        this._containerResults.setAttribute('style', `display: none !important; ${resultsWidth}`);
        this._loader.show();

      } else {
        this._containerDatePicker.setAttribute('style', '');
        this._containerResults.setAttribute('style', resultsWidth);

        if (this._medium) {
          height = Math.max(/*this._date.height +*/ this._picker.$height, this._results.$height);
        } else {
          height = this._results.$height + this._picker.$height;
        }

        this._loader.hide(() => {
          /**
           * @this {Footer}
           */
          this._containerLoader.setAttribute('style', 'display: none !important');
        });
      }
    } else {
      if (this._medium)
        this._containerDatePicker.setAttribute('style', 'display: none !important;');
      else
        this._containerDatePicker.setAttribute('style', '');

      this._containerResults.setAttribute('style', `display: none !important; ${resultsWidth}`);

      this._loader.hide(() => {
        /**
         * @this {Footer}
         */
        this._containerLoader.setAttribute('style', 'display: none !important');
      });
    }

    this._node.setAttribute('style', `height: ${height}px !important;`);
  }

  resize(containerWidth) {
    if (containerWidth >= CONTAINER_SIZE_MEDIUM) {
      this._medium = containerWidth;
    } else {
      this._medium = false;
    }

    this._picker.resize(containerWidth);
    this.render();
  }

  /**
   *
   * @return {FormatMainForm}
   */
  get $value() {
    return {
      date : this._picker.$value
    };
  }

  /**
   *
   * @param {FormatMainForm} options
   */
  update(options) {
    this._picker.update(options);
  }

}