'use strict';

import {docCreate} from '../../mixins/Element';
import {appendChildren} from '../../mixins/Element';

import l8n from './../../store/l8n';

import {generateMonthList} from '../../mixins/Date';

import PriceRange from './PriceRange';
import PriceGradient from './PriceGradient';

import StorePrices from '../../store/store/Prices';

export default class MonthsList {
  /**
   *
   * @param {HTMLElement} container
   * @param {EventStore} eventStore
   * @param {Theme} theme
   */
  constructor(container, eventStore, theme) {
    this._eventStore = eventStore;
    this._theme = theme;
    /**
     *
     * @type {MonthsOptions[]}
     * @private
     */
    this._options = generateMonthList();
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._container = container;
    /**
     *
     * @type {number}
     * @private
     */
    this._listYear = this._currentYear = (new Date()).getFullYear();

    this.__initHTML().__initEvents();

    /**
     *
     * @type {boolean}
     * @private
     */
    this._init = false;

  }

  /**
   *
   * @return {boolean}
   */
  get $isInit() {
    return this._init;
  }

  /**
   *
   * @return {boolean|*}
   */
  get $isLoading() {
    return this._loading;
  }

  /**
   *
   * @param {{detail:(ResultsShow)}} event
   * @private
   */
  __updateList_v2(/*{detail:(ResultsShow)}*/ event) {
    this._loading = !!event.detail.loading;

    if (!this._init && !this._loading)
      this._init = true;
    this.__updateMonths(event.detail.price).__updatePriceRange(event.detail.price);
  }

  /**
   *
   * @param {StorePrices} prices
   * @return {MonthsList}
   * @private
   */
  __updateMonths(/*StorePrices*/prices = new StorePrices()) {
    let gradient = PriceGradient.getPriceColor(prices.$minRange, this._theme);

    let options = this._options.map(function (/*MonthsOptions*/item) {
      /*eslint no-unexpected-multiline: "error"*/
      /**
       *
       * @type {MonthsOptions}
       */
      let newItem = {...item};
      /**
       *
       * @type {StorePrices}
       */
      let price = prices.getBy(`${newItem.year}_${newItem.month}`);
      if (price) {
        newItem.color = gradient.find(price.$delta);
        newItem.price = 'custom';
      } else {
        newItem.color = null;
        newItem.price = 'none';
      }
      return newItem;
    }, this);

    if (JSON.stringify(options) !== JSON.stringify(this._options)) {
      this._options = options;
      this.__initItems();
    }

    return this;
  }

  /**
   *
   * @param {StorePrices} prices
   * @return {MonthsList}
   * @private
   */
  __updatePriceRange(/*StorePrices*/prices = new StorePrices()) {
    this._priceRange.update(prices.$minRange);
    return this;
  }

  /**
   *
   * @return {MonthsList}
   * @private
   */
  __initHTML() {

    this._over = docCreate('div');

    this._node = docCreate('div', 'ottbp-months');
    // this._nodeLoader = docCreate('div', 'ottbp-months-loader');
    this._nodePriceRange = docCreate('div', 'ottbp-months-price_range');

    // this._loader = new Loader(this._nodeLoader);

    this.__initItems().__initPriceRange();

    // this._over.appendChild(this._nodeLoader);
    this._over.appendChild(this._node);
    this._over.appendChild(this._nodePriceRange);
    this._container.appendChild(this._over);

    return this;
  }

  /**
   *
   * @return {MonthsList}
   * @private
   */
  __initPriceRange() {
    this._priceRange = new PriceRange(this._nodePriceRange);
    return this;
  }

  /**
   *
   * @return {MonthsList}
   * @private
   */
  __initEvents() {
    // this._eventStore.on(['Query::update:monthly', 'Query::update:main'], ::this.__updateList, 0);
    // this._eventStore.on(['Controller::load:results'], ::this.__clearList_v2, 0);
    this._eventStore.on(['Controller::load:results'], ::this.__updateList_v2, 0);
    this._eventStore.on(['Controller::show:results'], ::this.__updateList_v2, 0);
    // this._eventStore.addEvent(this._container, 'click', ::this.__onClick);

    this._eventStore.delegateDocument('click', '.ottbp-month', ::this.__onClick_v2);
    return this;
  }

  /**
   *
   * @return {MonthsList}
   * @return {MonthsList}
   * @private
   */
  __initItems() {

    this._node.innerHTML = '';
    this._listYear = this._currentYear = (new Date()).getFullYear();
    /**
     *
     * @type {DocumentFragment}
     */
    let fragment = appendChildren(document.createDocumentFragment(), this._options.map(this.__initItem.bind(this)));
    this._node.appendChild(fragment);
    return this;
  }

  /**
   *
   * @param {MonthsOptions} item
   * @param {number} index
   * @return {HTMLElement}
   * @private
   */
  __initItem(item, index) {
    let itemNode = docCreate('div', 'ottbp-months__item');
    /**
     *
     * @type {string}
     */
    let title = l8n.month.full[item.month];

    if (this._currentYear !== item.year && this._listYear !== item.year) {
      title += ' ' + item.year;
      this._listYear = item.year;
    }

    let cl = (item.price ? `ottbp--${item.price}_price` : 'ottbp--none_price');
    let color = (item.color) ? `rgb(${item.color.r | 0},${item.color.g | 0},${item.color.b | 0})` : '';

    itemNode.innerHTML = `<button style="background-color: ${color} !important;" data-index="${index}" class="ottbp-js-hover ottbp-month ottbp-texts--month ottbp-btn ${cl}"><span class="ottbp-month__inner">${title}</span></button>`;

    return itemNode;
  }

  // /**
  //  *
  //  * @param {MouseEvent} event
  //  * @private
  //  */
  // __onClick(event) {
  //   event.stopPropagation();
  //   event.preventDefault();
  //
  //   let target = event.target;
  //   while (target && target != this._node) {
  //     if (target.className.indexOf('ottbp-month') >= 0 && target.hasAttribute('data-index')) {
  //       this.onSelect(this._options[parseInt(target.getAttribute('data-index'), 10)]);
  //     }
  //     target = target.parentNode;
  //   }
  // }

  /**
   *
   * @param {{detail:{target:HTMLElement, original:MouseEvent}}} event
   * @private
   */
  __onClick_v2(event) {

    let target = event.detail.target;
    let originalEvent = event.detail.original;

    if (target.hasAttribute('data-index')) {
      originalEvent.stopPropagation();
      originalEvent.preventDefault();

      this.onSelect(this._options[parseInt(target.getAttribute('data-index'), 10)]);

    }

  }

  /**
   *
   * @param {function} callback
   * @return {MonthsList}
   */
  whenSelect(callback) {
    this._onSelect = callback;
    return this;
  }

  /**
   *
   * @param {MonthsOptions} item
   */
  onSelect(item) {
    if (this._onSelect)
      this._onSelect(item);
  }

  show() {
    this._over.setAttribute('style', '');
    // this._over.style.display = '';
  }

  hide() {
    this._over.setAttribute('style', 'display:none !important;');
    // this._over.style.display = 'none';
  }

  // /**
  //  *
  //  * @param {MonthsOptions[]} options
  //  * @return {MonthsList}
  //  */
  // update(options) {
  //   // @TODO something
  //   return this;
  // }

}