'use strict';

import Format, {dateDurations} from './../../mixins/Format';
import {convertSearchFareToBestDeal} from './../../libs/OneTwoTrip';

export default class Response {
  /**
   *
   * @param {TripOptions} data_bestDealsResponse
   */
  constructor(data_bestDealsResponse) {
    /**
     *
     * @type {TripOptions}
     * @private
     */
    this._data = data_bestDealsResponse;
  }

  /**
   *
   * @return {string}
   */
  get $from() {
    return this._data.from;
  }

  /**
   *
   * @return {string}
   */
  get $to() {
    return this._data.to;
  }

  /**
   *
   * @return {?Date}
   */
  get $departure() {
    return Format.query.bestDeals.stringToDate(this._data.departDate);
  }

  /**
   *
   * @return {?Date}
   */
  get $return() {
    return Format.query.bestDeals.stringToDate(this._data.returnDate);
  }

  /**
   *
   * @return {number}
   */
  get $duration() {
    return dateDurations(this.$departure, this.$return);
  }

  /**
   *
   * @return {Array.<DirectionOptions>[]}
   */
  get $directions() {
    return this._data.directions;
  }

  /**
   *
   * @return {Array.<string,number>}
   */
  get $indexes() {
    let departureDate = this.$departure, returnDate = this.$return;
    let indexes = [`${departureDate.getFullYear()}_${departureDate.getMonth()}`, `${departureDate.getDate()}`];
    if (departureDate != returnDate)
      return indexes.concat([`${returnDate.getFullYear()}_${returnDate.getMonth()}`, `${returnDate.getDate()}`]);
    return indexes;
  }

  /**
   *
   * @return {number}
   */
  get $price() {
    return this._data.price;
  }

  /**
   *
   * @return {string}
   */
  get $currency() {
    return this._data.currency;
  }

  /**
   *
   * @return {number}
   */
  get $countFlares() {
    return Math.max(this.$countFlaresTo, this.$countFlaresBack);
  }

  /**
   *
   * @return {Number}
   */
  get $countFlaresTo() {
    return this._data.directions[0].length;
  }

  /**
   *
   * @return {Number}
   */
  get $countFlaresBack() {
    return (this._data.directions[1] ? this._data.directions[1].length : 0);
  }

  /**
   *
   * @return {string}
   */
  get $key() {
    return `${this.$from}_${this.$to}_${this._data.departDate}_${this._data.returnDate}${this.$price}`;
  }

  // CreateStatic

  /**
   *
   * @param {SearchOptions|TripOptions} options
   * @return {Response}
   */
  static create(options) {
    if (options.dirs)
      return Response.createFromSearch(options);
    return Response.createFromBestDeals(options);
  }

  /**
   *
   * @param {TripOptions} bestDealsResponseData
   * @return {Response}
   */
  static createFromBestDeals(bestDealsResponseData) {
    return new Response(bestDealsResponseData);
  }

  /**
   *
   * @param {SearchOptions} searchResponseData
   * @return {Response}
   */
  static createFromSearch(searchResponseData) {
    return new Response(convertSearchFareToBestDeal(searchResponseData));
  }

  /**
   *
   * @return {string}
   */
  get $route() {
    let depDate = this.$departure;
    let retDate = this.$return;

    let str = '';

    if (depDate) {
      let month = depDate.getMonth() + 1
        , day = depDate.getDate();
      str += `${day < 10 ? '0' + day : day}${month < 10 ? '0' + month : month}`;

      str += `${this.$from}${this.$to}`;

      if (retDate) {
        let month = retDate.getMonth() + 1
          , day = retDate.getDate();
        str += `${day < 10 ? '0' + day : day}${month < 10 ? '0' + month : month}`;
      }
    }

    return str;
  }

}