'use strict';
/**
 *
 * @param {HTMLElement|HTMLDocument|Window} element
 * @param {string} type
 * @param {function(event:Event)} callback
 * @param {boolean} [useCapture=false]
 */
export default function addEvent(element, type, callback, useCapture = false) {
  if (element.addEventListener) {
    element.addEventListener(type, callback, useCapture);
  } else {
    element.attachEvent(`on${type}`, callback);
  }
}