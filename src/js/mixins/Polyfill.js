'use strict';

// Не меняем прототип элементов так как это не наш сайт)

import './polyfill/Array';
import {closest} from './polyfill/Element';

export default {
  Element : {
    closest : closest
  }
}