'use strict';

export function trim(str) {
  if (typeof str !== 'string')
    return '';

  if (str.trim)
    return str.trim();
  return str.replace(/^\s+|\s+$/g, '');
}