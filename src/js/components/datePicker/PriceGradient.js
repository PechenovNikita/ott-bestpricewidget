'use strict';

import Color from './../../mixins/Color';

class PriceGradient {
  /**
   *
   * @param priceRange
   * @param {Theme} theme
   */
  constructor(priceRange, theme) {
    this._range = priceRange;

    this._gradientOptions = [{
      range : 0,
      color : theme.price().low
    }, {
      range : (priceRange[1] - priceRange[0]) / (priceRange[2] - priceRange[0]),
      color : theme.price().middle
    }, {
      range : 1,
      color : theme.price().high
    }];
  }

  find(delta) {
    if (!this._gradient)
      this._gradient = Color.gradient(this._gradientOptions);
    return this._gradient.find(delta);
  }
}

export default {
  /**
   *
   * @param priceRange
   * @param {Theme} theme
   * @return {PriceGradient}
   */
  getPriceColor(priceRange, theme){
    return new PriceGradient(priceRange, theme);
  }
};
