'use strict';

import Format from './../../mixins/Format';
import {deepExtend, deepClone}  from './../../mixins/Deep';
import {defaultFormData} from './../../config/_default';

export default class Query {
  /**
   *
   * @param {FormatMainForm} [options]
   */
  constructor(options) {
    /**
     *
     * @type {?string}
     * @private
     */
    this._fromCity = null;
    /**
     *
     * @type {?string}
     * @private
     */
    this._toCity = null;
    /**
     *
     * @type {?string}
     * @private
     */
    this._toCountry = null;
    /**
     *
     * @type {'E'|'B'}
     * @private
     */
    this._type = 'E';

    /**
     *
     * @type {boolean}
     * @private
     */
    this._roundtrip = true;
    /**
     *
     * @type {string|number}
     * @private
     */
    this._transfer = 'any';
    /**
     *
     * @type {string|number}
     * @private
     */
    this._duration = 'any';
    /**
     *
     * @type {?Date}
     * @private
     */
    this._departure = null;
    /**
     *
     * @type {?Date}
     * @private
     */
    this._departureDate = null;
    /**
     *
     * @type {?Date}
     * @private
     */
    this._returnDate = null;

    this.__parseOptions(options);
  }

  /**
   *
   * @return {string|null}
   */
  get $nameWithDate() {
    /**
     * @this {Query}
     */
    if (this._nameWithDate)
      return this._nameWithDate;
    /**
     *
     * @type {string|null}
     */
    let name = this.$name;

    name += `_${this.$departure ? this.$departure.getTime() : 'null'}`;
    name += `_${this.$return ? this.$return.getTime() : 'null'}`;
    /**
     *
     * @type {string|null}
     * @private
     */
    this._nameWithDate = name;

    return name;
  }

  /**
   *
   * @return {string|null}
   */
  get $nameFull() {
    if (this._nameFull)
      return this._nameFull;
    /**
     *
     * @type {string|null}
     */
    let name = this.$nameWithDate;

    if (this.$inMonth) {
      let month = this.$month;
      name += `_${month.year}_${month.month}`;
    } else {
      name += '_false';
    }
    /**
     *
     * @type {string|null}
     * @private
     */
    this._nameFull = name;

    return name;
  }

  /**
   *
   * @return {string|null}
   */
  get $nameWithAnyTransfer() {
    if (this.$transfer == 'any')
      return this.$name;
    return this.__name('any');
  }

  /**
   *
   * @return {string|null}
   */
  get $nameWithAnyDuration() {
    if (this.$duration === null)
      return this.$name;
    let query = this.$bestDealsQuery;
    query.stay_from = undefined;
    query.stay_to = undefined;
    return this.__name(this.$transfer, query);
  }

  /**
   *
   * @return {string|null}
   */
  get $nameWithAnyDurationAndTransfer() {
    if (this.$duration === null)
      return this.$name;
    let query = this.$bestDealsQuery;
    query.stay_from = undefined;
    query.stay_to = undefined;
    return this.__name('any', query);
  }

  /**
   *
   * @return {string|null}
   */
  get $name() {
    if (this._name) {
      return this._name;
    }
    /**
     *
     * @type {string|null}
     * @private
     */
    this._name = this.__name(this.$transfer);
    return this._name;
  }

  /**
   *
   * @param transfer
   * @param query
   * @return {string|null}
   * @private
   */
  __name(transfer, query = this.$bestDealsQuery) {
    if (!query || !query.origin || (!query.destination_countries && !query.destinations))
      return null;

    let str = `${query.origin}_${query.destination_countries}_${query.destinations}`;
    str += `_${(query.direct_flights ? 'true' : 'false')}`;
    str += `_${(query.roundtrip_flights ? 'true' : 'false')}`;
    str += `_${transfer}`;

    if (query.roundtrip_flights)
      str += `_${query.stay_from}_${query.stay_to}`;

    return str;
  }

  /**
   *
   * @return {string}
   */
  get $key() {
    if (!this._key) {
      this._key = this.__key();
    }
    return this._key;
  }

  // /**
  //  *
  //  * @return {string}
  //  */
  // get $key_withDeparture() {
  //   if (!this._key_withDeparture) {
  //     this._key_withDeparture = `${this.$key}_${this.departure ? this.departure.getTime() : 'null'}`;
  //   }
  //   return this._key_withDeparture;
  // }

  // /**
  //  *
  //  * @return {string}
  //  */
  // get $key_anyDuration() {
  //   if (!this._key_anyDuration) {
  //     this._key_anyDuration = this.__key(undefined, null);
  //   }
  //   return this._key_anyDuration;
  // }

  // /**
  //  *
  //  * @return {string}
  //  */
  // get $key_anyCountFlares() {
  //   if (!this._key_anyCountFlares) {
  //     this._key_anyCountFlares = this.__key(Infinity);
  //   }
  //   return this._key_anyCountFlares;
  // }

  // /**
  //  *
  //  * @return {string|*}
  //  */
  // get $key_anyCountFlaresAndDuration() {
  //   if (!this._key_anyCountFlaresAndDuration) {
  //     this._key_anyCountFlaresAndDuration = this.__key(Infinity, null);
  //   }
  //   return this._key_anyCountFlaresAndDuration;
  // }

  /**
   *
   * @param {number|Infinity} [countFlares]
   * @param {number|string|null} [duration]
   * @return {string}
   * @private
   */
  __key(countFlares = this.$countFlares, duration = this.$duration) {
    let str = `${this.$origin}_${this.$destinationCity}_${this.$destinationCountry}`;
    str += `_${this.$isRoundTrip}`;
    str += `_${countFlares}`;

    if (this.$isRoundTrip)
      str += `_${duration}`;

    return str;
  }

  /**
   *
   * @param {FormatMainForm} [options]
   * @private
   */
  __parseOptions(options) {
    /**
     * @type {FormatMainForm}
     */
    let tempOptions = deepExtend({}, defaultFormData, options);
    /**
     *
     * @type {FormatMainForm}
     * @private
     */
    this._options = tempOptions;

    this.$origin = tempOptions.inputs.from;
    this.$destinationCity = tempOptions.inputs.to;
    this.$destinationCountry = tempOptions.inputs.toCountry;

    this.$duration = tempOptions.selectors.duration;
    this.$transfer = tempOptions.selectors.transfer;
    this.$roundTrip = tempOptions.selectors.route;
    this.$type = tempOptions.selectors.type;

    this.$departure = tempOptions.date.start;
    this.$return = tempOptions.date.end;

  }

  /**
   *
   * @return {?string}
   */
  get $origin() {
    return typeof this._fromCity === 'string' && this._fromCity ? this._fromCity : null;
  }

  /**
   *
   * @param {?string} from
   */
  set $origin(from) {
    this._fromCity = from;
  }

  /**
   *
   * @return {?string}
   */
  get $destinationCity() {
    return typeof this._toCity === 'string' && this._toCity ? this._toCity : null;
  }

  /**
   *
   * @return {?string}
   */
  get $destinationCountry() {
    return typeof this._toCountry === 'string' && this._toCountry ? this._toCountry : null;
  }

  /**
   *
   * @param {?string} cityIata
   */
  set $destinationCity(cityIata) {
    if (cityIata) {
      this._toCity = cityIata;
      this._toCountry = null;
    } else {
      this._toCity = null;
    }
  }

  /**
   *
   * @param {?string} countryIata
   */
  set $destinationCountry(countryIata) {
    if (countryIata) {
      this._toCity = null;
      this._toCountry = countryIata;
    } else {
      this._toCountry = null;
    }
  }

  /**
   *
   * @return {boolean}
   */
  get $isDestinationCountry() {
    return !!this.$destinationCountry;
  }

  /**
   *
   * @return {boolean}
   */
  get $isDirect() {
    return (this.$countFlares == 1);
  }

  /**
   *
   * @return {boolean}
   */
  get $isRoundTrip() {
    return !!this._roundtrip;
  }

  /**
   *
   * @param {?boolean} roundTrip
   */
  set $roundTrip(roundTrip) {
    this._roundtrip = !!roundTrip;
  }

  // /**
  //  *
  //  * @return {?boolean}
  //  */
  // get $roundTrip() {
  //   return this._roundtrip;
  // }

  /**
   *
   * @return {number|string}
   */
  get $transfer() {
    return this._transfer;
  }

  /**
   *
   * @param {number|string} transfer
   */
  set $transfer(transfer) {
    switch (transfer) {
      case 'none':
      case 0:
        this._transfer = 'none';
        break;
      case 1:
        this._transfer = 1;
        break;
      default:
        this._transfer = 'any';
    }
  }

  /**
   *
   * @return {?string}
   */
  get $type() {
    switch (this._type) {
      case 'E':
      case 'e':
        return 'E';

      case 'B':
      case 'b':
        return 'B';
      default:
        return 'E';
    }
  }

  /**
   *
   * @param {?string} type
   */
  set $type(type) {
    switch (type) {
      case 'E':
      case 'e':
        this._type = 'E';
        break;
      case 'B':
      case 'b':
        this._type = 'B';
        break;
      default:
        this._type = 'E';
    }
  }

  /**
   *
   * @return {number}
   */
  get $countFlares() {
    // Без пересадок
    if (this._transfer == 'none')
      return 1;
    // Любые пересадки
    else if (this._transfer == 'any')
      return Infinity;
    // Не больше одной
    return 2;
  }

  /**
   *
   * @return {null|number|string}
   */
  get $duration() {
    let duration = parseInt(this._duration, 10);
    return (this._duration == 'any' || !duration) ? null : duration;
  }

  /**
   *
   * @param {null|number|string} duration
   */
  set $duration(duration) {
    if (!duration)
      this._duration = 'any';
    else
      this._duration = duration;
  }

  /**
   *
   * @param {?Date} date
   */
  set $departure(date) {
    this._departureDate = (date instanceof Date ? date : null);
  }

  /**
   *
   * @return {?Date}
   */
  get $departure() {
    return this._departureDate instanceof Date ? this._departureDate : null;
  }

  // /**
  //  *
  //  * @return {string}
  //  */
  // get $departureBestDeal() {
  //   let departure = this.departure;
  //   return departure instanceof Date ? Format.query.bestDeals.dateToString(departure) : '';
  // }

  /**
   *
   * @return {?number}
   */
  get $departureDay() {
    let date = this.$departure;
    return date instanceof Date ? date.getDate() : null;
  }

  /**
   *
   * @return {?number}
   */
  get $departureMonth() {
    let date = this.$departure;
    return date instanceof Date ? date.getMonth() : null;
  }

  /**
   *
   * @return {?number}
   */
  get $departureYear() {
    let date = this.$departure;
    return date instanceof Date ? date.getFullYear() : null;
  }

  /**
   *
   * @param {?Date} date
   */
  set $return(date) {
    this._returnDate = (date instanceof Date ? date : null);
  }

  /**
   *
   * @return {?Date}
   */
  get $return() {
    return this._returnDate instanceof Date ? this._returnDate : null;
  }

  /**
   *
   * @return {?number}
   */
  get $returnDay() {
    let date = this.$return;
    return date instanceof Date ? date.getDate() : null;
  }

  /**
   *
   * @return {?number}
   */
  get $returnMonth() {
    let date = this.$return;
    return date instanceof Date ? date.getMonth() : null;
  }

  /**
   *
   * @return {?number}
   */
  get $returnYear() {
    let date = this.$return;
    return date instanceof Date ? date.getFullYear() : null;
  }

  queryDate(name, query) {
    if (this[`_${name}Date`] instanceof Date) {
      let dateString = Format.query.bestDeals.dateToString(this[`_${name}Date`]);
      query[`${name}_date_from`] = dateString;
      query[`${name}_date_to`] = dateString;
    }
  }

  /**
   *
   * @return {OttDealsQuery}
   * @private
   */
  __basicBestDealsQuery() {
    if (this._queryBestDealsBasic)
      return deepClone(this._queryBestDealsBasic);
    /**
     *
     * @type {OttDealsQuery}
     */
    let query = {
      origin      : this.$origin,
      deals_limit : 500
    };

    // console.log('__basicBestDealsQuery()');

    if (this.$destinationCity)
      query.destinations = this.$destinationCity;
    else if (this.$destinationCountry)
      query.destination_countries = this.$destinationCountry;

    if (this.$isRoundTrip) {
      query.roundtrip_flights = true;
    }

    if (this.$isDirect) {
      query.direct_flights = true;
    }

    let duration = this.$duration;
    if (this.$isRoundTrip && duration) {
      query.stay_from = duration;
      query.stay_to = duration;
    }
    /**
     *
     * @type {OttDealsQuery}
     * @private
     */
    this._queryBestDealsBasic = query;

    return deepClone(query);
  }

  /**
   *
   * @return {OttDealsQuery}
   */
  get $commonBestDealsQuery() {
    return this.__basicBestDealsQuery();
  }

  /**
   *
   * @return {OttDealsQuery}
   */
  get $bestDealsQuery() {

    if (this._queryBestDeals)
      return deepClone(this._queryBestDeals);
    /**
     *
     * @type {OttDealsQuery}
     */
    let query = this.__basicBestDealsQuery();
    this.queryDate('return', query);
    this.queryDate('departure', query);
    /**
     *
     * @type {OttDealsQuery}
     * @private
     */
    this._queryBestDeals = query;
    return deepClone(query);
  }

  /**
   *
   * @param {number} year
   * @param {number} month
   * @return {OttDealsQuery}
   */
  bestDealsQueryMonthly(year, month) {

    let query = this.__basicBestDealsQuery();

    query.departure_date_from = Format.query.bestDeals.dateToString(new Date(year, month, 1));
    query.departure_date_to = Format.query.bestDeals.dateToString(new Date(year, month + 1, 0));
    query.group_by_date = true;

    return query;
  }

  /**
   *
   * @param {number} year
   * @param {number} month
   * @return {OttDealsQuery}
   */
  bestDealsQueryReturnMonthly(year, month) {
    let query = this.$bestDealsQuery;

    query.return_date_from = Format.query.bestDeals.dateToString(new Date(year, month, 1));
    query.return_date_to = Format.query.bestDeals.dateToString(new Date(year, month + 1, 0));

    return query;

  }

  // static dateToSearching

  /**
   *
   * @return {?OttFlightQuery}
   */
  get $searchingQuery() {
    /**
     * @this {Query}
     */
    /**
     *
     * @type {OttFlightQuery}
     */
    let query = {
      cs : this.$type
    };

    if (this.$origin && this.$destinationCity) {
      // console.log('(this.origin && this.destinationCity)');
      if (this.$isRoundTrip) {
        // console.log('(this.isRoundTrip)');
        if (this.$departure && this.$return) {
          // console.log('(this.departure && this.return)');
          query.route = `${Format.query.searching.dateToString(this.$departure)}${this.$origin}${this.$destinationCity}${Format.query.searching.dateToString(this.$return)}`;
        } else {
          // console.log('else (this.departure && this.return)');
          return null;
        }
      } else {
        // console.log('else (this.isRoundTrip)');
        if (this.$departure) {
          // console.log('(this.departure)');
          query.route = `${Format.query.searching.dateToString(this.$departure)}${this.$origin}${this.$destinationCity}`;
        } else {
          // console.log('else (this.departure)');
          return null;
        }
      }
    } else {
      // console.log('else (this.origin && this.destinationCity)');
      return null;
    }
    return query;
  }

  /**
   *
   * @return {boolean}
   */
  get $canBestDeals() {
    return !!(this.$origin && (this.$destinationCity || this.$destinationCountry));
  }

  /**
   *
   * @return {boolean}
   */
  get $needFromDayToMonthly() {
    return !!(this.$canBestDeals && this.$isRoundTrip && this.$departure && !this.$return);
  }

  // /**
  //  *
  //  * @return {boolean}
  //  */
  // get $canSearchFlight() {
  //   if (this.origin && this.destinationCity) {
  //     if (this.isRoundTrip) {
  //       return !!(this.$departure && this.$return);
  //     } else {
  //       return !!(this.$departure);
  //     }
  //   }
  //   return false;
  // }

  /**
   *
   * @param {Query} query
   * @return {boolean}
   */
  isSame(query) {
    return this.$key == query.$key;
  }

  /**
   *
   * @param {Query} query
   * @return {boolean}
   */
  isSameFull(query) {
    return this.$nameFull == query.$nameFull;
  }

  // /**
  //  *
  //  * @param {Query} query
  //  * @return {boolean}
  //  */
  // isSameWithDate(query) {
  //   return this.nameWithDate == query.nameWithDate;
  // }

  /**
   *
   * @return {boolean}
   */
  get $isComplete() {
    /**
     * @this {Query}
     */
    if (!this.$origin)
      return false;
    if (!this.$destinationCity && !this.$destinationCountry)
      return false;

    if (this.$isRoundTrip)
      return !!(this.$departure && this.$return);
    return !!this.$departure;
  }

  /**
   *
   * @return {boolean}
   */
  get $inMonth() {
    return !!(this.$month);
  }

  /**
   *
   * @return {?{year:number,month:number}}
   */
  get $month() {
    return this._options && this._options.date ? this._options.date.inMonth : null;
  }

  /**
   *
   * @param {FormatMainForm} options
   * @return {Query}
   */
  clone(options) {
    let newOptions = deepExtend({}, this._options, options);
    return new Query(newOptions);
  }
}
