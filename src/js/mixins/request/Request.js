'use strict';

/**
 *
 * @typedef {Object}    RequestOptions
 *
 * @property {function}   [success=function(response:any, id:string){}] - выполняется при ответе сервера
 * @property {function}   [fail=function(id:string){}] - выполняется при ошибке и timeout
 * @property {function}   [always=function(id:string){}] - выполняется всегда по завершению запроса
 *
 * @property {number}     [timeout=20] - время ожидания ответа (сек)
 * @property {number}     [repeats=5] - количество повторов при timeout
 * @property {string}     [responseType=json] - тип ответа
 *
 * @property {Object}     [data={}] - данные
 *
 * @property {string}     [method=callback] - название параметра функции jsonp
 */

import {deepExtend} from '../Deep';
import Format from '../Format';

/**
 *
 * @type {Array<Request>}
 */
const pausedRequest = [];

/**
 *
 * @type {number}
 */
let countRequest = 0;
const MAX_REQUESTS = 12;

function startPausedRequest() {
  setTimeout(function () {
    if (countRequest < MAX_REQUESTS && pausedRequest.length > 0) {
      pausedRequest.pop().send();
    }
  }, 237);
}

/**
 *
 * @type {RequestOptions}
 */
const defaultRequestOptions = {
  success : function () {},
  fail    : function () {},
  always  : function () {},

  timeout : 20,
  repeats : 3,

  responseType : 'json',

  data : {},

  method : 'callback'
};

export default class Request {
  /**
   *
   * @param {string} url
   * @param {RequestOptions} options
   */
  constructor(url, options) {
    /**
     *
     * @type {string}
     * @private
     */
    this._url = url;
    /**
     * @type {RequestOptions}
     */
    this.options = deepExtend({}, defaultRequestOptions, options);
    /**
     *
     * @type {Object}
     * @private
     */
    this._data = this.options.data;
    /**
     *
     * @type {number}
     * @private
     */
    this._timestamp = Date.now();
    /**
     *
     * @type {string}
     * @private
     */
    this._id = `${this._timestamp}_${Math.round(Math.random() * 1000001)}`;
    /**
     *
     * @type {boolean}
     * @private
     */
    this._done = false;
    /**
     *
     * @type {Object}
     * @private
     */
    this._adds = {};
    /**
     *
     * @type {number}
     * @private
     */
    this._tryes = 0;

    this.__initAdds();
  }

  /**
   *
   * @return {Request}
   * @private
   */
  __initAdds() {
    /* Дополнительные данные {this._adds} для потомков */
    return this;
  }

  // Events

  success(callback) {
    this.options.success = callback;
    return this;
  }

  fail(callback) {
    this.options.fail = callback;
    return this;
  }

  always(callback) {
    this.options.always = callback;
    return this;
  }

  // Properties
  /**
   *
   * @return {string}
   */
  get $id() {
    return this._id;
  }

  /**
   *
   * @return {Request}
   * @private
   */
  __initSrc() {
    this._src = Format.request.urlString(this._url, this._data, this._adds);
    return this;
  }

  // Методы

  send() {

    if (countRequest >= MAX_REQUESTS) {
      pausedRequest.push(this);
    } else {
      countRequest++;

      this._tryes++;
      this.__initSrc();
      this.__initSend();
      this.__initTimeout();
      return this;
    }

  }

  /**
   *
   * @return {Request}
   * @private
   */
  __initSend() {
    /* Отпарвляем запрос */
    return this;
  }

  /**
   *
   * @return {Request}
   * @private
   */
  __initTimeout() {
    /* Инициализируем таймаут */
    setTimeout(::this.__timeout, 1000 * this.options.timeout);
    return this;
  }

  // Ответы

  __success(response) {
    if (this._done)
      return;
    this._done = true;

    let data = response;

    if (this.options.responseType === 'json') {
      if (response) {
        if (typeof response === 'string') {
          try {
            data = JSON.parse(response);
          } catch (e) {
            // console.warn('Error parse json');
          }
        }
      } else {
        data = {};
      }
    }

    if (this.options.success)
      this.options.success(data, this.$id);

    this.__onSuccess(data);
    this.__always();
  }
  /*eslint no-unused-vars: ["error", {"args": "none"}]*/
  __onSuccess(response) {
    console.log(response);
    /* Делает после удачного ответа */
  }

  __timeout() {
    if (this._done)
      return;

    this.__clean();

    if (this._tryes < this.options.repeats) {
      this.__beforeNewTry();
      countRequest--;
      setTimeout(::this.send, 300);
    } else {
      if (this.options.fail)
        this.options.fail();

      this.__onTimeout();
      this.__always();
    }
  }

  __onTimeout() {
    /* После всех попыток запроса */
  }

  __fail() {
    if (this.options.fail)
      this.options.fail();
    this.__always();
  }

  __always() {
    if (this.options.always)
      this.options.always();
    this.__onAlways();

    countRequest--;
    startPausedRequest();
  }

  __onAlways() {
    /* После удачого или неудачного запроса */
  }

  __beforeNewTry() {
    /*Запускается перед инициализацией новой попытки*/
    return this;
  }

  abort() {

  }

  __clean() {

  }

}
