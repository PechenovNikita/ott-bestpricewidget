/*eslint no-unused-vars: ["error", { "args": "none" }]*/
'use strict';
import {DEALS as DEMO_DEALS} from '../../config/_demo';

import ControllerResults from '../controller/Results';
import Response from '../store/Response';
import DemoStorePrices from './StorePrices';

export default class DemoControllerResults extends ControllerResults {
  constructor() {
    super(null);

    this._store = null;
  }

  /**
   *
   * @param {Query} query
   * @return {ResultsShow}
   */
  show(query) {
    /**
     *
     * @type {Array.<Response>}
     */
    let results = DEMO_DEALS.map((data) => {
      return new Response(data);
    });
    return {
      loading : false,
      store   : {},
      query   : {},
      price   : new DemoStorePrices(),
      results : results
    };
  }

}