'use strict';

import airlinesEN from '../ref/airlines';

/**
 *
 * @param {string} iata
 * @return {string}
 */
export function byIATA(iata) {
  if(airlinesEN[iata])
    return airlinesEN[iata].split('|')[0];
  return '';
}