'use strict';
import Results from '../../blocks/results';

import {docCreate} from '../../mixins/Element';

export default class ResultsContainer {
  /**
   *
   * @param {HTMLElement} container
   * @param {EventStore} eventStore
   *
   */
  constructor(container, eventStore) {
    this._eventStore = eventStore;
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._container = container;
    this.__initHTML();
    this._container.appendChild(this._node);
  }

  __initHTML() {

    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._node = docCreate('div', 'ottbp-container-results');

    let _wrap = docCreate('div', 'ottbp-container-results__wrap');

    this._node.appendChild(_wrap);
    this._results = new Results(_wrap, undefined, undefined, this._eventStore);
    return this;
  }

  /**
   *
   * @return {boolean}
   */
  get $isLoading() {
    return this._results.$isLoading;
  }

  /**
   *
   * @return {number}
   */
  get $height() {
    let style = window.getComputedStyle(this._node)
      , padding_top = parseInt(style.getPropertyValue('padding-top'), 10);

    let _height = this._results.$height;

    console.log('$height', _height);

    this._node.setAttribute('style', `height: ${_height}px !important`);

    return _height + 2 * padding_top;
  }
}