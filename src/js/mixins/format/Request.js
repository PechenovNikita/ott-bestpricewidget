'use strict';

import {deepExtend} from './../Deep';
/**
 *
 * @param {Object} options
 * @param {Object} [adds={}]
 * @return {string}
 */
export function queryString(options, adds = {}) {
  let query = '', arr = [];
  options = deepExtend({}, adds, options);
  for (let propName in options) {
    if (options.hasOwnProperty(propName)) {
      if (options[propName] !== null) {
        arr.push(`${encodeURI(propName)}=${encodeURI(options[propName])}`);
      }
    }
  }

  query += arr.join('&');
  return query;
}
/**
 *
 * @param {string} url
 * @param {Object} options
 * @param {Object} [adds={}]
 * @return {string}
 */
export function urlString(url, options, adds = {}) {
  let query = queryString(options, adds);
  if (url.indexOf('?') > -1)
    return `${url}&${query}`;
  return `${url}?${query}`;
}

export default {
  queryString : queryString,
  urlString   : urlString
};
