'use strict';

import {docCreate} from './../mixins/Element';
import classList from '../mixins/polyfill/element/classList';

import InputSelect from '../components/InputSelect';
import SearchButton from '../components/SearchButton';

// import Eve from './../mixins/Event';

export default class Inputs {
  /**
   *
   * @param {HTMLElement} container
   * @param {EventStore} eventStore
   */
  constructor(container, eventStore) {
    this._eventStore = eventStore;
    this._container = container;
    this._options = {};
    this.__initHTML().__initEvents();

    this._container.appendChild(this._node);

  }

  /**
   *
   * @return {Inputs}
   */
  __initHTML() {
    this._node = docCreate('div', 'ottbp-inputs');
    this._fromNode = docCreate('div', 'ottbp-inputs__field ottbp-inputs--from');
    this._toNode = docCreate('div', 'ottbp-inputs__field ottbp-inputs--to');
    this._searchNode = docCreate('div', 'ottbp-inputs__field ottbp-inputs--search');

    this.__initInputs().__initBtn();

    this._node.appendChild(this._fromNode);
    this._node.appendChild(this._toNode);
    this._node.appendChild(this._searchNode);

    return this;
  }

  /**
   *
   * @return {Inputs}
   * @private
   */
  __initInputs() {
    /**
     *
     * @type {InputSelect}
     * @private
     */
    this._from = new InputSelect(this._fromNode, {placeholderDefault : 'Откуда'}, this._eventStore);
    /**
     *
     * @type {InputSelect}
     * @private
     */
    this._to = new InputSelect(this._toNode, {
      placeholderDefault : 'Куда: город или страна',
      country            : true
    }, this._eventStore);

    this._from.whenSelect(function (value) {
      this.__changeData('from', value);
    }.bind(this));

    this._to.whenSelect(function (value) {
      this.__changeData('to', value);
    }.bind(this));

    return this;
  }

  __initBtn() {
    this._btn = new SearchButton(this._searchNode, this._eventStore);
  }

  __initEvents() {
    this._eventStore.on('SearchButton::click', () => {
      if (this._to.$empty) {
        this._to.setError();
      }
      if (this._from.$empty) {
        this._from.setError();
      }
    });
    return this;
  }

  /**
   *
   * @param {string} type
   * @param {PlaceData} value
   * @private
   */
  __changeData(type, value) {
    this._options[type] = value;

    //@TODO удалить type and value
    this._eventStore.dispatch('Inputs::change', {
      type   : 'inputs',
      value  : this.$value,
      inputs : this.$value
    });

  }

  resize(mainWidth) {
    this.__setStyle(mainWidth);
  }

  __setStyle(mainWidth) {
    if (mainWidth < 488) {
      classList(this._node).remove('ottbp--big');
    } else {
      classList(this._node).add('ottbp--big');
    }
  }

  /**
   *
   * @param {FormatMainForm_Inputs} options
   */
  update(options) {

    this._from.update(options.from);

    if (options.to) {
      this._to.update(options.to, false);
    } else if (options.toCountry) {
      this._to.update(options.toCountry, true);
    }
  }

  /**
   *
   * @return {{from: string|null, to: string|null, toCountry: string|null}}
   */
  get $value() {
    /**
     *
     * @type {PlaceData}
     */
    let from = this._from.$value || {}
      , to = this._to.$value || {};

    return {
      from      : (from.iata ? from.iata : null),
      to        : ((to.type == 'country' || !to.iata) ? null : to.iata),
      toCountry : ((to.type !== 'country' || !to.iata) ? null : to.iata)
    };
  }

}