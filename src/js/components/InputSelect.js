'use strict';

import {docCreate} from './../mixins/Element';
import Helper from './inputSelect/helper';
import classList from '../mixins/polyfill/element/classList';

import {getAllByPart} from '../libs/Reference';
import {byIATA as getCity, byPart as getCityPart} from '../libs/reference/city';
import {byIATA as getCountry} from '../libs/reference/country';


export default class InputSelect {
  /**
   *
   * @param {HTMLElement} container
   * @param {Object} [options={}]
   * @param {string} [options.placeholder]
   * @param {string} [options.placeholderDefault]
   * @param {string} [options.value]
   * @param {boolean|undefined} [options.country]
   * @param {EventStore} eventStore
   */
  constructor(container, options = {}, eventStore) {
    this._eventStore = eventStore;
    /**
     * @type {HTMLElement}
     */
    this._container = container;
    /**
     *
     * @type {string}
     * @private
     */
    this._placeholderDefault = options.placeholderDefault || 'Placeholder';
    /**
     *
     * @type {string}
     * @private
     */
    this._placeholder = options.placeholder || '';
    /**
     *
     * @type {string}
     * @private
     */
    this._value = options.value || '';
    /**
     *
     * @type {boolean}
     * @private
     */
    this._country = !!options.country;

    this._data = {};

    this.initHTML();
  }

  /**
   *
   * @return {InputSelect}
   */
  initHTML() {

    this._node = docCreate('div', ['ottbp-control']);

    let fragment = document.createDocumentFragment();

    this.__initPlaceholderHTML(fragment)
      .__initInputHTML(fragment)
      .__initHelperHTML(fragment);

    this._node.appendChild(fragment);

    this._container.appendChild(this._node);
    return this;
  }

  __initHelperHTML(fragment) {
    let helper = docCreate('div', 'ottbp-control__helper');
    this._helper = new Helper(helper, {});
    this._helper.whenHover(this.__onHoverHelperList.bind(this));
    this._helper.whenSelect(this.__onSelectHelperList.bind(this));
    fragment.appendChild(helper);

    return this;
  }

  /**
   *
   * @param {DocumentFragment} fragment
   * @return {InputSelect}
   * @private
   */
  __initInputHTML(fragment) {
    let inputWrapper = docCreate('div', 'ottbp-control__input')
      , inputOver = docCreate('div', 'ottbp-input');
    /**
     *
     * @type {HTMLInputElement}
     * @private
     */
    this._input = docCreate('input', ['ottbp-input__field', 'ottbp-texts--textfield']);

    this._input.setAttribute('type', 'text');
    this._input.setAttribute('autocomplete', 'off');
    this._input.setAttribute('placeholder', '');

    this.__initInputEvents();

    inputWrapper.appendChild(inputOver);
    inputOver.appendChild(this._input);

    fragment.appendChild(inputWrapper);

    return this;
  }

  __initInputEvents() {

    this._input.addEventListener('input', this.onInput.bind(this));
    this._input.addEventListener('focus', this.onFocus.bind(this));
    this._input.addEventListener('blur', this.onBlur.bind(this));
    this._input.addEventListener('keydown', this.onKeyDown.bind(this));

    return this;
  }

  onKeyDown(event) {
    this.clearError();
    switch (event.keyCode) {
      //down
      case 40:
        event.stopPropagation();
        event.preventDefault();
        this._helper.highLight(+1);
        break;
      //up
      case 38:
        event.stopPropagation();
        event.preventDefault();
        this._helper.highLight(-1);
        break;
      //enter
      case 13:
        /**
         * Если нажали ентер до того как получили запрос от автокомплита
         * @type {boolean}
         * @private
         */
        this._entered = true;
        event.stopPropagation();
        event.preventDefault();
        this.__onSelectHelperList(this._helper.$currentItem);

        this._eventStore.dispatch('Control::set', {
          obj    : this,
          target : this._input
        });

        break;
    }
  }

  __initInputValue() {
    this._input.value = this._value;
    return this.__initPlaceHolderValue();
  }

  /**
   *
   * @param {PlaceData} item
   * @return {InputSelect}
   * @private
   */
  __placeHolderFromItem(item) {

    switch (item.type) {
      case 'country':
        this._placeholder = `${item.country.ru}`;
        break;
      case 'city':
        this._placeholder = `${item.city.ru},&nbsp;${item.country.ru}`;
    }
    return this;
  }

  /**
   *
   * @param {PlaceData} item
   * @private
   */
  __onHoverHelperList(item) {
    let cache = JSON.stringify(item);
    if (this._cacheHoverItemList == cache)
      return;
    this._cacheHoverItemList = cache;
    this.__placeHolderFromItem(item).__initPlaceHolderValue();
  }

  /**
   *
   * @param {PlaceData} item
   * @private
   */
  __onSelectHelperList(item) {
    this._cacheHoverItemList = null;
    if (!item)
      return;

    this.__setValue(item);
    this.__placeHolderFromItem(item);
    this._helper.update({items : []});
    this.hideHelper();
    this.__initInputValue();

    this.onSelect();

    this._eventStore.dispatch('Control::set', {
      obj    : this,
      target : this._input
    });

  }

  /**
   *
   * @param {DocumentFragment} fragment
   * @return {InputSelect}
   * @private
   */
  __initPlaceholderHTML(fragment) {
    let placeholderWrapper = docCreate('div', ['ottbp-control__placeholder'])
      , placeholderOver = docCreate('div', ['ottbp-placeholder', 'ottbp-texts--textfield']);

    this._placeholderHidden = docCreate('span', 'ottbp-placeholder__hidden');
    this._placeholderVisible = docCreate('span', 'ottbp-placeholder__visible');

    placeholderWrapper.appendChild(placeholderOver);
    placeholderOver.appendChild(this._placeholderHidden);
    placeholderOver.appendChild(this._placeholderVisible);

    fragment.appendChild(placeholderWrapper);
    return this.__initPlaceHolderValue();
  }

  /**
   *
   * @return {InputSelect}
   * @private
   */
  __initPlaceHolderValue() {
    if (!this._value || this._value.length == 0) {
      this.__setPlaceholder('', this._placeholderDefault);
      return this;
    } else {
      if (this._placeholder && this._value.length > 1) {
        this.__setPlaceholder(this._value, this._placeholder.slice(this._value.length));
      } else
        this.__setPlaceholder('', '');
    }
    return this;
  }

  /**
   *
   * @param {string} hidden
   * @param {string} visible
   * @return {InputSelect}
   * @private
   */
  __setPlaceholder(hidden, visible) {

    if (this._lastPlaceholder == hidden + '/' + visible)
      return this;

    this._lastPlaceholder = hidden + '/' + visible;

    this._placeholderHidden.innerHTML = hidden;
    this._placeholderVisible.innerHTML = visible;
    return this;
  }

  onFocus() {
    this._focused = true;
    this._entered = false;
    classList(this._node).add('ottbp-blur');
  }

  onBlur() {
    this._focused = false;
    setTimeout(function () {
      classList(this._node).remove('ottbp-blur');
      if (!this.$value.iata) {
        this._placeholder = '';
        this.__initPlaceHolderValue();
      }
      this.hideHelper();
    }.bind(this), 100);
  }

  showHelper() {
    classList(this._node).add('ottbp-helper');
  }

  hideHelper() {
    classList(this._node).remove('ottbp-helper');
  }

  onInput() {
    this._value = this._input.value;

    this._data = {};

    this._placeholder = '';

    if (this._value.length > 1) {
      if (this._country) {
        this.__onFind_v2(getAllByPart(this._value));
      } else {
        this.__onFind_v2(getCityPart(this._value));
      }// findPlaceAsync(this._value, this.__onFind.bind(this));
      this.showHelper();
    } else {
      this.hideHelper();
    }

    this.__initPlaceHolderValue();
  }

  /**
   *
   * @param {PlaceData[]} data
   * @private
   */
  __onFind_v2(data) {

    if (data) {
      if (!this._focused) {
        if (this._entered) {
          this.__setItem(data[0]);
        }
        return;
      }
      this._helper.update({items : data});
    }
  }

  /**
   *
   * @param {PlaceData} item
   * @return {InputSelect}
   * @private
   */
  __setValue(item) {
    this._data = item;
    switch (item.type) {
      case'country':
        this._value = item.country.ru;
        break;
      case 'city':
        this._value = item.city.ru;
        break;
    }
    return this;
  }

  /**
   *
   * @param {PlaceData} item
   * @private
   */
  __setItem(item) {
    if (!item)
      return;

    this.__setValue(item);
    this.__placeHolderFromItem(item);
    this.__initInputValue();

    this.onSelect();

    // this._eventStore.dispatch('Control::set', {
    //   obj    : this,
    //   target : this._input
    // });
  }

  clean() {
    this._data = {};
    this._value = '';
    this.__initInputValue();
    this.onSelect();
  }

  /**
   * @callback onInputSelect
   * @param {PlaceData} item
   */
  /**
   *
   * @param {onInputSelect} callback
   */
  whenSelect(callback) {
    /**
     *
     * @type {onInputSelect}
     * @private
     */
    this._onSelect = callback;
  }

  onSelect() {
    if (this._onSelect)
      this._onSelect(this.$value);
  }

  /**
   *
   * @param {string} [iata]
   * @param {boolean} [isCountry=false]
   */
  update(iata, isCountry = false) {

    if (iata) {
      if (isCountry) {

        let countryData = getCountry(iata);
        if (countryData) {
          this.__setItem(countryData);
        } else {
          this.clean();
        }
      } else {

        let cityData = getCity(iata);

        if (cityData) {
          this.__setItem(cityData);
        } else {
          this.clean();
        }

      }
    }
  }

  /**
   *
   * @return {boolean}
   */
  get $empty() {
    return !this.$value.iata;
  }

  /**
   *
   * @return {null|PlaceData}
   */
  get $value() {
    return this._data;
  }

  setError() {
    classList(this._node.parentNode).add('ottbp-control--error');
    classList(this._node).add('ottbp-control--error');
  }

  clearError() {
    classList(this._node).remove('ottbp-control--error');
    classList(this._node.parentNode).remove('ottbp-control--error');
  }
}