'use strict';

import {docCreate} from './../mixins/Element';
import DropDownSelect from '../components/DropDownSelect';
import selects from './../config/_configSelects';
// import Eve from './../mixins/Event';

import {CONTAINER_WIDTH as CW} from './../config/_constant';
import WIDTHS from './selects/_width';

export default class Selects {
  /**
   *
   * @param {HTMLElement} container
   * @param {EventStore} eventStore
   */
  constructor(container, eventStore) {
    this._eventStore = eventStore;
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._container = container;
    /**
     *
     * @type {{route?: string, type?: string, transfer?: string, duration?: string}.<string, string>}
     * @private
     */
    this._options = {};

    /**
     *
     * @type {{route?: DropDownSelect, type?: DropDownSelect, transfer?: DropDownSelect, duration?: DropDownSelect}.<string,DropDownSelect>}
     * @private
     */
    this._selectors = {};
    /**
     * Показывать (1) или не показывать (0) элементы по порядку
     * @type {Array.<number>}
     * @private
     */
    this._displays = [1, 0, 1, 1];

    this._size = 'big';

    this.__initHTML();
  }

  /**
   *
   * @return {Selects}
   * @private
   */
  __initHTML() {
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._node = docCreate('div', 'ottbp-selects');

    this.__initSelectsHTML().__initStyles();

    this._container.appendChild(this._node);

    return this;
  }

  /**
   *
   * @return {Selects}
   * @private
   */
  __initSelectsHTML() {

    this._containers = {};
    /**
     * @this {Selects}
     */
    selects.forEach(function (item) {
      let temp = docCreate('div', ['ottbp-selects__cell', `ottbp-selects--${item.type}`]);
      let wrapper = docCreate('div', 'ottbp-selects__cell__wrap');

      let tempDropDown = new DropDownSelect(
        item.selects,
        Selects.itemIndexByValue(item, this._options[item.type]),
        wrapper,
        undefined,
        this._eventStore);
      tempDropDown.whenSelect((function (value) {
        this.__onSelectsChange(item.type, value);
      }).bind(this));

      this._options[item.type] = tempDropDown.$value.value;

      this._selectors[item.type] = (tempDropDown);

      temp.appendChild(wrapper);
      this._node.appendChild(temp);

      this._containers[item.type] = temp;
    }, this);

    return this;
  }

  /**
   *
   * @return {Selects}
   * @private
   */
  __initStyles() {

    WIDTHS[this._displays.join('')][this._size].items.forEach(function (item) {
      /**
       * @this {Selects}
       */
      this._containers[item.type].setAttribute('style', `width:${item.percent}% !important;`);
    }, this);

    return this;
  }

  /**
   *
   * @param item
   * @param value
   * @return {number}
   */
  static itemIndexByValue(item, value) {
    return item.selects.reduce(function (defIndex, it, index) {
      if (it && typeof it.value !== 'undefined' && it.value === value)
        return index;
      return defIndex;
    }, 0);
  }

  /**
   *
   * @param {string} type
   * @param {DropDownSelectOptions} value
   * @private
   */
  __onSelectsChange(type, value) {
    if (this._options[type] !== value.value) {
      this._options[type] = value.value;
      this.__changeChain();

      // if (this._onChange)
      //   this._onChange();
      //@TODO удалить type and value
      this._eventStore.dispatch('Selects::change', {
        type      : 'selectors',
        value     : this.$value,
        selectors : this.$value,
      });

    }

  }

  /**
   *
   * @param {FormatMainForm_Selects} options
   */
  update(options) {
    Object.keys(options).forEach((key) => {
      this._selectors[key].update(options[key]);
    });
  }

  /**
   *
   * @return {{duration:string, route:boolean, transfer:string|number, type:string|number}}
   */
  get $value() {
    let val = {};

    for (let type in this._selectors) {
      if (this._selectors.hasOwnProperty(type)) {
        /**
         * @type {DropDownSelect}
         */
        let sel = this._selectors[type];
        val[type] = sel.$value.value;
      }
    }

    return val;
  }

  __changeChain() {

    if (this._options.route) {
      this._displays[3] = 1;
      this._selectors.duration.enable();
    } else {
      this._displays[3] = 0;
      this._selectors.duration.disable();
    }

    this.__initStyles();
  }

  resize(containerWidth) {
    if (containerWidth > CW.medium) {
      this._size = 'big';
    } else if (containerWidth > CW.small) {
      this._size = 'medium';
    } else if (containerWidth > CW.extra_small) {
      this._size = 'small';
    } else {
      this._size = 'extra_small';
    }

    this.render();
  }

  render() {
    let styles = WIDTHS[this._displays.join('')][this._size];
    this._displays.forEach(function (bool, index) {
      /**
       * @this {Selects}
       */
      if (bool) {
        this._selectors[styles.items[index].type].enable();
      } else {
        this._selectors[styles.items[index].type].disable();
      }
    }, this);
    this.__initStyles();
  }

}