'use strict';

let countryStore = {};

export default{
  add(countryIata, cityIata){
    if (!countryStore.hasOwnProperty(countryIata)) {
      countryStore[countryIata] = {};
    }
    if (countryStore[countryIata].hasOwnProperty(cityIata)) {
      countryStore[countryIata][cityIata]++;
      return;
    }
    countryStore[countryIata][cityIata] = 1;
  },
  get(countryIata){
    return countryStore[countryIata] ? countryStore[countryIata] : {};
  },
  test(){
    return countryStore;
  }
}