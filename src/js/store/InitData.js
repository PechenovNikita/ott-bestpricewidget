'use strict';
/**
 *
 * @type {FormatMainForm}
 */
export const defaultFormData = {
  inputs  : {
    from      : null,
    to        : null,
    toCountry : null
  },
  selectors : {
    duration : 'any',
    route    : true,
    transfer : 'any',
    type     : 'E'
  }
};