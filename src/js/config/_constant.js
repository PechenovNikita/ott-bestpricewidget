'use strict';
/**
 *
 * @type {number}
 */
export const
  CONTAINER_SIZE_EXTRA_SMALL = 475
  , CONTAINER_SIZE_SMALL = 560
  , CONTAINER_SIZE_MEDIUM = 768
  , CONTAINER_SIZE_BIG = 960
  , CONTAINER_SIZE_LARGE = 1200
  , CONTAINER_SIZE_EXTRA_LARGE = 1600;

export const CONTAINER_WIDTH = {
  extra_small : CONTAINER_SIZE_EXTRA_SMALL,
  small       : CONTAINER_SIZE_SMALL,
  medium      : CONTAINER_SIZE_MEDIUM,
  big         : CONTAINER_SIZE_BIG,
  large       : CONTAINER_SIZE_LARGE,
  extra_large : CONTAINER_SIZE_EXTRA_LARGE,
};

// export const STYLE_PREFIX = 'ottbp-';