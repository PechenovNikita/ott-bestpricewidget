const gulp = require('gulp')
  , watch = require('gulp-watch')
  , less = require('gulp-less')
  , livereload = require('gulp-livereload')
  , cssBase64 = require('gulp-css-base64');

const cleanCSS = require('gulp-clean-css');
const replace = require('gulp-replace');

const config = require('./_config');

function build_less() {

  return gulp.src('./src/less/index.less')
    .pipe(less())
    .pipe(cssBase64({
      baseDir : "../../dest/images",
    }))
    .pipe(gulp.dest(config.routes.dest + config.routes.css))
    .pipe(cleanCSS({
      compatibility : 'ie9',
      debug         : true
    }))
    // .pipe(replace(/\.ottbp\-/g, '$'))
    .pipe(gulp.dest(config.routes.src + 'css'))
    .pipe(livereload());
}

gulp.task('less:build', build_less);
gulp.task('less:watch', function () {
  return watch('./src/less/**/*.less', {verbose : true}, build_less);
});

module.exports = {
  build : gulp.parallel('less:build'),
  watch : gulp.parallel('less:watch')
};