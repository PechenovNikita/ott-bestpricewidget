'use strict';

import {isNative} from '../../Format';

const reduce = (() => {
  if (Array.prototype.reduce && isNative(Array.prototype.reduce)) {
    return (arr, callback, initialValue) => {
      return Array.prototype.reduce.call(arr, callback, initialValue);
    }
  }
  return (arr, callback, initialValue) => {
    value = initialValue;
    for (let i = 0; i < arr.length; i++) {
      value = callback(value, arr[i], i, arr);
    }
    return value;
  }
})();

export default reduce;