'use strict';

import {docCreate} from './../../mixins/Element';
import {price} from './../../mixins/Format';

export default class PriceRange {
  /**
   *
   * @param {HTMLElement} container
   * @param {number[]} [options=[]]
   */
  constructor(container, options = []) {
    this._container = container;
    this._options = options;

    this.initHTML();
  }

  initHTML() {
    this._node = docCreate('div');
    this.initItems();
    this._container.appendChild(this._node);
  }

  initItems() {

    this._node.innerHTML = '';
    let fragment = document.createDocumentFragment();

    let _isFinite = this._options ? this._options.reduce(function (last, num) {
      return last && isFinite(num);
    }, true) : false;

    if (this._options && this._options.length == 3 && _isFinite) {

      let min = this._options[0];

      this._options.forEach(function (pr, index) {
        if (min == pr && index > 0)
          return;

        let item = docCreate('div', 'ottbp-prices__item');

        let low = index + 1 < this._options.length / 2
          , middle = index + 1 < this._options.length;

        let innerHTML = '<div class="ottbp-price">';
        innerHTML += `<div class="ottbp-price__color"><div class="ottbp-price__color__circle ottbp--${low ? 'low' : (middle ? 'middle' : 'high')}_price"></div></div>`;
        innerHTML += `<div class="ottbp-price__cost ottbp-texts--price"><span class="ottbp-price__cost__inner">${price(pr, 'RUB')}</span></div>`;
        innerHTML += '</div>';

        item.innerHTML = innerHTML;
        fragment.appendChild(item);
      }, this);

    }/* else {

    }*/

    this._node.appendChild(fragment);
  }

  /**
   *
   * @param {number[]} [options=[]]
   * @return {PriceRange}
   */
  update(options = []) {
    this._options = options;
    this.initItems();
    return this;
  }
}
