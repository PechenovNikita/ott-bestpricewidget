'use strict';

import {docCreate} from '../mixins/Element';
import SearchSVG from '../mixins/svg/search';
// import Eve from '../mixins/Event';

export default class SearchButton {
  /**
   *
   * @param container
   * @param {EventStore} eventStore
   */
  constructor(container, eventStore) {
    this._eventStore = eventStore;
    this._container = container;
    this.__initHTML().__initEvents();
    this._container.appendChild(this._btn);
  }

  /**
   *
   * @return {SearchButton}
   * @private
   */
  __initHTML() {
    this._node = docCreate('div', 'ottbp-search');
    this._btn = docCreate('button', 'ottbp-search__btn');

    this.__initICON();

    this._node.appendChild(this._btn);

    return this;
  }

  /**
   *
   * @return {SearchButton}
   * @private
   */
  __initICON() {
    let icon = new SearchSVG();
    icon.resize(20, 20).fill('#616161');
    this._btn.innerHTML = `<span class="ottbp-search__btn__icon">${icon.toString()}</span><span class="ottbp-search__btn__text">Искать</span>`;

    return this;
  }

  /**
   *
   * @return {SearchButton}
   * @private
   */
  __initEvents() {
    this._eventStore.delegateDocument('click', this._btn, () => {
      this._eventStore.dispatch('SearchButton::click');
    });

    return this;
  }

}