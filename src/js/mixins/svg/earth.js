'use strict';

import Svg from '../Svg';
import './sprites';

export default class Earth extends Svg {
  constructor() {
    super();

    this._attr = {
      width  : 14,
      height : 14
    };

    this._options = {
      fill : '#616161'
    };

    this.update();
  }

  /**
   *
   * @return {Earth}
   */
  update() {
    this._inner = `<use xlink:href="#ottbp-s-earth" fill="${this._options.fill}"  class="ottbp-sprite-svg__earth"></use>`;
    return this.render();
  }

  /**
   *
   * @param {number} [width]
   * @param {number} [height]
   * @return {Earth}
   */
  resize(width = this._attr.width, height = this._attr.height) {
    return this.attr({
      width  : width,
      height : height
    });
  }

  /**
   *
   * @param fill
   * @return {Earth}
   */
  fill(fill) {
    this._options.fill = fill;
    return this.update();
  }

}

