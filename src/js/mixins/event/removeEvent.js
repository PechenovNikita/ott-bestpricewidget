'use strict';

export default function removeEvent(element, type, callback, useCapture = false) {
  if (element.removeEventListener) {
    element.removeEventListener(type, callback, useCapture);
  } else {
    element.detachEvent(`on${type}`, callback);
  }
}