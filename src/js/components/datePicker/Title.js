'use strict';

import {docCreate} from './../../mixins/Element';
import {dateString} from './../../mixins/Format';

import l8n from './../../store/l8n';

export default class Title {
  /**
   *
   * @param {HTMLElement} container
   * @param {EventStore} eventStore
   */
  constructor(container, eventStore) {
    this._eventStore = eventStore;
    this._container = container;
    this.__initHTML().__initEvents();
    this._container.appendChild(this._node);
  }

  /**
   *
   * @return {Title}
   * @private
   */
  __initHTML() {
    this._node = docCreate('div');
    return this;
  }

  /**
   *
   * @return {Title}
   * @private
   */
  __initEvents() {
    this._node.addEventListener('click', function (event) {
      let target = event.target;
      while (target && target !== this._node) {
        if (target.className.indexOf('ottbp-btn-delete') > -1) {
          this.__clearDateSelect();
          return false;
        }
        target = target.parentNode;
      }
      return true;
    }.bind(this));
    return this;
  }

  // whenClean(callback) {
  //   this._onClean = callback;
  //   return this;
  // }

  __clearDateSelect() {
    this.update();
    this._eventStore.dispatch('DatePicker::do:clean');
    // if (this._onClean)
    //   this._onClean();
  }

  /**
   *
   * @param {CalendarValue} [date]
   * @param {null|{year:number, month:number}} [inMonth]
   * @param {boolean} [isRange=false]
   * @return {Title}
   */
  update(date, inMonth, isRange = false) {
    let text = `<span>${l8n.DatePicker.title.empty}</span>`
      , defaultText = text;
    let canRemove = false;

    if (date) {

      if (isRange) {

        let start = '<span class="ottbp-gray">туда</span>', end = '<span class="ottbp-gray">обратно</span>';
        let selectRange = date.range, hoverRange = date.rangeHover;

        if (selectRange.start) {
          canRemove = true;
        }
        if (hoverRange.start) {
          start = `<span class="ottbp-gray">${dateString(hoverRange.start)}</span>`;
        } else if (selectRange.start) {
          start = `<span class="ottbp-black">${dateString(selectRange.start)}</span>`;
        }

        if (hoverRange.end) {
          if (this._duration) {
            start = `<span class="ottbp-gray">${dateString(hoverRange.start)}</span>`;
          } else {
            start = `<span class="ottbp-black">${dateString(hoverRange.start)}</span>`;
          }
          end = `<span class="ottbp-gray">${dateString(hoverRange.end)}</span>`;
        } else if (selectRange.end && !hoverRange.start) {
          end = `<span class="ottbp-black">${dateString(selectRange.end)}</span>`;
        }

        let newStart = start !== '<span class="ottbp-gray">туда</span>'
          , newEnd = end !== '<span class="ottbp-gray">обратно</span>';

        if (newStart || newEnd) {
          text = `${start} &mdash; ${end}`;

        }

      } else {
        if (date.hover) {
          text = `<span class="ottbp-gray">${dateString(date.hover)}</span>`;
        } else if (date.select) {

          canRemove = true;
          text = `<span class="ottbp-black">${dateString(date.select)}</span>`;
        }
      }

      // if (selectedStart /*&& selectedEnd*/) {
      //   canRemove = true;
      // }

      // if (data.st)
      //
    }

    if (text === defaultText && inMonth) {
      text = `<span>В ${l8n.month.praepositionalis[inMonth.month]}</span>`;
    }

    if (this._stringCache != text) {
      this._stringCache = text;
      this._node.innerHTML = `<div class="ottbp-date__title ottbp-texts--title"><span class="ottbp-date__title__inner">${text}${canRemove ? '<button class="ottbp-btn ottbp-btn-delete">&times;</button>' : ''}</span></div>`;
      this._eventStore.dispatch('DatePicker::title:change', {
        text      : text,
        canRemove : canRemove
      });
    }
    return this;
  }
}