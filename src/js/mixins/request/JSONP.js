'use strict';
import Request from './Request';

/**
 *
 * @type {number}
 */

let head = null;


/**
 * @class JSONP
 * @extends Request
 */
export default class JSONP extends Request {
  /**
   *
   * @param {string} url
   * @param {RequestOptions} {options={}}
   */
  constructor(url, options = {}) {
    super(url, options);
  }

  /**
   *
   * @return {JSONP}
   * @private
   */
  __initAdds() {
    this._adds = {};

    this._generatedFunction = `jsonp_${this.$id}`;
    this._adds[this.options.method] = this._generatedFunction;

    return this;
  }

  // __onResponse(response) {
  //   if (this._done)
  //     return;
  //   this._done = true;
  //
  //   countRequest--;
  //   this._options.success(response, this.id);
  //   this.__deleteFunction().__deleteScript();
  //   return this.__onAlways();
  // }

  __beforeNewTry() {
    // this.__deleteFunction().__deleteScript();
    this._generatedFunction = `jsonp_${this.$id}_try${this._tryes}`;
  }

  __onSuccess() {
    console.log(response);
    // this.__deleteFunction().__deleteScript();
  }

  __onAlways() {
    // if (this._options.always)
    //   this._options.always();
    // playRequest();
    // return this;
    this.__deleteScript();
  }

  /**
   *
   * @return {JSONP}
   * @private
   */
  __initFunction() {
    window[this._generatedFunction] = ::this.__success;
    return this;
  }

  /**
   *
   * @return {JSONP}
   * @private
   */
  __deleteFunction() {
    // try {
    //   delete window[this._generatedFunction];
    // } catch (e) {
    //   window[this._generatedFunction] = undefined;
    // }
    return this;
  }

  /**
   *
   * @return {JSONP}
   * @private
   */
  __initScript() {
    //noinspection JSValidateTypes
    /**
     *
     * @type {HTMLScriptElement}
     */
    let script = document.createElement('script');

    script.async = true;
    script.setAttribute('data-id', this._generatedFunction);

    script.src = this._src;

    if (!head)
      head = document.getElementsByTagName('head')[0];

    head.appendChild(script);

    if (!this._scripts)
      /**
       *
       * @type {Array<HTMLScriptElement>}
       * @private
       */
      this._scripts = [];
    this._scripts.push(script);

    return this;
  }

  /**
   *
   * @return {JSONP}
   * @private
   */
  __deleteScript() {
    // console.log("__deleteScript", this.id, this._generatedFunction);
    if (this._scripts)
      this._scripts.forEach(function (/* HTMLScriptElement */script) {
        if (script && script.parentNode)
          script.parentNode.removeChild(script);
      });
    return this;
  }

  // send() {
  // if (countRequest >= MAX_REQUESTS) {
  //   pausedRequest.push(this);
  // } else {
  //   countRequest++;
  //   this._tryes++;
  //   this.__initFunction().__initSrc().__initTimeout().__append();
  // }
  // }

  /**
   *
   * @return {JSONP}
   * @private
   */
  __initSend() {
    this.__initFunction().__initScript();
    return this;
  }

}
