'use strict';

const routeSelect = {
  type    : 'route',
  selects : [{
    title  : 'Туда-обратно',
    orange : true,
    value  : true
  }, {
    title  : 'В один конец',
    orange : true,
    value  : false
  }]
};

const typeSelect = {
  type    : 'type',
  selects : [{
    title  : 'Эконом',
    orange : true,
    value  : 'E'
  }, {
    title  : 'Бизнес',
    orange : true,
    value  : 'B'
  }]
};

const transferSelect = {
  type    : 'transfer',
  selects : [{
    title  : 'Любые пересадки',
    orange : false,
    value  : 'any'
  }, {
    title  : 'Без пересадок',
    orange : true,
    value  : 'none'
  }, {
    title  : 'Не больше одной',
    orange : true,
    value  : 1
  }]
};

const durationSelect = {
  type    : 'duration',
  selects : [{
    title  : 'Любое количество дней',
    orange : false,
    value  : 'any'
  }, {
    title  : 'На 3 дня',
    orange : true,
    value  : 3
  }, {
    title  : 'На 5 дней',
    orange : true,
    value  : 5
  }, {
    title  : 'На 7 дней',
    orange : true,
    value  : 7
  }]
};

const selects = [routeSelect, typeSelect, transferSelect, durationSelect];

export default selects;